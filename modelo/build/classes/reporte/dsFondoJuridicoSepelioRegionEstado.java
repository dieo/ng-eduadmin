/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsFondoJuridicoSepelioRegionEstado implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet fondo;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsFondoJuridicoSepelioRegionEstado(Object mod) {
        this.fondo = (java.sql.ResultSet)mod;
        try {
            this.fondo.last();
            total = this.fondo.getRow()+1;
            this.fondo.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.fondo.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedula".equals(jrCampo.getName())) {
                valor = fondo.getString("cedula");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = fondo.getString("apellidonombre");
            } else if("estado".equals(jrCampo.getName())) {
                valor = fondo.getString("estado");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = fondo.getInt("numerooperacion");
            } else if("regional".equals(jrCampo.getName())) {
                valor = fondo.getString("regional");
            } else if("importetitular".equals(jrCampo.getName())) {
                valor = fondo.getDouble("importetitular");
            } else if("cantidadadherente".equals(jrCampo.getName())) {
                valor = fondo.getInt("cantidadadherente");
            } else if("importeadherente".equals(jrCampo.getName())) {
                valor = fondo.getInt("cantidadadherente") * fondo.getDouble("importeadherente");
            } else if("moneda".equals(jrCampo.getName())) {
                valor = fondo.getString("moneda");
            } else if("fechaoperacion".equals(jrCampo.getName())) {
                valor = fondo.getDate("fechaoperacion");
            } else if("fechainicio".equals(jrCampo.getName())) {
                valor = fondo.getDate("fechaprimervencimiento");
            } else if("compromiso".equals(jrCampo.getName())) {
                valor = fondo.getDouble("montocapital");
            } else if("pagado".equals(jrCampo.getName())) {
                valor = fondo.getDouble("montocapital")-fondo.getDouble("saldocapital");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
