/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsContratoPrestamo implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet contrato;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsContratoPrestamo(Object mod) {
        this.contrato = (java.sql.ResultSet)mod;
        try {
            this.contrato.last();
            total = this.contrato.getRow()+1;
            this.contrato.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.contrato.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            String snumerosolicitud = new java.text.DecimalFormat("###,###,###,###").format(this.contrato.getInt("numerosolicitud"));
            String smutual = this.contrato.getString("mutual");
            String sprestatario = this.contrato.getString("prestatario");
            java.util.Date dfecha = this.contrato.getDate("fechageneracion");
            String sdia = new utilitario.utiLetra().leerNumero(utilitario.utiFecha.getDia(dfecha)+0.0).trim().toLowerCase();
            String smes = utilitario.utiFecha.getMes(dfecha);
            String sano = new utilitario.utiLetra().leerNumero(utilitario.utiFecha.getAnho(dfecha)+0.0).trim().toLowerCase();
            String sentidad = this.contrato.getString("entidad");
            //String spresidente = this.contrato.getString("presidente");
            //String scedulaPresidente = this.contrato.getString("cedulapresidente");
            //String scargoPresidente = this.contrato.getString("cargopresidente");
            //String stesorero = this.contrato.getString("tesorero");
            //String scedulaTesorero = this.contrato.getString("cedulatesorero");
            //String scargoTesorero = this.contrato.getString("cargotesorero");
            String sruc = this.contrato.getString("ruc");
            String scalle = this.contrato.getString("calle");
            String scedulaPrestatario1 = this.contrato.getString("cedulaprestatario1");
            String snombreApellidoPrestatario1 = this.contrato.getString("nombreapellidoprestatario1");
            String sdireccionPrestatario1 = utilitario.utiCadena.convertirMayusMinus(this.contrato.getString("direccionprestatario1"));
            String snumeroCasaPrestatario1 = "";
            if (this.contrato.getInt("numerocasaprestatario1")>0) snumeroCasaPrestatario1 = "Nº "+this.contrato.getInt("numerocasaprestatario1");
            String sciudadPrestatario1 = utilitario.utiCadena.convertirMayusMinus(this.contrato.getString("ciudadprestatario1"));
            String scargoPrestatario1 = utilitario.utiCadena.convertirMayusMinus(this.contrato.getString("cargoprestatario1"));
            String scedulaPrestatario2 = this.contrato.getString("cedulaprestatario2");
            String snombreApellidoPrestatario2 = this.contrato.getString("nombreapellidoprestatario2");
            String sdireccionPrestatario2 = utilitario.utiCadena.convertirMayusMinus(this.contrato.getString("direccionprestatario2"));
            String snumeroCasaPrestatario2 = "";
            if (this.contrato.getInt("numerocasaprestatario2")>0) snumeroCasaPrestatario2 = "Nº "+this.contrato.getInt("numerocasaprestatario2");
            String sciudadPrestatario2 = utilitario.utiCadena.convertirMayusMinus(this.contrato.getString("ciudadprestatario2"));
            String scargoPrestatario2 = utilitario.utiCadena.convertirMayusMinus(this.contrato.getString("cargoprestatario2"));
            String smonto = new java.text.DecimalFormat("###,###,###,###").format(this.contrato.getDouble("monto"));
            String smontoLetra = new utilitario.utiLetra().leerNumero(this.contrato.getDouble("monto")).trim().toLowerCase();
            String splazo = this.contrato.getString("plazo");
            String splazoLetra = new utilitario.utiLetra().leerNumero(this.contrato.getInt("plazo")+0.0).trim().toLowerCase();
            String stasaFijaMensual = new java.text.DecimalFormat("###.##").format(this.contrato.getDouble("tasafijamensual"));
            String stasaFijaMensualLetra = new utilitario.utiLetra().leerNumero(this.contrato.getDouble("tasafijamensual")).trim().toLowerCase();
            String stasaEfectivaAnual = new java.text.DecimalFormat("###.##").format(this.contrato.getDouble("tasaefectivaanual"));
            String stasaEfectivaAnualLetra = new utilitario.utiLetra().leerNumero(this.contrato.getDouble("tasaefectivaanual")).trim().toLowerCase();
            String sinteresPunitorio = new java.text.DecimalFormat("###.##").format(this.contrato.getDouble("interespunitorio"));
            String sinteresPunitorioLetra = new utilitario.utiLetra().leerNumero(this.contrato.getDouble("interespunitorio")).trim().toLowerCase();
            String splazoDia = new java.text.DecimalFormat("###.##").format(this.contrato.getDouble("plazodia"));
            String splazoDiaLetra = new utilitario.utiLetra().leerNumero(this.contrato.getDouble("plazodia")).trim().toLowerCase();
            if("referencia".equals(jrCampo.getName())) {
                valor = "Nº SOLICITUD: "+snumerosolicitud;
            } else if("textoprincipal".equals(jrCampo.getName())) {
                //valor = "En la ciudad de Asunción, Capital de la República del Paraguay, a los "+sdia+" día(s) del mes ";
                //valor += "de "+smes+" del año "+sano+", por una parte la "+sentidad+", representada en este acto por el ";
                //valor += spresidente+", con cédula de identidad civil Nº "+scedulaPresidente+" y la "+stesorero+", con cédula de identidad civil Nº "+scedulaTesorero+", ";
                //valor += scargoPresidente+" y "+scargoTesorero+" de la Comisión Directiva, respectivamente, con domicilio en la calle "+scalle+" de la ciudad de Asunción, ";
                //valor += "en adelante "+smutual+", y por la otra, "+snombreApellidoPrestatario1+", con cédula de identidad civil Nº ";
                valor = "En la ciudad de Asunción, Capital de la República del Paraguay, a los "+sdia+" día(s) del mes ";
                valor += "de "+smes+" del año "+sano+", por una parte la "+sentidad+", con Registro Unico de Contribuyente Nº ";
                valor += sruc+", con domicilio en la calle "+scalle+" de la ciudad de Asunción, ";
                valor += "en adelante "+smutual+", y por la otra, "+snombreApellidoPrestatario1+", con Cédula de Identidad Civil Nº ";
                valor += scedulaPrestatario1+", con domicilio en "+sdireccionPrestatario1+" "+snumeroCasaPrestatario1+" de la ciudad de "+sciudadPrestatario1;
                if (scedulaPrestatario2!=null) {
                    valor += ", y ";
                    valor += snombreApellidoPrestatario2+", con Cédula de Identidad Civil Nº "+scedulaPrestatario2+", ";
                    valor += "con domicilio en "+sdireccionPrestatario2+" "+snumeroCasaPrestatario2+" de la ciudad de "+sciudadPrestatario2+", respectivamente";
                }
                valor += ", en adelante "+sprestatario+", en este acto convienen en celebrar el presente CONTRATO DE PRESTAMO, ";
                valor += "que se regirá por las siguientes cláusulas y condiciones:";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            } else if("item1".equals(jrCampo.getName())) {
                valor = "Clásula Primera: DEL OBJETO: En virtud al presente Contrato, "+smutual+" da y otorga un préstamo de dinero a favor de "+sprestatario+", ";
                valor += "por la suma de Gs. "+smonto+" ("+smontoLetra.substring(0,1).toUpperCase()+smontoLetra.substring(1,smontoLetra.length())+" guaraníes) ";
                valor += "que, "+sprestatario+" declara/n recibir a su entera satisfacción a tiempo de la suscripción del presente Contrato, ";
                valor += "previa deducción del monto estipulado en concepto del Impuesto al Valor Agregado (IVA). La firma estampada por "+sprestatario+" ";
                valor += "en este Contrato constituye, en consecuencia, suficiente prueba que acredita el desembolso íntegro del préstamo. ";
                valor += "Se consideran en carácter de prestatarios al deudor y al codeudor.";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            } else if("item2".equals(jrCampo.getName())) {
                valor = "Cláusula Segunda: DEL PLAZO Y FORMA DE PAGO: "+sprestatario+" se obliga/n a devolver la suma total de dinero recibida, ";
                valor += "objeto de este Contrato, en el plazo máximo e improrrogable de "+splazo+" ("+splazoLetra+") meses, computables a partir ";
                valor += "de la fecha del desembolso del préstamo, mediante amortizaciones de capital e interés iguales y consecutivas, según el ";
                valor += "plan de pagos que en Anexo es parte integrante e indisoluble del presente Contrato. El pago del préstamo contratado ";
                valor += "se realizará mediante débito automático del salario que percibe como funcionario permanente o contratado del ";
                valor += "Ministerio de Salud Pública y Bienestar Social, como jubilado de la Caja de Jubilaciones y Pensiones o en la ";
                valor += "Tesorería de la "+smutual+". La tolerancia que otorgue "+smutual+" ante un eventual incumplimiento de pago de ";
                valor += "parte de "+sprestatario+" a cualesquiera de los pagos, no constituirá prórroga ni novación de este Contrato, ";
                valor += "toda vez que esa situación no afecta el derecho de "+smutual+" para exigir judicialmente o extrajudicialmente la ";
                valor += "cancelación total del préstamo, capital e interés, conforme a la liquidación, sin necesidad de ningún requerimiento previo.";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            } else if("item3".equals(jrCampo.getName())) {
                valor = "Clásula Tercera: DE LOS INTERESES: "+sprestatario+" se obliga/n a pagar intereses, con una Tasa Fija Mensual del ";
                valor += stasaFijaMensual+" % ("+stasaFijaMensualLetra+" porciento); cuya Tasa Efectiva Anual es de "+stasaEfectivaAnual+" % (";
                valor += stasaEfectivaAnualLetra+" porciento). El incumplimiento del pago de las cuotas periódicas en los montos y fechas previstas, ";
                valor += "sin necesidad de requerimiento, dará lugar al cobro de intereses punitorios sobre las cuotas vencidas en el porcentaje ";
                valor += "correspondiente al "+sinteresPunitorio+" % ("+sinteresPunitorioLetra+" porciento) de la tasa de interés moratorio estipulada";
                valor += "para el presente préstamo de dinero.";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            } else if("item4".equals(jrCampo.getName())) {
                valor = "Cláusula Cuarta: La mora se producirá por el mero vencimiento de los plazos, sin necesidad de interpelación ";
                valor += "judicial alguna. La falta de pago de tres o más cuotas consecutivas de amortización de capital y/o de sus tres o más ";
                valor += "cuotas consecutivas correspondiente al pago de los intereses, así como el incumplimiento por parte de "+sprestatario+" ";
                valor += "de cualquiera de las cláusuras del presente Contrato y de las prescripciones del Reglamento de Crédito, hará decaer ";
                valor += "de pleno derecho todos los plazos estipulados para el pago en cuotas de la restitución del préstamo y sus intereses, ";
                valor += "produciendo el vencimiento anticipado de todas ellas, y facilitando a "+smutual+"a declarar vencido todos los plazos ";
                valor += "establecidos a favor del deudor en este Contrato, y a exigir el pago de la totalidad de la deuda, incluyendo los intereses ";
                valor += "compensatorios devengados, y los intereses moratorios y punitorios que se devenguen más el Impuesto al Valor Agregado, ";
                valor += "hasta la cancelación definitiva del préstamo, procediendo a ejecutar para el efecto el Pagaré suscripto juntamente con este Contrato.";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            } else if("item5".equals(jrCampo.getName())) {
                valor = "Cláusula Quinta: "+sprestatario+" por el presente instrumento autoriza/n a "+smutual+", en forma irrevocable, otorgando ";
                valor += "suficiente mandato en todos los términos del artículo 917, inciso a) del Código Civil, para que por propia cuenta o a ";
                valor += "través de empresas especializadas, recabe en plaza datos sobre su situación patrimonial, solvencia económica, cumplimiento ";
                valor += "de obligaciones comerciales, como así también la confirmación y/o certificación de los datos por él proveídos; de igual manera ";
                valor += "y en los mismos términos, autorizan a proporcionar en forma responsable datos a terceros para referencias comerciales, ";
                valor += "situación patrimonial declarada en la institución; a recibir informaciones relacionadas a la organización, de igual manera, ";
                valor += "en los mismos términos "+sprestatario+" autoriza/n a "+smutual+" para que en caso de un atraso superior a "+splazoDia+" (";
                valor += splazoDiaLetra+") días en el pago de las obligaciones pactadas, incluyan su nombre personal o razón social presentada en el ";
                valor += "Registro de Morosos de INFORMCONF. La supresión de dicho registro se realizará de acuerdo a los términos establecidos en la ";
                valor += "Ley vigente en la materia. "+sprestatario+" autoriza/n suficientemente por este instrumento a "+smutual+" a la venta del ";
                valor += "presente crédito en caso de mora.";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            } else if("item6".equals(jrCampo.getName())) {
                valor = "Cláusula Sexta: En caso de incumplimiento por parte de "+sprestatario+" en el pago de las cuotas, éste autoriza a "+smutual+" ";
                valor += "a resolver el Contrato y a proceder a la demanda judicial de pago, donde los gastos derivados del requerimiento de pago ";
                valor += "serán a cargo exclusivo de "+sprestatario+".";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            } else if("item7".equals(jrCampo.getName())) {
                valor = "Cláusula Séptima: A todos los efectos legales y procesales, las partes se someten a la jurisdicción y competencia de los ";
                valor += "Tribunales de Asunción y constituyen domicilio especial en las direcciones que figuran al inicio del presente documento.";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            } else if("item8".equals(jrCampo.getName())) {
                valor = "En prueba de conformidad firman las partes en dos ejemplares de un mismo tenor y a un solo efecto.";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
                valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            } else if("prestatario1".equals(jrCampo.getName())) {
                if (snombreApellidoPrestatario2==null) {
                    valor = "";
                } else {
                    valor = "..............................................................................................\n";
                    valor += snombreApellidoPrestatario1 + " - " + scargoPrestatario1;
                }
            } else if("prestatario2".equals(jrCampo.getName())) {
                if (snombreApellidoPrestatario2==null) {
                    valor = "..............................................................................................\n";
                    valor += snombreApellidoPrestatario1 + " - " + scargoPrestatario1;
                } else {
                    valor = "..............................................................................................\n";
                    valor += snombreApellidoPrestatario2 + " - " + scargoPrestatario2;
                }
            //} else if("presidente".equals(jrCampo.getName())) {
            //    valor = "..............................................................................................\n";
            //    valor += spresidente + " - " + scargoPresidente;
            //} else if("tesorero".equals(jrCampo.getName())) {
            //    valor = "..............................................................................................\n";
            //    valor += stesorero + " - " + scargoTesorero;
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
