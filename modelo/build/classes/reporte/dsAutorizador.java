/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAutorizador implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modAutorizador mod;
    private int index = -1;

    public dsAutorizador(Object mod) {
        this.mod = (especifico.modAutorizador)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("funcionario".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFuncionario();
        } else if("nivelaprobacion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNivelAprobacion();
        }
        return valor;
    }
    
}
