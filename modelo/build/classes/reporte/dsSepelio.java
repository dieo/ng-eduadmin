/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsSepelio implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modSepelio mod;
    private int index = -1;

    public dsSepelio(Object mod) {
        this.mod = (especifico.modSepelio)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("estado".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getEstadoSolicitud();
        } else if("regional".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getRegional();
        } else if("numerosolicitud".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNumeroSolicitud();
        } else if("fechasolicitud".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaSolicitud();
        } else if("cedula".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCedula();
        } else if("apellidonombre".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getApellido()+", "+mod.getEntidad(index).getNombre();
        } else if("cobertura".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCoberturaSepelio();
        } else if("antiguedad".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getAntiguedad();
        } else if("observacionsolicitud".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getObservacionSolicitud();
        } else if("afectado".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCedulaAfectado()+" "+mod.getEntidad(index).getNombreAfectado();
        } else if("montoaprobado".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getMontoAprobado();
        } else if("fechaaprobacion".equals(jrCampo.getName())) {
            if (mod.getEntidad(index).getEstadoSolicitud().contains("ANULADO")) {
                valor = mod.getEntidad(index).getFechaAnulado();
            }
            if (mod.getEntidad(index).getEstadoSolicitud().contains("APROBADO")) {
                valor = mod.getEntidad(index).getFechaAprobado();
            }
            if (mod.getEntidad(index).getEstadoSolicitud().contains("RECHAZADO")) {
                valor = mod.getEntidad(index).getFechaRechazado();
            }
        } else if("observacionaprobacion".equals(jrCampo.getName())) {
            if (mod.getEntidad(index).getEstadoSolicitud().contains("ANULADO")) {
                valor = mod.getEntidad(index).getObservacionAnulado();
            }
            if (mod.getEntidad(index).getEstadoSolicitud().contains("APROBADO")) {
                valor = mod.getEntidad(index).getObservacionAprobado();
            }
            if (mod.getEntidad(index).getEstadoSolicitud().contains("RECHAZADO")) {
                valor = mod.getEntidad(index).getObservacionRechazado();
            }
        }
        return valor;
    }
    
}
