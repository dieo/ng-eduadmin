/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsListadoCobranzaResumenComercioExterno implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsListadoCobranzaResumenComercioExterno(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("idcuenta".equals(jrCampo.getName())) {
                valor = listado.getInt("idcuenta");
            } else if("descripcion".equals(jrCampo.getName())) {
                valor = listado.getString("descripcion");
            } else if("montoenviado".equals(jrCampo.getName())) {
                valor = listado.getDouble("enviado");
            } else if("montocobrado".equals(jrCampo.getName())) {
                valor = listado.getDouble("cobradototal");
            } else if("montosinhacienda".equals(jrCampo.getName())) {
                valor = listado.getDouble("cobradosinhacienda");
            } 
        } catch (Exception e) { }
        return valor;
    }
    
}
