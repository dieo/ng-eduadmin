/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsMovimientoCajaChicaConcepto implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet movimiento;
    private int index = 0;
    private int totalDetalle = 0;

    public dsMovimientoCajaChicaConcepto(Object mod) {
        this.movimiento = (java.sql.ResultSet)mod;
        try {
            this.movimiento.last();
            totalDetalle = this.movimiento.getRow()+1;
            this.movimiento.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { movimiento.next();
        } catch (Exception e) { }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
            try {
                if ("fechaoperacion".equals(jrCampo.getName())) {
                    valor = movimiento.getDate("fechaoperacion");
                } else if ("planilla".equals(jrCampo.getName())) {
                    int pos = utilitario.utiCadena.getPosicion(movimiento.getString("planilla"),"(",0);
                    valor = utilitario.utiCadena.convertirMayusMinus(movimiento.getString("planilla")).substring(0,pos).trim();
                } else if ("cierre".equals(jrCampo.getName())) {
                    if (utilitario.utiFecha.convertirToStringDMA(movimiento.getDate("fechacierre")).isEmpty()) valor = "";
                    else valor = utilitario.utiFecha.convertirToStringDMA(movimiento.getDate("fechacierre"))+" "+movimiento.getString("horacierre");
                } else if ("descripcion".equals(jrCampo.getName())) {
                    valor = movimiento.getString("descripcion");
                } else if ("concepto".equals(jrCampo.getName())) {
                    valor = movimiento.getString("concepto");
                } else if ("montodebito".equals(jrCampo.getName())) {
                    valor = movimiento.getDouble("montodebito");
                } else if ("montocredito".equals(jrCampo.getName())) {
                    valor = movimiento.getDouble("montocredito");
                } else if ("montoimpuesto".equals(jrCampo.getName())) {
                    valor = movimiento.getDouble("montoimpuesto");
                } else if ("montototal".equals(jrCampo.getName())) {
                    valor = movimiento.getDouble("montototal");
                }
            } catch (Exception e) { }
        return valor;
    }
    
}
