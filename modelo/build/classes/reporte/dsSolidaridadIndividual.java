/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsSolidaridadIndividual implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet solidaridad;
    private int index = 0;
    private int total = 0;

    public dsSolidaridadIndividual(Object mod) {
        this.solidaridad = (java.sql.ResultSet)mod;
        try {
            this.solidaridad.last();
            total = this.solidaridad.getRow()+1;
            this.solidaridad.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { solidaridad.next();
        } catch (Exception e) { }
        return ++index < this.total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedula".equals(jrCampo.getName())) {
                valor = solidaridad.getString("cedula");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = solidaridad.getString("apellido") + ", " + solidaridad.getString("nombre");
            } else if("antiguedad".equals(jrCampo.getName())) {
                valor = solidaridad.getInt("antiguedad")+" meses";
            } else if("cobertura".equals(jrCampo.getName())) {
                valor = solidaridad.getString("cobertura");
            } else if("numerosolicitud".equals(jrCampo.getName())) {
                valor = solidaridad.getInt("numerosolicitud");
            } else if("fechasolicitud".equals(jrCampo.getName())) {
                valor = solidaridad.getDate("fechasolicitud");
            } else if("observacionsolicitud".equals(jrCampo.getName())) {
                valor = solidaridad.getString("observacionsolicitud");
            } else if("estado".equals(jrCampo.getName())) {
                valor = solidaridad.getString("estado");
            } else if("afectado".equals(jrCampo.getName())) {
                valor = solidaridad.getString("cedulaafectado")+" "+solidaridad.getString("nombreafectado");
            } else if("montoaprobado".equals(jrCampo.getName())) {
                valor = solidaridad.getDouble("montoaprobado");
            } else if("moneda".equals(jrCampo.getName())) {
                valor = solidaridad.getString("moneda");
            } else if("fechaaprobacion".equals(jrCampo.getName())) {
                if (solidaridad.getString("estado").contains("ANULADO"))   valor = solidaridad.getDate("fechaanulado");
                if (solidaridad.getString("estado").contains("APROBADO"))  valor = solidaridad.getDate("fechaaprobado");
                if (solidaridad.getString("estado").contains("RECHAZADO")) valor = solidaridad.getDate("fecharechazado");
            } else if("observacionaprobacion".equals(jrCampo.getName())) {
                if (solidaridad.getString("estado").contains("ANULADO"))   valor = solidaridad.getString("observacionanulado");
                if (solidaridad.getString("estado").contains("APROBADO"))  valor = solidaridad.getString("observacionaprobado");
                if (solidaridad.getString("estado").contains("RECHAZADO")) valor = solidaridad.getString("observacionrechazado");
            } else if("documento".equals(jrCampo.getName())) {
                valor = "- "+solidaridad.getString("documento");
            } else if("chequeado".equals(jrCampo.getName())) {
                valor = "NO";
                if (solidaridad.getBoolean("chequeado")) valor = "SI";
            } else if("observaciondocumento".equals(jrCampo.getName())) {
                valor = solidaridad.getString("observaciondocumento");
            }
        } catch (Exception e) { }
        
        return valor;
    }
    
}
