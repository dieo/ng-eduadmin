/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbReceptorCheque implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modbReceptorCheque mod;
    private int index = -1;

    public dsbReceptorCheque(Object mod) {
        this.mod = (especifico.modbReceptorCheque)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("cedularuc".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCedulaRuc();
        } else if("razon".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDescripcion();
        } else if("personeria".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getPersoneria();
        } else if("centrocosto".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCentroCosto();
        }
        return valor;
    }
    
}
