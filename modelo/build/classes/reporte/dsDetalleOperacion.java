/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsDetalleOperacion implements net.sf.jasperreports.engine.JRDataSource {
    
    private java.sql.ResultSet detalle;
    private int index = 0;
    private int totalDetalle = 0;

   
    public dsDetalleOperacion(Object mod) {
        this.detalle = (java.sql.ResultSet)mod;
        try {
            this.detalle.last();
            totalDetalle = this.detalle.getRow()+1;
            this.detalle.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { detalle.next();
        } catch (Exception e) { }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if ("cedula".equals(jrCampo.getName())) {
                valor =detalle.getString("cedula");
            } else if ("apellidonombre".equals(jrCampo.getName())) {
                valor = detalle.getString("apellidonombre");
            } else if ("operacion".equals(jrCampo.getName())) {
                valor = detalle.getString("operacion");
            } else if ("fuente".equals(jrCampo.getName())) {
                valor = detalle.getString("fuente");
            } else if ("tipooperacion".equals(jrCampo.getName())) {
                valor = detalle.getString("tipooperacion");
            } else if ("numerosolicitud".equals(jrCampo.getName())) {
                valor = detalle.getInt("numerosolicitud");   
            } else if ("numerooperacion".equals(jrCampo.getName())) {
                valor = detalle.getInt("numerooperacion");    
            } else if ("fechaoperacion".equals(jrCampo.getName()))  {
                valor = detalle.getDate("fechaoperacion");
            } else if ("plazo".equals(jrCampo.getName())) {
                valor = detalle.getInt("plazo");
            } else if ("tasa".equals(jrCampo.getName())) {
                valor = detalle.getDouble("tasa");      
            } else if ("capital".equals(jrCampo.getName())) {
                valor = detalle.getDouble("capital");
            } else if ("interes".equals(jrCampo.getName())) {
                valor = detalle.getDouble("interes");   
            } else if ("giraduria".equals(jrCampo.getName())) {
                valor = detalle.getString("giraduria");    
            } else if ("rubro".equals(jrCampo.getName())) {
                valor = detalle.getString("rubro");
            } else if ("numerocuota".equals(jrCampo.getName())) {
                valor = detalle.getInt("numerocuota");
            } else if ("fechavencimiento".equals(jrCampo.getName()))  {
                valor = detalle.getDate("fechavencimiento");    
            } else if ("montocapital".equals(jrCampo.getName())) {
                valor = detalle.getDouble("montocapital");
            } else if ("montointeres".equals(jrCampo.getName())) {
                valor = detalle.getDouble("montointeres");
            } else if ("cobro".equals(jrCampo.getName())) {
                valor = detalle.getDouble("cobro");    
            } else if ("saldocapital".equals(jrCampo.getName())) {
                valor = detalle.getDouble("saldocapital");    
            } else if ("saldointeres".equals(jrCampo.getName())) {
                valor = detalle.getDouble("saldointeres");    
            } else if ("estadodetalle".equals(jrCampo.getName())) {
                valor = detalle.getString("estadodetalle");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
