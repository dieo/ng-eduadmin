/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbDepositoFecha implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modbDepositoCuenta mod;
    private int index = -1;

    public dsbDepositoFecha(Object mod) {
        this.mod = (especifico.modbDepositoCuenta)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("nrodeposito".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNumeroDeposito();
        } else if("boleta".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNumeroBoleta();
        } else if("fechadeposito".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFecha();
        } else if("montoefectivo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoEfectivo();
        } else if("montootro".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoChequeOtro();
        } else if("montopropio".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoChequePropio();
        } else if("montototal".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoEfectivo()+this.mod.getEntidad(index).getMontoChequeOtro()+this.mod.getEntidad(index).getMontoChequePropio();
        } else if("numerocuenta".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNumeroCuenta();
        } else if("concepto".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNota();
        }
        return valor;
    }
    
}
