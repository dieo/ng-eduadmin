/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dscHistoricoPedidos implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet pedidos;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dscHistoricoPedidos(Object mod) {
        this.pedidos = (java.sql.ResultSet)mod;
        try {
            this.pedidos.last();
            total = this.pedidos.getRow()+1;
            this.pedidos.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.pedidos.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        try {
            if("descripcion".equals(jrCampo.getName())) {
                valor = this.pedidos.getString("descripcion");
            } else if("filial".equals(jrCampo.getName())) {
                valor = this.pedidos.getString("filial");
            } else if("dependencia".equals(jrCampo.getName())) {
                valor = this.pedidos.getString("dependencia");
            } else if("solicitante".equals(jrCampo.getName())) {
                valor = this.pedidos.getString("apellidonombre");
            } else if("estado".equals(jrCampo.getName())) {
                valor = this.pedidos.getString("entregado2");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = this.pedidos.getDate("fecha");
            } else if("numeropedido".equals(jrCampo.getName())) {
                valor = this.pedidos.getInt("id");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = this.pedidos.getDouble("cantidad");
            } else if("costo".equals(jrCampo.getName())) {
                valor = this.pedidos.getDouble("costo");
            } else if("unidad".equals(jrCampo.getName())) {
                valor = this.pedidos.getString("unidadmedida");
            }
        } catch (Exception e){            
        }
        return valor;
    }
    
}
