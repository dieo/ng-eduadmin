/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsIngresoPlanilla implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modDetalleIngresoImpresion cabecera;
    private especifico.modDetalleIngresoImpresion detalle;
    private int index = -1;

    public dsIngresoPlanilla(Object mod, Object mod2) {
        this.cabecera = (especifico.modDetalleIngresoImpresion)mod;
        this.detalle = (especifico.modDetalleIngresoImpresion)mod2;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < detalle.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("planilla".equals(jrCampo.getName())) {
            valor = cabecera.getEntidad(0).getPlanilla();
        } else if("cajero".equals(jrCampo.getName())) {
            valor = cabecera.getEntidad(0).getFuncionario();
        } else if("sucursal".equals(jrCampo.getName())) {
            valor = cabecera.getEntidad(0).getSucursal();
        } else if("apertura".equals(jrCampo.getName())) {
            valor = cabecera.getEntidad(0).getApertura();
        } else if("cierre".equals(jrCampo.getName())) {
            valor = cabecera.getEntidad(0).getCierre();
        } else if("montoinicial".equals(jrCampo.getName())) {
            valor = cabecera.getEntidad(0).getMontoInicial();
        }
        if (detalle.getEntidad(index).getInforme().isEmpty()) {
            if("informe".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getInforme();
            } else if("cedularuc".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getRuc();
            } else if("razonsocial".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(detalle.getEntidad(index).getRazonSocial());
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getNumeroOperacion();
            } else if("fechaoperacion".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getFechaOperacion();
            } else if("estado".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(detalle.getEntidad(index).getEstado());
            } else if("importetotal".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getImporteTotal();
            } else if("numerodocumento".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getNumeroDocumento();
            } else if("grupo".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getGrupo();
            } else if("importe".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getImporte();
            }
        } else {
            if("informe".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getInforme();
            } else if("razonsocial".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getValor();
            } else if("importe".equals(jrCampo.getName())) {
                valor = detalle.getEntidad(index).getImporteTotal()-detalle.getEntidad(index).getImporte();
            }
        }
        return valor;
    }
    
}
