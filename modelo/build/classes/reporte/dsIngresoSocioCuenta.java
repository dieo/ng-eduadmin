/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsIngresoSocioCuenta implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsIngresoSocioCuenta(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedularuc".equals(jrCampo.getName())) {
                valor = listado.getString("cedula");
            } else if("razonsocial".equals(jrCampo.getName())) {
                valor = listado.getString("razonsocial");
            } else if("obs".equals(jrCampo.getName())) {
                valor = listado.getString("observacion");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = listado.getDate("fecha");
            } else if("ingreso".equals(jrCampo.getName())) {
                valor = listado.getInt("ingreso");
            } else if("importe".equals(jrCampo.getName())) {
                valor = listado.getDouble("importe");
            } else if("viacobro".equals(jrCampo.getName())) {
                valor = listado.getString("viacobro");
            } else if("valor".equals(jrCampo.getName())) {
                valor = listado.getString("valor");
            } else if("ingresosaldo".equals(jrCampo.getName())) {
                valor = listado.getDouble("ingresosaldo");
            } else if("idcuenta".equals(jrCampo.getName())) {
                valor = listado.getInt("idcuenta");
            } else if("cuenta".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("cuenta"));
            } else if("recibo".equals(jrCampo.getName())) {
                valor = listado.getString("recibo");
            } else if("factura".equals(jrCampo.getName())) {
                valor = listado.getString("factura");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = listado.getInt("numerooperacion");
            } else if("atraso".equals(jrCampo.getName())) {
                valor = listado.getDouble("atraso");
            } else if("mes".equals(jrCampo.getName())) {
                valor = listado.getDouble("mes");
            } else if("saldo".equals(jrCampo.getName())) {
                valor = listado.getDouble("saldo");
            } else if("montoexoneracion".equals(jrCampo.getName())) {
                valor = listado.getDouble("montoexoneracion");
            }
        } catch (Exception e) { }
        return valor;
    }
}
