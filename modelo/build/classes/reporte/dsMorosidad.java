/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsMorosidad implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0;
    private int totalDetalle = 0;

    public dsMorosidad(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            totalDetalle = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { listado.next();
        } catch (Exception e) { }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if ("cedula".equals(jrCampo.getName())) {
                valor = listado.getString("cedula");
            } else if ("nombreapellido".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("nombre")+" "+listado.getString("apellido"));
            } else if ("giraduriarubro".equals(jrCampo.getName())) {
                valor = listado.getString("giraduria")+" "+listado.getString("rubro");
            } else if ("regional".equals(jrCampo.getName())) {
                valor = listado.getString("regional");
            } else if ("cantidadaporte".equals(jrCampo.getName())) {
                valor = listado.getInt("diaap");
            } else if ("atrasoaporte".equals(jrCampo.getName())) {
                valor = listado.getDouble("atrasoap");
            } else if ("cantidadahorro".equals(jrCampo.getName())) {
                valor = listado.getInt("diaah");
            } else if ("atrasoahorro".equals(jrCampo.getName())) {
                valor = listado.getDouble("atrasoah");
            } else if ("cantidadfondojuridico".equals(jrCampo.getName())) {
                valor = listado.getInt("diafj");
            } else if ("atrasofondojuridico".equals(jrCampo.getName())) {
                valor = listado.getDouble("atrasofj");
            } else if ("cantidadoperacionfija".equals(jrCampo.getName())) {
                valor = listado.getInt("diaof");
            } else if ("atrasooperacionfija".equals(jrCampo.getName())) {
                valor = listado.getDouble("atrasoof");
            } else if ("cantidadcredito".equals(jrCampo.getName())) {
                valor = listado.getInt("diacr");
            } else if ("atrasocredito".equals(jrCampo.getName())) {
                valor = listado.getDouble("atrasocr");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
