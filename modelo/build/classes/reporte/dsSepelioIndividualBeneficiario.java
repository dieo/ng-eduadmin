/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsSepelioIndividualBeneficiario implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet sepelio;
    private int index = 0;
    private int total = 0;

    public dsSepelioIndividualBeneficiario(Object mod) {
        this.sepelio = (java.sql.ResultSet)mod;
        try {
            this.sepelio.last();
            total = this.sepelio.getRow()+1;
            this.sepelio.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { sepelio.next();
        } catch (Exception e) { }
        return ++index < this.total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedulabeneficiario".equals(jrCampo.getName())) {
                valor = sepelio.getString("cedulabeneficiario");
            } else if("apellidonombrebeneficiario".equals(jrCampo.getName())) {
                valor = sepelio.getString("apellidobeneficiario") + ", " + sepelio.getString("nombrebeneficiario");
            } else if("antiguedad".equals(jrCampo.getName())) {
                valor = sepelio.getInt("antiguedad")+" meses";
            } else if("cobertura".equals(jrCampo.getName())) {
                valor = sepelio.getString("coberturasepelio");
            } else if("numerosolicitud".equals(jrCampo.getName())) {
                valor = sepelio.getInt("numerosolicitud");
            } else if("fechasolicitud".equals(jrCampo.getName())) {
                valor = sepelio.getDate("fechasolicitud");
            } else if("observacionsolicitud".equals(jrCampo.getName())) {
                valor = sepelio.getString("observacionsolicitud");
            } else if("estado".equals(jrCampo.getName())) {
                valor = sepelio.getString("estado");
            } else if("afectado".equals(jrCampo.getName())) {
                valor = sepelio.getString("cedulaafectado")+" "+sepelio.getString("nombreafectado");
            } else if("montoaprobado".equals(jrCampo.getName())) {
                valor = sepelio.getDouble("montoaprobadobeneficiario");
            } else if("moneda".equals(jrCampo.getName())) {
                valor = sepelio.getString("moneda");
            } else if("fechaaprobacion".equals(jrCampo.getName())) {
                if (sepelio.getString("estado").contains("ANULADO"))   valor = sepelio.getDate("fechaanulado");
                if (sepelio.getString("estado").contains("APROBADO"))  valor = sepelio.getDate("fechaaprobado");
                if (sepelio.getString("estado").contains("RECHAZADO")) valor = sepelio.getDate("fecharechazado");
            } else if("observacionaprobacion".equals(jrCampo.getName())) {
                if (sepelio.getString("estado").contains("ANULADO"))   valor = sepelio.getString("observacionanulado");
                if (sepelio.getString("estado").contains("APROBADO"))  valor = sepelio.getString("observacionaprobado");
                if (sepelio.getString("estado").contains("RECHAZADO")) valor = sepelio.getString("observacionrechazado");
            } else if("cedulasocio".equals(jrCampo.getName())) {
                valor = sepelio.getString("cedula");
            } else if("apellidonombresocio".equals(jrCampo.getName())) {
                valor = sepelio.getString("apellido") + ", " + sepelio.getString("nombre");
            }
        } catch (Exception e) { }
        
        return valor;
    }
    
}
