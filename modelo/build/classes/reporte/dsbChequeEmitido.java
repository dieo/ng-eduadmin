/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbChequeEmitido implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modbChequeEmitido mod;
    private int index = -1;
    //private especifico.entbChequeEmitido entCheque;
    //private especifico.entbOrdenPago entOrden;
    //private int index = 0;

    //public dsbChequeEmitido(Object mod, Object mod2) {
    //    this.entCheque = (especifico.entbChequeEmitido)mod;
    //    this.entOrden = (especifico.entbOrdenPago)mod2;
    //}
    
    public dsbChequeEmitido(Object mod) {
        this.mod = (especifico.modbChequeEmitido)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
        //return ++index <= 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        //int i=77;
        if("numerocheque".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNumeroCheque();
        } else if("fechaemision".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaEmisionCheque();
        } else if("receptor".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getReceptorCheque();
        } else if("monto".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getMonto();
        } else if("numerocuenta".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNumeroCuenta();
        } else if("centrocosto".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCentroCosto();
        } else if("concepto".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getDescripcion();
        } 
        return valor;
    }
    
}
