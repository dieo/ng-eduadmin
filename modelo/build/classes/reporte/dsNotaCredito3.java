/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsNotaCredito3 implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet nota;
    private String scantidad = "";
    private String sdescripcion = "";
    private String spreciounitario = "";
    private String ssubtotal = "";
    private double utotalsubtotal = 0.0;
    private int index = 0;
    private int totalDetalle = 0;

    public dsNotaCredito3(Object mod) {
        this.nota = (java.sql.ResultSet)mod;
        try {
            this.nota.beforeFirst();
            while (this.nota.next()){
                scantidad += nota.getInt("cantidad") + "\n";
                sdescripcion += utilitario.utiCadena.convertirMayusMinus(nota.getString("descripcion")) + "\n";
                spreciounitario += utilitario.utiNumero.getMascara(nota.getDouble("precio"),0) + "\n";
                ssubtotal += utilitario.utiNumero.getMascara(nota.getDouble("precio")*nota.getDouble("cantidad"),0) + "\n";
                utotalsubtotal += nota.getDouble("precio")*nota.getDouble("cantidad");
            }
        } catch (Exception e) { }
        try {
            this.nota.last();
            totalDetalle = this.nota.getRow()+1;
            this.nota.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { nota.next();
        } catch (Exception e) { }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if (index == 1) {
            try {
                if ("fecha".equals(jrCampo.getName())) {
                    valor = utilitario.utiFecha.convertirToStringDMA(nota.getDate("fechanota"));
                } else if ("numeronota".equals(jrCampo.getName())) {
                    valor = nota.getInt("numeronota");
                } else if ("ruc".equals(jrCampo.getName())) {
                    valor = nota.getString("ruc");
                } else if ("razonsocial".equals(jrCampo.getName())) {
                    valor = utilitario.utiCadena.convertirMayusMinus(nota.getString("razonsocial"));
                } else if ("referencia".equals(jrCampo.getName())) {
                    valor = nota.getInt("referencia");
                } else if ("direccion".equals(jrCampo.getName())) {
                    valor = utilitario.utiCadena.convertirMayusMinus(nota.getString("direccion"));
                } else if ("telefono".equals(jrCampo.getName())) {
                    valor = nota.getString("telefono");
                } else if ("cantidad".equals(jrCampo.getName())) {
                    valor = scantidad;
                } else if ("descripcion".equals(jrCampo.getName())) {
                    valor = sdescripcion;
                } else if ("preciounitario".equals(jrCampo.getName())) {
                    valor = spreciounitario;
                } else if ("subtotal".equals(jrCampo.getName())) {
                    valor = ssubtotal;
                } else if ("totalsubtotal".equals(jrCampo.getName())) {
                    valor = utotalsubtotal;
                } else if ("impuesto".equals(jrCampo.getName())) {
                    valor = nota.getDouble("impuesto");
                } else if ("total".equals(jrCampo.getName())) {
                    valor = utotalsubtotal + nota.getDouble("impuesto");
                } else if ("montoletra".equals(jrCampo.getName())) {
                    String sletra = new utilitario.utiLetra().leerNumero(utotalsubtotal + nota.getDouble("impuesto")).toLowerCase();
                    valor = sletra.substring(0, 1).toUpperCase() + sletra.substring(1, sletra.length()) + ".-";
                } else if ("motivoanulacion".equals(jrCampo.getName())) {
                    if (nota.getString("motivo").contains("ANULACION")) valor = "X";
                } else if ("motivobonificacion".equals(jrCampo.getName())) {
                    if (nota.getString("motivo").contains("BONIFICACION")) valor = "X";
                } else if ("motivodescuento".equals(jrCampo.getName())) {
                    if (nota.getString("motivo").contains("DESCUENTO")) valor = "X";
                } else if ("motivodevolucion".equals(jrCampo.getName())) {
                    if (nota.getString("motivo").contains("DEVOLUCION")) valor = "X";
                } else if ("motivootro".equals(jrCampo.getName())) {
                    if (nota.getString("motivo").contains("OTRO")) valor = "X";
                } else if ("motivo".equals(jrCampo.getName())) {
                    if (nota.getString("motivo").contains("OTRO")) valor = "---";
                }
            } catch (Exception e) { }
        }
        return valor;
    }
    
}
