/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dscPedido implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet Pedido;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dscPedido(Object mod) {
        this.Pedido = (java.sql.ResultSet)mod;
        try {
            this.Pedido.last();
            total = this.Pedido.getRow()+1;
            this.Pedido.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    @Override
    
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.Pedido.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("numeropedido".equals(jrCampo.getName())) {
                valor = Pedido.getInt("id");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = Pedido.getDate("fecha");
            } else if("anho".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.getAnho(Pedido.getDate("fecha"));
            } else if("usuario".equals(jrCampo.getName())) {
                valor = Pedido.getString("apellidonombre");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = Pedido.getDouble("cantidad");
            } else if("entregado".equals(jrCampo.getName())) {
                valor = Pedido.getString("entregado2");
            } else if("estado".equals(jrCampo.getName())) {
                valor = Pedido.getString("estadopedido");
            } else if("descripcion".equals(jrCampo.getName())) {
                valor = Pedido.getString("descripcion");
            } else if("autorizador".equals(jrCampo.getName())) {
                valor = Pedido.getString("autorizador");
            } else if("idarticulo".equals(jrCampo.getName())) {
                valor = Pedido.getInt("idarticulo");
            } 
        } catch (Exception e) { 
        }
        return valor;
    }
    
}
