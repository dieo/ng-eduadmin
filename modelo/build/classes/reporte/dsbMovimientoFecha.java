/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbMovimientoFecha implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modbMovimientoCuenta mod;
    private int index = -1;

    public dsbMovimientoFecha(Object mod) {
        this.mod = (especifico.modbMovimientoCuenta)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("numeromovimiento".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNumeroMovimiento();
        } else if("comprobante".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getComprobante();
        } else if("fechamovimiento".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFecha();
        } else if("banco".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getBancoCuenta();
        } else if("tipomovimiento".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTipoMovimiento();
        } else if("importe".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMonto();
        } else if("centrocosto".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCentroCosto();
        }
        return valor;
    }
    
}
