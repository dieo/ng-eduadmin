/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsOrdenCreditoDeclaracionAutorizacion implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entMovimiento orden;
    private especifico.modMovimientoGarante garante;
    private int index = -1;

    public dsOrdenCreditoDeclaracionAutorizacion(Object mod, Object mod2) {
        this.orden = (especifico.entMovimiento)mod;
        this.garante = (especifico.modMovimientoGarante)mod2;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("fechasolicitud".equals(jrCampo.getName())) {
            int ano = orden.getFechaSolicitud().getYear()+1900;
            valor = "Asunción, "+orden.getFechaSolicitud().getDate()+" de "+utilitario.utiFecha.getMeses()[orden.getFechaSolicitud().getMonth()].toLowerCase()+" de "+ano;
        } else if("numerosolicitud".equals(jrCampo.getName())) {
            valor = "SOLICITUD DE ORDEN DE CREDITO Nº: "+utilitario.utiNumero.getMascara(orden.getNumeroSolicitud(),0)+".-";
        } else if("solicitante".equals(jrCampo.getName())) {
            valor = utilitario.utiCadena.convertirMayusMinus(orden.getNombreApellido().toLowerCase())+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(orden.getCedula(),0);
        } else if("codeudor1".equals(jrCampo.getName())) {
            if (garante.getRowCount()==1 || garante.getRowCount()==2) {
                int ipos = garante.getEntidad(0).getApellidoNombre().indexOf(",");
                String snombre = garante.getEntidad(0).getApellidoNombre().substring(ipos+2,garante.getEntidad(0).getApellidoNombre().length());
                String sapellido = garante.getEntidad(0).getApellidoNombre().substring(0,ipos);
                valor = utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(0).getCedula(),0);
            }
        } else if("codeudor2".equals(jrCampo.getName())) {
            if (garante.getRowCount()==2) {
                int ipos = garante.getEntidad(1).getApellidoNombre().indexOf(",");
                String snombre = garante.getEntidad(1).getApellidoNombre().substring(ipos+2,garante.getEntidad(1).getApellidoNombre().length());
                String sapellido = garante.getEntidad(1).getApellidoNombre().substring(0,ipos);
                valor = utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(1).getCedula(),0);
            }
        } else if("cantidadcodeudor".equals(jrCampo.getName())) {
            valor = garante.getRowCount();
        }
        return valor;
    }
    
}
