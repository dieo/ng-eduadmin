/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsUsuarioPermisos implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet UsuarioPermiso;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsUsuarioPermisos(Object mod) {
        this.UsuarioPermiso = (java.sql.ResultSet)mod;
        try {
            this.UsuarioPermiso.last();
            total = this.UsuarioPermiso.getRow()+1;
            this.UsuarioPermiso.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    @Override
    
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.UsuarioPermiso.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("apellidonombre".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("apellidonombre");
            } else if("nivel".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("nivelusuario");
            } else if("filial".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("filial");
            } else if("dependencia".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("dependencia");
            } else if("perfil".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("perfil");
            } else if("programa".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("programa");
            } else if("insertar".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("insertar");
            } else if("modificar".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("modificar");
            } else if("eliminar".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("eliminar");
            } else if("visualizar".equals(jrCampo.getName())) {
                valor = this.UsuarioPermiso.getString("visualizar");
            } else if("imprimir".equals(jrCampo.getName())) {
                valor = this.UsuarioPermiso.getString("imprimir");
            } else if("aprobar".equals(jrCampo.getName())) {
                valor = UsuarioPermiso.getString("aprobar");
            } else if("generar".equals(jrCampo.getName())) {
                valor = this.UsuarioPermiso.getString("generar");
            } else if("anular".equals(jrCampo.getName())) {
                valor = this.UsuarioPermiso.getString("anular");
            } else if("rechazar".equals(jrCampo.getName())) {
                valor = this.UsuarioPermiso.getString("rechazar");
            } 
        } catch (Exception e) { 
        }
        return valor;
    }
    
}
