/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPlanillaEnvio implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet envio;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsPlanillaEnvio(Object mod) {
        this.envio = (java.sql.ResultSet)mod;
        try {
            this.envio.last();
            total = this.envio.getRow()+1;
            this.envio.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.envio.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("regional".equals(jrCampo.getName())) {
                valor = envio.getString("regional");;
            } else if("destinatario".equals(jrCampo.getName())) {
                valor = envio.getString("destinatario");;
            } else if("cedula".equals(jrCampo.getName())) {
                valor = envio.getString("cedula");
            } else if("nombre".equals(jrCampo.getName())) {
                valor = envio.getString("nombre");
            } else if("apellido".equals(jrCampo.getName())) {
                valor = envio.getString("apellido");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = envio.getInt("cantidad");
            }
        } catch (Exception e) { 
        javax.swing.JOptionPane.showMessageDialog(null, e);
        }
        return valor;
    }
    
}
