/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAtencionSocioUsuarioFecha implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modAtencionSocio mod;
    private int index = -1;

    public dsAtencionSocioUsuarioFecha(Object mod) {
        this.mod = (especifico.modAtencionSocio)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("usuario".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getUsuarioNombre()+" "+mod.getEntidad(index).getUsuarioApellido();
        } else if("cedula".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCedula();
        } else if("apellidonombre".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNombreApellido();
        } else if("fecha".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaAtencion();
        } else if("hora".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getHoraAtencion();
        } else if("objeto".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getObjetoAtencion();
        } else if("solucion".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getSolucionAtencion();
        } else if("observacion".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getObservacion();
        }
        return valor;
    }
    
}
