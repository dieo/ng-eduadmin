/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPrestamoRecalculoDiferencianew implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entListaSocio socio;
    private especifico.entMovimiento ent;
    private especifico.modDetalleCancelacionCabeceranew modCabecera;
    private int index = -1;

    public dsPrestamoRecalculoDiferencianew(Object mod, Object mod2, Object mod3) {
        this.socio = (especifico.entListaSocio)mod;
        this.ent = (especifico.entMovimiento)mod2;
        this.modCabecera = (especifico.modDetalleCancelacionCabeceranew)mod3;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < modCabecera.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("fechasolicitud".equals(jrCampo.getName())) {
            valor = utilitario.utiFecha.convertirToStringDMA(ent.getFechaSolicitud());
        } else if("numerosolicitud".equals(jrCampo.getName())) {
            valor = utilitario.utiNumero.getMascara(ent.getNumeroSolicitud(),0);
        } else if("socio".equals(jrCampo.getName())) {
            valor = socio.getCedula()+"  "+socio.getNombre()+" "+socio.getApellido();
        } else if("item".equals(jrCampo.getName())) {
            valor = index+1; valor += ")";
        } else if("cuenta".equals(jrCampo.getName())) {
            valor = utilitario.utiCadena.convertirMayusMinus(modCabecera.getEntidad(index).getcCuenta());
            valor = valor + " Nro.Op. "+modCabecera.getEntidad(index).getcNumeroOperacion()+" ("+modCabecera.getEntidad(index).getcResumenCuota()+")";
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = modCabecera.getEntidad(index).getcNumeroOperacion();
        } else if("atraso".equals(jrCampo.getName())) {
            valor = modCabecera.cabecera.getEntidad(index).getcTotalAtraso();
        } else if("mes".equals(jrCampo.getName())) {
            valor = modCabecera.cabecera.getEntidad(index).getcTotalMes();
        } else if("saldo".equals(jrCampo.getName())) {
            valor = modCabecera.cabecera.getEntidad(index).getcTotalSaldo();
        } else if("aplicado".equals(jrCampo.getName())) {
            valor = modCabecera.getEntidad(index).getcTotalAplicado();
        } else if("diferencia".equals(jrCampo.getName())) {
            valor = modCabecera.cabecera.getEntidad(index).getcTotalAtraso()+modCabecera.cabecera.getEntidad(index).getcTotalMes()+modCabecera.cabecera.getEntidad(index).getcTotalSaldo()-modCabecera.getEntidad(index).getcTotalAplicado();
        }
        return valor;
    }
    
}
