/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

import utilitario.utiGeneral;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPlanillaGiraduriaNota implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet nota;
    private int index = 0;
    private int total = 0;

    public dsPlanillaGiraduriaNota(Object mod) {
        this.nota = (java.sql.ResultSet)mod;
        try {
            this.nota.last();
            total = this.nota.getRow()+1;
            this.nota.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.nota.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("fechaemision".equals(jrCampo.getName())) {
                valor = "Asunción, "+nota.getDate("fechahoy").getDate()+" de "+utilitario.utiFecha.getMeses()[nota.getDate("fechahoy").getMonth()].toLowerCase()+" de "+utilitario.utiFecha.getAnho(nota.getDate("fechahoy"));
            } else if("remitente".equals(jrCampo.getName())) {
                valor = nota.getString("tratamiento")+"\n";
                valor += nota.getString("contacto")+"\n";
                valor += nota.getString("cargo")+"\n";
                valor += nota.getString("giraduria")+"\n";
                valor += "Presente";
            } else if("textoprincipal".equals(jrCampo.getName())) {
                double utotalPermanente = this.nota.getDouble("montopermanente");
                double utotalContratado = this.nota.getDouble("montocontratado");
                valor = "De mi mayor consideración:\n";
                valor += "\n";
                valor += "Me dirijo a usted, a los efectos de presentar las planillas correspondientes al mes de "+utilitario.utiFecha.getMeses()[nota.getDate("fechaemision").getMonth()].toLowerCase()+" del "+utilitario.utiFecha.getAnho(nota.getDate("fechaemision"))+ " y a la vez solicitar ";
                valor += "su autorización para el descuento respectivo a través de la Giraduría de "+nota.getString("giraduria")+" a los funcionarios a su cargo, ";
                valor += "asociados a la MUTUAL NACIONAL DE FUNCIONARIOS DEL MSP Y BS.\n";
                valor += "\n";
                valor += "   Permanente: Gs. "+new java.text.DecimalFormat("###,###,###,###").format(utotalPermanente)+".-\n";
                valor += "   Contratado: Gs. "+new java.text.DecimalFormat("###,###,###,###").format(utotalContratado)+".-\n";
                valor += "\n";
                valor += "Agradeciendo su deferencia, aprovecho la oportunidad para saludarle cordialmente.";
            }    
        } catch (Exception e) { }
        return valor;
    }
    
}
