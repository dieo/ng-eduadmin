/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsExtracto implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entSocio socio;
    private especifico.entLugarLaboral labor;
    private especifico.modExtractoPrestamo ope;
    private int index = -1;
    private String rubro = "";

    public dsExtracto(Object mod, Object mod2, Object mod3) {
        this.socio = (especifico.entSocio)mod;
        this.labor = (especifico.entLugarLaboral)mod2;
        this.ope = (especifico.modExtractoPrestamo)mod3;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < ope.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if ("cedula".equals(jrCampo.getName())) {
            valor = socio.getCedula();
        } else if ("numerosocio".equals(jrCampo.getName())) {
            valor = socio.getNumeroSocio();
        } else if ("nombreapellido".equals(jrCampo.getName())) {
            valor = socio.getNombre()+" "+socio.getApellido();
        } else if ("fechaingreso".equals(jrCampo.getName())) {
            valor = socio.getFechaIngreso();
        } else if ("fecha".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getFecha();
        } else if ("antiguedad".equals(jrCampo.getName())) {
            valor = utilitario.utiFecha.obtenerAntiguedad(socio.getFechaIngreso(), new java.util.Date());
        } else if ("estado".equals(jrCampo.getName())) {
            valor = socio.getEstado();
        } else if ("giraduria".equals(jrCampo.getName())) {
            valor = labor.getGiraduria();
        } else if ("rubro".equals(jrCampo.getName())) {
            valor = labor.getRubro();
        } else if ("institucion".equals(jrCampo.getName())) {
            valor = labor.getInstitucion();
        } else if ("regional".equals(jrCampo.getName())) {
            valor = labor.getRegional();
        } else if ("cuenta".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getCuenta();
        } else if ("denominacion".equals(jrCampo.getName())) {
            valor = "- "+utilitario.utiCadena.convertirMayusMinus(ope.getEntidad(index).getDenominacion());
        } else if ("rubrodetalle".equals(jrCampo.getName())) {
            rubro = "";
            if (!ope.getEntidad(index).getRubro().isEmpty()) rubro = ope.getEntidad(index).getRubro();
            valor = rubro;
        } else if ("numerooperacion".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getNumeroOperacion();
        } else if ("fechaemision".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getFechaOperacion();
        } else if ("plazo".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getNumeroCuota()+"/"+ope.getEntidad(index).getPlazo();
        } else if ("diaatraso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getDiaAtraso();
        } else if ("monto".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getMontoTotal();
        } else if ("cobro".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getCobro();
        } else if ("atraso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getAtraso();
        } else if ("actual".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getActual();
        } else if ("saldo".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getSaldo();
        } else if ("tipoinforme".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getTipoInforme();
        }
        return valor;
    }
    
}
