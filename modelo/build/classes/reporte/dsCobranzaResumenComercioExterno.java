/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsCobranzaResumenComercioExterno implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsCobranzaResumenComercioExterno(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("periodo".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(utilitario.utiFecha.getMeses()[listado.getDate("periodo").getMonth()]) + "/" + utilitario.utiFecha.getAnho(listado.getDate("periodo"));
            } else if("cedula".equals(jrCampo.getName())) {
                valor = listado.getString("cedula");
            } else if("nombreapellido".equals(jrCampo.getName())) {
                valor = listado.getString("nombre")+" "+listado.getString("apellido");
            } else if("idcuenta".equals(jrCampo.getName())) {
                valor = listado.getInt("idcuenta");
            } else if("descripcion".equals(jrCampo.getName())) {
                valor = listado.getString("descripcion");
            } else if("montocapital".equals(jrCampo.getName())) {
                valor = listado.getDouble("montocapital");
            } else if("montocobrado".equals(jrCampo.getName())) {
                valor = listado.getDouble("montocobrado");
            } else if("saldocapital".equals(jrCampo.getName())) {
                valor = listado.getDouble("saldocapital");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
