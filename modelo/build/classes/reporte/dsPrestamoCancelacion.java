/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPrestamoCancelacion implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entMovimiento prestamo;
    private especifico.modDetalleCancelacionCabeceranew cabecera;
    private int iidImpuesto;
    private double umontoCancelacion = 0.0;
    private int index = -1;

    public dsPrestamoCancelacion(Object mod, Object mod2, Object mod3) {
        this.prestamo = (especifico.entMovimiento)mod;
        this.cabecera = (especifico.modDetalleCancelacionCabeceranew)mod2;
        this.iidImpuesto = (Integer)mod3;
        for (int i=0; i<cabecera.getRowCount(); i++) {
            if (cabecera.getEntidad(i).getcIdCuenta()!=this.iidImpuesto) {
                this.umontoCancelacion += cabecera.getEntidad(i).getcTotalAtraso()+cabecera.getEntidad(i).getcTotalMes()+cabecera.getEntidad(i).getcTotalSaldo()-cabecera.getEntidad(i).getcTotalExoneracion();
            }
        }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        if (cabecera.getRowCount()>0) {
            return ++index < cabecera.getRowCount();
        }
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("cedula".equals(jrCampo.getName())) {
            valor = utilitario.utiNumero.getMascara(prestamo.getCedula(),0);
        } else if("nombreapellido".equals(jrCampo.getName())) {
            valor = prestamo.getNombreApellido();
        } else if("procedencia".equals(jrCampo.getName())) {
            valor = prestamo.getZona();
        } else if("numerosolicitud".equals(jrCampo.getName())) {
            valor = prestamo.getNumeroSolicitud();
        } else if("fechasolicitud".equals(jrCampo.getName())) {
            valor = prestamo.getFechaSolicitud();
        } else if("montosolicitado".equals(jrCampo.getName())) {
            valor = prestamo.getMontoSolicitud();
        } else if("plazosolicitado".equals(jrCampo.getName())) {
            valor = prestamo.getPlazoSolicitud();
        } else if("tasainteres".equals(jrCampo.getName())) {
            valor = prestamo.getTasaInteres();
        } else if("tipoprestamo".equals(jrCampo.getName())) {
            valor = prestamo.getTipoCredito();
        } else if("destino".equals(jrCampo.getName())) {
            valor = prestamo.getDestino();
        } else if("observacion".equals(jrCampo.getName())) {
            valor = prestamo.getObservacionSolicitud();
        } else if("desembolso".equals(jrCampo.getName())) {
            valor = prestamo.getMontoSaldo();
        } else if("montointeres".equals(jrCampo.getName())) {
            valor = prestamo.getMontoSolicitud() * prestamo.getTasaInteres() * prestamo.getPlazoSolicitud() / 100;
        } else if("retencion".equals(jrCampo.getName())) {
            valor = prestamo.getRetencion();
        } else if("montoimpuesto".equals(jrCampo.getName())) {
            valor = prestamo.getMontoImpuesto();
        } else if("montocancelacion".equals(jrCampo.getName())) {
            valor = umontoCancelacion;
        }
        if (cabecera.getRowCount()>0) {
            if("item".equals(jrCampo.getName())) {
                valor = index+1; valor += ")";
            } else if("cuenta".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(cabecera.getEntidad(index).getcCuenta());
                valor = valor + " Nro.Op. "+cabecera.getEntidad(index).getcNumeroOperacion()+" ("+cabecera.getEntidad(index).getcResumenCuota()+")";
            } else if("atraso".equals(jrCampo.getName())) {
                valor = cabecera.getEntidad(index).getcTotalAtraso();
            } else if("mes".equals(jrCampo.getName())) {
                valor = cabecera.getEntidad(index).getcTotalMes();
            } else if("saldo".equals(jrCampo.getName())) {
                valor = cabecera.getEntidad(index).getcTotalSaldo();
            } else if("exoneracion".equals(jrCampo.getName())) {
                valor = cabecera.getEntidad(index).getcTotalExoneracion();
            }
        }
        return valor;
    }
    
}
