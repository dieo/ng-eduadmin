/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dscOrdenCompra implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet OrdenCompra;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dscOrdenCompra(Object mod) {
        this.OrdenCompra = (java.sql.ResultSet)mod;
        try {
            this.OrdenCompra.last();
            total = this.OrdenCompra.getRow()+1;
            this.OrdenCompra.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    @Override
    
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.OrdenCompra.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("numeroordencompra".equals(jrCampo.getName())) {
                valor = OrdenCompra.getInt("id");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = OrdenCompra.getDate("fecha");
            } else if("anho".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.getAnho(OrdenCompra.getDate("fecha"));
            } else if("tipopedido".equals(jrCampo.getName())) {
                valor = OrdenCompra.getString("tipopedido");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = OrdenCompra.getString("apellidonombre");
            } else if("ruc".equals(jrCampo.getName())) {
                valor = OrdenCompra.getString("ruc");
            } else if("proveedor".equals(jrCampo.getName())) {
                valor = OrdenCompra.getString("proveedor");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = OrdenCompra.getDouble("cantidad");
            } else if("costo".equals(jrCampo.getName())) {
                valor = OrdenCompra.getDouble("costo");
            } else if("total".equals(jrCampo.getName())) {
                valor = OrdenCompra.getDouble("total");
            } else if("cambio".equals(jrCampo.getName())) {
                valor = OrdenCompra.getDouble("cambio");
            } else if("descripcion".equals(jrCampo.getName())) {
                valor = OrdenCompra.getString("descripcion");
            } else if("condicion".equals(jrCampo.getName())) {
                valor = OrdenCompra.getString("condicion");
            } else if("filial".equals(jrCampo.getName())) {
                valor = OrdenCompra.getString("filial");
            } else if("moneda".equals(jrCampo.getName())) {
                valor = OrdenCompra.getString("moneda");
            } else if("idarticulo".equals(jrCampo.getName())) {
                valor = OrdenCompra.getInt("idarticulo");
            } 
        } catch (Exception e) { 
        }
        return valor;
    }
    
}
