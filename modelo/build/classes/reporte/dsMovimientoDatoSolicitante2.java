/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsMovimientoDatoSolicitante2 implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entSocio socio;
    private especifico.entMovimiento movimiento;
    private especifico.modLugarLaboral labor;
    private especifico.modReferenciaComercial comercial;
    private especifico.modReferenciaPersonal personal;
    private especifico.modBeneficiario beneficiario;
    private int indexLabor = 0;
    private int indexComercial = 0;
    private int indexPersonal = 0;
    private int indexBeneficiario = 0;
    private int total = 0;
    private int index = -1;

    public dsMovimientoDatoSolicitante2(Object mod, Object mod2, Object mod3, Object mod4, Object mod5, Object mod6) {
        this.socio = (especifico.entSocio)mod;
        this.movimiento = (especifico.entMovimiento)mod2;
        this.labor = (especifico.modLugarLaboral)mod3;
        this.comercial = (especifico.modReferenciaComercial)mod4;
        this.personal = (especifico.modReferenciaPersonal)mod5;
        this.beneficiario = (especifico.modBeneficiario)mod6;
        this.total = 1+this.labor.getRowCount()+this.comercial.getRowCount()+this.personal.getRowCount()+this.beneficiario.getRowCount();
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if (index==0 || index==total-1) {
            if("fechasolicitud".equals(jrCampo.getName())) {
                valor = "Asunción, "+movimiento.getFechaSolicitud().getDate()+" de "+utilitario.utiFecha.getMeses()[movimiento.getFechaSolicitud().getMonth()].toLowerCase()+" de "+utilitario.utiFecha.getAnho(movimiento.getFechaSolicitud());
            } else if("numerosolicitud".equals(jrCampo.getName())) {
                valor = "SOLICITUD Nº: "+utilitario.utiNumero.getMascara(movimiento.getNumeroSolicitud(),0)+".-";
            } else if("solicitante".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(movimiento.getNombreApellido().toLowerCase())+"\nC.I.C. Nº "+utilitario.utiNumero.getMascara(movimiento.getCedula(),0);
            } else if ("cedula".equals(jrCampo.getName())) {
                valor = socio.getCedula();
            } else if ("nombreapellido".equals(jrCampo.getName())) {
                valor = socio.getNombre() + " " + socio.getApellido();
            } else if ("sexo".equals(jrCampo.getName())) {
                valor = socio.getSexo();
            } else if ("direccion".equals(jrCampo.getName())) {
                valor = socio.getDireccion();
            } else if ("numerocasa".equals(jrCampo.getName())) {
                valor = socio.getNumeroCasa();
            } else if ("barrio".equals(jrCampo.getName())) {
                valor = socio.getBarrio();
            } else if ("ciudadresidencia".equals(jrCampo.getName())) {
                valor = socio.getCiudadResidencia();
            } else if ("telefonocelular".equals(jrCampo.getName())) {
                valor = socio.getTelefonoCelularPrincipal()+"/"+socio.getTelefonoCelular();
            } else if ("telefonolineabaja".equals(jrCampo.getName())) {
                valor = socio.getTelefonoLineaBaja();
            } else if ("fechanacimiento".equals(jrCampo.getName())) {
                valor = socio.getFechaNacimiento();
            } else if ("lugarnacimiento".equals(jrCampo.getName())) {
                valor = socio.getLugarNacimiento();
            } else if ("estadocivil".equals(jrCampo.getName())) {
                valor = socio.getEstadoCivil();
            } else if ("profesion".equals(jrCampo.getName())) {
                valor = socio.getProfesion();
            } else if ("email".equals(jrCampo.getName())) {
                valor = socio.getEmail();
            } else if ("tipocasa".equals(jrCampo.getName())) {
                valor = socio.getTipoCasa();
            }
        }
        if (index>0 && index<1+this.labor.getRowCount()) {
            indexLabor = index-1;
            if ("indexLabor".equals(jrCampo.getName())) {
                valor = indexLabor;
            } else if ("institucion".equals(jrCampo.getName())) {
                valor = labor.getEntidad(indexLabor).getInstitucion();
            } else if ("regional".equals(jrCampo.getName())) {
                valor = labor.getEntidad(indexLabor).getRegional();
            } else if ("rubro".equals(jrCampo.getName())) {
                valor = labor.getEntidad(indexLabor).getRubro();
            } else if ("giraduria".equals(jrCampo.getName())) {
                valor = labor.getEntidad(indexLabor).getGiraduria();
            } else if ("sueldo".equals(jrCampo.getName())) {
                valor = labor.getEntidad(indexLabor).getSueldo();
            } else if ("activo".equals(jrCampo.getName())) {
                if (labor.getEntidad(indexLabor).getActivo()) valor = "√";
            }
        }
        if (index>=1+this.labor.getRowCount() && index<1+this.labor.getRowCount()+this.comercial.getRowCount()) {
            indexComercial = index-1-this.labor.getRowCount();
            if ("indexComercial".equals(jrCampo.getName())) {
                valor = indexComercial;
            } else if ("comercio".equals(jrCampo.getName())) {
                valor = comercial.getEntidad(indexComercial).getComercio();
            } else if ("direccioncomercio".equals(jrCampo.getName())) {
                valor = comercial.getEntidad(indexComercial).getDireccion();
            } else if ("telefonocomercio".equals(jrCampo.getName())) {
                valor = comercial.getEntidad(indexComercial).getTelefono();
            }
        }
        if (index>=1+this.labor.getRowCount()+this.comercial.getRowCount() && index<1+this.labor.getRowCount()+this.comercial.getRowCount()+this.personal.getRowCount()) {
            indexPersonal = index-1-this.labor.getRowCount()-this.comercial.getRowCount();
            if ("indexPersonal".equals(jrCampo.getName())) {
                valor = indexPersonal;
            } else if ("cedulapersona".equals(jrCampo.getName())) {
                valor = personal.getEntidad(indexPersonal).getCedula();
            } else if ("nombreapellidopersona".equals(jrCampo.getName())) {
                valor = personal.getEntidad(indexPersonal).getNombre()+" "+personal.getEntidad(indexPersonal).getApellido();
            } else if ("telefonopersona".equals(jrCampo.getName())) {
                valor = personal.getEntidad(indexPersonal).getTelefono();
            } else if ("parentescopersona".equals(jrCampo.getName())) {
                valor = personal.getEntidad(indexPersonal).getParentesco();
            }
        }
        if (index>=1+this.labor.getRowCount()+this.comercial.getRowCount()+this.personal.getRowCount() && index<total) {
            indexBeneficiario = index-1-this.labor.getRowCount()-this.comercial.getRowCount()-this.personal.getRowCount();
            if ("indexBeneficiario".equals(jrCampo.getName())) {
                valor = indexBeneficiario;
            } else if ("cedulabeneficiario".equals(jrCampo.getName())) {
                valor = beneficiario.getEntidad(indexBeneficiario).getCedula();
            } else if ("nombreapellidobeneficiario".equals(jrCampo.getName())) {
                valor = beneficiario.getEntidad(indexBeneficiario).getNombre()+" "+beneficiario.getEntidad(indexBeneficiario).getApellido();
            } else if ("parentescobeneficiario".equals(jrCampo.getName())) {
                valor = beneficiario.getEntidad(indexBeneficiario).getParentesco();
            }
        }
        return valor;
    }
    
}
