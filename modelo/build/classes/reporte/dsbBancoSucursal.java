/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbBancoSucursal implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet Banco;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;
    
    public dsbBancoSucursal(Object mod) {
        this.Banco = (java.sql.ResultSet)mod;
        try {
            this.Banco.last();
            total = this.Banco.getRow()+1;
            this.Banco.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.Banco.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("ruc".equals(jrCampo.getName())) {
                valor = Banco.getString("ruc");
            } else if("descripcion".equals(jrCampo.getName())) {
                valor = Banco.getString("descripcion");
            } else if("contacto".equals(jrCampo.getName())) {
                valor = Banco.getString("contacto");
            } else if("email".equals(jrCampo.getName())) {
                valor = Banco.getString("email");
            } else if("web".equals(jrCampo.getName())) {
                valor = Banco.getString("web");
            } else if("sucursal".equals(jrCampo.getName())) {
                valor = Banco.getString("sucursal");
            } else if("ciudad".equals(jrCampo.getName())) {
                valor = Banco.getString("ciudad");
            } else if("direccion".equals(jrCampo.getName())) {
                valor = Banco.getString("direccion");
            } else if("telefono".equals(jrCampo.getName())) {
                valor = Banco.getString("telefono");
            }
        } catch (Exception e) { 
        }return valor;
    }
    
}
