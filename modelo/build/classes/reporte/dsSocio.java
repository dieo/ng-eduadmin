/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsSocio implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet socio;
    private int index = 0;
    private int total = 0;

    public dsSocio(Object mod) {
        this.socio = (java.sql.ResultSet)mod;
        try {
            this.socio.last();
            total = this.socio.getRow()+1;
            this.socio.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { socio.next();
        } catch (Exception e) { }
        return ++index < this.total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("numero".equals(jrCampo.getName())) {
                valor = index;
            } else if("cedula".equals(jrCampo.getName())) {
                valor = socio.getString("cedula");
            } else if("nombre".equals(jrCampo.getName())) {
                valor = socio.getString("nombre");
            } else if("apellido".equals(jrCampo.getName())) {
                valor = socio.getString("apellido");
            } else if("numerosocio".equals(jrCampo.getName())) {
                valor = socio.getInt("numerosocio");
            } else if("fechaingreso".equals(jrCampo.getName())) {
                java.util.Date fecha = socio.getDate("fechaingreso");
                valor = fecha;
            } else if("antiguedad".equals(jrCampo.getName())) {
                java.util.Date fecha = socio.getDate("fechaingreso");
                int[] antiguedad = utilitario.utiFecha.obtenerDiferencia(fecha, new java.util.Date());
                valor = antiguedad[0] + "a." + "  " + antiguedad[1] + "m.";
            } else if("estado".equals(jrCampo.getName())) {
                valor = socio.getString("estadosocio");
            }
        } catch (Exception e) { }
        
        return valor;
    }
    
}
