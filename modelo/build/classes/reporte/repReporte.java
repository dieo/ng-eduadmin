
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

import java.awt.Dialog;

/**
 *
 * @author Ing. Edison Martinez
 */
public class repReporte {

    private String smensaje;
    private final String srutaReporte;
    private java.awt.print.PrinterJob job = java.awt.print.PrinterJob.getPrinterJob();
    
    public repReporte(String srutaReporte) {
        this.smensaje = "";
        this.srutaReporte = srutaReporte;
        //this.srutaReporte = "C:\\MUTUAL\\SISTEMA\\reporte\\";
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean imprimir(short rep, Object mod, Object mod2, Object mod3, Object mod4, String stitulo, String ssubtitulo, String susuario) {
        try {
            String sruta = this.srutaReporte+generico.reporte.getNombre(rep)+".jasper";
            net.sf.jasperreports.engine.JasperPrint ds = null;
            java.util.HashMap parametro = new java.util.HashMap();
            parametro.put("titulo", stitulo.toUpperCase());
            parametro.put("subtitulo", ssubtitulo.toUpperCase());
            parametro.put("usuario", susuario);
            parametro.put("fecha", new java.util.Date());
            parametro.put("rutalogo", this.srutaReporte);
            if (rep==generico.reporte.repAutorizador)              ds = new reporte.dsAutorizador(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repBarrio)                   ds = new reporte.dsBarrio(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCargo)                    ds = new reporte.dsCargo(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCiudad)                   ds = new reporte.dsCiudad(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repDepartamento)             ds = new reporte.dsDepartamento(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repDescuento)                ds = new reporte.dsDescuento(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repEntidad)                  ds = new reporte.dsEntidad(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repEstadoCivil)              ds = new reporte.dsEstadoCivil(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFuncionario)              ds = new reporte.dsFuncionario(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repGiraduria)                ds = new reporte.dsGiraduria(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repInstitucion)              ds = new reporte.dsInstitucion(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repMoneda)                   ds = new reporte.dsMoneda(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPais)                     ds = new reporte.dsPais(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repParentesco)               ds = new reporte.dsParentesco(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repProfesion)                ds = new reporte.dsProfesion(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPromotor)                 ds = new reporte.dsPromotor(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repRamo)                     ds = new reporte.dsRamo(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repRegional)                 ds = new reporte.dsRegional(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repRubro)                    ds = new reporte.dsRubro(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAnalisisDisponibilidad)   ds = new reporte.dsAnalisisDisponibilidad(mod, mod2, mod3, mod4).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAhorroProgramadoPlan)     ds = new reporte.dsAhorroProgramadoPlan(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAhorroProgramadoLiquidacion) ds = new reporte.dsAhorroProgramadoLiquidacion(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAhorroProgramadoAnulacion) ds = new reporte.dsAhorroProgramadoAnulacion(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPagare)                   ds = new reporte.dsPagare(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPagare2)                  ds = new reporte.dsPagare2(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repOrdenCredito)             ds = new reporte.dsOrdenCredito(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repLiquidacion)              ds = new reporte.dsLiquidacion(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repLibroEgreso)              ds = new reporte.dscLibroEgreso(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAhorroProgramadoRegionEstado) ds = new reporte.dsAhorroProgramadoRegionEstado(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAhorroProgramadoRegionEstadov2) ds = new reporte.dsAhorroProgramadoRegionEstado(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAhorroProgramadoRegionEstadoProm) ds = new reporte.dsAhorroProgramadoRegionEstadoProm(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPrestamoSolicitud)        ds = new reporte.dsPrestamoSolicitud(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repMovimientoDeclaracionAutorizacion) ds = new reporte.dsMovimientoDeclaracionAutorizacion(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repLlamadaSocio)             ds = new reporte.dsLlamadaSocio(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repLlamadaUsuarioFecha)      ds = new reporte.dsLlamadaUsuarioFecha(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbBancoSucursal)           ds = new reporte.dsbBancoSucursal(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbCuentaBanco)             ds = new reporte.dsbCuentaBanco(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbPlanCuenta)              ds = new reporte.dsbPlanCuenta(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbReceptorCheque)          ds = new reporte.dsbReceptorCheque(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbCentroCosto)             ds = new reporte.dsbCentroCosto(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbTipoMovimiento)          ds = new reporte.dsbTipoMovimiento(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbDepositoFecha)           ds = new reporte.dsbDepositoFecha(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbLibroBanco)              ds = new reporte.dsbLibroBanco(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbMovimientoFecha)         ds = new reporte.dsbMovimientoFecha(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbOrdenPago)               ds = new reporte.dsbOrdenPago(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbOrdenPagoFecha)          ds = new reporte.dsbOrdenPagoFecha(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbChequeEmitido)           ds = new reporte.dsbChequeEmitido(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbChequeEntrega)           ds = new reporte.dsbChequeEntrega(mod).cargarInforme(sruta, parametro);
            //if (rep==generico.reporte.repbRemision)                ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbRemision)                ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbRemisionPromotor)        ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbRemisionRegionales)      ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbRemisionPagare)          ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbRemisionPagare2)         ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbRemisionRRHH)            ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbRemisionLista)           ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbRemisionResumen)         ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repbRemisionResumen2)        ds = new reporte.dsbRemision(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAtencionSocio)            ds = new reporte.dsAtencionSocio(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAtencionSocioUsuarioFecha) ds = new reporte.dsAtencionSocioUsuarioFecha(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repSolidaridad)              ds = new reporte.dsSolidaridad(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repSolidaridadIndividual)    ds = new reporte.dsSolidaridadIndividual(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repSolidaridadSolicitud)     ds = new reporte.dsSolidaridadSolicitud(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repSocio)                    ds = new reporte.dsSocio(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repSocioRegionalRubro)       ds = new reporte.dsSocioRegionalRubro(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repOperacionFijaRegionEstado) ds = new reporte.dsOperacionFijaRegionEstado(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repOperacionFijaRegionEstadov2) ds = new reporte.dsOperacionFijaRegionEstado(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repOperacionFijaLiquidacion) ds = new reporte.dsOperacionFijaLiquidacion(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPrestamoRegionEstado)     ds = new reporte.dsPrestamoRegionEstado(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPrestamoEstado)           ds = new reporte.dsPrestamoEstado(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repOrdenCreditoRegionEstado) ds = new reporte.dsOrdenCreditoRegionEstado(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repOrdenCreditoSolicitud)    ds = new reporte.dsOrdenCreditoSolicitud(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFondoJuridicoSepelioRegionEstado) ds = new reporte.dsFondoJuridicoSepelioRegionEstado(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFondoJuridicoSepelioLiquidacion) ds = new reporte.dsFondoJuridicoSepelioLiquidacion(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPromotorOperacion)        ds = new reporte.dsPromotorOperacion(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repSepelio)                  ds = new reporte.dsSepelio(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repSepelioIndividual)        ds = new reporte.dsSepelioIndividual(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCasoJuridico)             ds = new reporte.dsCasoJuridico(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCasoJuridicoIndividual)   ds = new reporte.dsCasoJuridicoIndividual(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repSepelioIndividualBeneficiario) ds = new reporte.dsSepelioIndividualBeneficiario(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repOperacionFijaDesautorizacion) ds = new reporte.dsOperacionFijaDesautorizacion(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFactura)                  ds = new reporte.dsFactura(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repMovimientoAnulacion)      ds = new reporte.dsMovimientoAnulacion(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repMovimientoDatoSolicitante) ds = new reporte.dsMovimientoDatoSolicitante(mod, mod2, mod3, mod4).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repDatoSocio)                ds = new reporte.dsDatoSocio(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repDatoSociov2)              ds = new reporte.dsDatoSociov2(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repIngresoPlanilla)          ds = new reporte.dsIngresoPlanilla(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repIngresoCuenta)            ds = new reporte.dsIngresoCuenta(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repIngresoIndividual)        ds = new reporte.dsIngresoIndividual(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repLlamadaSocioRegional)     ds = new reporte.dsLlamadaSocioRegional(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repLlamadaUsuarioFechaRegional) ds = new reporte.dsLlamadaUsuarioFechaRegional(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPrestamoRecalculoDiferencia) ds = new reporte.dsPrestamoRecalculoDiferencia(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPrestamoRecalculoDiferencianew) ds = new reporte.dsPrestamoRecalculoDiferencianew(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtracto)                 ds = new reporte.dsExtracto(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtractov2)               ds = new reporte.dsExtracto(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtractov3)               ds = new reporte.dsExtractoRegional(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtractoCerradoCobrado)   ds = new reporte.dsExtractoCerradoCobrado(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtractoCerradoCobradoFuncMSP) ds = new reporte.dsExtractoCerradoCobradoFuncMSP(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtractoFuncionarioMSP)   ds = new reporte.dsExtractoFuncionarioMSP(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtractoFuncionarioMSPv2) ds = new reporte.dsExtractoFuncionarioMSPv2(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPrestamoCancelacion)      ds = new reporte.dsPrestamoCancelacion(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFactura3)                 ds = new reporte.dsFactura3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFactura3ver3cre)          ds = new reporte.dsFactura3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFactura3ver4cre)          ds = new reporte.dsFactura3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFactura3ver5cre)          ds = new reporte.dsFactura3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFactura3ver6cre)          ds = new reporte.dsFactura3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repFactura3ver7cre)          ds = new reporte.dsFactura3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repRecibo3)                  ds = new reporte.dsRecibo3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repNotaCredito3)             ds = new reporte.dsNotaCredito3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repNotaCredito3ver2cre)      ds = new reporte.dsNotaCredito3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repNotaCredito3ver3cre)      ds = new reporte.dsNotaCredito3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repNotaCredito3ver4cre)      ds = new reporte.dsNotaCredito3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repNotaCredito3ver5cre)      ds = new reporte.dsNotaCredito3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repNotaCredito3ver6cre)      ds = new reporte.dsNotaCredito3(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repControlLegajo)            ds = new reporte.dsControlLegajoTipo(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repControlLegajoTipo)        ds = new reporte.dsControlLegajoTipo(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repMovimientoCajaChicaPlanilla) ds = new reporte.dsMovimientoCajaChicaPlanilla(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repMovimientoCajaChicaConcepto) ds = new reporte.dsMovimientoCajaChicaConcepto(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repControlArchivo)           ds = new reporte.dsControlArchivo(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPlanillaMovimiento)       ds = new reporte.dsPlanillaMovimiento(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPlanillaEnvio)            ds = new reporte.dsPlanillaEnvio(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAuditoria)                ds = new reporte.dsAuditoria(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repHistorico)                ds = new reporte.dsHistorico(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtractoCerradoCobradov2) ds = new reporte.dsExtractoCerradoCobradov2(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repMorosidad)                ds = new reporte.dsMorosidad(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repListadoBoletaOrdenCredito) ds = new reporte.dsListadoBoletaOrdenCredito(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCobranzaResumenGiraduria) ds = new reporte.dsListadoCobranzaResumenGiraduria(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCobranzaResumenComercioExterno) ds = new reporte.dsListadoCobranzaResumenComercioExterno(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repInformeGiraduria)         ds = new reporte.dsInformeGiraduria(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repListadoFactura)           ds = new reporte.dsListadoFactura(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repListadoNotaCredito)       ds = new reporte.dsListadoNotaCredito(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repListadoComercios)         ds = new reporte.dsListadoComercios(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repListadoJubilados)         ds = new reporte.dsListadoJubilados(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCambiosdeEstadoporFecha)  ds = new reporte.dsCambiosdeEstadoporFecha(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repPlanillaGiraduriaNota)    ds = new reporte.dsPlanillaGiraduriaNota(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repInformePrestamoEmitido)   ds = new reporte.dsInformePrestamoEmitido(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repInformePrestamoRegion)    ds = new reporte.dsInformePrestamoRegion(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repIngresoIndividualporSocio) ds = new reporte.dsIngresoIndividualporSocio(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repContratoPrestamo)         ds = new reporte.dsContratoPrestamo(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repListadoPrestamoIndividual) ds = new reporte.dsListadoPrestamoIndividual(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repDetalleOperacion)         ds = new reporte.dsDetalleOperacion(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repIngresoSocioCuenta)       ds = new reporte.dsIngresoSocioCuenta(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtractoCerradoCobradov3) ds = new reporte.dsExtractoCerradoCobradov3(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCobranzaResumenCuenta)    ds = new reporte.dsCobranzaResumenCuenta(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCobranzaCuenta)           ds = new reporte.dsCobranzaCuenta(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repInformePrestamoporSocio)  ds = new reporte.dsInformePrestamoporSocio(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repInformePrestamoCancelacion) ds = new reporte.dsInformePrestamoCancelacion(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repInformePrestamoEmitido2)  ds = new reporte.dsInformePrestamoEmitido(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repDetalleIngreso)           ds = new reporte.dsDetalleIngreso(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCobranzaSocio)            ds = new reporte.dsCobranzaSocio(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCDAPlan)                  ds = new reporte.dsCDAPlan(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCDACertificado)           ds = new reporte.dsCDACertificado(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCDALista)                 ds = new reporte.dsCDALista(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCDAListaTitular)          ds = new reporte.dsCDAListaTitular(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCDARetiroMensual)         ds = new reporte.dsCDARetiroMensual(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCDARetiroCapital)         ds = new reporte.dsCDARetiroCapital(mod, mod2).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repInformeGiraduriaHacienda) ds = new reporte.dsInformeGiraduria(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repExtractov5)               ds = new reporte.dsExtractov2(mod, mod2, mod3).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCobranzaResumenComercioExternoCuenta) ds = new reporte.dsCobranzaResumenComercioExterno(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCobranzaListadoSocioComercioExterno) ds = new reporte.dsCobranzaResumenComercioExterno(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCobranzaResumenComercioExterno2) ds = new reporte.dsCobranzaResumenComercioExterno(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repCobranzaSaldoCuenta) ds = new reporte.dsCobranzaSaldoCuenta(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repListadoPrestamoPorAutorizador) ds = new reporte.dsInformePrestamoPorAutorizador(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repUsuarioPermisos) ds = new reporte.dsUsuarioPermisos(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcArticulo) ds = new reporte.dscArticulo(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcCategoria) ds = new reporte.dscCategoria(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcPresupuesto) ds = new reporte.dscPresupuesto(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcOrdenCompra) ds = new reporte.dscOrdenCompra(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcPedido) ds = new reporte.dscPedido(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcInventario) ds = new reporte.dscInventario(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcTransferencias) ds = new reporte.dscTransferencias(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcLibroEgreso) ds = new reporte.dscLibroEgreso(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcCuentasApagar) ds = new reporte.dscLibroEgreso(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcHistoricoCompras) ds = new reporte.dscHistoricoCompras(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcDevolucion) ds = new reporte.dscDevolucion(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repcHistoricoPedidos) ds = new reporte.dscHistoricoPedidos(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAsientoBorrador)    ds = new reporte.dsAsientoBorrador(mod).cargarInforme(sruta, parametro);
            if (rep==generico.reporte.repAutorizacionDescuento)    ds = new reporte.dsAutorizacionDescuento(mod).cargarInforme(sruta, parametro);
            
            net.sf.jasperreports.view.JasperViewer venVisor = new net.sf.jasperreports.view.JasperViewer(ds, false);
            venVisor.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE); // hace que la vista previa quede por encima de todas las ventanas
            venVisor.setTitle(stitulo);
            venVisor.setExtendedState(javax.swing.JFrame.MAXIMIZED_BOTH);
            venVisor.setVisible(true);
        } catch(Exception e) {
            this.smensaje = "Error al generar reporte: " + e.toString();
            return false;
        }
        return true;
    }
    
    public boolean imprimir(short rep, Object mod, Object mod2, Object mod3, Object mod4, Object mod5, Object mod6, String stitulo, String ssubtitulo, String susuario) {
        try {
            String sruta = this.srutaReporte+generico.reporte.getNombre(rep)+".jasper";
            net.sf.jasperreports.engine.JasperPrint ds = null;
            java.util.HashMap parametro = new java.util.HashMap();
            parametro.put("titulo", stitulo.toUpperCase());
            parametro.put("subtitulo", ssubtitulo.toUpperCase());
            parametro.put("usuario", susuario);
            parametro.put("fecha", new java.util.Date());
            parametro.put("rutalogo", this.srutaReporte);
            if (rep==generico.reporte.repMovimientoDatoSolicitante2) ds = new reporte.dsMovimientoDatoSolicitante2(mod, mod2, mod3, mod4, mod5, mod6).cargarInforme(sruta, parametro);
            net.sf.jasperreports.view.JasperViewer venVisor = new net.sf.jasperreports.view.JasperViewer(ds, false);
            venVisor.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE); // hace que la vista previa quede por encima de todas las ventanas
            venVisor.setTitle(stitulo);
            venVisor.setExtendedState(javax.swing.JFrame.MAXIMIZED_BOTH);
            venVisor.setVisible(true);
        } catch(Exception e) {
            this.smensaje = "Error al generar reporte: " + e.toString();
            return false;
        }
        return true;
    }
    
    public boolean imprimirDirecto(short rep, Object mod, Object mod2, Object mod3, Object mod4, String stitulo, String ssubtitulo, String susuario) {
        try {
            net.sf.jasperreports.engine.JRDataSource ds = null;
            java.util.HashMap parametro = new java.util.HashMap();
            parametro.put("titulo", stitulo.toUpperCase());
            parametro.put("subtitulo", ssubtitulo.toUpperCase());
            parametro.put("usuario", susuario);
            parametro.put("fecha", new java.util.Date());
            parametro.put("rutalogo", this.srutaReporte);
            if (rep==generico.reporte.repOrdenCredito)     ds = new reporte.dsOrdenCredito(mod, mod2);
            if (rep==generico.reporte.repFactura)          ds = new reporte.dsFactura(mod, mod2);
            if (rep==generico.reporte.repFactura3)         ds = new reporte.dsFactura3(mod);
            if (rep==generico.reporte.repFactura3ver2tes)  ds = new reporte.dsFactura3(mod);
            if (rep==generico.reporte.repRecibo)           ds = new reporte.dsRecibo(mod, mod2);
            if (rep==generico.reporte.repRecibo3)          ds = new reporte.dsRecibo3(mod);
            if (rep==generico.reporte.repRecibo3ver2tes)   ds = new reporte.dsRecibo3(mod);
            if (rep==generico.reporte.repRecibo3ver3tes)   ds = new reporte.dsRecibo3(mod);
            if (rep==generico.reporte.repRecibo3ver4tes)   ds = new reporte.dsRecibo3(mod);
            if (rep==generico.reporte.repRecibo3ver5tes)   ds = new reporte.dsRecibo3(mod);
            if (rep==generico.reporte.repNotaCredito3)     ds = new reporte.dsNotaCredito3(mod);
            if (rep==generico.reporte.repNotaCredito3ver5cre) ds = new reporte.dsNotaCredito3(mod);
            //if (rep==generico.reporte.repbChequeVision)    ds = new reporte.dsbChequeEmitido(mod, mod2);
            if (rep==generico.reporte.repFactura3ver3tes)  ds = new reporte.dsFactura3(mod);
            if (rep==generico.reporte.repFactura3ver4tes)  ds = new reporte.dsFactura3(mod);
            if (rep==generico.reporte.repFactura3ver5tes)  ds = new reporte.dsFactura3(mod);
            if (rep==generico.reporte.repbOrdenPago)       ds = new reporte.dsbOrdenPago(mod);
            net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(this.srutaReporte+generico.reporte.getNombre(rep)+".jasper", parametro, ds);
            this.establecerValores(informe);
        } catch(Exception e) {
            this.smensaje = "Error al generar reporte: " + e.toString();
            return false;
        }
        return true;
    }
    
    private void establecerValores(net.sf.jasperreports.engine.JasperPrint informe) {
        try {
            javax.print.attribute.PrintRequestAttributeSet printRequestAttributeSet = new javax.print.attribute.HashPrintRequestAttributeSet();
            javax.print.attribute.standard.MediaSizeName mediaSizeName = javax.print.attribute.standard.MediaSize.findMedia(4,4,javax.print.attribute.standard.MediaPrintableArea.INCH);
            //printRequestAttributeSet.add(mediaSizeName);
            printRequestAttributeSet.add(new javax.print.attribute.standard.Copies(job.getCopies())); // this.icantidad));
            net.sf.jasperreports.engine.export.JRPrintServiceExporter exporter;
            exporter = new net.sf.jasperreports.engine.export.JRPrintServiceExporter();
            exporter.setParameter(net.sf.jasperreports.engine.JRExporterParameter.JASPER_PRINT, informe);
            exporter.setParameter(net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter.PRINT_SERVICE, job.getPrintService());
            exporter.setParameter(net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, job.getPrintService().getAttributes());
            exporter.setParameter(net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter.PRINT_REQUEST_ATTRIBUTE_SET, printRequestAttributeSet);
            exporter.setParameter(net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
            exporter.setParameter(net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
            exporter.exportReport();
        } catch (Exception e) { }
    }

    // Valores para la impresora por defecto:
    // Boleta de Orden de Compra BOC
    // Factura                   FAC
    // Recibo                    REC
    // Nota de Credito           NCR
    public boolean imprimir(String impresora, int cantidad) {
        try {
            javax.print.PrintService[] servicios = javax.print.PrintServiceLookup.lookupPrintServices(null, null);
            javax.print.PrintService impresoraPorDefecto = javax.print.PrintServiceLookup.lookupDefaultPrintService();
            for (int i=0; i<servicios.length; i++){
                if (servicios[i].getName().toUpperCase().contains(impresora.toUpperCase()) && !impresora.isEmpty()) {
                    impresoraPorDefecto = servicios[i];
                }
            }
            job.setPrintService(impresoraPorDefecto);
            job.setCopies(cantidad);
            return job.printDialog();
        } catch (Exception e) { 
            return false;
        }
    }
    
}