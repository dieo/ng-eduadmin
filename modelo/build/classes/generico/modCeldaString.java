/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modCeldaString extends javax.swing.DefaultCellEditor implements javax.swing.table.TableCellRenderer {

    
    java.awt.Color colorParSeleccionado = new java.awt.Color(0,102,153);
    java.awt.Color colorImparSeleccionado = new java.awt.Color(0,102,153);
    //java.awt.Color colorImparSeleccionado = new java.awt.Color(153,204,255);
    java.awt.Color colorParNoSeleccionado = new java.awt.Color(255,255,255);
    java.awt.Color colorImparNoSeleccionado = new java.awt.Color(240,240,240);
    java.awt.Font fuenteSeleccionado = new java.awt.Font("Arial", 1, 10);
    java.awt.Font fuenteNoSeleccionado = new java.awt.Font("Arial", 1, 10);
    java.awt.Color colorFuenteSeleccionado = new java.awt.Color(255,255,255);
    java.awt.Color colorFuenteNoSeleccionado = new java.awt.Color(0,0,0);
    boolean si = false;
    
    public modCeldaString() {
        super(new javax.swing.JFormattedTextField());
    }
    
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        javax.swing.JFormattedTextField campo = new javax.swing.JFormattedTextField();  // Creamos text field
        campo.setFont(new java.awt.Font("Arial", 1, 10));
        campo.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        if (isSelected) { // Ponemos un color distinto para la etiqueta según si la celda está o no seleccionada.
            if (row%2==0) campo.setBackground(colorParSeleccionado);
            else campo.setBackground(colorImparSeleccionado);
            campo.setForeground(colorFuenteSeleccionado);
            campo.setFont(fuenteSeleccionado);
        } else {
            if (row%2==0) campo.setBackground(colorParNoSeleccionado);
            else campo.setBackground(colorImparNoSeleccionado); 
            campo.setForeground(colorFuenteNoSeleccionado);
            campo.setFont(fuenteNoSeleccionado);
        }
        if (value instanceof String) { // para valores del tipo String
            campo.setText((String)value);
            campo.setToolTipText(campo.getText());
        }
        if (value instanceof java.util.Date) {
            campo.setText(convertirToStringDMA((java.util.Date)value));
            campo.setToolTipText(campo.getText());
            campo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        }
        if (value instanceof Integer) { // para valores del tipo Integer
            campo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0"))));
            campo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            campo.setValue(((Integer)value));
            campo.setToolTipText(campo.getText());
        } 
        if (value instanceof Double) { // para valores del tipo Double
            campo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.0"))));
            campo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            campo.setValue(((Double)value));
            campo.setToolTipText(campo.getText());
        }
        if (value instanceof java.math.BigDecimal) {
            campo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00")))); 
            campo.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING); 
            campo.setValue(((Integer)value).intValue());
            campo.setToolTipText(campo.getText());
        }
        return campo;
    }

    public static final String convertirToStringDMA(java.util.Date dfecha) {
        java.text.SimpleDateFormat formatoFechaDMA = new java.text.SimpleDateFormat("dd/MM/yy");
        String sfecha;
        try {
            sfecha = formatoFechaDMA.format(dfecha);
        } catch (Exception e) {
            sfecha = "";
        }
        return sfecha;
    }
}

