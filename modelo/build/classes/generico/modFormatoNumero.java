/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modFormatoNumero {

    public final javax.swing.text.DefaultFormatterFactory FormatoDecimal(boolean bseparadorMil, int icantidadDecimal) {
        String scadenaFormatoDespliegue = "#,###" + this.getPuntoFlotante(icantidadDecimal);
        String scadenaFormatoEdicion = "#" + this.getPuntoFlotante(icantidadDecimal);
        if (!bseparadorMil) scadenaFormatoDespliegue = scadenaFormatoEdicion; // sin separador de mil
        java.text.DecimalFormat formatoDespliegue = new java.text.DecimalFormat(scadenaFormatoDespliegue);
        java.text.DecimalFormat formatoEdicion = new java.text.DecimalFormat(scadenaFormatoEdicion);
        //if (!bseparadorMil) formatoEdicion.setGroupingUsed(false); // no quiero separador de miles
        javax.swing.text.NumberFormatter despliegueFormatoNumero = new javax.swing.text.NumberFormatter(formatoDespliegue);
        javax.swing.text.NumberFormatter edicionFormatoNumero = new javax.swing.text.NumberFormatter(formatoEdicion);
        if (!(icantidadDecimal>0)) edicionFormatoNumero.setOverwriteMode(true); // modo de sobreescritura
        edicionFormatoNumero.setAllowsInvalid(false); // no admite caracteres incorrectos
        javax.swing.text.DefaultFormatterFactory actualFactory = new javax.swing.text.DefaultFormatterFactory(despliegueFormatoNumero, despliegueFormatoNumero, edicionFormatoNumero);
        return actualFactory;
    }

    private String getPuntoFlotante(int icantidad) {
        if (icantidad > 0) return ".0000000000".substring(0, 1+icantidad);
        return "";
    }

    public final javax.swing.text.MaskFormatter setMascara(String smascara) {
        javax.swing.text.MaskFormatter mascara = null;
        try { // ejemplo "##,## %" o "##/##/## ##:##:##"
            mascara = new javax.swing.text.MaskFormatter(smascara);
        } catch (Exception e) { }
        mascara.setPlaceholderCharacter('_');
        mascara.setCommitsOnValidEdit(true);
        return mascara;
    }
    
}