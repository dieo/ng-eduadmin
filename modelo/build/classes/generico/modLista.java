/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modLista implements javax.swing.ComboBoxModel {

    private java.util.ArrayList <generico.entLista> data = new java.util.ArrayList <generico.entLista>();
    private java.util.ArrayList <javax.swing.event.ListDataListener> list = new java.util.ArrayList <javax.swing.event.ListDataListener>();
    private generico.entLista selectedItem;
    
    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = anItem instanceof generico.entLista ? ((generico.entLista) anItem) : null;
        for (javax.swing.event.ListDataListener l : list) {
            l.contentsChanged(new javax.swing.event.ListDataEvent(this, javax.swing.event.ListDataEvent.CONTENTS_CHANGED, 0, 0));
        }
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Object getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void addListDataListener(javax.swing.event.ListDataListener l) {
        list.add(l);
    }

    @Override
    public void removeListDataListener(javax.swing.event.ListDataListener l) {
        list.remove(l);
    }
    
    public generico.entLista getEntidad(int index) {
        return data.get(index);
    }
    
    public generico.entLista getEntidad() {
        return selectedItem == null ? null : selectedItem;
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarModelo(java.util.ArrayList<generico.entLista> lst) {
        for (int i=0; i<lst.size(); i++) {
            data.add(lst.get(i));
        }
    }

    public void agregarItem(generico.entLista ent) {
        data.add(ent);
    }

    public void eliminarItem(int i) {
        data.remove(i);
    }
    
    public void cargarModelo(java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                data.add(new generico.entLista().cargar(rs));
            }
        } catch (Exception e) { }
    }

    public void cargarModelo(generico.entLista ent) {
        data.add(ent);
    }

    public generico.entLista setItem(int id) {
        for (generico.entLista ent : data) {
            if (ent.getId()==id) {
                return ent;
            }
        }
        return null;
    }

    public short getId(String sdescripcion) {
        for (int i=0; i<this.getSize(); i++) {
            if (this.getEntidad(i).getDescripcion().equals(sdescripcion)) return (short)this.getEntidad(i).getId();
        }
        return (short)0;
    }
    
    public String getDescripcion(short hid) {
        for (int i=0; i<this.getSize(); i++) {
            if (this.getEntidad(i).getId() == hid) return this.getEntidad(i).getDescripcion();
        }
        return "";
    }
    
}
