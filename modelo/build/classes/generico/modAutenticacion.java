/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

import javax.swing.event.ListDataListener;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modAutenticacion implements javax.swing.ListModel {

    public java.util.ArrayList <especifico.entUsuario> data = new java.util.ArrayList <especifico.entUsuario>();
    private java.util.ArrayList <ListDataListener> list = new java.util.ArrayList <ListDataListener>();
    private String smensaje;
    private int iposicionAutenticada;

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Object getElementAt(int index) {
        return ((especifico.entUsuario)data.get(index)).getFuncionario();
    }

    public especifico.entUsuario getEntidad(int index) {
        return ((especifico.entUsuario)data.get(index));
    }
    
    @Override
    public void addListDataListener(ListDataListener l) {
        list.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        list.remove(l);
    }

    public void removerTodo() {
        data.removeAll(data);
    }

    public void removerEliminado() {
    }
    
    public void cargarTable(java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                data.add(new especifico.entUsuario().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    public String getMensaje() {
        return this.smensaje;
    }
    
    public int getPosicionAutenticada() {
        return this.iposicionAutenticada;
    }

}
