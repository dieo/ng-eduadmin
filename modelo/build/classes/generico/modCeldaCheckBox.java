/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modCeldaCheckBox extends javax.swing.DefaultCellEditor implements javax.swing.table.TableCellRenderer {

    java.awt.Color colorParSeleccionado = new java.awt.Color(102,153,255);
    java.awt.Color colorImparSeleccionado = new java.awt.Color(153,204,255);
    java.awt.Color colorParNoSeleccionado = new java.awt.Color(219,229,241);
    java.awt.Color colorImparNoSeleccionado = new java.awt.Color(255,255,255);
    java.awt.Font fuenteSeleccionado = new java.awt.Font("Tahoma", 1, 9);
    java.awt.Font fuenteNoSeleccionado = new java.awt.Font("Tahoma", 0, 9);
    java.awt.Color colorFuenteSeleccionado = new java.awt.Color(255,255,255);
    java.awt.Color colorFuenteNoSeleccionado = new java.awt.Color(0,0,0);

    private javax.swing.JCheckBox campo = new javax.swing.JCheckBox();
    
    /** Constructor de clase */
    public modCeldaCheckBox() {
        super(new javax.swing.JCheckBox());
    }

   /** retorna valor de celda */
    @Override
    public Object getCellEditorValue() {
        return campo.isSelected();        
    }

    /** Segun el valor de la celda selecciona/deseleciona el JCheckBox */
    @Override
    public java.awt.Component getTableCellEditorComponent(javax.swing.JTable table, Object value, boolean isSelected, int row, int column) {
        campo.setSelected(((Boolean) value).booleanValue());
        return campo;     
    }

    /** cuando termina la manipulacion de la celda */
    @Override
    public boolean stopCellEditing() {
        campo.setSelected(((Boolean)getCellEditorValue()).booleanValue());
        return super.stopCellEditing();
    }

    /** retorna componente */
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) { // Ponemos un color distinto para la etiqueta según si la celda está o no seleccionada.
            if (row%2==0) campo.setBackground(colorParSeleccionado);
            else campo.setBackground(colorImparSeleccionado);
            campo.setForeground(colorFuenteSeleccionado);
            campo.setFont(fuenteSeleccionado);
        } else {
            if (row%2==0) campo.setBackground(colorParNoSeleccionado);
            else campo.setBackground(colorImparNoSeleccionado); 
            campo.setForeground(colorFuenteNoSeleccionado);
            campo.setFont(fuenteNoSeleccionado);
        }
        campo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        if (((Boolean)value).booleanValue()==true) campo.setToolTipText("Verdadero");
        else campo.setToolTipText("Falso");
        campo.setSelected(((Boolean)value).booleanValue());
        return campo;
    }
    
}
