/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modRenderCheckBox extends javax.swing.JCheckBox implements javax.swing.table.TableCellRenderer {

    private javax.swing.JComponent component = new javax.swing.JCheckBox();

    /** Constructor de clase */
    public modRenderCheckBox() {
        //setOpaque(true);
    }
    
    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        //Color de fondo de la celda
        //((javax.swing.JCheckBox) component).setBackground(new java.awt.Color(0,200,0));
        //obtiene valor boolean y coloca valor en el JCheckBox
        boolean b = ((Boolean) value).booleanValue();
        ((javax.swing.JCheckBox)component).setSelected(b);
        return ((javax.swing.JCheckBox)component);
    }

}
