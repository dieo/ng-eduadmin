/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modAhorroProgramadoDetalle implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";
    
    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entAhorroProgramadoDetalle getEntidad() {
        return (especifico.entAhorroProgramadoDetalle)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entAhorroProgramadoDetalle nuevo) {
        nuevo.copiar((especifico.entAhorroProgramadoDetalle)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "CUOTA";
            case 1: return "FECHA VENC.";
            case 2: return "IMPORTE";
            case 3: return "PAGO";
            case 4: return "SALDO";
            case 5: return "CANT. DIA";
            case 6: return "INTERES";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return java.util.Date.class;
            case 2: return Double.class;
            case 3: return Double.class;
            case 4: return Double.class;
            case 5: return Integer.class;
            case 6: return Double.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entAhorroProgramadoDetalle ent;
        ent = (especifico.entAhorroProgramadoDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getCuota();
            case 1: return ent.getFechaVencimiento();
            case 2: return ent.getImporte();
            case 3: return ent.getPago();
            case 4: return ent.getSaldo();
            case 5: return ent.getCantidadDia();
            case 6: return ent.getInteres();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entAhorroProgramadoDetalle ent;
        ent = (especifico.entAhorroProgramadoDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setCuota((Integer)aValue); break;
            case 1: ent.setFechaVencimiento((java.util.Date)aValue); break;
            case 2: ent.setImporte((Double)aValue); break;
            case 3: ent.setPago((Double)aValue); break;
            case 4: ent.setSaldo((Double)aValue); break;
            case 5: ent.setCantidadDia((Integer)aValue); break;
            case 6: ent.setInteres((Double)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entAhorroProgramadoDetalle getEntidad(int rowIndex) {
        return (especifico.entAhorroProgramadoDetalle)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entAhorroProgramadoDetalle nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entAhorroProgramadoDetalle nuevo, int rowIndex) {
        nuevo.copiar((especifico.entAhorroProgramadoDetalle)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entAhorroProgramadoDetalle().cargar(rs));
            }
        } catch (Exception e) {
        }
    }

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(15);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(62);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(52);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(52);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(52);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(15);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(52);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public void recalcularInteres() {
        
    }

    public especifico.entAhorroProgramadoDetalle getTotales() {
        especifico.entAhorroProgramadoDetalle ent = new especifico.entAhorroProgramadoDetalle();
        for (int i=0; i<this.getRowCount(); i++) {
            ent.setImporte(ent.getImporte()+this.getEntidad(i).getImporte());
            ent.setPago(ent.getPago()+this.getEntidad(i).getPago());
            ent.setCantidadDia(ent.getCantidadDia()+this.getEntidad(i).getCantidadDia());
            ent.setInteres(ent.getInteres()+this.getEntidad(i).getInteres());
        }
        ent.setSaldo(this.getEntidad(this.getRowCount()-1).getSaldo());
        return ent;
    }
    
    public especifico.entAhorroProgramadoDetalle getUltimoPago() {
        especifico.entAhorroProgramadoDetalle ent = new especifico.entAhorroProgramadoDetalle();
        int i = 0;
        while (i<this.getRowCount()) {
            if (this.getEntidad(i).getImporte()-this.getEntidad(i).getSaldo()>0) return ent;
            i++;
        }
        return ent;
    }
    
    public void cargarImporteFechaVencimiento(int plazo, double importe, java.util.Date primerVencimiento) {
        for (int i=0; i<plazo; i++) {
            this.insertar(new especifico.entAhorroProgramadoDetalle());
            this.getEntidad(i).setCuota(i+1);
            this.getEntidad(i).setFechaVencimiento((java.util.Date)primerVencimiento.clone());
            this.getEntidad(i).setImporte(importe);
            primerVencimiento = utilitario.utiFecha.getVencimiento(primerVencimiento, 2);
        }
    }
    
    public void cargarSaldo() {
        for (int i=0; i<this.getRowCount(); i++) {
            if (i==0) {
                this.getEntidad(i).setSaldo(this.getEntidad(i).getPago());
            } else {
                this.getEntidad(i).setSaldo(this.getEntidad(i).getPago()+this.getEntidad(i-1).getSaldo());
            }
        }
    }
    
    public void cargarProyeccionPago(double upago, int icuotaDesde, int icuotaHasta) { // vuelvo a cargar la proyección en el supuesto caso que abone regularmente.
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getCuota()>=icuotaDesde && this.getEntidad(i).getCuota()<=icuotaHasta) {
                this.getEntidad(i).setPago(upago);
            }
        }
    }

    public void cargarProyeccionFecha() { // vuelvo a cargar la proyección de la fecha
        for (int i=0; i<this.getRowCount(); i++) {
            java.util.Date dfechaAux = (java.util.Date)this.getEntidad(i).getFechaVencimiento().clone();
            dfechaAux = utilitario.utiFecha.getUltimoDia(dfechaAux);
            this.getEntidad(i).setFechaVencimiento(dfechaAux);
        }
    }

    public void calcularInteres(double utasaInteresRetiro, java.util.Date dfechaOperacion) { // realizo el cálculo de los intereses, según la cantidad de días
        for (int i=0; i<this.getRowCount(); i++) {
            if (utilitario.utiFecha.getAMD(this.getEntidad(i).getFechaVencimiento()) < utilitario.utiFecha.getAMD(dfechaOperacion)) {
                if (i > 0) {
                    this.getEntidad(i).setCantidadDia(utilitario.utiFecha.obtenerDiferenciaDia(this.getEntidad(i-1).getFechaVencimiento(), this.getEntidad(i).getFechaVencimiento()));
                    this.getEntidad(i).setInteres((Integer)this.getEntidad(i).getCantidadDia()*utasaInteresRetiro/365*this.getEntidad(i-1).getSaldo()/100);
                }
                this.getEntidad(i).setInteres(utilitario.utiNumero.redondear(this.getEntidad(i).getInteres(),0));
            } else {
                this.getEntidad(i).setCantidadDia(0);
                this.getEntidad(i).setInteres(0);
            }
        }
    }
    
}
