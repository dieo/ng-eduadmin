/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modDetalleLiquidacionSocionew implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entDetalleLiquidacionSocionew getEntidad() {
        return (especifico.entDetalleLiquidacionSocionew)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entDetalleLiquidacionSocionew nuevo) {
        nuevo.copiar((especifico.entDetalleLiquidacionSocionew)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "CUOTA";
            case 1: return "VENCIMIENTO";
            case 2: return "CAPITAL";
            case 3: return "INTERES";
            case 4: return "SALDO";
            case 5: return "COBRO";
            case 6: return "EXONERACION";
            case 7: return "ESTADO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return java.util.Date.class;
            case 2: return Double.class;
            case 3: return Double.class;
            case 4: return Double.class;
            case 5: return Double.class;
            case 6: return Double.class;
            case 7: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entDetalleLiquidacionSocionew ent;
        ent = (especifico.entDetalleLiquidacionSocionew)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getdCuota();
            case 1: return ent.getdFechaVencimiento();
            case 2: return ent.getdMontoCapital();
            case 3: return ent.getdMontoInteres();
            case 4: return ent.getdMontoSaldo();
            case 5: return ent.getdMontoCobro();
            case 6: return ent.getdMontoExoneracion();
            case 7: return ent.getdEstado();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entDetalleLiquidacionSocionew ent;
        ent = (especifico.entDetalleLiquidacionSocionew)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setdCuota((Integer)aValue); break;
            case 1: ent.setdFechaVencimiento((java.util.Date)aValue); break;
            case 2: ent.setdMontoCapital((Double)aValue); break;
            case 3: ent.setdMontoInteres((Double)aValue); break;
            case 4: ent.setdMontoSaldo((Double)aValue); break;
            case 5: ent.setdMontoCobro((Double)aValue); break;
            case 6: ent.setdMontoExoneracion((Double)aValue); break;
            case 7: ent.setdEstado((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entDetalleLiquidacionSocionew getEntidad(int rowIndex) {
        return (especifico.entDetalleLiquidacionSocionew)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entDetalleLiquidacionSocionew nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entDetalleLiquidacionSocionew nuevo, int rowIndex) {
        nuevo.copiar((especifico.entDetalleLiquidacionSocionew)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entDetalleLiquidacionSocionew().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        tbl.getColumnModel().getColumn(0).setMaxWidth(30);
        tbl.getColumnModel().getColumn(1).setMaxWidth(100);
        tbl.getColumnModel().getColumn(2).setMaxWidth(95);
        tbl.getColumnModel().getColumn(3).setMaxWidth(95);
        tbl.getColumnModel().getColumn(4).setMaxWidth(95);
        tbl.getColumnModel().getColumn(5).setMaxWidth(95);
        tbl.getColumnModel().getColumn(6).setMaxWidth(95);
        tbl.getColumnModel().getColumn(7).setMaxWidth(35);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }
    
    public String getMensaje() {
        return this.smensaje;
    }
    
}
