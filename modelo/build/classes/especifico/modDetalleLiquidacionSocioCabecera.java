/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modDetalleLiquidacionSocioCabecera implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    public especifico.modDetalleLiquidacionSocioCabecera cabecera;
    public especifico.modDetalleLiquidacionSocionew detalle = new especifico.modDetalleLiquidacionSocionew();
    public especifico.modDetalleLiquidacionSocionew detalleVer = new especifico.modDetalleLiquidacionSocionew();
    private String smensaje = "";

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entDetalleLiquidacionSocionew getEntidad() {
        return (especifico.entDetalleLiquidacionSocionew)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entDetalleLiquidacionSocionew nuevo) {
        nuevo.copiar((especifico.entDetalleLiquidacionSocionew)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "OPERACION";
            //case 1: return "ENTIDAD";
            case 1: return "NRO.OP.";
            case 2: return "FECHA OP.";
            case 3: return "PLAZO";
            case 4: return "ATRASO";
            case 5: return "MES";
            case 6: return "SALDO";
            case 7: return "A CANC.";
            case 8: return "C";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            //case 1: return String.class;
            case 1: return Integer.class;
            case 2: return java.util.Date.class;
            case 3: return Integer.class;
            case 4: return Double.class;
            case 5: return Double.class;
            case 6: return Double.class;
            case 7: return Double.class;
            case 8: return Boolean.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entDetalleLiquidacionSocionew ent;
        ent = (especifico.entDetalleLiquidacionSocionew)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getcCuenta();
            //case 1: return ent.getcEntidad();
            case 1: return ent.getcNumeroOperacion();
            case 2: return ent.getcFechaOperacion();
            case 3: return ent.getcPlazo();
            case 4: return ent.getcTotalAtraso();
            case 5: return ent.getcTotalMes();
            case 6: return ent.getcTotalSaldo();
            case 7: return ent.getcTotalCancelar();
            case 8: return ent.getcCancela();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entDetalleLiquidacionSocionew ent;
        ent = (especifico.entDetalleLiquidacionSocionew)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setcCuenta((String)aValue); break;
            //case 1: ent.setcEntidad((String)aValue); break;
            case 1: ent.setcNumeroOperacion((Integer)aValue); break;
            case 2: ent.setcFechaOperacion((java.util.Date)aValue); break;
            case 3: ent.setcPlazo((Integer)aValue); break;
            case 4: ent.setcTotalAtraso((Double)aValue); break;
            case 5: ent.setcTotalMes((Double)aValue); break;
            case 6: ent.setcTotalSaldo((Double)aValue); break;
            case 7: ent.setcTotalCancelar((Double)aValue); break;
            case 8: ent.setcCancela((Boolean)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entDetalleLiquidacionSocionew getEntidad(int rowIndex) {
        return (especifico.entDetalleLiquidacionSocionew)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entDetalleLiquidacionSocionew nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entDetalleLiquidacionSocionew nuevo, int rowIndex) {
        nuevo.copiar((especifico.entDetalleLiquidacionSocionew)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entDetalleLiquidacionSocionew().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        // establece el tamaño de las columnas (620-20)
        tbl.getColumnModel().getColumn(0).setMinWidth(120);
       // tbl.getColumnModel().getColumn(1).setMinWidth(50);
        tbl.getColumnModel().getColumn(1).setMinWidth(50);
        tbl.getColumnModel().getColumn(2).setMinWidth(60);
        tbl.getColumnModel().getColumn(3).setMinWidth(30);
        tbl.getColumnModel().getColumn(4).setMinWidth(70);
        tbl.getColumnModel().getColumn(5).setMinWidth(70);
        tbl.getColumnModel().getColumn(6).setMinWidth(70);
        tbl.getColumnModel().getColumn(7).setMinWidth(70);
        tbl.getColumnModel().getColumn(8).setMinWidth(20);
        tbl.getTableHeader().setFont(new java.awt.Font("Arial", 1, 9));
        tbl.setFont(new java.awt.Font("Arial", 1, 9));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox());*/
    }
    
    public String getMensaje() {
        return this.smensaje;
    }

    public void crearCabecera() {
        int iidMovimiento=0, iidCuenta=0; // para realizar el corte
        String movcue1="", movcue2="";
        for (int i=0; i<detalle.getRowCount(); i++) {
            movcue1 = utilitario.utiNumero.convertirToString(detalle.getEntidad(i).getcIdMovimiento())+"-"+utilitario.utiNumero.convertirToString(detalle.getEntidad(i).getcIdCuenta());
            movcue2 = utilitario.utiNumero.convertirToString(iidMovimiento)+"-"+utilitario.utiNumero.convertirToString(iidCuenta);
            if (!movcue1.equals(movcue2)) { // realizo el corte para insertar entidad en la cabecera
                iidMovimiento = detalle.getEntidad(i).getcIdMovimiento(); // guardo el nuevo idmovimiento
                iidCuenta = detalle.getEntidad(i).getcIdCuenta(); // guardo el nuevo idmovimiento
                this.insertar(new especifico.entDetalleLiquidacionSocionew());
                this.getEntidad(this.getRowCount()-1).setcIdMovimiento(detalle.getEntidad(i).getcIdMovimiento());
                this.getEntidad(this.getRowCount()-1).setcIdLiquidacionSocio(detalle.getEntidad(i).getcIdLiquidacionSocio());
                this.getEntidad(this.getRowCount()-1).setcTabla(detalle.getEntidad(i).getcTabla());
                this.getEntidad(this.getRowCount()-1).setcNumeroOperacion(detalle.getEntidad(i).getcNumeroOperacion());
                try { this.getEntidad(this.getRowCount()-1).setcFechaOperacion((java.util.Date)detalle.getEntidad(i).getcFechaOperacion().clone()); }
                catch (Exception e) { }
                this.getEntidad(this.getRowCount()-1).setcTasaInteresMoratorio(detalle.getEntidad(i).getcTasaInteresMoratorio());
                this.getEntidad(this.getRowCount()-1).setcPlazo(detalle.getEntidad(i).getcPlazo());
                this.getEntidad(this.getRowCount()-1).setcCancela(detalle.getEntidad(i).getcCancela());
                this.getEntidad(this.getRowCount()-1).setcIdCuenta(detalle.getEntidad(i).getcIdCuenta());
                this.getEntidad(this.getRowCount()-1).setcCuenta(detalle.getEntidad(i).getcCuenta());
                this.getEntidad(this.getRowCount()-1).setcIdEntidad(detalle.getEntidad(i).getcIdEntidad());
                if (detalle.getEntidad(i).getcEntidad() == null) detalle.getEntidad(i).setcEntidad("");
                this.getEntidad(this.getRowCount()-1).setcEntidad(detalle.getEntidad(i).getcEntidad());
                this.getEntidad(this.getRowCount()-1).setcTasaImpuesto(detalle.getEntidad(i).getcTasaImpuesto());
                this.getEntidad(this.getRowCount()-1).setcTasaExoneracion(detalle.getEntidad(i).getcTasaExoneracion());
                this.getEntidad(this.getRowCount()-1).setcPrioridad(detalle.getEntidad(i).getcPrioridad());
                this.getEntidad(this.getRowCount()-1).idesde = i;
            }
            if (detalle.getEntidad(i).getdEstado() == null) detalle.getEntidad(i).setdEstado("");
            this.getEntidad(this.getRowCount()-1).ihasta = i;
        }
    }

    public void setIdliquidacionsocio(int iidliquidacionsocio) {
        for (int i=0; i<detalle.getRowCount(); i++) {
            detalle.getEntidad(i).setcIdLiquidacionSocio(iidliquidacionsocio);
        }
    }
    
    public void rellenarEstadoSaldo(java.util.Date dfecha, boolean bcerrado, boolean bconCancelacion, int iidInteresMoratorio, int iidInteresPunitorio, int iidImpuesto) {
        if (dfecha == null) return;
        java.util.Date dfechaSaldo = utilitario.utiFecha.getUltimoDia(dfecha);
        for (int i=0; i<detalle.getRowCount(); i++) {
            if (detalle.getEntidad(i).getdMontoSaldo()>0) {
                if (utilitario.utiFecha.getAM(detalle.getEntidad(i).getdFechaVencimiento()) < utilitario.utiFecha.getAM(dfechaSaldo)) {
                    detalle.getEntidad(i).setdEstado(especifico.entDetalleLiquidacionSocionew.estado_atraso);
                }
                if (utilitario.utiFecha.getAM(detalle.getEntidad(i).getdFechaVencimiento()) == utilitario.utiFecha.getAM(dfechaSaldo)) {
                    if (bcerrado) {
                        detalle.getEntidad(i).setdEstado(especifico.entDetalleLiquidacionSocionew.estado_cobro);
                    } else {
                        if (detalle.getEntidad(i).getcIdCuenta()!=iidInteresMoratorio && detalle.getEntidad(i).getcIdCuenta()!=iidInteresPunitorio && detalle.getEntidad(i).getcIdCuenta()!=iidImpuesto) {
                            detalle.getEntidad(i).setdEstado("");
                        }
                    }
                }
                if (utilitario.utiFecha.getAM(detalle.getEntidad(i).getdFechaVencimiento()) > utilitario.utiFecha.getAM(dfechaSaldo)) {
                    if (bconCancelacion) detalle.getEntidad(i).setdEstado(especifico.entDetalleLiquidacionSocionew.estado_saldo);
                    else detalle.getEntidad(i).setdEstado("");
                }
                if (detalle.getEntidad(i).getdEstado().equals(especifico.entDetalleLiquidacionSocionew.estado_atraso) || detalle.getEntidad(i).getdEstado().equals(especifico.entDetalleLiquidacionSocionew.estado_cobro) || detalle.getEntidad(i).getdEstado().equals(especifico.entDetalleCancelacionnew.estado_saldo)) {
                    detalle.getEntidad(i).setdMontoCobro(detalle.getEntidad(i).getdMontoSaldo());
                } else {
                    detalle.getEntidad(i).setdMontoCobro(0);
                }
            }
        }
    }

    public void rellenarEstadoSaldo(int iindice, java.util.Date dfecha, boolean bconCancelacion) {
        if (dfecha == null) return;
        java.util.Date dfechaSaldo = utilitario.utiFecha.getUltimoDia(dfecha);
        for (int i=this.getEntidad(iindice).idesde; i<=this.getEntidad(iindice).ihasta; i++) {
            detalle.getEntidad(i).setcCancela(bconCancelacion);
            if (detalle.getEntidad(i).getdMontoSaldo()>0) {
                if (utilitario.utiFecha.getAM(detalle.getEntidad(i).getdFechaVencimiento()) > utilitario.utiFecha.getAM(dfechaSaldo)) {
                    if (bconCancelacion) {
                        detalle.getEntidad(i).setdEstado(especifico.entDetalleLiquidacionSocionew.estado_saldo);
                        detalle.getEntidad(i).setdMontoCobro(detalle.getEntidad(i).getdMontoSaldo());
                    } else {
                        detalle.getEntidad(i).setdEstado("");
                        detalle.getEntidad(i).setdMontoCobro(0);
                        detalle.getEntidad(i).setdMontoExoneracion(0);
                    }
                }
            }
        }
        if (!this.esValidoRellenar(iindice, dfecha)) return;
        for (int i=this.getEntidad(iindice).idesde; i<=this.getEntidad(iindice).ihasta; i++) {
            if (bconCancelacion) {
                if (detalle.getEntidad(i).getdEstado().equals(especifico.entDetalleLiquidacionSocionew.estado_saldo)) {
                    detalle.getEntidad(i).setdMontoExoneracion(detalle.getEntidad(i).getdMontoCobro() * detalle.getEntidad(i).getcTasaExoneracion() / 100);
                }
            } else {
                detalle.getEntidad(i).setdMontoExoneracion(0);
            }
        }
    }
    
    public int buscarCuenta(int iidCuenta, int iidMovimiento) {
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getcIdCuenta()==iidCuenta && this.getEntidad(i).getcIdMovimiento()==iidMovimiento) {
                return i;
            }
        }
        return -1;
    }
    
    public boolean esValidoRellenar(int iindice, java.util.Date dfecha) {
        int icantidadSaldo = 0;
        int icantidadSaldar = 0;
        java.util.Date dfechaSaldo = utilitario.utiFecha.getUltimoDia(dfecha);
        for (int i=this.getEntidad(iindice).idesde; i<=this.getEntidad(iindice).ihasta; i++) {
            if (utilitario.utiFecha.getAM(detalle.getEntidad(i).getdFechaVencimiento()) > utilitario.utiFecha.getAM(dfechaSaldo)) icantidadSaldo++;
            if (detalle.getEntidad(i).getdEstado().equals(especifico.entDetalleIngresonew.estado_saldo)) icantidadSaldar++;
        }
        if (icantidadSaldo-icantidadSaldar == 0 && icantidadSaldar > 1) return true;
        else return false;
    }  
    
    public void calcularSubtotal() {
        for (int k=0; k<this.getRowCount(); k++) {
            this.getEntidad(k).setcTotalAtraso(0.0);
            this.getEntidad(k).setcTotalMes(0.0);
            this.getEntidad(k).setcTotalAplicado(0.0);
            this.getEntidad(k).setcTotalSaldo(0.0);
            this.getEntidad(k).setcTotalCancelar(0.0);
            this.getEntidad(k).setcTotalExoneracion(0.0);
            for (int i=this.getEntidad(k).idesde; i<=this.getEntidad(k).ihasta; i++) {
                if (detalle.getEntidad(i).getdEstado().equals(especifico.entDetalleLiquidacionSocionew.estado_atraso)) {
                    this.getEntidad(k).setcTotalAtraso(this.getEntidad(k).getcTotalAtraso() + detalle.getEntidad(i).getdMontoCobro());
                }
                if (detalle.getEntidad(i).getdEstado().equals(especifico.entDetalleLiquidacionSocionew.estado_cobro)) {
                    this.getEntidad(k).setcTotalMes(this.getEntidad(k).getcTotalMes() + detalle.getEntidad(i).getdMontoCobro());
                }
                if (detalle.getEntidad(i).getdEstado().equals(especifico.entDetalleLiquidacionSocionew.estado_saldo)) {
                    this.getEntidad(k).setcTotalSaldo(this.getEntidad(k).getcTotalSaldo() + detalle.getEntidad(i).getdMontoCobro());
                }
                this.getEntidad(k).setcTotalExoneracion(this.getEntidad(k).getcTotalExoneracion() + detalle.getEntidad(i).getdMontoExoneracion());                
                this.getEntidad(k).setcTotalAplicado(this.getEntidad(k).getcTotalAplicado() + detalle.getEntidad(i).getdMontoAplicado());
            }
            this.getEntidad(k).setcTotalCancelar(this.getEntidad(k).getcTotalAtraso() + this.getEntidad(k).getcTotalMes() + this.getEntidad(k).getcTotalSaldo());
        }
    }
    
    public especifico.entDetalleLiquidacionSocionew calcularTotalCabecera() {
        especifico.entDetalleLiquidacionSocionew ent = new especifico.entDetalleLiquidacionSocionew();
        for (int i=0; i<this.getRowCount(); i++) {
            ent.setcTotalAtraso(ent.getcTotalAtraso()+this.getEntidad(i).getcTotalAtraso());
            ent.setcTotalMes(ent.getcTotalMes()+this.getEntidad(i).getcTotalMes());
            ent.setcTotalSaldo(ent.getcTotalSaldo()+this.getEntidad(i).getcTotalSaldo());
            ent.setcTotalCancelar(ent.getcTotalCancelar()+this.getEntidad(i).getcTotalCancelar());
            ent.setcTotalExoneracion(ent.getcTotalExoneracion()+this.getEntidad(i).getcTotalExoneracion());
        }
        return ent;
    }
    
    public especifico.entDetalleLiquidacionSocionew calcularTotalDetalle(int iindice) {
        especifico.entDetalleLiquidacionSocionew ent = new especifico.entDetalleLiquidacionSocionew();
        for (int i=this.getEntidad(iindice).idesde; i<=this.getEntidad(iindice).ihasta; i++) {
            ent.setdMontoSaldo(ent.getdMontoSaldo() + detalle.getEntidad(i).getdMontoSaldo());
            ent.setdMontoCobro(ent.getdMontoCobro() + detalle.getEntidad(i).getdMontoCobro());
            ent.setdMontoExoneracion(ent.getdMontoExoneracion() + detalle.getEntidad(i).getdMontoExoneracion());
        }
        return ent;
    }
    
    public void crearCuenta(java.util.Date dfecha, int iidliquidacionsocio, int icantidadDiaGracia, int iidCuenta, String scuenta, String sprioridad, int iidInteresMoratorio, int iidInteresPunitorio, double utasaImpuesto) {
        if (dfecha == null) return;
        double umonto = 0.0; // para calcular el total del interés moratorio
        for (int i=0; i<detalle.getRowCount(); i++) {
            if (iidCuenta==iidInteresMoratorio) {
                int diasMora = utilitario.utiFecha.obtenerDiferenciaDia(detalle.getEntidad(i).getdFechaVencimiento(), (java.util.Date)dfecha.clone());
                diasMora = diasMora - icantidadDiaGracia;
                if (!utilitario.utiGeneral.calculaMora(detalle.getEntidad(i).getdFechaVencimiento())) diasMora = 0;
                detalle.getEntidad(i).setdMontoInteresMoratorio(0.0);
                if (diasMora>0) {
                    double tasaMora = detalle.getEntidad(i).getcTasaInteresMoratorio()*12/365*diasMora;
                    detalle.getEntidad(i).setdMontoInteresMoratorio(utilitario.utiNumero.redondear(detalle.getEntidad(i).getdMontoSaldo()*tasaMora/100, 0));
                }
                umonto += detalle.getEntidad(i).getdMontoInteresMoratorio();
            }
            if (iidCuenta==iidInteresPunitorio) {
                int diasMora = utilitario.utiFecha.obtenerDiferenciaDia(detalle.getEntidad(i).getdFechaVencimiento(), (java.util.Date)dfecha.clone());
                diasMora = diasMora - icantidadDiaGracia;
                detalle.getEntidad(i).setdMontoInteresPunitorio(0.0);
                if (diasMora>0) {
                    double tasaMora = detalle.getEntidad(i).getcTasaInteresPunitorio()*12/365*diasMora;
                    detalle.getEntidad(i).setdMontoInteresPunitorio(utilitario.utiNumero.redondear(detalle.getEntidad(i).getdMontoSaldo()*tasaMora/100, 0));
                }
                umonto += detalle.getEntidad(i).getdMontoInteresPunitorio();
            }
        }
        umonto += utilitario.utiNumero.redondear(umonto*utasaImpuesto/100, 0);
        if (umonto>0 && iidCuenta==iidInteresMoratorio) detalle.insertar(this.crearEntidad(iidliquidacionsocio, iidCuenta, scuenta, dfecha, sprioridad, umonto, especifico.entDetalleLiquidacionSocionew.estado_atraso));
        if (umonto>0 && iidCuenta==iidInteresPunitorio) detalle.insertar(this.crearEntidad(iidliquidacionsocio, iidCuenta, scuenta, dfecha, sprioridad, umonto, especifico.entDetalleLiquidacionSocionew.estado_atraso));
    }

    public void crearCuenta(java.util.Date dfecha, int iidliquidacionsocio, int iidCuenta, String scuenta, String sprioridad, double umonto, double utasa, int iplazo, double utasaImpuesto) {
        double umontoImpuesto = (umonto * utasa * iplazo / 100) * utasaImpuesto / 100;
        detalle.insertar(this.crearEntidad(iidliquidacionsocio, iidCuenta, scuenta, dfecha, sprioridad, utilitario.utiNumero.redondear(umontoImpuesto, 0), especifico.entDetalleLiquidacionSocionew.estado_atraso));
    }

    private especifico.entDetalleLiquidacionSocionew crearEntidad(int iidliquidacionsocio, int iidCuenta, String scuenta, java.util.Date dfecha, String sprioridad, double umonto, String sestado) {
        especifico.entDetalleLiquidacionSocionew ent = new especifico.entDetalleLiquidacionSocionew();
        ent.setcIdLiquidacionSocio(iidliquidacionsocio);
        ent.setcIdCuenta(iidCuenta);
        ent.setcCuenta(scuenta);
        ent.setcFechaOperacion((java.util.Date)dfecha.clone());
        ent.setcTabla(especifico.entDetalleOperacion.tabla_operacionfija);
        ent.setcPrioridad(sprioridad);
        ent.setcPlazo(1);
        ent.setcCancela(false);
        ent.setdCuota(1);
        ent.setdFechaVencimiento((java.util.Date)dfecha.clone());
        ent.setdMontoSaldo(umonto);
        ent.setdMontoCobro(umonto);
        ent.setdEstado(sestado);
        return ent;
    }
    
    public void copiar(int indice) { // realiza una copia del detalle para una ver una operación específica
        detalleVer.removerTodo();
        for (int i=this.getEntidad(indice).idesde; i<=this.getEntidad(indice).ihasta; i++) {
            detalleVer.insertar(detalle.getEntidad(i).copiar(new especifico.entDetalleLiquidacionSocionew()));
        }
    }
    
    public void establecerOrden() {
        especifico.modDetalleLiquidacionSocionew modAux = new especifico.modDetalleLiquidacionSocionew();
        for(int i=0; i<detalle.getRowCount(); i++) {
            modAux.insertar(new especifico.entDetalleLiquidacionSocionew());
            modAux.getEntidad(modAux.getRowCount()-1).setcIdCuenta(detalle.getEntidad(i).getcIdCuenta());
            modAux.getEntidad(modAux.getRowCount()-1).setdFechaVencimiento((java.util.Date)detalle.getEntidad(i).getdFechaVencimiento().clone());
            modAux.getEntidad(modAux.getRowCount()-1).setcPrioridad(detalle.getEntidad(i).getcPrioridad());
            modAux.getEntidad(modAux.getRowCount()-1).setdOrden(i);
        }
        for(int i=0; i<modAux.getRowCount(); i++) {
            for(int k=i+1; k<modAux.getRowCount(); k++) {
                if (utilitario.utiFecha.getAMD(modAux.getEntidad(i).getdFechaVencimiento()) < utilitario.utiFecha.getAMD(modAux.getEntidad(k).getdFechaVencimiento())) {
                    this.intercambio(modAux.getEntidad(i), modAux.getEntidad(k));
                }
                if (utilitario.utiFecha.getAMD(modAux.getEntidad(i).getdFechaVencimiento()) == utilitario.utiFecha.getAMD(modAux.getEntidad(k).getdFechaVencimiento())) {
                    if (utilitario.utiCadena.obtenerPrioridad(modAux.getEntidad(i).getcPrioridad()) > utilitario.utiCadena.obtenerPrioridad(modAux.getEntidad(k).getcPrioridad())) {
                        this.intercambio(modAux.getEntidad(i), modAux.getEntidad(k));
                    }
                }
            }
        }
        for(int i=0; i<modAux.getRowCount(); i++) {
            detalle.getEntidad(modAux.getEntidad(i).getdOrden()).setdOrden(i);
        }
    }

    private void intercambio(especifico.entDetalleLiquidacionSocionew origen, especifico.entDetalleLiquidacionSocionew destino) {
        especifico.entDetalleLiquidacionSocionew entAux = new especifico.entDetalleLiquidacionSocionew();
        origen.copiar(entAux);
        destino.copiar(origen);
        entAux.copiar(destino);
    }
    
    public void resumirCuota() { // 
        for(int k=0; k<this.getRowCount(); k++) {
            java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
            for(int i=this.getEntidad(k).idesde; i<=this.getEntidad(k).ihasta; i++) {
                if (detalle.getEntidad(i).getdMontoCobro()>0) {
                    lst.add(new generico.entLista(detalle.getEntidad(i).getdCuota(), "", "", (short)0));
                }
            }
            this.getEntidad(k).setcResumenCuota(utilitario.utiCadena.resumirCuota(lst)+"/"+this.getEntidad(k).getcPlazo());
        }
    }
    
    public void crearCopia() { // realiza una copia para la impresión de recalculo
        cabecera = new especifico.modDetalleLiquidacionSocioCabecera();
        cabecera.removerTodo();
        for (int i=0; i<this.getRowCount(); i++) {
            cabecera.insertar(new especifico.entDetalleLiquidacionSocionew());
            this.getEntidad(i).copiar(cabecera.getEntidad(i));
        }
    }

    public void distribuirCobro(double ucobro, int iidInteresMoratorio, int iidInteresPunitorio, int iidImpuesto) { // distribuye el cobro
        int i = 0, indice = 0;
        while (i < detalle.getRowCount() && ucobro > 0) { // carga nuevamente el cobro de acuerdo al efectivo
            indice = buscar(i);
            // para el recalculo no debe cobrar interes moratorio, interes punitorio e impuesto
                if ((detalle.getEntidad(indice).getdEstado().equals(especifico.entDetalleIngreso.estado_atraso) || detalle.getEntidad(indice).getdEstado().equals(especifico.entDetalleIngreso.estado_cobro)) && detalle.getEntidad(indice).getdMontoCobro()>0) {
                    if (ucobro > detalle.getEntidad(indice).getdMontoCobro()) {
                        detalle.getEntidad(indice).setdMontoAplicado(detalle.getEntidad(indice).getdMontoCobro());
                        ucobro = ucobro - detalle.getEntidad(indice).getdMontoCobro();
                        detalle.getEntidad(indice).setdMontoCobro(0);
                    } else {
                        detalle.getEntidad(indice).setdMontoAplicado(ucobro);
                        detalle.getEntidad(indice).setdMontoCobro(detalle.getEntidad(indice).getdMontoCobro() - ucobro);
                        ucobro = 0;
                    }
                }
            i++;
        }
    }

    private int buscar(int iorden) {
        int i = 0;
        while (i < detalle.getRowCount()) {
            if (detalle.getEntidad(i).getdOrden()==iorden) return i;
            i++;
        }
        return -1;
    }
    
}
