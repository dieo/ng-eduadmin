/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modReferenciaPersonal implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";
    
    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entReferenciaPersonal getEntidad() {
        return (especifico.entReferenciaPersonal)(data.get(tbl.getSelectedRow()));
    }

    public void modificar(especifico.entReferenciaPersonal nuevo) {
        nuevo.copiar((especifico.entReferenciaPersonal)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "CEDULA";
            case 1: return "NOMBRE";
            case 2: return "APELLIDO";
            case 3: return "DIRECCION";
            case 4: return "TELEFONO";
            case 5: return "PARENTESCO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return String.class;
            case 2: return String.class;
            case 3: return String.class;
            case 4: return String.class;
            case 5: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entReferenciaPersonal ent;
        ent = (especifico.entReferenciaPersonal)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getCedula();
            case 1: return ent.getNombre();
            case 2: return ent.getApellido();
            case 3: return ent.getDireccion();
            case 4: return ent.getTelefono();
            case 5: return ent.getParentesco();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entReferenciaPersonal ent;
        ent = (especifico.entReferenciaPersonal)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setCedula((String)aValue); break;
            case 1: ent.setNombre((String)aValue); break;
            case 2: ent.setApellido((String)aValue); break;
            case 3: ent.setDireccion((String)aValue); break;
            case 4: ent.setTelefono((String)aValue); break;
            case 5: ent.setParentesco((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entReferenciaPersonal getEntidad(int rowIndex) {
        return (especifico.entReferenciaPersonal)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entReferenciaPersonal nuevo) {
        try {
            data.add(nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar (especifico.entReferenciaPersonal nuevo, int rowIndex) {
        nuevo.copiar((especifico.entReferenciaPersonal)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.last();
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entReferenciaPersonal().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    public void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(70);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(70);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(80);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(20);
        // alineación a la derecha
        //javax.swing.table.DefaultTableCellRenderer tcr = new javax.swing.table.DefaultTableCellRenderer();
        //tcr.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        //tbl.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString()); 
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); 
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString()); */
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
}
