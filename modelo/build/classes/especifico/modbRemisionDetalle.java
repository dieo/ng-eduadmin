/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class modbRemisionDetalle implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";
    
    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entbRemisionDetalle getEntidad() {
        return (especifico.entbRemisionDetalle)(data.get(tbl.getSelectedRow()));
    }

    public void modificar(especifico.entbRemisionDetalle nuevo) {
        nuevo.copiar((especifico.entbRemisionDetalle)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "TIPO DOCUMENTO";
            case 1: return "Nº DOCUMENTO";
            case 2: return "DESCRIPCION";
            case 3: return "NOTA CREDITO";
            case 4: return "CANTIDAD";
            case 5: return "MONTO";
            case 6: return "LEGAJO COMPLETO";
            case 7: return "RECIBIDO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return Integer.class;
            case 2: return String.class;
            case 3: return Integer.class;
            case 4: return Double.class;
            case 5: return Double.class;
            case 6: return Boolean.class;
            case 7: return Boolean.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entbRemisionDetalle ent;
        ent = (especifico.entbRemisionDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getTipoDocumento();
            case 1: return ent.getNumeroDocumento();
            case 2: return ent.getDescripcion();
            case 3: return ent.getNumeroNota();
            case 4: return ent.getCantidad();
            case 5: return ent.getMonto();
            case 6: return ent.getLegajoCompleto();
            case 7: return ent.getRecibido();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entbRemisionDetalle ent;
        ent = (especifico.entbRemisionDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setTipoDocumento((String)aValue); break;
            case 1: ent.setNumeroDocumento((Integer)aValue); break;
            case 2: ent.setDescripcion((String)aValue); break;
            case 3: ent.setNumeroNota((Integer)aValue); break;
            case 4: ent.setCantidad((Double)aValue); break;
            case 5: ent.setMonto((Double)aValue); break;
            case 6: ent.setLegajoCompleto((Boolean)aValue); break;
            case 7: ent.setRecibido((Boolean)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entbRemisionDetalle getEntidad(int rowIndex) {
        return (especifico.entbRemisionDetalle)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar(especifico.entbRemisionDetalle nuevo) {
        try {
            data.add(nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entbRemisionDetalle nuevo, int rowIndex) {
        nuevo.copiar((especifico.entbRemisionDetalle)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar(int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }
    
    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable(java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entbRemisionDetalle().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(130);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(7).setPreferredWidth(10);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }
    
    public double getTotal() {
        double total = 0;
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getEstadoRegistro()!=generico.entConstante.estadoregistro_eliminado){
                total += this.getEntidad(i).getMonto();
            }
        }
        return total;
    }

    
}
