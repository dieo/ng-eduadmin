/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modExtractoCierre implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    public especifico.modExtractoCierre aporte;
    public especifico.modExtractoCierre operacion;
    public especifico.modExtractoCierre prestamo;

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entExtractoCierre getEntidad() {
        return (especifico.entExtractoCierre)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entExtractoCierre nuevo) {
        nuevo.copiar((especifico.entExtractoCierre)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "CUENTA";
            case 1: return "RUBRO";
            case 2: return "NRO.OP.";
            case 3: return "FEC.EMI.";
            case 4: return "MONTO CIERRE";
            case 5: return "MONTO COBRO";
            case 6: return "DIFERENCIA";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return String.class;
            case 2: return Integer.class;
            case 3: return java.util.Date.class;
            case 4: return Double.class;
            case 5: return Double.class;
            case 6: return Double.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entExtractoCierre ent;
        ent = (especifico.entExtractoCierre)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getDenominacion();
            case 1: return ent.getRubro();
            case 2: return ent.getNumeroOperacion();
            case 3: return ent.getFechaOperacion();
            case 4: return ent.getMontoCierre();
            case 5: return ent.getMontoCobro();
            case 6: return ent.getMontoDiferencia();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entExtractoCierre ent;
        ent = (especifico.entExtractoCierre)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setDenominacion((String)aValue); break;
            case 1: ent.setRubro((String)aValue); break;
            case 2: ent.setNumeroOperacion((Integer)aValue); break;
            case 3: ent.setFechaOperacion((java.util.Date)aValue); break;
            case 4: ent.setMontoCierre((Double)aValue); break;
            case 5: ent.setMontoCobro((Double)aValue); break;
            case 6: ent.setMontoDiferencia((Double)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entExtractoCierre getEntidad(int rowIndex) {
        return (especifico.entExtractoCierre)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entExtractoCierre nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entExtractoCierre nuevo, int rowIndex) {
        nuevo.copiar((especifico.entExtractoCierre)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entExtractoCierre().cargar(rs));
            }
        } catch (Exception e) {
        }
    }

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(120);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(40);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public especifico.entExtractoCierre getTotales() {
        especifico.entExtractoCierre ent = new especifico.entExtractoCierre();
        for (int i=0; i<this.getRowCount(); i++) {
            ent.setMontoCierre(ent.getMontoCierre()+this.getEntidad(i).getMontoCierre());
            ent.setMontoCobro(ent.getMontoCobro()+this.getEntidad(i).getMontoCobro());
            ent.setMontoDiferencia(ent.getMontoDiferencia()+this.getEntidad(i).getMontoDiferencia());
        }
        return ent;
    }
    
    public void separarGrupo(int iidAporteCuotaSocial, int iidAporteCapital, int iidSolidaridad, int iidFondoPrevision) {
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getTabla().equals(especifico.entDetalleOperacion.tabla_operacionfija)) {
                if (this.getEntidad(i).getIdCuenta()==iidAporteCuotaSocial || this.getEntidad(i).getIdCuenta()==iidAporteCapital || this.getEntidad(i).getIdCuenta()==iidSolidaridad || this.getEntidad(i).getIdCuenta()==iidFondoPrevision) {
                    this.aporte.insertar(this.getEntidad(i));
                } else {
                    this.operacion.insertar(this.getEntidad(i));
                }
            }
            if (this.getEntidad(i).getTabla().equals(especifico.entDetalleOperacion.tabla_fondojuridicosepelio) || this.getEntidad(i).getTabla().equals(especifico.entDetalleOperacion.tabla_ahorroprogramado)) {
                this.operacion.insertar(this.getEntidad(i));
            }
            if (this.getEntidad(i).getTabla().equals(especifico.entDetalleOperacion.tabla_movimiento)) {
                this.prestamo.insertar(this.getEntidad(i));
            }
        }
    }
    
}
