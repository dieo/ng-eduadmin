/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;

/**
 *
 * @author Lic. Didier Barreto
 */
public class modCDA implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private final java.util.LinkedList data = new java.util.LinkedList();
    private final java.util.LinkedList list = new java.util.LinkedList();
    private final String smensaje = "";

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "Nº CDA";
            case 1: return "SERIE";
            case 2: return "CEDULA";
            case 3: return "TIPO CDA";
            case 4: return "FECHA INICIO";
            case 5: return "ESTADO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return String.class;
            case 2: return String.class;
            case 3: return String.class;
            case 4: return java.util.Date.class;
            case 5: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entCDA ent;
        ent = (especifico.entCDA)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getNumeroCDA();
            case 1: return ent.getSerie();
            case 2: return ent.getCedula();
            case 3: return ent.getTipoCDA();
            case 4: return ent.getFechaInicio();
            case 5: return ent.getEstadoCDA();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entCDA ent;
        ent = (especifico.entCDA)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setNumeroCDA((Integer)aValue); break;
            case 1: ent.setSerie((String)aValue); break;
            case 2: ent.setCedula((String)aValue); break;
            case 3: ent.setTipoCDA((String)aValue); break;
            case 4: ent.setFechaInicio((java.util.Date)aValue); break;
            case 5: ent.setEstadoCDA((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entCDA getEntidad() {
        return (especifico.entCDA)(data.get(tbl.getSelectedRow()));
    }
    
    public especifico.entCDA getEntidad(int rowIndex) {
        return (especifico.entCDA)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entCDA nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entCDA nuevo) {
        nuevo.copiar((especifico.entCDA)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entCDA().cargar(rs));
            }
        } catch (SQLException e) {
        }
    }

    public void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(25);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(105);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(50);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox());*/
    }
 
    public String getMensaje() {
        return this.smensaje;
    }

}
