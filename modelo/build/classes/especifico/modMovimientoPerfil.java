/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;


/**
 *
 * @author Ing. Edison Martinez
 */
public class modMovimientoPerfil implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entMovimientoPerfil getEntidad() {
        return (especifico.entMovimientoPerfil)(data.get(tbl.getSelectedRow()));
    }

    public void modificar(especifico.entMovimientoPerfil nuevo) {
        nuevo.copiar((especifico.entMovimientoPerfil)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "PERFIL";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entMovimientoPerfil ent;
        ent = (especifico.entMovimientoPerfil)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getPerfil();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entMovimientoPerfil ent;
        ent = (especifico.entMovimientoPerfil)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setPerfil((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entMovimientoPerfil getEntidad(int rowIndex) {
        return (especifico.entMovimientoPerfil)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar(especifico.entMovimientoPerfil nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entMovimientoPerfil nuevo, int rowIndex) {
        nuevo.copiar((especifico.entMovimientoPerfil)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar(int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entMovimientoPerfil().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    /*public void establecerFormato(javax.swing.JTable tbl) {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(300);
    }*/

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(300);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }
    
    public boolean esValido(int ifilaActual, int iidPerfil) {
        if (ifilaActual==0 & iidPerfil==0) { // no interesa la validacion de unicidad, validacion de un registro de perfil
            if (this.getRowCount()<=0) {
                this.smensaje = "El usuario debe tener por lo menos un perfil";
                return false;
            }
        }
        for (int i=0; i<this.getRowCount(); i++) { // validacion de unicidad
            if (this.getEntidad(i).getIdPerfil()==iidPerfil && i!=ifilaActual) {
                this.smensaje = "Verifique el Perfil sea único";
                return false;
            }
        }
        return true;
    }

    public void cargarEnModeloGenerico(utilitario.modLista modLista, int iidUsuario) {
        modLista.removerTodo();
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getIdUsuario()==iidUsuario) {
                modLista.cargarModelo(new utilitario.entLista(this.getEntidad(i).getIdPerfil(), this.getEntidad(i).getPerfil(), "", (short)0));
            }
        }
    }
    
    public String getMensaje() {
        return this.smensaje;
    }
    
}
