/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsInformePrestamoCancelacion implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet operacion;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsInformePrestamoCancelacion(Object mod) {
        this.operacion = (java.sql.ResultSet)mod;
        try {
            this.operacion.last();
            total = this.operacion.getRow()+1;
            this.operacion.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.operacion.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedula".equals(jrCampo.getName())) {
                valor = operacion.getString("cedula");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(operacion.getString("apellidonombre"));
            } else if("numerosolicitud".equals(jrCampo.getName())) {
                valor = operacion.getInt("numerosolicitud");
            } else if("fechasolicitud".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechageneracion");
            } else if("plazo".equals(jrCampo.getName())) {
                valor = operacion.getInt("plazoaprobado");
            } else if("fuentefinanciera".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(operacion.getString("entidad"));
            } else if("montocapital".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montoaprobado");
            } else if("montointeres".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montointeres");
            } else if("montosaldo".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montosaldo");
            } else if("tipocredito".equals(jrCampo.getName())) {
                valor = operacion.getString("tipocredito");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = operacion.getInt("numerooperacion");
            } else if("montocancelacion".equals(jrCampo.getName())) { 
                valor = operacion.getDouble("montocancelacion");
            } else if("montoimpuesto".equals(jrCampo.getName())) { 
                valor = operacion.getDouble("montoimpuesto");
            } else if("idcuenta".equals(jrCampo.getName())) { 
                valor = operacion.getInt("idcuenta");
            } else if("cuenta".equals(jrCampo.getName())) { 
                valor = operacion.getString("cuenta");
            } else if("cuota".equals(jrCampo.getName())) { 
                valor = operacion.getInt("cuota");
            } else if("plazo2".equals(jrCampo.getName())) { 
                valor = operacion.getInt("plazo");
            } else if("vencimiento".equals(jrCampo.getName())) { 
                valor = operacion.getDate("fechavencimiento");
            } else if("operacion".equals(jrCampo.getName())) { 
                valor = operacion.getInt("operacion");
            } else if("fechaoperacion".equals(jrCampo.getName())) { 
                valor = operacion.getDate("fechaoperacion");
            } else if("estado".equals(jrCampo.getName())) { 
                valor = operacion.getString("estado");
            } else if("capital".equals(jrCampo.getName())) { 
                valor = operacion.getDouble("capital");
            } else if("interes".equals(jrCampo.getName())) { 
                valor = operacion.getDouble("interes");
            } else if("exoneracion".equals(jrCampo.getName())) { 
                valor = operacion.getDouble("exoneracion");
            } else if("cobro".equals(jrCampo.getName())) { 
                valor = operacion.getDouble("cobro");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
