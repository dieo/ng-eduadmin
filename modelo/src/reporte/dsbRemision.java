/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbRemision implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet Remision;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;
    
    public dsbRemision(Object mod) {
        this.Remision = (java.sql.ResultSet)mod;
        try {
            this.Remision.last();
            total = this.Remision.getRow()+1;
            this.Remision.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    @Override
    
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.Remision.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("referente".equals(jrCampo.getName())) {
                valor = Remision.getString("destino");
            } else if("localidad".equals(jrCampo.getName())) {
                valor = Remision.getString("localidad");
            } else if("dependencia".equals(jrCampo.getName())) {
                valor = Remision.getString("dependencia");
            } else if("promotor".equals(jrCampo.getName())) {
                valor = Remision.getString("promotor");
            } else if("orden".equals(jrCampo.getName())) {
                valor = Remision.getInt("orden");
            } else if("numeroremision".equals(jrCampo.getName())) {
                valor = Remision.getInt("idremision")+"/"+utilitario.utiFecha.getAnho(Remision.getDate("fechadesde"));
            } else if("codigo".equals(jrCampo.getName())) {
                valor = Remision.getString("codigo");
            } else if("courrier".equals(jrCampo.getName())) {
                valor = Remision.getInt("idcourier");
            } else if("courier".equals(jrCampo.getName())) {
                valor = Remision.getString("courier");
            } else if("numerocourier".equals(jrCampo.getName())) {
                valor = Remision.getString("numerocourier");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = Remision.getInt("cantidad");
            } else if("numerodocumento".equals(jrCampo.getName())) {
                valor = Remision.getInt("numerodocumento");
            } else if("tipodocumento".equals(jrCampo.getName())) {
                valor = Remision.getString("tipodocumento");
            } else if("banco".equals(jrCampo.getName())) {
                valor = Remision.getString("banco");
            } else if("monto".equals(jrCampo.getName())) {
                valor = Remision.getDouble("monto");
            } else if("total".equals(jrCampo.getName())) {
                valor = Remision.getDouble("total");
            } else if("numeronota".equals(jrCampo.getName())) {
                valor = Remision.getInt("numeronota");
            } else if("fechaenvio".equals(jrCampo.getName())) {  
                valor = Remision.getDate("fechaenvio");
            } else if("fechadesde".equals(jrCampo.getName())) {  
                valor = Remision.getDate("fechadesde");
            } else if("fechahasta".equals(jrCampo.getName())) {
                valor = Remision.getDate("fechahasta");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = Remision.getDate("fecha");
            } else if("estado".equals(jrCampo.getName())) {
                valor = Remision.getString("estadoremision");
            } else if("centrocosto".equals(jrCampo.getName())) {
                valor = Remision.getString("centrocosto");
            } else if("concepto".equals(jrCampo.getName())) {
                valor = Remision.getString("descripcion");
            } else if("legajo".equals(jrCampo.getName())) {
                if(Remision.getBoolean("legajocompleto")) valor = "X";
                else valor = "";
            } else if("recibido".equals(jrCampo.getName())) {
                if(Remision.getBoolean("recibido")) valor = "X";
                else valor = "";
            } 
        } catch (Exception e) {}
        return valor;
    }
    
}
