/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAhorroProgramadoAnulacion implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entAhorroProgramado ahorro;
    private double usaldo;
    private int index = -1;

    public dsAhorroProgramadoAnulacion(Object mod) {
        this.ahorro = (especifico.entAhorroProgramado)mod;
        this.usaldo = 0.0;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("cedula".equals(jrCampo.getName())) {
            valor = ahorro.getCedula();
        } else if("apellidonombre".equals(jrCampo.getName())) {
            valor = ahorro.getApellidoNombre();
        } else if("estado".equals(jrCampo.getName())) {
            valor = ahorro.getEstadoAhorro();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = ahorro.getNumeroOperacion();
        } else if("contrato".equals(jrCampo.getName())) {
            valor = ahorro.getContrato();
        } else if("plan".equals(jrCampo.getName())) {
            valor = ahorro.getPlan();
        } else if("plazo".equals(jrCampo.getName())) {
            valor = ahorro.getPlazo();
        } else if("importe".equals(jrCampo.getName())) {
            valor = ahorro.getImporte();
        } else if("moneda".equals(jrCampo.getName())) {
            valor = ahorro.getMoneda();
        } else if("tasainteres".equals(jrCampo.getName())) {
            valor = ahorro.getTasaInteres();
        } else if("fechainicio".equals(jrCampo.getName())) {
            valor = ahorro.getFechaPrimerVencimiento();
        } else if("promotor".equals(jrCampo.getName())) {
            valor = ahorro.getPromotor();
        } else if("regional".equals(jrCampo.getName())) {
            valor = ahorro.getRegional();
        } else if("fechaanulacion".equals(jrCampo.getName())) {
            valor = ahorro.getFechaAnulado();
        } else if("motivoanulacion".equals(jrCampo.getName())) {
            valor = ahorro.getObservacionAnulado();
        }
        return valor;
    }
    
}
