/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsOperacionFijaRegionEstado implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet operacion;
    private double usaldo;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsOperacionFijaRegionEstado(Object mod) {
        this.operacion = (java.sql.ResultSet)mod;
        this.usaldo = 0.0;
        try {
            this.operacion.last();
            total = this.operacion.getRow()+1;
            this.operacion.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.operacion.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedula".equals(jrCampo.getName())) {
                valor = operacion.getString("cedula");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = operacion.getString("apellidonombre");
            } else if("estado".equals(jrCampo.getName())) {
                valor = operacion.getString("estado");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = operacion.getInt("numerooperacion");
            } else if("promotor".equals(jrCampo.getName())) {
                valor = operacion.getString("promotor");
            } else if("regional".equals(jrCampo.getName())) {
                valor = operacion.getString("regional");
            } else if("tipocuenta".equals(jrCampo.getName())) {
                valor = operacion.getString("tipocuenta");
            } else if("cuenta".equals(jrCampo.getName())) {
                valor = operacion.getString("cuenta");
            } else if("plazo".equals(jrCampo.getName())) {
                valor = operacion.getInt("plazo");
            } else if("importe".equals(jrCampo.getName())) {
                valor = operacion.getDouble("importe");
            } else if("importetotal".equals(jrCampo.getName())) {
                valor = operacion.getDouble("importetotal");
            } else if("moneda".equals(jrCampo.getName())) {
                valor = operacion.getString("moneda");
            } else if("fechaoperacion".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechaoperacion");
            } else if("fechainicio".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechaprimervencimiento");
            } else if("fechavencimiento".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechavencimiento");
            } else if("pagado".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montocapital")-operacion.getDouble("saldocapital");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
