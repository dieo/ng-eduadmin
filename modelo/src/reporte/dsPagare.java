/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPagare implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entMovimiento orden;
    private especifico.entSocio socio;
    private especifico.modMovimientoGarante garante;
    private int index = -1;

    public dsPagare(Object mod, Object mod1, Object mod2) {
        this.orden = (especifico.entMovimiento)mod;
        this.socio = (especifico.entSocio)mod1;
        this.garante = (especifico.modMovimientoGarante)mod2;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("numerosolicitud".equals(jrCampo.getName())) {
            valor = this.orden.getNumeroSolicitud();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = this.orden.getNumeroOperacion();
        } else if("fechaoperacion".equals(jrCampo.getName())) {
            valor = this.orden.getFechaGeneracion();
        } else if("fechavencimiento".equals(jrCampo.getName())) {
            valor = this.orden.getFechaVencimiento();
        } else if("textoprincipal".equals(jrCampo.getName())) {
            double utotal = this.orden.getMontoAprobado()+this.orden.getMontoInteres();
            String sletra = new utilitario.utiLetra().leerNumero(utotal).trim().toLowerCase();
            valor = "Pagaré(mos) a la orden de la Mutual Nacional de Funcionarios del Ministerio de Salud Pública y Bienestar Social, ";
            valor += "sin protesto y solidariamente en su domicilio, sito en Brasil Nº 999 esq. Tte. Fariña, la suma de Gs. ";
            valor += new java.text.DecimalFormat("###,###,###,###").format(utotal)+" ";
            valor += "(" + sletra.substring(0,1).toUpperCase()+sletra.substring(1,sletra.length()) + " Guaraníes) ";
            valor += "por igual valor recibido en efectivo a mi (nuestra) entrega satisfacción. Expreso(amos) mi (nuestra) conformidad a todos los ";
            valor += "recaudos solicitados por la Mutual y los siguientes puntos que se enumera: ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
        } else if("nombreapellidosocio".equals(jrCampo.getName())) {
            valor = this.socio.getNombre()+" "+this.socio.getApellido();
        } else if("direccionsocio".equals(jrCampo.getName())) {
            valor = this.socio.getDireccion() + " Nº " + this.socio.getNumeroCasa();
        } else if("cedulasocio".equals(jrCampo.getName())) {
            valor = this.socio.getCedula();
        } else if("nombreapellidogarante1".equals(jrCampo.getName())) {
            try                 { valor = this.garante.getEntidad(0).getNombre()+" "+this.garante.getEntidad(0).getApellido(); }
            catch (Exception e) { valor = ""; }
        } else if("direcciongarante1".equals(jrCampo.getName())) {
            try                 { valor = this.garante.getEntidad(0).getDireccion(); }
            catch (Exception e) { valor = ""; }
        } else if("cedulagarante1".equals(jrCampo.getName())) {
            try                 { valor = this.garante.getEntidad(0).getCedula(); }
            catch (Exception e) { valor = ""; }
        } else if("nombreapellidogarante2".equals(jrCampo.getName())) {
            try                 { valor = this.garante.getEntidad(1).getNombre()+" "+this.garante.getEntidad(1).getApellido(); }
            catch (Exception e) { valor = ""; }
        } else if("direcciongarante2".equals(jrCampo.getName())) {
            try                 { valor = this.garante.getEntidad(1).getDireccion(); }
            catch (Exception e) { valor = ""; }
        } else if("cedulagarante2".equals(jrCampo.getName())) {
            try                 { valor = this.garante.getEntidad(1).getCedula(); }
            catch (Exception e) { valor = ""; }
        } else if("nombreapellidogarante3".equals(jrCampo.getName())) {
            try                 { valor = this.garante.getEntidad(2).getNombre()+" "+this.garante.getEntidad(2).getApellido(); }
            catch (Exception e) { valor = ""; }
        } else if("direcciongarante3".equals(jrCampo.getName())) {
            try                 { valor = this.garante.getEntidad(2).getDireccion(); }
            catch (Exception e) { valor = ""; }
        } else if("cedulagarante3".equals(jrCampo.getName())) {
            try                 { valor = this.garante.getEntidad(2).getCedula(); }
            catch (Exception e) { valor = ""; }
        }
        return valor;
    }
    
}
