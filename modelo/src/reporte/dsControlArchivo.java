/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsControlArchivo implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet archivo;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsControlArchivo(Object mod) {
        this.archivo = (java.sql.ResultSet)mod;
        try {
            this.archivo.last();
            total = this.archivo.getRow()+1;
            this.archivo.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.archivo.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = archivo.getInt("numerooperacion");
            } else if("fechaproceso".equals(jrCampo.getName())) {
                valor = archivo.getDate("fechaproceso");
            } else if("fechasolicitud".equals(jrCampo.getName())) {
                valor = archivo.getDate("fechasolicitud");
            } else if("fechaaprobacion".equals(jrCampo.getName())) {
                valor = archivo.getDate("fechaaprobacion");
            } else if("fechavencimiento".equals(jrCampo.getName())) {
                valor = archivo.getDate("fechavencimiento");
            } else if("fechaalta".equals(jrCampo.getName())) {
                valor = archivo.getDate("fechaalta");
            } else if("fechacancela".equals(jrCampo.getName())) {
                valor = archivo.getDate("fechacancela");
            } else if("fechabaja".equals(jrCampo.getName())) {
                valor = archivo.getDate("fechabaja");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = archivo.getString("cedula");
            } else if("nombreapellido".equals(jrCampo.getName())) {
                valor = archivo.getString("nombre")+" "+archivo.getString("apellido");
            } else if("monto".equals(jrCampo.getName())) {
                valor = archivo.getDouble("monto");
            } else if("tipoprestamo".equals(jrCampo.getName())) {
                valor = archivo.getString("tipoprestamo");
            } else if("region".equals(jrCampo.getName())) {
                valor = archivo.getString("region");
            } else if("ubicacion".equals(jrCampo.getName())) {
                valor = archivo.getString("ubicacion");
            } else if("tipooperacion".equals(jrCampo.getName())) {
                valor = archivo.getString("tipooperacion");
            } else if("tipodocumento".equals(jrCampo.getName())) {
                valor = archivo.getString("tipodocumento");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
