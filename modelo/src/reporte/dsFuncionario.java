/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsFuncionario implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modFuncionario mod;
    private int index = -1;

    public dsFuncionario(Object mod) {
        this.mod = (especifico.modFuncionario)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("nombre".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNombre();
        } else if("apellido".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getApellido();
        } else if("tipo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTipoFuncionario();
        } else if("cargo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCargo();
        } else if("estado".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getEstado();
        }
        return valor;
    }
    
}
