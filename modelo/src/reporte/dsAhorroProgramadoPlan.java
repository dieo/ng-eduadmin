/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAhorroProgramadoPlan implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modAhorroProgramadoPlan mod;
    private int index = -1;

    public dsAhorroProgramadoPlan(Object mod) {
        this.mod = (especifico.modAhorroProgramadoPlan)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("descripcion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDescripcion();
        } else if("importebase".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getImporteBase();
        } else if("moneda".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMoneda();
        } else if("plazo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getPlazo();
        } else if("tasainteres".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTasaInteres();
        } else if("estado".equals(jrCampo.getName())) {
            if (this.mod.getEntidad(index).getActivo()) valor = "ACTIVO";
            else valor = "";
        }
        return valor;
    }
    
}
