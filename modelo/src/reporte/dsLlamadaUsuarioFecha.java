/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsLlamadaUsuarioFecha implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modLlamada mod;
    private int index = -1;

    public dsLlamadaUsuarioFecha(Object mod) {
        this.mod = (especifico.modLlamada)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("usuario".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getUsuarioNombre()+" "+mod.getEntidad(index).getUsuarioApellido();
        } else if("cedula".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCedula();
        } else if("apellidonombre".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNombreApellido();
        } else if("fecha".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaLlamada();
        } else if("hora".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getHoraLlamada();
        } else if("duracion".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getDuracion();
        } else if("telefonoorigen".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getTelefonoOrigen();
        } else if("telefonodestino".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getTelefonoDestino();
        } else if("objeto".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getObjetoLlamada();
        } else if("solucion".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getSolucionLlamada();
        } else if("observacion".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getObservacion();
        }
        return valor;
    }
    
}
