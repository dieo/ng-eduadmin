/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsListadoComercios implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0;
    private int totalDetalle = 0;

    public dsListadoComercios(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            totalDetalle = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { listado.next();
        } catch (Exception e) { }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if ("comercio".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("comercio"));
            } else if ("direccion".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("direccion"));
            } else if ("telefono".equals(jrCampo.getName())) {
                valor = listado.getString("telefono");
            } else if ("ramo".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("ramo"));
            } else if ("plazomaximo".equals(jrCampo.getName())) {
                valor = listado.getInt("plazomaximo");
            } else if ("ciudad".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("ciudad"));
            } else if ("regional".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("regional"));    
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
