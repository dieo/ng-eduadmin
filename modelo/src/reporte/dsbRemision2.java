/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbRemision2 implements net.sf.jasperreports.engine.JRDataSource {

    private final especifico.entbRemision entRemision;
    private final especifico.modbRemisionDetalle modRemisionDetalle;
    private int index = -1;

    public dsbRemision2(Object mod, Object mod2) {
        this.entRemision = (especifico.entbRemision)mod;
        this.modRemisionDetalle = (especifico.modbRemisionDetalle)mod2;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.modRemisionDetalle.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("referente".equals(jrCampo.getName())) {
            valor = entRemision.getDestino();
        } else if("promotor".equals(jrCampo.getName())) {
            valor = entRemision.getPromotor();
        } else if("numeroremision".equals(jrCampo.getName())) {
            valor = entRemision.getId()+"/"+utilitario.utiFecha.getAnho(entRemision.getFechaDesde());
        } else if("codigo".equals(jrCampo.getName())) {
            valor = entRemision.getCodigo();
        } else if("courrier".equals(jrCampo.getName())) {
            valor = entRemision.getIdCourier();
        } else if("numerodocumento".equals(jrCampo.getName())) {
            valor = modRemisionDetalle.getEntidad(index).getNumeroDocumento();
        } else if("tipodocumento".equals(jrCampo.getName())) {
            valor = modRemisionDetalle.getEntidad(index).getTipoDocumento();
        } else if("banco".equals(jrCampo.getName())) {
            valor = modRemisionDetalle.getEntidad(index).getBanco();
        } else if("monto".equals(jrCampo.getName())) {
            valor = modRemisionDetalle.getEntidad(index).getMonto();
        } else if("numeronota".equals(jrCampo.getName())) {
            valor = modRemisionDetalle.getEntidad(index).getNumeroNota();
        } else if("fechaenvio".equals(jrCampo.getName())) {  
            valor = entRemision.getFechaEnvio();
        } else if("fechadesde".equals(jrCampo.getName())) {  
            valor = entRemision.getFechaDesde();
        } else if("fechahasta".equals(jrCampo.getName())) {
            valor = entRemision.getFechaHasta();
        } else if("estado".equals(jrCampo.getName())) {
            valor = entRemision.getEstado();
        } else if("concepto".equals(jrCampo.getName())) {
            valor = modRemisionDetalle.getEntidad(index).getDescripcion();
        } else if("legajo".equals(jrCampo.getName())) {
            if(modRemisionDetalle.getEntidad(index).getLegajoCompleto()) valor = "X";
            else valor = "";
        } 
        return valor;
    }
    
}
