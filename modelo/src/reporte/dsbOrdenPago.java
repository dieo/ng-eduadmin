/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbOrdenPago implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet OrdenPago;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsbOrdenPago(Object mod) {
        this.OrdenPago = (java.sql.ResultSet)mod;
        try {
            this.OrdenPago.last();
            total = this.OrdenPago.getRow()+1;
            this.OrdenPago.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    @Override
    
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.OrdenPago.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("numeroorden".equals(jrCampo.getName())) {
                valor = OrdenPago.getInt("nroordenpago");
            } else if("fechaorden".equals(jrCampo.getName())) {
                valor = OrdenPago.getDate("fecha");
            } else if("numerosolicitud".equals(jrCampo.getName())) {
                valor = OrdenPago.getInt("nrosolicitud");
            } else if("receptor".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("receptor");
            } else if("numerocuenta".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("numerocuenta");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("cedularuc");
            } else if("remision".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("remision");
            } else if("numeroplancuentaop".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("nroplancuentaop");
            } else if("nombrecuentaop".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getString("nombrecuentaop");
            } else if("explicacionop".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getString("descripcion");
            } else if("importeop".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getDouble("montodetalleop");
            } else if("numeroplancuentacheque".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("nroplancuentacheque");
            } else if("nombrecuentacheque".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getString("nombrecuentacheque");
            } else if("explicacioncheque".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getString("explicacioncheque");
            } else if("filial".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getString("filial");
            } else if("importecheque".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getDouble("montocheque");
            } else if("montoletras".equals(jrCampo.getName())) {
                String letra = "RECIBI DE LA MUTUAL NACIONAL DE FUNCIONARIOS DEL M.S.P. y B.S., LA SUMA DE GUARANÍES: ";
                letra += new utilitario.utiLetra().leerNumero(this.OrdenPago.getDouble("montocheque"));
                valor = letra+ ".- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
            } 
        } catch (Exception e) { 
        }
        return valor;
    }
    
}
