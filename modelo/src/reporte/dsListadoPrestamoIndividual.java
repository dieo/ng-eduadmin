/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsListadoPrestamoIndividual implements net.sf.jasperreports.engine.JRDataSource {
    
    private java.sql.ResultSet detalle;
    private int index = 0;
    private int totalDetalle = 0;

   
    public dsListadoPrestamoIndividual(Object mod) {
        this.detalle = (java.sql.ResultSet)mod;
        try {
            this.detalle.last();
            totalDetalle = this.detalle.getRow()+1;
            this.detalle.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { detalle.next();
        } catch (Exception e) { }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if ("cedula".equals(jrCampo.getName())) {
                valor =detalle.getString("cedula");
            } else if ("apellidonombre".equals(jrCampo.getName())) {
                valor = detalle.getString("apellidonombre");
            } else if ("giraduria".equals(jrCampo.getName())) {
                valor = detalle.getString("giraduria");
            } else if ("rubro".equals(jrCampo.getName())) {
                valor = detalle.getString("rubro");
            } else if ("regional".equals(jrCampo.getName())) {
                valor = detalle.getString("regional");
            } else if ("numerosolicitud".equals(jrCampo.getName())) {
                valor = detalle.getInt("numerosolicitud");   
            } else if ("fechahorasolicitud".equals(jrCampo.getName())) {
                if (detalle.getString("horasolicitud")==null) {
                    valor = utilitario.utiFecha.convertirToStringDMACorto(detalle.getDate("fechasolicitud"))+" 00:00";
                } else {
                    valor = utilitario.utiFecha.convertirToStringDMACorto(detalle.getDate("fechasolicitud"))+ " " +detalle.getString("horasolicitud").substring(0,5);
                }                    
            } else if ("numerooperacion".equals(jrCampo.getName()))  {
                valor = detalle.getInt("numerooperacion");
            } else if ("fechahoraaprobado".equals(jrCampo.getName())) {
                if (detalle.getString("horaaprobado")==null) {
                    valor = utilitario.utiFecha.convertirToStringDMACorto(detalle.getDate("fechaaprobado"))+" 00:00";
                } else {
                    valor = utilitario.utiFecha.convertirToStringDMACorto(detalle.getDate("fechaaprobado"))+ " " +detalle.getString("horaaprobado").substring(0,5);
                }                       
            } else if ("cancelacion".equals(jrCampo.getName())) {
                if (detalle.getBoolean("cancelacion")==true) valor = "SI";
                else valor = "";
            } else if ("plazo".equals(jrCampo.getName())) {
                valor = detalle.getInt("plazo");      
            } else if ("monto".equals(jrCampo.getName())) {
                valor = detalle.getDouble("monto");
            } else if ("montocancelado".equals(jrCampo.getName())) {
                valor = detalle.getDouble("montocancelado");   
            } else if ("desembolso".equals(jrCampo.getName())) {
                valor = detalle.getDouble("desembolso");   
            } else if ("fechahoracheque".equals(jrCampo.getName())) {
                if (detalle.getString("horacheque")==null) {
                    valor = utilitario.utiFecha.convertirToStringDMACorto(detalle.getDate("fechacheque"))+" 00:00";
                } else {
                    valor = utilitario.utiFecha.convertirToStringDMACorto(detalle.getDate("fechacheque"))+ " " +detalle.getString("horacheque").substring(0,5);
                }
            }  else if ("tipocredito".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(detalle.getString("tipocredito"));
            } else if ("fuentefinanciamiento".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(detalle.getString("fuentefinanciamiento"));
            } else if ("entidad".equals(jrCampo.getName()))  {
                valor = utilitario.utiCadena.convertirMayusMinus(detalle.getString("entidad"));    
            } else if ("viacobro".equals(jrCampo.getName())) {
                valor = detalle.getString("viacobro");
            } else if ("estado".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(detalle.getString("estado"));
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
