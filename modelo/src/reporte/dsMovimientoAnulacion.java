/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsMovimientoAnulacion implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entMovimiento movimiento;
    private especifico.modMovimientoGarante garante;
    private int index = -1;

    public dsMovimientoAnulacion(Object mod, Object mod2) {
        this.movimiento = (especifico.entMovimiento)mod;
        this.garante = (especifico.modMovimientoGarante)mod2;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("cedula".equals(jrCampo.getName())) {
            valor = movimiento.getCedula();
        } else if("nombreapellido".equals(jrCampo.getName())) {
            valor = movimiento.getNombreApellido();
        } else if("comercio".equals(jrCampo.getName())) {
            valor = movimiento.getEntidad();
        } else if("numerosolicitud".equals(jrCampo.getName())) {
            valor = movimiento.getNumeroSolicitud();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = movimiento.getNumeroOperacion();
        } else if("estado".equals(jrCampo.getName())) {
            valor = movimiento.getEstadoSolicitud();
        } else if("plazosolicitado".equals(jrCampo.getName())) {
            valor = movimiento.getPlazoSolicitud();
        } else if("plazoaprobado".equals(jrCampo.getName())) {
            valor = movimiento.getPlazoAprobado();
        } else if("importesolicitado".equals(jrCampo.getName())) {
            valor = movimiento.getMontoSolicitud();
        } else if("importeaprobado".equals(jrCampo.getName())) {
            valor = movimiento.getMontoAprobado();
        } else if("moneda".equals(jrCampo.getName())) {
            valor = movimiento.getMoneda();
        } else if("fechainicio".equals(jrCampo.getName())) {
            valor = movimiento.getFechaPrimerVencimiento();
        } else if("fechavencimiento".equals(jrCampo.getName())) {
            valor = movimiento.getFechaVencimiento();
        } else if("fechaanulacion".equals(jrCampo.getName())) {
            valor = movimiento.getFechaAnulado();
        } else if("observacionanulacion".equals(jrCampo.getName())) {
            valor = movimiento.getObservacionAnulado();
        } else if("codeudor1".equals(jrCampo.getName())) {
            if (garante.getRowCount()==1 || garante.getRowCount()==2) {
                int ipos = garante.getEntidad(0).getApellidoNombre().indexOf(",");
                String snombre = garante.getEntidad(0).getApellidoNombre().substring(ipos+2,garante.getEntidad(0).getApellidoNombre().length());
                String sapellido = garante.getEntidad(0).getApellidoNombre().substring(0,ipos);
                valor = utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(0).getCedula(),0);
            }
        } else if("codeudor2".equals(jrCampo.getName())) {
            if (garante.getRowCount()==2) {
                int ipos = garante.getEntidad(1).getApellidoNombre().indexOf(",");
                String snombre = garante.getEntidad(1).getApellidoNombre().substring(ipos+2,garante.getEntidad(1).getApellidoNombre().length());
                String sapellido = garante.getEntidad(1).getApellidoNombre().substring(0,ipos);
                valor = utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(1).getCedula(),0);
            }
        }
        return valor;
    }
    
}
