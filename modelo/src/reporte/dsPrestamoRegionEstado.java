/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPrestamoRegionEstado implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet operacion;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsPrestamoRegionEstado(Object mod) {
        this.operacion = (java.sql.ResultSet)mod;
        try {
            this.operacion.last();
            total = this.operacion.getRow()+1;
            this.operacion.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.operacion.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedula".equals(jrCampo.getName())) {
                valor = operacion.getString("cedula");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = operacion.getString("apellidonombre");
            } else if("estado".equals(jrCampo.getName())) {
                valor = operacion.getString("estado");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = operacion.getInt("numerooperacion");
            } else if("regional".equals(jrCampo.getName())) {
                valor = operacion.getString("regional");
            } else if("tipocredito".equals(jrCampo.getName())) {
                valor = operacion.getString("tipocredito");
            } else if("fuentefinanciera".equals(jrCampo.getName())) {
                valor = operacion.getString("entidad");
            } else if("destino".equals(jrCampo.getName())) {
                valor = operacion.getString("destino");
            } else if("plazo".equals(jrCampo.getName())) {
                valor = operacion.getInt("plazoaprobado");
            } else if("importe".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montoaprobado");
            } else if("moneda".equals(jrCampo.getName())) {
                valor = operacion.getString("moneda");
            } else if("fechaoperacion".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechaaprobado");
            } else if("fechainicio".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechaprimervencimiento");
            } else if("fechavencimiento".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechavencimiento");
            } else if("compromiso".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montocapital")+operacion.getDouble("montointeres");
            } else if("pagado".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montocapital")-operacion.getDouble("saldocapital");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
