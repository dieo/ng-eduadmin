/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsDetalleIngreso implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsDetalleIngreso(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedularuc".equals(jrCampo.getName())) {
                valor = listado.getString("cedula");
            } else if("razonsocial".equals(jrCampo.getName())) {
                valor = listado.getString("razonsocial");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = listado.getDate("fecha");
            } else if("ingreso".equals(jrCampo.getName())) {
                valor = listado.getInt("ingreso");
            } else if("viacobro".equals(jrCampo.getName())) {
                valor = listado.getString("viacobro");
            } else if("valor".equals(jrCampo.getName())) {
                valor = listado.getString("valor");
            } else if("idcuenta".equals(jrCampo.getName())) {
                valor = listado.getInt("idcuenta");
            } else if("cuenta".equals(jrCampo.getName())) {
                valor = listado.getString("cuenta");
            } else if("entidad".equals(jrCampo.getName())) {
                valor = listado.getString("entidad");
            } else if("cuota".equals(jrCampo.getName())) {
                valor = listado.getInt("cuota");
            } else if("plazo".equals(jrCampo.getName())) {
                valor = listado.getInt("plazo");
            } else if("vencimiento".equals(jrCampo.getName())) {
                valor = listado.getDate("fechavencimiento");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = listado.getInt("numerooperacion");
            } else if("montocuota".equals(jrCampo.getName())) {
                valor = listado.getDouble("montocuota");
            } else if("montocobro".equals(jrCampo.getName())) {
                valor = listado.getDouble("montocobro");
            } else if("montoexoneracion".equals(jrCampo.getName())) {
                valor = listado.getDouble("montoexoneracion");
            } else if("estado".equals(jrCampo.getName())) {
                valor = listado.getString("estado");
            } else if("diasatraso".equals(jrCampo.getName())) {
                valor = listado.getInt("diasatraso");
            } else if("calificacion".equals(jrCampo.getName())) {
                valor = listado.getString("calificacion");
            } else if("totaldiasatraso".equals(jrCampo.getName())) {
                valor = listado.getInt("totaldiasatraso");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = listado.getInt("cantidad");
            }
        } catch (Exception e) { }
        return valor;
    }
}
