/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsCasoJuridico implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modCasoJuridico mod;
    private int index = -1;

    public dsCasoJuridico(Object mod) {
        this.mod = (especifico.modCasoJuridico)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("ciudad".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCiudad();
        } else if("estadojuridico".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getEstadoJuridico();
        } else if("numerocaso".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNumeroCaso();
        } else if("fecha".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFecha();
        } else if("cedula".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCedula();
        } else if("apellidonombre".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getApellido()+", "+mod.getEntidad(index).getNombre();
        } else if("telefono".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getTelefono();
        } else if("juzgado".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getJuzgado();
        } else if("caso".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCaso();
        }
        return valor;
    }
    
}
