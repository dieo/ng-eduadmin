/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dscTransferencias implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modcTransferencias mod;
    private int index = -1;

    public dscTransferencias(Object mod) {
        this.mod = (especifico.modcTransferencias)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("id".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getId();
        } else if("fecha".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFecha();
        } else if("estado".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getEstado();
        } else if("descripcion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDescripcion();
        } else if("origen".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFilialOrigen();
        } else if("destino".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFilialDestino();
        } else if("cantidad".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCantidad();
        }  
        return valor;
    }
    
}
