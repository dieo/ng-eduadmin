/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsEntidad implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modEntidad mod;
    private int index = -1;

    public dsEntidad(Object mod) {
        this.mod = (especifico.modEntidad)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("ruc".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getRuc();
        } else if("descripcion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDescripcion();
        } else if("estado".equals(jrCampo.getName())) {
            if (this.mod.getEntidad(index).getActivo()) valor = "ACTIVO";
            else valor = "";
        } else if("direccion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDireccion();
        } else if("ciudad".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCiudad();
        } else if("telefono".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTelefono();
        } else if("tipoentidad".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTipoEntidad();
        } else if("ramo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getRamo();
        } else if("plazo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getPlazoMaximo();
        } else if("bonificacion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getBonificacion();
        } else if("tasa".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTasaInteres();
        }
        return valor;
    }
    
}
