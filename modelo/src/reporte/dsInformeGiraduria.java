/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsInformeGiraduria implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsInformeGiraduria(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("beneficiario".equals(jrCampo.getName())) {
                valor = listado.getInt("beneficiario");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = listado.getString("cedula");
            } else if("nombre".equals(jrCampo.getName())) {
                valor = listado.getString("nombre");
            } else if("apellido".equals(jrCampo.getName())) {
                valor = listado.getString("apellido");
            } else if("monto".equals(jrCampo.getName())) {
                valor = listado.getDouble("monto");
            } 
        } catch (Exception e) { }
        return valor;
    }
    
}
