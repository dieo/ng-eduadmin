/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsListadoJubilados implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private String scantidad = "";
    private String sdescripcion = "";
    private String spreciounitario = "";
    private String ssubtotal = "";
    private double utotalsubtotal = 0.0;
    private int index = 0;
    private int totalDetalle = 0;

    public dsListadoJubilados(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            totalDetalle = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { listado.next();
        } catch (Exception e) { }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if ("cedula".equals(jrCampo.getName())) {
                valor =listado.getString("cedula");
            } else if ("apellidonombre".equals(jrCampo.getName())) {
                valor = listado.getString("apellido")+", "+listado.getString("nombre");
            } else if ("nrobeneficiario".equals(jrCampo.getName())) {
                valor = listado.getString("numerobeneficiario");
            } else if ("estado".equals(jrCampo.getName())) {
                valor = listado.getString("estado");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
