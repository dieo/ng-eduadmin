/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAuditoria implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet auditoria;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsAuditoria(Object mod) {
        this.auditoria = (java.sql.ResultSet)mod;
        try {
            this.auditoria.last();
            total = this.auditoria.getRow()+1;
            this.auditoria.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.auditoria.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("usuario".equals(jrCampo.getName())) {
                valor = auditoria.getString("usuario");
            } else if("tabla".equals(jrCampo.getName())) {
                valor = auditoria.getString("nombretabla");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = auditoria.getString("fechaactualizacion");
            } else if("operacion".equals(jrCampo.getName())) {
                valor = auditoria.getString("operacion");
            } else if("valoranterior".equals(jrCampo.getName())) {
                valor = auditoria.getString("valorviejo");
            } else if("valornuevo".equals(jrCampo.getName())) {
                valor = auditoria.getString("valornuevo");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
