/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dscPresupuesto implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet Presupuesto;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dscPresupuesto(Object mod) {
        this.Presupuesto = (java.sql.ResultSet)mod;
        try {
            this.Presupuesto.last();
            total = this.Presupuesto.getRow()+1;
            this.Presupuesto.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    @Override
    
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.Presupuesto.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("numeropresupuesto".equals(jrCampo.getName())) {
                valor = Presupuesto.getInt("id");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = Presupuesto.getDate("fecha");
            } else if("proveedor".equals(jrCampo.getName())) {
                valor = Presupuesto.getString("proveedor");
            } else if("validez".equals(jrCampo.getName())) {
                valor = Presupuesto.getInt("validez");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = Presupuesto.getDouble("cantidad");
            } else if("costo".equals(jrCampo.getName())) {
                valor = Presupuesto.getDouble("costo");
            } else if("total".equals(jrCampo.getName())) {
                valor = Presupuesto.getDouble("total");
            } else if("cambio".equals(jrCampo.getName())) {
                valor = Presupuesto.getDouble("cambio");
            } else if("descripcion".equals(jrCampo.getName())) {
                valor = Presupuesto.getString("descripcion");
            } else if("moneda".equals(jrCampo.getName())) {
                valor = Presupuesto.getString("moneda");
            } else if("idarticulo".equals(jrCampo.getName())) {
                valor = Presupuesto.getInt("idarticulo");
            } 
        } catch (Exception e) { 
        }
        return valor;
    }
    
}
