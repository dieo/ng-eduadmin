/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dscInventario implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet Inventario;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dscInventario(Object mod) {
        this.Inventario = (java.sql.ResultSet)mod;
        try {
            this.Inventario.last();
            total = this.Inventario.getRow()+1;
            this.Inventario.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    @Override
    
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.Inventario.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("numeroinventario".equals(jrCampo.getName())) {
                valor = Inventario.getInt("id");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = Inventario.getDate("fecha");
            } else if("anho".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.getAnho(Inventario.getDate("fecha"));
            } else if("estado".equals(jrCampo.getName())) {
                valor = Inventario.getString("estadoinventario");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = Inventario.getString("apellidonombre")+" - "+Inventario.getString("dependencia");
            } else if("filial".equals(jrCampo.getName())) {
                valor = Inventario.getString("filial");
            } else if("existencia".equals(jrCampo.getName())) {
                valor = Inventario.getDouble("existenciafisica");
            } else if("stock".equals(jrCampo.getName())) {
                valor = Inventario.getDouble("stockactual");
            } else if("diferencia".equals(jrCampo.getName())) {
                valor = Inventario.getDouble("diferencia");
            } else if("costo".equals(jrCampo.getName())) {
                valor = Inventario.getDouble("costo");
            } else if("descripcion".equals(jrCampo.getName())) {
                valor = Inventario.getString("articulo");
            } else if("observacion".equals(jrCampo.getName())) {
                valor = Inventario.getString("observacion");
            } else if("idarticulo".equals(jrCampo.getName())) {
                valor = Inventario.getInt("idarticulo");
            } 
        } catch (Exception e) { 
        }
        return valor;
    }
    
}
