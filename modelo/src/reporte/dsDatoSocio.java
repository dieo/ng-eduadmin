/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsDatoSocio implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entSocio socio;
    private especifico.modLugarLaboral labor;
    private especifico.modBeneficiario beneficiario;
    private int index = -1;

    public dsDatoSocio(Object mod, Object mod2, Object mod3) {
        this.socio = (especifico.entSocio)mod;
        this.labor = (especifico.modLugarLaboral)mod2;
        this.beneficiario = (especifico.modBeneficiario)mod3;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("fechaactualizacion".equals(jrCampo.getName())) {
                java.util.Date dfecha = new java.util.Date();
                int ano =  dfecha.getYear()+1900;
                valor = "Asunción, "+dfecha.getDate()+" de "+utilitario.utiFecha.getMeses()[dfecha.getMonth()].toLowerCase()+" de "+ano;
            } else if("solicitante".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(socio.getNombre().toLowerCase() + " " + socio.getApellido().toLowerCase())+" - C.I.C. Nº "+socio.getCedula();
            } else if ("cedula".equals(jrCampo.getName())) {
                valor = socio.getCedula();
            } else if ("nombreapellido".equals(jrCampo.getName())) {
                valor = socio.getNombre() + " " + socio.getApellido();
            } else if ("sexo".equals(jrCampo.getName())) {
                valor = socio.getSexo();
            } else if ("direccion".equals(jrCampo.getName())) {
                valor = socio.getDireccion();
            } else if ("numerocasa".equals(jrCampo.getName())) {
                valor = socio.getNumeroCasa();
            } else if ("barrio".equals(jrCampo.getName())) {
                valor = socio.getBarrio();
            } else if ("ciudadresidencia".equals(jrCampo.getName())) {
                valor = socio.getCiudadResidencia();
            } else if ("telefonocelular".equals(jrCampo.getName())) {
                valor = socio.getTelefonoCelularPrincipal()+"/"+socio.getTelefonoCelular();
            } else if ("telefonolineabaja".equals(jrCampo.getName())) {
                valor = socio.getTelefonoLineaBaja();
            } else if ("fechanacimiento".equals(jrCampo.getName())) {
                valor = socio.getFechaNacimiento();
            } else if ("lugarnacimiento".equals(jrCampo.getName())) {
                valor = socio.getLugarNacimiento();
            } else if ("estadocivil".equals(jrCampo.getName())) {
                valor = socio.getEstadoCivil();
            } else if ("profesion".equals(jrCampo.getName())) {
                valor = socio.getProfesion();
            } else if ("email".equals(jrCampo.getName())) {
                valor = socio.getEmail();
            } else if ("tipocasa".equals(jrCampo.getName())) {
                valor = socio.getTipoCasa();
            } else if ("institucion1".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(0).getInstitucion();
            } else if ("institucion2".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(1).getInstitucion();
            } else if ("institucion3".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(2).getInstitucion();
            } else if ("institucion4".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(3).getInstitucion();
            } else if ("institucion5".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(4).getInstitucion();
            } else if ("regional1".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(0).getRegional();
            } else if ("regional2".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(1).getRegional();
            } else if ("regional3".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(2).getRegional();
            } else if ("regional4".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(3).getRegional();
            } else if ("regional5".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(4).getRegional();
            } else if ("rubro1".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(0).getRubro();
            } else if ("rubro2".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(1).getRubro();
            } else if ("rubro3".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(2).getRubro();
            } else if ("rubro4".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(3).getRubro();
            } else if ("rubro5".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(4).getRubro();
            } else if ("giraduria1".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(0).getGiraduria();
            } else if ("giraduria2".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(1).getGiraduria();
            } else if ("giraduria3".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(2).getGiraduria();
            } else if ("giraduria4".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(3).getGiraduria();
            } else if ("giraduria5".equals(jrCampo.getName())) {
                valor = "---";
                valor = labor.getEntidad(4).getGiraduria();
            } else if ("sueldo1".equals(jrCampo.getName())) {
                valor = 0.0;
                valor = labor.getEntidad(0).getSueldo();
            } else if ("sueldo2".equals(jrCampo.getName())) {
                valor = 0.0;
                valor = labor.getEntidad(1).getSueldo();
            } else if ("sueldo3".equals(jrCampo.getName())) {
                valor = 0.0;
                valor = labor.getEntidad(2).getSueldo();
            } else if ("sueldo4".equals(jrCampo.getName())) {
                valor = 0.0;
                valor = labor.getEntidad(3).getSueldo();
            } else if ("sueldo5".equals(jrCampo.getName())) {
                valor = 0.0;
                valor = labor.getEntidad(4).getSueldo();
            } else if ("activo1".equals(jrCampo.getName())) {
                valor = "-";
                if (labor.getEntidad(0).getActivo()) valor = "√";
            } else if ("activo2".equals(jrCampo.getName())) {
                valor = "-";
                if (labor.getEntidad(1).getActivo()) valor = "√";
            } else if ("activo3".equals(jrCampo.getName())) {
                valor = "-";
                if (labor.getEntidad(2).getActivo()) valor = "√";
            } else if ("activo4".equals(jrCampo.getName())) {
                valor = "-";
                if (labor.getEntidad(3).getActivo()) valor = "√";
            } else if ("activo5".equals(jrCampo.getName())) {
                valor = "-";
                if (labor.getEntidad(4).getActivo()) valor = "√";
            } else if ("cedula1".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(0).getCedula();
            } else if ("cedula2".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(1).getCedula();
            } else if ("cedula3".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(2).getCedula();
            } else if ("cedula4".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(3).getCedula();
            } else if ("cedula5".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(4).getCedula();
            } else if ("nombreapellido1".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(0).getNombre()+" "+beneficiario.getEntidad(0).getApellido();
            } else if ("nombreapellido2".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(1).getNombre()+" "+beneficiario.getEntidad(1).getApellido();
            } else if ("nombreapellido3".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(2).getNombre()+" "+beneficiario.getEntidad(2).getApellido();
            } else if ("nombreapellido4".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(3).getNombre()+" "+beneficiario.getEntidad(3).getApellido();
            } else if ("nombreapellido5".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(4).getNombre()+" "+beneficiario.getEntidad(4).getApellido();
            } else if ("parentesco1".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(0).getParentesco();
            } else if ("parentesco2".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(1).getParentesco();
            } else if ("parentesco3".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(2).getParentesco();
            } else if ("parentesco4".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(3).getParentesco();
            } else if ("parentesco5".equals(jrCampo.getName())) {
                valor = "---";
                valor = beneficiario.getEntidad(4).getParentesco();
            }
        } catch (Exception e) {
            
        }
        return valor;
    }
    
}
