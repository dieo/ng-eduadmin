/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsIngresoIndividual implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modDetalleIngresoImpresion operacion;
    private int index = -1;

    public dsIngresoIndividual(Object mod) {
        this.operacion = (especifico.modDetalleIngresoImpresion)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.operacion.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("cedularuc".equals(jrCampo.getName())) {
            valor = operacion.getEntidad(index).getRuc();
        } else if("razonsocial".equals(jrCampo.getName())) {
            valor = operacion.getEntidad(index).getRazonSocial();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = operacion.getEntidad(index).getNumeroOperacion();
        } else if("fechaoperacion".equals(jrCampo.getName())) {
            valor = operacion.getEntidad(index).getFechaOperacion();
        } else if("estado".equals(jrCampo.getName())) {
            valor = utilitario.utiCadena.convertirMayusMinus(operacion.getEntidad(index).getEstado());
        } else if("importetotal".equals(jrCampo.getName())) {
            valor = operacion.getEntidad(index).getImporteTotal();
        } else if("cuenta".equals(jrCampo.getName())) {
            valor = utilitario.utiCadena.convertirMayusMinus(operacion.getEntidad(index).getCuenta());
        } else if("numeroplanilla".equals(jrCampo.getName())) {
            valor = operacion.getEntidad(index).getNumeroPlanilla();
        } else if("documento".equals(jrCampo.getName())) {
            valor = utilitario.utiCadena.convertirMayusMinus(operacion.getEntidad(index).getDocumento());
        } else if("numerodocumento".equals(jrCampo.getName())) {
            valor = operacion.getEntidad(index).getNumeroDocumento();
        } else if("importe".equals(jrCampo.getName())) {
            valor = operacion.getEntidad(index).getImporte();
        }
        return valor;
    }
    
}
