/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAhorroProgramadoLiquidacion implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entAhorroProgramado ahorro;
    private especifico.modAhorroProgramadoDetalle mod;
    private int index = -1;

    public dsAhorroProgramadoLiquidacion(Object mod, Object mod2) {
        this.ahorro = (especifico.entAhorroProgramado)mod;
        this.mod = (especifico.modAhorroProgramadoDetalle)mod2;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("cedula".equals(jrCampo.getName())) {
            valor = ahorro.getCedula();
        } else if("apellidonombre".equals(jrCampo.getName())) {
            valor = ahorro.getApellidoNombre();
        } else if("estado".equals(jrCampo.getName())) {
            valor = ahorro.getEstadoAhorro();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = ahorro.getNumeroOperacion();
        } else if("fechaoperacion".equals(jrCampo.getName())) {
            valor = ahorro.getFechaOperacion();
        } else if("contrato".equals(jrCampo.getName())) {
            valor = ahorro.getContrato();
        } else if("promotor".equals(jrCampo.getName())) {
            valor = ahorro.getPromotor();
        } else if("regional".equals(jrCampo.getName())) {
            valor = ahorro.getRegional();
        } else if("plan".equals(jrCampo.getName())) {
            valor = ahorro.getPlan();
        } else if("plazo".equals(jrCampo.getName())) {
            valor = ahorro.getPlazo();
        } else if("importe".equals(jrCampo.getName())) {
            valor = ahorro.getImporte();
        } else if("moneda".equals(jrCampo.getName())) {
            valor = ahorro.getMoneda();
        } else if("fechainicio".equals(jrCampo.getName())) {
            valor = ahorro.getFechaPrimerVencimiento();
        } else if("tasainteres".equals(jrCampo.getName())) {
            valor = ahorro.getTasaInteres();
        } else if("tasainteresretirado".equals(jrCampo.getName())) {
            valor = ahorro.getTasaInteresRetirado();
        } else if("observacionretirado".equals(jrCampo.getName())) {
            valor = ahorro.getObservacionRetirado();
        } else if("fecharetirado".equals(jrCampo.getName())) {
            valor = ahorro.getFechaRetirado();
        } else if("liquidacion".equals(jrCampo.getName())) {
            valor = ahorro.getMontoLiquidacion();
        } else if("dias".equals(jrCampo.getName())) {
            try {
                valor = ahorro.getFechaRetirado().getDate()-1;
            } catch(Exception e) {
                valor = 0;
            }
        } else if("interesfinal".equals(jrCampo.getName())) {
            valor = ahorro.getMontoInteres();
        } else if("gastoadministrativo".equals(jrCampo.getName())) {
            valor = ahorro.getGastoAdministrativo();
        } else if("montoretirado".equals(jrCampo.getName())) {
            valor = ahorro.getMontoRetirado();
        } else if("cuota".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCuota() + "/" + this.ahorro.getPlazo();
        } else if("fechavencimiento".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFechaVencimiento();
        } else if("cantidaddia".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCantidadDia();
        } else if("interes".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getInteres();
        } else if("importecuota".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getImporte();
        } else if("saldo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getSaldo();
        } else if("pago".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getPago();
        }
        return valor;
    }
    
}
