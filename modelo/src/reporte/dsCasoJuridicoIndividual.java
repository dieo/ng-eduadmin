/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsCasoJuridicoIndividual implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet caso;
    private int index = 0;
    private int total = 0;

    public dsCasoJuridicoIndividual(Object mod) {
        this.caso = (java.sql.ResultSet)mod;
        try {
            this.caso.last();
            total = this.caso.getRow()+1;
            this.caso.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { caso.next();
        } catch (Exception e) { }
        return ++index < this.total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedula".equals(jrCampo.getName())) {
                valor = caso.getString("cedula");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = caso.getString("apellido") + ", " + caso.getString("nombre");
            } else if("numerocaso".equals(jrCampo.getName())) {
                valor = caso.getInt("numerocaso");
            } else if("telefono".equals(jrCampo.getName())) {
                valor = caso.getString("telefono");
            } else if("ciudad".equals(jrCampo.getName())) {
                valor = caso.getString("ciudad");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = caso.getDate("fecha");
            } else if("estadojuridico".equals(jrCampo.getName())) {
                valor = caso.getString("estadojuridico");
            } else if("juzgado".equals(jrCampo.getName())) {
                valor = caso.getString("juzgado");
            } else if("caso".equals(jrCampo.getName())) {
                valor = caso.getString("caso");
            } else if("fechaevento".equals(jrCampo.getName())) {
                valor = caso.getDate("fechaevento");
            } else if("evento".equals(jrCampo.getName())) {
                valor = caso.getString("evento");
            } else if("asesor".equals(jrCampo.getName())) {
                valor = caso.getString("asesor");
            }
        } catch (Exception e) { }
        
        return valor;
    }
    
}
