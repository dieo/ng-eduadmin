/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbOrdenPagoFecha implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modbOrdenPago mod;
    private int index = -1;

    public dsbOrdenPagoFecha(Object mod) {
        this.mod = (especifico.modbOrdenPago)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("numeroorden".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNumeroOrden();
        } else if("fechaoperacion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFecha();
        } else if("receptor".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getReceptor();
        } else if("monto".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTotal();
        } else if("numerocuenta".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNumeroCuenta();
        } else if("concepto".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getConcepto();
        } else if("numerocheque".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNumeroCheque();
        }
        return valor;
    }
    
}
