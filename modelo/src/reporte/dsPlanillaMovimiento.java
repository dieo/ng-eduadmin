/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPlanillaMovimiento implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet movimiento;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsPlanillaMovimiento(Object mod) {
        this.movimiento = (java.sql.ResultSet)mod;
        try {
            this.movimiento.last();
            total = this.movimiento.getRow()+1;
            this.movimiento.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.movimiento.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("destinatario".equals(jrCampo.getName())) {
                valor = movimiento.getString("destinatario");
            } else if("regional".equals(jrCampo.getName())) {
                valor = movimiento.getString("regional");;
            } else if("remitente".equals(jrCampo.getName())) {
                valor = movimiento.getString("remitente");;
            } else if("fecha".equals(jrCampo.getName())) {
                valor = movimiento.getDate("fecha");
            } else if("observacion".equals(jrCampo.getName())) {
                valor = movimiento.getString("observacion");;
            } else if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = movimiento.getInt("numerooperacion");
            } else if("fechaaprobacion".equals(jrCampo.getName())) {
                valor = movimiento.getDate("fechaaprobacion");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = movimiento.getString("cedula");
            } else if("nombreapellido".equals(jrCampo.getName())) {
                valor = movimiento.getString("nombreapellido");
            } else if("monto".equals(jrCampo.getName())) {
                valor = movimiento.getDouble("montoaprobado");
            } else if("tipodocumento".equals(jrCampo.getName())) {
                valor = movimiento.getString("tipodocumento");
            } else if("tipooperacion".equals(jrCampo.getName())) {
                valor = movimiento.getString("tipooperacion");
            } else if("ubicacion".equals(jrCampo.getName())) {
                valor = movimiento.getString("ubicacion");
            }
        } catch (Exception e) { 
        javax.swing.JOptionPane.showMessageDialog(null, e);
        }
        return valor;
    }
    
}
