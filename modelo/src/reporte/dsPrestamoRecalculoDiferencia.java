/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPrestamoRecalculoDiferencia implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entListaSocio socio;
    private especifico.entMovimiento ent;
    private especifico.modDetalleCancelacionCabecera modCabecera;
    private int index = -1;

    public dsPrestamoRecalculoDiferencia(Object mod, Object mod2, Object mod3) {
        this.socio = (especifico.entListaSocio)mod;
        this.ent = (especifico.entMovimiento)mod2;
        this.modCabecera = (especifico.modDetalleCancelacionCabecera)mod3;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < modCabecera.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("fechasolicitud".equals(jrCampo.getName())) {
            valor = utilitario.utiFecha.convertirToStringDMA(ent.getFechaSolicitud());
        } else if("numerosolicitud".equals(jrCampo.getName())) {
            valor = utilitario.utiNumero.getMascara(ent.getNumeroSolicitud(),0);
        } else if("socio".equals(jrCampo.getName())) {
            valor = socio.getCedula()+"  "+socio.getNombre()+" "+socio.getApellido();
        } else if("item".equals(jrCampo.getName())) {
            valor = index+1; valor += ")";
        } else if("cuenta".equals(jrCampo.getName())) {
            valor = modCabecera.getEntidad(index).getCuenta();
            valor = valor + " Nro.Op. "+modCabecera.getEntidad(index).getNumeroOperacion()+" ("+modCabecera.getEntidad(index).getReferencia()+")";
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = modCabecera.getEntidad(index).getNumeroOperacion();
        } else if("atraso".equals(jrCampo.getName())) {
            valor = modCabecera.getEntidad(index).getMontoAtraso();
        } else if("saldo".equals(jrCampo.getName())) {
            valor = modCabecera.getEntidad(index).getMontoSaldo();
        } else if("exoneracion".equals(jrCampo.getName())) {
            valor = modCabecera.getEntidad(index).getMontoExoneracion();
        } else if("cobrado".equals(jrCampo.getName())) {
            valor = modCabecera.getEntidad(index).getMontoCobro();
        } else if("diferencia".equals(jrCampo.getName())) {
            valor = modCabecera.getEntidad(index).getMontoAtraso()+modCabecera.getEntidad(index).getMontoSaldo()-modCabecera.getEntidad(index).getMontoCobro();
        }
        return valor;
    }
    
}
