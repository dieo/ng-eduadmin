/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsFactura3 implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet factura;
    private String scantidad = "";
    private String sdescripcion = "";
    private String spreciounitario = "";
    private String smontoexenta = "";
    private String smontogravada5 = "";
    private String smontogravada10 = "";
    private double utotalexenta = 0.0;
    private double utotalgravada5 = 0.0;
    private double utotalgravada10 = 0.0;
    private int index = 0;
    private int totalDetalle = 0;

    public dsFactura3(Object mod) {
        this.factura = (java.sql.ResultSet)mod;
        try {
            this.factura.beforeFirst();
            while (this.factura.next()){
                scantidad += factura.getInt("cantidad") + "\n";
                sdescripcion += utilitario.utiCadena.convertirMayusMinus(factura.getString("producto"))+" "+factura.getString("referencia") + "\n";
                spreciounitario += utilitario.utiNumero.getMascara(factura.getDouble("precio"),0) + "\n";
                smontoexenta += utilitario.utiNumero.getMascara(factura.getDouble("exenta"),0) + "\n";
                smontogravada5 += utilitario.utiNumero.getMascara(factura.getDouble("gravada5"),0) + "\n";
                smontogravada10 += utilitario.utiNumero.getMascara(factura.getDouble("gravada10"),0) + "\n";
                utotalexenta += factura.getDouble("exenta");
                utotalgravada5 += factura.getDouble("gravada5");
                utotalgravada10 += factura.getDouble("gravada10");
            }
        } catch (Exception e) { }
        try {
            this.factura.last();
            totalDetalle = this.factura.getRow()+1;
            this.factura.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { factura.next();
        } catch (Exception e) { }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if (index == 1) {
            try {
                if ("fecha".equals(jrCampo.getName())) {
                    valor = utilitario.utiFecha.convertirToStringDMA(factura.getDate("fechafactura"));
                } else if ("contado".equals(jrCampo.getName())) {
                    if (factura.getString("tipofactura").contains("CONTADO")) valor = "X";
                } else if ("credito".equals(jrCampo.getName())) {
                    if (factura.getString("tipofactura").contains("CREDITO")) valor = "X";
                } else if ("numerofactura".equals(jrCampo.getName())) {
                    valor = factura.getInt("numerofactura");
                } else if ("ruc".equals(jrCampo.getName())) {
                    valor = factura.getString("ruc");
                } else if ("razonsocial".equals(jrCampo.getName())) {
                    valor = utilitario.utiCadena.convertirMayusMinus(factura.getString("razonsocial"));
                } else if ("notaremision".equals(jrCampo.getName())) {
                    valor = factura.getInt("notaremision");
                } else if ("direccion".equals(jrCampo.getName())) {
                    valor = utilitario.utiCadena.convertirMayusMinus(factura.getString("direccion"));
                } else if ("telefono".equals(jrCampo.getName())) {
                    valor = factura.getString("telefono");
                } else if ("moneda".equals(jrCampo.getName())) {
                    valor = factura.getString("moneda");
                } else if ("montoexenta".equals(jrCampo.getName())) {
                    valor = smontoexenta;
                } else if ("montogravada5".equals(jrCampo.getName())) {
                    valor = smontogravada5;
                } else if ("montogravada10".equals(jrCampo.getName())) {
                    valor = smontogravada10;
                } else if ("cantidad".equals(jrCampo.getName())) {
                    valor = scantidad;
                } else if ("descripcion".equals(jrCampo.getName())) {
                    valor = sdescripcion;
                } else if ("preciounitario".equals(jrCampo.getName())) {
                    valor = spreciounitario;
                } else if ("totalexenta".equals(jrCampo.getName())) {
                    valor = utotalexenta;
                } else if ("totalgravada5".equals(jrCampo.getName())) {
                    valor = utotalgravada5;
                } else if ("totalgravada10".equals(jrCampo.getName())) {
                    valor = utotalgravada10;
                } else if ("total".equals(jrCampo.getName())) {
                    valor = factura.getDouble("total");
                } else if ("montoletra".equals(jrCampo.getName())) {
                    String sletra = new utilitario.utiLetra().leerNumero(factura.getDouble("total")).toLowerCase();
                    valor = sletra.substring(0, 1).toUpperCase() + sletra.substring(1, sletra.length()) + ".-";
                } else if ("montoimpuesto5".equals(jrCampo.getName())) {
                    valor = factura.getDouble("impuesto5");
                } else if ("montoimpuesto10".equals(jrCampo.getName())) {
                    valor = factura.getDouble("impuesto10");
                }
            } catch (Exception e) { }
        }
        return valor;
    }
    
}
