/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsInformePrestamoEmitido implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet operacion;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsInformePrestamoEmitido(Object mod) {
        this.operacion = (java.sql.ResultSet)mod;
        try {
            this.operacion.last();
            total = this.operacion.getRow()+1;
            this.operacion.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.operacion.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("cedula".equals(jrCampo.getName())) {
                valor = operacion.getString("cedula");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(operacion.getString("apellidonombre"));
            } else if("numerosolicitud".equals(jrCampo.getName())) {
                valor = operacion.getInt("numerosolicitud");
            } else if("fechasolicitud".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechageneracion");
            } else if("fechavencimiento".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechavencimiento");
            } else if("fechaprimervencimiento".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechaprimervencimiento");
            } else if("plazo".equals(jrCampo.getName())) {
                valor = operacion.getString("plazoaprobado");
            } else if("fuentefinanciera".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(operacion.getString("entidad"));
            } else if("montocapital".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montoaprobado");
            } else if("montointeres".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montointeres");
            } else if("montosaldo".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montosaldo");
            } else if("montocuota".equals(jrCampo.getName())) {
                valor = operacion.getDouble("cuota");
            } else if("montosaldocuota".equals(jrCampo.getName())) {
                valor = operacion.getDouble("saldocuota");
            } else if("tipocredito".equals(jrCampo.getName())) {
                valor = operacion.getString("tipocredito");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = operacion.getInt("numerooperacion");
            } else if("montocancelacion".equals(jrCampo.getName())) { 
                valor = operacion.getDouble("montocancelacion");
            } else if("montoimpuesto".equals(jrCampo.getName())) { 
                valor = operacion.getDouble("montoimpuesto");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
