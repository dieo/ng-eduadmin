/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

import especifico.modDetalleCancelacionCabeceranew;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPrestamoSolicitud implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entMovimiento prestamo;
    private especifico.modDetalleCancelacionCabeceranew cabecera;
    private especifico.modMovimientoGarante garante;
    private boolean esCancelacion = false;
    private int index = -1;

    public dsPrestamoSolicitud(Object mod, Object mod2, Object mod3) {
        this.prestamo = (especifico.entMovimiento)mod;
        this.cabecera = (especifico.modDetalleCancelacionCabeceranew)mod2;
        this.garante = (especifico.modMovimientoGarante)mod3;
        if (this.cabecera.getRowCount()>0) esCancelacion = true;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        if (cabecera.getRowCount()>0) {
            return ++index < cabecera.getRowCount();
        }
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try{
        if("fechasolicitud".equals(jrCampo.getName())) {
            int ano = prestamo.getFechaSolicitud().getYear()+1900;
            valor = "Asunción, "+prestamo.getFechaSolicitud().getDate()+" de "+utilitario.utiFecha.getMeses()[prestamo.getFechaSolicitud().getMonth()].toLowerCase()+" de "+ano;
        } else if("encabezado".equals(jrCampo.getName())) {
            valor = "Señor\nPresidente de la Comisión Directiva\nMutual Nacional de Funcionarios del M.S.P. y B.S.\nPresente";
        } else if("tipoprestamo".equals(jrCampo.getName())) {
            valor = prestamo.getTipoCredito();
        } else if("remision".equals(jrCampo.getName())) {
            valor = prestamo.getRemision();
        } else if("texto".equals(jrCampo.getName())) {
            String sletra = new utilitario.utiLetra().leerNumero(prestamo.getMontoSolicitud()).trim().toLowerCase();
            sletra = sletra.substring(0,1).toUpperCase()+sletra.substring(1,sletra.length());
            double umontoCuota = (prestamo.getMontoSolicitud()+(prestamo.getMontoSolicitud()*prestamo.getPlazoSolicitud()*prestamo.getTasaInteres()/100))/prestamo.getPlazoSolicitud();
            valor = "\t"+prestamo.getNombreApellido()+", en mi carácter de socio/a, con C.I.C. Nº "+utilitario.utiNumero.getMascara(prestamo.getCedula(),0)+" solicito un Préstamo de Gs. "+
                    utilitario.utiNumero.getMascara(prestamo.getMontoSolicitud(),0) + " (" + sletra + " Guaraníes) "+
                    "a ser destinado a "+prestamo.getDestino()+", en un plazo de "+prestamo.getPlazoSolicitud()+" meses, en cuotas mensuales de Gs. "+utilitario.utiNumero.getMascara(umontoCuota,0);
        } else if("codeudor".equals(jrCampo.getName())) {
            int ipos = 0;
            String snombre = "";
            String sapellido = "";
            valor = "Codeudor: ";
            if (garante.getRowCount()==0) return "";
            if (garante.getRowCount()==2) valor = "Codeudores: ";
            if (garante.getRowCount()==1 || garante.getRowCount()==2) {
                ipos = garante.getEntidad(0).getApellidoNombre().indexOf(",");
                snombre = garante.getEntidad(0).getApellidoNombre().substring(ipos+2,garante.getEntidad(0).getApellidoNombre().length());
                sapellido = garante.getEntidad(0).getApellidoNombre().substring(0,ipos);
                valor += utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(0).getCedula(),0);
                if (garante.getRowCount()==2) {
                    valor += "  ---  ";
                    ipos = garante.getEntidad(1).getApellidoNombre().indexOf(",");
                    snombre = garante.getEntidad(1).getApellidoNombre().substring(ipos+2,garante.getEntidad(1).getApellidoNombre().length());
                    sapellido = garante.getEntidad(1).getApellidoNombre().substring(0,ipos);
                    valor += utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(1).getCedula(),0);
                }
            }
        } else if("solicitante".equals(jrCampo.getName())) {
            valor = utilitario.utiCadena.convertirMayusMinus(prestamo.getNombreApellido().toLowerCase())+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(prestamo.getCedula(),0);
        } else if("codeudor1".equals(jrCampo.getName())) {
            valor = "";
            if (garante.getRowCount()==0) return "";
            if (garante.getRowCount()==1 || garante.getRowCount()==2) {
                int ipos = garante.getEntidad(0).getApellidoNombre().indexOf(",");
                String snombre = garante.getEntidad(0).getApellidoNombre().substring(ipos+2,garante.getEntidad(0).getApellidoNombre().length());
                String sapellido = garante.getEntidad(0).getApellidoNombre().substring(0,ipos);
                valor = utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(0).getCedula(),0);
            }
        } else if("codeudor2".equals(jrCampo.getName())) {
            valor = "";
            if (garante.getRowCount()==0) return "";
            if (garante.getRowCount()==2) {
                int ipos = garante.getEntidad(1).getApellidoNombre().indexOf(",");
                String snombre = garante.getEntidad(1).getApellidoNombre().substring(ipos+2,garante.getEntidad(1).getApellidoNombre().length());
                String sapellido = garante.getEntidad(1).getApellidoNombre().substring(0,ipos);
                valor = utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(1).getCedula(),0);
            }
        } else if("cantidadcodeudor".equals(jrCampo.getName())) {
            valor = 0;
            valor = garante.getRowCount();
        } else if("informecredito".equals(jrCampo.getName())) {
            String conGarante = "SIN Garante";
            String conCancelacion = "SIN Cancelación";
            if (garante.getRowCount()>0) conGarante = "CON Garante";
            if (esCancelacion) conCancelacion = "CON Cancelación";
            valor = "Rubro: "+prestamo.getRubro().toUpperCase()+" - Procedencia: "+prestamo.getZona().toUpperCase()+" - Antigüedad: "+utilitario.utiFecha.obtenerDiferenciaMes(prestamo.getFechaIngreso(), new java.util.Date())[0]+" mes/es - "+conGarante+" - "+conCancelacion;
        } else if("operacioncancelada".equals(jrCampo.getName())) {
            String soperacionCancelada = "Operación Cancelada: ";
            for (int i=0; i<cabecera.getRowCount(); i++) {
                if (i>0 && i<cabecera.getRowCount()) soperacionCancelada += " - ";
                soperacionCancelada += utilitario.utiCadena.convertirMayusMinus(cabecera.getEntidad(i).getcCuenta())+" "+utilitario.utiNumero.getMascara(cabecera.getEntidad(i).getcNumeroOperacion(),0);
            }
            valor = soperacionCancelada;
        }
        if (cabecera.getRowCount()>0) {
            if("item".equals(jrCampo.getName())) {
                valor = index+1; valor += ")";
            } else if("cuenta".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(cabecera.getEntidad(index).getcCuenta());
                //if (cabecera.getEntidad(index).getcDescripcion()!=null) valor = valor+" "+cabecera.getEntidad(index).getDescripcion();
                valor = valor + " Nro.Op. "+cabecera.getEntidad(index).getcNumeroOperacion()+" ("+cabecera.getEntidad(index).getcResumenCuota()+")";
            } else if("atraso".equals(jrCampo.getName())) {
                valor = cabecera.getEntidad(index).getcTotalAtraso();
            } else if("mes".equals(jrCampo.getName())) {
                valor = cabecera.getEntidad(index).getcTotalMes();
            } else if("saldo".equals(jrCampo.getName())) {
                valor = cabecera.getEntidad(index).getcTotalSaldo();
            } else if("exoneracion".equals(jrCampo.getName())) {
                valor = cabecera.getEntidad(index).getcTotalExoneracion();
            }
        }
        }catch(Exception e){
            javax.swing.JOptionPane.showMessageDialog(null, e.toString()+"-"+valor);
        }
        return valor;
    }
    
}
