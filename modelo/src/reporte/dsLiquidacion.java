/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsLiquidacion implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entMovimiento orden;
    private especifico.entSocio socio;
    private especifico.modDetalleOperacion mod;
    private double usaldoCapital;

    private int index = -1;

    public dsLiquidacion(Object mod, Object mod1, Object mod2) {
        this.orden = (especifico.entMovimiento)mod;
        this.socio = (especifico.entSocio)mod1;
        this.mod = (especifico.modDetalleOperacion)mod2;
        this.usaldoCapital = orden.getMontoAprobado();
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("cedula".equals(jrCampo.getName())) {
            valor = this.socio.getCedula();
        } else if("nombreapellido".equals(jrCampo.getName())) {
            valor = this.socio.getNombre() + " " + this.socio.getApellido();
        } else if("numerosolicitud".equals(jrCampo.getName())) {
            valor = this.orden.getNumeroSolicitud();
        } else if("fechasolicitud".equals(jrCampo.getName())) {
            valor = this.orden.getFechaSolicitud();
        } else if("montosolicitado".equals(jrCampo.getName())) {
            valor = this.orden.getMontoSolicitud();
        } else if("plazosolicitado".equals(jrCampo.getName())) {
            valor = this.orden.getPlazoSolicitud();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = this.orden.getNumeroOperacion();
        } else if("fechaaprobacion".equals(jrCampo.getName())) {
            valor = this.orden.getFechaAprobado();
        } else if("montoaprobado".equals(jrCampo.getName())) {
            valor = this.orden.getMontoAprobado();
        } else if("plazoaprobado".equals(jrCampo.getName())) {
            valor = this.orden.getPlazoAprobado();
        } else if("tasainteres".equals(jrCampo.getName())) {
            valor = this.orden.getTasaInteres();
        } else if("montointeres".equals(jrCampo.getName())) {
            valor = this.orden.getMontoInteres();
        } else if("retencion".equals(jrCampo.getName())) {
            valor = this.orden.getRetencion();
        } else if("montoimpuesto".equals(jrCampo.getName())) {
            valor = this.orden.getMontoImpuesto();
        } else if("desembolso".equals(jrCampo.getName())) {
            valor = this.orden.getMontoSaldo();
        } else if("boleta".equals(jrCampo.getName())) {
            valor = this.orden.getNumeroBoleta();
        } else if("entidad".equals(jrCampo.getName())) {
            valor = this.orden.getEntidad();
        } else if("observacion".equals(jrCampo.getName())) {
            valor = this.orden.getObservacionAprobado();
        } else if("cuota".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCuota() + "/" + this.orden.getPlazoAprobado();
        } else if("fechavencimiento".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFechaVencimiento();
        } else if("saldocapital".equals(jrCampo.getName())) {
            this.usaldoCapital -= this.mod.getEntidad(index).getMontoCapital();
            valor = this.usaldoCapital;
        } else if("capital".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoCapital();
        } else if("interes".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoInteres();
        } else if("importecuota".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoCapital() + this.mod.getEntidad(index).getMontoInteres();
        } else if("autorizador".equals(jrCampo.getName())) {
            valor = this.orden.getAutorizador();
        } else if("oficial".equals(jrCampo.getName())) {
            valor = this.orden.getOficial();
        } else if("analista".equals(jrCampo.getName())) {
            valor = this.orden.getAnalista();
        } else if("remision".equals(jrCampo.getName())) {
            valor = this.orden.getRemision();
        } else if("viacobro".equals(jrCampo.getName())) {
            String sgiraduria = "";
            String srubro = "";
            String sasignacion = "";
            if (this.orden.getGiraduria()!=null) sgiraduria = this.orden.getGiraduria();
            if (this.orden.getRubro()!=null) srubro = this.orden.getRubro();
            if (this.orden.getAsignacion()!=null) sasignacion = this.orden.getAsignacion();
            valor = sgiraduria+" - "+srubro+" - "+sasignacion;
        }
        return valor;
    }
    
}
