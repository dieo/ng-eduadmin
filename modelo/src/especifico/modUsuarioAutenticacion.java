/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import javax.swing.event.ListDataListener;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modUsuarioAutenticacion implements javax.swing.ListModel {

    public java.util.ArrayList <especifico.entUsuario> data = new java.util.ArrayList <especifico.entUsuario>();
    private java.util.ArrayList <ListDataListener> list = new java.util.ArrayList <ListDataListener>();
    private String smensaje;
    private int iposicionAutenticada;

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Object getElementAt(int index) {
        return ((especifico.entUsuario)data.get(index)).getFuncionario();
    }

    public especifico.entUsuario getEntidad(int index) {
        return ((especifico.entUsuario)data.get(index));
    }
    
    @Override
    public void addListDataListener(ListDataListener l) {
        list.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        list.remove(l);
    }

    public void removerTodo() {
        data.removeAll(data);
    }

    public void removerEliminado() {
    }
    
    public void cargarTable(java.sql.ResultSet rs, int iidUsuarioPrioridad) {
        modUsuarioAutenticacion aux = new modUsuarioAutenticacion();
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                aux.data.add(new especifico.entUsuario().cargar(rs));
            }
        } catch (Exception e) {
        }
        for (int i=0; i<aux.getSize(); i++) {
            if (aux.getEntidad(i).getId()==iidUsuarioPrioridad) {
                data.add(aux.getEntidad(i));
            }
        }
        for (int i=0; i<aux.getSize(); i++) {
            if (aux.getEntidad(i).getId()!=iidUsuarioPrioridad) {
                data.add(aux.getEntidad(i));
            }
        }
    }
    
    /*public boolean autenticado(String susuario, String scontrasena, int iidAdministrador) {
        this.smensaje = "";
        this.iposicionAutenticada = -1;
        if (susuario.isEmpty() || scontrasena.isEmpty()) { // el usuario o la contrasena está vacío
            this.smensaje = "Usuario y Contraseña deben contener valores";
            return false;
        }
        for (int i=0; i<this.getSize(); i++) { // realiza la busqueda de usuario
            if (susuario.trim().toLowerCase().equals(this.getEntidad(i).getUsuario().trim().toLowerCase())) { // encuentra un usuario igual
                if (this.getEntidad(i).getSesionIniciada() > 0 && !(this.getEntidad(i).getIdNivelUsuario() == iidAdministrador)) {
                    this.smensaje = "Ya tiene una Sesión abierta \nVerifique con el Administrador del Sistema";
                    return false;
                } else { // no tiene sesión iniciada
                    if (!scontrasena.trim().equals(this.getEntidad(i).getContrasena().trim())) { // las contrasenas no son iguales
                        this.smensaje = "Contraseña incorrecta";
                        return false;
                    } else { // las contrasenas son iguales
                        this.iposicionAutenticada = i;
                        return true;
                    }
                }
            }
        }
        this.smensaje = "Usuario no existe";
        return false;
    }*/
    
    public int getIdUsuario(String susuario) {
        for (int i=0; i<this.getSize(); i++) { // realiza la busqueda de usuario
            if (susuario.trim().equals(this.getEntidad(i).getUsuario().trim())) { // encuentra un usuario igual
                return this.getEntidad(i).getId();
            }
        }
        return 0;
    }
    
    public int getPosicion(String susuario) {
        for (int i=0; i<this.getSize(); i++) { // realiza la busqueda de usuario
            if (susuario.trim().equals(this.getEntidad(i).getUsuario().trim())) { // encuentra un usuario igual
                return i;
            }
        }
        return 0;
    }
    
    public String getMensaje() {
        return this.smensaje;
    }
    
    public int getPosicionAutenticada() {
        return this.iposicionAutenticada;
    }

}
