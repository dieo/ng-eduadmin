/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modDetalleFacturaIngreso implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje;

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entDetalleFacturaIngreso getEntidad() {
        return (especifico.entDetalleFacturaIngreso)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entDetalleFacturaIngreso nuevo) {
        nuevo.copiar((especifico.entDetalleFacturaIngreso)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "ITEM";
            case 1: return "CANT.";
            case 2: return "DESCRIPCION";
            case 3: return "UNID.MEDIDA";
            case 4: return "PRECIO UNIT.";
            case 5: return "% DTO";
            case 6: return "EXENTA";
            case 7: return "IMP. 5%";
            case 8: return "IMP. 10%";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return Double.class;
            case 2: return String.class;
            case 3: return String.class;
            case 4: return Double.class;
            case 5: return Double.class;
            case 6: return Double.class;
            case 7: return Double.class;
            case 8: return Double.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entDetalleFacturaIngreso ent;
        ent = (especifico.entDetalleFacturaIngreso)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return rowIndex+1;
            case 1: return ent.getCantidad();
//            case 2: return utilitario.utiCadena.convertirMayusMinus(ent.getProducto())+" "+ent.getReferencia();
            case 2: return utilitario.utiCadena.convertirMayusMinus(ent.getProducto());
            case 3: return ent.getUnidadMedida();
            case 4: return ent.getPrecio();
            case 5: return ent.getPorcDto();
            case 6: return ent.getExenta();
            case 7: return ent.getGravada5();
            case 8: return ent.getGravada10();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entDetalleFacturaIngreso ent;
        ent = (especifico.entDetalleFacturaIngreso)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setItem((Integer)aValue); break;
            case 1: ent.setCantidad((Double)aValue); break;
            case 2: ent.setProducto((String)aValue); break;
            case 3: ent.setUnidadMedida((String)aValue); break;
            case 4: ent.setPrecio((Double)aValue); break;
            case 5: ent.setPorcDto((Double)aValue); break;
            case 6: ent.setExenta((Double)aValue); break;
            case 7: ent.setGravada5((Double)aValue); break;
            case 8: ent.setGravada10((Double)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entDetalleFacturaIngreso getEntidad(int rowIndex) {
        return (especifico.entDetalleFacturaIngreso)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entDetalleFacturaIngreso nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entDetalleFacturaIngreso nuevo, int rowIndex) {
        nuevo.copiar((especifico.entDetalleFacturaIngreso)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entDetalleFacturaIngreso().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setMaxWidth(40);
        tbl.getColumnModel().getColumn(1).setMaxWidth(40);
        tbl.getColumnModel().getColumn(2).setMaxWidth(600);
        tbl.getColumnModel().getColumn(3).setMaxWidth(60);
        tbl.getColumnModel().getColumn(4).setMaxWidth(80);
        tbl.getColumnModel().getColumn(5).setMaxWidth(80);
        tbl.getColumnModel().getColumn(6).setMaxWidth(80);
        tbl.getColumnModel().getColumn(7).setMaxWidth(80);
        tbl.getColumnModel().getColumn(8).setMaxWidth(80);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public especifico.entDetalleFacturaIngreso getTotales() {
        especifico.entDetalleFacturaIngreso ent = new especifico.entDetalleFacturaIngreso();
        for (int i=0; i<this.getRowCount(); i++) {
            ent.setExenta(ent.getExenta()+this.getEntidad(i).getExenta());
            ent.setGravada5(ent.getGravada5()+this.getEntidad(i).getGravada5());
            ent.setGravada10(ent.getGravada10()+this.getEntidad(i).getGravada10());
        }
        return ent;
    }
    
}
