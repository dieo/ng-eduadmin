/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class modbOrdenPagoDetalle implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }

    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entbOrdenPagoDetalle getEntidad() {
        return (especifico.entbOrdenPagoDetalle) (data.get(tbl.getSelectedRow()));
    }

    public void modificar(especifico.entbOrdenPagoDetalle nuevo) {
        nuevo.copiar((especifico.entbOrdenPagoDetalle) (data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent(this, this.getRowCount() - 1, this.getRowCount() - 1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores(evento);
    }

    public void eliminar() {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent(this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores(evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
//            case 0:
//                return "PLAN DE CUENTA";
//            case 1:
//                return "CUENTA";
            case 0:
                return "DESCRIPCIÓN";
            case 1:
                return "MONTO";
            default:
                return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
//            case 0:
//                return String.class;
//            case 1:
//                return String.class;
            case 0:
                return String.class;
            case 1:
                return Double.class;
            default:
                return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entbOrdenPagoDetalle ent;
        ent = (especifico.entbOrdenPagoDetalle) (data.get(rowIndex));
        switch (columnIndex) {
//            case 0:
//                return ent.getNroPlanCuenta();
//            case 1:
//                return ent.getPlanCuenta();
            case 0:
                return ent.getDescripcion();
            case 1:
                return ent.getMonto();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entbOrdenPagoDetalle ent;
        ent = (especifico.entbOrdenPagoDetalle) (data.get(rowIndex));
        switch (columnIndex) {
//            case 0:
//                ent.setNroPlanCuenta((String) aValue);
//                break;
//            case 1:
//                ent.setPlanCuenta((String) aValue);
//                break;
            case 0:
                ent.setDescripcion((String) aValue);
                break;
            case 1:
                ent.setMonto((Double) aValue);
                break;
            default:
                break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent(this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entbOrdenPagoDetalle getEntidad(int rowIndex) {
        return (especifico.entbOrdenPagoDetalle) (data.get(rowIndex));
    }

    private void avisaSuscriptores(javax.swing.event.TableModelEvent evento) {
        for (int i = 0; i < list.size(); i++) {
            ((javax.swing.event.TableModelListener) list.get(i)).tableChanged(evento);
        }
    }

    public void insertar(especifico.entbOrdenPagoDetalle nuevo) {
        try {
            data.add(nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent(this, this.getRowCount() - 1, this.getRowCount() - 1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores(evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entbOrdenPagoDetalle nuevo, int rowIndex) {
        nuevo.copiar((especifico.entbOrdenPagoDetalle) (data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent(this, this.getRowCount() - 1, this.getRowCount() - 1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores(evento);
    }

    public void eliminar(int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent(this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores(evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }

    public void cargarTable(java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entbOrdenPagoDetalle().cargar(rs));
            }
        } catch (Exception e) {
        }
    }

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(115);
//        tbl.getColumnModel().getColumn(2).setPreferredWidth(115);
//        tbl.getColumnModel().getColumn(3).setPreferredWidth(40);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public double getTotal() {
        double total = 0;
        for (int i = 0; i < this.getRowCount(); i++) {
            total += this.getEntidad(i).getMonto();
        }
        return total;
    }

}
