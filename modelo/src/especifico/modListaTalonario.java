/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modListaTalonario implements javax.swing.table.TableModel {

    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje;

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "NRO.INICIAL";
            case 1: return "NRO.FINAL";
            case 2: return "FECHA VENC.";
            case 3: return "SECUENCIA";
            case 4: return "OK";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return Integer.class;
            case 2: return java.util.Date.class;
            case 3: return Integer.class;
            case 4: return Boolean.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entListaTalonario ent;
        ent = (especifico.entListaTalonario)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getNumeroInicial();
            case 1: return ent.getNumeroFinal();
            case 2: return ent.getFechaVencimiento();
            case 3: return ent.getSecuencia();
            case 4: return (Boolean)ent.getAcepta();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entListaTalonario ent;
        ent = (especifico.entListaTalonario)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setNumeroInicial((Integer)aValue); break;
            case 1: ent.setNumeroFinal((Integer)aValue); break;
            case 2: ent.setFechaVencimiento((java.util.Date)aValue); break;
            case 3: ent.setSecuencia((Integer)aValue); break;
            case 4: ent.setAcepta((Boolean)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entListaTalonario getEntidad(int rowIndex) {
        return (especifico.entListaTalonario)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entListaTalonario nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entListaTalonario nuevo, int rowIndex) {
        nuevo.copiar((especifico.entListaTalonario)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entListaTalonario().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    public void establecerFormato(javax.swing.JTable tbl) {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(60);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(60);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(60);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(60);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(60);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.getColumnModel().getColumn(4).setCellEditor(new generico.modCeldaCheckBox());
        tbl.getColumnModel().getColumn(4).setCellRenderer(new generico.modRenderCheckBox());*/
    }

    public void marcar(boolean bmarcar) {
        for (int i=0; i<this.getRowCount(); i++) {
            this.getEntidad(i).setAcepta(bmarcar);
        }
    }
    
    public int getCantidad() {
        int icantidad = 0;
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getAcepta()) {
                icantidad++;
            }
        }
        return icantidad;
    }
    
    public String getMensaje() {
        return this.smensaje;
    }

}
