/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modcFacturaEgresoDetalle implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje;

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entcFacturaEgresoDetalle getEntidad() {
        return (especifico.entcFacturaEgresoDetalle)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entcFacturaEgresoDetalle nuevo) {
        nuevo.copiar((especifico.entcFacturaEgresoDetalle)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "ITEM";
            case 1: return "CANT.";
            case 2: return "PRODUCTO";
            case 3: return "STOCK DE";
            case 4: return "UNID.MEDIDA";
            case 5: return "PRECIO UNIT.";
            case 6: return "EXENTA";
            case 7: return "IMP. 5%";
            case 8: return "IMP. 10%";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return Double.class;
            case 2: return String.class;
            case 3: return String.class;
            case 4: return String.class;
            case 5: return Integer.class;
            case 6: return Integer.class;
            case 7: return Integer.class;
            case 8: return Integer.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entcFacturaEgresoDetalle ent;
        ent = (especifico.entcFacturaEgresoDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return rowIndex+1;
            case 1: return ent.getCantidad();
            case 2: return ent.getDescripcion();
            case 3: return ent.getFilial();
            case 4: return ent.getUnidadMedida();
            case 5: return ent.getCosto();
            case 6:
                if (ent.getImpuesto().contains("EXENTA")) return ent.getCosto()*ent.getCantidad();
                else return 0.0;
            case 7: 
                if (ent.getImpuesto().contains("5")) return ent.getCosto()*ent.getCantidad();
                else return 0.0;
            case 8: 
                if (ent.getImpuesto().contains("10")) return ent.getCosto()*ent.getCantidad();
                else return 0.0;
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entcFacturaEgresoDetalle ent;
        ent = (especifico.entcFacturaEgresoDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setItem((Integer)aValue); break;
            case 1: ent.setCantidad((Double)aValue); break;
            case 2: ent.setDescripcion((String)aValue); break;
            case 3: ent.setFilial((String)aValue); break;
            case 4: ent.setUnidadMedida((String)aValue); break;
            case 5: ent.setCosto((Double)aValue); break;
            case 6:
                if (ent.getImpuesto().contains("EXENTA")) { ent.setSubtotal((Double)aValue); break; }
                else { ent.setSubtotal((Double)aValue); break; }
            case 7:
                if (ent.getImpuesto().contains("5")) { ent.setSubtotal((Double)aValue); break; }
                else { ent.setSubtotal((Double)aValue); break; }
            case 8:
                if (ent.getImpuesto().contains("10")) { ent.setSubtotal((Double)aValue); break; }
                else { ent.setSubtotal((Double)aValue); break; }
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entcFacturaEgresoDetalle getEntidad(int rowIndex) {
        return (especifico.entcFacturaEgresoDetalle)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entcFacturaEgresoDetalle nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entcFacturaEgresoDetalle nuevo, int rowIndex) {
        nuevo.copiar((especifico.entcFacturaEgresoDetalle)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entcFacturaEgresoDetalle().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(5);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(5);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(130);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(25);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(15);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(7).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(8).setPreferredWidth(30);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public double getTotal(String columna) {
        double total = 0.0;
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getImpuesto().contains(columna)) {
                total += this.getEntidad(i).getCosto()*this.getEntidad(i).getCantidad();
            }
        }
        return total;
    }
    
    public double getTotalImpuesto(String columna) {
        double total = 0.0;
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getImpuesto().contains(columna)) {
                total += (this.getEntidad(i).getCosto()*this.getEntidad(i).getCantidad()*this.getEntidad(i).getTasa())/(100+this.getEntidad(i).getTasa());
            }
        }
        return total;
    }
    
}
