/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modExtractoCerradoCobrado implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entExtractoCerradoCobrado getEntidad() {
        return (especifico.entExtractoCerradoCobrado)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entExtractoCerradoCobrado nuevo) {
        nuevo.copiar((especifico.entExtractoCerradoCobrado)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "CUENTA";
            case 1: return "RUBRO";
            case 2: return "NRO.OP.";
            case 3: return "CERRADO";
            case 4: return "GIRADURIA";
            case 5: return "VENTANILLA";
            case 6: return "ASIGNACION";
            case 7: return "REEMBOLSO";
            case 8: return "DIFERENCIA";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return String.class;
            case 2: return Integer.class;
            case 3: return Double.class;
            case 4: return Double.class;
            case 5: return Double.class;
            case 6: return Double.class;
            case 7: return Double.class;
            case 8: return Double.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entExtractoCerradoCobrado ent;
        ent = (especifico.entExtractoCerradoCobrado)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getCuenta();
            case 1: return ent.getRubro();
            case 2: return ent.getNumeroOperacion();
            case 3: return ent.getMontoCerrado();
            case 4: return ent.getMontoGiraduria();
            case 5: return ent.getMontoVentanilla();
            case 6: return ent.getMontoAsignacion();
            case 7: return ent.getMontoReembolso();
            case 8: return ent.getMontoDiferencia();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entExtractoCerradoCobrado ent;
        ent = (especifico.entExtractoCerradoCobrado)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setCuenta((String)aValue); break;
            case 1: ent.setRubro((String)aValue); break;
            case 2: ent.setNumeroOperacion((Integer)aValue); break;
            case 3: ent.setMontoCerrado((Double)aValue); break;
            case 4: ent.setMontoGiraduria((Double)aValue); break;
            case 5: ent.setMontoVentanilla((Double)aValue); break;
            case 6: ent.setMontoAsignacion((Double)aValue); break;
            case 7: ent.setMontoReembolso((Double)aValue); break;
            case 8: ent.setMontoDiferencia((Double)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entExtractoCerradoCobrado getEntidad(int rowIndex) {
        return (especifico.entExtractoCerradoCobrado)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entExtractoCerradoCobrado nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entExtractoCerradoCobrado nuevo, int rowIndex) {
        nuevo.copiar((especifico.entExtractoCerradoCobrado)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entExtractoCerradoCobrado().cargar(rs));
            }
        } catch (Exception e) {
        }
    }

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(100);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(10);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(10);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(7).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(8).setPreferredWidth(30);
        tbl.getTableHeader().setFont(new java.awt.Font("Arial Narrow", 1, 10));
        tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); 
    }

    public especifico.entExtractoCerradoCobrado getTotales() {
        especifico.entExtractoCerradoCobrado ent = new especifico.entExtractoCerradoCobrado();
        for (int i=0; i<this.getRowCount(); i++) {
            ent.setMontoCerrado(ent.getMontoCerrado()+this.getEntidad(i).getMontoCerrado());
            ent.setMontoGiraduria(ent.getMontoGiraduria()+this.getEntidad(i).getMontoGiraduria());
            ent.setMontoVentanilla(ent.getMontoVentanilla()+this.getEntidad(i).getMontoVentanilla());
            ent.setMontoAsignacion(ent.getMontoAsignacion()+this.getEntidad(i).getMontoAsignacion());
            ent.setMontoReembolso(ent.getMontoReembolso()+this.getEntidad(i).getMontoReembolso());
            ent.setMontoDiferencia(ent.getMontoDiferencia()+this.getEntidad(i).getMontoDiferencia());
        }
        return ent;
    }
    
}
