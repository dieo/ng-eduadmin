/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modPlanillaIngreso implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entPlanillaIngreso getEntidad() {
        return (especifico.entPlanillaIngreso)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entPlanillaIngreso nuevo) {
        nuevo.copiar((especifico.entPlanillaIngreso)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "NUMERO";
            case 1: return "DESCRIPCION";
            case 2: return "FUNCIONARIO";
            case 3: return "SUCURSAL";
            case 4: return "FECHA APERT.";
            case 5: return "HORA APERT.";
            case 6: return "FECHA CIERRE";
            case 7: return "HORA CIERRE";
            case 8: return "ACTIVO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return String.class;
            case 2: return String.class;
            case 3: return String.class;
            case 4: return java.util.Date.class;
            case 5: return String.class;
            case 6: return java.util.Date.class;
            case 7: return String.class;
            case 8: return Boolean.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entPlanillaIngreso ent;
        ent = (especifico.entPlanillaIngreso)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getNumeroPlanilla();
            case 1: return ent.getDescripcion();
            case 2: return ent.getFuncionario();
            case 3: return ent.getSucursal();
            case 4: return ent.getFechaApertura();
            case 5: return ent.getHoraApertura();
            case 6: return ent.getFechaCierre();
            case 7: return ent.getHoraCierre();
            case 8: return ent.getActivo();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entPlanillaIngreso ent;
        ent = (especifico.entPlanillaIngreso)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setNumeroPlanilla((Integer)aValue); break;
            case 1: ent.setDescripcion((String)aValue); break;
            case 2: ent.setFuncionario((String)aValue); break;
            case 3: ent.setSucursal((String)aValue); break;
            case 4: ent.setFechaApertura((java.util.Date)aValue); break;
            case 5: ent.setHoraApertura((String)aValue); break;
            case 6: ent.setFechaCierre((java.util.Date)aValue); break;
            case 7: ent.setHoraCierre((String)aValue); break;
            case 8: ent.setActivo((Boolean)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entPlanillaIngreso getEntidad(int rowIndex) {
        return (especifico.entPlanillaIngreso)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar(especifico.entPlanillaIngreso nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        }catch(Exception e) {
        }
    }

    public void modificar(especifico.entPlanillaIngreso nuevo, int rowIndex) {
        nuevo.copiar((especifico.entPlanillaIngreso)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar(int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        }catch(Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }

    public void cargarTable(java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entPlanillaIngreso().cargar(rs));
            }
        } catch (Exception e) {
        }
    }

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(70);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(55);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(55);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(15);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(7).setPreferredWidth(15);
        tbl.getColumnModel().getColumn(8).setPreferredWidth(10);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }
    
}
