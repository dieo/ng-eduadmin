/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class modPlanPago implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";
    
    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entPlanPago getEntidad() {
        return (especifico.entPlanPago)(data.get(tbl.getSelectedRow()));
    }

    public void modificar(especifico.entPlanPago nuevo) {
        nuevo.copiar((especifico.entPlanPago)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "NRO OPERACION";
            case 1: return "FECHA";
            case 2: return "FECHA VTO";
            case 3: return "ENTREGA INICIAL";
            case 4: return "CUOTAS";
            case 5: return "MONTO MENSUAL";
            case 6: return "TOTAL";
            case 7: return "SALDO";
            case 8: return "ESTADO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return java.util.Date.class;
            case 2: return java.util.Date.class;
            case 3: return Double.class;
            case 4: return Integer.class;
            case 5: return Double.class;
            case 6: return Double.class;
            case 7: return Double.class;
            case 8: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entPlanPago ent;
        ent = (especifico.entPlanPago)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getId();
            case 1: return ent.getFecha();
            case 2: return ent.getFechavencimiento();
            case 3: return ent.getMontoinicial();
            case 4: return ent.getCantidadcuotas();
            case 5: return ent.getMontoMensual();
            case 6: return ent.getMontoTotal();
            case 7: return ent.getSaldo();
            case 8: return ent.getEstado();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entPlanPago ent;
        ent = (especifico.entPlanPago)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setId((Integer)aValue); break;
            case 1: ent.setFecha((java.util.Date)aValue); break;
            case 2: ent.setFechavencimiento((java.util.Date)aValue); break;
            case 3: ent.setMontoinicial((Double)aValue); break;
            case 4: ent.setCantidadcuotas((Integer)aValue); break;
            case 5: ent.setMontoMensual((Double)aValue); break;
            case 6: ent.setMontoTotal((Double)aValue); break;
            case 7: ent.setSaldo((Double)aValue); break;
            case 8: ent.setEstado((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entPlanPago getEntidad(int rowIndex) {
        return (especifico.entPlanPago)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entPlanPago nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entPlanPago nuevo, int rowIndex) {
        nuevo.copiar((especifico.entPlanPago)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entPlanPago().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(7).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(8).setPreferredWidth(50);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public String getMensaje() {
        return this.smensaje;
    }

}
