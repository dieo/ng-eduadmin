/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class modcPedido implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";
    
    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entcPedido getEntidad() {
        return (especifico.entcPedido)(data.get(tbl.getSelectedRow()));
    }

    public void modificar(especifico.entcPedido nuevo) {
        nuevo.copiar((especifico.entcPedido)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "NRO PEDIDO";
            case 1: return "FECHA";
            case 2: return "TIPO PEDIDO";
            case 3: return "USUARIO";
            case 4: return "DEPENDENCIA";
            case 5: return "ESTADO PEDIDO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return java.util.Date.class;
            case 2: return String.class;
            case 3: return String.class;
            case 4: return String.class;
            case 5: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entcPedido ent;
        ent = (especifico.entcPedido)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getId();
            case 1: return ent.getFecha();
            case 2: return ent.getTipoPedido();
            case 3: return ent.getUsuario();
            case 4: return ent.getDependencia();
            case 5: return ent.getEstadoPedido();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entcPedido ent;
        ent = (especifico.entcPedido)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setFecha((java.util.Date)aValue); break;
            case 1: ent.setId((Integer)aValue); break;
            case 2: ent.setTipoPedido((String)aValue); break;
            case 3: ent.setUsuario((String)aValue); break;
            case 4: ent.setDependencia((String)aValue); break;
            case 5: ent.setEstadoPedido((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entcPedido getEntidad(int rowIndex) {
        return (especifico.entcPedido)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entcPedido nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entcPedido nuevo, int rowIndex) {
        nuevo.copiar((especifico.entcPedido)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entcPedido().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(25);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(125);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(50);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
    }
    
    public String getMensaje() {
        return this.smensaje;
    }

}
