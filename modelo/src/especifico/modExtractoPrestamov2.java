/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modExtractoPrestamov2 implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    public especifico.modExtractoPrestamov2 prestamo;
    public especifico.modExtractoPrestamov2 operacion;
    public especifico.modExtractoAportev2 aporte;

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entExtractov2 getEntidad() {
        return (especifico.entExtractov2)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entExtractov2 nuevo) {
        nuevo.copiar((especifico.entExtractov2)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 13;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "CUENTA";
            case 1: return "RUBRO";
            case 2: return "NRO.OP.";
            case 3: return "FEC.EMI.";
            case 4: return "PLAZO";
            case 5: return "TASA";
            case 6: return "CANC.";
            case 7: return "MONTO";
            case 8: return "COBRO";
            case 9: return "CERRADO";
            case 10: return "ATRASO";
            case 11: return "ACTUAL";
            case 12: return "SALDO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return String.class;
            case 2: return Integer.class;
            case 3: return java.util.Date.class;
            case 4: return Integer.class;
            case 5: return Double.class;
            case 6: return Boolean.class;
            case 7: return Double.class;
            case 8: return Double.class;
            case 9: return Double.class;
            case 10: return Double.class;
            case 11: return Double.class;
            case 12: return Double.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entExtractov2 ent;
        ent = (especifico.entExtractov2)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getDenominacion();
            case 1: return ent.getRubro();
            case 2: return ent.getNumeroOperacion();
            case 3: return ent.getFechaOperacion();
            case 4: return ent.getPlazo();
            case 5: return ent.getTasaInteres();
            case 6: return ent.getCancelacion();
            case 7: return ent.getMontoTotal();
            case 8: return ent.getCobro();
            case 9: return ent.getCerrado();
            case 10: return ent.getAtraso();
            case 11: return ent.getActual();
            case 12: return ent.getSaldo();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entExtractov2 ent;
        ent = (especifico.entExtractov2)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setDenominacion((String)aValue); break;
            case 1: ent.setRubro((String)aValue); break;
            case 2: ent.setNumeroOperacion((Integer)aValue); break;
            case 3: ent.setFechaOperacion((java.util.Date)aValue); break;
            case 4: ent.setPlazo((Integer)aValue); break;
            case 5: ent.setTasaInteres((Double)aValue); break;
            case 6: ent.setCancelacion((Boolean)aValue); break;
            case 7: ent.setMontoTotal((Double)aValue); break;
            case 8: ent.setCobro((Double)aValue); break;
            case 9: ent.setCerrado((Double)aValue); break;
            case 10: ent.setAtraso((Double)aValue); break;
            case 11: ent.setActual((Double)aValue); break;
            case 12: ent.setSaldo((Double)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entExtractov2 getEntidad(int rowIndex) {
        return (especifico.entExtractov2)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entExtractov2 nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entExtractov2 nuevo, int rowIndex) {
        nuevo.copiar((especifico.entExtractov2)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        //this.removerTodo();
        String stipo = "";
        this.prestamo = new especifico.modExtractoPrestamov2();
        this.operacion = new especifico.modExtractoPrestamov2();
        this.aporte = new especifico.modExtractoAportev2();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                stipo = rs.getString("tipo");
                if (stipo.equals("P")) {
                    this.prestamo.insertar(new especifico.entExtractov2().cargar(rs));
                }
                if (stipo.equals("O")) {
                    this.operacion.insertar(new especifico.entExtractov2().cargar(rs));
                }
                if (stipo.equals("A")) {
                    this.aporte.insertar(new especifico.entExtractov2().cargar(rs));
                }
            }
        } catch (Exception e) {
        }
    }

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(100);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(1);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(2);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(2);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(1);
        tbl.getColumnModel().getColumn(7).setPreferredWidth(45);
        tbl.getColumnModel().getColumn(8).setPreferredWidth(45);
        tbl.getColumnModel().getColumn(9).setPreferredWidth(45);
        tbl.getColumnModel().getColumn(10).setPreferredWidth(45);
        tbl.getColumnModel().getColumn(11).setPreferredWidth(45);
        tbl.getColumnModel().getColumn(12).setPreferredWidth(45);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1,8));
        tbl.setFont(new java.awt.Font("Tahoma", 1, 8));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public especifico.entExtractov2 getTotales() {
        especifico.entExtractov2 ent = new especifico.entExtractov2();
        for (int i=0; i<this.getRowCount(); i++) {
            ent.setMontoTotal(ent.getMontoTotal()+this.getEntidad(i).getMontoTotal());
            ent.setCobro(ent.getCobro()+this.getEntidad(i).getCobro());
            ent.setAtraso(ent.getAtraso()+this.getEntidad(i).getAtraso());
            ent.setCerrado(ent.getCerrado()+this.getEntidad(i).getCerrado());
            ent.setActual(ent.getActual()+this.getEntidad(i).getActual());
            ent.setSaldo(ent.getSaldo()+this.getEntidad(i).getSaldo());
        }
        return ent;
    }
    
    public void eliminarOperaciones() {
        int i=0;
        while (i<this.getRowCount()) { // elimina las operaciones con plazo mayor a 0 y no tengan saldo pendiente
            if (this.getEntidad(i).getPlazo()>0 && this.getEntidad(i).getAtraso()+this.getEntidad(i).getActual()+this.getEntidad(i).getSaldo()==0) {
                this.eliminar(i);
            } else {
                i++;
            }
        }
    }

}
