/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class tipo {
    
    public static final String claseProducto_ = "CPR";
    public class claseProducto {
        public class subtipo {
            public static final String egreso = "EGR";
            public static final String ingreso = "ING";
        }
    }
    
    public static final String estadoAhorroProgramado_ = "EAP";
    public class estadoAhorroProgramado {
        public class subtipo {
            public static final String activo = "ACT";
            public static final String anulado = "ANU";
            public static final String cumplido = "CUM";
            public static final String retirado = "RET";
        }
    }
    
    public static final String estadoBeneficiario_ = "EBE";
    public class estadoBeneficiario {
        public class subtipo {
            public static final String beneficiario = "BEN";
            public static final String inactivo = "INA";
        }
    }
    
    public static final String estadoCda_ = "ECD";
    public class estadoCda {
        public class subtipo {
            public static final String anulado = "ANU";
            public static final String noCorresponde = "NCO";
            public static final String renovado = "REN";
            public static final String retirado = "RET";
            public static final String vencido = "VEN";
            public static final String vigente = "VIG";
        }
    }
    
    public static final String estadoCheque_ = "ECH";
    public class estadoCheque {
        public class subtipo {
            public static final String guion = "---";
            public static final String anulado = "ANU";
            public static final String impreso = "IMP";
        }
    }
    
    public static final String estadoDepositoCuenta_ = "EDC";
    public class estadoDepositoCuenta {
        public class subtipo {
            public static final String activo = "ACT";
            public static final String anulado = "ANU";
        }
    }
    
    public static final String estadoEventoFormulario_ = "EEF";
    public class estadoEventoFormulario {
        public class subtipo {
            public static final String anulado = "ANU";
            public static final String aprobado = "APR";
            public static final String corregido = "COR";
            public static final String verificado = "VER";
        }
    }
    
    public static final String estadoFactura_ = "EFA";
    public class estadoFactura {
        public class subtipo {
            public static final String activo = "ACT";
            public static final String anulado = "ANU";
        }
    }
    
    public static final String estadoFondoJuridicoSepelio_ = "EJS";
    public class estadoFondoJuridicoSepelio {
        public class subtipo {
            public static final String activo = "ACT";
            public static final String anulado = "ANU";
            public static final String cumplido = "CUM";
            public static final String renunciado = "REN";
        }
    }
    
    public static final String estadoFuncionario_ = "EFU";
    public class estadoFuncionario {
        public class subtipo {
            public static final String activo = "ACT";
            public static final String despedido = "DES";
            public static final String renunciado = "REN";
        }
    }
    
    public static final String estadoIngreso_ = "EIN";
    public class estadoIngreso {
        public class subtipo {
            public static final String anulado = "ANU";
            public static final String emitido = "EMI";
            public static final String pendiente = "PEN";
        }
    }
    
    public static final String estadoMovimientoCuenta_ = "EMC";
    public class estadoMovimientoCuenta {
        public class subtipo {
            public static final String activo = "ACT";
            public static final String anulado = "ANU";
        }
    }
    
    public static final String estadoNotaCredito_ = "ENC";
    public class estadoNotaCredito {
        public class subtipo {
            public static final String activo = "ACT";
            public static final String anulado = "ANU";
        }
    }
    
    public static final String estadoOperacionFija_ = "EOF";
    public class estadoOperacionFija {
        public class subtipo {
            public static final String activo = "ACT";
            public static final String anulado = "ANU";
            public static final String cumplido = "CUM";
            public static final String inactivo = "INA";
            public static final String renunciado = "REN";
        }
    }
    
    public static final String estadoOrdenCredito_ = "EOC";
    public class estadoOrdenCredito {
        public class subtipo {
            public static final String anulado = "ANU";
            public static final String aprobado = "APR";
            public static final String generado = "GEN";
            public static final String pendienteAnalisis = "PAN";
            public static final String pendienteGeneracion = "PGE";
            public static final String rechazado = "REC";
        }
    }
    
    public static final String estadoOrdenPago_ = "EOP";
    public class estadoOrdenPago {
        public class subtipo {
            public static final String anulado = "ANU";
            public static final String confirmado = "CON";
            public static final String pendiente = "PEN";
        }
    }
    
    public static final String estadoPrestamo_ = "EPR";
    public class estadoPrestamo {
        public class subtipo {
            public static final String anulado = "ANU";
            public static final String aprobado = "APR";
            public static final String generado = "GEN";
            public static final String pendienteAnalisis = "PAN";
            public static final String pendienteGeneracion = "PGE";
            public static final String rechazado = "REC";
        }
    }

    public static final String estadoRecibo_ = "ERE";
    public class estadoRecibo {
        public class subtipo {
            public static final String activo = "ACT";
            public static final String anulado = "ANU";
        }
    }

    public static final String estadoSepelio_ = "ESE";
    public class estadoSepelio {
        public class subtipo {
            public static final String anulado = "ANU";
            public static final String aprobado = "APR";
            public static final String pendienteAnalisis = "PAN";
            public static final String rechazado = "REC";
        }
    }

    public static final String estadoSocio_ = "ESO";
    public class estadoSocio {
        public class subtipo {
            public static final String guion = "---";
            public static final String activo = "ACT";
            public static final String asesoriaJuridica = "AJU";
            public static final String cobroJudicial = "CJU";
            public static final String excluido = "EXC";
            public static final String expulsado = "EXP";
            public static final String fallecido = "FAL";
            public static final String gestionCobro = "GCO";
            public static final String incobrable = "INC";
            public static final String judicial = "JUD";
            public static final String pendienteRenuncia = "PRE";
            public static final String retirado = "RET";
            public static final String tramiteJubilatorio = "TJU";
        }
    }

    public static final String estadoSolidaridad_ = "ESL";
    public class estadoSolidaridad {
        public class subtipo {
            public static final String anulado = "ANU";
            public static final String aprobado = "APR";
            public static final String pendienteAnalisis = "PAN";
            public static final String rechazado = "REC";
        }
    }

}
