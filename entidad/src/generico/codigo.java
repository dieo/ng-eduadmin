/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package generico;

/**
 *
 * @author Informatica
 */
public class codigo {

    public static final int Predeterminado_000 = 0;
    public static final int AporteCapital_ACA = 1;
    public static final int AporteCuotaSocial_ACS = 2;
    public static final int Solidaridad_SOL = 3;
    public static final int FondoPrevision_FYN = 4;
    public static final int CuotaIngreso_CIN = 5;
    public static final int GastoAdministrativo_GAD = 6;
    public static final int MantenimientoSede_MSE = 7;
    public static final int PrestamoMutual_PMU = 8;
    public static final int PrestamoFinanciera_PFI = 9;
    public static final int OrdenCredito_OCR = 10;
    public static final int RetencionPrestamo_RPR = 11;
    public static final int RetencionOrdenCredito_ROC = 12;
    public static final int InteresPrestamo_IPR = 13;
    public static final int InteresMoratorio_IMO = 14;
    public static final int InteresPunitorio_IPU = 15;
    public static final int AhorroProgramado_APR = 16;
    public static final int FondoJuridicoSepelio_FJS = 17;
    public static final int ImpuestoValorAgregadoInteresCompensatorio_IIC = 18;
    
    public static final String getNombre(int i) {
        Class clase;
        try {
            clase = Class.forName("generico.codigo");
            return clase.getDeclaredFields()[i].getName().substring(clase.getDeclaredFields()[i].getName().length()-3); 
        } catch (Exception e) { }
        return "";
    }

    public static final java.util.ArrayList getNombre() {
        java.util.ArrayList lst = new java.util.ArrayList();
        Class clase;
        try {
            clase = Class.forName("generico.codigo");
            for (int i=0; i<clase.getDeclaredFields()[i].getName().length(); i++) {
                lst.add(clase.getDeclaredFields()[i].getName().substring(clase.getDeclaredFields()[i].getName().length()-3));
            }
        } catch (Exception e) { }
        return lst;
    }

}
