/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entLista {

    public static final short tipo_texto = 0;
    public static final short tipo_numero = 1;
    public static final short tipo_fecha = 2;
    public static final short tipo_boolean = 3;
    
    private int iid;
    private String sdescripcion;
    private String scampo;
    private short htipo;

    public entLista(){
        this.iid = 0;
        this.sdescripcion = "";
        this.scampo = "";
        this.htipo = tipo_texto;
    }

    public entLista(int iid, String sdescripcion, String scampo, short htipo){
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.scampo = scampo;
        this.htipo = htipo;
    }

    public entLista cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setCampo(""); }
        catch(Exception e) {}
        try { this.setTipo((short)0); }
        catch(Exception e) {}
        return this;
    }
    
    @Override
    public String toString() {
        return this.getDescripcion();
    }

    public void setId(int iid){
        this.iid = iid;
    }

    public void setDescripcion(String sdescripcion){
        this.sdescripcion = sdescripcion;
    }

    public void setCampo(String scampo){
        this.scampo = scampo;
    }

    public void setTipo(short htipo){
        this.htipo = htipo;
    }

    public int getId(){
        return this.iid;
    }

    public String getDescripcion(){
        return this.sdescripcion;
    }

    public String getCampo(){
        return this.scampo;
    }

    public short getTipo(){
        return this.htipo;
    }

}
