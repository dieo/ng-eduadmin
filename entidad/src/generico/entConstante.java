

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */

public class entConstante {
    
    // ESTADOS DE REGISTRO
    public static final short estadoregistro_predeterminado = 0;
    public static final short estadoregistro_pendiente = 1;
    public static final short estadoregistro_insertado = 2;
    public static final short estadoregistro_modificado = 3;
    public static final short estadoregistro_eliminado = 4;

//    public static String port = "1080";
    public static String port = "5432";
    //public static String host = "";
    public static String host = "localhost";
//    public static String host = "190.128.212.110";
//    public static String host = "192.168.10.6";
//    public static String baseDato = "MutualMSPBS";
//    public static String baseDato = "MutualMSPBS02Mar21";
    public static String baseDato = "Mutualmspjun";
    //public static String baseDato = "MutualPrueba";
    //public static String baseDato = "MutualMSPBS02mar18";
    //public static String baseDato = "MutualMSP";

    public static void leer() {
        
    }
    
    /*public static void leer() {
        String ruta = "";
        java.io.File archivo = null;
        java.io.FileReader fr = null;
        java.io.BufferedReader br = null;
        try { // Apertura del fichero y creacion de BufferedReader para poder hacer una lectura comoda (disponer del metodo readLine()).
            ruta = new java.io.File(" ").getAbsolutePath();
            archivo = new java.io.File(ruta.trim()+"dist\\lib\\configuracion.txt");
            try {
                fr = new java.io.FileReader(archivo);
            } catch(Exception e) {
                crear();
                fr = new java.io.FileReader(archivo);
            }
            br = new java.io.BufferedReader(fr);
            String linea;
            while ((linea=br.readLine())!=null) {
                if (linea.substring(0,2).equals("04")) host = linea.substring(3).trim(); // servidor
                //if (linea.substring(0,2).equals("05")) host = linea.substring(3).trim(); // local
                if (linea.substring(0,2).equals("06")) baseDato = linea.substring(3).trim();
            }
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            try {
                if( null!=fr ) fr.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }*/
    
    public static void crear() {
        String ruta = "";
        String barra = "\\";
        String especial = "*#*";
        java.io.File archivo = null;
        java.io.FileWriter fw = null;
        java.io.PrintWriter pw = null;
        try {
            ruta = new java.io.File(" ").getAbsolutePath();
            while (ruta.indexOf(barra)>0) ruta = ruta.substring(0,ruta.indexOf(barra))+especial+ruta.substring(ruta.indexOf(barra)+barra.length(),ruta.length());
            while (ruta.indexOf(especial)>0) ruta = ruta.substring(0,ruta.indexOf(especial))+barra+barra+ruta.substring(ruta.indexOf(especial)+especial.length(),ruta.length());
            ruta = ruta.trim()+"dist\\lib\\";
            archivo = new java.io.File(ruta);
            archivo.mkdirs();
            fw = new java.io.FileWriter(ruta+"configuracion.txt");
            pw = new java.io.PrintWriter(fw);
            pw.println("01:org.postgresql.Driver");
            pw.println("02:jdbc:postgresql");
            pw.println("03:5432");
            pw.println("04:192.168.10.6");
            pw.println("05:localhost");
            pw.println("06:MutualMSPBS");
            pw.println("07:autenticador");
            pw.println("08:mUtualMsp");
        } catch (Exception e) {
        } finally {
            try {
                if (null != fw) fw.close();
            } catch (Exception e2) { }
        }
    }
    
    
}
