/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleSolidaridad {
    
    private int iid;
    private int iidSolidaridad;
    private String sdescripcion;
    private boolean bchequeado;
    private String sobservacion;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final int LONGITUD_OBSERVACION = 50;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Documento (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_CHEQUEADO = "Chequeado";
    public static final String TEXTO_OBSERVACION = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public entDetalleSolidaridad() {
        this.iid = 0;
        this.iidSolidaridad = 0;
        this.sdescripcion = "";
        this.bchequeado = false;
        this.sobservacion = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleSolidaridad(int iid, int iidSolidaridad, String sdescripcion, boolean bchequeado, String sobservacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidSolidaridad = iidSolidaridad;
        this.sdescripcion = sdescripcion;
        this.bchequeado = bchequeado;
        this.sobservacion = sobservacion;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidSolidaridad, String sdescripcion, boolean bchequeado, String sobservacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidSolidaridad = iidSolidaridad;
        this.sdescripcion = sdescripcion;
        this.bchequeado = bchequeado;
        this.sobservacion = sobservacion;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public entDetalleSolidaridad copiar(entDetalleSolidaridad destino) {
        destino.setEntidad(this.getId(), this.getIdSolidaridad(), this.getDescripcion(), this.getChequeado(), this.getObservacion(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleSolidaridad cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSolidaridad(rs.getInt("idsolidaridad")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setChequeado(rs.getBoolean("chequeado")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdSolidaridad(int iidSolidaridad) {
        this.iidSolidaridad = iidSolidaridad;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setChequeado(boolean bchequeado) {
        this.bchequeado = bchequeado;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getIdSolidaridad() {
        return this.iidSolidaridad;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public boolean getChequeado() {
        return this.bchequeado;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detallesolidaridad("+
            this.getId()+","+
            this.getIdSolidaridad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getChequeado()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
}
