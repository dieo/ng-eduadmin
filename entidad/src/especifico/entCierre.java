/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entCierre {

    private int iid;
    private int iidMovimiento;
    private int iidDetalleOperacion;
    private int icuota;
    private java.util.Date dfechaVencimiento;
    private double umontoCapital;
    private double umontoInteres;
    private double umontoSaldo;
    private int iidMoneda;
    private String stabla;
    private int inumeroOperacion;
    private int iplazo;
    private java.util.Date dfechaOperacion;
    private double utasaInteresMoratorio;
    private double utasaInteresPunitorio;
    private double uinteresMoratorio;
    private double uinteresPunitorio;
    private int iidCuenta;
    private boolean bmutual;
    private String sprioridad;
    private int iidEntidad;
    private int iidFechaCierre;
    private java.util.Date dfechaCierre;
    private int iidSocio;
    private int iidGiraduria;
    private int iidRubro;
    private boolean bpresentado;
    private String scedula;
    private int iidEstadoSocio;
    private short hestadoRegistro;
    
    public entCierre() {
        this.iid = 0;
        this.iidMovimiento = 0;
        this.iidDetalleOperacion = 0;
        this.icuota = 0;
        this.dfechaVencimiento = null;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.umontoSaldo = 0.0;
        this.iidMoneda = 0;
        this.stabla = "";
        this.inumeroOperacion = 0;
        this.iplazo = 0;
        this.dfechaOperacion = null;
        this.utasaInteresMoratorio = 0.0;
        this.utasaInteresPunitorio = 0.0;
        this.uinteresMoratorio = 0.0;
        this.uinteresPunitorio = 0.0;
        this.iidCuenta = 0;
        this.bmutual = false;
        this.sprioridad = "";
        this.iidEntidad = 0;
        this.iidFechaCierre = 0;
        this.dfechaCierre = null;
        this.iidSocio = 0;
        this.iidGiraduria = 0;
        this.iidRubro = 0;
        this.bpresentado = false;
        this.scedula = "";
        this.iidEstadoSocio = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entCierre(int iid, int iidMovimiento, int iidDetalleOperacion, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double umontoSaldo, int iidMoneda, String stabla, int inumeroOperacion, int iplazo, java.util.Date dfechaOperacion, double utasaInteresMoratorio, double utasaInteresPunitorio, double uinteresMoratorio, double uinteresPunitorio, int iidCuenta, boolean bmutual, String sprioridad, int iidEntidad, int iidFechaCierre, java.util.Date dfechaCierre, int iidSocio, int iidGiraduria, int iidRubro, boolean bpresentado, String scedula, int iidEstadoSocio, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoSaldo = umontoSaldo;
        this.iidMoneda = iidMoneda;
        this.stabla = stabla;
        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.uinteresMoratorio = uinteresMoratorio;
        this.uinteresPunitorio = uinteresPunitorio;
        this.iidCuenta = iidCuenta;
        this.bmutual = bmutual;
        this.sprioridad = sprioridad;
        this.iidEntidad = iidEntidad;
        this.iidFechaCierre = iidFechaCierre;
        this.dfechaCierre = dfechaCierre;
        this.iidSocio = iidSocio;
        this.iidGiraduria = iidGiraduria;
        this.iidRubro = iidRubro;
        this.bpresentado = bpresentado;
        this.scedula = scedula;
        this.iidEstadoSocio = iidEstadoSocio;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidMovimiento, int iidDetalleOperacion, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double umontoSaldo, int iidMoneda, String stabla, int inumeroOperacion, int iplazo, java.util.Date dfechaOperacion, double utasaInteresMoratorio, double utasaInteresPunitorio, double uinteresMoratorio, double uinteresPunitorio, int iidCuenta, boolean bmutual, String sprioridad, int iidEntidad, int iidFechaCierre, java.util.Date dfechaCierre, int iidSocio, int iidGiraduria, int iidRubro, boolean bpresentado, String scedula, int iidEstadoSocio, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoSaldo = umontoSaldo;
        this.iidMoneda = iidMoneda;
        this.stabla = stabla;
        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.uinteresMoratorio = uinteresMoratorio;
        this.uinteresPunitorio = uinteresPunitorio;
        this.iidCuenta = iidCuenta;
        this.bmutual = bmutual;
        this.sprioridad = sprioridad;
        this.iidEntidad = iidEntidad;
        this.iidFechaCierre = iidFechaCierre;
        this.dfechaCierre = dfechaCierre;
        this.iidSocio = iidSocio;
        this.iidGiraduria = iidGiraduria;
        this.iidRubro = iidRubro;
        this.bpresentado = bpresentado;
        this.scedula = scedula;
        this.iidEstadoSocio = iidEstadoSocio;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entCierre copiar(entCierre destino) {
        destino.setEntidad(this.getId(), this.getIdMovimiento(), this.getIdDetalleOperacion(), this.getCuota(), this.getFechaVencimiento(), this.getMontoCapital(), this.getMontoInteres(), this.getMontoSaldo(), this.getIdMoneda(), this.getTabla(),  this.getNumeroOperacion(), this.getPlazo(), this.getFechaOperacion(), this.getTasaInteresMoratorio(), this.getTasaInteresPunitorio(), this.getInteresMoratorio(), this.getInteresPunitorio(), this.getIdCuenta(), this.getMutual(), this.getPrioridad(), this.getIdEntidad(), this.getIdFechaCierre(), this.getFechaCierre(), this.getIdSocio(), this.getIdGiraduria(), this.getIdRubro(), this.getPresentado(), this.getCedula(), this.getIdEstadoSocio(), this.getEstadoRegistro());
        return destino;
    }

    public entCierre cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) { }
        try { this.setIdDetalleOperacion(rs.getInt("iddetalleoperacion")); }
        catch(Exception e) { }
        try { this.setCuota(rs.getInt("cuota")); }
        catch(Exception e) { }
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) { }
        try { this.setMontoCapital(rs.getDouble("montocapital")); }
        catch(Exception e) { }
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) { }
        try { this.setMontoSaldo(rs.getDouble("saldo")); }
        catch(Exception e) { }
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) { }
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) { }
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) { }
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) { }
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) { }
        try { this.setTasaInteresMoratorio(rs.getDouble("tasainteresmoratorio")); }
        catch(Exception e) { }
        try { this.setTasaInteresPunitorio(rs.getDouble("tasainterespunitorio")); }
        catch(Exception e) { }
        try { this.setInteresMoratorio(rs.getDouble("interesmoratorio")); }
        catch(Exception e) { }
        try { this.setInteresPunitorio(rs.getDouble("interespunitorio")); }
        catch(Exception e) { }
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) { }
        try { this.setMutual(rs.getBoolean("mutual")); }
        catch(Exception e) { }
        try { this.setPrioridad(rs.getString("prioridad")); }
        catch(Exception e) { }
        try { this.setIdEntidad(rs.getInt("identidad")); }
        catch(Exception e) { }
        try { this.setIdFechaCierre(rs.getInt("idfechacierre")); }
        catch(Exception e) { }
        try { this.setFechaCierre(rs.getDate("fechacierre")); }
        catch(Exception e) { }
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) { }
        try { this.setIdGiraduria(rs.getInt("idgiraduria")); }
        catch(Exception e) { }
        try { this.setIdRubro(rs.getInt("idrubro")); }
        catch(Exception e) { }
        try { this.setPresentado(rs.getBoolean("presentado")); }
        catch(Exception e) { }
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) { }
        try { this.setIdEstadoSocio(rs.getInt("idestadosocio")); }
        catch(Exception e) { }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }
    
    public void setIdDetalleOperacion(int iidDetalleOperacion) {
        this.iidDetalleOperacion = iidDetalleOperacion;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }
    
    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }
    
    public void setMontoSaldo(double umontoSaldo) {
        this.umontoSaldo = umontoSaldo;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }
    
    public void setTasaInteresMoratorio(double utasaInteresMoratorio) {
        this.utasaInteresMoratorio = utasaInteresMoratorio;
    }
    
    public void setTasaInteresPunitorio(double utasaInteresPunitorio) {
        this.utasaInteresPunitorio = utasaInteresPunitorio;
    }
    
    public void setInteresMoratorio(double uinteresMoratorio) {
        this.uinteresMoratorio = uinteresMoratorio;
    }

    public void setInteresPunitorio(double uinteresPunitorio) {
        this.uinteresPunitorio = uinteresPunitorio;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setMutual(boolean bmutual) {
        this.bmutual = bmutual;
    }

    public void setPrioridad(String sprioridad) {
        this.sprioridad = sprioridad;
    }

    public void setIdEntidad(int iidEntidad) {
        this.iidEntidad = iidEntidad;
    }

    public void setIdFechaCierre(int iidFechaCierre) {
        this.iidFechaCierre = iidFechaCierre;
    }

    public void setFechaCierre(java.util.Date dfechaCierre) {
        this.dfechaCierre = dfechaCierre;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }

    public void setIdRubro(int iidRubro) {
        this.iidRubro = iidRubro;
    }

    public void setPresentado(boolean bpresentado) {
        this.bpresentado = bpresentado;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setIdEstadoSocio(int iidEstadoSocio) {
        this.iidEstadoSocio = iidEstadoSocio;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getIdMovimiento() {
        return this.iidMovimiento;
    }
    
    public int getIdDetalleOperacion() {
        return this.iidDetalleOperacion;
    }

    public int getCuota() {
        return this.icuota;
    }
    
    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }
    
    public double getMontoSaldo() {
        return this.umontoSaldo;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getTabla() {
        return this.stabla;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }
    
    public double getTasaInteresMoratorio() {
        return this.utasaInteresMoratorio;
    }
    
    public double getTasaInteresPunitorio() {
        return this.utasaInteresPunitorio;
    }
    
    public double getInteresMoratorio() {
        return this.uinteresMoratorio;
    }

    public double getInteresPunitorio() {
        return this.uinteresPunitorio;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public boolean getMutual() {
        return this.bmutual;
    }

    public String getPrioridad() {
        return this.sprioridad;
    }

    public int getIdEntidad() {
        return this.iidEntidad;
    }

    public int getIdFechaCierre() {
        return this.iidFechaCierre;
    }

    public java.util.Date getFechaCierre() {
        return this.dfechaCierre;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public int getIdRubro() {
        return this.iidRubro;
    }

    public boolean getPresentado() {
        return this.bpresentado;
    }
    
    public String getCedula() {
        return this.scedula;
    }
    
    public int getIdEstadoSocio() {
        return this.iidEstadoSocio;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getSentencia() {
        return "SELECT cierre("+
            this.getId()+","+
            this.getIdMovimiento()+","+
            this.getIdDetalleOperacion()+","+
            this.getCuota()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaVencimiento())+","+
            this.getMontoCapital()+","+
            this.getMontoInteres()+","+
            this.getMontoSaldo()+","+
            this.getIdMoneda()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getNumeroOperacion()+","+
            this.getPlazo()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaOperacion())+","+
            this.getTasaInteresMoratorio()+","+
            this.getTasaInteresPunitorio()+","+
            this.getInteresMoratorio()+","+
            this.getInteresPunitorio()+","+
            this.getIdCuenta()+","+
            this.getMutual()+","+
            this.getIdFechaCierre()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaCierre())+","+
            this.getIdSocio()+","+
            this.getIdGiraduria()+","+
            this.getIdRubro()+","+
            this.getPresentado()+","+
            this.getIdEntidad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            this.getIdEstadoSocio()+","+
            this.getEstadoRegistro()+")";
    }
    
    public String getConsulta(java.util.Date dfechaCorte) {
        return "SELECT * FROM vwoperacionmaster WHERE saldocapital+saldointeres>0" +
            " AND fechavencimiento<=" + utilitario.utiFecha.getFechaGuardadoMac(dfechaCorte);
    }

    /*public String getConsultaXXX(java.util.Date dfecha, double utasaInteresPunitorio, int iidInteres, String sinteres, String sprioridad, int iidEstadoOrdenCredito, int iidEstadoPrestamo, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, mo.idsocio, ll.idrubro, ll.idgiraduria, mo.idmoneda, mo.id AS idmovimiento, 0 AS idmovimientocancela, mo.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+utasaInteresPunitorio+" AS tasainterespunitorio, mo.tasainteres AS tasainteresmoratorio, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idestado="+iidEstadoOrdenCredito+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON mo.idsocio=s.id LEFT JOIN lugarlaboral AS ll ON s.id=ll.idsocio "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+") AS ci ON dop.id=ci.iddetalleoperacion "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 0 AS id, mo.idsocio, ll.idrubro, ll.idgiraduria, mo.idmoneda, mo.id AS idmovimiento, 0 AS idmovimientocancela, mo.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+utasaInteresPunitorio+" AS tasainterespunitorio, mo.tasainteres AS tasainteresmoratorio, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idestado="+iidEstadoPrestamo+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON mo.idsocio=s.id LEFT JOIN lugarlaboral AS ll ON s.id=ll.idsocio "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta=mo.idcuenta "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 0 AS id, mo.idsocio, ll.idrubro, ll.idgiraduria, mo.idmoneda, mo.id AS idmovimiento, 0 AS idmovimientocancela, "+iidInteres+" AS idcuenta, '"+sinteres+"' AS cuenta, '"+sprioridad+"' AS prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+utasaInteresPunitorio+" AS tasainterespunitorio, mo.tasainteres AS tasainteresmoratorio, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, ci.saldo AS montocobro, tc.exoneracion AS tasaexoneracion "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idestado="+iidEstadoPrestamo+") AS mo LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito=tc.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON mo.idsocio=s.id LEFT JOIN lugarlaboral AS ll ON s.id=ll.idsocio "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta="+iidInteres+" "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 0 AS id, ap.idsocio, ll.idrubro, ll.idgiraduria, ap.idmoneda, ap.id AS idmovimiento, 0 AS idmovimientocancela, ap.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, ap.fechaoperacion, 0 AS tasainterespunitorio, 0 AS tasainteresmoratorio, ap.plazo, ap.numerooperacion, 0 AS tasaimpuesto, ap.importe*ap.plazo AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM ahorroprogramado WHERE idestado="+iidEstadoAhorro+") AS ap LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=ap.id "+
                       "LEFT JOIN cuenta AS c ON ap.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON ap.idsocio=s.id LEFT JOIN lugarlaboral AS ll ON s.id=ll.idsocio "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta=ap.idcuenta "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='ap' "+
                   "UNION " +
                   "SELECT 0 AS id, of.idsocio, ll.idrubro, ll.idgiraduria, of.idmoneda, of.id AS idmovimiento, 0 AS idmovimientocancela, of.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, of.fechaoperacion, 0 AS tasainterespunitorio, 0 AS tasainteresmoratorio, of.plazo, of.numerooperacion, 0 AS tasaimpuesto, of.importe AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM operacionfija WHERE idestado="+iidEstadoOperacion+") AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON of.idsocio=s.id LEFT JOIN lugarlaboral AS ll ON s.id=ll.idsocio "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta=of.idcuenta "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='of' "+
                   "UNION " +
                   "SELECT 0 AS id, js.idsocio, ll.idrubro, ll.idgiraduria, js.idmoneda, js.id AS idmovimiento, 0 AS idmovimientocancela, js.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, js.fechaoperacion, 0 AS tasainterespunitorio, 0 AS tasainteresmoratorio, 0 AS plazo, js.numerooperacion, 0 AS tasaimpuesto, js.importetitular+js.importeadherente*js.cantidadadherente AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM fondojuridicosepelio WHERE idestado="+iidEstadoFondo+") AS js LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=js.id "+
                       "LEFT JOIN cuenta AS c ON js.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON js.idsocio=s.id LEFT JOIN lugarlaboral AS ll ON s.id=ll.idsocio "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta=js.idcuenta "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='js' "+
               ") AS detalle "+sorden;
    }*/

    
    
    //    CORREGIR MONTOSALDO -> SALDO EN TODAS LAS CONSULTAS QUE ESTAN DEBAJO
    
    
    
    public String getConsultaInteresPrestamo(java.util.Date dfecha, int iidInteres, String sinteres, String sprioridad, int iidMutual, int iidEstadoPrestamo, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, mo.idsocio, s.cedula, s.idestadosocio, ll.idrubro, ll.idgiraduria, mo.idmoneda, mo.id AS idmovimiento, 0 AS idmovimientocancela, "+iidInteres+" AS idcuenta, '"+sinteres+"' AS cuenta, '"+sprioridad+"' AS prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS saldo, 0 AS montocobro, tc.exoneracion AS tasaexoneracion, 0 AS tasainteresmoratorio, 0 AS tasainterespunitorio "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idestado="+iidEstadoPrestamo+" AND identidad="+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito=tc.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN socio AS s ON mo.idsocio=s.id LEFT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE') AS ll ON s.id=ll.idsocio "+
                       //"LEFT JOIN socio AS s ON mo.idsocio=s.id RIGHT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE' AND idgiraduria=6) AS ll ON s.id=ll.idsocio "+
                       "WHERE (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) AND mo.fechasolicitud>=s.fechaingreso AND dop.saldointeres>0 AND dop.tabla='mo' AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+
               ") AS detalle "+sorden;
    }

    public String getConsultaPrestamoMutual(java.util.Date dfecha, double utasaInteresPunitorio, int iidMutual, int iidEstadoPrestamo, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, mo.idsocio, s.cedula, s.idestadosocio, ll.idrubro, ll.idgiraduria, mo.idmoneda, mo.id AS idmovimiento, 0 AS idmovimientocancela, mo.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS saldo, 0 AS montocobro, 0 AS tasaexoneracion, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres "+
                       "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                       "     ELSE mo.tasainteres END AS tasainteresmoratorio, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres*"+utasaInteresPunitorio+"/100 "+
                       "     WHEN mo.tasainteres>5 THEN (mo.tasainteres/mo.plazoaprobado)*"+utasaInteresPunitorio+"/100 "+
                       "     ELSE mo.tasainteres*"+utasaInteresPunitorio+"/100 END AS tasainterespunitorio "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idestado="+iidEstadoPrestamo+" AND identidad="+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN socio AS s ON mo.idsocio=s.id LEFT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE') AS ll ON s.id=ll.idsocio AND ll.activo='TRUE' "+
                       //"LEFT JOIN socio AS s ON mo.idsocio=s.id RIGHT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE' AND idgiraduria=6) AS ll ON s.id=ll.idsocio AND ll.activo='TRUE' "+
                       "WHERE (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) AND mo.fechasolicitud>=s.fechaingreso AND dop.saldocapital>0 AND dop.tabla='mo' AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+
               ") AS detalle "+sorden;
    }
    
    public String getConsultaPrestamoFinanciera(java.util.Date dfecha, int iidMutual, int iidEstadoPrestamo, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, mo.idsocio, s.cedula, s.idestadosocio, ll.idrubro, ll.idgiraduria, mo.idmoneda, mo.id AS idmovimiento, 0 AS idmovimientocancela, mo.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS saldo, 0 AS montocobro, 0 AS tasaexoneracion, 0 AS tasainteresmoratorio, 0 AS tasainterespunitorio "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idestado="+iidEstadoPrestamo+" AND identidad<>"+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN socio AS s ON mo.idsocio=s.id LEFT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE') AS ll ON s.id=ll.idsocio "+
                       //"LEFT JOIN socio AS s ON mo.idsocio=s.id RIGHT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE' AND idgiraduria=6) AS ll ON s.id=ll.idsocio "+
                       "WHERE (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) AND mo.fechasolicitud>=s.fechaingreso AND dop.saldocapital>0 AND dop.tabla='mo' AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+
               ") AS detalle "+sorden;
    }
    
    public String getConsultaOrdenCredito(java.util.Date dfecha, double utasaInteresPunitorio, int iidEstadoOrdenCredito, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, mo.idsocio, s.cedula, s.idestadosocio, ll.idrubro, ll.idgiraduria, mo.idmoneda, mo.id AS idmovimiento, 0 AS idmovimientocancela, mo.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS saldo, 0 AS montocobro, 0 AS tasaexoneracion, 0 AS tasainteresmoratorio, 0 AS tasainterespunitorio "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idestado="+iidEstadoOrdenCredito+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON mo.idsocio=s.id LEFT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE') AS ll ON s.id=ll.idsocio "+
                       //"LEFT JOIN socio AS s ON mo.idsocio=s.id RIGHT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE' AND idgiraduria=6) AS ll ON s.id=ll.idsocio "+
                       "WHERE (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) AND mo.fechasolicitud>=s.fechaingreso AND dop.saldocapital+dop.saldointeres>0 AND dop.tabla='mo' AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+
               ") AS detalle "+sorden;
    }

    public String getConsultaAhorroProgramado(java.util.Date dfecha, int iidEstadoAhorro, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, ap.idsocio, s.cedula, s.idestadosocio, ll.idrubro, ll.idgiraduria, ap.idmoneda, ap.id AS idmovimiento, 0 AS idmovimientocancela, ap.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, ap.fechaoperacion, ap.plazo, ap.numerooperacion, 0 AS tasaimpuesto, ap.importe*ap.plazo AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS saldo, 0 AS montocobro, 0 AS tasaexoneracion, 0 AS tasainteresmoratorio, 0 AS tasainterespunitorio "+
                       "FROM (SELECT * FROM ahorroprogramado WHERE idestado="+iidEstadoAhorro+") AS ap LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=ap.id "+
                       "LEFT JOIN cuenta AS c ON ap.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON ap.idsocio=s.id LEFT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE') AS ll ON s.id=ll.idsocio "+
                       "WHERE (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) AND dop.saldocapital+dop.saldointeres>0 AND dop.tabla='ap' AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+
               ") AS detalle "+sorden;
    }
    
    public String getConsultaOperacionFija(java.util.Date dfecha, int iidEstadoOperacion, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, of.idsocio, s.cedula, s.idestadosocio, ll.idrubro, ll.idgiraduria, of.idmoneda, of.id AS idmovimiento, 0 AS idmovimientocancela, of.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, of.fechaoperacion, of.plazo, of.numerooperacion, 0 AS tasaimpuesto, of.importe AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS saldo, 0 AS montocobro, 0 AS tasaexoneracion, 0 AS tasainteresmoratorio, 0 AS tasainterespunitorio "+
                       "FROM (SELECT * FROM operacionfija WHERE idcuenta<>17 AND idcuenta<>85 AND idestado="+iidEstadoOperacion+") AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON of.idsocio=s.id LEFT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE') AS ll ON s.id=ll.idsocio "+
                       "WHERE (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) AND of.idcuenta<>14 AND of.idcuenta<>48 AND dop.saldocapital+dop.saldointeres>0 AND dop.tabla='of' AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+
               ") AS detalle "+sorden;
    }
    
    public String getConsultaOperacionFijaSeguro(java.util.Date dfecha, int iidEstadoOperacion, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, of.idsocio, s.cedula, s.idestadosocio, ll.idrubro, ll.idgiraduria, of.idmoneda, of.id AS idmovimiento, 0 AS idmovimientocancela, of.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, of.fechaoperacion, of.plazo, of.numerooperacion, 0 AS tasaimpuesto, of.importe AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS saldo, 0 AS montocobro, 0 AS tasaexoneracion, 0 AS tasainteresmoratorio, 0 AS tasainterespunitorio "+
                       "FROM (SELECT * FROM operacionfija WHERE idcuenta<>17 AND idcuenta<>85 AND idestado="+iidEstadoOperacion+") AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON of.idsocio=s.id LEFT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE') AS ll ON s.id=ll.idsocio "+
                       "WHERE (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) AND (of.idcuenta=14 OR of.idcuenta=48) AND dop.saldocapital+dop.saldointeres>0 AND dop.tabla='of' AND dop.fechavencimiento="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+
               ") AS detalle "+sorden;
    }
    
    public String getConsultaOperacionFijaInteresMoratorioPunitorio(java.util.Date dfecha, int iidEstadoOperacion, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, of.idsocio, s.cedula, s.idestadosocio, ll.idrubro, ll.idgiraduria, of.idmoneda, of.id AS idmovimiento, 0 AS idmovimientocancela, of.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, of.fechaoperacion, of.plazo, of.numerooperacion, 0 AS tasaimpuesto, of.importe AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS saldo, 0 AS montocobro, 0 AS tasaexoneracion, 0 AS tasainteresmoratorio, 0 AS tasainterespunitorio "+
                       "FROM (SELECT * FROM operacionfija WHERE (idcuenta=17 OR idcuenta=85) AND idestado="+iidEstadoOperacion+") AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON of.idsocio=s.id LEFT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE') AS ll ON s.id=ll.idsocio "+
                       "WHERE (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) AND dop.saldocapital+dop.saldointeres>0 AND dop.tabla='of' AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+
               ") AS detalle "+sorden;
    }
    
    public String getConsultaFondoJuridico(java.util.Date dfecha, int iidEstadoFondo, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, js.idsocio, s.cedula, s.idestadosocio, ll.idrubro, ll.idgiraduria, js.idmoneda, js.id AS idmovimiento, 0 AS idmovimientocancela, js.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, js.fechaoperacion, 0 AS plazo, js.numerooperacion, 0 AS tasaimpuesto, js.importetitular+js.importeadherente*js.cantidadadherente AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS saldo, 0 AS montocobro, 0 AS tasaexoneracion, 0 AS tasainteresmoratorio, 0 AS tasainterespunitorio "+
                       "FROM (SELECT * FROM fondojuridicosepelio WHERE idestado="+iidEstadoFondo+") AS js LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=js.id "+
                       "LEFT JOIN cuenta AS c ON js.idcuenta=c.id "+
                       "LEFT JOIN socio AS s ON js.idsocio=s.id LEFT JOIN (SELECT * FROM lugarlaboral WHERE activo='TRUE') AS ll ON s.id=ll.idsocio "+
                       "WHERE (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) AND dop.saldocapital+dop.saldointeres>0 AND dop.tabla='js' AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(dfecha)+
               ") AS detalle "+sorden;
    }

    public String getConsultaComercioExterno(java.util.Date dfecha, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, so.id AS idsocio, oe.cedula, 20 AS idestadosocio, ll.idrubro, ll.idgiraduria, 1 AS idmoneda, oe.id AS idmovimiento, 0 AS idmovimientocancela, oe.idcuenta, c.descripcion AS cuenta, c.prioridad, doe.tabla, oe.fecha AS fechaoperacion, oe.plazo, 0 AS numerooperacion, 0 AS tasaimpuesto, oe.monto AS totalcapital, 0 AS totalinteres, doe.id AS iddetalleoperacion, doe.numerocuota AS cuota, doe.fechavencimiento, doe.montocapital, 0 AS montointeres, doe.saldocapital AS saldo, 0 AS montocobro, 0 AS tasaexoneracion, 0 AS tasainteresmoratorio, 0 AS tasainterespunitorio "+
                       "FROM operacionexterna AS oe LEFT JOIN detalleoperacionexterno AS doe ON doe.idmovimiento=oe.id "+
                       "LEFT JOIN cuenta AS c ON oe.idcuenta=c.id "+
                       "LEFT JOIN socio AS so ON oe.idsocio=so.id AND so.idestadosocio=20 "+
                       "LEFT JOIN lugarlaboral AS ll ON so.id=ll.idsocio AND ll.activo='TRUE' "+
                "WHERE doe.saldocapital>0 AND doe.tabla='ce' AND TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' "+
               ") AS detalle "+sorden;
    }
    
}
