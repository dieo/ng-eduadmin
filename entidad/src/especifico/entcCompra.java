/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entcCompra {

    private int iid;
    private int iidOrdenCompra;
    private int iidPresupuesto;
    private int iidFacturaEgreso;
    private String snumeroFactura;
    private int itimbrado;
    private java.util.Date dfechaFactura;
    private int iidProveedor;
    private String sproveedor;
    private String sruc;
    private int iidFilial;
    private String sfilial;
    //private int iidDependencia;
    //private String sdependencia;
    //private int iidTipoPedido;
    //private String stipoPedido;
    private java.util.Date dfecha;
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    private int iidMoneda;
    private String smoneda;
    private int iidEstadoCompra;
    private String sestadoCompra;
    private double utotalCompra;
    //private double ucambio;
    //private double utotalGs;
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_IDORDENCOMPRA = "Nº de Orden de Compra en que se basa la Compra (no vacía, valor positivo)";
    public static final String TEXTO_IDPRESUPUESTO = "Nº de Presupuesto en que se basa la OC (0 si no esta relacionado con Presupuesto)";
    public static final String TEXTO_NUMEROFACTURA = "Nº de Factura de Compra (no vacía)";
    public static final String TEXTO_FILIAL = "Filial para la que se realiza la Compra (no vacía)";
    //public static final String TEXTO_DEPENDENCIA = "Dependencia para la que se realiza la Compra (no vacía)";
    public static final String TEXTO_PROVEEDOR = "Proveedor del artículo (no vacía)";
    public static final String TEXTO_FECHA = "Fecha del presupuesto (no vacía)";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación de la OC (no editable)";
    public static final String TEXTO_TOTAL_COMPRA = "Monto total de la Compra (no editable)";
    //public static final String TEXTO_CAMBIO = "Monto del cambio del día de la moneda (no vacía, 1 si en Gs)";
    //public static final String TEXTO_TOTAL_GS = "Monto total de la OC en Gs (no editable)";
    public static final String TEXTO_MONEDA = "Moneda en que se cotiza la OC (no vacía)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Motivo de la Anulación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    //public static final String TEXTO_TIPO_PEDIDO = "Tipo de Pedido de la OC (no vacía)";
    public static final String TEXTO_ESTADO = "Estado de la Compra (no editable)";
    
    
    public entcCompra() {
        this.iid = 0;
        this.iidOrdenCompra = 0;
        this.iidPresupuesto = 0;
        this.iidFacturaEgreso = 0;
        this.snumeroFactura = "";
        this.itimbrado = 0;
        this.dfechaFactura = null;
        this.iidProveedor = 0;
        this.sproveedor = "";
        this.sruc = "";
        this.iidFilial = 0;
        this.sfilial = "";
        //this.iidDependencia = 0;
        //this.sdependencia = "";
        //this.iidTipoPedido = 0;
        //this.stipoPedido = "";
        this.dfecha = null;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iidEstadoCompra = 0;
        this.sestadoCompra = "";
        this.utotalCompra = 0.0;
        //this.ucambio = 0.0;
        //this.utotalGs = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entcCompra(int iid, int iidOrdenCompra, int iidPresupuesto, int iidFacturaEgreso, String snumeroFactura, int iidProveedor, String sproveedor, int iidFilial, String sfilial, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidMoneda,  String smoneda, int iidEstadoCompra, String sestadoCompra, double utotalCompra, short hestadoRegistro) {
        this.iid = iid;
        this.iidOrdenCompra = iidOrdenCompra;
        this.iidPresupuesto = iidPresupuesto;
        this.iidFacturaEgreso = iidFacturaEgreso;
        this.snumeroFactura = snumeroFactura;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        //this.iidDependencia = iidDependencia;
        //this.sdependencia = sdependencia;
        //this.iidTipoPedido = iidTipoPedido;
        //this.stipoPedido = stipoPedido;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidEstadoCompra = iidEstadoCompra;
        this.sestadoCompra = sestadoCompra;
        this.utotalCompra = utotalCompra;
        //this.ucambio = ucambio;
        //this.utotalGs = utotalGs;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidOrdenCompra, int iidPresupuesto, int iidFacturaEgreso, String snumeroFactura, int iidProveedor, String sproveedor, int iidFilial, String sfilial, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidMoneda,  String smoneda, int iidEstadoCompra, String sestadoCompra, double utotalCompra, short hestadoRegistro) {
        this.iid = iid;
        this.iidOrdenCompra = iidOrdenCompra;
        this.iidPresupuesto = iidPresupuesto;
        this.iidFacturaEgreso = iidFacturaEgreso;
        this.snumeroFactura = snumeroFactura;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        //this.iidDependencia = iidDependencia;
        //this.sdependencia = sdependencia;
        //this.iidTipoPedido = iidTipoPedido;
        //this.stipoPedido = stipoPedido;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidEstadoCompra = iidEstadoCompra;
        this.sestadoCompra = sestadoCompra;
        this.utotalCompra = utotalCompra;
        //this.ucambio = ucambio;
        //this.utotalGs = utotalGs;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entcCompra copiar(entcCompra destino) {
        destino.setEntidad(this.getId(), this.getIdOrdenCompra(), this.getIdPresupuesto(), this.getIdFacturaEgreso(), this.getNumeroFactura(), this.getIdProveedor(), this.getProveedor(), this.getIdFilial(), this.getFilial(), this.getFecha(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getIdMoneda(), this.getMoneda(), this.getIdEstadoCompra(), this.getEstadoCompra(), this.getTotalCompra(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcCompra cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdPresupuesto(rs.getInt("idpresupuesto")); }
        catch(Exception e) {}
        try { this.setIdOrdenCompra(rs.getInt("idordencompra")); }
        catch(Exception e) {}
        try { this.setIdFacturaEgreso(rs.getInt("idfacturaegreso")); }
        catch(Exception e) {}
        try { this.setNumeroFactura(rs.getString("numerofactura")); }
        catch(Exception e) {}
        try { this.setIdProveedor(rs.getInt("idproveedor")); }
        catch(Exception e) {}
        try { this.setProveedor(rs.getString("proveedor")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setIdEstadoCompra(rs.getInt("idestadocompra")); }
        catch(Exception e) {}
        try { this.setEstadoCompra(rs.getString("estadocompra")); }
        catch(Exception e) {}
        try { this.setTotalCompra(rs.getDouble("totalcompra")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdPresupuesto(int iidPresupuesto) {
        this.iidPresupuesto = iidPresupuesto;
    }

    public void setIdOrdenCompra(int iidOrdenCompra) {
        this.iidOrdenCompra = iidOrdenCompra;
    }
    
    public void setIdFacturaEgreso(int iidFacturaEgreso) {
        this.iidFacturaEgreso = iidFacturaEgreso;
    }
    
    public void setNumeroFactura(String snumeroFactura) {
        this.snumeroFactura = snumeroFactura;
    }

    public void setIdProveedor(int iidProveedor) {
        this.iidProveedor = iidProveedor;
    }
    
    public void setProveedor(String sproveedor) {
        this.sproveedor = sproveedor;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }
    
    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setIdEstadoCompra(int iidEstadoCompra) {
        this.iidEstadoCompra = iidEstadoCompra;
    }

    public void setEstadoCompra(String sestadoCompra) {
        this.sestadoCompra = sestadoCompra;
    }
    
    public void setTotalCompra(double utotalCompra) {
        this.utotalCompra = utotalCompra;
    }
        
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdPresupuesto() {
        return this.iidPresupuesto;
    }

    public int getIdOrdenCompra() {
        return this.iidOrdenCompra;
    }
    
    public int getIdFacturaEgreso() {
        return this.iidFacturaEgreso;
    }
    
    public String getNumeroFactura() {
        return this.snumeroFactura;
    }

    public int getIdProveedor() {
        return this.iidProveedor;
    }
    
    public String getProveedor() {
        return this.sproveedor;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }
    
    public String getFilial() {
        return this.sfilial;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public int getIdEstadoCompra() {
        return this.iidEstadoCompra;
    }

    public String getEstadoCompra() {
        return this.sestadoCompra;
    }

    public double getTotalCompra() {
        return this.utotalCompra;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getIdFilial()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FILIAL;
            bvalido = false;
        }
        if (this.getIdProveedor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_PROVEEDOR;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.compra("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            this.getIdOrdenCompra()+","+
            this.getIdPresupuesto()+","+
            this.getIdProveedor()+","+
            this.getIdFacturaEgreso()+","+
            this.getIdFilial()+","+
            this.getTotalCompra()+","+
            this.getIdMoneda()+","+
            this.getIdEstadoCompra()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha", "fecha", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Numero OC", "idordencompra", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(3, "Proveedor", "proveedor", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(4, "Estado", "estadocompra", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
