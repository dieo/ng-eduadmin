/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entcPedido {

    private int iid;
    private int iidUsuario;
    private String susuario;
    private int iidAutorizador;
    private String sautorizador;
    private int iidFilial;
    private String sfilial;
    private int iidFilial2;
    private String sfilial2;
    private int iidDependencia;
    private String sdependencia;
    private int iidTipoPedido;
    private String stipoPedido;
    private java.util.Date dfecha;
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    private int iidEstadoPedido;
    private String sestadoPedido;
    private String shora;
    private int iidDocumento;
    private String snumeroDocumento;
    private int iidFuncionario;
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_USUARIO = "Usuario que solicita (no vacía)";
    public static final String TEXTO_AUTORIZADOR = "Persona que autoriza el pedido";
    public static final String TEXTO_FILIAL = "Ubicación del stock del artículo solicitado (no vacía)";
    public static final String TEXTO_FILIAL2 = "Filial (no vacía)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia (no vacía)";
    public static final String TEXTO_FECHA = "Fecha del pedido (no vacía)";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Motivo de la Anulación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_TIPO_PEDIDO = "Tipo de Pedido (no vacía)";
    public static final String TEXTO_HORA = "Hora en que se autorizó el pedido (no editable)";
    public static final String TEXTO_DOCUMENTO = "Nro de Documento generado por pago de uniforme";
    public static final String TEXTO_ESTADO = "Estado del Pedido (no editable)";
    public static final String TEXTO_FUNCIONARIO = "Sólo en caso de pedido de uniformes";
    
    
    public entcPedido() {
        this.iid = 0;
        this.iidUsuario = 0;
        this.susuario = "";
        this.iidAutorizador = 0;
        this.sautorizador = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.iidFilial2 = 0;
        this.sfilial2 = "";
        this.iidDependencia = 0;
        this.sdependencia = "";
        this.iidTipoPedido = 0;
        this.stipoPedido = "";
        this.dfecha = null;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.iidEstadoPedido = 0;
        this.sestadoPedido = "";
        this.shora = "";
        this.iidDocumento = 0;
        this.snumeroDocumento = "";
        this.iidFuncionario = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entcPedido(int iid, int iidUsuario, String susuario, int iidAutorizador, String sautorizador, int iidFilial, String sfilial, int iidFilial2, String sfilial2, int iidDependencia, String sdependencia, int iidTipoPedido, String stipoPedido, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidEstadoPedido, String sestadoPedido, String shora, int iidDocumento, String snumeroDocumento, int iidFuncionario, short hestadoRegistro) {
        this.iid = iid;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidFilial2 = iidFilial2;
        this.sfilial2 = sfilial2;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidTipoPedido = iidTipoPedido;
        this.stipoPedido = stipoPedido;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidEstadoPedido = iidEstadoPedido;
        this.sestadoPedido = sestadoPedido;
        this.iidDocumento = iidDocumento;
        this.snumeroDocumento = snumeroDocumento;
        this.shora = shora;
        this.iidFuncionario = iidFuncionario;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidUsuario, String susuario, int iidAutorizador, String sautorizador, int iidFilial, String sfilial, int iidFilial2, String sfilial2, int iidDependencia, String sdependencia, int iidTipoPedido, String stipoPedido, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidEstadoPedido, String sestadoPedido, String shora, int iidDocumento, String snumeroDocumento, int iidFuncionario, short hestadoRegistro) {
        this.iid = iid;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidFilial2 = iidFilial2;
        this.sfilial2 = sfilial2;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidTipoPedido = iidTipoPedido;
        this.stipoPedido = stipoPedido;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidEstadoPedido = iidEstadoPedido;
        this.sestadoPedido = sestadoPedido;
        this.iidDocumento = iidDocumento;
        this.snumeroDocumento = snumeroDocumento;
        this.shora = shora;
        this.iidFuncionario = iidFuncionario;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entcPedido copiar(entcPedido destino) {
        destino.setEntidad(this.getId(), this.getIdUsuario(), this.getUsuario(), this.getIdAutorizador(), this.getAutorizador(), this.getIdFilial(), this.getFilial(), this.getIdFilial2(), this.getFilial2(), this.getIdDependencia(), this.getDependencia(), this.getIdTipoPedido(), this.getTipoPedido(), this.getFecha(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getIdEstadoPedido(), this.getEstadoPedido(), this.getHora(), this.getIdDocumento(), this.getNumeroDocumento(), this.getIdFuncionario(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcPedido cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuario(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setIdAutorizador(rs.getInt("idautorizador")); }
        catch(Exception e) {}
        try { this.setAutorizador(rs.getString("autorizador")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        try { this.setIdFilial2(rs.getInt("idfilial2")); }
        catch(Exception e) {}
        try { this.setFilial2(rs.getString("filial2")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        try { this.setIdTipoPedido(rs.getInt("idtipopedido")); }
        catch(Exception e) {}
        try { this.setTipoPedido(rs.getString("tipopedido")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setIdEstadoPedido(rs.getInt("idestadopedido")); }
        catch(Exception e) {}
        try { this.setEstadoPedido(rs.getString("estadopedido")); }
        catch(Exception e) {}
        try { this.setHora(rs.getString("hora")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }
    
    public void setUsuario(String susuario) {
        this.susuario = susuario;
    }

    public void setIdAutorizador(int iidAutorizador) {
        this.iidAutorizador = iidAutorizador;
    }
    
    public void setAutorizador(String sautorizador) {
        this.sautorizador = sautorizador;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setIdFilial2(int iidFilial2) {
        this.iidFilial2 = iidFilial2;
    }
    
    public void setFilial2(String sfilial2) {
        this.sfilial2 = sfilial2;
    }

    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }
    
    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public void setIdTipoPedido(int iidTipoPedido) {
        this.iidTipoPedido = iidTipoPedido;
    }
    
    public void setTipoPedido(String stipoPedido) {
        this.stipoPedido = stipoPedido;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }
    
    public void setIdEstadoPedido(int iidEstadoPedido) {
        this.iidEstadoPedido = iidEstadoPedido;
    }

    public void setEstadoPedido(String sestadoPedido) {
        this.sestadoPedido = sestadoPedido;
    }
    
    public void setHora(String shora) {
        this.shora = shora;
    }

    public void setIdDocumento(int iidDocumento) {
        this.iidDocumento = iidDocumento;
    }

    public void setNumeroDocumento(String snumeroDocumento) {
        this.snumeroDocumento = snumeroDocumento;
    }

    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }
    
    public String getUsuario() {
        return this.susuario;
    }

    public int getIdAutorizador() {
        return this.iidAutorizador;
    }
    
    public String getAutorizador() {
        return this.sautorizador;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }
    
    public String getFilial() {
        return this.sfilial;
    }

    public int getIdFilial2() {
        return this.iidFilial2;
    }
    
    public String getFilial2() {
        return this.sfilial2;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }
    
    public String getDependencia() {
        return this.sdependencia;
    }

    public int getIdTipoPedido() {
        return this.iidTipoPedido;
    }
    
    public String getTipoPedido() {
        return this.stipoPedido;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public int getIdEstadoPedido() {
        return this.iidEstadoPedido;
    }

    public String getEstadoPedido() {
        return this.sestadoPedido;
    }

    public String getHora() {
        return this.shora;
    }

    public int getIdDocumento() {
        return iidDocumento;
    }

    public String getNumeroDocumento() {
        return snumeroDocumento;
    }

    public int getIdFuncionario() {
        return iidFuncionario;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getIdFilial()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FILIAL;
            bvalido = false;
        }
        if (this.getIdTipoPedido()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_TIPO_PEDIDO;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.pedido("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            this.getIdTipoPedido()+","+
            this.getIdUsuario()+","+
            this.getIdAutorizador()+","+
            this.getIdFilial()+","+
            this.getIdDependencia()+","+
            this.getIdEstadoPedido()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHora())+","+
            this.getIdDocumento()+","+
            this.getIdFuncionario()+","+
            this.getIdFilial2()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha", "fecha", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Nº de Pedido", "id", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(3, "Tipo de Pedido", "tipopedido", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(4, "Usuario", "apellidonombre", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(5, "Estado", "estadopedido", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(6, "Stock de", "filial", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(7, "Dependencia", "dependencia", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(8, "Filial", "filial2", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
