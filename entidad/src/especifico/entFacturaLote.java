/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entFacturaLote {
    
    private int iid;
    private int iidSocio;
    private String scedula;
    private String snombre;
    private String sapellido;
    private String sdireccion;
    private String stelefono;
    private int iidGiraduria;
    private int iidRubro;
    private double umonto;
    private double umontoAplicado;
    private java.util.Date dfechaAplicada;
    private java.util.Date dperiodo;
    private boolean bfacturar;
    
    private short hestadoRegistro;
    private String smensaje;
    
    public entFacturaLote() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.iidGiraduria = 0;
        this.iidRubro = 0;
        this.umonto = 0.0;
        this.umontoAplicado = 0.0;
        this.dfechaAplicada = null;
        this.dperiodo = null;
        this.bfacturar = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entFacturaLote(int iid, int iidSocio, String scedula, String snombre, String sapellido, String sdireccion, String stelefono, int iidGiraduria, int iidRubro, double umonto, double umontoAplicado, java.util.Date dfechaAplicada, java.util.Date dperiodo, boolean bfacturar, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidGiraduria = iidGiraduria;
        this.iidRubro = iidRubro;
        this.umonto = umonto;
        this.umontoAplicado = umontoAplicado;
        this.dfechaAplicada = dfechaAplicada;
        this.dperiodo = dperiodo;
        this.bfacturar = bfacturar;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
     public void setEntidad(int iid, int iidSocio, String scedula, String snombre, String sapellido, String sdireccion, String stelefono, int iidGiraduria, int iidRubro, double umonto, double umontoAplicado, java.util.Date dfechaAplicada, java.util.Date dperiodo, boolean bfacturar, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidGiraduria = iidGiraduria;
        this.iidRubro = iidRubro;
        this.umonto = umonto;
        this.umontoAplicado = umontoAplicado;
        this.dfechaAplicada = dfechaAplicada;
        this.dperiodo = dperiodo;
        this.bfacturar = bfacturar;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entFacturaLote copiar(entFacturaLote destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getNombre(), this.getApellido(), this.getDireccion(), this.getTelefono(), this.getIdGiraduria(), this.getIdRubro(), this.getMonto(), this.getMontoAplicado(), this.getFechaAplicada(), this.getPeriodo(), this.getFacturar(), this.getEstadoRegistro());
        return destino;
    }
    
    public entFacturaLote cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIdGiraduria(rs.getInt("idgiraduria")); }
        catch(Exception e) {}
        try { this.setIdRubro(rs.getInt("rubro")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        try { this.setMontoAplicado(rs.getDouble("montoaplicado")); }
        catch(Exception e) {}
        try { this.setFechaAplicada(rs.getDate("fechaaplicada")); }
        catch(Exception e) {}
        try { this.setPeriodo(rs.getDate("periodo")); }
        catch(Exception e) {}
        try { this.setFacturar(false); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }
    
    public void setNombre(String snombre) {
        this.snombre = snombre;
    }
    
    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }
    
    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }
    
    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }

    public void setIdRubro(int iidRubro) {
        this.iidRubro = iidRubro;
    }

    public void setMonto(double umonto) {
        this.umonto = umonto;
    }

    public void setMontoAplicado(double umontoAplicado) {
        this.umontoAplicado = umontoAplicado;
    }

    public void setFechaAplicada(java.util.Date dfechaAplicada) {
        this.dfechaAplicada = dfechaAplicada;
    }

    public void setPeriodo(java.util.Date dperiodo) {
        this.dperiodo = dperiodo;
    }

    public void setFacturar(boolean bfacturar) {
        this.bfacturar = bfacturar;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }
    
    public String getNombre() {
        return this.snombre;
    }
    
    public String getApellido() {
        return this.sapellido;
    }
    
    public String getDireccion() {
        return this.sdireccion;
    }
    
    public String getTelefono() {
        return this.stelefono;
    }
    
    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public int getIdRubro() {
        return this.iidRubro;
    }

    public double getMonto() {
        return this.umonto;
    }
    
    public double getMontoAplicado() {
        return this.umontoAplicado;
    }
    
    public java.util.Date getFechaAplicada() {
        return this.dfechaAplicada;
    }
    
    public java.util.Date getPeriodo() {
        return this.dperiodo;
    }
    
    public boolean getFacturar() {
        return this.bfacturar;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public String getSentencia() {
        return "SELECT archivogiraduria("+
            this.getId()+","+
            this.getIdSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            this.getIdGiraduria()+","+
            this.getIdRubro()+","+
            this.getMonto()+","+
            this.getMontoAplicado()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAplicada())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getPeriodo())+","+
            this.getFacturar()+","+
            this.getEstadoRegistro()+")";
    }

    public String getConsultaSocio(int iidRubro, java.util.Date dperiodo, String scedula) {
        return "SELECT ag.id, ag.cedula, SUM(ag.monto) AS monto, ag.periodo, s.id AS idsocio, s.nombre nombre, s.apellido, s.direccion, s.telefonocelular AS telefono "+
            "FROM archivogiraduria AS ag "+
            "LEFT JOIN socio AS s ON ag.cedula=s.cedula AND (s.idestadosocio=20 OR s.idestadosocio=24 OR s.idestadosocio=224) "+
            "WHERE ag.procesado='FALSE' AND ag.periodo="+utilitario.utiFecha.getFechaGuardadoMac(dperiodo)+" AND ag.idrubro="+iidRubro+" AND ag.cedula='"+scedula+"' "+
            "GROUP BY ag.id, ag.cedula, ag.periodo, s.id, s.nombre, s.apellido, s.direccion, s.telefonocelular, ag.procesado ";
            //"UNION "+
            //"SELECT ag.id, ag.cedula, SUM(ag.monto) AS monto, ag.periodo, 0 AS idsocio, ag.idgiraduria, ag.idrubro, ag.nombre, ag.apellido, '--' AS direccion, '--' AS telefono "+
            //"FROM archivogiraduria AS ag "+
            //"WHERE ag.procesado='FALSE' AND ag.idsocio=0 AND ag.periodo="+utilitario.utiFecha.getFechaGuardadoMac(dperiodo)+" AND ag.cedula='"+scedula+"' ";
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
