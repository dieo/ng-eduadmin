/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entBarrio extends generico.entGenerica{
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Barrio (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";

    public entBarrio() {
        super();
    }

    public entBarrio(int iid, String sdescripcion, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
    }
    
    public void setEntidad(int iid, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entBarrio copiar(entBarrio destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getEstadoRegistro());
        return destino;
    }

    public entBarrio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        return this;
    }
    
    public boolean esValido() {
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) this.smensaje += TEXTO_DESCRIPCION + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public String getSentencia() {
        return "SELECT barrio("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
    public void setCampo(java.util.ArrayList<Object> lst) {
        lst.add(this.getId());
        lst.add(this.getDescripcion());
    }
    
    public static final int getId(String sdescripcion) {
        int id = 0;
        if (sdescripcion.contains("CONCEPCION")) id = 1;
        else if (sdescripcion.contains("SAN PEDRO")) id = 2;
        else if (sdescripcion.contains("CORDILLERA")) id = 3;
        else if (sdescripcion.contains("GUAIRA")) id = 4;
        else if (sdescripcion.contains("CAAGUAZU")) id = 5;
        else if (sdescripcion.contains("CAAZAPA")) id = 6;
        else if (sdescripcion.contains("ITAPUA")) id = 7;
        else if (sdescripcion.contains("MISIONES")) id = 8;
        else if (sdescripcion.contains("PARAGUARI")) id = 9;
        else if (sdescripcion.contains("ALTO PARANA")) id = 10;
        else if (sdescripcion.contains("XI NIVEL CENTRAL")) id = 11;
        else if (sdescripcion.contains("EEMBUCU")) id = 12;
        else if (sdescripcion.contains("AMAMBAY")) id = 13;
        else if (sdescripcion.contains("CANINDEYU")) id = 14;
        else if (sdescripcion.contains("HAYES")) id = 15;
        else if (sdescripcion.contains("ALTO PARAGUAY")) id = 16;
        else if (sdescripcion.contains("BOQUERON")) id = 17;
        else if (sdescripcion.contains("CAPITAL")) id = 18;
        else if (sdescripcion.contains("GRAN ASUNCION")) id = 19;
        else if (sdescripcion.contains("OVIEDO")) id = 20;
        else if (sdescripcion.contains("SAN IGNACIO")) id = 21;
        else if (sdescripcion.contains("AYOLAS")) id = 22;
        else if (sdescripcion.contains("CURUGUATY")) id = 23;
        else if (sdescripcion.contains("BELLA VISTA")) id = 24;
        else if (sdescripcion.contains("SAN ESTANISLAO")) id = 25;
        else if (sdescripcion.contains("EMBOS")) id = 26;
        else if (sdescripcion.contains("YEGROS")) id = 27;
        else if (sdescripcion.contains("ALBERDI")) id = 28;
        else if (sdescripcion.contains("YPACARAI")) id = 29;
        else if (sdescripcion.contains("AQUINO")) id = 30;
        else if (sdescripcion.contains("HORQUETA")) id = 31;
        else if (sdescripcion.contains("SANTA RITA")) id = 32;
        else if (sdescripcion.contains("HERNANDARIAS")) id = 33;
        else if (sdescripcion.contains("NEPOMUCENO")) id = 34;
        else if (sdescripcion.contains("ALTOS")) id = 35;
        else if (sdescripcion.contains("ROSARIO")) id = 36;
        else if (sdescripcion.contains("INDEPENDENCIA")) id = 37;
        else if (sdescripcion.contains("YUTY")) id = 38;
        else if (sdescripcion.contains("---")) id = 39;
        else if (sdescripcion.contains("AGUARAY")) id = 40;
        return id;
    }
    
}
