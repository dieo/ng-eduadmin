/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entCasoJuridico {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String snombre;
    private String sapellido;
    private int inumeroCaso;
    private java.util.Date dfecha;
    private int iidCiudad;
    private String sciudad;
    private String stelefono;
    private int iidEstadoJuridico;
    private String sestadoJuridico;
    private String scaso;
    private int iidJuzgado;
    private String sjuzgado;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_TELEFONO = 20;
    public static final int LONGITUD_CASO = 250;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_NUMERO_CASO = "Número de Caso (no editable)";
    public static final String TEXTO_FECHA = "Fecha (no vacía)";
    public static final String TEXTO_CIUDAD = "Ciudad (no editable)";
    public static final String TEXTO_TELEFONO = "Teléfono (no vacío, hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_ESTADO_JURIDICO = "Estado Jurídico (no vacío)";
    public static final String TEXTO_CASO = "Caso Jurídico (no vacía, hasta " + LONGITUD_CASO + " caracteres)";
    public static final String TEXTO_JUZGADO = "Juzgado (no vacío)";
    
    public entCasoJuridico() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.inumeroCaso = 0;
        this.dfecha = null;
        this.iidCiudad = 0;
        this.sciudad = "";
        this.stelefono = "";
        this.iidEstadoJuridico = 0;
        this.sestadoJuridico = "";
        this.scaso = "";
        this.iidJuzgado = 0;
        this.sjuzgado = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entCasoJuridico(int iid, int iidSocio, String scedula, String snombre, String sapellido, int inumeroCaso, java.util.Date dfecha, int iidCiudad, String sciudad, String stelefono, int iidEstadoJuridico, String sestadoJuridico, String scaso, int iidJuzgado, String sjuzgado, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.inumeroCaso = inumeroCaso;
        this.dfecha = dfecha;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.stelefono = stelefono;
        this.iidEstadoJuridico = iidEstadoJuridico;
        this.sestadoJuridico = sestadoJuridico;
        this.scaso = scaso;
        this.iidJuzgado = iidJuzgado;
        this.sjuzgado = sjuzgado;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, String snombre, String sapellido, int inumeroCaso, java.util.Date dfecha, int iidCiudad, String sciudad, String stelefono, int iidEstadoJuridico, String sestadoJuridico, String scaso, int iidJuzgado, String sjuzgado, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.inumeroCaso = inumeroCaso;
        this.dfecha = dfecha;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.stelefono = stelefono;
        this.iidEstadoJuridico = iidEstadoJuridico;
        this.sestadoJuridico = sestadoJuridico;
        this.scaso = scaso;
        this.iidJuzgado = iidJuzgado;
        this.sjuzgado = sjuzgado;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entCasoJuridico copiar(entCasoJuridico destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getNombre(), this.getApellido(), this.getNumeroCaso(), this.getFecha(), this.getIdCiudad(), this.getCiudad(), this.getTelefono(), this.getIdEstadoJuridico(), this.getEstadoJuridico(), this.getCaso(), this.getIdJuzgado(), this.getJuzgado(), this.getEstadoRegistro());
        return destino;
    }
    
    public entCasoJuridico cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setNumeroCaso(rs.getInt("numerocaso")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setIdCiudad(rs.getInt("idciudad")); }
        catch(Exception e) {}
        try { this.setCiudad(rs.getString("ciudad")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIdEstadoJuridico(rs.getInt("idestadojuridico")); }
        catch(Exception e) {}
        try { this.setEstadoJuridico(rs.getString("estadojuridico")); }
        catch(Exception e) {}
        try { this.setCaso(rs.getString("caso")); }
        catch(Exception e) {}
        try { this.setIdJuzgado(rs.getInt("idjuzgado")); }
        catch(Exception e) {}
        try { this.setJuzgado(rs.getString("juzgado")); }
        catch(Exception e) {}
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombre(String snombre) {
        this.snombre = snombre;
    }

    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }

    public void setNumeroCaso(int inumeroCaso) {
        this.inumeroCaso = inumeroCaso;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setIdCiudad(int iidCiudad) {
        this.iidCiudad = iidCiudad;
    }

    public void setCiudad(String sciudad) {
        this.sciudad = sciudad;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setIdEstadoJuridico(int iidEstadoJuridico) {
        this.iidEstadoJuridico = iidEstadoJuridico;
    }

    public void setEstadoJuridico(String sestadoJuridico) {
        this.sestadoJuridico = sestadoJuridico;
    }

    public void setCaso(String scaso) {
        this.scaso = scaso;
    }

    public void setIdJuzgado(int iidJuzgado) {
        this.iidJuzgado = iidJuzgado;
    }
    
    public void setJuzgado(String sjuzgado) {
        this.sjuzgado = sjuzgado;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombre() {
        return this.snombre;
    }

    public String getApellido() {
        return this.sapellido;
    }

    public int getNumeroCaso() {
        return this.inumeroCaso;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public int getIdCiudad() {
        return this.iidCiudad;
    }

    public String getCiudad() {
        return this.sciudad;
    }

    public String getTelefono() {
        return this.stelefono;
    }

    public int getIdEstadoJuridico() {
        return this.iidEstadoJuridico;
    }

    public String getEstadoJuridico() {
        return this.sestadoJuridico;
    }

    public String getCaso() {
        return this.scaso;
    }

    public int getIdJuzgado() {
        return this.iidJuzgado;
    }
    
    public String getJuzgado() {
        return this.sjuzgado;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdSocio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getFecha()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getIdCiudad()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CIUDAD;
            bvalido = false;
        }
        if (this.getTelefono().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO;
            bvalido = false;
        }
        if (this.getIdEstadoJuridico()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ESTADO_JURIDICO;
            bvalido = false;
        }
        if (this.getCaso().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CASO;
            bvalido = false;
        }
        if (this.getIdJuzgado()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_JUZGADO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT casojuridico ("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getNumeroCaso()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            this.getIdCiudad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getIdEstadoJuridico()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCaso())+","+
            this.getIdJuzgado()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Numero Caso", "numerocaso", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Fecha", "fecha", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(4, "Ciudad", "ciudad", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Estado Jurídico", "estadojuridico", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Caso", "caso", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Juzgado", "juzgado", generico.entLista.tipo_texto));
        return lst;
    }

}
