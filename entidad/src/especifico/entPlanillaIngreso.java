/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPlanillaIngreso extends generico.entGenerica {

    protected int iidFuncionario;
    protected String sfuncionario;
    protected int iidSucursal;
    protected String ssucursal;
    protected java.util.Date dfechaApertura;
    protected String shoraApertura;
    protected java.util.Date dfechaCierre;
    protected String shoraCierre;
    protected boolean bactivo;
    protected double umontoInicial;
    protected int inumeroPlanilla;
    protected int iidViaCobro;
    protected String sviaCobro;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de planilla (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_FUNCIONARIO = "Funcionario (no vacío)";
    public static final String TEXTO_SUCURSAL = "Sucursal (no vacío)";
    public static final String TEXTO_FECHA_APERTURA = "Fecha de apertura de planilla (no vacía)";
    public static final String TEXTO_HORA_APERTURA = "Hora de apertura de planilla (no vacía)";
    public static final String TEXTO_FECHA_CIERRE = "Fecha de cierre de planilla (no editable)";
    public static final String TEXTO_HORA_CIERRE = "Hora de cierre de planilla (no editable)";
    public static final String TEXTO_ACTIVO = "Activo (no editable)";
    public static final String TEXTO_MONTO_INICIAL = "Monto Inicial de apertura de planilla (valor positivo)";
    public static final String TEXTO_NUMERO_PLANILLA = "Número de Planilla (no editable)";
    public static final String TEXTO_VIA_COBRO = "Vía de Cobro (no vacía)";

    public entPlanillaIngreso() {
        super();
        this.iidFuncionario = 0;
        this.sfuncionario = "";
        this.iidSucursal = 0;
        this.ssucursal = "";
        this.dfechaApertura = null;
        this.shoraApertura = "";
        this.dfechaCierre = null;
        this.shoraCierre = "";
        this.bactivo = false;
        this.umontoInicial = 0.0;
        this.inumeroPlanilla = 0;
        this.iidViaCobro = 0;
        this.sviaCobro = "";
    }

    public entPlanillaIngreso(int iid, String sdescripcion, int iidFuncionario, String sfuncionario, int iidSucursal, String ssucursal, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, boolean bactivo, double umontoInicial, int inumeroPlanilla, int iidViaCobro, String sviaCobro) {
        super(iid, sdescripcion);
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.iidSucursal = iidSucursal;
        this.ssucursal = ssucursal;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.bactivo = bactivo;
        this.umontoInicial = umontoInicial;
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidViaCobro = iidViaCobro;
        this.sviaCobro = sviaCobro;
    }

    public entPlanillaIngreso(int iid, String sdescripcion, int iidFuncionario, String sfuncionario, int iidSucursal, String ssucursal, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, boolean bactivo, double umontoInicial, int inumeroPlanilla, int iidViaCobro, String sviaCobro, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.iidSucursal = iidSucursal;
        this.ssucursal = ssucursal;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.bactivo = bactivo;
        this.umontoInicial = umontoInicial;
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidViaCobro = iidViaCobro;
        this.sviaCobro = sviaCobro;
    }

    public void setEntidad(int iid, String sdescripcion, int iidFuncionario, String sfuncionario, int iidSucursal, String ssucursal, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, boolean bactivo, double umontoInicial, int inumeroPlanilla, int iidViaCobro, String sviaCobro, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.iidSucursal = iidSucursal;
        this.ssucursal = ssucursal;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.bactivo = bactivo;
        this.umontoInicial = umontoInicial;
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidViaCobro = iidViaCobro;
        this.sviaCobro = sviaCobro;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entPlanillaIngreso copiar(entPlanillaIngreso destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdFuncionario(), this.getFuncionario(), this.getIdSucursal(), this.getSucursal(), this.getFechaApertura(), this.getHoraApertura(), this.getFechaCierre(), this.getHoraCierre(), this.getActivo(), this.getMontoInicial(), this.getNumeroPlanilla(), this.getIdViaCobro(), this.getViaCobro(), this.getEstadoRegistro());
        return destino;
    }

    public entPlanillaIngreso cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        try { this.setFuncionario(rs.getString("funcionario")); }
        catch(Exception e) {}
        try { this.setIdSucursal(rs.getInt("idsucursal")); }
        catch(Exception e) {}
        try { this.setSucursal(rs.getString("sucursal")); }
        catch(Exception e) {}
        try { this.setFechaApertura(rs.getDate("fechaapertura")); }
        catch(Exception e) {}
        try { this.setHoraApertura(rs.getString("horaapertura")); }
        catch(Exception e) {}
        try { this.setFechaCierre(rs.getDate("fechacierre")); }
        catch(Exception e) {}
        try { this.setHoraCierre(rs.getString("horacierre")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setMontoInicial(rs.getDouble("montoinicial")); }
        catch(Exception e) {}
        try { this.setNumeroPlanilla(rs.getInt("numeroplanilla")); }
        catch(Exception e) {}
        try { this.setIdViaCobro(rs.getInt("idviacobro")); }
        catch(Exception e) {}
        try { this.setViaCobro(rs.getString("viacobro")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }

    public void setFuncionario(String sfuncionario) {
        this.sfuncionario = sfuncionario;
    }

    public void setIdSucursal(int iidSucursal) {
        this.iidSucursal = iidSucursal;
    }

    public void setSucursal(String ssucursal) {
        this.ssucursal = ssucursal;
    }

    public void setFechaApertura(java.util.Date dfechaApertura) {
        this.dfechaApertura = dfechaApertura;
    }

    public void setHoraApertura(String shoraApertura) {
        this.shoraApertura = shoraApertura;
    }

    public void setFechaCierre(java.util.Date dfechaCierre) {
        this.dfechaCierre = dfechaCierre;
    }

    public void setHoraCierre(String shoraCierre) {
        this.shoraCierre = shoraCierre;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setMontoInicial(double umontoInicial) {
        this.umontoInicial = umontoInicial;
    }

    public void setNumeroPlanilla(int inumeroPlanilla) {
        this.inumeroPlanilla = inumeroPlanilla;
    }

    public void setIdViaCobro(int iidViaCobro) {
        this.iidViaCobro = iidViaCobro;
    }

    public void setViaCobro(String sviaCobro) {
        this.sviaCobro = sviaCobro;
    }

    public int getIdFuncionario() {
        return this.iidFuncionario;
    }

    public String getFuncionario() {
        return this.sfuncionario;
    }

    public int getIdSucursal() {
        return this.iidSucursal;
    }

    public String getSucursal() {
        return this.ssucursal;
    }

    public java.util.Date getFechaApertura() {
        return this.dfechaApertura;
    }

    public String getHoraApertura() {
        return this.shoraApertura;
    }

    public java.util.Date getFechaCierre() {
        return this.dfechaCierre;
    }

    public String getHoraCierre() {
        return this.shoraCierre;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public double getMontoInicial() {
        return this.umontoInicial;
    }

    public int getNumeroPlanilla() {
        return this.inumeroPlanilla;
    }

    public int getIdViaCobro() {
        return this.iidViaCobro;
    }

    public String getViaCobro() {
        return this.sviaCobro;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdFuncionario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO;
            bvalido = false;
        }
        if (this.getIdSucursal()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SUCURSAL;
            bvalido = false;
        }
        if (this.getFechaApertura()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_APERTURA;
            bvalido = false;
        }
        if (this.getHoraApertura().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_APERTURA;
            bvalido = false;
        } else {
            if (!utilitario.utiFecha.esValidoHoraLarga(this.getHoraApertura())) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Hora inválida";
                bvalido = false;
            }
        }
        if (this.getMontoInicial()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_INICIAL;
            bvalido = false;
        }
        if (this.getIdViaCobro()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_VIA_COBRO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoCerrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaCierre()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_CIERRE;
            bvalido = false;
        }
        if (this.getHoraCierre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_CIERRE;
            bvalido = false;
        } else {
            if (!utilitario.utiFecha.esValidoHoraLarga(this.getHoraCierre())) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Hora inválida";
                bvalido = false;
            }
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT planillaingreso("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdFuncionario()+","+
            this.getIdSucursal()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaApertura())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraApertura())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaCierre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraCierre())+","+
            this.getActivo()+","+
            this.getMontoInicial()+","+
            this.getNumeroPlanilla()+","+
            this.getIdViaCobro()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número Planilla", "numeroplanilla", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Funcionario", "funcionario", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Sucursal", "sucursal", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Fecha apertura", "fechaapertura", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(6, "Fecha cierre", "fechacierre", generico.entLista.tipo_fecha));
        return lst;
    }
    
}
