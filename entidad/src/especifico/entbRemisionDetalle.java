/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;



/**
 *
 * @author Lic. Didier Barreto
 */
public class entbRemisionDetalle {
    
    protected int iid;
    protected int iidRemision;
    protected int iidDocumento;
    protected java.util.Date dfecha;
    protected int inumeroDocumento;
    protected double umonto;
    protected int iidEstadoCheque;
    protected String sestadoCheque;
    protected int iidEstadoEntrega;
    protected String sestadoEntrega;
    private int iidBanco;
    private String sbanco;
    protected String scedulaRuc;
    protected String sreceptorCheque;
    protected int iidMovimiento;
    protected int inumerosolicitud;
    protected int iidTipoDocumento;
    protected String stipoDocumento;
    protected int inumerooperacion;
    protected int iidCentroCosto;
    protected String scentroCosto;
    protected int inumeroNota;
    protected String sdescripcion;
    protected double ucantidad;
    protected boolean blegajocompleto;
    protected boolean brecibido;
    private int iidCourier;
    private String scourier;
    private String snroCourier;
    protected short cestado;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_TIPO_DOCUMENTO = "Tipo de documento remitido (no vacía)";
    public static final String TEXTO_NUMERO_DOCUMENTO = "Número del documento remitido (no vacía)";
    public static final String TEXTO_NUMERO_NOTA = "Número del la Nota de Crédito";
    public static final String TEXTO_FECHA = "Fecha del documento";
    public static final String TEXTO_ESTADOCHEQUE = "Estado del cheque";
    public static final String TEXTO_ESTADOENTREGA = "Estado de la entrega de cheque";
    public static final String TEXTO_MONTO = "Monto del cheque";
    public static final String TEXTO_DESCRIPCION = "Descripcion del documento remitido";
    public static final String TEXTO_BANCO = "Entidad Bancaria de la cual procede el cheque";
    public static final String TEXTO_CENTRO_COSTO = "Centro de costo de la operación";
    public static final String TEXTO_CANTIDAD = "Cantidad de documentos a enviar";
    public static final String TEXTO_LEGAJOCOMPLETO = "Si es legajo completo";
    public static final String TEXTO_RECIBIDO = "Estado del documento";
    public static final String TEXTO_COURIER = "Nº de Courier";
    
    public entbRemisionDetalle() {
        this.iid = 0;
        this.iidRemision = 0;
        this.iidTipoDocumento = 0;
        this.stipoDocumento = "";
        this.iidDocumento = 0;
        this.inumeroDocumento = 0;
        this.inumeroNota = 0;
        this.iidBanco = 0;
        this.sbanco = "";
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.iidEstadoCheque = 0;
        this.sestadoCheque = "";
        this.umonto = 0;
        this.sdescripcion = "";
        this.ucantidad = 0;
        this.blegajocompleto = false;
        this.brecibido = false;
        this.dfecha = null;
        this.iidCourier = 0;
        this.scourier = "";
        this.snroCourier = "";
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entbRemisionDetalle(int iid, int iidRemision, int iidTipoDocumento, String stipoDocumento, int iidDocumento, int inumeroDocumento, int inumeroNota, int iidBanco, String sbanco, int iidCentroCosto, String scentroCosto, int iidEstadoCheque, String sestadoCheque, double umonto, String sdescripcion, double ucantidad, boolean blegajocompleto, boolean brecibido, java.util.Date dfecha, int iidCourier, String scourier, String snroCourier, short cestado) {
        this.iid = iid;
        this.iidRemision = iidRemision;
        this.iidTipoDocumento = iidTipoDocumento;
        this.stipoDocumento = stipoDocumento;
        this.iidDocumento = iidDocumento;
        this.inumeroDocumento = inumeroDocumento;
        this.inumeroNota = inumeroNota;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidEstadoCheque = iidEstadoCheque;
        this.sestadoCheque = sestadoCheque;
        this.umonto = umonto;
        this.sdescripcion = sdescripcion;
        this.ucantidad = ucantidad;
        this.blegajocompleto = blegajocompleto;
        this.brecibido = brecibido;
        this.dfecha = dfecha;
        this.iidCourier = iidCourier;
        this.scourier = scourier;
        this.snroCourier = snroCourier;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidRemision, int iidTipoDocumento, String stipoDocumento, int iidDocumento, int inumeroDocumento, int inumeroNota, int iidBanco, String sbanco, int iidCentroCosto, String scentroCosto, int iidEstadoCheque, String sestadoCheque, double umonto, String sdescripcion, double ucantidad, boolean blegajocompleto, boolean brecibido, java.util.Date dfecha, int iidCourier, String scourier, String snroCourier, short cestado) {
        this.iid = iid;
        this.iidRemision = iidRemision;
        this.iidTipoDocumento = iidTipoDocumento;
        this.stipoDocumento = stipoDocumento;
        this.iidDocumento = iidDocumento;
        this.inumeroDocumento = inumeroDocumento;
        this.inumeroNota = inumeroNota;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidEstadoCheque = iidEstadoCheque;
        this.sestadoCheque = sestadoCheque;
        this.umonto = umonto;
        this.sdescripcion = sdescripcion;
        this.ucantidad = ucantidad;
        this.blegajocompleto = blegajocompleto;
        this.brecibido = brecibido;
        this.dfecha = dfecha;
        this.iidCourier = iidCourier;
        this.scourier = scourier;
        this.snroCourier = snroCourier;
        this.cestado = cestado;
    }

    public entbRemisionDetalle copiar(entbRemisionDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdRemision(), this.getIdTipoDocumento(), this.getTipoDocumento(), this.getIdDocumento(), this.getNumeroDocumento(), this.getNumeroNota(), this.getIdBanco(), this.getBanco(), this.getIdCentroCosto(), this.getCentroCosto(), this.getIdEstadoCheque(), this.getEstadoCheque(), this.getMonto(), this.getDescripcion(), this.getCantidad(),  this.getLegajoCompleto(), this.getRecibido(), this.getFecha(), this.getIdCourier(), this.getCourier(), this.getNroCourier(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbRemisionDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("idremisiondetalle")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdRemision(rs.getInt("idremision")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdDocumento(rs.getInt("iddocumento")); }
        catch(java.sql.SQLException e) { }
        try { this.setNumeroDocumento(rs.getInt("numerodocumento")); }
        catch(java.sql.SQLException e) { }
        try { this.setNumeroNota(rs.getInt("numeronota")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdBanco(rs.getInt("idbanco")); }
        catch(java.sql.SQLException e) { }
        try { this.setBanco(rs.getString("banco")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdCentroCosto(rs.getInt("idcentrocosto")); }
        catch(java.sql.SQLException e) { }
        try { this.setCentroCosto(rs.getString("centrocosto")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdTipoDocumento(rs.getInt("idtipodocumento")); }
        catch(java.sql.SQLException e) { }
        try { this.setTipoDocumento(rs.getString("tipodocumento")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdEstadoCheque(rs.getInt("idestadocheque")); }
        catch(java.sql.SQLException e) { }
        try { this.setEstadoCheque(rs.getString("estadocheque")); }
        catch(java.sql.SQLException e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(java.sql.SQLException e) { }
        try { this.setMonto(rs.getDouble("monto")); }
        catch(java.sql.SQLException e) { }
        try { this.setCantidad(rs.getDouble("cantidad")); }
        catch(java.sql.SQLException e) { }
        try { this.setLegajoCompleto(rs.getBoolean("legajocompleto")); }
        catch(java.sql.SQLException e) { }
        try { this.setRecibido(rs.getBoolean("recibido")); }
        catch(java.sql.SQLException e) { }
        try { this.setFecha(rs.getDate("fecha")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdCourier(rs.getInt("idcourier")); }
        catch(Exception e) {}
        try { this.setCourier(rs.getString("courier")); }
        catch(Exception e) {}
        try { this.setNroCourier(rs.getString("numerocourier")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdRemision(int iidRemision) {
        this.iidRemision = iidRemision;
    }

    public void setIdDocumento(int iidDocumento) {
        this.iidDocumento = iidDocumento;
    }

    public void setNumeroDocumento(int inumeroDocumento) {
        this.inumeroDocumento = inumeroDocumento;
    }

    public void setNumeroNota(int inumeroNota) {
        this.inumeroNota = inumeroNota;
    }

    public void setIdTipoDocumento(int iidTipoDocumento) {
        this.iidTipoDocumento = iidTipoDocumento;
    }

    public void setTipoDocumento(String stipoDocumento) {
        this.stipoDocumento = stipoDocumento;
    }

    public void setIdEstadoCheque(int iidEstadoCheque) {
        this.iidEstadoCheque = iidEstadoCheque;
    }

    public void setEstadoCheque(String sestadoCheque) {
        this.sestadoCheque = sestadoCheque;
    }
    
    public void setIdBanco(int iidBanco) {
        this.iidBanco = iidBanco;
    }

    public void setBanco(String sbanco) {
        this.sbanco = sbanco;
    }
    
    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }

    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }
    
    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setMonto(double umonto) {
        this.umonto = umonto;
    }

    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setLegajoCompleto(boolean blegajocompleto) {
        this.blegajocompleto = blegajocompleto;
    }

    public void setRecibido(boolean brecibido) {
        this.brecibido = brecibido;
    }
    
    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setIdCourier(int iidCourier) {
        this.iidCourier = iidCourier;
    }

    public void setCourier(String scourier) {
        this.scourier = scourier;
    }
    
    public void setNroCourier(String snroCourier) {
        this.snroCourier = snroCourier;
    }
    
    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdRemision() {
        return this.iidRemision;
    }
        
    public int getIdDocumento() {
        return this.iidDocumento;
    }
        
    public int getNumeroDocumento() {
        return this.inumeroDocumento;
    }
        
    public int getNumeroNota() {
        return this.inumeroNota;
    }
        
    public int getIdTipoDocumento() {
        return this.iidTipoDocumento;
    }
   
    public String getTipoDocumento() {
        return this.stipoDocumento;
    }
        
    public int getIdBanco() {
        return this.iidBanco;
    }
   
    public String getBanco() {
        return this.sbanco;
    }
        
    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }
   
    public String getCentroCosto() {
        return this.scentroCosto;
    }
    
    public int getIdEstadoCheque() {
        return this.iidEstadoCheque;
    }
   
    public String getEstadoCheque() {
        return this.sestadoCheque;
    }
    
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public double getMonto() {
        return this.umonto;
    }

    public double getCantidad() {
        return this.ucantidad;
    }

    public boolean getLegajoCompleto() {
        return this.blegajocompleto;
    }

    public boolean getRecibido() {
        return this.brecibido;
    }
    
    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public int getIdCourier() {
        return this.iidCourier;
    }

    public String getCourier() {
        return this.scourier;
    }

    public String getNroCourier() {
        return this.snroCourier;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        /*if (this.getNumeroDocumento()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_NUMERO_DOCUMENTO;
            bvalido = false;
        }
        if ((this.getMonto()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO;
            bvalido = false;
        }
        if ((this.getCantidad()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CANTIDAD;
            bvalido = false;
        }*/
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.remisiondetalle("+
            this.getId()+","+
            this.getIdRemision()+","+
            this.getIdDocumento()+","+
            this.getIdTipoDocumento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getCantidad()+","+
            this.getLegajoCompleto()+","+
            this.getRecibido()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            this.getIdCourier()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNroCourier())+","+
            this.getEstadoRegistro()+")";
    }

}
