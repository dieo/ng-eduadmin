/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entHistorico {

    private int iidSocio;
    private int iidMovimiento;
    private String stipo;
    private int iidCuenta;
    private String scuenta;
    private String sdenominacion;
    private int inumeroOperacion;
    private int inumeroSolicitud;
    private java.util.Date dfechaOperacion;
    private int iplazo;
    private double utasaInteres;
    private boolean bcancelacion;
    private int iidTipoCredito;
    private String stipoCredito;
    private int iidEntidad;
    private String sentidad;
    private java.util.Date dfechaVencimiento;
    private double umontoTotal;
    private double usaldo;
    private String stabla;
    private String srubro;
    private String sestado;
    
    private int iestadoRegistro;
    
    public entHistorico() {
        this.iidSocio = 0;
        this.iidMovimiento = 0;
        this.stipo = "";
        this.iidCuenta = 0;
        this.scuenta = "";
        this.sdenominacion = "";
        this.inumeroOperacion = 0;
        this.inumeroSolicitud = 0;
        this.dfechaOperacion = null;
        this.iplazo = 0;
        this.utasaInteres = 0.0;
        this.bcancelacion = false;
        this.iidTipoCredito = 0;
        this.stipoCredito = "";
        this.iidEntidad = 0;
        this.sentidad = "";
        this.dfechaVencimiento = null;
        this.umontoTotal = 0.0;
        this.usaldo = 0.0;
        this.stabla = "";
        this.srubro = "";
        this.sestado = "";
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entHistorico(int iidSocio, int iidMovimiento, String stipo, int iidCuenta, String scuenta, String sdenominacion, int inumeroOperacion, int inumeroSolicitud, java.util.Date dfechaOperacion, int iplazo, double utasaInteres, boolean bcancelacion, int iidTipoCredito, String stipoCredito, int iidEntidad, String sentidad, java.util.Date dfechaVencimiento, double umontoTotal, double usaldo, String stabla, String srubro, String sestado, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.stipo = stipo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdenominacion = sdenominacion;
        this.inumeroOperacion = inumeroOperacion;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaOperacion = dfechaOperacion;
        this.iplazo = iplazo;
        this.utasaInteres = utasaInteres;
        this.bcancelacion = bcancelacion;
        this.iidTipoCredito = iidTipoCredito;
        this.stipoCredito = stipoCredito;
        this.iidEntidad = iidEntidad;
        this.sentidad = sentidad;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoTotal = umontoTotal;
        this.usaldo = usaldo;
        this.stabla = stabla;
        this.srubro = srubro;
        this.sestado = sestado;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iidSocio, int iidMovimiento, String stipo, int iidCuenta, String scuenta, String sdenominacion, int inumeroOperacion, int inumeroSolicitud, java.util.Date dfechaOperacion, int iplazo, double utasaInteres, boolean bcancelacion, int iidTipoCredito, String stipoCredito, int iidEntidad, String sentidad, java.util.Date dfechaVencimiento, double umontoTotal, double usaldo, String stabla, String srubro, String sestado, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.stipo = stipo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdenominacion = sdenominacion;
        this.inumeroOperacion = inumeroOperacion;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaOperacion = dfechaOperacion;
        this.iplazo = iplazo;
        this.utasaInteres = utasaInteres;
        this.bcancelacion = bcancelacion;
        this.iidTipoCredito = iidTipoCredito;
        this.stipoCredito = stipoCredito;
        this.iidEntidad = iidEntidad;
        this.sentidad = sentidad;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoTotal = umontoTotal;
        this.usaldo = usaldo;
        this.stabla = stabla;
        this.srubro = srubro;
        this.sestado = sestado;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entHistorico copiar(entHistorico destino) {
        destino.setEntidad(this.getIdSocio(), this.getIdMovimiento(), this.getTipo(), this.getIdCuenta(), this.getCuenta(), this.getDenominacion(), this.getNumeroOperacion(), this.getNumeroSolicitud(), this.getFechaOperacion(), this.getPlazo(), this.getTasaInteres(), this.getCancelacion(), this.getIdTipoCredito(), this.getTipoCredito(), this.getIdEntidad(), this.getEntidad(), this.getFechaVencimiento(), this.getMontoTotal(), this.getSaldo(), this.getTabla(), this.getRubro(), this.getEstado(), this.getEstadoRegistro());
        return destino;
    }

    public entHistorico cargar(java.sql.ResultSet rs) {
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setTipo(rs.getString("tipo")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setDenominacion(rs.getString("denominacion")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(Exception e) {}
        try { this.setCancelacion(rs.getBoolean("cancelacion")); }
        catch(Exception e) {}
        try { this.setIdTipoCredito(rs.getInt("idtipocredito")); }
        catch(Exception e) {}
        try { this.setTipoCredito(rs.getString("tipocredito")); }
        catch(Exception e) {}
        try { this.setIdEntidad(rs.getInt("identidad")); }
        catch(Exception e) {}
        try { this.setEntidad(rs.getString("entidad")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setMontoTotal(rs.getDouble("montototal")); }
        catch(Exception e) {}
        try { this.setSaldo(rs.getDouble("saldo")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setTipo(String stipo) {
        this.stipo = stipo;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setDenominacion(String sdenominacion) {
        this.sdenominacion = sdenominacion;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = (java.util.Date)dfechaOperacion.clone();
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setTasaInteres(double utasaInteres) {
        this.utasaInteres = utasaInteres;
    }

    public void setCancelacion(boolean bcancelacion) {
        this.bcancelacion = bcancelacion;
    }

    public void setIdTipoCredito(int iidTipoCredito) {
        this.iidTipoCredito = iidTipoCredito;
    }

    public void setTipoCredito(String stipoCredito) {
        this.stipoCredito = stipoCredito;
    }

    public void setIdEntidad(int iidEntidad) {
        this.iidEntidad = iidEntidad;
    }

    public void setEntidad(String sentidad) {
        this.sentidad = sentidad;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setMontoTotal(double umontoTotal) {
        this.umontoTotal = umontoTotal;
    }

    public void setSaldo(double usaldo) {
        this.usaldo = usaldo;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public String getTipo() {
        return this.stipo;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getDenominacion() {
        return this.sdenominacion;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public Double getTasaInteres() {
        return this.utasaInteres;
    }

    public boolean getCancelacion() {
        return this.bcancelacion;
    }

    public int getIdTipoCredito() {
        return this.iidTipoCredito;
    }

    public String getTipoCredito() {
        return this.stipoCredito;
    }

    public int getIdEntidad() {
        return this.iidEntidad;
    }

    public String getEntidad() {
        return this.sentidad;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getMontoTotal() {
        return this.umontoTotal;
    }

    public double getSaldo() {
        return this.usaldo;
    }

    public String getTabla() {
        return this.stabla;
    }

    public String getRubro() {
        return this.srubro;
    }

    public String getEstado() {
        return this.sestado;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    public String getConsulta(java.util.Date dfecha, int iidSocio, int iidMutual, int iidAporteCapital, int iidSolidaridad, int iidCuotaSocial, int iidFondoPrevision, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, int iidInteresPrestamo, int iidEstadoOrdenCredito, int iidEstadoPrestamo, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 'OC' AS tipo, mo.id AS idmovimiento, mo.cuenta||' ('||mo.entidad||')' AS denominacion, mo.idcuenta, mo.cuenta, mo.identidad, mo.entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, mo.tasainteres, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.montoaprobado AS montototal, mo.idtipocredito, mo.tipocredito, dop.fechavencimiento, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, mo.estado "+ // ORDEN DE CREDITO
                       "FROM (SELECT * FROM vwmovimiento1 WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado<>"+iidEstadoOrdenCredito+" AND identidad<>"+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'PM' AS tipo, mo.id AS idmovimiento, mo.cuenta||' '||mo.tipocreditobreve AS denominacion, mo.idcuenta, mo.cuenta, mo.identidad, mo.entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres "+
                       "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                       "     ELSE mo.tasainteres END AS tasainteres, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.montoaprobado AS montototal, mo.idtipocredito, mo.tipocredito, dop.fechavencimiento, dop.saldocapital+dop.saldointeres AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, mo.estado "+ // PRESTAMO MUTUAL
                       "FROM (SELECT * FROM vwmovimiento1 WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado<>"+iidEstadoPrestamo+" AND identidad="+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'PF' AS tipo, mo.id AS idmovimiento, mo.cuenta||' '||mo.tipocreditobreve||' ('||mo.entidad||')' AS denominacion, mo.idcuenta, mo.cuenta, mo.identidad, mo.entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres "+
                       "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                       "     ELSE mo.tasainteres END AS tasainteres, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.montoaprobado AS montototal, mo.idtipocredito, mo.tipocredito, dop.fechavencimiento, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, mo.estado "+ // PRESTAMO FINANCIERA
                       "FROM (SELECT * FROM vwmovimiento1 WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado<>"+iidEstadoPrestamo+" AND identidad<>"+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'O' AS tipo, ap.id AS idmovimiento, ap.cuenta||' ('||ap.plan||')' AS denominacion, ap.idcuenta, ap.cuenta, 0 AS identidad, '' AS entidad, dop.tabla, ap.fechaoperacion, 0 AS tasainteres, ap.plazo, FALSE AS cancelacion, ap.numerooperacion, 0 AS numerosolicitud, ap.importe*ap.plazo AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, ap.estado "+
                       "FROM (SELECT * FROM vwahorroprogramado1 WHERE idsocio="+iidSocio+" AND idestado<>"+iidEstadoAhorro+") AS ap LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=ap.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=ap.id AND cr.tabla='ap' "+
                       "WHERE dop.tabla='ap' "+
                   "UNION " +
                   "SELECT 'O' AS tipo, of.id AS idmovimiento, of.cuenta AS denominacion, of.idcuenta, of.cuenta, 0 AS identidad, '' AS entidad, dop.tabla, of.fechaoperacion, 0 AS tasainteres, of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, of.importe AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, of.estado "+
                       "FROM (SELECT * FROM vwoperacionfija1 WHERE idsocio="+iidSocio+" AND idestado<>"+iidEstadoOperacion+" AND idcuenta<>"+iidAporteCapital+" AND idcuenta<>"+iidSolidaridad+" AND idcuenta<>"+iidCuotaSocial+" AND idcuenta<>"+iidFondoPrevision+") AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                       "WHERE dop.tabla='of' "+
                   "UNION " +
                   "SELECT 'O' AS tipo, js.id AS idmovimiento, js.cuenta AS denominacion, js.idcuenta, js.cuenta, 0 AS identidad, '' AS entidad, dop.tabla, js.fechaoperacion, 0 AS tasainteres, 0 AS plazo, FALSE AS cancelacion, js.numerooperacion, 0 AS numerosolicitud, js.importetitular+js.importeadherente*js.cantidadadherente AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, js.estado "+
                       "FROM (SELECT * FROM vwfondojuridicosepelio1 WHERE idsocio="+iidSocio+" AND idestado<>"+iidEstadoFondo+") AS js LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=js.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=js.id AND cr.tabla='js' "+
                       "WHERE dop.tabla='js' "+
                   "UNION " +
                   "SELECT 'CE' AS tipo, ce.id AS idmovimiento, 'Ch. '||ce.numerocheque||' '||ce.descripcion AS denominacion, 0 AS idcuenta, '' AS cuenta, 0 AS identidad, '' AS entidad, 'ce' AS tabla, ce.fecha AS fechaoperacion, 0 AS tasainteres, 0 AS plazo, FALSE AS cancelacion, ce.id AS numerooperacion, 0 AS numerosolicitud, ce.monto AS montototal, 0 AS idtipocredito, '' AS tipocredito, ce.fecha AS fechavencimiento, 0 AS saldo, '' AS rubro, '' AS estado "+
                       "FROM banco.vwchequeemitido1 AS ce "+
                       "WHERE ce.cedularuc=(SELECT cedula FROM socio WHERE id="+iidSocio+") "+
                   "UNION " +
                   "SELECT 'SS' AS tipo, ss.id AS idmovimiento, ss.cobertura AS denominacion, 0 AS idcuenta, 'x' AS cuenta, 0 AS identidad, '' AS entidad, 'ss' AS tabla, ss.fechaaprobado AS fechaoperacion, 0 AS tasainteres, 0 AS plazo, FALSE AS cancelacion, ss.numerosolicitud AS numerooperacion, ss.numerosolicitud AS numerosolicitud, ss.montoaprobado AS montototal, 0 AS idtipocredito, '' AS tipocredito, ss.fechasolicitud AS fechavencimiento, 0 AS saldo, '' AS rubro, ss.estado "+
                       "FROM vwsolidaridad1 AS ss "+
                       "WHERE ss.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT 'SE' AS tipo, se.id AS idmovimiento, se.coberturasepelio AS denominacion, 0 AS idcuenta, 'x' AS cuenta, 0 AS identidad, '' AS entidad, 'se' AS tabla, se.fechaaprobado AS fechaoperacion, 0 AS tasainteres, 0 AS plazo, FALSE AS cancelacion, se.numerosolicitud AS numerooperacion, se.numerosolicitud AS numerosolicitud, se.montoaprobado AS montototal, 0 AS idtipocredito, '' AS tipocredito, se.fechasolicitud AS fechavencimiento, 0 AS saldo, '' AS rubro, se.estado "+
                       "FROM vwsepelio1 AS se "+
                       "WHERE se.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT 'A' AS tipo, of.id AS idmovimiento, of.cuenta AS denominacion, of.idcuenta, of.cuenta, 0 AS identidad, '' AS entidad, dop.tabla, of.fechaoperacion, 0 AS tasainteres, of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, of.importe AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, of.estado "+
                       "FROM (SELECT * FROM vwoperacionfija1 WHERE idsocio="+iidSocio+" AND idestado<>"+iidEstadoOperacion+" AND (idcuenta="+iidAporteCapital+" OR idcuenta="+iidSolidaridad+" OR idcuenta="+iidCuotaSocial+" OR idcuenta="+iidFondoPrevision+")) AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                       "WHERE dop.tabla='of' "+
               ") AS detalle "+sorden;
    }

    public String getConsultaX(java.util.Date dfecha, int iidSocio, int iidMutual, int iidAporteCapital, int iidSolidaridad, int iidCuotaSocial, int iidFondoPrevision, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, int iidInteresPrestamo, int iidEstadoOrdenCredito, int iidEstadoPrestamo, String sorden) {
        return "SELECT ope.*, dop.fechavencimiento, dop.saldocapital+dop.saldointeres AS saldo FROM("+
            "SELECT "+
                "'OC' AS tipo, mo.id AS idmovimiento, cu.descripcion||' ('||en.descripcion||')' AS denominacion, mo.idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.idtipocredito, "+
                "tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro, st.descripcion AS estado "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN subtipo AS st ON mo.idestado=st.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado<>"+iidEstadoOrdenCredito+" AND mo.identidad<>"+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'PM' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve AS denominacion, mo.idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "CASE WHEN mo.tasainteres=0 THEN en.tasainteres  "+
                "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                "     ELSE mo.tasainteres END AS tasainteres, "+
                "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro, st.descripcion AS estado "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN subtipo AS st ON mo.idestado=st.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado<>"+iidEstadoPrestamo+" AND mo.identidad="+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'PF' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve||' ('||en.descripcion||')' AS denominacion, "+
                "mo.idcuenta, cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "CASE WHEN mo.tasainteres=0 THEN en.tasainteres "+
                "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                "     ELSE mo.tasainteres END AS tasainteres, "+
                "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, "+
                "mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro, st.descripcion AS estado "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN subtipo AS st ON mo.idestado=st.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado<>"+iidEstadoPrestamo+" AND mo.identidad<>"+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, ap.id AS idmovimiento, cu.descripcion||' ('||pa.descripcion||')' AS denominacion, ap.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'ap' AS tabla, ap.fechaoperacion, 0 AS tasainteres, ap.plazo, "+
                "FALSE AS cancelacion, ap.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, ap.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro, st.descripcion AS estado "+
                "FROM ahorroprogramado AS ap "+
                "LEFT JOIN planahorroprogramado AS pa ON ap.idplan=pa.id "+
                "LEFT JOIN socio AS so ON ap.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON ap.idcuenta=cu.id "+
                "LEFT JOIN subtipo AS st ON ap.idestado=st.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=ap.id AND cr.tabla='ap' "+
                "WHERE ap.idsocio="+iidSocio+" AND ap.idestado<>"+iidEstadoAhorro+" "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, of.id AS idmovimiento, cu.descripcion AS denominacion, of.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, of.fechaoperacion, 0 AS tasainteres, "+
                "of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, of.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro, st.descripcion AS estado "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id "+
                "LEFT JOIN subtipo AS st ON of.idestado=st.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                "WHERE of.idsocio="+iidSocio+" AND of.idestado<>"+iidEstadoOperacion+" AND of.idcuenta<>"+iidAporteCapital+" "+
                "AND of.idcuenta<>"+iidSolidaridad+" AND of.idcuenta<>"+iidCuotaSocial+" AND of.idcuenta<>"+iidFondoPrevision+" "+
            "UNION "+
            "SELECT "+
                "'A' AS tipo, of.id AS idmovimiento, cu.descripcion AS denominacion, of.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, of.fechaoperacion, 0 AS tasainteres, "+
                "of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, of.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro, st.descripcion AS estado "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id "+
                "LEFT JOIN subtipo AS st ON of.idestado=st.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                "WHERE of.idsocio="+iidSocio+" AND of.idestado<>"+iidEstadoOperacion+" AND (of.idcuenta="+iidAporteCapital+" "+
                "OR of.idcuenta="+iidSolidaridad+" OR of.idcuenta="+iidCuotaSocial+" OR of.idcuenta="+iidFondoPrevision+") "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, js.id AS idmovimiento, cu.descripcion AS denominacion, js.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'js' AS tabla, js.fechaoperacion, 0 AS tasainteres, "+
                "0 AS plazo, FALSE AS cancelacion, js.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, "+
                "js.importetitular+js.importeadherente*js.cantidadadherente AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro, st.descripcion AS estado "+
                "FROM fondojuridicosepelio AS js "+
                "LEFT JOIN socio AS so ON js.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON js.idcuenta=cu.id "+
                "LEFT JOIN subtipo AS st ON js.idestado=st.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=js.id AND cr.tabla='js' "+
                "WHERE js.idsocio="+iidSocio+" AND js.idestado<>"+iidEstadoFondo+" "+
            ") AS ope "+
                "INNER JOIN detalleoperacion AS dop ON dop.idmovimiento=ope.idmovimiento AND dop.tabla=ope.tabla "+
            "UNION "+
            "SELECT 'CE' AS tipo, ce.id AS idmovimiento, 'Ch. '||ce.numerocheque||' '||ce.descripcion AS denominacion, 0 AS idcuenta, '' AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'ce' AS tabla, ce.fecha AS fechaoperacion, 0 AS tasainteres, 0 AS plazo, FALSE AS cancelacion, "+
                "ce.id AS numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, ce.monto AS montototal, "+
                "'' AS rubro, '' AS estado, ce.fecha AS fechavencimiento, 0 AS saldo "+
                "FROM banco.vwchequeemitido1 AS ce "+
                "WHERE ce.cedularuc=(SELECT cedula FROM socio WHERE id="+iidSocio+") "+
            "UNION " +
            "SELECT 'SS' AS tipo, ss.id AS idmovimiento, ss.cobertura AS denominacion, 0 AS idcuenta, '' AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'ss' AS tabla, ss.fechaaprobado AS fechaoperacion, 0 AS tasainteres, 0 AS plazo, FALSE AS cancelacion, "+
                "ss.numerosolicitud AS numerooperacion, ss.numerosolicitud AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, "+
                "ss.montoaprobado AS montototal, '' AS rubro, ss.estado, ss.fechasolicitud AS fechavencimiento, 0 AS saldo "+
                "FROM vwsolidaridad1 AS ss "+
                "WHERE ss.idsocio="+iidSocio+" "+
            "UNION " +
            "SELECT 'SE' AS tipo, se.id AS idmovimiento, se.coberturasepelio AS denominacion, 0 AS idcuenta, '' AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'se' AS tabla, se.fechaaprobado AS fechaoperacion, 0 AS tasainteres, 0 AS plazo, FALSE AS cancelacion, "+
                "se.numerosolicitud AS numerooperacion, se.numerosolicitud AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, "+
                "se.montoaprobado AS montototal, '' AS rubro, se.estado, se.fechasolicitud AS fechavencimiento, 0 AS saldo "+
                "FROM vwsepelio1 AS se "+
                "WHERE se.idsocio="+iidSocio+" "+
            sorden;
    }
    
    public String getConsultaEstado(String sfiltro, String sorden) {
        return "SELECT * FROM detalleoperacion WHERE "+sfiltro+sorden;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
