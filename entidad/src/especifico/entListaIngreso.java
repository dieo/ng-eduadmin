/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entListaIngreso {

    private int iid;
    private int inumeroIngreso;
    private int icuota;
    private java.util.Date dfechaOperacion;
    private java.util.Date dfechaOperacionIngreso;
    private int iidCuenta;
    private String scuenta;
    private double umontoAmortizacion;
    private double umontoExoneracion;
    private double umontoCapital;
    private double umontoInteres;
    private String sestado;
    
    private int iestadoRegistro;
    
    public entListaIngreso() {
        this.iid = 0;
        this.inumeroIngreso = 0;
        this.icuota = 0;
        this.dfechaOperacion = null;
        this.dfechaOperacionIngreso = null;
        this.iidCuenta = 0;
        this.scuenta = "";
        this.umontoAmortizacion = 0.0;
        this.umontoExoneracion = 0.0;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.sestado = "";
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entListaIngreso(int iid, int inumeroIngreso, int icuota, java.util.Date dfechaOperacion, java.util.Date dfechaOperacionIngreso, int iidCuenta, String scuenta, double umontoAmortizacion, double umontoExoneracion, double umontoCapital, double umontoInteres, String sestado, int iestadoRegistro) {
        this.iid = iid;
        this.inumeroIngreso = inumeroIngreso;
        this.icuota = icuota;
        this.dfechaOperacion = dfechaOperacion;
        this.dfechaOperacionIngreso = dfechaOperacionIngreso;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.umontoAmortizacion = umontoAmortizacion;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.sestado = sestado;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroIngreso, int icuota, java.util.Date dfechaOperacion, java.util.Date dfechaOperacionIngreso, int iidCuenta, String scuenta, double umontoAmortizacion, double umontoExoneracion, double umontoCapital, double umontoInteres, String sestado, int iestadoRegistro) {
        this.iid = iid;
        this.inumeroIngreso = inumeroIngreso;
        this.icuota = icuota;
        this.dfechaOperacion = dfechaOperacion;
        this.dfechaOperacionIngreso = dfechaOperacionIngreso;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.umontoAmortizacion = umontoAmortizacion;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.sestado = sestado;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entListaIngreso copiar(entListaIngreso destino) {
        destino.setEntidad(this.getId(), this.getNumeroIngreso(), this.getCuota(), this.getFechaOperacion(), this.getFechaOperacionIngreso(), this.getIdCuenta(), this.getCuenta(), this.getMontoAmortizacion(), this.getMontoExoneracion(), this.getMontoCapital(), this.getMontoInteres(), this.getEstado(), this.getEstadoRegistro());
        return destino;
    }

    public entListaIngreso cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroIngreso(rs.getInt("numeroingreso")); }
        catch(Exception e) {}
        try { this.setCuota(rs.getInt("cuota")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setFechaOperacionIngreso(rs.getDate("fechaoperacioningreso")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setMontoAmortizacion(rs.getDouble("montocobro")); }
        catch(Exception e) {}
        try { this.setMontoExoneracion(rs.getDouble("montoexoneracion")); }
        catch(Exception e) {}
        try { this.setMontoCapital(0.0); }
        catch(Exception e) {}
        try { this.setMontoInteres(0.0); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroIngreso(int inumeroIngreso) {
        this.inumeroIngreso = inumeroIngreso;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = (java.util.Date)dfechaOperacion.clone();
    }

    public void setFechaOperacionIngreso(java.util.Date dfechaOperacionIngreso) {
        this.dfechaOperacionIngreso = (java.util.Date)dfechaOperacionIngreso.clone();
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setMontoAmortizacion(double umontoAmortizacion) {
        this.umontoAmortizacion = umontoAmortizacion;
    }

    public void setMontoExoneracion(double umontoExoneracion) {
        this.umontoExoneracion = umontoExoneracion;
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getNumeroIngreso() {
        return this.inumeroIngreso;
    }

    public int getCuota() {
        return this.icuota;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public java.util.Date getFechaOperacionIngreso() {
        return this.dfechaOperacionIngreso;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public double getMontoAmortizacion() {
        return this.umontoAmortizacion;
    }

    public double getMontoExoneracion() {
        return this.umontoExoneracion;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public String getEstado() {
        return this.sestado;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

}
