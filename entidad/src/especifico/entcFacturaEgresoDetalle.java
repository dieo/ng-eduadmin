/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entcFacturaEgresoDetalle {

    protected int iid;
    protected int iitem;
    protected int iidFactura;
    protected int iidPlanCuenta;
    protected String snroPlanCuenta;
    protected String snroPlanCuenta1;
    protected String splanCuenta;
    protected int iidArticulo;
    protected String sdescripcion;
    protected int iidFilial;
    protected String sfilial;
    protected int iidUnidadMedida;
    protected String sunidadMedida;
    protected int iidImpuesto;
    protected String simpuesto;
    protected double utasa;
    protected double ucantidad;
    protected double uconversion;
    protected double uunidades;
    protected double ucosto;
    protected double usubtotal;
    protected boolean bivaGasto;
    protected int iidCentroCosto;
    protected String scentroCosto;
    
    protected short hidEstado;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ITEM = "Item (no editable)";
    public static final String TEXTO_CANTIDAD = "Cantidad (no vacío, valor positivo)";
    public static final String TEXTO_PRODUCTO = "Producto (no vacío)";
    public static final String TEXTO_FILIAL = "Filial a que corresponde (no vacío)";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de Medida (no vacía)";
    public static final String TEXTO_IMPUESTO = "Impuesto (no vacío)";
    public static final String TEXTO_IVAGASTO = "Si va a IVA gasto";
    public static final String TEXTO_PRECIO = "Precio unitario (no vacío, valor positivo)";

    public entcFacturaEgresoDetalle() {
        this.iid = 0;
        this.iitem = 0;
        this.iidFactura = 0;
        this.iidPlanCuenta = 0;
        this.snroPlanCuenta = "";
        this.snroPlanCuenta1 = "";
        this.splanCuenta = "";
        this.iidArticulo = 0;
        this.sdescripcion = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.iidUnidadMedida = 0;
        this.sunidadMedida = "";
        this.iidImpuesto = 0;
        this.simpuesto = "";
        this.utasa = 0.0;
        this.ucantidad = 0.0;
        this.uconversion = 0.0;
        this.uunidades = 0.0;
        this.ucosto = 0.0;
        this.usubtotal = 0.0;
        this.bivaGasto = false;
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.hidEstado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entcFacturaEgresoDetalle(int iid, int iitem, int iidFactura, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta1, String splanCuenta, int iidProducto, String sproducto, int iidFilial, String sfilial, int iidUnidadMedida, String sunidadMedida, int iidImpuesto, String simpuesto, double utasa, double ucantidad, double uconversion, double uunidades, double uprecio, double usubtotal, boolean bivaGasto, int iidCentroCosto, String scentroCosto, short hidEstado) {
        this.iid = iid;
        this.iitem = iitem;
        this.iidFactura = iidFactura;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta1 = snroPlanCuenta1;
        this.splanCuenta = splanCuenta;
        this.iidArticulo = iidProducto;
        this.sdescripcion = sproducto;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.utasa = utasa;
        this.ucantidad = ucantidad;
        this.uconversion = uconversion;
        this.uunidades = uunidades;
        this.ucosto = uprecio;
        this.usubtotal = usubtotal;
        this.bivaGasto = bivaGasto;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.hidEstado = hidEstado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iitem, int iidFactura, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta1, String splanCuenta, int iidProducto, String sproducto, int iidFilial, String sfilial, int iidUnidadMedida, String sunidadMedida, int iidImpuesto, String simpuesto, double utasa, double ucantidad, double uconversion, double uunidades, double uprecio, double usubtotal, boolean bivaGasto, int iidCentroCosto, String scentroCosto, short hidEstado) {
        this.iid = iid;
        this.iitem = iitem;
        this.iidFactura = iidFactura;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta1 = snroPlanCuenta1;
        this.splanCuenta = splanCuenta;
        this.iidArticulo = iidProducto;
        this.sdescripcion = sproducto;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.utasa = utasa;
        this.ucantidad = ucantidad;
        this.uconversion = uconversion;
        this.uunidades = uunidades;
        this.ucosto = uprecio;
        this.usubtotal = usubtotal;
        this.bivaGasto = bivaGasto;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.hidEstado = hidEstado;
    }

    public entcFacturaEgresoDetalle copiar(entcFacturaEgresoDetalle destino) {
        destino.setEntidad(this.getId(), this.getItem(), this.getIdFactura(), this.getIdPlanCuenta(), this.getNroPlanCuenta(), this.getNroPlanCuenta1(), this.getPlanCuenta(), this.getIdArticulo(), this.getDescripcion(), this.getIdFilial(), this.getFilial(), this.getIdUnidadMedida(), this.getUnidadMedida(), this.getIdImpuesto(), this.getImpuesto(), this.getTasa(), this.getCantidad(), this.getConversion(), this.getUnidades(), this.getCosto(), this.getSubtotal(), this.getIvaGasto(), this.getIdCentroCosto(), this.getCentroCosto(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcFacturaEgresoDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setItem(0); }
        catch(Exception e) {}
        try { this.setIdFactura(rs.getInt("idfacturaegreso")); }
        catch(Exception e) {}
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) { }
        try { this.setNroPlanCuenta(rs.getString("nroplancuenta1")); }
        catch(Exception e) { }
        try { this.setNroPlanCuenta1(rs.getString("nroplancuenta")); }
        catch(Exception e) { }
        try { this.setPlanCuenta(rs.getString("plancuenta")); }
        catch(Exception e) { }
        try { this.setIdArticulo(rs.getInt("idarticulo")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) { }
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) { }
        try { this.setIdUnidadMedida(rs.getInt("idunidadmedida")); }
        catch(Exception e) {}
        try { this.setUnidadMedida(rs.getString("unidadmedida")); }
        catch(Exception e) {}
        try { this.setIdImpuesto(rs.getInt("idimpuesto")); }
        catch(Exception e) {}
        try { this.setImpuesto(rs.getString("impuesto")); }
        catch(Exception e) {}
        try { this.setTasa(rs.getDouble("tasa")); }
        catch(Exception e) {}
        try { this.setCantidad(rs.getDouble("cantidad")); }
        catch(Exception e) {}
        try { this.setConversion(rs.getDouble("conversion")); }
        catch(Exception e) {}
        try { this.setUnidades(rs.getDouble("unidades")); }
        catch(Exception e) {}
        try { this.setCosto(rs.getDouble("costo")); }
        catch(Exception e) {}
        try { this.setSubtotal(0.0); }
        catch(Exception e) {}
        try { this.setIvaGasto(rs.getBoolean("ivagasto")); }
        catch(Exception e) {}
        try { this.setIdCentroCosto(rs.getInt("idcentrocosto")); }
        catch(Exception e) {}
        try { this.setCentroCosto(rs.getString("centrocosto")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setItem(int iitem) {
        this.iitem = iitem;
    }

    public void setIdFactura(int iidFactura) {
        this.iidFactura = iidFactura;
    }

    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }
    
    public void setNroPlanCuenta(String snroPlanCuenta) {
        this.snroPlanCuenta = snroPlanCuenta;
    }
    
    public void setNroPlanCuenta1(String snroPlanCuenta1) {
        this.snroPlanCuenta1 = snroPlanCuenta1;
    }
    
    public void setPlanCuenta(String splanCuenta) {
        this.splanCuenta = splanCuenta;
    }
    
    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }
    
    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }

    public void setUnidadMedida(String sunidadMedida) {
        this.sunidadMedida = sunidadMedida;
    }

    public void setIdImpuesto(int iidImpuesto) {
        this.iidImpuesto = iidImpuesto;
    }

    public void setImpuesto(String simpuesto) {
        this.simpuesto = simpuesto;
    }

    public void setTasa(double utasa) {
        this.utasa = utasa;
    }

    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setConversion(double uconversion) {
        this.uconversion = uconversion;
    }

    public void setUnidades(double uunidades) {
        this.uunidades = uunidades;
    }

    public void setCosto(double ucosto) {
        this.ucosto = ucosto;
    }

    public void setSubtotal(double usubtotal) {
        this.usubtotal = usubtotal;
    }

    public void setEstadoRegistro(short hidEstado) {
        this.hidEstado = hidEstado;
    }

    public void setIvaGasto(boolean bivaGasto) {
        this.bivaGasto = bivaGasto;
    }

    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }

    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }

    public int getId() {
        return this.iid;
    }

    public int getItem() {
        return this.iitem;
    }

    public int getIdFactura() {
        return this.iidFactura;
    }

    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }
    
    public String getNroPlanCuenta() {
        return this.snroPlanCuenta;
    }
    
    public String getNroPlanCuenta1() {
        return this.snroPlanCuenta1;
    }
    
    public String getPlanCuenta() {
        return this.splanCuenta;
    }
    
    public int getIdArticulo() {
        return this.iidArticulo;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }
    
    public String getFilial() {
        return this.sfilial;
    }
    
    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }

    public String getUnidadMedida() {
        return this.sunidadMedida;
    }

    public int getIdImpuesto() {
        return this.iidImpuesto;
    }

    public String getImpuesto() {
        return this.simpuesto;
    }

    public double getTasa() {
        return this.utasa;
    }

    public double getCantidad() {
        return this.ucantidad;
    }

    public double getConversion() {
        return this.uconversion;
    }

    public double getUnidades() {
        return this.uunidades;
    }

    public double getCosto() {
        return this.ucosto;
    }

    public double getSubtotal() {
        return this.usubtotal;
    }

    public boolean getIvaGasto() {
        return bivaGasto;
    }

    public int getIdCentroCosto() {
        return iidCentroCosto;
    }

    public String getCentroCosto() {
        return scentroCosto;
    }

    public short getEstadoRegistro() {
        return this.hidEstado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdFilial()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FILIAL;
            bvalido = false;
        }
        if (this.getCantidad()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CANTIDAD;
            bvalido = false;
        }
        if (this.getCosto()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PRECIO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.facturaegresodetalle("+
            this.getId()+","+
            this.getIdFactura()+","+
            this.getIdPlanCuenta()+","+
            this.getIdArticulo()+","+
            this.getIdFilial()+","+
            this.getIdImpuesto()+","+
            this.getCantidad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getCosto()+","+
            this.getUnidades()+","+
            this.getCosto()*this.getCantidad()+","+
            this.getIvaGasto()+","+
            this.getIdCentroCosto()+","+
            this.getEstadoRegistro()+")";
    }

}
