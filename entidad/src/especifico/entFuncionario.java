/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entFuncionario extends generico.entGenericaPersona {

    protected int iidTipoFuncionario;
    protected String stipoFuncionario;
    protected int iidEstado;
    protected String sestado;
    protected int iidCargo;
    protected String scargo;
    protected int iidFilial;
    protected String sfilial;
    protected int iidDependencia;
    protected String sdependencia;
    
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLID0 = "Apellido (no vacío, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_TIPO_FUNCIONARIO = "Tipo de funcionario (no vacío)";
    public static final String TEXTO_ESTADO = "Estado (no vacío)";
    public static final String TEXTO_CARGO = "Cargo (no vacío)";
    public static final String TEXTO_FILIAL = "Filial (no vacía)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia (no vacía)";

    public entFuncionario() {
        super();
        this.iidTipoFuncionario = 0;
        this.stipoFuncionario = "";
        this.iidEstado = 0;
        this.sestado = "";
        this.iidCargo = 0;
        this.scargo = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.iidDependencia = 0;
        this.sdependencia = "";
    }

    public entFuncionario(int iid, String snombre, String sapellido, int iidTipoFuncionario, String stipoFuncionario, int iidEstado, String sestado, int iidCargo, String scargo, int iidFilial, String sfilial, int iidDependencia, String sdependencia, short hestado) {
        super(iid, snombre, sapellido, "", hestado);
        this.iidTipoFuncionario = iidTipoFuncionario;
        this.stipoFuncionario = stipoFuncionario;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
    }

    public void setEntidad(int iid, String snombre, String sapellido, int iidTipoFuncionario, String stipoFuncionario, int iidEstado, String sestado, int iidCargo, String scargo, int iidFilial, String sfilial, int iidDependencia, String sdependencia, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.iidTipoFuncionario = iidTipoFuncionario;
        this.stipoFuncionario = stipoFuncionario;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entFuncionario copiar(entFuncionario destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getIdTipoFuncionario(), this.getTipoFuncionario(), this.getIdEstado(), this.getEstado(), this.getIdCargo(), this.getCargo(), this.getIdFilial(), this.getFilial(), this.getIdDependencia(), this.getDependencia(), this.getEstadoRegistro());
        return destino;
    }
    
    public entFuncionario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setIdTipoFuncionario(rs.getInt("idtipofuncionario")); }
        catch(Exception e) {}
        try { this.setTipoFuncionario(rs.getString("tipofuncionario")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setIdCargo(rs.getInt("idcargo")); }
        catch(Exception e) {}
        try { this.setCargo(rs.getString("cargo")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdTipoFuncionario(int iidTipoFuncionario) {
        this.iidTipoFuncionario = iidTipoFuncionario;
    }
    
    public void setTipoFuncionario(String stipoFuncionario) {
        this.stipoFuncionario = stipoFuncionario;
    }
    
    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }
    
    public void setEstado(String sestado) {
        this.sestado = sestado;
    }
    
    public void setIdCargo(int iidCargo) {
        this.iidCargo = iidCargo;
    }
    
    public void setCargo(String scargo) {
        this.scargo = scargo;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }
    
    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public int getIdTipoFuncionario() {
        return this.iidTipoFuncionario;
    }
    
    public String getTipoFuncionario() {
        return this.stipoFuncionario;
    }
    
    public int getIdEstado() {
        return this.iidEstado;
    }
    
    public String getEstado() {
        return this.sestado;
    }
    
    public int getIdCargo() {
        return this.iidCargo;
    }
    
    public String getCargo() {
        return this.scargo;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }
    
    public String getFilial() {
        return this.sfilial;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }
    
    public String getDependencia() {
        return this.sdependencia;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_APELLID0;
            bvalido = false;
        }
        if (this.getIdTipoFuncionario() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_FUNCIONARIO;
            bvalido = false;
        }
        if (this.getIdEstado() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ESTADO;
            bvalido = false;
        }
        if (this.getIdCargo() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CARGO;
            bvalido = false;
        }
        if (this.getIdFilial() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FILIAL;
            bvalido = false;
        }
        if (this.getIdDependencia() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DEPENDENCIA;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT funcionario("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            this.getIdTipoFuncionario()+","+
            this.getIdEstado()+","+
            this.getIdCargo()+","+
            this.getIdFilial()+","+
            this.getIdDependencia()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Apellido", "apellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Tipo Funcionario", "tipofuncionario", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Estado", "estado", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Cargo", "cargo", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Filial", "filial", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Dependencia", "dependencia", generico.entLista.tipo_texto));
        return lst;
    }
    
}
