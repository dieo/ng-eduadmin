/*0
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleIngreso {
    
    private int iid;
    private int iidMovimiento;
    private int iidIngreso;
    private int iidMovimientoCancela;
    private String stabla;
    private int iidDetalleOperacion;
    private double umontoCapital;
    private double umontoInteres;
    private double umontoExoneracion;
    private double umontoSaldo;
    private double uinteresMoratorio;
    private double uinteresPunitorio;
    private String senviado;
    private String sestado; // A=atraso - S=saldo - E=enviado
    private int icuota;
    private java.util.Date dfechaVencimiento;
    private double uexoneracion;
    private int iorden;

    private int inumeroOperacion;
    private int iplazo;
    private java.util.Date dfechaOperacion;
    private double utasaInteresMoratorio;
    private double utasaInteresPunitorio;
    private boolean batraso;
    private boolean bcancela;
    private int iidCuenta;
    private String scuenta;
    private String sdescripcion;
    private String sprioridad;
    private double umontoAtraso;
    //private double umontoSaldoTotal;
    private double umontoCobro;

    private short hestadoRegistro;
    private String smensaje;

    public static final String estado_atraso = "A";
    public static final String estado_enviado = "E";
    public static final String estado_saldo = "S";
    public static final String estado_cobro = "C";

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CUENTA = "Cuenta (no vacía)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (no vacío)";
    public static final String TEXTO_PLAZO = "Plazo (no editable)";
    public static final String TEXTO_CAPITAL = "Capital (no editable)";
    public static final String TEXTO_INTERES = "Interés (no editable)";
    public static final String TEXTO_MONEDA = "Moneda (no editable)";
    public static final String TEXTO_CUOTA = "Cuota (no editable)";
    public static final String TEXTO_CUOTA_INICIAL = "Cuota Inicial (no editable)";
    public static final String TEXTO_CUOTA_FINAL = "Cuota Final (no editable)";
    public static final String TEXTO_TASA = "Tasa de Interés (no editable)";
    public static final String TEXTO_MONTO_CAPITAL = "Monto Capital (no vacío, valor positivo)";
    public static final String TEXTO_MONTO_INTERES = "Monto Interés (no vacío, valor positivo)";
    public static final String TEXTO_ENVIADO = "Enviado a Giraduría";
    public static final String TEXTO_CANCELA = "Cancela";

    public static final String TEXTO_FECHA_OPERACION = "Fecha de Operación (no editable)";
    public static final String TEXTO_ATRASO_CAPITAL = "Atraso Capital (no editable)";
    public static final String TEXTO_ATRASO_INTERES = "Atraso Interés (no editable)";
    public static final String TEXTO_SALDO_CAPITAL = "Saldo Capital (no editable)";
    public static final String TEXTO_SALDO_INTERES = "Saldo Interés (no editable)";
    public static final String TEXTO_ACTUAL_CAPITAL = "Cuota Actual Capital (no editable)";
    public static final String TEXTO_ACTUAL_INTERES = "Cuota Actual Interés (no editable)";
    
    public entDetalleIngreso() {
        this.iid = 0;
        this.iidMovimiento = 0;
        this.iidIngreso = 0;
        this.iidMovimientoCancela = 0;
        this.stabla = "";
        this.iidDetalleOperacion = 0;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.umontoExoneracion = 0.0;
        this.umontoSaldo = 0.0;
        this.uinteresMoratorio = 0.0;
        this.uinteresPunitorio = 0.0;
        this.senviado = "";
        this.sestado = "";
        this.icuota = 0;
        this.dfechaVencimiento = null;
        this.uexoneracion = 0.0;
        this.iorden = 0;

        this.inumeroOperacion = 0;
        this.iplazo = 0;
        this.dfechaOperacion = null;
        this.utasaInteresMoratorio = 0.0;
        this.utasaInteresPunitorio = 0.0;
        this.batraso = false;
        this.bcancela = false;
        this.iidCuenta = 0;
        this.scuenta = "";
        this.sdescripcion = "";
        this.sprioridad = "";
        this.umontoAtraso = 0.0;
        //this.umontoSaldoTotal = 0.0;
        this.umontoCobro = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetalleIngreso(int iid, int iidMovimiento, int iidIngreso, int iidMovimientoCancela, String stabla, int iidDetalleOperacion, double umontoCapital, double umontoInteres, double umontoExoneracion, double umontoSaldo, double usaldoInteres, double uinteresMoratorio, double uinteresPunitorio, String senviado, String sestado, int icuota, java.util.Date dfechaVencimiento, double uexoneracion, int iorden, int inumeroOperacion, int iplazo, java.util.Date dfechaOperacion, double utasaInteresMoratorio, double utasaInteresPunitorio, boolean batraso, boolean bcancela, int iidCuenta, String scuenta, String sdescripcion, String sprioridad, double umontoAtraso, double umontoSaldoTotal, double umontoCobro, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidIngreso = iidIngreso;
        this.iidMovimientoCancela = iidMovimientoCancela;
        this.stabla = stabla;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoSaldo = umontoSaldo;
        this.uinteresMoratorio = uinteresMoratorio;
        this.uinteresPunitorio = uinteresPunitorio;
        this.senviado = senviado;
        this.sestado = sestado;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.uexoneracion = uexoneracion;
        this.iorden = iorden;

        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.batraso = batraso;
        this.bcancela = bcancela;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdescripcion = sdescripcion;
        this.sprioridad = sprioridad;
        this.umontoAtraso = umontoAtraso;
        //this.umontoSaldoTotal = umontoSaldoTotal;
        this.umontoCobro = umontoCobro;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidMovimiento, int iidIngreso, int iidMovimientoCancela, String stabla, int iidDetalleOperacion, double umontoCapital, double umontoInteres, double umontoExoneracion, double umontoSaldo, double usaldoInteres, double uinteresMoratorio, double uinteresPunitorio, String senviado, String sestado, int icuota, java.util.Date dfechaVencimiento, double uexoneracion, int iorden, int inumeroOperacion, int iplazo, java.util.Date dfechaOperacion, double utasaInteresMoratorio, double utasaInteresPunitorio, boolean batraso, boolean bcancela, int iidCuenta, String scuenta, String sdescripcion, String sprioridad, double umontoAtraso, double umontoSaldoTotal, double umontoCobro, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidIngreso = iidIngreso;
        this.iidMovimientoCancela = iidMovimientoCancela;
        this.stabla = stabla;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoSaldo = umontoSaldo;
        this.uinteresMoratorio = uinteresMoratorio;
        this.uinteresPunitorio = uinteresPunitorio;
        this.senviado = senviado;
        this.sestado = sestado;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.uexoneracion = uexoneracion;
        this.iorden = iorden;

        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.batraso = batraso;
        this.bcancela = bcancela;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdescripcion = sdescripcion;
        this.sprioridad = sprioridad;
        this.umontoAtraso = umontoAtraso;
        //this.umontoSaldoTotal = umontoSaldoTotal;
        this.umontoCobro = umontoCobro;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleIngreso copiar(entDetalleIngreso destino) {
        //destino.setEntidad(this.getId(), this.getIdMovimiento(), this.getIdIngreso(), this.getIdMovimientoCancela(), this.getTabla(), this.getIdDetalleOperacion(), this.getMontoCapital(), this.getMontoInteres(), this.getMontoExoneracion(), this.getMontoSaldo(), 0, this.getInteresMoratorio(), this.getInteresPunitorio(), this.getEnviado(), this.getEstado(), this.getCuota(), this.getFechaVencimiento(), this.getExoneracion(), this.getOrden(), this.getNumeroOperacion(), this.getPlazo(), this.getFechaOperacion(), this.getTasaInteresMoratorio(), this.getTasaInteresPunitorio(), this.getAtraso(), this.getCancela(), this.getIdCuenta(), this.getCuenta(), this.getDescripcion(), this.getPrioridad(), this.getMontoAtraso(), this.getMontoSaldoTotal(), this.getMontoCobro(), this.getEstadoRegistro());
        destino.setEntidad(this.getId(), this.getIdMovimiento(), this.getIdIngreso(), this.getIdMovimientoCancela(), this.getTabla(), this.getIdDetalleOperacion(), this.getMontoCapital(), this.getMontoInteres(), this.getMontoExoneracion(), this.getMontoSaldo(), 0, this.getInteresMoratorio(), this.getInteresPunitorio(), this.getEnviado(), this.getEstado(), this.getCuota(), this.getFechaVencimiento(), this.getExoneracion(), this.getOrden(), this.getNumeroOperacion(), this.getPlazo(), this.getFechaOperacion(), this.getTasaInteresMoratorio(), this.getTasaInteresPunitorio(), this.getAtraso(), this.getCancela(), this.getIdCuenta(), this.getCuenta(), this.getDescripcion(), this.getPrioridad(), this.getMontoAtraso(), 0, this.getMontoCobro(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleIngreso cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) { }
        try { this.setIdIngreso(rs.getInt("idingreso")); }
        catch(Exception e) { }
        try { this.setIdMovimientoCancela(rs.getInt("idmovimientocancela")); }
        catch(Exception e) { }
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) { }
        try { this.setIdDetalleOperacion(rs.getInt("iddetalleoperacion")); }
        catch(Exception e) { }
        try { this.setMontoCapital(rs.getDouble("montocapital")); }
        catch(Exception e) { }
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) { }
        try { this.setMontoExoneracion(rs.getDouble("montoexoneracion")); }
        catch(Exception e) { }
        try { this.setMontoSaldo(rs.getDouble("montosaldo")); }
        catch(Exception e) { }
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) { }
        if (this.getEstado().equals(estado_atraso) || this.getEstado().equals(estado_cobro) || this.getEstado().equals(estado_saldo)) {
            try { this.setMontoCobro(rs.getDouble("montosaldo")); }
            catch(Exception e) { }
        } else {
            try { this.setMontoCobro(0.0); }
            catch(Exception e) { }
        }
        try { this.setInteresMoratorio(rs.getDouble("interesmoratorio")); }
        catch(Exception e) { }
        try { this.setInteresPunitorio(rs.getDouble("interespunitorio")); }
        catch(Exception e) { }
        try { this.setEnviado(rs.getString("enviado")); }
        catch(Exception e) { }
        try { this.setCuota(rs.getInt("cuota")); }
        catch(Exception e) { }
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) { }
        try { this.setExoneracion(rs.getDouble("exoneracion")); }
        catch(Exception e) { }
        try { this.setOrden(rs.getInt("orden")); }
        catch(Exception e) { }
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) { }
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) { }
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) { }
        try { this.setTasaInteresMoratorio(rs.getDouble("tasainteresmoratorio")); }
        catch(Exception e) { }
        try { this.setTasaInteresPunitorio(rs.getDouble("tasainterespunitorio")); }
        catch(Exception e) { }
        try { this.setAtraso(rs.getBoolean("atraso")); }
        catch(Exception e) { }
        try { this.setCancela(rs.getBoolean("cancela")); }
        catch(Exception e) { }
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) { }
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) { }
        try { this.setPrioridad(rs.getString("prioridad")); }
        catch(Exception e) { }
        try { this.setMontoAtraso(rs.getDouble("montoatraso")); }
        catch(Exception e) { }
        //try { this.setMontoSaldoTotal(rs.getDouble("montosaldototal")); }
        //catch(Exception e) { }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setIdIngreso(int iidIngreso) {
        this.iidIngreso = iidIngreso;
    }

    public void setIdMovimientoCancela(int iidMovimientoCancela) {
        this.iidMovimientoCancela = iidMovimientoCancela;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setIdDetalleOperacion(int iidDetalleOperacion) {
        this.iidDetalleOperacion = iidDetalleOperacion;
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setMontoExoneracion(double umontoExoneracion) {
        this.umontoExoneracion = umontoExoneracion;
    }

    public void setMontoSaldo(double umontoSaldo) {
        this.umontoSaldo = umontoSaldo;
    }

    public void setInteresMoratorio(double uinteresMoratorio) {
        this.uinteresMoratorio = uinteresMoratorio;
    }

    public void setInteresPunitorio(double uinteresPunitorio) {
        this.uinteresPunitorio = uinteresPunitorio;
    }

    public void setEnviado(String senviado) {
        this.senviado = senviado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setExoneracion(double uexoneracion) {
        this.uexoneracion = uexoneracion;
    }

    public void setOrden(int iorden) {
        this.iorden = iorden;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setTasaInteresMoratorio(double utasaInteresMoratorio) {
        this.utasaInteresMoratorio = utasaInteresMoratorio;
    }

    public void setTasaInteresPunitorio(double utasaInteresPunitorio) {
        this.utasaInteresPunitorio = utasaInteresPunitorio;
    }

    public void setAtraso(boolean batraso) {
        this.batraso = batraso;
    }

    public void setCancela(boolean bcancela) {
        this.bcancela = bcancela;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setPrioridad(String sprioridad) {
        this.sprioridad = sprioridad;
    }

    public void setMontoAtraso(double umontoAtraso) {
        this.umontoAtraso = umontoAtraso;
    }

    public void setMontoCobro(double umontoCobro) {
        this.umontoCobro = umontoCobro;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public int getIdIngreso() {
        return this.iidIngreso;
    }

    public int getIdMovimientoCancela() {
        return this.iidMovimientoCancela;
    }

    public String getTabla() {
        return this.stabla;
    }

    public int getIdDetalleOperacion() {
        return this.iidDetalleOperacion;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public double getMontoExoneracion() {
        return this.umontoExoneracion;
    }

    public double getMontoSaldo() {
        return this.umontoSaldo;
    }

    public double getInteresMoratorio() {
        return this.uinteresMoratorio;
    }

    public double getInteresPunitorio() {
        return this.uinteresPunitorio;
    }

    public String getEnviado() {
        return this.senviado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public int getCuota() {
        return this.icuota;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getExoneracion() {
        return this.uexoneracion;
    }

    public int getOrden() {
        return this.iorden;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public double getTasaInteresMoratorio() {
        return this.utasaInteresMoratorio;
    }

    public double getTasaInteresPunitorio() {
        return this.utasaInteresPunitorio;
    }

    public boolean getAtraso() {
        return this.batraso;
    }

    public boolean getCancela() {
        return this.bcancela;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public String getPrioridad() {
        return this.sprioridad;
    }

    public double getMontoAtraso() {
        return this.umontoAtraso;
    }

    public double getMontoCobro() {
        return this.umontoCobro;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getMontoCapital()< 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            //this.smensaje += TEXTO_MONTO_CAPITAL;
            bvalido = false;
        }
        if (this.getMontoInteres()< 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            //this.smensaje += TEXTO_MONTO_INTERES;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentenciaIngreso() {
        return "SELECT detalleingreso("+
            this.getId()+","+
            this.getIdMovimiento()+","+
            this.getIdIngreso()+","+
            this.getIdDetalleOperacion()+","+
            this.getCuota()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaVencimiento())+","+
            this.getMontoCapital()+","+
            this.getMontoInteres()+","+
            this.getMontoSaldo()+","+
            //this.getSaldoCapital()+","+
            //this.getSaldoInteres()+","+
            this.getMontoExoneracion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getEnviado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getEstado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getNumeroOperacion()+","+
            this.getPlazo()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaOperacion())+","+
            this.getTasaInteresMoratorio()+","+
            this.getIdCuenta()+","+
            this.getCancela()+","+
            this.getInteresMoratorio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getInteresPunitorio()+","+
            this.getEstadoRegistro()+")";
    }
    
    public String getSentenciaCancelacion() {
        return "SELECT detallecancelacion("+
            this.getId()+","+
            this.getIdMovimiento()+","+
            this.getIdMovimientoCancela()+","+
            this.getIdDetalleOperacion()+","+
            this.getCuota()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaVencimiento())+","+
            this.getMontoCapital()+","+
            this.getMontoInteres()+","+
            this.getMontoSaldo()+","+
            this.getMontoExoneracion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getEnviado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getEstado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getNumeroOperacion()+","+
            this.getPlazo()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaOperacion())+","+
            this.getTasaInteresMoratorio()+","+
            this.getIdCuenta()+","+
            this.getCancela()+","+
            this.getInteresMoratorio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getInteresPunitorio()+","+
            this.getEstadoRegistro()+")";
    }
    
    public String getActualizarMovimiento(boolean esInteres, boolean revertir) {
        return "SELECT actualizarmovimiento("+this.getIdDetalleOperacion()+","+this.getIdIngreso()+","+this.getMontoCobro()+","+esInteres+","+revertir+")";
    }
    
    public String getConsulta(int iidSocio, int iidMutual, double utasaInteresPunitorio, int iidInteres, String sinteres, int iidEstadoOrdenCredito, int iidEstadoPrestamo, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, String sorden) {
        return "SELECT * FROM (" + // para orden de credito
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,mo.idsocio,mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion,mo.montoaprobado AS monto,mo.plazoaprobado AS plazo,mo.tasainteres AS tasainteresmoratorio,"+utasaInteresPunitorio+" AS tasainterespunitorio,mo.idestado,mo.idcuenta,c.descripcion AS cuenta, c.prioridad, mo.idimpuesto, i.tasa AS tasaImpuesto, 0 AS exoneracion, e.descripcion AS descripcion, (SELECT sum(saldocapital+saldointeres) FROM detalleoperacion WHERE idmovimiento=mo.id AND tabla='mo' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion as dop "+
                       "LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoOrdenCredito+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " + // para capital prestamo
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,mo.idsocio,mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion,mo.montoaprobado AS monto,mo.plazoaprobado AS plazo,mo.tasainteres AS tasainteresmoratorio,"+utasaInteresPunitorio+" AS tasainterespunitorio,mo.idestado,mo.idcuenta,c.descripcion AS cuenta, c.prioridad, mo.idimpuesto, i.tasa AS tasaImpuesto, 0 AS exoneracion, tc.descripcion AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=mo.id AND tabla='mo' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion as dop "+
                       "LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                       "WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND tc.prestamo='TRUE' AND mo.identidad="+iidMutual+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " + // para interes prestamo
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, 0 AS saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, mo.idsocio, mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion, mo.montoaprobado AS monto,mo.plazoaprobado AS plazo, 0 AS tasainteresmoratorio,0 AS tasainterespunitorio, mo.idestado, "+iidInteres+" AS idcuenta, '"+sinteres+"' AS cuenta, c.prioridad, mo.idimpuesto, i.tasa AS tasaImpuesto, tc.exoneracion, tc.descripcion AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=mo.id AND tabla='mo' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion as dop "+
                       "LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                       "WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND tc.prestamo='TRUE' AND mo.identidad="+iidMutual+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " + // para prestamo financiera
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,mo.idsocio,mo.idmoneda,mo.numerooperacion,mo.fechaaprobado AS fechaoperacion,mo.montoaprobado AS monto,mo.plazoaprobado AS plazo,mo.tasainteres AS tasainteresmoratorio,"+utasaInteresPunitorio+" AS tasainterespunitorio,mo.idestado,mo.idcuenta,c.descripcion AS cuenta, c.prioridad, mo.idimpuesto, i.tasa AS tasaImpuesto, 0 AS exoneracion, e.descripcion AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=mo.id AND tabla='mo' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion as dop "+
                       "LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                       "WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND tc.prestamo='TRUE' AND mo.identidad<>"+iidMutual+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " + // para ahorro programado
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,ap.idsocio,ap.idmoneda,ap.numerooperacion,ap.fechaoperacion,ap.importe AS monto,ap.plazo,ap.tasainteres AS tasainteresmoratorio,0 AS tasainterespunitorio,ap.idestado,ap.idcuenta,c.descripcion AS cuenta, c.prioridad, 0 AS idimpuesto, 0 AS tasaImpuesto, 0 AS exoneracion, p.descripcion AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=ap.id AND tabla='ap' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion AS dop "+
                       "LEFT JOIN ahorroprogramado AS ap ON dop.idmovimiento=ap.id "+
                       "LEFT JOIN cuenta AS c ON ap.idcuenta=c.id "+
                       "LEFT JOIN planahorroprogramado AS p ON ap.idplan=p.id "+
                       "WHERE dop.tabla='ap' AND ap.idestado="+iidEstadoAhorro+" AND ap.idsocio="+iidSocio+" "+
                   "UNION " + // para operacion fija
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,of.idsocio,of.idmoneda,of.numerooperacion,of.fechaoperacion,of.importe AS monto,of.plazo,of.tasainteres AS tasainteresmoratorio,0 AS tasainterespunitorio,of.idestado,of.idcuenta,c.descripcion AS cuenta, c.prioridad, 0 AS idimpuesto, 0 AS tasaImpuesto, 0 AS exoneracion, '' AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=of.id AND tabla='of' GROUP BY idmovimiento) as totalsaldo "+ 
                       "FROM detalleoperacion AS dop "+
                       "LEFT JOIN operacionfija AS of ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "WHERE dop.tabla='of' AND of.idestado="+iidEstadoOperacion+" AND of.idsocio="+iidSocio+" "+
                   "UNION " + // para fondo juridico y sepelio
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,js.idsocio,js.idmoneda,js.numerooperacion,js.fechaoperacion,js.importetitular+js.importeadherente*js.cantidadadherente AS monto,0 AS plazo,0 AS tasainteresmoratorio,0 AS tasainterespunitorio,js.idestado,js.idcuenta,c.descripcion AS cuenta, c.prioridad, 0 AS idimpuesto, 0 AS tasaImpuesto, 0 AS exoneracion, '' AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=js.id AND tabla='js' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion AS dop "+
                       "LEFT JOIN fondojuridicosepelio AS js ON dop.idmovimiento=js.id "+
                       "LEFT JOIN cuenta AS c ON js.idcuenta=c.id "+
                       "WHERE dop.tabla='js' AND js.idestado="+iidEstadoFondo+" AND js.idsocio="+iidSocio+" "+
               ") AS detalle WHERE (totalsaldo>0 AND plazo>0) OR (plazo=0) ORDER BY "+sorden;
    }
    
    public String getConsulta(int iidMutual, double utasaInteresPunitorio, int iidInteres, String sinteres, int iidEstadoOrdenCredito, int iidEstadoPrestamo, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, String sorden) {
        return "SELECT * FROM (" + // para orden de credito
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,mo.idsocio,mo.idmoneda,mo.numerooperacion,mo.fechaaprobado AS fechaoperacion,mo.montoaprobado AS monto,mo.plazoaprobado AS plazo,mo.tasainteres AS tasainteresmoratorio,"+utasaInteresPunitorio+" AS tasainterespunitorio,mo.idestado,mo.idcuenta,c.descripcion AS cuenta, c.prioridad, mo.idimpuesto, i.tasa AS tasaImpuesto, 0 AS exoneracion, e.descripcion AS descripcion, (SELECT sum(saldocapital+saldointeres) FROM detalleoperacion WHERE idmovimiento=mo.id AND tabla='mo' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion as dop "+
                       "LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoOrdenCredito+" "+
                   "UNION " + // para capital prestamo
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,mo.idsocio,mo.idmoneda,mo.numerooperacion,mo.fechaaprobado AS fechaoperacion,mo.montoaprobado AS monto,mo.plazoaprobado AS plazo,mo.tasainteres AS tasainteresmoratorio,"+utasaInteresPunitorio+" AS tasainterespunitorio,mo.idestado,mo.idcuenta,c.descripcion AS cuenta, c.prioridad, mo.idimpuesto, i.tasa AS tasaImpuesto, 0 AS exoneracion, tc.descripcion AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=mo.id AND tabla='mo' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion as dop "+
                       "LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                       "WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND tc.prestamo='TRUE' AND mo.identidad="+iidMutual+" "+
                   "UNION " + // para interes prestamo
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, 0 AS saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, mo.idsocio, mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion, mo.montoaprobado AS monto,mo.plazoaprobado AS plazo, 0 AS tasainteresmoratorio,0 AS tasainterespunitorio, mo.idestado, "+iidInteres+" AS idcuenta, '"+sinteres+"' AS cuenta, c.prioridad, mo.idimpuesto, i.tasa AS tasaImpuesto, tc.exoneracion, tc.descripcion AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=mo.id AND tabla='mo' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion as dop "+
                       "LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                       "WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND tc.prestamo='TRUE' AND mo.identidad="+iidMutual+" "+
                   "UNION " + // para prestamo financiera
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,mo.idsocio,mo.idmoneda,mo.numerooperacion,mo.fechaaprobado AS fechaoperacion,mo.montoaprobado AS monto,mo.plazoaprobado AS plazo,mo.tasainteres AS tasainteresmoratorio,"+utasaInteresPunitorio+" AS tasainterespunitorio,mo.idestado,mo.idcuenta,c.descripcion AS cuenta, c.prioridad, mo.idimpuesto, i.tasa AS tasaImpuesto, 0 AS exoneracion, e.descripcion AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=mo.id AND tabla='mo' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion as dop "+
                       "LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                       "WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND tc.prestamo='TRUE' AND mo.identidad<>"+iidMutual+" "+
                   "UNION " + // para ahorro programado
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,ap.idsocio,ap.idmoneda,ap.numerooperacion,ap.fechaoperacion,ap.importe AS monto,ap.plazo,ap.tasainteres AS tasainteresmoratorio,0 AS tasainterespunitorio,ap.idestado,ap.idcuenta,c.descripcion AS cuenta, c.prioridad, 0 AS idimpuesto, 0 AS tasaImpuesto, 0 AS exoneracion, p.descripcion AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=ap.id AND tabla='ap' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion AS dop "+
                       "LEFT JOIN ahorroprogramado AS ap ON dop.idmovimiento=ap.id "+
                       "LEFT JOIN cuenta AS c ON ap.idcuenta=c.id "+
                       "LEFT JOIN planahorroprogramado AS p ON ap.idplan=p.id "+
                       "WHERE dop.tabla='ap' AND ap.idestado="+iidEstadoAhorro+" "+
                   "UNION " + // para operacion fija
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,of.idsocio,of.idmoneda,of.numerooperacion,of.fechaoperacion,of.importe AS monto,of.plazo,of.tasainteres AS tasainteresmoratorio,0 AS tasainterespunitorio,of.idestado,of.idcuenta,c.descripcion AS cuenta, c.prioridad, 0 AS idimpuesto, 0 AS tasaImpuesto, 0 AS exoneracion, '' AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=of.id AND tabla='of' GROUP BY idmovimiento) as totalsaldo "+ 
                       "FROM detalleoperacion AS dop "+
                       "LEFT JOIN operacionfija AS of ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "WHERE dop.tabla='of' AND of.idestado="+iidEstadoOperacion+" "+
                   "UNION " + // para fondo juridico y sepelio
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla,js.idsocio,js.idmoneda,js.numerooperacion,js.fechaoperacion,js.importetitular+js.importeadherente*js.cantidadadherente AS monto,0 AS plazo,0 AS tasainteresmoratorio,0 AS tasainterespunitorio,js.idestado,js.idcuenta,c.descripcion AS cuenta, c.prioridad, 0 AS idimpuesto, 0 AS tasaImpuesto, 0 AS exoneracion, '' AS descripcion, (SELECT SUM(saldocapital) FROM detalleoperacion WHERE idmovimiento=js.id AND tabla='js' GROUP BY idmovimiento) as totalsaldo "+
                       "FROM detalleoperacion AS dop "+
                       "LEFT JOIN fondojuridicosepelio AS js ON dop.idmovimiento=js.id "+
                       "LEFT JOIN cuenta AS c ON js.idcuenta=c.id "+
                       "WHERE dop.tabla='js' AND js.idestado="+iidEstadoFondo+" "+
               ") AS detalle "+
               "LEFT JOIN socio AS s ON detalle.idsocio=s.id "+
               "LEFT JOIN lugarlaboral AS ll ON ll.idsocio=s.id "+
               "WHERE (detalle.totalsaldo>0 AND detalle.plazo>0) OR (detalle.plazo=0) "+
               "ORDER BY "+sorden;
    }
    
}
