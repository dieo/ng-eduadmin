/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbListaPlanCuenta {
    
    protected int iid;
    protected String snumeroCuenta;
    protected String snumeroCuenta1;
    protected String sdescripcionCuenta;
    protected boolean bacepta;
    
    public static final int LONGITUD_DESCRIPCION = 50;

    public static final String TEXTO_DESCRIPCION = "Texto a buscar";

    public static final String TEXTO_NUMERO_CUENTA = "Número de Cuenta";
    public static final String TEXTO_DESCRIPCION_CUENTA = "Descripción de Cuenta";
    public static final String TEXTO_ACEPTA = "Acepta";

    public entbListaPlanCuenta() {
        this.iid = 0;
        this.snumeroCuenta = "";
        this.snumeroCuenta1 = "";
        this.sdescripcionCuenta = "";
        this.bacepta = false;
    }

    public entbListaPlanCuenta(int iid, String snumeroCuenta, String snumeroCuenta1, String sdescripcionCuenta, boolean bacepta) {
        this.iid = iid;
        this.snumeroCuenta = snumeroCuenta;
        this.snumeroCuenta1 = snumeroCuenta1;
        this.sdescripcionCuenta = sdescripcionCuenta;
        this.bacepta = bacepta;
    }

    public void setEntidad(int iid, String snumeroCuenta, String snumeroCuenta1, String sdescripcionCuenta, boolean bacepta) {
        this.iid = iid;
        this.snumeroCuenta = snumeroCuenta;
        this.snumeroCuenta1 = snumeroCuenta1;
        this.sdescripcionCuenta = sdescripcionCuenta;
        this.bacepta = bacepta;
    }

    public entbListaPlanCuenta copiar(entbListaPlanCuenta destino) {
        destino.setEntidad(this.getId(), this.getNumeroCuenta(), this.getNumeroCuenta1(), this.getDescripcionCuenta(), this.getAcepta());
        return destino;
    }
    
    public entbListaPlanCuenta cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroCuenta(rs.getString("nroplancuenta")); }
        catch(Exception e) {}
        try { this.setNumeroCuenta1(rs.getString("nroplancuenta1")); }
        catch(Exception e) {}
        try { this.setDescripcionCuenta(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setAcepta(false); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroCuenta(String snumeroCuenta) {
        this.snumeroCuenta = snumeroCuenta;
    }

    public void setNumeroCuenta1(String snumeroCuenta1) {
        this.snumeroCuenta1 = snumeroCuenta1;
    }

    public void setDescripcionCuenta(String sdescripcionCuenta) {
        this.sdescripcionCuenta = sdescripcionCuenta;
    }

    public void setAcepta(boolean bacepta) {
        this.bacepta = bacepta;
    }
    
    public int getId() {
        return this.iid;
    }

    public String getNumeroCuenta() {
        return this.snumeroCuenta;
    }

    public String getNumeroCuenta1() {
        return this.snumeroCuenta1;
    }
    
    public String getDescripcionCuenta() {
        return this.sdescripcionCuenta;
    }
    
    public boolean getAcepta() {
        return this.bacepta;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número de Cuenta", "nroplancuenta", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Descripción de Cuenta", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
}
