/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entReferenciaComercial {

    protected int iid;
    protected String scomercio;
    protected String sdireccion;
    protected String stelefono;
    protected String sresponsable;
    protected String sresultado;
    protected int iidSocio;
    protected short cestado;
    protected String smensaje;
    
    public static final int LONGITUD_COMERCIO = 80;
    public static final int LONGITUD_DIRECCION = 200;
    public static final int LONGITUD_TELEFONO = 100;
    public static final int LONGITUD_RESPONSABLE = 100;
    public static final int LONGITUD_RESULTADO = 100;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_COMERCIO = "Comercio (hasta "+LONGITUD_COMERCIO+" caracteres)";
    public static final String TEXTO_DIRECCION = "Dirección (hasta "+LONGITUD_DIRECCION+" caracteres)";
    public static final String TEXTO_TELEFONO = "Teléfono (hasta "+LONGITUD_TELEFONO+" caracteres)";
    public static final String TEXTO_RESPONSABLE = "Responsable (hasta " + LONGITUD_RESPONSABLE + " caracteres)";
    public static final String TEXTO_RESULTADO = "Resultado (hasta " + LONGITUD_RESULTADO + " caracteres)";

    public entReferenciaComercial() {
        this.iid = 0;
        this.scomercio = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.sresponsable = "";
        this.sresultado = "";
        this.iidSocio = 0;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entReferenciaComercial(int iid, String scomercio, String sdireccion, String stelefono, String sresponsable, String sresultado, int iidSocio, short cestado) {
        this.iid = iid;
        this.scomercio = scomercio;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.sresponsable = sresponsable;
        this.sresultado = sresultado;
        this.iidSocio = iidSocio;
        this.cestado = cestado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, String scomercio, String sdireccion, String stelefono, String sresponsable, String sresultado, int iidSocio, short cestado) {
        this.iid = iid;
        this.scomercio = scomercio;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.sresponsable = sresponsable;
        this.sresultado = sresultado;
        this.iidSocio = iidSocio;
        this.cestado = cestado;
    }

    public entReferenciaComercial copiar(entReferenciaComercial destino) {
        destino.setEntidad(this.getId(), this.getComercio(), this.getDireccion(), this.getTelefono(), this.getResponsable(), this.getResultado(), this.getIdSocio(), this.getEstadoRegistro());
        return destino;
    }
    
    public entReferenciaComercial cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setComercio(rs.getString("comercio")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setResponsable(rs.getString("responsable")); }
        catch(Exception e) {}
        try { this.setResultado(rs.getString("resultado")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setComercio(String scomercio) {
        this.scomercio = scomercio;
    }

    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setResponsable(String sresponsable) {
        this.sresponsable = sresponsable;
    }

    public void setResultado(String sresultado) {
        this.sresultado = sresultado;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }

    public int getId() {
        return this.iid;
    }

    public String getComercio() {
        return this.scomercio;
    }

    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefono() {
        return this.stelefono;
    }

    public String getResponsable() {
        return this.sresponsable;
    }

    public String getResultado() {
        return this.sresultado;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getComercio().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_COMERCIO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT referenciacomercial("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getResponsable())+","+
            utilitario.utiCadena.getTextoGuardado(this.getResultado())+","+
            this.getIdSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getComercio())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getEstadoRegistro()+")";
    }

}
