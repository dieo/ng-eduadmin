/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entGenericaPersona;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entSubordinado extends entGenericaPersona {

    protected java.util.Date dfechaNacimiento;
    protected int iidParentesco;
    protected String sparentesco;
    protected int iidSocio;
    
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final int LONGITUD_CEDULA = 12;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacío, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_CEDULA = "Apellido (no vacío, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_FECHA_NACIMIENTO = "Fecha de Nacimiento (no vacía)";
    public static final String TEXTO_PARENTESCO = "Parentesco (no vacío)";

    public entSubordinado() {
        super();
        this.dfechaNacimiento = null;
        this.iidParentesco = 0;
        this.iidSocio = 0;
    }

    public entSubordinado(int iid, String snombre, String sapellido, String scedula, java.util.Date dfechaNacimiento, int iidParentesco, String sparentesco, int iidSocio, short hestadoRegistro) {
        super(iid, snombre, sapellido, scedula, hestadoRegistro);
        this.dfechaNacimiento = dfechaNacimiento;
        this.iidParentesco = iidParentesco;
        this.sparentesco = sparentesco;
        this.iidSocio = iidSocio;
    }
    
    public void setEntidad(int iid, String snombre, String sapellido, String scedula, java.util.Date dfechaNacimiento, int iidParentesco, String sparentesco, int iidSocio, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre.trim().toUpperCase();
        this.sapellido = sapellido.trim().toUpperCase();
        this.scedula = scedula;
        this.dfechaNacimiento = dfechaNacimiento;
        this.iidParentesco = iidParentesco;
        this.sparentesco = sparentesco;
        this.iidSocio = iidSocio;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entSubordinado copiar(entSubordinado destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getFechaNacimiento(), this.getIdParentesco(), this.getParentesco(), this.getIdSocio(), this.getEstadoRegistro());
        return destino;
    }
    
    public entSubordinado cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setFechaNacimiento(rs.getDate("fechanacimiento")); }
        catch(Exception e) {}
        try { this.setIdParentesco(rs.getInt("idparentesco")); }
        catch(Exception e) {}
        try { this.setParentesco(rs.getString("parentesco")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdParentesco(int iidParentesco) {
        this.iidParentesco = iidParentesco;
    }

    public void setParentesco(String sparentesco) {
        this.sparentesco = sparentesco;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setFechaNacimiento(java.util.Date dfechaNacimiento) {
        this.dfechaNacimiento = dfechaNacimiento;
    }

    public int getIdParentesco() {
        return this.iidParentesco;
    }

    public String getParentesco() {
        return this.sparentesco;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public java.util.Date getFechaNacimiento() {
        return this.dfechaNacimiento;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_APELLIDO;
            bvalido = false;
        }
        if (this.getIdParentesco() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PARENTESCO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT subordinado("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaNacimiento())+","+
            this.getIdParentesco()+","+
            this.getIdSocio()+","+
            this.getEstadoRegistro()+")";
    }

}