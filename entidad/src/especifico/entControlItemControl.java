/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entControlItemControl extends generico.entGenerica{
    
    private int iidFormulario;
    private boolean bactivo;
    private boolean btitulo;

    public static final int LONGITUD_DESCRIPCION = 200;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Item de Control (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_ACTIVO = "Activo";
    public static final String TEXTO_TITULO = "Título";

    public entControlItemControl() {
        super();
        this.iidFormulario = 0;
        this.bactivo = false;
        this.btitulo = false;
    }

    public entControlItemControl(int iid, String sdescripcion, int iidFormulario, boolean bactivo, boolean btitulo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidFormulario = iidFormulario;
        this.bactivo = bactivo;
        this.btitulo = btitulo;
    }
    
    public void setEntidad(int iid, String sdescripcion, int iidFormulario, boolean bactivo, boolean btitulo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidFormulario = iidFormulario;
        this.bactivo = bactivo;
        this.btitulo = btitulo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entControlItemControl copiar(entControlItemControl destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdFormulario(), this.getActivo(), this.getTitulo(), this.getEstadoRegistro());
        return destino;
    }

    public entControlItemControl cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdFormulario(rs.getInt("idformulario")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setTitulo(rs.getBoolean("titulo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdFormulario(int iidFormulario) {
        this.iidFormulario = iidFormulario;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setTitulo(boolean btitulo) {
        this.btitulo = btitulo;
    }

    public int getIdFormulario() {
        return this.iidFormulario;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public boolean getTitulo() {
        return this.btitulo;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT itemcontrol("+
            this.getId()+","+
            this.getIdFormulario()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getActivo()+","+
            this.getTitulo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
}
