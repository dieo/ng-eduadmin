/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entbCuentaTalonario {

    private int iid;
    private int iidCuentaBanco;
    private String scuentaBanco;
    private int iidTalonario;
    private String stalonario;
    private boolean bactivo;
    private short hestadoRegistro;
    private String smensaje;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CUENTA_BANCO = "Cuenta Banco (no vacío)";
    public static final String TEXTO_TALONARIO = "Talonario (no vacío)";

    public entbCuentaTalonario() {
        this.iid = 0;
        this.iidCuentaBanco = 0;
        this.scuentaBanco = "";
        this.iidTalonario = 0;
        this.stalonario = "";
        this.bactivo = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entbCuentaTalonario(int iid, int iidCuentaBanco, String scuentaBanco, int iidTalonario, String stalonario, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.iidCuentaBanco = iidCuentaBanco;
        this.scuentaBanco = scuentaBanco;
        this.iidTalonario = iidTalonario;
        this.stalonario = stalonario;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidCuentaBanco, String scuentaBanco, int iidTalonario, String stalonario, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.iidCuentaBanco = iidCuentaBanco;
        this.scuentaBanco = scuentaBanco;
        this.iidTalonario = iidTalonario;
        this.stalonario = stalonario;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public entbCuentaTalonario copiar(entbCuentaTalonario destino) {
        destino.setEntidad(this.getId(), this.getIdCuentaBanco(), this.getCuentaBanco(), this.getIdTalonario(), this.getTalonario(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }

    public entbCuentaTalonario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdCuentaBanco(rs.getInt("idbancocuenta")); }
        catch(Exception e) {}
        try { this.setCuentaBanco(rs.getString("bancocuenta")); }
        catch(Exception e) {}
        try { this.setIdTalonario(rs.getInt("idtalonario")); }
        catch(Exception e) {}
        try { this.setTalonario(rs.getString("talonario")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdCuentaBanco(int iidCuentaBanco) {
        this.iidCuentaBanco = iidCuentaBanco;
    }

    public void setCuentaBanco(String scuentaBanco) {
        this.scuentaBanco = scuentaBanco;
    }

    public void setIdTalonario(int iidTalonario) {
        this.iidTalonario = iidTalonario;
    }

    public void setTalonario(String stalonario) {
        this.stalonario = stalonario;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdCuentaBanco() {
        return this.iidCuentaBanco;
    }

    public String getCuentaBanco() {
        return this.scuentaBanco;
    }

    public int getIdTalonario() {
        return this.iidTalonario;
    }

    public String getTalonario() {
        return this.stalonario;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido(boolean esUnicoTalonario, boolean esUnicoActivo) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdCuentaBanco()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CUENTA_BANCO;
            bvalido = false;
        }
        if (this.getIdTalonario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TALONARIO;
            bvalido = false;
        }
        if (!esUnicoTalonario) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "El talonario ya está asignado";
            bvalido = false;
        }
        /*if (!esUnicoActivo) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Ya existe un talonario activo";
            bvalido = false;
        }*/
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.cuentatalonario("+
            this.getId()+","+
            this.getIdCuentaBanco()+","+
            this.getIdTalonario()+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

}
