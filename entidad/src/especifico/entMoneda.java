/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entGenerica;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMoneda extends entGenerica{

    protected String ssimbolo;
    protected double utipoCambio;
    protected int iidPais;
    protected String spais;
    protected boolean bactivo;

    public static final int LONGITUD_DESCRIPCION = 40;
    public static final int LONGITUD_SIMBOLO = 5;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Moneda (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_SIMBOLO = "Símbolo de Moneda (no vacío, hasta " + LONGITUD_SIMBOLO + " caracteres)";
    public static final String TEXTO_TIPO_CAMBIO = "Tipo de Cambio (valor positivo)";
    public static final String TEXTO_PAIS = "País (no vacío)";
    public static final String TEXTO_ACTIVO = "Activo";

    public entMoneda() {
        super();
        this.ssimbolo = "";
        this.utipoCambio = 0;
        this.iidPais = 0;
        this.spais = "";
        this.bactivo = false;
    }

    public entMoneda(int iid, String sdescripcion, String ssimbolo, double utipoCambio, int iidPais, String spais, boolean bactivo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.ssimbolo = ssimbolo;
        this.utipoCambio = utipoCambio;
        this.iidPais = iidPais;
        this.spais = spais;
        this.bactivo = bactivo;
    }

    public void setEntidad(int iid, String sdescripcion, String ssimbolo, double utipoCambio, int iidPais, String spais, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.ssimbolo = ssimbolo;
        this.utipoCambio = utipoCambio;
        this.iidPais = iidPais;
        this.spais = spais;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entMoneda copiar(entMoneda destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getSimbolo(), this.getTipoCambio(), this.getIdPais(), this.getPais(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }

    public entMoneda cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setSimbolo(rs.getString("simbolo")); }
        catch(Exception e) {}
        try { this.setTipoCambio(rs.getDouble("tipocambio")); }
        catch(Exception e) {}
        try { this.setIdPais(rs.getInt("idpais")); }
        catch(Exception e) {}
        try { this.setPais(rs.getString("pais")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setSimbolo(String ssimbolo) {
        this.ssimbolo = ssimbolo;
    }

    public void setTipoCambio(double utipoCambio) {
        this.utipoCambio = utipoCambio;
    }

    public void setIdPais(int iidPais) {
        this.iidPais = iidPais;
    }

    public void setPais(String spais) {
        this.spais = spais;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public String getSimbolo() {
        return this.ssimbolo;
    }

    public double getTipoCambio() {
        return this.utipoCambio;
    }

    public int getIdPais() {
        return this.iidPais;
    }

    public String getPais() {
        return this.spais;
    }

    public boolean getActivo() {
        return this.bactivo;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getSimbolo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SIMBOLO;
            bvalido = false;
        }
        if (this.getTipoCambio()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_CAMBIO;
            bvalido = false;
        }
        if (this.getIdPais()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PAIS;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT moneda("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getSimbolo())+","+
            this.getTipoCambio()+","+
            this.getIdPais()+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Símbolo", "simbolo", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Tipo Cambio", "tipocambio", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "País", "pais", generico.entLista.tipo_texto));
        return lst;
    }

}
