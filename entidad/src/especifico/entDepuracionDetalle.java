/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDepuracionDetalle {

    private int iid;
    private int inumeroCuota;
    private java.util.Date dfechaVencimiento;
    private double umontoCapital;
    private double umontoInteres;
    private double usaldoCapital;
    private double usaldoInteres;
    private short hestadoRegistro;
    
    public entDepuracionDetalle() {
        this.iid = 0;
        this.inumeroCuota = 0;
        this.dfechaVencimiento = null;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.usaldoCapital = 0.0;
        this.usaldoInteres = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDepuracionDetalle(int iid, int inumeroCuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double usaldoCapital, double usaldoInteres, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroCuota = inumeroCuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.usaldoCapital = usaldoCapital;
        this.usaldoInteres = usaldoInteres;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroCuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double usaldoCapital, double usaldoInteres, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroCuota = inumeroCuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.usaldoCapital = usaldoCapital;
        this.usaldoInteres = usaldoInteres;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDepuracionDetalle copiar(entDepuracionDetalle destino) {
        destino.setEntidad(this.getId(), this.getNumeroCuota(), this.getFechaVencimiento(), this.getMontoCapital(), this.getMontoInteres(), this.getSaldoCapital(), this.getSaldoInteres(), this.getEstadoRegistro());
        return destino;
    }

    public entDepuracionDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setNumeroCuota(rs.getInt("numerocuota")); }
        catch(Exception e) { }
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) { }
        try { this.setMontoCapital(rs.getDouble("montocapital")); }
        catch(Exception e) { }
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) { }
        try { this.setSaldoCapital(rs.getDouble("saldocapital")); }
        catch(Exception e) { }
        try { this.setSaldoInteres(rs.getDouble("saldointeres")); }
        catch(Exception e) { }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroCuota(int inumeroCuota) {
        this.inumeroCuota = inumeroCuota;
    }
    
    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setSaldoCapital(double usaldoCapital) {
        this.usaldoCapital = usaldoCapital;
    }

    public void setSaldoInteres(double usaldoInteres) {
        this.usaldoInteres = usaldoInteres;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getNumeroCuota() {
        return this.inumeroCuota;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public double getSaldoCapital() {
        return this.usaldoCapital;
    }

    public double getSaldoInteres() {
        return this.usaldoInteres;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getSentencia() {
        return "";
    }
    
    public String getConsulta(int iidMovimiento, String stabla) {
        return "SELECT det.id, det.numerocuota, det.fechavencimiento, det.montocapital, det.montointeres, det.saldocapital, det.saldointeres " +
            "FROM detalleoperacion AS det " +
            "WHERE det.idmovimiento="+iidMovimiento+" AND det.tabla='"+stabla+"' " +
            "ORDER BY det.fechavencimiento";
    }

}
