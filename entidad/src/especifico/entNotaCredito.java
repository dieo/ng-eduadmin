/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entNotaCredito {
    
    private int iid;
    private java.util.Date dfechaNota;
    private int iidPersona;
    private String sruc;
    private String srazonSocial;
    private String sdireccion;
    private String stelefono;
    private int iidUsuario;
    private String susuario;
    private int iidTalonario;
    private String stalonario;
    private String slocal;
    private String spuntoExpedicion;
    private int inumeroNota;
    private int iidEstadoNota;
    private String sestadoNota;
    private double utotal;
    private int iidMoneda;
    private String smoneda;
    private int iidMotivo;
    private String smotivo;
    private boolean bsocio;
    
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    private String sreferencia;
    private double uimpuesto;
    private int iidOperacion;
    private String stabla;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_RUC = 12;
    public static final int LONGITUD_RAZON_SOCIAL = 80;
    public static final int LONGITUD_DIRECCION = 100;
    public static final int LONGITUD_TELEFONO = 40;
    public static final int LONGITUD_LOCAL = 3;
    public static final int LONGITUD_PUNTO_EXPEDICION = 3;
    public static final int LONGITUD_OBSERVACION_ANULADO = 100;
    public static final int LONGITUD_REFERENCIA = 200;
    public static final int CANTIDAD_LINEA_NOTA_CREDITO = 8;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FECHA_NOTA = "Fecha de Nota de Crédito (no vacía)";
    public static final String TEXTO_RUC = "RUC (no vacío)";
    public static final String TEXTO_RAZON_SOCIAL = "Razón Social (no vacía)";
    public static final String TEXTO_DIRECCION = "Dirección (no vacía)";
    public static final String TEXTO_TELEFONO = "Teléfono (no vacía)";
    public static final String TEXTO_USUARIO = "Usuario (no editable)";
    public static final String TEXTO_TALONARIO = "Talonario (no vacío)";
    public static final String TEXTO_LOCAL = "Local (no vacío, hasta "+LONGITUD_LOCAL+" caracteres)";
    public static final String TEXTO_PUNTO_EXPEDICION = "Punto de Expedición (no vacío, hasta "+LONGITUD_PUNTO_EXPEDICION+" caracteres)";
    public static final String TEXTO_NUMERO_NOTA = "Número de Nota de Crédito (no vacío, no editable)";
    public static final String TEXTO_ESTADO_NOTA = "Estado de Nota de Crédito (no editable)";
    public static final String TEXTO_TOTAL = "Importe Total (no editable, valor positivo)";
    public static final String TEXTO_TOTAL_A_ACREDITAR = "Importe Total a Acreditar (no editable, valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_SOCIO = "Socio";
    public static final String TEXTO_MOTIVO = "Motivo (no vacío)";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación de anulación (no vacía, hasta " + LONGITUD_OBSERVACION_ANULADO + " caracteres)";
    public static final String TEXTO_REFERENCIA = "Referencia (hasta " + LONGITUD_REFERENCIA + " caracteres)";
    public static final String TEXTO_IMPUESTO = "Impuesto (valor positivo)";
    
    public entNotaCredito() {
        this.iid = 0;
        this.dfechaNota = null;
        this.iidPersona = 0;
        this.sruc = "";
        this.srazonSocial = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.iidUsuario = 0;
        this.susuario = "";
        this.iidTalonario = 0;
        this.stalonario = "";
        this.slocal = "";
        this.spuntoExpedicion = "";
        this.inumeroNota = 0;
        this.iidEstadoNota = 0;
        this.sestadoNota = "";
        this.utotal = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iidMotivo = 0;
        this.smotivo = "";
        this.bsocio = false;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.sreferencia = "";
        this.uimpuesto = 0.0;
        this.iidOperacion = 0;
        this.stabla = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entNotaCredito(int iid, java.util.Date dfechaNota, int iidPersona, String sruc, String srazonSocial, String sdireccion, String stelefono, int iidUsuario, String susuario, int iidTalonario, String stalonario, String slocal, String spuntoExpedicion, int inumeroNota, int iidEstadoNota, String sestadoNota, double utotal, int iidMoneda, String smoneda, int iidMotivo, String smotivo, boolean bsocio, java.util.Date dfechaAnulado, String sobservacionAnulado, String sreferencia, double uimpuesto, int iidOperacion, String stabla, short hestadoRegistro) {
        this.iid = iid;
        this.dfechaNota = dfechaNota;
        this.iidPersona = iidPersona;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidTalonario = iidTalonario;
        this.stalonario = stalonario;
        this.slocal = slocal;
        this.spuntoExpedicion = spuntoExpedicion;
        this.inumeroNota = inumeroNota;
        this.iidEstadoNota = iidEstadoNota;
        this.sestadoNota = sestadoNota;
        this.utotal = utotal;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidMotivo = iidMotivo;
        this.smotivo = smotivo;
        this.bsocio = bsocio;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.sreferencia = sreferencia;
        this.uimpuesto = uimpuesto;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, java.util.Date dfechaNota, int iidPersona, String sruc, String srazonSocial, String sdireccion, String stelefono, int iidUsuario, String susuario, int iidTalonario, String stalonario, String slocal, String spuntoExpedicion, int inumeroNota, int iidEstadoNota, String sestadoNota, double utotal, int iidMoneda, String smoneda, int iidMotivo, String smotivo, boolean bsocio, java.util.Date dfechaAnulado, String sobservacionAnulado, String sreferencia, double uimpuesto, int iidOperacion, String stabla, short hestadoRegistro) {
        this.iid = iid;
        this.dfechaNota = dfechaNota;
        this.iidPersona = iidPersona;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidTalonario = iidTalonario;
        this.stalonario = stalonario;
        this.slocal = slocal;
        this.spuntoExpedicion = spuntoExpedicion;
        this.inumeroNota = inumeroNota;
        this.iidEstadoNota = iidEstadoNota;
        this.sestadoNota = sestadoNota;
        this.utotal = utotal;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidMotivo = iidMotivo;
        this.smotivo = smotivo;
        this.bsocio = bsocio;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.sreferencia = sreferencia;
        this.uimpuesto = uimpuesto;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entNotaCredito copiar(entNotaCredito destino) {
        destino.setEntidad(this.getId(), this.getFechaNota(), this.getIdPersona(), this.getRuc(), this.getRazonSocial(), this.getDireccion(), this.getTelefono(), this.getIdUsuario(), this.getUsuario(), this.getIdTalonario(), this.getTalonario(), this.getLocal(), this.getPuntoExpedicion(), this.getNumeroNota(), this.getIdEstadoNota(), this.getEstadoNota(), this.getTotal(), this.getIdMoneda(), this.getMoneda(), this.getIdMotivo(), this.getMotivo(), this.getSocio(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getReferencia(), this.getImpuesto(), this.getIdOperacion(), this.getTabla(), this.getEstadoRegistro());
        return destino;
    }
    
    public entNotaCredito cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setFechaNota(rs.getDate("fechanota")); }
        catch(Exception e) { }
        try { this.setSocio(rs.getBoolean("socio")); }
        catch(Exception e) { }
        try { this.setIdPersona(rs.getInt("idpersona")); }
        catch(Exception e) { }
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) { }
        try { this.setRazonSocial(rs.getString("razonsocial")); }
        catch(Exception e) { }
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) { }
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) { }
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) { }
        try { this.setUsuario(rs.getString("usuario")); }
        catch(Exception e) { }
        try { this.setIdTalonario(rs.getInt("idtalonario")); }
        catch(Exception e) { }
        try { this.setTalonario(rs.getString("talonario")); }
        catch(Exception e) { }
        try { this.setLocal(rs.getString("local")); }
        catch(Exception e) { }
        try { this.setPuntoExpedicion(rs.getString("puntoexpedicion")); }
        catch(Exception e) { }
        try { this.setNumeroNota(rs.getInt("numeronota")); }
        catch(Exception e) { }
        try { this.setIdEstadoNota(rs.getInt("idestado")); }
        catch(Exception e) { }
        try { this.setEstadoNota(rs.getString("estadonota")); }
        catch(Exception e) { }
        try { this.setTotal(rs.getDouble("total")); }
        catch(Exception e) { }
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) { }
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) { }
        try { this.setIdMotivo(rs.getInt("idmotivo")); }
        catch(Exception e) { }
        try { this.setMotivo(rs.getString("motivo")); }
        catch(Exception e) { }
        try { this.setSocio(rs.getBoolean("socio")); }
        catch(Exception e) { }
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) { }
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) { }
        try { this.setReferencia(rs.getString("referencia")); }
        catch(Exception e) { }
        try { this.setImpuesto(rs.getDouble("impuesto")); }
        catch(Exception e) { }
        try { this.setIdOperacion(rs.getInt("idoperacion")); }
        catch(Exception e) { }
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setFechaNota(java.util.Date dfechaNota) {
        this.dfechaNota = dfechaNota;
    }

    public void setIdPersona(int iidPersona) {
        this.iidPersona = iidPersona;
    }
    
    public void setRuc(String sruc) {
        this.sruc = sruc;
    }
    
    public void setRazonSocial(String srazonSocial) {
        this.srazonSocial = srazonSocial;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }
    
    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }
    
    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }
    
    public void setUsuario(String susuario) {
        this.susuario = susuario;
    }
    
    public void setIdTalonario(int iidTalonario) {
        this.iidTalonario = iidTalonario;
    }
    
    public void setTalonario(String stalonario) {
        this.stalonario = stalonario;
    }
    
    public void setLocal(String slocal) {
        this.slocal = slocal;
    }
    
    public void setPuntoExpedicion(String spuntoExpedicion) {
        this.spuntoExpedicion = spuntoExpedicion;
    }
    
    public void setNumeroNota(int inumeroNota) {
        this.inumeroNota = inumeroNota;
    }

    public void setIdEstadoNota(int iidEstadoNota) {
        this.iidEstadoNota = iidEstadoNota;
    }
    
    public void setEstadoNota(String sestadoNota) {
        this.sestadoNota = sestadoNota;
    }
    
    public void setTotal(double utotal) {
        this.utotal = utotal;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setIdMotivo(int iidMotivo) {
        this.iidMotivo = iidMotivo;
    }
    
    public void setMotivo(String smotivo) {
        this.smotivo = smotivo;
    }
    
    public void setSocio(boolean bsocio) {
        this.bsocio = bsocio;
    }
    
    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setReferencia(String sreferencia) {
        this.sreferencia = sreferencia;
    }

    public void setImpuesto(double uimpuesto) {
        this.uimpuesto = uimpuesto;
    }

    public void setIdOperacion(int iidOperacion) {
        this.iidOperacion = iidOperacion;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public java.util.Date getFechaNota() {
        return this.dfechaNota;
    }

    public int getIdPersona() {
        return this.iidPersona;
    }
    
    public String getRuc() {
        return this.sruc;
    }
    
    public String getRazonSocial() {
        return this.srazonSocial;
    }
    
    public String getDireccion() {
        return this.sdireccion;
    }
    
    public String getTelefono() {
        return this.stelefono;
    }
    
    public int getIdUsuario() {
        return this.iidUsuario;
    }
    
    public String getUsuario() {
        return this.susuario;
    }
    
    public int getIdTalonario() {
        return this.iidTalonario;
    }
    
    public String getTalonario() {
        return this.stalonario;
    }
    
    public String getLocal() {
        return this.slocal;
    }
    
    public String getPuntoExpedicion() {
        return this.spuntoExpedicion;
    }
    
    public int getNumeroNota() {
        return this.inumeroNota;
    }

    public int getIdEstadoNota() {
        return this.iidEstadoNota;
    }

    public String getEstadoNota() {
        return this.sestadoNota;
    }
    
    public double getTotal() {
        return this.utotal;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }
    
    public String getMoneda() {
        return this.smoneda;
    }

    public int getIdMotivo() {
        return this.iidMotivo;
    }
    
    public String getMotivo() {
        return this.smotivo;
    }
    
    public boolean getSocio() {
        return this.bsocio;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public String getReferencia() {
        return this.sreferencia;
    }

    public double getImpuesto() {
        return this.uimpuesto;
    }

    public int getIdOperacion() {
        return this.iidOperacion;
    }

    public String getTabla() {
        return this.stabla;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaNota()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_NOTA;
            bvalido = false;
        }
        if (this.getRuc().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RUC;
            bvalido = false;
        }
        if (this.getRazonSocial().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RAZON_SOCIAL;
            bvalido = false;
        }
        if (this.getDireccion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DIRECCION;
            bvalido = false;
        }
        if (this.getTelefono().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO;
            bvalido = false;
        }
        if (this.getIdUsuario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_USUARIO;
            bvalido = false;
        }
        if (this.getIdTalonario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TALONARIO;
            bvalido = false;
        }
        if (this.getLocal().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_LOCAL;
            bvalido = false;
        }
        if (this.getPuntoExpedicion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PUNTO_EXPEDICION;
            bvalido = false;
        }
        if (this.getNumeroNota()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_NOTA;
            bvalido = false;
        }
        if (this.getIdEstadoNota()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ESTADO_NOTA;
            bvalido = false;
        }
        if (this.getTotal()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TOTAL;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getIdMotivo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MOTIVO;
            bvalido = false;
        }
        if (this.getImpuesto()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPUESTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaAnulado()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_ANULADO;
            bvalido = false;
        }
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT notacredito("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaNota())+","+
            this.getIdPersona()+","+
            this.getIdUsuario()+","+
            this.getIdTalonario()+","+
            utilitario.utiCadena.getTextoGuardado(this.getLocal())+","+
            utilitario.utiCadena.getTextoGuardado(this.getPuntoExpedicion())+","+
            this.getNumeroNota()+","+
            this.getIdEstadoNota()+","+
            this.getTotal()+","+
            this.getIdMoneda()+","+
            this.getIdMotivo()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            this.getSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getRuc())+","+
            utilitario.utiCadena.getTextoGuardado(this.getRazonSocial())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            utilitario.utiCadena.getTextoGuardado(this.getReferencia())+","+
            this.getImpuesto()+","+
            this.getIdOperacion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha Nota de Crédito", "fechanota", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(2, "Razón Social", "razonsocial", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Funcionario", "funcionario", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Número Nota de Crédito", "numeronota", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(5, "Estado Nota de Crédito", "estadonota", generico.entLista.tipo_texto));
        return lst;
    }
    
}
