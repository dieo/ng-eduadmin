/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbDepositoCuenta {

    private int iid;
    private int inumerodeposito;
    private int iidBancoCuenta;
    private int iidBanco;
    private String snumerocuenta;
    private String sbancocuenta;
    private String snroPlanBancoCuenta;
    private String sbanco;
    private java.util.Date dfecha;
    private int iidEstado;
    private String sestado;
    private int iidCentroCosto;
    private String scentroCosto;
    private int iidPlanCuenta;
    private String snroPlanCuenta;
    private double dmontoEfectivo;
    private double dmontoChequeOtro;
    private double dmontoChequePropio;
    private String snota;
    private int inumeroBoleta;
    private int iidBancoSucursal;
    private String sbancoSucursal;
    private String srecibido;
    private boolean basiento;
    private java.util.Date dfechaIngreso;
    private int iidFilial;
    private String sfilial;
    private short hestadoRegistro;
    private String smensaje;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    public static final int LONGITUD_NOTA = 100;
    public static final int LONGITUD_RECIBIDO = 50;
    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMERO_DEPOSITO = "Nro. de Depósito (no editable)";
    public static final String TEXTO_ESTADO = "Estado de Depósito (no editable)";
    public static final String TEXTO_BANCO_CUENTA = "Cuenta Bancaria sobre la que se opera (no vacía)";
    public static final String TEXTO_FECHA = "Fecha en que se realizó el depósito (no vacía)";
    public static final String TEXTO_FECHA_DEPOSITO = "Fecha de carga del depósito (no vacía)";
    public static final String TEXTO_CENTRO_COSTO = "Centro de Costo (no vacío)";
    public static final String TEXTO_PLANCUENTA = "Plan de cuenta (no vacía)";
    public static final String TEXTO_MONTO_EFECTIVO = "Monto en efectivo de la operación (valor positivo)";
    public static final String TEXTO_MONTO_CHEQUE_OTRO = "Monto total de los cheques de otros bancos depositados (valor positivo)";
    public static final String TEXTO_MONTO_CHEQUE_PROPIO = "Monto total de los cheques del mismo banco depositados (valor positivo)";
    public static final String TEXTO_NOTA = "Descripción de depósito";
    public static final String TEXTO_NUMERO_BOLETA = "Número de la Boleta de depósito (no vacía)";
    public static final String TEXTO_BANCO_SUCURSAL = "Sucursal en donde se realizó el depósito (no vacío)";
    public static final String TEXTO_RECIBIDO = "Anotación de Número de Caja/Cajero que recibió el depósito (no vacío)";
    public static final String TEXTO_ASIENTO = "Si fue asentado o no el depósito";
    public static final String TEXTO_FILIAL = "Filial (no vacía)";
    
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    
    
    public entbDepositoCuenta() {
        this.iid = 0;
        this.inumerodeposito = 0;
        this.iidBancoCuenta = 0;
        this.iidBanco = 0;
        this.snumerocuenta = "";
        this.sbancocuenta = "";
        this.snroPlanBancoCuenta = "";
        this.sbanco = "";
        this.dfecha = null;
        this.iidEstado = 0;
        this.sestado = "";
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.iidPlanCuenta = 0;
        this.snroPlanCuenta = "";
        this.dmontoEfectivo = 0.0;
        this.dmontoChequeOtro = 0.0;
        this.dmontoChequePropio = 0.0;
        this.snota = "";
        this.inumeroBoleta = 0;
        this.iidBancoSucursal = 0;
        this.sbancoSucursal = "";
        this.srecibido = "";
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.basiento = false;
        this.dfechaIngreso = null;
        this.iidFilial = 0;
        this.sfilial = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbDepositoCuenta(int iid, int inumerodeposito, int iidBancoCuenta, int iidBanco, String snumerocuenta, String sbancocuenta, String snroPlanBancoCuenta, String sbanco, java.util.Date dfecha, int iidEstado, String sestado, int iidCentroCosto,  String scentroCosto, int iidPlanCuenta, String snroPlanCuenta, double dmontoEfectivo, double dmontoChequeOtro, double dmontoChequePropio, String snota, int inumeroBoleta, int iidBancoSucursal, String sbancoSucursal, String srecibido, java.util.Date dfechaAnulado, String sobservacionAnulado, boolean basiento, java.util.Date dfechaDeposito, int iidFilial, String sfilial, short hestadoRegistro) {
        this.iid = iid;
        this.inumerodeposito = inumerodeposito;
        this.iidBancoCuenta = iidBancoCuenta;
        this.iidBanco = iidBanco;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.snroPlanBancoCuenta = snroPlanBancoCuenta;
        this.sbanco = sbanco;
        this.dfecha = dfecha;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.dmontoEfectivo = dmontoEfectivo;
        this.dmontoChequeOtro = dmontoChequeOtro;
        this.dmontoChequePropio = dmontoChequePropio;
        this.snota = snota;
        this.inumeroBoleta = inumeroBoleta;
        this.iidBancoSucursal = iidBancoSucursal;
        this.sbancoSucursal = sbancoSucursal;
        this.srecibido = srecibido;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.basiento = basiento;
        this.dfechaIngreso = dfechaDeposito;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumerodeposito, int iidBancoCuenta, int iidBanco, String snumerocuenta, String sbancocuenta, String snroPlanBancoCuenta, String sbanco, java.util.Date dfecha, int iidEstado, String sestado, int iidCentroCosto,  String scentroCosto, int iidPlanCuenta, String snroPlanCuenta, double dmontoEfectivo, double dmontoChequeOtro, double dmontoChequePropio, String snota, int inumeroBoleta, int iidBancoSucursal, String sbancoSucursal, String srecibido, java.util.Date dfechaAnulado, String sobservacionAnulado, boolean basiento, java.util.Date dfechaDeposito, int iidFilial, String sfilial, short hestadoRegistro) {
        this.iid = iid;
        this.inumerodeposito = inumerodeposito;
        this.iidBancoCuenta = iidBancoCuenta;
        this.iidBanco = iidBanco;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.snroPlanBancoCuenta = snroPlanBancoCuenta;
        this.sbanco = sbanco;
        this.dfecha = dfecha;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.dmontoEfectivo = dmontoEfectivo;
        this.dmontoChequeOtro = dmontoChequeOtro;
        this.dmontoChequePropio = dmontoChequePropio;
        this.snota = snota;
        this.inumeroBoleta = inumeroBoleta;
        this.iidBancoSucursal = iidBancoSucursal;
        this.sbancoSucursal = sbancoSucursal;
        this.srecibido = srecibido;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.basiento = basiento;
        this.dfechaIngreso = dfechaDeposito;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entbDepositoCuenta copiar(entbDepositoCuenta destino) {
        destino.setEntidad(this.getId(), this.getNumeroDeposito(), this.getIdBancoCuenta(), this.getIdBanco(), this.getNumeroCuenta(), this.getBancoCuenta(), this.getNroPlanBancoCuenta(), this.getBanco(), this.getFecha(), this.getIdEstado(), this.getEstado(), this.getIdCentroCosto(), this.getCentroCosto(), this.getIdPlanCuenta(), this.getNroPlanCuenta(), this.getMontoEfectivo(), this.getMontoChequeOtro(), this.getMontoChequePropio(),  this.getNota(), this.getNumeroBoleta(), this.getIdBancoSucursal(), this.getBancoSucursal(), this.getRecibido(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getAsiento(), this.getFechaIngreso(), this.getIdFilial(), this.getFilial(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbDepositoCuenta cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroDeposito(rs.getInt("nrodeposito")); }
        catch(Exception e) {}
        try { this.setIdBancoCuenta(rs.getInt("idbancocuenta")); }
        catch(Exception e) {}
        try { this.setIdBanco(rs.getInt("idbanco")); }
        catch(Exception e) {}
        try { this.setNumeroCuenta(rs.getString("numerocuenta")); }
        catch(Exception e) {}
        try { this.setBancoCuenta(rs.getString("cuentabanco")); }
        catch(Exception e) {}
        try { this.setNroPlanBancoCuenta(rs.getString("nroplancuentabanco")); }
        catch(Exception e) {}
        try { this.setBanco(rs.getString("banco")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaIngreso(rs.getDate("fechaingreso")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setIdCentroCosto(rs.getInt("idcentrocosto")); }
        catch(Exception e) {}
        try { this.setCentroCosto(rs.getString("centrocosto")); }
        catch(Exception e) {}
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) {}
        try { this.setNroPlanCuenta(rs.getString("nroplancuenta")); }
        catch(Exception e) {}
        try { this.setMontoEfectivo(rs.getDouble("montoefectivo")); }
        catch(Exception e) {}
        try { this.setMontoChequeOtro(rs.getDouble("montochequeotro")); }
        catch(Exception e) {}
        try { this.setMontoChequePropio(rs.getDouble("montochequepropio")); }
        catch(Exception e) {}
        try { this.setNota(rs.getString("nota")); }
        catch(Exception e) {}
        try { this.setNumeroBoleta(rs.getInt("numeroboleta")); }
        catch(Exception e) {}
        try { this.setIdBancoSucursal(rs.getInt("idbancosucursal")); }
        catch(Exception e) {}
        try { this.setBancoSucursal(rs.getString("bancosucursal")); }
        catch(Exception e) {}
        try { this.setRecibido(rs.getString("recibido")); }
        catch(Exception e) {}
        try { this.setAsiento(rs.getBoolean("asiento")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroDeposito(int inumerodeposito) {
        this.inumerodeposito = inumerodeposito;
    }

    public void setIdBancoCuenta(int iidBancoCuenta) {
        this.iidBancoCuenta = iidBancoCuenta;
    }
    
    public void setIdBanco(int iidBanco) {
        this.iidBanco = iidBanco;
    }

    public void setNumeroCuenta(String snumerocuenta) {
        this.snumerocuenta = snumerocuenta;
    }

    public void setBancoCuenta(String sbancocuenta) {
        this.sbancocuenta = sbancocuenta;
    }
    
    public void setNroPlanBancoCuenta(String snroPlanBancoCuenta) {
        this.snroPlanBancoCuenta = snroPlanBancoCuenta;
    }
    
    public void setBanco(String sbanco) {
        this.sbanco = sbanco;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setFechaIngreso(java.util.Date dfechaDeposito) {
        this.dfechaIngreso = dfechaDeposito;
    }

    public void setAsiento(boolean basiento) {
        this.basiento = basiento;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }

    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }
    
    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }
    
    public void setNroPlanCuenta(String snroPlanCuenta) {
        this.snroPlanCuenta = snroPlanCuenta;
    }

    public void setMontoEfectivo(double dmontoEfectivo) {
        this.dmontoEfectivo = dmontoEfectivo;
    }
    
    public void setMontoChequeOtro(double dmontoChequeOtro) {
        this.dmontoChequeOtro = dmontoChequeOtro;
    }
    
    public void setMontoChequePropio(double dmontoChequePropio) {
        this.dmontoChequePropio = dmontoChequePropio;
    }
    
    public void setNota(String snota) {
        this.snota = snota;
    }
    
    public void setNumeroBoleta(int inumeroBoleta) {
        this.inumeroBoleta = inumeroBoleta;
    }
    
    public void setIdBancoSucursal(int iidBancoSucursal) {
        this.iidBancoSucursal = iidBancoSucursal;
    }
    
    public void setBancoSucursal(String sbancoSucursal) {
        this.sbancoSucursal = sbancoSucursal;
    }
     
    public void setRecibido(String srecibido) {
        this.srecibido = srecibido;
    }
     
    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }

    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getNumeroDeposito() {
        return this.inumerodeposito;
    }

    public int getIdBancoCuenta() {
        return this.iidBancoCuenta;
    }
    
    public int getIdBanco() {
        return this.iidBanco;
    }

    public String getNumeroCuenta() {
        return this.snumerocuenta;
    }

    public String getBancoCuenta() {
        return this.sbancocuenta;
    }
    
    public String getNroPlanBancoCuenta() {
        return this.snroPlanBancoCuenta;
    }

    public String getBanco() {
        return this.sbanco;
    }
    
    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public java.util.Date getFechaIngreso() {
        return this.dfechaIngreso;
    }

    public boolean getAsiento() {
        return this.basiento;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }
 
    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }

    public String getCentroCosto() {
        return this.scentroCosto;
    }

    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }

    public String getNroPlanCuenta() {
        return this.snroPlanCuenta;
    }

    public double getMontoEfectivo() {
        return this.dmontoEfectivo;
    }
    
    public double getMontoChequeOtro() {
        return this.dmontoChequeOtro;
    }
    
    public double getMontoChequePropio() {
        return this.dmontoChequePropio;
    }
    
    public String getNota() {
        return this.snota;
    }

    public int getNumeroBoleta() {
        return this.inumeroBoleta;
    }

    public int getIdBancoSucursal() {
        return this.iidBancoSucursal;
    }

    public String getBancoSucursal() {
        return this.sbancoSucursal;
    }
    
    public String getRecibido() {
        return this.srecibido;
    }
    
    public int getIdFilial() {
        return this.iidFilial;
    }

    public String getFilial() {
        return this.sfilial;
    }
    
    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        /*if (this.getIdCentroCosto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CENTRO_COSTO;
            bvalido = false;
        }*/
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getRecibido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_RECIBIDO;
            bvalido = false;
        }
        if (this.getIdBancoCuenta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_BANCO_CUENTA;
            bvalido = false;
        }
        if (this.getIdPlanCuenta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_PLANCUENTA;
            bvalido = false;
        }
        /*if (this.getMontoEfectivo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO_EFECTIVO;
            bvalido = false;
        }        
        if (this.getMontoEfectivo()== 0 && this.getMontoChequeOtro()== 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO_EFECTIVO;
            bvalido = false;
        }*/        
        if (this.getMontoEfectivo()== 0 && this.getMontoChequeOtro()== 0 && this.getMontoChequePropio()==0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "No se introdujo ningún monto para este depósito. Verifique.";
            bvalido = false;
        }        
        if (this.getNumeroBoleta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_NUMERO_BOLETA;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.deposito("+
            this.getId()+","+
            this.getIdBancoCuenta()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            this.getIdCentroCosto()+","+
            this.getIdPlanCuenta()+","+
            this.getMontoEfectivo()+","+
            this.getMontoChequeOtro()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNota())+","+
            this.getNumeroBoleta()+","+
            this.getIdBancoSucursal()+","+
            utilitario.utiCadena.getTextoGuardado(this.getRecibido())+","+
            this.getIdEstado()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            this.getMontoChequePropio()+","+
            this.getNumeroDeposito()+","+
            this.getAsiento()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaIngreso())+","+
            this.getIdFilial()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha Depósito", "fecha", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Fecha Ingreso", "fechaingreso", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(3, "Número Depósito", "nrodeposito", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(4, "Número Boleta", "numeroboleta", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(5, "Centro de Costo", "centrocosto", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(6, "Número de cuenta", "numerocuenta", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(7, "Descripción de cuenta", "cuentabanco", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(8, "Estado", "estado", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(9, "Nota", "nota", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(10, "Filial", "filial", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
