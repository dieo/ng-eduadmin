/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetallePlanillaMovimiento {

    private int iid;
    private int iidPlanilla;
    private String splanilla;
    private int iidOperacion;
    private String soperacion;
    private int inumeroPlanilla;
    private int iidTipoMovimiento;
    private String stipoMovimiento;
    private String sdestinatario;
    private String sremitente;
    private int iidRegional;
    private String sregional;
    private String sobservacion;
    private java.util.Date dfecha;

    private short hestadoRegistro;
    private String smensaje;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_PLANILLA = "Planilla de Movimiento (no editable)";
    public static final String TEXTO_OPERACION = "Operación (no vacío)";

    public entDetallePlanillaMovimiento() {
        this.iid = 0;
        this.iidPlanilla = 0;
        this.splanilla = "";
        this.iidOperacion = 0;
        this.soperacion = "";
        this.inumeroPlanilla = 0;
        this.iidTipoMovimiento = 0;
        this.stipoMovimiento = "";
        this.sdestinatario = "";
        this.sremitente = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.sobservacion = "";
        this.dfecha = null;
       this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetallePlanillaMovimiento(int iid, int iidPlanilla, String splanilla, int iidOperacion, String soperacion, int inumeroPlanilla, int iidTipoMovimiento, String stipoMovimiento, String sdestinatario, String sremitente, int iidRegional, String sregional, String sobservacion, java.util.Date dfecha, short hestadoRegistro) {
        this.iid = iid;
        this.iidPlanilla = iidPlanilla;
        this.splanilla = splanilla;
        this.iidOperacion = iidOperacion;
        this.soperacion = soperacion;
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidTipoMovimiento = iidTipoMovimiento;
        this.stipoMovimiento = stipoMovimiento;
        this.sdestinatario = sdestinatario;
        this.sremitente = sremitente;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.sobservacion = sobservacion;
        this.dfecha = dfecha;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidPlanilla, String splanilla, int iidOperacion, String soperacion, int inumeroPlanilla, int iidTipoMovimiento, String stipoMovimiento, String sdestinatario, String sremitente, int iidRegional, String sregional, String sobservacion, java.util.Date dfecha, short hestadoRegistro) {
        this.iid = iid;
        this.iidPlanilla = iidPlanilla;
        this.splanilla = splanilla;
        this.iidOperacion = iidOperacion;
        this.soperacion = soperacion;
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidTipoMovimiento = iidTipoMovimiento;
        this.stipoMovimiento = stipoMovimiento;
        this.sdestinatario = sdestinatario;
        this.sremitente = sremitente;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.sobservacion = sobservacion;
        this.dfecha = dfecha;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetallePlanillaMovimiento copiar(entDetallePlanillaMovimiento destino) {
        destino.setEntidad(this.getId(), this.getIdPlanilla(), this.getPlanilla(), this.getIdOperacion(), this.getOperacion(), this.getNumeroPlanilla(), this.getIdTipoMovimiento(), this.getTipoMovimiento(), this.getDestinatario(), this.getRemitente(), this.getIdRegional(), this.getRegional(), this.getObservacion(), this.getFecha(), this.getEstadoRegistro());
        return destino;
    }
    
    public entDetallePlanillaMovimiento cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdPlanilla(rs.getInt("idplanilla")); }
        catch(Exception e) {}
        try { this.setPlanilla(rs.getString("planilla")); }
        catch(Exception e) {}
        try { this.setIdOperacion(rs.getInt("idoperacion")); }
        catch(Exception e) {}
        try { this.setOperacion(rs.getString("operacion")); }
        catch(Exception e) {}
        try { this.setNumeroPlanilla(rs.getInt("numeroplanilla")); }
        catch(Exception e) {}
        try { this.setIdTipoMovimiento(rs.getInt("idtipomovimiento")); }
        catch(Exception e) {}
        try { this.setTipoMovimiento(rs.getString("tipomovimiento")); }
        catch(Exception e) {}
        try { this.setDestinatario(rs.getString("destinatario")); }
        catch(Exception e) {}
        try { this.setRemitente(rs.getString("remitente")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdPlanilla(int iidPlanilla) {
        this.iidPlanilla = iidPlanilla;
    }

    public void setPlanilla(String splanilla) {
        this.splanilla = splanilla;
    }

    public void setIdOperacion(int iidOperacion) {
        this.iidOperacion = iidOperacion;
    }

    public void setOperacion(String soperacion) {
        this.soperacion = soperacion;
    }

    public void setNumeroPlanilla(int inumeroPlanilla) {
        this.inumeroPlanilla = inumeroPlanilla;
    }

    public void setIdTipoMovimiento(int iidTipoMovimiento) {
        this.iidTipoMovimiento = iidTipoMovimiento;
    }

    public void setTipoMovimiento(String stipoMovimiento) {
        this.stipoMovimiento = stipoMovimiento;
    }

    public void setDestinatario(String sdestinatario) {
        this.sdestinatario = sdestinatario;
    }

    public void setRemitente(String sremitente) {
        this.sremitente = sremitente;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdPlanilla() {
        return this.iidPlanilla;
    }

    public String getPlanilla() {
        return this.splanilla;
    }

    public int getIdOperacion() {
        return this.iidOperacion;
    }

    public String getOperacion() {
        return this.soperacion;
    }

    public int getNumeroPlanilla() {
        return this.inumeroPlanilla;
    }

    public int getIdTipoMovimiento() {
        return this.iidTipoMovimiento;
    }

    public String getTipoMovimiento() {
        return this.stipoMovimiento;
    }

    public String getDestinatario() {
        return this.sdestinatario;
    }

    public String getRemitente() {
        return this.sremitente;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdOperacion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OPERACION;
            bvalido = false;
        }
        if (esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Operación ya fue registrada en la Planilla";
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT detalleplanillamovimiento("+
            this.getId()+","+
            this.getIdPlanilla()+","+
            this.getIdOperacion()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
