/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbCentroCosto extends generico.entGenerica{
    
    protected int iidPlanCuenta;
    public static final int LONGITUD_DESCRIPCION = 100;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Centro de Costo (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";

    public entbCentroCosto() {
        super();
        this.iidPlanCuenta = 0;
    }

    public entbCentroCosto(int iid, String sdescripcion, int iidPlanCuenta) {
        super(iid, sdescripcion);
        this.iidPlanCuenta = iidPlanCuenta;
    }

    public entbCentroCosto(int iid, String sdescripcion, int iidPlanCuenta, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidPlanCuenta = iidPlanCuenta;
    }

    public void setEntidad(int iid, String sdescripcion, int iidPlanCuenta, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidPlanCuenta = iidPlanCuenta;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entbCentroCosto copiar(entbCentroCosto destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdPlanCuenta(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbCentroCosto cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) {}
        return this;
    }

    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }

    public int getIdPlanCuenta() {
        return iidPlanCuenta;
    }
    
    public String getCampoConcatenado() {
        return this.getId()+" "+this.getDescripcion();
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT banco.centrocosto("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdPlanCuenta()+","+
            this.getEstadoRegistro()+")";
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", new generico.entLista().tipo_texto));
        return lst;
    }
    
}