/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entAnalisisDisponibilidad2 {

    private int iid;
    private java.util.Date dfecha;
    private String stabla;
    private int iidMovimiento;
    private int iidSocio;
    private String scedula;
    private String snombreApellido;
    private int iidRubro;
    private String srubro;
    private int iidGiraduria;
    private String sgiraduria;
    private double usueldo;
    private int iidMoneda;
    private String smoneda;
    private double ujubilacion;
    private double uiva;
    private double udisponible;
    private double uporcentajeJubilacion;
    private double uporcentajeIva;
    private double uporcentajeDisponible;
    private double usueldoLiquido;
    private double utotalDescuentoActual;
    private double utotalDescuentoSiguiente;
    private double usaldoDisponibleActual;
    private double usaldoDisponibleSiguiente;
    private double usaldo75Actual;
    private double usaldo75Siguiente;
    private double uporcentajeJubilacionRubro;
    private double uporcentajeIvaRubro;
    
    protected short hestadoRegistro;
    protected String smensaje;
    
    public static final int LONGITUD_DIRECCION = 100;
    public static final int LONGITUD_TELEFONO = 40;
    public static final int LONGITUD_DATO_ADICIONAL = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FECHA = "Fecha (no vacía)";
    public static final String TEXTO_CEDULA = "Cédula (no editable)";
    public static final String TEXTO_NOMBRE_APELLIDO = "Nombre y Apellido (no editable)";
    public static final String TEXTO_RUBRO = "Rubro (no editable)";
    public static final String TEXTO_GIRADURIA = "Giraduría (no editable)";
    public static final String TEXTO_SUELDO = "Sueldo (no vacío, valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_JUBILACION = "Jubilación (no vacío, valor positivo)";
    public static final String TEXTO_IVA = "Iva (no vacío, valor positivo)";
    public static final String TEXTO_DISPONIBLE = "Disponibilidad (no vacío, valor positivo)";
    public static final String TEXTO_PORCENTAJE_JUBILACION = "Porcentaje de Jubilación (no vacío, valor positivo)";
    public static final String TEXTO_PORCENTAJE_IVA = "Porcentaje de Iva (no vacío, valor positivo)";
    public static final String TEXTO_PORCENTAJE_DISPONIBLE = "Porcentaje de Disponibilidad (no vacío, valor positivo)";
    public static final String TEXTO_SUELDO_LIQUIDO = "Sueldo Líquido (no editable)";
    public static final String TEXTO_TOTAL_DESCUENTO = "Total Descuento (no editable)";
    public static final String TEXTO_SALDO_DISPONIBLE = "Saldo Disponible (no editable)";
    public static final String TEXTO_SALDO_75 = "Saldo 75% (no editable)";

    public entAnalisisDisponibilidad2() {
        this.iid = 0;
        this.dfecha = null;
        this.stabla = "";
        this.iidMovimiento = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.snombreApellido = "";
        this.iidRubro = 0;
        this.srubro = "";
        this.iidGiraduria = 0;
        this.sgiraduria = "";
        this.usueldo = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.ujubilacion = 0.0;
        this.uiva = 0.0;
        this.udisponible = 0.0;
        this.uporcentajeJubilacion = 0.0;
        this.uporcentajeIva = 0.0;
        this.uporcentajeDisponible = 0.0;
        this.usueldoLiquido = 0.0;
        this.utotalDescuentoActual = 0.0;
        this.utotalDescuentoSiguiente = 0.0;
        this.usaldoDisponibleActual = 0.0;
        this.usaldoDisponibleSiguiente = 0.0;
        this.usaldo75Actual = 0.0;
        this.usaldo75Siguiente = 0.0;
        this.uporcentajeJubilacionRubro = 0.0;
        this.uporcentajeIvaRubro = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entAnalisisDisponibilidad2(int iid, java.util.Date dfecha, String stabla, int iidMovimiento, int iidSocio, String scedula, String snombreApellido, int iidRubro, String srubro, int iidGiraduria, String sgiraduria, double usueldo, int iidMoneda, String smoneda, double ujubilacion, double uiva, double udisponible, double uporcentajeJubilacion, double uporcentajeIva, double uporcentajeDisponible, double usueldoLiquido, double utotalDescuentoActual, double utotalDescuentoSiguiente, double usaldoDisponibleActual, double usaldoDisponibleSiguiente, double usaldo75Actual, double usaldo75Siguiente, double uporcentajeJubilacionRubro, double uporcentajeIvaRubro, short hestadoRegistro) {
        this.iid = iid;
        this.dfecha = dfecha;
        this.stabla = stabla;
        this.iidMovimiento = iidMovimiento;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.usueldo = usueldo;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.ujubilacion = ujubilacion;
        this.uiva = uiva;
        this.udisponible = udisponible;
        this.uporcentajeJubilacion = uporcentajeJubilacion;
        this.uporcentajeIva = uporcentajeIva;
        this.uporcentajeDisponible = uporcentajeDisponible;
        this.usueldoLiquido = usueldoLiquido;
        this.utotalDescuentoActual = utotalDescuentoActual;
        this.utotalDescuentoSiguiente = utotalDescuentoSiguiente;
        this.usaldoDisponibleActual = usaldoDisponibleActual;
        this.usaldoDisponibleSiguiente = usaldoDisponibleSiguiente;
        this.usaldo75Actual = usaldo75Actual;
        this.usaldo75Siguiente = usaldo75Siguiente;
        this.uporcentajeJubilacionRubro = uporcentajeJubilacionRubro;
        this.uporcentajeIvaRubro = uporcentajeIvaRubro;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, java.util.Date dfecha, String stabla, int iidMovimiento, int iidSocio, String scedula, String snombreApellido, int iidRubro, String srubro, int iidGiraduria, String sgiraduria, double usueldo, int iidMoneda, String smoneda, double ujubilacion, double uiva, double udisponible, double uporcentajeJubilacion, double uporcentajeIva, double uporcentajeDisponible, double usueldoLiquido, double utotalDescuentoActual, double utotalDescuentoSiguiente, double usaldoDisponibleActual, double usaldoDisponibleSiguiente, double usaldo75Actual, double usaldo75Siguiente, double uporcentajeJubilacionRubro, double uporcentajeIvaRubro, short hestadoRegistro) {
        this.iid = iid;
        this.dfecha = dfecha;
        this.stabla = stabla;
        this.iidMovimiento = iidMovimiento;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.usueldo = usueldo;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.ujubilacion = ujubilacion;
        this.uiva = uiva;
        this.udisponible = udisponible;
        this.uporcentajeJubilacion = uporcentajeJubilacion;
        this.uporcentajeIva = uporcentajeIva;
        this.uporcentajeDisponible = uporcentajeDisponible;
        this.usueldoLiquido = usueldoLiquido;
        this.utotalDescuentoActual = utotalDescuentoActual;
        this.utotalDescuentoSiguiente = utotalDescuentoSiguiente;
        this.usaldoDisponibleActual = usaldoDisponibleActual;
        this.usaldoDisponibleSiguiente = usaldoDisponibleSiguiente;
        this.usaldo75Actual = usaldo75Actual;
        this.usaldo75Siguiente = usaldo75Siguiente;
        this.uporcentajeJubilacionRubro = uporcentajeJubilacionRubro;
        this.uporcentajeIvaRubro = uporcentajeIvaRubro;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entAnalisisDisponibilidad2 copiar(entAnalisisDisponibilidad2 destino) {
        destino.setEntidad(this.getId(), this.getFecha(), this.getTabla(), this.getIdMovimiento(), this.getIdSocio(), this.getCedula(), this.getNombreApellido(), this.getIdRubro(), this.getRubro(), this.getIdGiraduria(), this.getGiraduria(), this.getSueldo(), this.getIdMoneda(), this.getMoneda(), this.getJubilacion(), this.getIva(), this.getDisponible(), this.getPorcentajeJubilacion(), this.getPorcentajeIva(), this.getPorcentajeDisponible(), this.getSueldoLiquido(), this.getTotalDescuentoActual(), this.getTotalDescuentoSiguiente(), this.getSaldoDisponibleActual(), this.getSaldoDisponibleSiguiente(), this.getSaldo75Actual(), this.getSaldo75Siguiente(), this.getPorcentajeJubilacionRubro(), this.getPorcentajeIvaRubro(), this.getEstadoRegistro());
        return destino;
    }

    public entAnalisisDisponibilidad2 cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setNombreApellido(rs.getString("nombreapellido")); }
        catch(Exception e) {}
        try { this.setIdRubro(rs.getInt("idrubro")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        try { this.setIdGiraduria(rs.getInt("idgiraduria")); }
        catch(Exception e) {}
        try { this.setGiraduria(rs.getString("giraduria")); }
        catch(Exception e) {}
        try { this.setSueldo(rs.getDouble("sueldo")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setJubilacion(rs.getDouble("jubilacion")); }
        catch(Exception e) {}
        try { this.setIva(rs.getDouble("iva")); }
        catch(Exception e) {}
        try { this.setDisponible(rs.getDouble("disponible")); }
        catch(Exception e) {}
        try { this.setPorcentajeJubilacion(rs.getDouble("porcentajejubilacion")); }
        catch(Exception e) {}
        try { this.setPorcentajeIva(rs.getDouble("porcentajeiva")); }
        catch(Exception e) {}
        try { this.setPorcentajeDisponible(rs.getDouble("porcentajedisponible")); }
        catch(Exception e) {}
        try { this.setPorcentajeJubilacionRubro(rs.getDouble("porcentajejubilacionrubro")); }
        catch(Exception e) {}
        try { this.setPorcentajeIvaRubro(rs.getDouble("porcentajeivarubro")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombreApellido(String snombreApellido) {
        this.snombreApellido = snombreApellido;
    }

    public void setIdRubro(int iidRubro) {
        this.iidRubro = iidRubro;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }

    public void setGiraduria(String sgiraduria) {
        this.sgiraduria = sgiraduria;
    }

    public void setSueldo(double usueldo) {
        this.usueldo = usueldo;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }

    public void setJubilacion(double ujubilacion) {
        this.ujubilacion = ujubilacion;
    }

    public void setIva(double uiva) {
        this.uiva = uiva;
    }

    public void setDisponible(double udisponible) {
        this.udisponible = udisponible;
    }

    public void setPorcentajeJubilacion(double uporcentajeJubilacion) {
        this.uporcentajeJubilacion = uporcentajeJubilacion;
    }

    public void setPorcentajeIva(double uporcentajeIva) {
        this.uporcentajeIva = uporcentajeIva;
    }

    public void setPorcentajeDisponible(double uporcentajeDisponible) {
        this.uporcentajeDisponible = uporcentajeDisponible;
    }

    public void setSueldoLiquido(double usueldoLiquido) {
        this.usueldoLiquido = usueldoLiquido;
    }

    public void setTotalDescuentoActual(double utotalDescuentoActual) {
        this.utotalDescuentoActual = utotalDescuentoActual;
    }

    public void setTotalDescuentoSiguiente(double utotalDescuentoSiguiente) {
        this.utotalDescuentoSiguiente = utotalDescuentoSiguiente;
    }

    public void setSaldoDisponibleActual(double usaldoDisponibleActual) {
        this.usaldoDisponibleActual = usaldoDisponibleActual;
    }

    public void setSaldoDisponibleSiguiente(double usaldoDisponibleSiguiente) {
        this.usaldoDisponibleSiguiente = usaldoDisponibleSiguiente;
    }

    public void setSaldo75Actual(double usaldo75Actual) {
        this.usaldo75Actual = usaldo75Actual;
    }

    public void setSaldo75Siguiente(double usaldo75Siguiente) {
        this.usaldo75Siguiente = usaldo75Siguiente;
    }

    public void setPorcentajeJubilacionRubro(double uporcentajeJubilacionRubro) {
        this.uporcentajeJubilacionRubro = uporcentajeJubilacionRubro;
    }

    public void setPorcentajeIvaRubro(double uporcentajeIvaRubro) {
        this.uporcentajeIvaRubro = uporcentajeIvaRubro;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public String getTabla() {
        return this.stabla;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombreApellido() {
        return this.snombreApellido;
    }

    public int getIdRubro() {
        return this.iidRubro;
    }

    public String getRubro() {
        return this.srubro;
    }

    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public String getGiraduria() {
        return this.sgiraduria;
    }

    public double getSueldo() {
        return this.usueldo;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public double getJubilacion() {
        return this.ujubilacion;
    }

    public double getIva() {
        return this.uiva;
    }

    public double getDisponible() {
        return this.udisponible;
    }

    public double getPorcentajeJubilacion() {
        return this.uporcentajeJubilacion;
    }

    public double getPorcentajeIva() {
        return this.uporcentajeIva;
    }

    public double getPorcentajeDisponible() {
        return this.uporcentajeDisponible;
    }

    public double getSueldoLiquido() {
        return this.usueldoLiquido;
    }

    public double getTotalDescuentoActual() {
        return this.utotalDescuentoActual;
    }

    public double getTotalDescuentoSiguiente() {
        return this.utotalDescuentoSiguiente;
    }

    public double getSaldoDisponibleActual() {
        return this.usaldoDisponibleActual;
    }

    public double getSaldoDisponibleSiguiente() {
        return this.usaldoDisponibleSiguiente;
    }

    public double getSaldo75Actual() {
        return this.usaldo75Actual;
    }

    public double getSaldo75Siguiente() {
        return this.usaldo75Siguiente;
    }

    public double getPorcentajeJubilacionRubro() {
        return this.uporcentajeJubilacionRubro;
    }

    public double getPorcentajeIvaRubro() {
        return this.uporcentajeIvaRubro;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getJubilacion() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_JUBILACION;
            bvalido = false;
        }
        if (this.getPorcentajeJubilacion() < 0 || this.getPorcentajeJubilacion() >= 100) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PORCENTAJE_JUBILACION;
            bvalido = false;
        }
        if (this.getIva() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IVA;
            bvalido = false;
        }
        if (this.getPorcentajeIva() < 0 || this.getPorcentajeIva() >= 100) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PORCENTAJE_IVA;
            bvalido = false;
        }
        if (this.getDisponible() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DISPONIBLE;
            bvalido = false;
        }
        if (this.getPorcentajeDisponible() < 0 || this.getPorcentajeDisponible() > 100) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PORCENTAJE_DISPONIBLE;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT analisis("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getIdMovimiento()+","+
            this.getIdRubro()+","+
            this.getIdGiraduria()+","+
            this.getSueldo()+","+
            this.getIdMoneda()+","+
            this.getJubilacion()+","+
            this.getIva()+","+
            this.getDisponible()+","+
            this.getPorcentajeJubilacion()+","+
            this.getPorcentajeIva()+","+
            this.getPorcentajeDisponible()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
