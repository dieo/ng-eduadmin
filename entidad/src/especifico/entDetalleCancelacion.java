/*0
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleCancelacion {
    
    private int iid;
    private int iidMovimiento;
    private int iidIngreso;
    private int iidMovimientoCancela;
    private String stabla;
    private int iidDetalleOperacion;
    private double umontoCapital;
    private double umontoInteres;
    private double umontoExoneracion;
    private double umontoSaldo;
    private double uinteresMoratorio;
    private String senviado;
    private String sestado; // A=atraso - C=cobro - S=saldo - E=enviado
    private int icuota;
    private java.util.Date dfechaVencimiento;

    private int inumeroOperacion;
    private int iplazo;
    private java.util.Date dfechaOperacion;
    private double utasaInteres;
    private boolean batraso;
    private boolean bcancela;
    private int iidCuenta;
    private String scuenta;
    private String sdescripcion;
    private double umontoAtraso;
    private double umontoSaldoTotal;
    private int iidImpuesto;
    private double utasaImpuesto;

    private short hestadoRegistro;
    private String smensaje;

    public static final String estado_atraso = "A";
    public static final String estado_cobro = "C";
    public static final String estado_enviado = "E";
    public static final String estado_saldo = "S";
    
    public entDetalleCancelacion() {
        this.iid = 0;
        this.iidMovimiento = 0;
        this.iidIngreso = 0;
        this.iidMovimientoCancela = 0;
        this.stabla = "";
        this.iidDetalleOperacion = 0;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.umontoExoneracion = 0.0;
        this.umontoSaldo = 0.0;
        this.uinteresMoratorio = 0.0;
        this.senviado = "";
        this.sestado = "";
        this.icuota = 0;
        this.dfechaVencimiento = null;

        this.inumeroOperacion = 0;
        this.iplazo = 0;
        this.dfechaOperacion = null;
        this.utasaInteres = 0.0;
        this.batraso = false;
        this.bcancela = false;
        this.iidCuenta = 0;
        this.scuenta = "";
        this.sdescripcion = "";
        this.umontoAtraso = 0.0;
        this.umontoSaldoTotal = 0.0;
        this.iidImpuesto = 0;
        this.utasaImpuesto = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetalleCancelacion(int iid, int iidMovimiento, int iidIngreso, int iidMovimientoCancela, String stabla, int iidDetalleOperacion, double umontoCapital, double umontoInteres, double umontoExoneracion, double umontoSaldo, double uinteresMoratorio, String senviado, String sestado, int icuota, java.util.Date dfechaVencimiento, int inumeroOperacion, int iplazo, java.util.Date dfechaOperacion, double utasaInteres, boolean batraso, boolean bcancela, int iidCuenta, String scuenta, String sdescripcion, double umontoAtraso, double umontoSaldoTotal, int iidImpuesto, double utasaImpuesto, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidIngreso = iidIngreso;
        this.iidMovimientoCancela = iidMovimientoCancela;
        this.stabla = stabla;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoSaldo = umontoSaldo;
        this.uinteresMoratorio = uinteresMoratorio;
        this.senviado = senviado;
        this.sestado = sestado;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;

        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteres = utasaInteres;
        this.batraso = batraso;
        this.bcancela = bcancela;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdescripcion = sdescripcion;
        this.umontoAtraso = umontoAtraso;
        this.umontoSaldoTotal = umontoSaldoTotal;
        this.iidImpuesto = iidImpuesto;
        this.utasaImpuesto = utasaImpuesto;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidMovimiento, int iidIngreso, int iidMovimientoCancela, String stabla, int iidDetalleOperacion, double umontoCapital, double umontoInteres, double umontoExoneracion, double umontoSaldo, double uinteresMoratorio, String senviado, String sestado, int icuota, java.util.Date dfechaVencimiento, int inumeroOperacion, int iplazo, java.util.Date dfechaOperacion, double utasaInteres, boolean batraso, boolean bcancela, int iidCuenta, String scuenta, String sdescripcion, double umontoAtraso, double umontoSaldoTotal, int iidImpuesto, double utasaImpuesto, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidIngreso = iidIngreso;
        this.iidMovimientoCancela = iidMovimientoCancela;
        this.stabla = stabla;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoSaldo = umontoSaldo;
        this.uinteresMoratorio = uinteresMoratorio;
        this.senviado = senviado;
        this.sestado = sestado;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;

        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteres = utasaInteres;
        this.batraso = batraso;
        this.bcancela = bcancela;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdescripcion = sdescripcion;
        this.umontoAtraso = umontoAtraso;
        this.umontoSaldoTotal = umontoSaldoTotal;
        this.iidImpuesto = iidImpuesto;
        this.utasaImpuesto = utasaImpuesto;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleCancelacion copiar(entDetalleCancelacion destino) {
        destino.setEntidad(this.getId(), this.getIdMovimiento(), this.getIdIngreso(), this.getIdMovimientoCancela(), this.getTabla(), this.getIdDetalleOperacion(), this.getMontoCapital(), this.getMontoInteres(), this.getMontoExoneracion(), this.getMontoSaldo(), this.getInteresMoratorio(), this.getEnviado(), this.getEstado(), this.getCuota(), this.getFechaVencimiento(), this.getNumeroOperacion(), this.getPlazo(), this.getFechaOperacion(), this.getTasaInteres(), this.getAtraso(), this.getCancela(), this.getIdCuenta(), this.getCuenta(), this.getDescripcion(), this.getMontoAtraso(), this.getMontoSaldoTotal(), this.getIdImpuesto(), this.getTasaImpuesto(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleCancelacion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) { }
        try { this.setIdIngreso(rs.getInt("idingreso")); }
        catch(Exception e) { }
        try { this.setIdMovimientoCancela(rs.getInt("idmovimientocancela")); }
        catch(Exception e) { }
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) { }
        try { this.setIdDetalleOperacion(rs.getInt("iddetalleoperacion")); }
        catch(Exception e) { }
        try { this.setMontoCapital(rs.getDouble("montocapital")); }
        catch(Exception e) { }
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) { }
        try { this.setMontoExoneracion(rs.getDouble("montoexoneracion")); }
        catch(Exception e) { }
        try { this.setMontoSaldo(rs.getDouble("montosaldo")); }
        catch(Exception e) { }
        try { this.setInteresMoratorio(rs.getDouble("interesmoratorio")); }
        catch(Exception e) { }
        try { this.setEnviado(rs.getString("enviado")); }
        catch(Exception e) { }
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) { }
        try { this.setCuota(rs.getInt("cuota")); }
        catch(Exception e) { }
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) { }
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) { }
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) { }
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) { }
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(Exception e) { }
        try { this.setAtraso(rs.getBoolean("atraso")); }
        catch(Exception e) { }
        try { this.setCancela(rs.getBoolean("cancela")); }
        catch(Exception e) { }
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) { }
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) { }
        try { this.setMontoAtraso(rs.getDouble("montoatraso")); }
        catch(Exception e) { }
        try { this.setMontoSaldoTotal(rs.getDouble("montosaldototal")); }
        catch(Exception e) { }
        try { this.setIdImpuesto(rs.getInt("idimpuesto")); }
        catch(Exception e) { }
        try { this.setTasaImpuesto(rs.getDouble("tasaimpuesto")); }
        catch(Exception e) { }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setIdIngreso(int iidIngreso) {
        this.iidIngreso = iidIngreso;
    }

    public void setIdMovimientoCancela(int iidMovimientoCancela) {
        this.iidMovimientoCancela = iidMovimientoCancela;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setIdDetalleOperacion(int iidDetalleOperacion) {
        this.iidDetalleOperacion = iidDetalleOperacion;
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setMontoExoneracion(double umontoExoneracion) {
        this.umontoExoneracion = umontoExoneracion;
    }

    public void setMontoSaldo(double umontoSaldo) {
        this.umontoSaldo = umontoSaldo;
    }

    public void setInteresMoratorio(double uinteresMoratorio) {
        this.uinteresMoratorio = uinteresMoratorio;
    }

    public void setEnviado(String senviado) {
        this.senviado = senviado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setTasaInteres(double utasaInteres) {
        this.utasaInteres = utasaInteres;
    }

    public void setAtraso(boolean batraso) {
        this.batraso = batraso;
    }

    public void setCancela(boolean bcancela) {
        this.bcancela = bcancela;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setMontoAtraso(double umontoAtraso) {
        this.umontoAtraso = umontoAtraso;
    }

    public void setMontoSaldoTotal(double umontoSaldoTotal) {
        this.umontoSaldoTotal = umontoSaldoTotal;
    }

    public void setIdImpuesto(int iidImpuesto) {
        this.iidImpuesto = iidImpuesto;
    }

    public void setTasaImpuesto(double utasaImpuesto) {
        this.utasaImpuesto = utasaImpuesto;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public int getIdIngreso() {
        return this.iidIngreso;
    }

    public int getIdMovimientoCancela() {
        return this.iidMovimientoCancela;
    }

    public String getTabla() {
        return this.stabla;
    }

    public int getIdDetalleOperacion() {
        return this.iidDetalleOperacion;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public double getMontoExoneracion() {
        return this.umontoExoneracion;
    }

    public double getMontoSaldo() {
        return this.umontoSaldo;
    }

    public double getInteresMoratorio() {
        return this.uinteresMoratorio;
    }

    public String getEnviado() {
        return this.senviado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public int getCuota() {
        return this.icuota;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public double getTasaInteres() {
        return this.utasaInteres;
    }

    public boolean getAtraso() {
        return this.batraso;
    }

    public boolean getCancela() {
        return this.bcancela;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public double getMontoAtraso() {
        return this.umontoAtraso;
    }

    public double getMontoSaldoTotal() {
        return this.umontoSaldoTotal;
    }

    public int getIdImpuesto() {
        return this.iidImpuesto;
    }

    public double getTasaImpuesto() {
        return this.utasaImpuesto;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getMontoCapital()< 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            //this.smensaje += TEXTO_MONTO_CAPITAL;
            bvalido = false;
        }
        if (this.getMontoInteres()< 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            //this.smensaje += TEXTO_MONTO_INTERES;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detallecancelacion("+
            this.getId()+","+
            this.getIdMovimiento()+","+
            this.getIdMovimientoCancela()+","+
            this.getIdDetalleOperacion()+","+
            this.getCuota()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaVencimiento())+","+
            this.getMontoCapital()+","+
            this.getMontoInteres()+","+
            this.getMontoSaldo()+","+
            this.getMontoExoneracion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getEnviado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getEstado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getNumeroOperacion()+","+
            this.getPlazo()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaOperacion())+","+
            this.getTasaInteres()+","+
            this.getIdCuenta()+","+
            this.getCancela()+","+
            this.getInteresMoratorio()+","+
            this.getEstadoRegistro()+")";
    }
    
    public String getConsulta(int iidSocio, int iidInteres, String sinteres, int iidEstadoOrdenCredito, int iidEstadoPrestamo, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo) {
        return "SELECT * FROM (" +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, mo.idsocio, mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion, mo.montoaprobado AS monto,mo.plazoaprobado AS plazo, mo.tasainteres, mo.idestado, mo.idcuenta, c.descripcion AS cuenta, mo.idimpuesto, i.tasa AS tasaImpuesto "+
                       "FROM detalleoperacion as dop LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id LEFT JOIN cuenta AS c ON mo.idcuenta=c.id LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoOrdenCredito+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, 0 AS saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, mo.idsocio, mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion, mo.montoaprobado AS monto,mo.plazoaprobado AS plazo, mo.tasainteres, mo.idestado, mo.idcuenta, c.descripcion AS cuenta, mo.idimpuesto, i.tasa AS tasaImpuesto "+
                       "FROM detalleoperacion as dop LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id LEFT JOIN cuenta AS c ON mo.idcuenta=c.id LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, dop.saldointeres, 0 AS saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, mo.idsocio, mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion, mo.montoaprobado AS monto,mo.plazoaprobado AS plazo, mo.tasainteres, mo.idestado, "+iidInteres+" AS idcuenta, '"+sinteres+"' AS cuenta, mo.idimpuesto, i.tasa AS tasaImpuesto "+
                       "FROM detalleoperacion as dop LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id LEFT JOIN cuenta AS c ON mo.idcuenta=c.id LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, ap.idsocio, ap.idmoneda, ap.numerooperacion, ap.fechaoperacion, ap.importe AS monto, ap.plazo,ap.tasainteres, ap.idestado, ap.idcuenta, c.descripcion AS cuenta, 0 AS idimpuesto, 0 AS tasaImpuesto "+
                       "FROM detalleoperacion AS dop LEFT JOIN ahorroprogramado AS ap ON dop.idmovimiento=ap.id LEFT JOIN cuenta AS c ON ap.idcuenta=c.id WHERE dop.tabla='ap' AND ap.idestado="+iidEstadoAhorro+" AND ap.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, of.idsocio, of.idmoneda, of.numerooperacion, of.fechaoperacion, of.importe AS monto, of.plazo,of.tasainteres, of.idestado, of.idcuenta, c.descripcion AS cuenta, 0 AS idimpuesto, 0 AS tasaImpuesto "+ 
                       "FROM detalleoperacion AS dop LEFT JOIN operacionfija AS of ON dop.idmovimiento=of.id LEFT JOIN cuenta AS c ON of.idcuenta=c.id WHERE dop.tabla='of' AND of.idestado="+iidEstadoOperacion+" AND of.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, js.idsocio, js.idmoneda, js.numerooperacion, js.fechaoperacion, js.importetitular+js.importeadherente*js.cantidadadherente AS monto, 0 AS plazo, 0.0 AS tasainteres, js.idestado, js.idcuenta, c.descripcion AS cuenta, 0 AS idimpuesto, 0 AS tasaImpuesto "+
                       "FROM detalleoperacion AS dop LEFT JOIN fondojuridicosepelio AS js ON dop.idmovimiento=js.id LEFT JOIN cuenta AS c ON js.idcuenta=c.id WHERE dop.tabla='js' AND js.idestado="+iidEstadoFondo+" AND js.idsocio="+iidSocio+" "+
               ") AS detalle ORDER BY tabla, numerooperacion, idcuenta DESC, cuota";
    }
    
}
