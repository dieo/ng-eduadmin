/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entGenericaPersona;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entReferenciaPersonal extends entGenericaPersona {

    protected String sdireccion;
    protected String stelefono;
    protected int iidSocio;
    protected int iidParentesco;
    protected String sparentesco;
    
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final int LONGITUD_CEDULA = 12;
    public static final int LONGITUD_DIRECCION = 200;
    public static final int LONGITUD_TELEFONO = 100;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacío, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_CEDULA = "Cédula (no vacía, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_DIRECCION = "Dirección (no vacía, hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_TELEFONO = "Teléfono (no vacío, hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_PARENTESCO = "Parentesco (no vacío)";

    public entReferenciaPersonal() {
        super();
        this.sdireccion = "";
        this.stelefono = "";
        this.iidSocio = 0;
        this.iidParentesco = 0;
        this.sparentesco = "";
    }

    public entReferenciaPersonal(int iid, String snombre, String sapellido, String scedula, String sdireccion, String stelefono, int iidSocio, int iidParentesco, String sparentesco, short hestadoRegistro) {
        super(iid, snombre, sapellido, scedula, hestadoRegistro);
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidSocio = iidSocio;
        this.iidParentesco = iidParentesco;
        this.sparentesco = sparentesco;
    }
    
    public void setEntidad(int iid, String snombre, String sapellido, String scedula, String sdireccion, String stelefono, int iidSocio, int iidParentesco, String sparentesco, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre.trim().toUpperCase();
        this.sapellido = sapellido.trim().toUpperCase();
        this.scedula = scedula;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidSocio = iidSocio;
        this.iidParentesco = iidParentesco;
        this.sparentesco = sparentesco;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entReferenciaPersonal copiar(entReferenciaPersonal destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getDireccion(), this.getTelefono(), this.getIdSocio(), this.getIdParentesco(), this.getParentesco(), this.getEstadoRegistro());
        return destino;
    }
    
    public entReferenciaPersonal cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setIdParentesco(rs.getInt("idparentesco")); }
        catch(Exception e) {}
        try { this.setParentesco(rs.getString("parentesco")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdParentesco(int iidParentesco) {
        this.iidParentesco = iidParentesco;
    }

    public void setParentesco(String sparentesco) {
        this.sparentesco = sparentesco;
    }

    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefono() {
        return this.stelefono;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public int getIdParentesco() {
        return this.iidParentesco;
    }

    public String getParentesco() {
        return this.sparentesco;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_APELLIDO;
            bvalido = false;
        }
        if (this.getDireccion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DIRECCION;
            bvalido = false;
        }
        if (this.getTelefono().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO;
            bvalido = false;
        }
        if (this.getIdParentesco()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PARENTESCO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT referenciapersonal("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getIdSocio()+","+
            this.getIdParentesco()+","+
            this.getEstadoRegistro()+")";
    }

}
