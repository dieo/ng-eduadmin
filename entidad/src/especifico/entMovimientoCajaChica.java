/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMovimientoCajaChica extends generico.entGenerica {

    protected int iidPlanilla;
    protected int inumeroPlanilla;
    protected String splanilla;
    protected int iidConcepto;
    protected String sconcepto;
    protected java.util.Date dfechaOperacion;
    protected double umontoDebito;
    protected double umontoCredito;
    protected double umontoImpuesto;
    protected double umontoTotal;
    
    public static final int LONGITUD_DESCRIPCION = 200;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Movimiento (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_NUMERO_PLANILLA = "Número de Planilla (no editable)";
    public static final String TEXTO_CONCEPTO = "Concepto de Caja Chica (no vacío)";
    public static final String TEXTO_FECHA_OPERACION = "Fecha de operacion (no vacía)";
    public static final String TEXTO_MONTO_DEBITO = "Monto Débito (valor positivo)";
    public static final String TEXTO_MONTO_CREDITO = "Monto Crédito (valor positivo)";
    public static final String TEXTO_MONTO_IMPUESTO = "Monto Impuesto (valor positivo)";
    public static final String TEXTO_MONTO_TOTAL = "Monto Total (valor positivo)";

    public entMovimientoCajaChica() {
        super();
        this.iidPlanilla = 0;
        this.inumeroPlanilla = 0;
        this.splanilla = "";
        this.iidConcepto = 0;
        this.sconcepto = "";
        this.dfechaOperacion = null;
        this.umontoDebito = 0.0;
        this.umontoCredito = 0.0;
        this.umontoImpuesto = 0.0;
        this.umontoTotal = 0.0;
    }

    public entMovimientoCajaChica(int iid, String sdescripcion, int iidPlanilla, int inumeroPlanilla, String splanilla, int iidConcepto, String sconcepto, java.util.Date dfechaOperacion, double umontoDebito, double umontoCredito, double umontoImpuesto, double umontoTotal) {
        super(iid, sdescripcion);
        this.iidPlanilla = iidPlanilla;
        this.inumeroPlanilla = inumeroPlanilla;
        this.splanilla = splanilla;
        this.iidConcepto = iidConcepto;
        this.sconcepto = sconcepto;
        this.dfechaOperacion = dfechaOperacion;
        this.umontoDebito = umontoDebito;
        this.umontoCredito = umontoCredito;
        this.umontoImpuesto = umontoImpuesto;
        this.umontoTotal = umontoTotal;
    }

    public entMovimientoCajaChica(int iid, String sdescripcion, int iidPlanilla, int inumeroPlanilla, String splanilla, int iidConcepto, String sconcepto, java.util.Date dfechaOperacion, double umontoDebito, double umontoCredito, double umontoImpuesto, double umontoTotal, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidPlanilla = iidPlanilla;
        this.inumeroPlanilla = inumeroPlanilla;
        this.splanilla = splanilla;
        this.iidConcepto = iidConcepto;
        this.sconcepto = sconcepto;
        this.dfechaOperacion = dfechaOperacion;
        this.umontoDebito = umontoDebito;
        this.umontoCredito = umontoCredito;
        this.umontoImpuesto = umontoImpuesto;
        this.umontoTotal = umontoTotal;
    }

    public void setEntidad(int iid, String sdescripcion, int iidPlanilla, int inumeroPlanilla, String splanilla, int iidConcepto, String sconcepto, java.util.Date dfechaOperacion, double umontoDebito, double umontoCredito, double umontoImpuesto, double umontoTotal, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidPlanilla = iidPlanilla;
        this.inumeroPlanilla = inumeroPlanilla;
        this.splanilla = splanilla;
        this.iidConcepto = iidConcepto;
        this.sconcepto = sconcepto;
        this.dfechaOperacion = dfechaOperacion;
        this.umontoDebito = umontoDebito;
        this.umontoCredito = umontoCredito;
        this.umontoImpuesto = umontoImpuesto;
        this.umontoTotal = umontoTotal;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entMovimientoCajaChica copiar(entMovimientoCajaChica destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdPlanilla(), this.getNumeroPlanilla(), this.getPlanilla(), this.getIdConcepto(), this.getConcepto(), this.getFechaOperacion(), this.getMontoDebito(), this.getMontoCredito(), this.getMontoImpuesto(), this.getMontoTotal(), this.getEstadoRegistro());
        return destino;
    }

    public entMovimientoCajaChica cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdPlanilla(rs.getInt("idplanilla")); }
        catch(Exception e) {}
        try { this.setNumeroPlanilla(rs.getInt("numeroplanilla")); }
        catch(Exception e) {}
        try { this.setPlanilla(rs.getString("planilla")); }
        catch(Exception e) {}
        try { this.setIdConcepto(rs.getInt("idconcepto")); }
        catch(Exception e) {}
        try { this.setConcepto(rs.getString("concepto")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setMontoDebito(rs.getDouble("montodebito")); }
        catch(Exception e) {}
        try { this.setMontoCredito(rs.getDouble("montocredito")); }
        catch(Exception e) {}
        try { this.setMontoImpuesto(rs.getDouble("montoimpuesto")); }
        catch(Exception e) {}
        try { this.setMontoTotal(rs.getDouble("montototal")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdPlanilla(int iidPlanilla) {
        this.iidPlanilla = iidPlanilla;
    }

    public void setNumeroPlanilla(int inumeroPlanilla) {
        this.inumeroPlanilla = inumeroPlanilla;
    }

    public void setPlanilla(String splanilla) {
        this.splanilla = splanilla;
    }

    public void setIdConcepto(int iidConcepto) {
        this.iidConcepto = iidConcepto;
    }

    public void setConcepto(String sconcepto) {
        this.sconcepto = sconcepto;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setMontoDebito(double umontoDebito) {
        this.umontoDebito = umontoDebito;
    }

    public void setMontoCredito(double umontoCredito) {
        this.umontoCredito = umontoCredito;
    }

    public void setMontoImpuesto(double umontoImpuesto) {
        this.umontoImpuesto = umontoImpuesto;
    }

    public void setMontoTotal(double umontoTotal) {
        this.umontoTotal = umontoTotal;
    }

    public int getIdPlanilla() {
        return this.iidPlanilla;
    }

    public int getNumeroPlanilla() {
        return this.inumeroPlanilla;
    }

    public String getPlanilla() {
        return this.splanilla;
    }

    public int getIdConcepto() {
        return this.iidConcepto;
    }

    public String getConcepto() {
        return this.sconcepto;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public double getMontoDebito() {
        return this.umontoDebito;
    }

    public double getMontoCredito() {
        return this.umontoCredito;
    }

    public double getMontoImpuesto() {
        return this.umontoImpuesto;
    }

    public double getMontoTotal() {
        return this.umontoTotal;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdConcepto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CONCEPTO;
            bvalido = false;
        }
        if (this.getFechaOperacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getMontoDebito()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_DEBITO;
            bvalido = false;
        }
        if (this.getMontoCredito()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_CREDITO;
            bvalido = false;
        }
        if ((this.getMontoDebito()>0 && this.getMontoCredito()>0) || (this.getMontoDebito()==0 && this.getMontoCredito()==0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Entre Débido y Crédito, sólo uno debe contener valor";
            bvalido = false;
        }
        if (this.getMontoImpuesto()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_IMPUESTO;
            bvalido = false;
        }
        if (this.getMontoTotal()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_TOTAL;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT movimientocajachica("+
            this.getId()+","+
            this.getIdPlanilla()+","+
            this.getIdConcepto()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaOperacion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getMontoDebito()+","+
            this.getMontoCredito()+","+
            this.getMontoImpuesto()+","+
            this.getMontoTotal()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número Planilla", "numeroplanilla", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Concepto", "concepto", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(3, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
}
