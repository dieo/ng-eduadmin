/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entcArticuloDetalle {
    
    protected int iid;
    protected int iidArticulo;
    protected int iidFilial;
    protected String sdescripcion;
    protected String sfilial;
    protected double ustock;
    protected double uminimo;
    protected double umaximo;
    protected short cestado;
    protected int iidPlanCuenta;
    protected String snroPlanCuenta;
    protected String snroPlanCuenta1;
    protected String splanCuenta;
    protected String smensaje;
        
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FILIAL = "Filial a que pertenece (no vacía)";
    public static final String TEXTO_STOCK = "Existencia del producto (no editable)";
    public static final String TEXTO_MINIMO = "Stock Mínimo del producto para solicitar reposición (no vacío, valor positivo)";
    public static final String TEXTO_MAXIMO = "Stock Máximo del producto para solicitar reposición (no vacío, valor positivo)";
    public static final String TEXTO_PLAN_CUENTA = "Plan de Cuenta para el producto";
    
    public entcArticuloDetalle() {
        this.iid = 0;
        this.iidFilial = 0;
        this.iidArticulo = 0;
        this.sdescripcion = "";
        this.sfilial = "";
        this.ustock = 0.0;
        this.uminimo = 0.0;
        this.umaximo = 0.0;
        this.iidPlanCuenta = 0;
        this.snroPlanCuenta = "";
        this.snroPlanCuenta1 = "";
        this.splanCuenta = "";
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entcArticuloDetalle(int iid, int iidFilial, int iidArticulo, String sdescripcion, String sfilial, double ustock, double uminimo, double umaximo, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta1, String splanCuenta, short cestado) {
        this.iid = iid;
        this.iidFilial = iidFilial;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.sfilial = sfilial;
        this.ustock = ustock;
        this.uminimo = uminimo;
        this.umaximo = umaximo;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta1 = snroPlanCuenta1;
        this.splanCuenta = splanCuenta;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidFilial, int iidArticulo, String sdescripcion, String sfilial, double ustock, double uminimo, double umaximo, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta1, String splanCuenta, short cestado) {
        this.iid = iid;
        this.iidFilial = iidFilial;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.sfilial = sfilial;
        this.ustock = ustock;
        this.uminimo = uminimo;
        this.umaximo = umaximo;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta1 = snroPlanCuenta1;
        this.splanCuenta = splanCuenta;
        this.cestado = cestado;
    }

    public entcArticuloDetalle copiar(entcArticuloDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdFilial(), this.getIdArticulo(), this.getDescripcion(), this.getFilial(), this.getStock(), this.getMinimo(),  this.getMaximo(), this.getIdPlanCuenta(), this.getNroPlanCuenta(), this.getNroPlanCuenta1(), this.getPlanCuenta(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcArticuloDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(SQLException e) { }
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(SQLException e) { }
        try { this.setIdArticulo(rs.getInt("idarticulo")); }
        catch(SQLException e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(SQLException e) { }
        try { this.setFilial(rs.getString("filial")); }
        catch(SQLException e) { }
        try { this.setStock(rs.getDouble("stock")); }
        catch(SQLException e) { }
        try { this.setMinimo(rs.getDouble("minimo")); }
        catch(SQLException e) { }
        try { this.setMaximo(rs.getDouble("maximo")); }
        catch(SQLException e) { }
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) { }
        try { this.setNroPlanCuenta(rs.getString("nroplancuenta1")); }
        catch(Exception e) { }
        try { this.setNroPlanCuenta1(rs.getString("nroplancuenta")); }
        catch(Exception e) { }
        try { this.setPlanCuenta(rs.getString("plancuenta")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }

    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setStock(double ustock) {
        this.ustock = ustock;
    }

    public void setMinimo(double uminimo) {
        this.uminimo = uminimo;
    }

    public void setMaximo(double umaximo) {
        this.umaximo = umaximo;
    }

    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }
    
    public void setNroPlanCuenta(String snroPlanCuenta) {
        this.snroPlanCuenta = snroPlanCuenta;
    }
    
    public void setNroPlanCuenta1(String snroPlanCuenta1) {
        this.snroPlanCuenta1 = snroPlanCuenta1;
    }
    
    public void setPlanCuenta(String splanCuenta) {
        this.splanCuenta = splanCuenta;
    }
    
    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdArticulo() {
        return this.iidArticulo;
    }
   
    public int getIdFilial() {
        return this.iidFilial;
    }
        
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public String getFilial() {
        return this.sfilial;
    }
        
    public double getStock() {
        return this.ustock;
    }

    public double getMinimo() {
        return this.uminimo;
    }

    public double getMaximo() {
        return this.umaximo;
    }

    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }
    
    public String getNroPlanCuenta() {
        return this.snroPlanCuenta;
    }
    
    public String getNroPlanCuenta1() {
        return this.snroPlanCuenta1;
    }
    
    public String getPlanCuenta() {
        return this.splanCuenta;
    }
    
    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if ((this.getMinimo()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MINIMO;
            bvalido = false;
        }
        if ((this.getMaximo()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MAXIMO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.articulodetalle("+
            this.getId()+","+
            this.getIdArticulo()+","+
            this.getIdFilial()+","+
            this.getStock()+","+
            this.getMinimo()+","+
            this.getMaximo()+","+
            this.getIdPlanCuenta()+","+
            this.getEstadoRegistro()+")";
    }

}
