/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Lic. Didier Barreto
 */
public class entcProduccionDetalle {

    protected int iid;
    protected int iidProduccion;
    protected int iidArticulo;
    protected String sdescripcion;
    protected int iidFilial;
    protected String sfilial;
    protected int iidUnidadMedida;
    protected String sunidadMedida;
    protected double ucantidad;
    protected boolean bentregado;
    private short hestadoRegistro;
    protected String smensaje;

    public static final int LONGITUD_DESCRIPCION = 200;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción del producto (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de Medida";
    public static final String TEXTO_ENTREGADO = "Si se entregò el pedido";
    public static final String TEXTO_ARTICULO = "Producto solicitado (no vacía)";
    public static final String TEXTO_CANTIDAD = "Cantidad solicitada del Producto (no vacía, valor positivo)";
    public static final String TEXTO_CUOTAS = "Cantidad de cuotas para abonar por el artículo (valor positivo, 0 en caso de pago al contado)";
    public static final String TEXTO_FRECUENCIA = "Frecuencia para el pago del artículo";
    public static final String TEXTO_FECHAVENC = "Fecha para el primer vencimiento del descuento";

    public entcProduccionDetalle() {
        this.iid = 0;
        this.iidProduccion = 0;
        this.iidUnidadMedida = 0;
        this.sunidadMedida = "";
        this.iidArticulo = 0;
        this.sdescripcion = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.ucantidad = 0.0;
        this.bentregado = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entcProduccionDetalle(int iid, int iidProduccion, int iidUnidadMedida, String sunidadMedida, int iidArticulo, String sdescripcion, int iidFilial, String sfilial, double ucantidad, boolean bentregado, short hestadoRegistro) {
        this.iid = iid;
        this.iidProduccion = iidProduccion;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.ucantidad = ucantidad;
        this.bentregado = bentregado;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidProduccion, int iidUnidadMedida, String sunidadMedida, int iidArticulo, String sdescripcion, int iidFilial, String sfilial, double ucantidad, boolean bentregado, short hestadoRegistro) {
        this.iid = iid;
        this.iidProduccion = iidProduccion;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.ucantidad = ucantidad;
        this.bentregado = bentregado;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entcProduccionDetalle copiar(entcProduccionDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdProduccion(), this.getIdUnidadMedida(), this.getUnidadMedida(), this.getIdArticulo(), this.getDescripcion(), this.getIdFilial(), this.getFilial(), this.getCantidad(), this.getEntregado(), this.getEstadoRegistro());
        return destino;
    }

    public entcProduccionDetalle cargar(java.sql.ResultSet rs) {
        try {
            this.setId(rs.getInt("id"));
        } catch (SQLException e) {
        }
        try {
            this.setIdProduccion(rs.getInt("idproduccion"));
        } catch (SQLException e) {
        }
        try {
            this.setIdUnidadMedida(rs.getInt("idunidadmedida"));
        } catch (SQLException e) {
        }
        try {
            this.setUnidadMedida(rs.getString("unidadmedida"));
        } catch (SQLException e) {
        }
        try {
            this.setIdArticulo(rs.getInt("idarticulo"));
        } catch (SQLException e) {
        }
        try {
            this.setDescripcion(rs.getString("descripcion"));
        } catch (SQLException e) {
        }
        try {
            this.setIdFilial(rs.getInt("idfilial"));
        } catch (SQLException e) {
        }
        try {
            this.setFilial(rs.getString("filial"));
        } catch (SQLException e) {
        }
        try {
            this.setCantidad(rs.getDouble("cantidad"));
        } catch (SQLException e) {
        }
        try {
            this.setEntregado(rs.getBoolean("entregado"));
        } catch (SQLException e) {
        }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdProduccion(int iidProduccion) {
        this.iidProduccion = iidProduccion;
    }

    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public int setIdFilial(int iidFilial) {
        return this.iidFilial = iidFilial;
    }

    public String setFilial(String filial) {
        return this.sfilial = filial;
    }

    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }

    public void setUnidadMedida(String sunidadMedida) {
        this.sunidadMedida = sunidadMedida;
    }

    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setEntregado(boolean bentregado) {
        this.bentregado = bentregado;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdProduccion() {
        return this.iidProduccion;
    }

    public int getIdArticulo() {
        return this.iidArticulo;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }

    public String getFilial() {
        return this.sfilial;
    }

    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }

    public String getUnidadMedida() {
        return this.sunidadMedida;
    }

    public double getCantidad() {
        return this.ucantidad;
    }

    public boolean getEntregado() {
        return this.bentregado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public short getEstadoRegistro() {
        return hestadoRegistro;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdArticulo() < 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_ARTICULO;
            bvalido = false;
        }
        if ((this.getCantidad() < 0)) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_CANTIDAD;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT compras.producciondetalle("
                + this.getId() + ","
                + this.getIdProduccion() + ","
                + this.getIdArticulo() + ","
                + this.getIdFilial() + ","
                + this.getCantidad() + ","
                + this.getEntregado() + ","
                + this.getEstadoRegistro() + ")";
    }

}
