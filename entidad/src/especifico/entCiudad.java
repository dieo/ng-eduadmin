/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entCiudad extends generico.entGenerica {

    protected int iidDepartamento;
    protected String sdepartamento;

    public static final int LONGITUD_DESCRIPCION = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Ciudad (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_DEPARTAMENTO = "Departamento (no vacío)";

    public entCiudad() {
        super();
        this.iidDepartamento = 0;
        this.sdepartamento = "";
    }

    public entCiudad(int iid, String sdescripcion, int iidDepartamento, String sdepartamento) {
        super(iid, sdescripcion);
        this.iidDepartamento = iidDepartamento;
        this.sdepartamento = sdepartamento;
    }

    public entCiudad(int iid, String sdescripcion, int iidDepartamento, String sdepartamento, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidDepartamento = iidDepartamento;
        this.sdepartamento = sdepartamento;
    }

    public void setEntidad(int iid, String sdescripcion, int iidDepartamento, String sdepartamento, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidDepartamento = iidDepartamento;
        this.sdepartamento = sdepartamento;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entCiudad copiar(entCiudad destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdDepartamento(), this.getDepartamento(), this.getEstadoRegistro());
        return destino;
    }

    public entCiudad cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdDepartamento(rs.getInt("iddepartamento")); }
        catch(Exception e) {}
        try { this.setDepartamento(rs.getString("departamento")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdDepartamento(int iidDepartamento) {
        this.iidDepartamento = iidDepartamento;
    }

    public void setDepartamento(String sdepartamento) {
        this.sdepartamento = sdepartamento;
    }

    public int getIdDepartamento() {
        return this.iidDepartamento;
    }

    public String getDepartamento() {
        return this.sdepartamento;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdDepartamento()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DEPARTAMENTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT ciudad("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdDepartamento()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Departamento", "departamento", generico.entLista.tipo_texto));
        return lst;
    }
    
}
