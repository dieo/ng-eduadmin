/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entBeneficiario extends generico.entGenericaPersona {

    protected int iidParentesco;
    protected String sparentesco;
    protected int iidSocio;
    protected String sdireccion;
    protected String stelefono;
    protected int iidBarrio;
    protected String sbarrio;
    protected int iidCiudadResidencia;
    protected String sciudadResidencia;

    public static final int LONGITUD_CEDULA = 12;
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final int LONGITUD_DIRECCION = 100;
    public static final int LONGITUD_TELEFONO = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CEDULA = "Cédula (no vacía, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacío, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_PARENTESCO = "Parentesco (no vacío)";
    public static final String TEXTO_DIRECCION = "Dirección (no vacía, hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_TELEFONO = "Teléfono (no vacío, hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_BARRIO = "Barrio (no vacío)";
    public static final String TEXTO_CIUDAD_RESIDENCIA = "Ciudad de Residencia (no vacía)";

    public entBeneficiario() {
        super();
        this.iidParentesco = 0;
        this.sparentesco = "";
        this.iidSocio = 0;
        this.sdireccion = "";
        this.stelefono = "";
        this.iidBarrio = 0;
        this.sbarrio = "";
        this.iidCiudadResidencia = 0;
        this.sciudadResidencia = "";
    }

    public entBeneficiario(int iid, String snombre, String sapellido, String scedula, int iidParentesco, String sparentesco, int iidSocio, String sdireccion, String stelefono, int iidBarrio, String sbarrio, int iidCiudadResidencia, String sciudadResidencia, short hestadoRegistro) {
        super(iid, snombre, sapellido, scedula, hestadoRegistro);
        this.iidParentesco = iidParentesco;
        this.sparentesco = sparentesco;
        this.iidSocio = iidSocio;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidBarrio = iidBarrio;
        this.sbarrio = sbarrio;
        this.iidCiudadResidencia = iidCiudadResidencia;
        this.sciudadResidencia = sciudadResidencia;
    }
    
    public void setEntidad(int iid, String snombre, String sapellido, String scedula, int iidParentesco, String sparentesco, int iidSocio, String sdireccion, String stelefono, int iidBarrio, String sbarrio, int iidCiudadResidencia, String sciudadResidencia, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre.trim().toUpperCase();
        this.sapellido = sapellido.trim().toUpperCase();
        this.scedula = scedula;
        this.iidParentesco = iidParentesco;
        this.sparentesco = sparentesco;
        this.iidSocio = iidSocio;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidBarrio = iidBarrio;
        this.sbarrio = sbarrio;
        this.iidCiudadResidencia = iidCiudadResidencia;
        this.sciudadResidencia = sciudadResidencia;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entBeneficiario copiar(entBeneficiario destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getIdParentesco(), this.getParentesco(), this.getIdSocio(), this.getDireccion(), this.getTelefono(), this.getIdBarrio(), this.getBarrio(), this.getIdCiudadResidencia(), this.getCiudadResidencia(), this.getEstadoRegistro());
        return destino;
    }

    public entBeneficiario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setIdParentesco(rs.getInt("idparentesco")); }
        catch(Exception e) {}
        try { this.setParentesco(rs.getString("parentesco")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIdBarrio(rs.getInt("idbarrio")); }
        catch(Exception e) {}
        try { this.setBarrio(rs.getString("barrio")); }
        catch(Exception e) {}
        try { this.setIdCiudadResidencia(rs.getInt("idciudadresidencia")); }
        catch(Exception e) {}
        try { this.setCiudadResidencia(rs.getString("ciudadresidencia")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdParentesco(int iidParentesco) {
        this.iidParentesco = iidParentesco;
    }

    public void setParentesco(String sparentesco) {
        this.sparentesco = sparentesco;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setIdBarrio(int iidBarrio) {
        this.iidBarrio = iidBarrio;
    }

    public void setBarrio(String sbarrio) {
        this.sbarrio = sbarrio;
    }

    public void setIdCiudadResidencia(int iidCiudadResidencia) {
        this.iidCiudadResidencia = iidCiudadResidencia;
    }

    public void setCiudadResidencia(String sciudadResidencia) {
        this.sciudadResidencia = sciudadResidencia;
    }

    public int getIdParentesco() {
        return this.iidParentesco;
    }

    public String getParentesco() {
        return this.sparentesco;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefono() {
        return this.stelefono;
    }

    public int getIdBarrio() {
        return this.iidBarrio;
    }

    public String getBarrio() {
        return this.sbarrio;
    }

    public int getIdCiudadResidencia() {
        return this.iidCiudadResidencia;
    }

    public String getCiudadResidencia() {
        return this.sciudadResidencia;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getCedula().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CEDULA;
            bvalido = false;
        }
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_APELLIDO;
            bvalido = false;
        }
        if (this.getIdParentesco() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PARENTESCO;
            bvalido = false;
        }
        if (this.getDireccion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DIRECCION;
            bvalido = false;
        }
        if (this.getTelefono().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO;
            bvalido = false;
        }
        if (this.getIdBarrio() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_BARRIO;
            bvalido = false;
        }
        if (this.getIdCiudadResidencia()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CIUDAD_RESIDENCIA;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT beneficiario("+
            this.getId()+","+
            this.getIdParentesco()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            this.getIdSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getIdBarrio()+","+
            this.getIdCiudadResidencia()+","+
            this.getEstadoRegistro()+")";
    }

}
