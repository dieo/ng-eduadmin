/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entAsesor {
    
    private int iid;
    private String snombre;
    private String sapellido;
    private int iidProfesion;
    private String sprofesion;
    private boolean bactivo;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacío, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_PROFESION = "Profesión (no vacía)";
    public static final String TEXTO_ACTIVO = "Activo";
    
    public entAsesor() {
        this.iid = 0;
        this.snombre = "";
        this.sapellido = "";
        this.iidProfesion = 0;
        this.sprofesion = "";
        this.bactivo = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }
    
    public entAsesor(int iid, String snombre, String sapellido, int iidProfesion, String sprofesion, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.iidProfesion = iidProfesion;
        this.sprofesion = sprofesion;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, String snombre, String sapellido, int iidProfesion, String sprofesion, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.iidProfesion = iidProfesion;
        this.sprofesion = sprofesion;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public entAsesor copiar(entAsesor destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getIdProfesion(), this.getProfesion(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }
    
    public entAsesor cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setIdProfesion(rs.getInt("idprofesion")); }
        catch(Exception e) {}
        try { this.setProfesion(rs.getString("profesion")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setNombre(String snombre) {
        this.snombre = snombre;
    }
    
    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }
    
    public void setIdProfesion(int iidProfesion) {
        this.iidProfesion = iidProfesion;
    }
    
    public void setProfesion(String sprofesion) {
        this.sprofesion = sprofesion;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public int getId() {
        return this.iid;
    }
    
    public String getNombre() {
        return this.snombre;
    }
    
    public String getApellido() {
        return this.sapellido;
    }
    
    public int getIdProfesion() {
        return this.iidProfesion;
    }
    
    public String getProfesion() {
        return this.sprofesion;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }
    
    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_APELLIDO;
            bvalido = false;
        }
        if (this.getIdProfesion()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PROFESION;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT asesor("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            this.getIdProfesion()+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Apellido", "apellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Profesión", "profesion", generico.entLista.tipo_texto));
        return lst;
    }
    
}
