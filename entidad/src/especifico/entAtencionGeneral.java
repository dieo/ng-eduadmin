/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entAtencionGeneral {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String snombreApellido;
    private String sregional;

    private short hestadoRegistro;
    private String smensaje;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_CEDULA = "Cédula (no vacía)";
    public static final String TEXTO_NOMBRE_APELLIDO = "Nombre y Apellido (no vacío)";
    public static final String TEXTO_REGIONAL = "Regional (no editable)";

    public entAtencionGeneral() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.snombreApellido = "";
        this.sregional = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entAtencionGeneral(int iid, int iidSocio, String scedula, String snombreApellido, String sregional, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.sregional = sregional;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, String snombreApellido, String sregional, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.sregional = sregional;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entAtencionGeneral copiar(entAtencionGeneral destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getNombreApellido(), this.getRegional(), this.getEstadoRegistro());
        return destino;
    }
    
    public entAtencionGeneral cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombreApellido(rs.getString("nombreapellido")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombreApellido(String snombreApellido) {
        this.snombreApellido = snombreApellido;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombreApellido() {
        return this.snombreApellido;
    }

    public String getRegional() {
        return this.sregional;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getCedula().isEmpty() || this.getNombreApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getCedula().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CEDULA;
            bvalido = false;
        }
        if (this.getNombreApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE_APELLIDO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT atenciongeneral("+
            this.getId()+","+
            this.getIdSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombreApellido())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Nombre y Apellido", "nombreapellido", generico.entLista.tipo_texto));
        return lst;
    }

}
