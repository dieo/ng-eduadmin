/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entAtencionSocio {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String snombreApellido;
    private int iidObjetoAtencion;
    private String sobjetoAtencion;
    private int iidSolucionAtencion;
    private String ssolucionAtencion;
    private java.util.Date dfechaAtencion;
    private String shoraAtencion;
    private String sobservacion;
    private int iidUsuario;
    private String susuarioNombre;
    private String susuarioApellido;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_OBSERVACION = 250;
    public static final int LONGITUD_NOMBRE_APELLIDO = 80;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_OBJETO_ATENCION = "Objeto de la Atención (no vacío)";
    public static final String TEXTO_SOLUCION_ATENCION = "Solución de la Atención (no vacío)";
    public static final String TEXTO_FECHA_ATENCION = "Fecha de la Atención (no vacía)";
    public static final String TEXTO_HORA_ATENCION = "Hora de la Atención (no vacía)";
    public static final String TEXTO_OBSERVACION = "Observación (no vacía)";
    public static final String TEXTO_USUARIO = "Usuario (no editable)";

    public entAtencionSocio() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.snombreApellido = "";
        this.iidObjetoAtencion = 0;
        this.sobjetoAtencion = "";
        this.iidSolucionAtencion = 0;
        this.ssolucionAtencion = "";
        this.dfechaAtencion = null;
        this.shoraAtencion = "";
        this.sobservacion = "";
        this.iidUsuario = 0;
        this.susuarioNombre = "";
        this.susuarioApellido = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entAtencionSocio(int iid, int iidSocio, String scedula, String snombreApellido, int iidObjetoAtencion, String sobjetoAtencion, int iidSolucionAtencion, String ssolucionAtencion, java.util.Date dfechaAtencion, String shoraAtencion, String sobservacion, int iidUsuario, String susuarioNombre, String susuarioApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidObjetoAtencion = iidObjetoAtencion;
        this.sobjetoAtencion = sobjetoAtencion;
        this.iidSolucionAtencion = iidSolucionAtencion;
        this.ssolucionAtencion = ssolucionAtencion;
        this.dfechaAtencion = dfechaAtencion;
        this.shoraAtencion = shoraAtencion;
        this.sobservacion = sobservacion;
        this.iidUsuario = iidUsuario;
        this.susuarioNombre = susuarioNombre;
        this.susuarioApellido = susuarioApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, String snombreApellido, int iidObjetoAtencion, String sobjetoAtencion, int iidSolucionAtencion, String ssolucionAtencion, java.util.Date dfechaAtencion, String shoraAtencion, String sobservacion, int iidUsuario, String susuarioNombre, String susuarioApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidObjetoAtencion = iidObjetoAtencion;
        this.sobjetoAtencion = sobjetoAtencion;
        this.iidSolucionAtencion = iidSolucionAtencion;
        this.ssolucionAtencion = ssolucionAtencion;
        this.dfechaAtencion = dfechaAtencion;
        this.shoraAtencion = shoraAtencion;
        this.sobservacion = sobservacion;
        this.iidUsuario = iidUsuario;
        this.susuarioNombre = susuarioNombre;
        this.susuarioApellido = susuarioApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entAtencionSocio copiar(entAtencionSocio destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getNombreApellido(), this.getIdObjetoAtencion(), this.getObjetoAtencion(), this.getIdSolucionAtencion(), this.getSolucionAtencion(), this.getFechaAtencion(), this.getHoraAtencion(), this.getObservacion(), this.getIdUsuario(), this.getUsuarioNombre(), this.getUsuarioApellido(), this.getEstadoRegistro());
        return destino;
    }
    
    public entAtencionSocio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombreApellido(rs.getString("nombreapellido")); }
        catch(Exception e) {}
        try { this.setIdObjetoAtencion(rs.getInt("idobjetoatencion")); }
        catch(Exception e) {}
        try { this.setObjetoAtencion(rs.getString("objetoatencion")); }
        catch(Exception e) {}
        try { this.setIdSolucionAtencion(rs.getInt("idsolucionatencion")); }
        catch(Exception e) {}
        try { this.setSolucionAtencion(rs.getString("solucionatencion")); }
        catch(Exception e) {}
        try { this.setFechaAtencion(rs.getDate("fechaatencion")); }
        catch(Exception e) {}
        try { this.setHoraAtencion(rs.getString("horaatencion")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuarioNombre(rs.getString("usuarionombre")); }
        catch(Exception e) {}
        try { this.setUsuarioApellido(rs.getString("usuarioapellido")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombreApellido(String snombreApellido) {
        this.snombreApellido = snombreApellido;
    }

    public void setIdObjetoAtencion(int iidObjetoAtencion) {
        this.iidObjetoAtencion = iidObjetoAtencion;
    }

    public void setObjetoAtencion(String sobjetoAtencion) {
        this.sobjetoAtencion = sobjetoAtencion;
    }

    public void setIdSolucionAtencion(int iidSolucionAtencion) {
        this.iidSolucionAtencion = iidSolucionAtencion;
    }

    public void setSolucionAtencion(String ssolucionAtencion) {
        this.ssolucionAtencion = ssolucionAtencion;
    }

    public void setFechaAtencion(java.util.Date dfechaAtencion) {
        this.dfechaAtencion = dfechaAtencion;
    }

    public void setHoraAtencion(String shoraAtencion) {
        this.shoraAtencion = shoraAtencion;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }

    public void setUsuarioNombre(String susuarioNombre) {
        this.susuarioNombre = susuarioNombre;
    }

    public void setUsuarioApellido(String susuarioApellido) {
        this.susuarioApellido = susuarioApellido;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombreApellido() {
        return this.snombreApellido;
    }

    public int getIdObjetoAtencion() {
        return this.iidObjetoAtencion;
    }

    public String getObjetoAtencion() {
        return this.sobjetoAtencion;
    }

    public int getIdSolucionAtencion() {
        return this.iidSolucionAtencion;
    }

    public String getSolucionAtencion() {
        return this.ssolucionAtencion;
    }

    public java.util.Date getFechaAtencion() {
        return this.dfechaAtencion;
    }

    public String getHoraAtencion() {
        return this.shoraAtencion;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }

    public String getUsuarioNombre() {
        return this.susuarioNombre;
    }

    public String getUsuarioApellido() {
        return this.susuarioApellido;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getCedula().isEmpty() || this.getNombreApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getIdObjetoAtencion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBJETO_ATENCION;
            bvalido = false;
        }
        if (this.getFechaAtencion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_ATENCION;
            bvalido = false;
        }
        if (this.getHoraAtencion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_ATENCION;
            bvalido = false;
        }
        if (this.getIdSolucionAtencion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBJETO_ATENCION;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        if (this.getIdUsuario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_USUARIO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT atencionsocio("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdObjetoAtencion()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAtencion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraAtencion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getIdUsuario()+","+
            this.getIdSolucionAtencion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombreApellido())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Nombre y Apellido", "nombreapellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Objeto Atencion", "objetoatencion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Fecha Atencion", "fechaatencion", generico.entLista.tipo_fecha));
        return lst;
    }

}
