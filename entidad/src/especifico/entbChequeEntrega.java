/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbChequeEntrega {

    private int iid;
    private int inumeroSolicitud;
    private int iidCheque;
    private int iidBancoCuenta;
    private String snumerocuenta;
    private String sbancocuenta;
    private int inumerocheque;
    private java.util.Date dfechaEmisionCheque;
    private java.util.Date dfechaCobro;
    private java.util.Date dfechaEntrega;
    private java.util.Date dfechaDevolucion;
    private java.util.Date dfechaEnvio;
    private int inumeroorden;
    private double dmonto;
    private int iidEstadoCheque;
    private String sestadoCheque;
    private int iidEstadoEntrega;
    private String sestadoEntrega;
    private int iidReceptorCheque;
    protected String scedula;
    protected String scedulaReceptor;
    protected String snombre;
    protected String sreceptorCheque;
    protected String sintermediario;
    private String sobservacion;
    private String sobservacionEnvio;
    protected String sobservacionDevolucion;
    protected String sdescripcion;
    private int iidTalonario;
    private int iidCentroCosto;
    private int iidRemision;
    private int iidPromotor;
    private String scentroCosto;
    private String shora;
    private String sremision;
    private String spromotor;
    private int iidDependencia;
    private String sdependencia;
    private int iidFilial;
    private String sfilial;
    private String stelefono;
    
    private short hestadoRegistro;
    private String smensaje;

    
    public static final int LONGITUD_OBSERVACION = 100;
    public static final int LONGITUD_OBSERVACION_DEVOLUCION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ESTADO_ENTREGA = "Estado de la Entrega (no editable)";
    public static final String TEXTO_ESTADO_CHEQUE = "Estado del Cheque (no editable)";
    public static final String TEXTO_NUMERO_CHEQUE = "Número del Cheque (no editable)";
    public static final String TEXTO_REMISION = "Si es para envio a representante del interior (no editable)";
    public static final String TEXTO_CUENTA_BANCO = "Cuenta Banco (no editable)";
    public static final String TEXTO_MONTO = "Monto del Cheque (no editable)";
    public static final String TEXTO_FECHA_ENTREGA = "Fecha de entrega del Cheque";
    public static final String TEXTO_FECHA_ENVIO = "Fecha de envío del Cheque";
    public static final String TEXTO_FECHA_EMISION = "Fecha de emisión del Cheque (no editable)";
    public static final String TEXTO_PROMOTOR = "Si es via Promotor (no editable)";
    public static final String TEXTO_RECEPTOR = "Nombre del Receptor del Cheque (no editable)";
    public static final String TEXTO_CEDULA_RECEPTOR = "Cédula del Receptor del Cheque (no editable)";
    public static final String TEXTO_NOMBRE = "Nombre de la Persona que retira el Cheque (no vacía)";
    public static final String TEXTO_INTERMEDIARIO = "Nombre de Promotor o representante a quien se envía el Cheque (no vacía)";
    public static final String TEXTO_CEDULA = "Cédula de la Persona que retira el Cheque (no vacía)";
    public static final String TEXTO_CENTRO_COSTO = "Centro de Costo (no editable)";
    public static final String TEXTO_FILIAL = "Filial (no vacía)";
    
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción del concepto (no editable)";
    public static final String TEXTO_OBSERVACION = "Observación (hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_OBSERVACION_DEVOLUCION = "Observación del porqué se devolvió el cheque (no vacía, hasta " + LONGITUD_OBSERVACION_DEVOLUCION + " caracteres)";
    
    
    public entbChequeEntrega() {
        this.iid = 0;
        this.inumeroSolicitud = 0;
        this.iidCheque = 0;
        this.iidBancoCuenta = 0;
        this.snumerocuenta = "";
        this.sbancocuenta = "";
        this.inumerocheque = 0;
        this.dfechaEmisionCheque = null;
        this.dfechaCobro = null;
        this.dfechaEntrega = null;
        this.dfechaDevolucion = null;
        this.dfechaEnvio = null;
        this.inumeroorden = 0;
        this.dmonto = 0.0;
        this.iidEstadoCheque = 0;
        this.sestadoCheque = "";
        this.iidEstadoEntrega = 0;
        this.sestadoEntrega = "";
        this.iidReceptorCheque = 0;
        this.scedula = "";
        this.scedulaReceptor = "";
        this.snombre = "";
        this.sintermediario = "";
        this.sreceptorCheque = "";
        this.iidTalonario = 0;
        this.sobservacion = "";
        this.sobservacionEnvio = "";
        this.sobservacionDevolucion = "";
        this.sdescripcion = "";
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.iidRemision = 0;
        this.sremision = "";
        this.iidPromotor = 0;
        this.spromotor = "";
        this.iidDependencia = 0;
        this.sdependencia = "";
        this.shora = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.stelefono = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbChequeEntrega(int iid, int inumeroSolicitud, int iidCheque, int iidBancoCuenta, String snumerocuenta, String sbancocuenta, int inumerocheque, java.util.Date dfechaEmisionCheque, java.util.Date dfechaCobro, java.util.Date dfechaEntrega, java.util.Date dfechaDevolucion, java.util.Date dfechaEnvio, int inumeroorden, double dmonto, int iidEstadoCheque, String sestadoCheque, int iidEstadoEntrega, String sestadoEntrega, int iidReceptorCheque, String scedulaReceptor, String scedula, String sreceptorCheque, String snombre, String sintermediario, int iidTalonario, String sobservacion, String sobservacionEnvio, String sobservacionDevolucion, String sdescripcion, int iidCentroCosto, String scentroCosto, String shora, int iidRemision, String sremision, int iidPromotor, String spromotor, int iidDependencia, String sdependencia, int iidFilial, String sfilial, String stelefono, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroSolicitud = inumeroSolicitud;
        this.iidCheque = iidCheque;
        this.iidBancoCuenta = iidBancoCuenta;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.inumerocheque = inumerocheque;
        this.dfechaEmisionCheque = dfechaEmisionCheque;
        this.dfechaCobro = dfechaCobro;
        this.dfechaEntrega = dfechaEntrega;
        this.dfechaDevolucion = dfechaDevolucion;
        this.dfechaEnvio = dfechaEnvio;
        this.inumeroorden = inumeroorden;
        this.dmonto = dmonto;
        this.iidEstadoCheque = iidEstadoCheque;
        this.sestadoCheque = sestadoCheque;
        this.iidEstadoEntrega = iidEstadoEntrega;
        this.sestadoEntrega = sestadoEntrega;
        this.iidReceptorCheque = iidReceptorCheque;
        this.scedulaReceptor = scedulaReceptor;
        this.scedula = scedula;
        this.sreceptorCheque = sreceptorCheque;
        this.snombre = snombre;
        this.sintermediario = sintermediario;
        this.iidTalonario = iidTalonario;
        this.sobservacion = sobservacion;
        this.sobservacionEnvio = sobservacionEnvio;
        this.sobservacionDevolucion = sobservacionDevolucion;
        this.sdescripcion = sdescripcion;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidRemision = iidRemision;
        this.sremision = sremision;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.shora = shora;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.stelefono = stelefono;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroSolicitud, int iidCheque, int iidBancoCuenta, String snumerocuenta, String sbancocuenta, int inumerocheque, java.util.Date dfechaEmisionCheque, java.util.Date dfechaCobro, java.util.Date dfechaEntrega, java.util.Date dfechaDevolucion, java.util.Date dfechaEnvio, int inumeroorden, double dmonto, int iidEstadoCheque, String sestadoCheque, int iidEstadoEntrega, String sestadoEntrega, int iidReceptorCheque, String scedulaReceptor, String scedula, String sreceptorCheque, String snombre, String sintermediario, int iidTalonario, String sobservacion, String sobservacionEnvio, String sobservacionDevolucion, String sdescripcion, int iidCentroCosto, String scentroCosto, String shora, int iidRemision, String sremision, int iidPromotor, String spromotor, int iidDependencia, String sdependencia, int iidFilial, String sfilial, String stelefono, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroSolicitud = inumeroSolicitud;
        this.iidCheque = iidCheque;
        this.iidBancoCuenta = iidBancoCuenta;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.inumerocheque = inumerocheque;
        this.dfechaEmisionCheque = dfechaEmisionCheque;
        this.dfechaCobro = dfechaCobro;
        this.dfechaEntrega = dfechaEntrega;
        this.dfechaDevolucion = dfechaDevolucion;
        this.dfechaEnvio = dfechaEnvio;
        this.inumeroorden = inumeroorden;
        this.dmonto = dmonto;
        this.iidEstadoCheque = iidEstadoCheque;
        this.sestadoCheque = sestadoCheque;
        this.iidEstadoEntrega = iidEstadoEntrega;
        this.sestadoEntrega = sestadoEntrega;
        this.iidReceptorCheque = iidReceptorCheque;
        this.scedulaReceptor = scedulaReceptor;
        this.scedula = scedula;
        this.sreceptorCheque = sreceptorCheque;
        this.snombre = snombre;
        this.sintermediario = sintermediario;
        this.iidTalonario = iidTalonario;
        this.sobservacion = sobservacion;
        this.sobservacionEnvio = sobservacionEnvio;
        this.sobservacionDevolucion = sobservacionDevolucion;
        this.sdescripcion = sdescripcion;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidRemision = iidRemision;
        this.sremision = sremision;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.shora = shora;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.stelefono = stelefono;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entbChequeEntrega copiar(entbChequeEntrega destino) {
        destino.setEntidad(this.getId(), this.getNumeroSolicitud(), this.getIdCheque(),this.getIdBancoCuenta(), this.getNumeroCuenta(), this.getBancoCuenta(), this.getNumeroCheque(), this.getFechaEmisionCheque(), this.getFechaCobro(), this.getFechaEntrega(), this.getFechaDevolucion(), this.getFechaEnvio(), this.getNumeroOrden(), this.getMonto(), this.getIdEstadoCheque(), this.getEstadoCheque(), this.getIdEstadoEntrega(), this.getEstadoEntrega(), this.getIdReceptorCheque(), this.getCedulaReceptor(), this.getCedula(), this.getReceptorCheque(), this.getNombre(), this.getIntermediario(), this.getIdTalonario(), this.getObservacion(), this.getObservacionEnvio(), this.getObservacionDevolucion(), this.getDescripcion(), this.getIdCentroCosto(), this.getCentroCosto(), this.getHora(), this.getIdRemision(), this.getRemision(), this.getIdPromotor(), this.getPromotor(), this.getIdDependencia(), this.getDependencia(), this.getIdFilial(), this.getFilial(), this.getTelefono(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbChequeEntrega cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setIdCheque(rs.getInt("idcheque")); }
        catch(Exception e) {}
        try { this.setIdBancoCuenta(rs.getInt("idbancocuenta")); }
        catch(Exception e) {}
        try { this.setNumeroCuenta(rs.getString("numerocuenta")); }
        catch(Exception e) {}
        try { this.setBancoCuenta(rs.getString("cuentabanco")); }
        catch(Exception e) {}
        try { this.setNumeroCheque(rs.getInt("numerocheque")); }
        catch(Exception e) {}
        try { this.setFechaEmisionCheque(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaEnvio(rs.getDate("fechaenvio")); }
        catch(Exception e) {}
        try { this.setFechaEntrega(rs.getDate("fechaentrega")); }
        catch(Exception e) {}
        try { this.setFechaDevolucion(rs.getDate("fechadevolucion")); }
        catch(Exception e) {}
        try { this.setNumeroOrden(rs.getInt("numeroorden")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        try { this.setIdEstadoCheque(rs.getInt("idestadocheque")); }
        catch(Exception e) {}
        try { this.setEstadoCheque(rs.getString("estadocheque")); }
        catch(Exception e) {}
        try { this.setIdEstadoEntrega(rs.getInt("idestadoentrega")); }
        catch(Exception e) {}
        try { this.setEstadoEntrega(rs.getString("estadoentrega")); }
        catch(Exception e) {}
        try { this.setIdReceptorCheque(rs.getInt("idreceptor")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setCedulaReceptor(rs.getString("cedularuc")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIntermediario(rs.getString("intermediario")); }
        catch(Exception e) {}
        try { this.setReceptorCheque(rs.getString("receptor")); }
        catch(Exception e) {}
        try { this.setIdCentroCosto(rs.getInt("idcentrocosto")); }
        catch(Exception e) {}
        try { this.setCentroCosto(rs.getString("centrocosto")); }
        catch(Exception e) {}
        try { this.setIdRemision(rs.getInt("idremision")); }
        catch(Exception e) {}
        try { this.setRemision(rs.getString("remision")); }
        catch(Exception e) {}
        try { this.setIdPromotor(rs.getInt("idpromotor")); }
        catch(Exception e) {}
        try { this.setPromotor(rs.getString("promotor")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        try { this.setIdTalonario(rs.getInt("idtalonario")); }
        catch(Exception e) {}
        try { this.setFechaCobro(rs.getDate("fechacobro")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setObservacionEnvio(rs.getString("observacionenvio")); }
        catch(Exception e) {}
        try { this.setObservacionDevolucion(rs.getString("observaciondevolucion")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setHora(rs.getString("hora")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setIdCheque(int iidCheque) {
        this.iidCheque = iidCheque;
    }

    public void setIdBancoCuenta(int iidBancoCuenta) {
        this.iidBancoCuenta = iidBancoCuenta;
    }

    public void setNumeroCuenta(String snumerocuenta) {
        this.snumerocuenta = snumerocuenta;
    }

    public void setBancoCuenta(String sbancocuenta) {
        this.sbancocuenta = sbancocuenta;
    }

    public void setNumeroCheque(int inumerocheque) {
        this.inumerocheque = inumerocheque;
    }

    public void setFechaEmisionCheque(java.util.Date dfechaEmisionCheque) {
        this.dfechaEmisionCheque = dfechaEmisionCheque;
    }
    
    public void setFechaCobro(java.util.Date dfechaCobro) {
        this.dfechaCobro = dfechaCobro;
    }
    
    public void setFechaEnvio(java.util.Date dfechaEnvio) {
        this.dfechaEnvio = dfechaEnvio;
    }
    
    public void setFechaEntrega(java.util.Date dfechaEntrega) {
        this.dfechaEntrega = dfechaEntrega;
    }
    
    public void setFechaDevolucion(java.util.Date dfechaDevolucion) {
        this.dfechaDevolucion = dfechaDevolucion;
    }
    
    public void setNumeroOrden(int inumeroorden) {
        this.inumeroorden = inumeroorden;
    }

    public void setMonto(double dmonto) {
        this.dmonto = dmonto;
    }
    
    public void setIdEstadoCheque(int iidEstadoCheque) {
        this.iidEstadoCheque = iidEstadoCheque;
    }

    public void setEstadoCheque(String sestadoCheque) {
        this.sestadoCheque = sestadoCheque;
    }
    
    public void setIdEstadoEntrega(int iidEstadoEntrega) {
        this.iidEstadoEntrega = iidEstadoEntrega;
    }

    public void setEstadoEntrega(String sestadoEntrega) {
        this.sestadoEntrega = sestadoEntrega;
    }
    
    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }

    public void setIdReceptorCheque(int iidReceptorCheque) {
        this.iidReceptorCheque = iidReceptorCheque;
    }

    public void setCedulaReceptor(String scedulaReceptor) {
        this.scedulaReceptor = scedulaReceptor;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setReceptorCheque(String sreceptorCheque) {
        this.sreceptorCheque = sreceptorCheque;
    }

    public void setNombre(String snombre) {
        this.snombre = snombre;
    }
    
    public void setIntermediario(String sintermediario) {
        this.sintermediario = sintermediario;
    }
    
    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }
    
    public void setIdTalonario(int iidTalonario) {
        this.iidTalonario = iidTalonario;
    }
    
    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setIdRemision(int iidRemision) {
        this.iidRemision = iidRemision;
    }
    
    public void setRemision(String sremision) {
        this.sremision = sremision;
    }
    
    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }
    
    public void setPromotor(String spromotor) {
        this.spromotor = spromotor;
    }
    
    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }
    
    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public void setHora(String shora) {
        this.shora = shora;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setObservacionEnvio(String sobservacionEnvio) {
        this.sobservacionEnvio = sobservacionEnvio;
    }

    public void setObservacionDevolucion(String sobservacionDevolucion) {
        this.sobservacionDevolucion = sobservacionDevolucion;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }

    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public int getIdCheque() {
        return this.iidCheque;
    }

    public int getIdBancoCuenta() {
        return this.iidBancoCuenta;
    }

    public String getNumeroCuenta() {
        return this.snumerocuenta;
    }

    public String getBancoCuenta() {
        return this.sbancocuenta;
    }
    
    public int getNumeroCheque() {
        return this.inumerocheque;
    }

    public java.util.Date getFechaEmisionCheque() {
        return this.dfechaEmisionCheque;
    }

    public java.util.Date getFechaCobro() {
        return this.dfechaCobro;
    }

    public java.util.Date getFechaEnvio() {
        return this.dfechaEnvio;
    }

    public java.util.Date getFechaEntrega() {
        return this.dfechaEntrega;
    }

    public java.util.Date getFechaDevolucion() {
        return this.dfechaDevolucion;
    }

    public int getNumeroOrden() {
        return this.inumeroorden;
    }

    public double getMonto() {
        return this.dmonto;
    }
    
    public int getIdEstadoCheque() {
        return this.iidEstadoCheque;
    }

    public String getEstadoCheque() {
        return this.sestadoCheque;
    }
    
    public int getIdEstadoEntrega() {
        return this.iidEstadoEntrega;
    }

    public String getEstadoEntrega() {
        return this.sestadoEntrega;
    }
 
    public String getCentroCosto() {
        return this.scentroCosto;
    }
 
    public int getIdReceptorCheque() {
        return this.iidReceptorCheque;
    }

    public String getCedulaReceptor() {
        return this.scedulaReceptor;
    } 

    public String getCedula() {
        return this.scedula;
    } 

    public String getReceptorCheque() {
        return this.sreceptorCheque;
    }

    public String getNombre() {
        return this.snombre;
    }
 
    public String getIntermediario() {
        return this.sintermediario;
    }
 
    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }

    public int getIdTalonario() {
        return this.iidTalonario;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public int getIdRemision() {
        return this.iidRemision;
    }

    public String getRemision() {
        return this.sremision;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public String getPromotor() {
        return this.spromotor;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }

    public String getDependencia() {
        return this.sdependencia;
    }

    public String getHora() {
        return this.shora;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public String getObservacionEnvio() {
        return this.sobservacionEnvio;
    }

    public String getObservacionDevolucion() {
        return this.sobservacionDevolucion;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }

    public String getFilial() {
        return this.sfilial;
    }

    public String getTelefono() {
        return stelefono;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getEstadoCheque().equals("ANULADO")){
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "El cheque fue anulado y no puede ser entregado.";
            bvalido = false;
        }
        if (utilitario.utiFecha.obtenerDiferenciaDia(this.getFechaEmisionCheque(), this.getFechaEntrega())<0){
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La fecha de entrega no puede ser menor a la fecha de emisión del cheque";
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoDevolver() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionDevolucion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_DEVOLUCION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoEnviar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionEnvio().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.entregacheque("+
            this.getId()+","+
            this.getIdCheque()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaEntrega())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            this.getIdEstadoEntrega()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaDevolucion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionDevolucion())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaEnvio())+","+
            utilitario.utiCadena.getTextoGuardado(this.getIntermediario())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionEnvio())+","+
            this.getIdDependencia()+","+
            this.getIdFilial()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha entrega", "fechaentrega", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(2, "Número Cheque", "numerocheque", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(3, "Cuenta Banco", "cuentabanco", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Estado de entrega", "estadoentrega", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Estado del cheque", "estadocheque", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Cédula Receptor", "cedularuc", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(8, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(9, "Receptor", "receptor", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(10, "Concepto ", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(11, "Fecha emisión", "fecha", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(12, "Intermediario", "intermediario", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(13, "Remisión", "remision", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(14, "Promotor", "promotor", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(15, "Centro de Costo", "centrocosto", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(16, "Fecha envío", "fechaenvio", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(17, "Fecha anulación", "fechaanulado", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(18, "Fecha devolución", "fechadevolucion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(19, "Dependencia", "dependencia", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(20, "Filial", "filial", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
