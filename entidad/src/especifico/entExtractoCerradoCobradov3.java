/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entExtractoCerradoCobradov3 {

    private int iidSocio;
    private int iidMovimiento;
    private String speriodo;
    private String sdescripcion;
    private double umontoAtraso;
    private double umontoCerrado;
    private double umontoGiraduria;
    private double umontoVentanilla;
    private double umontoAsignacion;
    private double umontoAplicado;
    private double umontoDiferencia;
    private String srubro;
    private int iestadoRegistro;
    
    public entExtractoCerradoCobradov3() {
        this.iidSocio = 0;
        this.iidMovimiento = 0;
        this.speriodo = "";
        this.sdescripcion = "";
        this.umontoAtraso = 0.0;
        this.umontoCerrado = 0.0;
        this.umontoGiraduria = 0.0;
        this.umontoVentanilla = 0.0;
        this.umontoAsignacion = 0.0;
        this.umontoAplicado = 0.0;
        this.umontoDiferencia = 0.0;
        this.srubro = "";
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entExtractoCerradoCobradov3(int iidSocio, int iidMovimiento, String speriodo, String sdescripcion, double umontoAtraso, double umontoCerrado, double umontoGiraduria, double umontoVentanilla, double umontoAsignacion, double umontoAplicado, double umontoDiferencia, String srubro, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.speriodo = speriodo;
        this.sdescripcion = sdescripcion;
        this.umontoAtraso = umontoAtraso;
        this.umontoCerrado = umontoCerrado;
        this.umontoGiraduria = umontoGiraduria;
        this.umontoVentanilla = umontoVentanilla;
        this.umontoAsignacion = umontoAsignacion;
        this.umontoAplicado = umontoAplicado;
        this.umontoDiferencia = umontoDiferencia;
        this.srubro = srubro;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iidSocio, int iidMovimiento, String speriodo, String sdescripcion, double umontoAtraso, double umontoCerrado, double umontoGiraduria, double umontoVentanilla, double umontoAsignacion, double umontoAplicado, double umontoDiferencia, String srubro, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.speriodo = speriodo;
        this.sdescripcion = sdescripcion;
        this.umontoAtraso = umontoAtraso;
        this.umontoCerrado = umontoCerrado;
        this.umontoGiraduria = umontoGiraduria;
        this.umontoVentanilla = umontoVentanilla;
        this.umontoAsignacion = umontoAsignacion;
        this.umontoAplicado = umontoAplicado;
        this.umontoDiferencia = umontoDiferencia;
        this.srubro = srubro;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entExtractoCerradoCobradov3 copiar(entExtractoCerradoCobradov3 destino) {
        destino.setEntidad(this.getIdSocio(), this.getIdMovimiento(), this.getPeriodo(), this.getDescripcion(), this.getAtraso(), this.getCerrado(), this.getGiraduria(), this.getVentanilla(), this.getAsignacion(), this.getAplicado(), this.getDiferencia(), this.getRubro(), this.getEstadoRegistro());
        return destino;
    }

    public entExtractoCerradoCobradov3 cargar(java.sql.ResultSet rs) {
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setPeriodo(rs.getString("periodo")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setAtraso(rs.getDouble("atraso")); }
        catch(Exception e) {}
        try { this.setCerrado(rs.getDouble("cerrado")); }
        catch(Exception e) {}
        try { this.setGiraduria(rs.getDouble("giraduria")); }
        catch(Exception e) {}
        try { this.setVentanilla(rs.getDouble("ventanilla")); }
        catch(Exception e) {}
        try { this.setAsignacion(rs.getDouble("asignacion")); }
        catch(Exception e) {}
        try { this.setAplicado(rs.getDouble("sob_aplicado")); }
        catch(Exception e) {}
        try { this.setDiferencia(rs.getDouble("atraso")+rs.getDouble("cerrado")-(rs.getDouble("giraduria")+rs.getDouble("ventanilla")+rs.getDouble("sob_aplicado"))); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("tipo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setPeriodo(String speriodo) {
        this.speriodo = speriodo;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setAtraso(double umontoAtraso) {
        this.umontoAtraso = umontoAtraso;
    }

    public void setCerrado(double umontoCerrado) {
        this.umontoCerrado = umontoCerrado;
    }

    public void setGiraduria(double umontoGiraduria) {
        this.umontoGiraduria = umontoGiraduria;
    }

    public void setVentanilla(double umontoVentanilla) {
        this.umontoVentanilla = umontoVentanilla;
    }

    public void setAsignacion(double umontoAsignacion) {
        this.umontoAsignacion = umontoAsignacion;
    }

    public void setAplicado(double umontoAplicado) {
        this.umontoAplicado = umontoAplicado;
    }

    public void setDiferencia(double umontoDiferencia) {
        this.umontoDiferencia = umontoDiferencia;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public String getPeriodo() {
        return this.speriodo;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public Double getAtraso() {
        return this.umontoAtraso;
    }

    public Double getCerrado() {
        return this.umontoCerrado;
    }

    public Double getGiraduria() {
        return this.umontoGiraduria;
    }

    public Double getVentanilla() {
        return this.umontoVentanilla;
    }

    public double getAsignacion() {
        return this.umontoAsignacion;
    }

    public double getAplicado() {
        return this.umontoAplicado;
    }

    public double getDiferencia() {
        return this.umontoDiferencia;
    }

    public String getRubro() {
        return this.srubro;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    public String getConsultaCerradoDetalle(java.util.Date dfechaInicial, java.util.Date dfechaFinal, int iidSocio, String srangoCierre, String srangoAplicado, String srangoCobroGiraduria, String srangoCobro, String srangoVencimiento, String srangoSobrante, String scedula) {
        if (iidSocio==0) {
            iidSocio = -1;
        }
        return 
        "(SELECT (CASE WHEN cie.periodo IS NULL THEN dev.periodo ELSE cie.periodo END) as fecha, " +
        "(CASE WHEN cie.periodo IS NULL THEN to_char(dev.periodo, 'TMMonth')||'/'||to_char(dev.periodo, 'YYYY') ELSE to_char(cie.periodo, 'TMMonth')||'/'||to_char(cie.periodo, 'YYYY') END) as periodo, "+
        "	(CASE WHEN cie.tabla='of' THEN 1 " +
        "	WHEN cie.tabla='js' THEN 2 " +
        "	WHEN cie.tabla='ap' THEN 3 " +
        "	WHEN cie.tabla='mo' THEN 4 " +
        "	WHEN cie.tabla='ce' THEN 6 " +
        "	ELSE 5 END) as orden, " +
        "	(CASE WHEN cie.idcuenta IS NULL THEN dev.idcuenta ELSE cie.idcuenta END) as idcuenta, " +
        "	(CASE WHEN cie.descripcion2 IS NULL THEN dev.descripcion2 ELSE cie.descripcion2 END) as descripcion, " +
        "       (CASE WHEN cie.fechavencimiento IS NULL THEN dev.fechavencimiento ELSE cie.fechavencimiento END) as fechavencimiento, " +
        "	(CASE WHEN cie.idmovimiento IS NULL THEN dev.idmovimiento ELSE cie.idmovimiento END) as idmovimiento, " +
        "	(CASE WHEN cie.tipo IS NULL THEN dev.tipo ELSE cie.tipo END) as tipo, " +
        "	(CASE WHEN cie.atraso IS NULL THEN dev.atraso ELSE cie.atraso END) as atraso, " +
        "	(CASE WHEN cie.cerrado IS NULL THEN dev.cerrado " +
        "	WHEN (cie.cerrado IS NOT NULL and dev.cerrado IS NOT NULL) THEN cie.cerrado+dev.cerrado " +
        "	ELSE cie.cerrado END)::NUMERIC(18) as cerrado, " +
        "	(CASE WHEN gir.giraduria IS NOT NULL THEN gir.giraduria ELSE 0 END)::NUMERIC(18) as giraduria, " +
        "	(CASE WHEN gir.ventanilla IS NOT NULL THEN gir.ventanilla ELSE 0 END)::NUMERIC(18) as ventanilla, " +
        "	(CASE WHEN dev.sob_aplicado IS NOT NULL THEN dev.sob_aplicado ELSE 0 END)::NUMERIC(18) as sob_aplicado, " +
        "	(CASE WHEN gir.asignacion IS NOT NULL THEN gir.asignacion ELSE 0 END)::NUMERIC(18) as asignacion, " +
        "	(CASE WHEN cie.idsocio IS NULL THEN dev.idsocio ELSE cie.idsocio END) as idsocio, " +
        "	(CASE WHEN cie.cedula IS NULL THEN dev.cedula ELSE cie.cedula END) as cedula " +
        "FROM (select ci.fechacierre AS periodo, ci.idcuenta, " +
        "	case when ci.idcuenta = 105 then 'OC ('||ent.descripcion ||') - Op.'||ci.numerooperacion||' - '||TO_CHAR(ci.fechaoperacion,'DD/MM/YY')||' - C/P: '||ci.cuota||'/'||ci.plazo||' - Vto. '||TO_CHAR(ci.fechavencimiento, 'DD/MM/YY') " +        
        "       when ci.idcuenta = 98 then 'FINANCIERA ('||ent.descripcion ||') - Op.'||ci.numerooperacion||' - '||TO_CHAR(ci.fechaoperacion, 'DD/MM/YY')||' - C/P: '||ci.cuota||'/'||ci.plazo||' - Vto. '||TO_CHAR(ci.fechavencimiento, 'DD/MM/YY') " +
        "	else cu.descripcion||' - Op.'||ci.numerooperacion||' - '||coalesce(TO_CHAR(ci.fechaoperacion, 'DD/MM/YY'), TO_CHAR(ci.fechacierre, 'DD/MM/YY'))||' - C/P: '||ci.cuota||'/'||ci.plazo||' - Vto. '||TO_CHAR(ci.fechavencimiento, 'DD/MM/YY') end as descripcion2, " +
        "	ci.fechavencimiento, ci.idmovimiento, ci.tabla, gi.descripcionbreve||' '||ru.descripcionbreve as tipo, " +
        "	cast(sum(case when TO_CHAR(ci.fechavencimiento,'YYYYMM00') < TO_CHAR(ci.fechacierre,'YYYYMM00') " +
        "	then ci.saldo else 0 end) as numeric(12)) ATRASO, " +
        "	cast(sum(case when TO_CHAR(ci.fechavencimiento,'YYYYMM00') = TO_CHAR(ci.fechacierre,'YYYYMM00') " +
        "	then ci.saldo else 0 end) as numeric(12)) CERRADO, " +
        "	0 as giraduria, 0 as ventanilla, 0 as sob_aplicado, 0 as asignacion, ci.cedula, ci.idsocio " +
        "	from cierre as ci " +
        "	left join cuenta as cu on cu.id = ci.idcuenta " +
        "	left join giraduria as gi on gi.id = ci.idgiraduria " +
        "	left join rubro as ru on ru.id = ci.idrubro " +
        "	left join movimiento as mo on mo.id = ci.idmovimiento and mo.idcuenta = ci.idcuenta " +
        "	left join entidad as ent on ent.id = mo.identidad " +
        "	LEFT join tipocredito as tc on tc.id = mo.idtipocredito " +
        "	where 0=0 " +
        "	and ("+srangoCierre+") "+ 
        "	and ci.idsocio = " + iidSocio + " "+
        "	group by ci.idsocio, ci.cedula, ci.fechacierre, ci.idcuenta, ci.fechavencimiento, descripcion2, ci.idmovimiento, ci.tabla, tipo " +
        "	order by ci.fechacierre, ci.cedula, ci.idcuenta) as CIE " +
        "LEFT JOIN (select ci.fechacierre AS periodo, di.idcuenta, ci.fechavencimiento, cu.descripcion, ci.idmovimiento, ci.tabla, 0 AS ATRASO, 0 as CERRADO, " +
        "	cast(sum(case when p.idviacobro = 38 and ("+srangoCobroGiraduria+") " + 
        "	then di.montocobro else 0 end) as numeric(12)) GIRADURIA, " +
        "	cast(sum(case when (p.idviacobro = 39 and dv.idtipovalor <> 10 and dv.idtipovalor <> 9) and ("+srangoCobro+") " + 
        "	then di.montocobro else 0 end) as numeric(12)) VENTANILLA, " +
        "	cast(sum(case when ((p.idviacobro = 39 OR p.idviacobro = 221) and dv.idtipovalor = 9) and ("+srangoCobro+") " + 
	"       then di.montocobro else 0 end) as numeric(12)) ASIGNACION, " +
        "	0 as SOB_APLICADO, i.idsocio, so.cedula " +
        "	from cierre as ci " +
        "	right join detalleingreso as di on di.iddetalleoperacion = ci.iddetalleoperacion and di.idcuenta = ci.idcuenta " +
        "	left join ingreso as i on i.id = di.idingreso " +
        "	left join planillaingreso as p on p.id = i.idplanilla " +
        "	left join detallevalor as dv on dv.idingreso = i.id " +
        "	left join cuenta as cu on di.idcuenta=cu.id " +
        "	left join socio as so on so.id = i.idsocio " +
        "	left join movimiento as mo on mo.id = di.idmovimiento and mo.idcuenta = ci.idcuenta " +
        "	left join entidad as ent on ent.id = mo.identidad " +
        "	LEFT join tipocredito as tc on tc.id = mo.idtipocredito " +
        "	where 0=0 and i.idestado = 146 " +
        "	and ("+srangoCierre+") " +
        "	and ci.idsocio = " + iidSocio + " " +
        "	group by i.idsocio, so.cedula, ci.fechacierre, di.idcuenta, ci.fechavencimiento, cu.descripcion, ci.idmovimiento, ci.tabla " +
        "	order by ci.fechacierre, so.cedula, di.idcuenta) as GIR ON gir.periodo = cie.periodo and gir.idcuenta = cie.idcuenta and cie.idsocio = gir.idsocio and cie.idmovimiento = gir.idmovimiento and cie.fechavencimiento = gir.fechavencimiento " +
        "FULL OUTER JOIN (select di.fechavencimiento AS periodo, cu.id as idcuenta, " +
        "	case when di.idcuenta = 105 then 'OC:'||ent.descripcion ||' - Op.'||di.numerooperacion||' - '||TO_CHAR(di.fechaoperacion,'DD/MM/YY')||' - C/P: '||di.cuota||'/'||di.plazo||' - Vto. '||TO_CHAR(di.fechavencimiento, 'DD/MM/YY')  " +
        "	when di.idcuenta = 98 then 'FI:'||ent.descripcion ||' - Op.'||di.numerooperacion||' - '||TO_CHAR(di.fechaoperacion, 'DD/MM/YY')||' - C/P: '||di.cuota||'/'||di.plazo||' - Vto. '||TO_CHAR(di.fechavencimiento, 'DD/MM/YY')  " +
        "	else cu.descripcion||' - Op.'||di.numerooperacion||' - '||coalesce(TO_CHAR(di.fechaoperacion, 'DD/MM/YY'), TO_CHAR(di.fechavencimiento, 'DD/MM/YY'))||' - C/P: '||di.cuota||'/'||di.plazo||' - Vto. '||TO_CHAR(di.fechavencimiento, 'DD/MM/YY')  end as descripcion2, " +
        "	di.fechavencimiento, di.idmovimiento, 'MC -' as tipo, di.tabla, 0 AS ATRASO, cast(sum(di.montocobro) as numeric(12)) AS CERRADO, 0 as GIRADURIA, 0 as VENTANILLA, " +
        "	cast(sum(di.montocobro) as numeric(12)) AS SOB_APLICADO, 0 as asignacion, so.id as idsocio, so.cedula " +
        "	from detalleingreso as di " +
        "	left join ingreso as ing on ing.id = di.idingreso " +
        "	left join planillaingreso as pi on pi.id = ing.idplanilla " +
        "	left join detallevalor as dv on dv.idingreso = ing.id " +
        "	left join socio as so on so.id = ing.idsocio " +
        "	left join movimiento as mo on mo.id = di.idmovimiento and mo.idcuenta = di.idcuenta " +
        "	left join entidad as ent on ent.id = mo.identidad " +
        "	LEFT join tipocredito as tc on tc.id = mo.idtipocredito " +
        "	left join cuenta as cu on di.idcuenta=cu.id " +
        "	where ing.idestado = 146 and (pi.idviacobro = 39 OR pi.idviacobro = 222) and dv.idtipovalor = 10 " +
        "	AND ("+srangoVencimiento+") " + 
        "	and so.id = " + iidSocio + " " +
        "	group by so.id, so.cedula, periodo, cu.id, di.fechavencimiento, descripcion2, di.idmovimiento, di.tabla, tipo  " +
        "	order by so.cedula, cu.id) as DEV ON TO_CHAR(cie.periodo,'YYYYMM00') = TO_CHAR(dev.periodo,'YYYYMM00') and cie.idcuenta = dev.idcuenta and cie.idsocio = dev.idsocio and dev.idmovimiento = cie.idmovimiento and dev.fechavencimiento = cie.fechavencimiento " +
        "union	(select ag.periodo as fecha, to_char(ag.periodo, 'TMMonth')||'/'||to_char(ag.periodo, 'YYYY') as periodo, 7 as orden, 0 as idcuenta, 'SOBRANTE (APLICAR O REEMBOLSAR)' as descripcion, ag.periodo as fechavencimiento, 0 as idmovimiento, '--' as tipo, 0 as atraso, 0 as cerrado, " +
        "       cast(sum(ag.monto-ag.montoaplicado) as numeric(12)) as GIRADURIA, 0 as ventanilla, 0 as sob_aplicado, 0 as asignacion, so.id, so.cedula " +
        "       from archivogiraduria ag " +
        "       inner join socio so on so.cedula = ag.cedula and so.id = " + iidSocio + " " +
        "       where (ag.monto - ag.montoaplicado > 0) " +
        "       and ("+srangoSobrante+") " +
        "       group by ag.periodo, so.id, so.cedula) " +
        "UNION  (select ci.fechacierre AS fecha, to_char(ci.fechacierre, 'TMMonth')||'/'||to_char(ci.fechacierre, 'YYYY') as periodo, 6 as orden, ci.idcuenta, " +
        "	cu.descripcion||' - Op.'||ci.idmovimiento||' - '||coalesce(TO_CHAR(ci.fechaoperacion, 'DD/MM/YY'), TO_CHAR(ci.fechacierre, 'DD/MM/YY')) as descripcion, " +
        "	ci.fechavencimiento, ci.idmovimiento, gi.descripcionbreve||' '||ru.descripcionbreve as tipo, " +
        "	0 as ATRASO, " +
        "	cast(sum(ci.saldo) as numeric(12)) as CERRADO, " +
        "	sum((CASE WHEN di.montocobro IS NOT NULL THEN di.montocobro ELSE 0 END))::NUMERIC(18) as giraduria, " +
        "	0 as ventanilla, 0 as sob_aplicado, 0 as asignacion, ci.idsocio, ci.cedula " +
        "	FROM cierre as ci " +
        "	left join cuenta as cu on cu.id = ci.idcuenta " +
        "	left join giraduria as gi on gi.id = ci.idgiraduria " +
        "	left join rubro as ru on ru.id = ci.idrubro " +
        "	left join detalleingreso as di on di.iddetalleoperacion = ci.iddetalleoperacion " +
        "	where 0=0 " +
        "	and ("+srangoCierre+") " +
        "	and ci.idsocio = 0 and ci.cedula = '"+scedula+"'" +
        "	group by ci.idsocio, ci.cedula, ci.fechacierre, ci.idcuenta, cu.descripcion, ci.idmovimiento, tipo, ci.fechaoperacion, ci.fechavencimiento " +
        "	order by ci.fechacierre, ci.cedula, ci.idcuenta) " +  
        "ORDER BY fecha, orden, idmovimiento, fechavencimiento, idcuenta)";        
    }
    public String getConsultaCerrado(java.util.Date dfechaInicial, java.util.Date dfechaFinal, int iidSocio, String srangoCierre, String srangoAplicado, String srangoCobroGiraduria, String srangoCobro, String srangoVencimiento, String srangoSobrante, String scedula) {
        if (iidSocio==0) {
            iidSocio = -1;
        }
        return 
        "(SELECT (CASE WHEN cie.periodo IS NULL THEN dev.periodo ELSE cie.periodo END) as fecha, " +
        "(CASE WHEN cie.periodo IS NULL THEN to_char(dev.periodo, 'TMMonth')||'/'||to_char(dev.periodo, 'YYYY') ELSE to_char(cie.periodo, 'TMMonth')||'/'||to_char(cie.periodo, 'YYYY') END) as periodo, "+
        "	(CASE WHEN cie.tabla='of' THEN 1 " +
        "	WHEN cie.tabla='js' THEN 2 " +
        "	WHEN cie.tabla='ap' THEN 3 " +
        "	WHEN cie.tabla='mo' THEN 4 " +
        "	WHEN cie.tabla='ce' THEN 6 " +
        "	ELSE 5 END) as orden, " +
        "	(CASE WHEN cie.idcuenta IS NULL THEN dev.idcuenta ELSE cie.idcuenta END) as idcuenta, " +
        "	(CASE WHEN cie.descripcion2 IS NULL THEN dev.descripcion2 ELSE cie.descripcion2 END) as descripcion, " +
        "	(CASE WHEN cie.idmovimiento IS NULL THEN dev.idmovimiento ELSE cie.idmovimiento END) as idmovimiento, " +
        "	(CASE WHEN cie.tipo IS NULL THEN dev.tipo ELSE cie.tipo END) as tipo, " +
        "	(CASE WHEN cie.atraso IS NULL THEN dev.atraso ELSE cie.atraso END) as atraso, " +
        "	(CASE WHEN cie.cerrado IS NULL THEN dev.cerrado " +
        "	WHEN (cie.cerrado IS NOT NULL and dev.cerrado IS NOT NULL) THEN cie.cerrado+dev.cerrado " +
        "	ELSE cie.cerrado END)::NUMERIC(18) as cerrado, " +
        "	(CASE WHEN gir.giraduria IS NOT NULL THEN gir.giraduria ELSE 0 END)::NUMERIC(18) as giraduria, " +
        "	(CASE WHEN gir.ventanilla IS NOT NULL THEN gir.ventanilla ELSE 0 END)::NUMERIC(18) as ventanilla, " +
        "	(CASE WHEN dev.sob_aplicado IS NOT NULL THEN dev.sob_aplicado ELSE 0 END)::NUMERIC(18) as sob_aplicado, " +
        "	(CASE WHEN gir.asignacion IS NOT NULL THEN gir.asignacion ELSE 0 END)::NUMERIC(18) as asignacion, " +
        "	(CASE WHEN cie.idsocio IS NULL THEN dev.idsocio ELSE cie.idsocio END) as idsocio, " +
        "	(CASE WHEN cie.cedula IS NULL THEN dev.cedula ELSE cie.cedula END) as cedula " +
        "FROM (select ci.fechacierre AS periodo, ci.idcuenta, " +
        "	case when ci.idcuenta = 105 then 'OC ('||ent.descripcion ||') - Op.'||ci.numerooperacion||' - '||TO_CHAR(ci.fechaoperacion,'DD/MM/YY') " +        "	when ci.idcuenta = 98 then 'FINANCIERA ('||ent.descripcion ||') - Op.'||ci.numerooperacion||' - '||TO_CHAR(ci.fechaoperacion, 'DD/MM/YY') " +
        "	else cu.descripcion||' - Op.'||ci.numerooperacion||' - '||coalesce(TO_CHAR(ci.fechaoperacion, 'DD/MM/YY'), TO_CHAR(ci.fechacierre, 'DD/MM/YY')) end as descripcion2, " +
        "	ci.idmovimiento, ci.tabla, gi.descripcionbreve||' '||ru.descripcionbreve as tipo, " +
        "	cast(sum(case when TO_CHAR(ci.fechavencimiento,'YYYYMM00') < TO_CHAR(ci.fechacierre,'YYYYMM00') " +
        "	then ci.saldo else 0 end) as numeric(12)) ATRASO, " +
        "	cast(sum(case when TO_CHAR(ci.fechavencimiento,'YYYYMM00') = TO_CHAR(ci.fechacierre,'YYYYMM00') " +
        "	then ci.saldo else 0 end) as numeric(12)) CERRADO, " +
        "	0 as giraduria, 0 as ventanilla, 0 as sob_aplicado, 0 as asignacion, ci.cedula, ci.idsocio " +
        "	from cierre as ci " +
        "	left join cuenta as cu on cu.id = ci.idcuenta " +
        "	left join giraduria as gi on gi.id = ci.idgiraduria " +
        "	left join rubro as ru on ru.id = ci.idrubro " +
        "	left join movimiento as mo on mo.id = ci.idmovimiento and mo.idcuenta = ci.idcuenta " +
        "	left join entidad as ent on ent.id = mo.identidad " +
        "	LEFT join tipocredito as tc on tc.id = mo.idtipocredito " +
        "	where 0=0 " +
        "	and ("+srangoCierre+") "+ 
        "	and ci.idsocio = " + iidSocio + " "+
        "	group by ci.idsocio, ci.cedula, ci.fechacierre, ci.idcuenta, descripcion2, ci.idmovimiento, ci.tabla, tipo " +
        "	order by ci.fechacierre, ci.cedula, ci.idcuenta) as CIE " +
        "LEFT JOIN (select ci.fechacierre AS periodo, di.idcuenta, cu.descripcion, ci.idmovimiento, ci.tabla, 0 AS ATRASO, 0 as CERRADO, " +
        "	cast(sum(case when p.idviacobro = 38 and ("+srangoCobroGiraduria+") " + 
        "	then di.montocobro else 0 end) as numeric(12)) GIRADURIA, " +
        "	cast(sum(case when (p.idviacobro = 39 and dv.idtipovalor <> 10 and dv.idtipovalor <> 9) and ("+srangoCobro+") " + 
        "	then di.montocobro else 0 end) as numeric(12)) VENTANILLA, " +
        "	cast(sum(case when ((p.idviacobro = 39 OR p.idviacobro = 221) and dv.idtipovalor = 9) and ("+srangoCobro+") " + 
	"       then di.montocobro else 0 end) as numeric(12)) ASIGNACION, " +
        "	0 as SOB_APLICADO, i.idsocio, so.cedula " +
        "	from cierre as ci " +
        "	right join detalleingreso as di on di.iddetalleoperacion = ci.iddetalleoperacion and di.idcuenta = ci.idcuenta " +
        "	left join ingreso as i on i.id = di.idingreso " +
        "	left join planillaingreso as p on p.id = i.idplanilla " +
        "	left join detallevalor as dv on dv.idingreso = i.id " +
        "	left join cuenta as cu on di.idcuenta=cu.id " +
        "	left join socio as so on so.id = i.idsocio " +
        "	left join movimiento as mo on mo.id = di.idmovimiento and mo.idcuenta = ci.idcuenta " +
        "	left join entidad as ent on ent.id = mo.identidad " +
        "	LEFT join tipocredito as tc on tc.id = mo.idtipocredito " +
        "	where 0=0 and i.idestado = 146 " +
        "	and ("+srangoCierre+") " +
        "	and ci.idsocio = " + iidSocio + " " +
        "	group by i.idsocio, so.cedula, ci.fechacierre, di.idcuenta, cu.descripcion, ci.idmovimiento, ci.tabla " +
        "	order by ci.fechacierre, so.cedula, di.idcuenta) as GIR ON gir.periodo = cie.periodo and gir.idcuenta = cie.idcuenta and cie.idsocio = gir.idsocio and cie.idmovimiento = gir.idmovimiento " +
        "FULL OUTER JOIN (select di.fechavencimiento AS periodo, cu.id as idcuenta, " +
        "	case when di.idcuenta = 105 then 'OC:'||ent.descripcion ||' - Op.'||di.numerooperacion||' - '||TO_CHAR(di.fechaoperacion,'DD/MM/YY') " +
        "	when di.idcuenta = 98 then 'FI:'||ent.descripcion ||' - Op.'||di.numerooperacion||' - '||TO_CHAR(di.fechaoperacion, 'DD/MM/YY') " +
        "	else cu.descripcion||' - Op.'||di.numerooperacion||' - '||coalesce(TO_CHAR(di.fechaoperacion, 'DD/MM/YY'), TO_CHAR(di.fechavencimiento, 'DD/MM/YY')) end as descripcion2, " +
        "	di.idmovimiento, 'MC -' as tipo, di.tabla, 0 AS ATRASO, cast(sum(di.montocobro) as numeric(12)) AS CERRADO, 0 as GIRADURIA, 0 as VENTANILLA, " +
        "	cast(sum(di.montocobro) as numeric(12)) AS SOB_APLICADO, 0 as asignacion, so.id as idsocio, so.cedula " +
        "	from detalleingreso as di " +
        "	left join ingreso as ing on ing.id = di.idingreso " +
        "	left join planillaingreso as pi on pi.id = ing.idplanilla " +
        "	left join detallevalor as dv on dv.idingreso = ing.id " +
        "	left join socio as so on so.id = ing.idsocio " +
        "	left join movimiento as mo on mo.id = di.idmovimiento and mo.idcuenta = di.idcuenta " +
        "	left join entidad as ent on ent.id = mo.identidad " +
        "	LEFT join tipocredito as tc on tc.id = mo.idtipocredito " +
        "	left join cuenta as cu on di.idcuenta=cu.id " +
        "	where ing.idestado = 146 and (pi.idviacobro = 39 OR pi.idviacobro = 222) and dv.idtipovalor = 10 " +
        "	AND ("+srangoVencimiento+") " + 
        "	and so.id = " + iidSocio + " " +
        "	group by so.id, so.cedula, di.fechavencimiento, cu.id, descripcion2, di.idmovimiento, di.tabla, tipo  " +
        "	order by so.cedula, cu.id) as DEV ON TO_CHAR(cie.periodo,'YYYYMM00') = TO_CHAR(dev.periodo,'YYYYMM00') and cie.idcuenta = dev.idcuenta and cie.idsocio = dev.idsocio and dev.idmovimiento = cie.idmovimiento " +
        "union	(select i.fechaaaplicar as fecha, to_char(i.fechaaaplicar, 'TMMonth')||'/'||to_char(i.fechaaaplicar, 'YYYY') as periodo, 7 as orden, 0 as idcuenta, 'SOBRANTE (APLICAR O REEMBOLSAR)' as descripcion, 0 as idmovimiento, '--' as tipo, 0 as atraso, 0 as cerrado, " +
        "	cast(sum(i.saldo) as numeric(12)) as GIRADURIA, 0 as ventanilla, 0 as sob_aplicado, 0 as asignacion, i.idsocio, so.cedula " +
        "	from ingreso i " +
        "	left join socio so on so.id = i.idsocio	" +
        "	left join planillaingreso as p on p.id = i.idplanilla " +
        "	where i.idsocio = " + iidSocio + " " +
        "	and i.idestado = 146 and i.saldo > 0 and p.idviacobro = 38 " +
        "	and ("+srangoAplicado+") " + 
        "	group by i.fechaaaplicar, i.idsocio, so.cedula) " +
        "union	(select ag.periodo as fecha, to_char(ag.periodo, 'TMMonth')||'/'||to_char(ag.periodo, 'YYYY') as periodo, 7 as orden, 0 as idcuenta, 'SOBRANTE (APLICAR O REEMBOLSAR)' as descripcion, 0 as idmovimiento, '--' as tipo, 0 as atraso, 0 as cerrado, " +
        "       cast(sum(ag.monto-ag.montoaplicado) as numeric(12)) as GIRADURIA, 0 as ventanilla, 0 as sob_aplicado, 0 as asignacion, so.id, so.cedula " +
        "       from archivogiraduria ag " +
        "       inner join socio so on so.cedula = ag.cedula and so.id = " + iidSocio + " " +
        "       where (ag.monto - ag.montoaplicado > 0) " +
        "       and ("+srangoSobrante+") " +
        "       group by ag.periodo, so.id, so.cedula) " +
        "UNION  (select ci.fechacierre AS fecha, to_char(ci.fechacierre, 'TMMonth')||'/'||to_char(ci.fechacierre, 'YYYY') as periodo, 6 as orden, ci.idcuenta, " +
        "	cu.descripcion||' - Op.'||ci.idmovimiento||' - '||coalesce(TO_CHAR(ci.fechaoperacion, 'DD/MM/YY'), TO_CHAR(ci.fechacierre, 'DD/MM/YY')) as descripcion, " +
        "	ci.idmovimiento, gi.descripcionbreve||' '||ru.descripcionbreve as tipo, " +
        "	0 as ATRASO, " +
        "	cast(sum(ci.saldo) as numeric(12)) as CERRADO, " +
        "	sum((CASE WHEN di.montocobro IS NOT NULL THEN di.montocobro ELSE 0 END))::NUMERIC(18) as giraduria, " +
        "	0 as ventanilla, 0 as sob_aplicado, 0 as asignacion, ci.idsocio, ci.cedula " +
        "	FROM cierre as ci " +
        "	left join cuenta as cu on cu.id = ci.idcuenta " +
        "	left join giraduria as gi on gi.id = ci.idgiraduria " +
        "	left join rubro as ru on ru.id = ci.idrubro " +
        "	left join detalleingreso as di on di.iddetalleoperacion = ci.iddetalleoperacion " +
        "	where 0=0 " +
        "	and ("+srangoCierre+") " +
        "	and ci.idsocio = 0 and ci.cedula = '"+scedula+"'" +
        "	group by ci.idsocio, ci.cedula, ci.fechacierre, ci.idcuenta, cu.descripcion, ci.idmovimiento, tipo, ci.fechaoperacion " +
        "	order by ci.fechacierre, ci.cedula, ci.idcuenta) " +                        
        "ORDER BY fecha, orden, idmovimiento)";        
    }
        
    public String getConsultaCerradoFuncionarioMSP(java.util.Date dfecha, String scedula, String sorden) {
        String speriodo = "PERIODO "+utilitario.utiFecha.getMeses()[dfecha.getMonth()]+"/"+utilitario.utiFecha.getAnho(dfecha);
        return "SELECT ci.idcuenta, '"+speriodo+"' AS periodo, cu.descripcion AS cuenta, gi.descripcionbreve||' '||ru.descripcionbreve AS rubro, ci.idmovimiento, ci.numerooperacion, ci.plazo, ci.tabla, SUM(ci.saldo) AS montocerrado "+
            "FROM cierre AS ci "+
            "LEFT JOIN cuenta AS cu ON ci.idcuenta=cu.id "+
            "LEFT JOIN giraduria AS gi ON ci.idgiraduria=gi.id "+
            "LEFT JOIN rubro AS ru ON ci.idrubro=ru.id "+
            "WHERE ci.fechacierre='"+utilitario.utiFecha.getUltimoDia(dfecha)+"' AND ci.cedula='"+scedula+"' "+
            "GROUP BY ci.idcuenta, cu.descripcion, gi.descripcionbreve, ru.descripcionbreve, ci.idmovimiento, ci.numerooperacion, ci.plazo, ci.tabla "+
            "ORDER BY ci.idcuenta";
    }
    
    public String getConsultaGiraduriaFuncionarioMSP(java.util.Date dfecha, String scedula) {
        return "SELECT di.idcuenta, cu.descripcion AS cuenta, di.idmovimiento, di.numerooperacion, di.tabla, SUM(di.montocobro) AS montogiraduria "+
            "FROM detalleingreso AS di "+
            "LEFT JOIN ingreso AS ing ON di.idingreso=ing.id "+
            "LEFT JOIN cuenta AS cu ON di.idcuenta=cu.id "+
            "LEFT JOIN planillaingreso AS pi ON ing.idplanilla=pi.id "+
            "WHERE ing.idestado=146 AND pi.idviacobro=38 AND ing.ruc='"+scedula+"' AND di.tabla='ce' AND ing.fechaaaplicar='"+utilitario.utiFecha.getUltimoDia(dfecha)+"' "+
            "GROUP BY di.idcuenta, cu.descripcion, di.idmovimiento, di.numerooperacion, di.tabla "+
            "ORDER BY di.idcuenta";
    }
       
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
