/*
 * To change this template, choose Tools | Templa
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entcProduccion {

    private int iid;
    private int iidAutorizador;
    private String sautorizador;
    private java.util.Date dfecha;
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    private int iidEstado;
    private String sestado;
    private String shora;
    private int iidPedido;
    private int iidFuncionario;
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_USUARIO = "Usuario que solicita (no vacía)";
    public static final String TEXTO_AUTORIZADOR = "Persona que autoriza el pedido";
    public static final String TEXTO_FILIAL = "Ubicación del stock del artículo solicitado (no vacía)";
    public static final String TEXTO_FILIAL2 = "Filial (no vacía)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia (no vacía)";
    public static final String TEXTO_FECHA = "Fecha del pedido (no vacía)";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Motivo de la Anulación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_TIPO_PEDIDO = "Tipo de Pedido (no vacía)";
    public static final String TEXTO_HORA = "Hora en que se autorizó el pedido (no editable)";
    public static final String TEXTO_DOCUMENTO = "Nro de Documento generado por pago de uniforme";
    public static final String TEXTO_ESTADO = "Estado del Pedido (no editable)";
    public static final String TEXTO_FUNCIONARIO = "Sólo en caso de pedido de uniformes";
    
    
    public entcProduccion() {
        this.iid = 0;
        this.iidAutorizador = 0;
        this.sautorizador = "";
        this.dfecha = null;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.iidEstado = 0;
        this.sestado = "";
        this.shora = "";
        this.iidPedido = 0;
        this.iidFuncionario = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entcProduccion(int iid, int iidAutorizador, String sautorizador, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidEstado, String sestado, String shora, int iidPedido, int iidFuncionario, short hestadoRegistro) {
        this.iid = iid;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidPedido = iidPedido;
        this.shora = shora;
        this.iidFuncionario = iidFuncionario;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidAutorizador, String sautorizador, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidEstado, String sestado, String shora, int iidPedido, int iidFuncionario, short hestadoRegistro) {
        this.iid = iid;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidPedido = iidPedido;
        this.shora = shora;
        this.iidFuncionario = iidFuncionario;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entcProduccion copiar(entcProduccion destino) {
        destino.setEntidad(this.getId(), this.getIdAutorizador(), this.getAutorizador(), this.getFecha(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getIdEstado(), this.getEstado(), this.getHora(), this.getIdPedido(), this.getIdFuncionario(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcProduccion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdAutorizador(rs.getInt("idautorizador")); }
        catch(Exception e) {}
        try { this.setAutorizador(rs.getString("autorizador")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setIdPedido(rs.getInt("idpedido")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setHora(rs.getString("hora")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdAutorizador(int iidAutorizador) {
        this.iidAutorizador = iidAutorizador;
    }
    
    public void setAutorizador(String sautorizador) {
        this.sautorizador = sautorizador;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }
    
    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }
    
    public void setHora(String shora) {
        this.shora = shora;
    }

    public void setIdPedido(int iidPedido) {
        this.iidPedido = iidPedido;
    }

    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdAutorizador() {
        return this.iidAutorizador;
    }
    
    public String getAutorizador() {
        return this.sautorizador;
    }
    
    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public String getHora() {
        return this.shora;
    }

    public int getIdPedido() {
        return iidPedido;
    }

    public int getIdFuncionario() {
        return iidFuncionario;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }  
    
    public String getSentencia() {
        return "SELECT compras.produccion("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            this.getIdAutorizador()+","+
            this.getIdEstado()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHora())+","+
            this.getIdPedido()+","+
            this.getIdFuncionario()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha", "fecha", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Nº de Pedido", "idpedido", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(3, "Estado", "estado", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
