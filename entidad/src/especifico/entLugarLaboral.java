/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entLugarLaboral {

    protected int iid;
    protected int iidInstitucion;
    protected String sinstitucion;
    protected String sdireccion;
    protected String stelefono;
    protected int iidCargo;
    protected String scargo;
    protected java.util.Date dfechaIngreso;
    protected String santiguedad;
    protected String sdatoAdicional;
    protected int iidSocio;
    protected boolean bactivo;
    protected short hestadoRegistro;
    protected int iidRubro;
    protected String srubro;
    protected int iidRegional;
    protected String sregional;
    protected int iidGiraduria;
    protected String sgiraduria;
    protected double usueldo;
    protected int iidMoneda;
    protected String smoneda;
    protected int inumeroBeneficiario;
    protected String smensaje;
    
    public static final int LONGITUD_DIRECCION = 100;
    public static final int LONGITUD_TELEFONO = 40;
    public static final int LONGITUD_DATO_ADICIONAL = 100;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_INSTITUCION = "Institución (no vacía)";
    public static final String TEXTO_DIRECCION = "Dirección (hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_TELEFONO = "Teléfono (hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_CARGO = "Cargo (no vacío)";
    public static final String TEXTO_FECHA_INGRESO = "Fecha de Ingreso";
    public static final String TEXTO_DATO_ADICIONAL = "Dato Adicional (hasta " + LONGITUD_DATO_ADICIONAL + " caracteres)";
    public static final String TEXTO_ACTIVO = "Pertenece a la Institución (solo el único marcado es válido)";
    public static final String TEXTO_RUBRO = "Rubro (no vacío)";
    public static final String TEXTO_REGIONAL = "Regional (no vacío)";
    public static final String TEXTO_GIRADURIA = "Giraduría (no vacía)";
    public static final String TEXTO_SUELDO = "Sueldo (no vacío, valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_NUMERO_BENEFICIARIO = "Número de Beneficiario (valor positivo)";

    public entLugarLaboral() {
        this.iid = 0;
        this.iidInstitucion = 0;
        this.sinstitucion = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.iidCargo = 0;
        this.scargo = "";
        this.dfechaIngreso = null;
        this.santiguedad = "";
        this.sdatoAdicional = "";
        this.iidSocio = 0;
        this.bactivo = false;
        this.iidRubro = 0;
        this.srubro = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.iidGiraduria = 0;
        this.sgiraduria = "";
        this.usueldo = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.inumeroBeneficiario = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entLugarLaboral(int iid, int iidInstitucion, String sinstitucion, String sdireccion, String stelefono, int iidCargo, String scargo, java.util.Date dfechaIngreso, String santiguedad, String sdatoAdicional, int iidSocio, boolean bactivo, int iidRubro, String srubro, int iidRegional, String sregional, int iidGiraduria, String sgiraduria, double usueldo, int iidMoneda, String smoneda, int inumeroBeneficiario, short hestadoRegistro) {
        this.iid = iid;
        this.iidInstitucion = iidInstitucion;
        this.sinstitucion = sinstitucion;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.dfechaIngreso = dfechaIngreso;
        this.santiguedad = santiguedad;
        this.sdatoAdicional = sdatoAdicional;
        this.iidSocio = iidSocio;
        this.bactivo = bactivo;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.usueldo = usueldo;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.inumeroBeneficiario = inumeroBeneficiario;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidInstitucion, String sinstitucion, String sdireccion, String stelefono, int iidCargo, String scargo, java.util.Date dfechaIngreso, String santiguedad, String sdatoAdicional, int iidSocio, boolean bactivo, int iidRubro, String srubro, int iidRegional, String sregional, int iidGiraduria, String sgiraduria, double usueldo, int iidMoneda, String smoneda, int inumeroBeneficiario, short hestadoRegistro) {
        this.iid = iid;
        this.iidInstitucion = iidInstitucion;
        this.sinstitucion = sinstitucion;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.dfechaIngreso = dfechaIngreso;
        this.santiguedad = santiguedad;
        this.sdatoAdicional = sdatoAdicional;
        this.iidSocio = iidSocio;
        this.bactivo = bactivo;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.usueldo = usueldo;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.inumeroBeneficiario = inumeroBeneficiario;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entLugarLaboral copiar(entLugarLaboral destino) {
        destino.setEntidad(this.getId(), this.getIdInstitucion(), this.getInstitucion(), this.getDireccion(), this.getTelefono(), this.getIdCargo(), this.getCargo(), this.getFechaIngreso(), this.getAntiguedad(), this.getDatoAdicional(), this.getIdSocio(), this.getActivo(), this.getIdRubro(), this.getRubro(), this.getIdRegional(), this.getRegional(), this.getIdGiraduria(), this.getGiraduria(), this.getSueldo(), this.getIdMoneda(), this.getMoneda(), this.getNumeroBeneficiario(), this.getEstadoRegistro());
        return destino;
    }

    public entLugarLaboral cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdInstitucion(rs.getInt("idinstitucion")); }
        catch(Exception e) {}
        try { this.setInstitucion(rs.getString("institucion")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIdCargo(rs.getInt("idcargo")); }
        catch(Exception e) {}
        try { this.setCargo(rs.getString("cargo")); }
        catch(Exception e) {}
        try { this.setFechaIngreso(rs.getDate("fechaingreso")); }
        catch(Exception e) {}
        try { 
            int[] edad = utilitario.utiFecha.obtenerDiferencia(this.getFechaIngreso(), new java.util.Date());
            this.setAntiguedad(edad[0] + "a." + "  " + edad[1] + "m.");
        }
        catch(Exception e) {}
        try { this.setDatoAdicional(rs.getString("datoadicional")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setIdRubro(rs.getInt("idrubro")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        try { this.setIdGiraduria(rs.getInt("idgiraduria"));}
        catch(Exception e) {}
        try { this.setGiraduria(rs.getString("giraduria")); }
        catch(Exception e) {}
        try { this.setSueldo(rs.getDouble("sueldo")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setNumeroBeneficiario(rs.getInt("numerobeneficiario")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdInstitucion(int iidInstitucion) {
        this.iidInstitucion = iidInstitucion;
    }

    public void setInstitucion(String sinstitucion) {
        this.sinstitucion = sinstitucion;
    }

    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setIdCargo(int iidCargo) {
        this.iidCargo = iidCargo;
    }

    public void setCargo(String scargo) {
        this.scargo = scargo;
    }

    public void setFechaIngreso(java.util.Date dfechaIngreso) {
        this.dfechaIngreso = dfechaIngreso;
    }

    public void setAntiguedad(String santiguedad) {
        this.santiguedad = santiguedad;
    }

    public void setDatoAdicional(String sdatoAdicional) {
        this.sdatoAdicional = sdatoAdicional;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setIdRubro(int iidRubro) {
        this.iidRubro = iidRubro;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }

    public void setGiraduria(String sgiraduria) {
        this.sgiraduria = sgiraduria;
    }

    public void setSueldo(double usueldo) {
        this.usueldo = usueldo;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setNumeroBeneficiario(int inumeroBeneficiario) {
        this.inumeroBeneficiario = inumeroBeneficiario;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdInstitucion() {
        return this.iidInstitucion;
    }

    public String getInstitucion() {
        return this.sinstitucion;
    }

    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefono() {
        return this.stelefono;
    }

    public int getIdCargo() {
        return this.iidCargo;
    }

    public String getCargo() {
        return this.scargo;
    }

    public java.util.Date getFechaIngreso() {
        return this.dfechaIngreso;
    }

    public String getAntiguedad() {
        return this.santiguedad;
    }

    public String getDatoAdicional() {
        return this.sdatoAdicional;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public int getIdRubro() {
        return this.iidRubro;
    }

    public String getRubro() {
        return this.srubro;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public String getGiraduria() {
        return this.sgiraduria;
    }

    public double getSueldo() {
        return this.usueldo;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }
    
    public int getNumeroBeneficiario() {
        return this.inumeroBeneficiario;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido(int cantidadActivo) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdInstitucion() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_INSTITUCION;
            bvalido = false;
        }
        if (this.getIdCargo() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CARGO;
            bvalido = false;
        }
        if (this.getIdRegional() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        if (this.getIdRubro() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RUBRO;
            bvalido = false;
        }
        if (this.getIdGiraduria() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_GIRADURIA;
            bvalido = false;
        }
        if (this.getSueldo() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SUELDO;
            bvalido = false;
        }
        if (this.getIdMoneda() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getNumeroBeneficiario() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_BENEFICIARIO;
            bvalido = false;
        }
        if (this.getActivo() && cantidadActivo > 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Ya existe un lugar laboral activo";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT lugarlaboral("+
            this.getId()+","+
            this.getIdInstitucion()+","+
            this.getIdCargo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaIngreso())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDatoAdicional())+","+
            this.getIdSocio()+","+
            this.getActivo()+","+
            this.getIdRubro()+","+
            this.getIdRegional()+","+
            this.getIdGiraduria()+","+
            this.getSueldo()+","+
            this.getIdMoneda()+","+
            this.getNumeroBeneficiario()+","+
            this.getEstadoRegistro()+")";
    }

}
