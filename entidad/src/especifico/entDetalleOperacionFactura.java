/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleOperacionFactura {
    
    private int iid;
    private int iidMovimiento;
    private int iidDetalleOperacion;
    private int iidFacturaIngreso;
    private double umonto;
    protected short hestado;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FUNCIONARIO = "Nombre y Apellido del Funcionario (no vacío)";
    public static final String TEXTO_NIVEL_APROBACION = "Nivel de Aprobación (no vacío)";
    
    public entDetalleOperacionFactura() {
        this.iid = 0;
        this.iidMovimiento = 0;
        this.iidDetalleOperacion = 0;
        this.iidFacturaIngreso = 0;
        this.umonto = 0.0;
        this.hestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }
    
    public entDetalleOperacionFactura(int iid, int iidMovimiento, int iidDetalleOperacion, int iidFacturaIngreso, double umonto, short hestado) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.iidFacturaIngreso = iidFacturaIngreso;
        this.umonto = umonto;
        this.hestado = hestado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidMovimiento, int iidDetalleOperacion, int iidFacturaIngreso, double umonto, short hestado) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.iidFacturaIngreso = iidFacturaIngreso;
        this.umonto = umonto;
        this.hestado = hestado;
        this.smensaje = "";
    }
    
    public entDetalleOperacionFactura copiar(entDetalleOperacionFactura destino) {
        destino.setEntidad(this.getId(), this.getIdMovimiento(), this.getIdDetalleOperacion(), this.getIdFacturaIngreso(), this.getMonto(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleOperacionFactura cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setIdDetalleOperacion(rs.getInt("iddetalleoperacion")); }
        catch(Exception e) {}
        try { this.setIdFacturaIngreso(rs.getInt("idfacturaingreso")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }
    
    public void setIdDetalleOperacion(int iidDetalleOperacion) {
        this.iidDetalleOperacion = iidDetalleOperacion;
    }
    
    public void setIdFacturaIngreso(int iidFacturaIngreso) {
        this.iidFacturaIngreso = iidFacturaIngreso;
    }
    
    public void setMonto(double umonto) {
        this.umonto = umonto;
    }
    
    public void setEstadoRegistro(short hestado) {
        this.hestado = hestado;
    }
    
    public int getId() {
        return this.iid;
    }
    
    public int getIdMovimiento() {
        return this.iidMovimiento;
    }
    
    public int getIdDetalleOperacion() {
        return this.iidDetalleOperacion;
    }
    
    public int getIdFacturaIngreso() {
        return this.iidFacturaIngreso;
    }
    
    public double getMonto() {
        return this.umonto;
    }
    
    public short getEstadoRegistro() {
        return this.hestado;
    }
    
    public String getMensaje() {
        return this.smensaje;
    }

    public String getSentencia() {
        return "SELECT detalleoperacionfactura("+
            this.getId()+","+
            this.getIdMovimiento()+","+
            this.getIdDetalleOperacion()+","+
            this.getIdFacturaIngreso()+","+
            this.getMonto()+","+
            this.getEstadoRegistro()+")";
    }

}
