/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entControlModalidad extends generico.entGenerica{
    
    private int iidFormulario;

    public static final int LONGITUD_DESCRIPCION = 30;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Modalidad (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";

    public entControlModalidad() {
        super();
        this.iidFormulario = 0;
    }

    public entControlModalidad(int iid, String sdescripcion, int iidFormulario, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidFormulario = iidFormulario;
    }
    
    public void setEntidad(int iid, String sdescripcion, int iidFormulario, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidFormulario = iidFormulario;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entControlModalidad copiar(entControlModalidad destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdFormulario(), this.getEstadoRegistro());
        return destino;
    }

    public entControlModalidad cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdFormulario(rs.getInt("idformulario")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdFormulario(int iidFormulario) {
        this.iidFormulario = iidFormulario;
    }

    public int getIdFormulario() {
        return this.iidFormulario;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT modalidad("+
            this.getId()+","+
            this.getIdFormulario()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
}
