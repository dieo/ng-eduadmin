/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleNotaCredito {
    
    private int iid;
    private int iidNota;
    private int iitem;
    private int iidProducto;
    private String sproducto;
    private double ucantidad;
    private double uprecio;
    private double utotal;
    private int iidFactura;
    private String slocal;
    private String spuntoExpedicion;
    private int inumeroFactura;
    private String sdescripcion;
    private int iidOperacion;
    private String stabla;
    private double uimpuesto;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ITEM = "Item (no editable)";
    public static final String TEXTO_PRODUCTO = "Producto (no vacía)";
    public static final String TEXTO_CANTIDAD = "Cantidad (no vacía, valor positivo)";
    public static final String TEXTO_PRECIO = "Precio (no vacío, valor positivo)";
    public static final String TEXTO_FACTURA = "Factura (no vacía)";
    public static final String TEXTO_DESCRIPCION = "Descripción";
    public static final String TEXTO_IMPUESTO = "Impuesto";
    
    public entDetalleNotaCredito() {
        this.iid = 0;
        this.iidNota = 0;
        this.iitem = 0;
        this.iidProducto = 0;
        this.sproducto = "";
        this.ucantidad = 0.0;
        this.uprecio = 0.0;
        this.utotal = 0.0;
        this.iidFactura = 0;
        this.slocal = "";
        this.spuntoExpedicion = "";
        this.inumeroFactura = 0;
        this.sdescripcion = "";
        this.iidOperacion = 0;
        this.stabla = "";
        this.uimpuesto = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleNotaCredito(int iid, int iidNota, int iitem, int iidProducto, String sproducto, double ucantidad, double uprecio, double utotal, int iidFactura, String slocal, String spuntoExpedicion, int inumeroFactura, String sdescripcion, int iidOperacion, String stabla, double uimpuesto, short hestadoRegistro) {
        this.iid = iid;
        this.iidNota = iidNota;
        this.iitem = iitem;
        this.iidProducto = iidProducto;
        this.sproducto = sproducto;
        this.ucantidad = ucantidad;
        this.uprecio = uprecio;
        this.utotal = utotal;
        this.iidFactura = iidFactura;
        this.slocal = slocal;
        this.spuntoExpedicion = spuntoExpedicion;
        this.inumeroFactura = inumeroFactura;
        this.sdescripcion = sdescripcion;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.uimpuesto = uimpuesto;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidNota, int iitem, int iidProducto, String sproducto, double ucantidad, double uprecio, double utotal, int iidFactura, String slocal, String spuntoExpedicion, int inumeroFactura, String sdescripcion, int iidOperacion, String stabla, double uimpuesto, short hestadoRegistro) {
        this.iid = iid;
        this.iidNota = iidNota;
        this.iitem = iitem;
        this.iidProducto = iidProducto;
        this.sproducto = sproducto;
        this.ucantidad = ucantidad;
        this.uprecio = uprecio;
        this.utotal = utotal;
        this.iidFactura = iidFactura;
        this.slocal = slocal;
        this.spuntoExpedicion = spuntoExpedicion;
        this.inumeroFactura = inumeroFactura;
        this.sdescripcion = sdescripcion;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.uimpuesto = uimpuesto;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleNotaCredito copiar(entDetalleNotaCredito destino) {
        destino.setEntidad(this.getId(), this.getIdNota(), this.getItem(), this.getIdProducto(), this.getProducto(), this.getCantidad(), this.getPrecio(), this.getTotal(), this.getIdFactura(), this.getLocal(), this.getPuntoExpedicion(), this.getNumeroFactura(), this.getDescripcion(), this.getIdOperacion(), this.getTabla(), this.getImpuesto(), this.getEstadoRegistro());
        return destino;
    }
    
    public entDetalleNotaCredito cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setIdNota(rs.getInt("idnota")); }
        catch(Exception e) { }
        try { this.setItem(rs.getInt("item")); }
        catch(Exception e) { }
        try { this.setIdProducto(rs.getInt("idproducto")); }
        catch(Exception e) { }
        try { this.setProducto(rs.getString("producto")); }
        catch(Exception e) { }
        try { this.setCantidad(rs.getDouble("cantidad")); }
        catch(Exception e) { }
        try { this.setPrecio(rs.getDouble("precio")); }
        catch(Exception e) { }
        try { this.setTotal(rs.getDouble("precio")*rs.getDouble("cantidad")); }
        catch(Exception e) { }
        try { this.setIdFactura(rs.getInt("idfactura")); }
        catch(Exception e) { }
        try { this.setLocal(rs.getString("local")); }
        catch(Exception e) { }
        try { this.setPuntoExpedicion(rs.getString("puntoexpedicion")); }
        catch(Exception e) { }
        try { this.setNumeroFactura(rs.getInt("numerofactura")); }
        catch(Exception e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) { }
        try { this.setIdOperacion(rs.getInt("idoperacion")); }
        catch(Exception e) { }
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) { }
        try { this.setImpuesto(rs.getDouble("impuesto")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdNota(int iidNota) {
        this.iidNota = iidNota;
    }
    
    public void setItem(int iitem) {
        this.iitem = iitem;
    }
    
    public void setIdProducto(int iidProducto) {
        this.iidProducto = iidProducto;
    }
    
    public void setProducto(String sproducto) {
        this.sproducto = sproducto;
    }
    
    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }
    
    public void setPrecio(double uprecio) {
        this.uprecio = uprecio;
    }

    public void setTotal(double utotal) {
        this.utotal = utotal;
    }

    public void setIdFactura(int iidFactura) {
        this.iidFactura = iidFactura;
    }
    
    public void setLocal(String slocal) {
        this.slocal = slocal;
    }
    
    public void setPuntoExpedicion(String spuntoExpedicion) {
        this.spuntoExpedicion = spuntoExpedicion;
    }
    
    public void setNumeroFactura(int inumeroFactura) {
        this.inumeroFactura = inumeroFactura;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setIdOperacion(int iidOperacion) {
        this.iidOperacion = iidOperacion;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setImpuesto(double uimpuesto) {
        this.uimpuesto = uimpuesto;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getIdNota() {
        return this.iidNota;
    }
    
    public int getItem() {
        return this.iitem;
    }
    
    public int getIdProducto() {
        return this.iidProducto;
    }
    
    public String getProducto() {
        return this.sproducto;
    }
    
    public double getCantidad() {
        return this.ucantidad;
    }
    
    public double getPrecio() {
        return this.uprecio;
    }
    
    public double getTotal() {
        return this.utotal;
    }
    
    public int getIdFactura() {
        return this.iidFactura;
    }
    
    public String getLocal() {
        return this.slocal;
    }
    
    public String getPuntoExpedicion() {
        return this.spuntoExpedicion;
    }
    
    public int getNumeroFactura() {
        return this.inumeroFactura;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public int getIdOperacion() {
        return this.iidOperacion;
    }

    public String getTabla() {
        return this.stabla;
    }

    public double getImpuesto() {
        return this.uimpuesto;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdProducto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PRODUCTO;
            bvalido = false;
        }
        if (this.getCantidad()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CANTIDAD;
            bvalido = false;
        }
        if (this.getPrecio()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PRECIO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT detallenotacredito("+
            this.getId()+","+
            this.getIdNota()+","+
            this.getItem()+","+
            this.getIdProducto()+","+
            this.getCantidad()+","+
            this.getPrecio()+","+
            this.getIdFactura()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdOperacion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getImpuesto()+","+
            this.getEstadoRegistro()+")";
    }

}
