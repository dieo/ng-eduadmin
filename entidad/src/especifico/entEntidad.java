/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entEntidad extends generico.entGenerica {
    
    protected String sdireccion;
    protected String sruc;
    protected String stelefono;
    protected int iidCiudad;
    protected String sciudad;
    protected String scontacto;
    protected String spromotorAutorizado;
    protected String sweb;
    protected String semail;
    protected boolean bactivo;
    protected int iidTipoEntidad;
    protected String stipoEntidad;
    protected int iidRegional;
    protected String sregional;
    protected int iidRamo;
    protected String sramo;
    protected int iplazoMaximo;
    protected double dbonificacion;
    protected double dtasaInteres;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final int LONGITUD_RUC = 12;
    public static final int LONGITUD_DIRECCION = 40;
    public static final int LONGITUD_TELEFONO = 40;
    public static final int LONGITUD_CONTACTO = 40;
    public static final int LONGITUD_PROMOTOR_AUTORIZADO = 100;
    public static final int LONGITUD_WEB = 30;
    public static final int LONGITUD_EMAIL = 50;
    
    public static final int INICIO_PLAZO_MAXIMO = 0;
    public static final int FIN_PLAZO_MAXIMO = 999;
    public static final int INICIO_BONIFICACION = 0;
    public static final int FIN_BONIFICACION = 100;
    public static final int INICIO_TASA_INTERES = 0;
    public static final int FIN_TASA_INTERES = 100;
        
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_RUC = "RUC de Entidad (hasta " + LONGITUD_RUC + " caracteres)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Entidad (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres, sin la secuencia '_/_')";
    public static final String TEXTO_DIRECCION = "Dirección (hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_TELEFONO = "Teléfono (hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_CIUDAD = "Ciudad (no vacía)";
    public static final String TEXTO_CONTACTO = "Contacto con Entidad (hasta " + LONGITUD_CONTACTO + " caracteres)";
    public static final String TEXTO_PROMOTOR_AUTORIZADO = "Promotor Autorizado (hasta " + LONGITUD_PROMOTOR_AUTORIZADO + " caracteres)";
    public static final String TEXTO_WEB = "Página web de Entidad (hasta " + LONGITUD_WEB + " caracteres)";
    public static final String TEXTO_EMAIL = "E-mail de Entidad (hasta " + LONGITUD_EMAIL + " caracteres)";
    public static final String TEXTO_TIPO_ENTIDAD = "Tipo de Entidad (no vacío)";
    public static final String TEXTO_REGIONAL = "Regional de Entidad (no vacío)";
    public static final String TEXTO_ACTIVO = "Activo";
    public static final String TEXTO_RAMO = "Ramo (no vacío)";
    public static final String TEXTO_PLAZOMAXIMO = "Plazo Máximo que otorga Entidad (no vacío, valor entero desde " + INICIO_PLAZO_MAXIMO + " hasta " + FIN_PLAZO_MAXIMO + ")";
    public static final String TEXTO_BONIFICACION = "Porcentaje de Bonificación de Entidad (no vacío, desde " + INICIO_BONIFICACION + " hasta " + FIN_BONIFICACION + ", con hasta 2 decimales)";
    public static final String TEXTO_TASAINTERES = "Tasa vigente de Entidad (no vacía, desde "  + INICIO_TASA_INTERES + " hasta " + FIN_TASA_INTERES + ", con hasta 2 decimales)";
    
    public entEntidad() {
        super();
        this.sruc = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.iidCiudad = 0;
        this.sciudad = "";
        this.scontacto = "";
        this.spromotorAutorizado = "";
        this.sweb = "";
        this.semail = "";
        this.iidTipoEntidad = 0;
        this.stipoEntidad = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.bactivo = false;
        this.iidRamo = 0;
        this.sramo = "";
        this.iplazoMaximo = 0;
        this.dbonificacion = 0.0;
        this.dtasaInteres = 0.0;
    }

    public entEntidad(int iid, String sdescripcion, String sdireccion, String stelefono, int iidTipoEntidad, String stipoEntidad, int iidRegional, String sregional, String sruc, int iidCiudad, String sciudad, String scontacto, String spromotorAutorizado, String sweb, String semail, boolean bactivo, int iidRamo, String sramo, int iplazoMaximo, double dbonificacion, double dtasaInteres, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.sruc = sruc;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.scontacto = scontacto;
        this.spromotorAutorizado = spromotorAutorizado;
        this.sweb = sweb;
        this.semail = semail;
        this.iidTipoEntidad = iidTipoEntidad;
        this.stipoEntidad = stipoEntidad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.bactivo = bactivo;
        this.iidRamo = iidRamo;
        this.sramo = sramo;
        this.iplazoMaximo = iplazoMaximo;
        this.dbonificacion = dbonificacion;
        this.dtasaInteres = dtasaInteres;
    }
    
     public void setEntidad(int iid, String sdescripcion, String sdireccion, String stelefono, int iidTipoEntidad, String stipoEntidad, int iidRegional, String sregional, String sruc, int iidCiudad, String sciudad, String scontacto, String spromotorAutorizado, String sweb, String semail, boolean bactivo, int iidRamo, String sramo, int iplazoMaximo, double dbonificacion, double dtasaInteres, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.sruc = sruc;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.scontacto = scontacto;
        this.spromotorAutorizado = spromotorAutorizado;
        this.sweb = sweb;
        this.semail = semail;
        this.iidTipoEntidad = iidTipoEntidad;
        this.stipoEntidad = stipoEntidad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.bactivo = bactivo;
        this.iidRamo = iidRamo;
        this.sramo = sramo;
        this.iplazoMaximo = iplazoMaximo;
        this.dbonificacion = dbonificacion;
        this.dtasaInteres = dtasaInteres;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entEntidad copiar(entEntidad destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getDireccion(), this.getTelefono(), this.getIdTipoEntidad(), this.getTipoEntidad(), this.getIdRegional(), this.getRegional(), this.getRuc(), this.getIdCiudad(), this.getCiudad(), this.getContacto(), this.getPromotorAutorizado(), this.getWeb(), this.getEmail(), this.getActivo(), this.getIdRamo(), this.getRamo(), this.getPlazoMaximo(), this.getBonificacion(), this.getTasaInteres(), this.getEstadoRegistro());
        return destino;
    }
    
    public entEntidad cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIdCiudad(rs.getInt("idciudad")); }
        catch(Exception e) {}
        try { this.setCiudad(rs.getString("ciudad")); }
        catch(Exception e) {}
        try { this.setContacto(rs.getString("contacto")); }
        catch(Exception e) {}
        try { this.setPromotorAutorizado(rs.getString("promotorautorizado")); }
        catch(Exception e) {}
        try { this.setWeb(rs.getString("web")); }
        catch(Exception e) {}
        try { this.setEmail(rs.getString("email")); }
        catch(Exception e) {}
        try { this.setIdTipoEntidad(rs.getInt("idtipoentidad")); }
        catch(Exception e) {}
        try { this.setTipoEntidad(rs.getString("tipoentidad")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setIdRamo(rs.getInt("idramo")); }
        catch(Exception e) {}
        try { this.setRamo(rs.getString("ramo")); }
        catch(Exception e) {}
        try { this.setPlazoMaximo(rs.getInt("plazomaximo")); }
        catch(Exception e) {}
        try { this.setBonificacion(rs.getDouble("bonificacion")); }
        catch(Exception e) {}
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setRuc(String sruc) {
        this.sruc = sruc;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setIdCiudad(int iidCiudad) {
        this.iidCiudad = iidCiudad;
    }
    
    public void setCiudad(String sciudad) {
        this.sciudad = sciudad;
    }
    
    public void setContacto(String scontacto) {
        this.scontacto = scontacto;
    }
    
    public void setPromotorAutorizado(String spromotorAutorizado) {
        this.spromotorAutorizado = spromotorAutorizado;
    }
    
    public void setWeb(String sweb) {
        this.sweb = sweb;
    }
    
    public void setEmail(String semail) {
        this.semail = semail;
    }
    
    public void setIdTipoEntidad(int iidTipoEntidad) {
        this.iidTipoEntidad = iidTipoEntidad;
    }

    public void setTipoEntidad(String stipoEntidad) {
        this.stipoEntidad = stipoEntidad;
    }
    
    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setIdRamo(int iidRamo) {
        this.iidRamo = iidRamo;
    }
    
    public void setRamo(String sramo) {
        this.sramo = sramo;
    }
    
    public void setPlazoMaximo(int iplazoMaximo) {
        this.iplazoMaximo = iplazoMaximo;
    }
    
    public void setBonificacion(double dbonificacion) {
        this.dbonificacion = dbonificacion;
    }
    
    public void setTasaInteres(double dtasaInteres) {
        this.dtasaInteres = dtasaInteres;
    }

    public String getRuc() {
        return this.sruc;
    }
    
    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefono() {
        return this.stelefono;
    }
    
    public int getIdCiudad() {
        return this.iidCiudad;
    }
    
    public String getCiudad() {
        return this.sciudad;
    }
    
    public String getContacto() {
        return this.scontacto;
    }
    
    public String getPromotorAutorizado() {
        return this.spromotorAutorizado;
    }
    
    public String getWeb() {
        return this.sweb;
    }
    
    public String getEmail() {
        return this.semail;
    }

    public int getIdTipoEntidad() {
        return this.iidTipoEntidad;
    }

    public String getTipoEntidad() {
        return this.stipoEntidad;
    }
    
    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }
    
    public int getIdRamo() {
        return this.iidRamo;
    }
    
    public String getRamo() {
        return this.sramo;
    }
    
    public int getPlazoMaximo() {
        return this.iplazoMaximo;
    }
    
    public double getBonificacion() {
        return this.dbonificacion;
    }
    
    public double getTasaInteres() {
        return this.dtasaInteres;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty() || this.getDescripcion().contains(" / ")) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdTipoEntidad() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_ENTIDAD;
            bvalido = false;
        }
        if (this.getIdCiudad() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CIUDAD;
            bvalido = false;
        }
        if (this.getIdRamo() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RAMO;
            bvalido = false;
        }
        if (!(this.getPlazoMaximo()>=INICIO_PLAZO_MAXIMO && this.getPlazoMaximo()<=FIN_PLAZO_MAXIMO)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZOMAXIMO;
            bvalido = false;
        }
        if (!(this.getBonificacion()>=INICIO_BONIFICACION && this.getBonificacion()<=FIN_BONIFICACION)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_BONIFICACION;
            bvalido = false;
        }
        if (!(this.getTasaInteres()>=INICIO_TASA_INTERES && this.getTasaInteres()<=FIN_TASA_INTERES)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TASAINTERES;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT entidad("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getIdTipoEntidad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getRuc())+","+
            this.getIdCiudad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getContacto())+","+
            utilitario.utiCadena.getTextoGuardado(this.getWeb())+","+
            utilitario.utiCadena.getTextoGuardado(this.getEmail())+","+
            this.getActivo()+","+
            this.getIdRamo()+","+
            this.getPlazoMaximo()+","+
            this.getBonificacion()+","+
            this.getTasaInteres()+","+
            utilitario.utiCadena.getTextoGuardado(this.getPromotorAutorizado())+","+
            this.getIdRegional()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Ruc", "ruc", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Tipo Entidad", "tipoentidad", generico.entLista.tipo_texto));
        return lst;
    }
    
}
