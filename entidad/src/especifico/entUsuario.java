/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entUsuario {
    
    private int iid;
    private int iidFuncionario;
    private String sfuncionario;
    private String susuario;
    private String scontrasena;
    private String sconfirmacion;
    private int iidNivelUsuario;
    private String snivelUsuario;
    private int iidPerfilUsuario;
    private String sperfilUsuario;
    private int iidFilial;
    private String sfilial;
    private boolean bactivo;
    private short hestado;
    private String smensaje;
    
    private String susuarioAnterior;
    
    public static final int LONGITUD_USUARIO = 25;
    public static final int LONGITUD_CONTRASENA = 25;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FUNCIONARIO = "Apellido y Nombre del Funcionario (no vacío)";
    public static final String TEXTO_USUARIO = "Usuario (no vacío, hasta " + LONGITUD_USUARIO + " caracteres)";
    public static final String TEXTO_CONTRASENA = "Contraseña (no vacía, hasta " + LONGITUD_CONTRASENA + " caracteres)";
    public static final String TEXTO_CONFIRMACION = "Confirmación de Contraseña (no vacía, hasta " + LONGITUD_CONTRASENA + " caracteres)";
    public static final String TEXTO_NIVEL_USUARIO = "Nivel de Usuario (no vacío)";
    public static final String TEXTO_PERFIL_USUARIO = "Perfil de Usuario";
    public static final String TEXTO_FILIAL = "Filial de Usuario";
    public static final String TEXTO_ACTIVO = "Activo";
    
    public entUsuario() {
        this.iid = 0;
        this.iidFuncionario = 0;
        this.sfuncionario = "";
        this.susuario = "";
        this.scontrasena = "";
        this.sconfirmacion = "";
        this.iidNivelUsuario = 0;
        this.snivelUsuario = "";
        this.iidPerfilUsuario = 0;
        this.sperfilUsuario = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.bactivo = false;
        this.hestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
        this.susuarioAnterior = "";
    }
    
    public entUsuario(int iid, int iidFuncionario, String sfuncionario, String susuario, String scontrasena, String sconfirmacion, int iidNivelUsuario, String snivelUsuario, int iidPerfilUsuario, String sperfilUsuario, int iidFilial, String sfilial, boolean bactivo, String susuarioAnterior, short hestado) {
        this.iid = iid;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.susuario = susuario;
        this.scontrasena = scontrasena;
        this.sconfirmacion = sconfirmacion;
        this.iidNivelUsuario = iidNivelUsuario;
        this.snivelUsuario = snivelUsuario;
        this.iidPerfilUsuario = iidPerfilUsuario;
        this.sperfilUsuario = sperfilUsuario;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.bactivo = bactivo;
        this.hestado = hestado;
        this.smensaje = "";
        this.susuarioAnterior = susuarioAnterior;
    }
    
    public void setEntidad(int iid, int iidFuncionario, String sfuncionario, String susuario, String scontrasena, String sconfirmacion, int iidNivelUsuario, String snivelUsuario, int iidPerfilUsuario, String sperfilUsuario, int iidFilial, String sfilial, boolean bactivo, String susuarioAnterior, short hestado) {
        this.iid = iid;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.susuario = susuario;
        this.scontrasena = scontrasena;
        this.sconfirmacion = sconfirmacion;
        this.iidNivelUsuario = iidNivelUsuario;
        this.snivelUsuario = snivelUsuario;
        this.iidPerfilUsuario = iidPerfilUsuario;
        this.sperfilUsuario = sperfilUsuario;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.bactivo = bactivo;
        this.hestado = hestado;
        this.smensaje = "";
        this.susuarioAnterior = susuarioAnterior;
    }
    
    public entUsuario copiar(entUsuario destino) {
        destino.setEntidad(this.getId(), this.getIdFuncionario(), this.getFuncionario(), this.getUsuario(), this.getContrasena(), this.getConfirmacion(), this.getIdNivelUsuario(), this.getNivelUsuario(), this.getIdPerfilUsuario(), this.getPerfilUsuario(), this.getIdFilial(), this.getFilial(), this.getActivo(), this.getUsuarioAnterior(), this.getEstadoRegistro());
        return destino;
    }
    
    public entUsuario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        try { this.setFuncionario(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setUsuario(rs.getString("usuario")); }
        catch(Exception e) {}
        try { this.setContrasena(rs.getString("contrasena")); }
        catch(Exception e) {}
        try { this.setConfirmacion(rs.getString("contrasena")); }
        catch(Exception e) {}
        try { this.setIdNivelUsuario(rs.getInt("idnivelusuario")); }
        catch(Exception e) {}
        try { this.setNivelUsuario(rs.getString("nivelusuario")); }
        catch(Exception e) {}
        try { this.setIdPerfilUsuario(rs.getInt("idperfilusuario")); }
        catch(Exception e) {}
        try { this.setPerfilUsuario(rs.getString("perfilusuario")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }
    
    public void setFuncionario(String sfuncionario) {
        this.sfuncionario = sfuncionario;
    }
    
    public void setUsuario(String susuario) {
        this.susuario = susuario.toLowerCase();
    }
    
    public void setContrasena(String scontrasena) {
        this.scontrasena = scontrasena;
    }
    
    public void setConfirmacion(String sconfirmacion) {
        this.sconfirmacion = sconfirmacion;
    }
    
    public void setIdNivelUsuario(int iidNivelUsuario) {
        this.iidNivelUsuario = iidNivelUsuario;
    }
    
    public void setNivelUsuario(String snivelUsuario) {
        this.snivelUsuario = snivelUsuario;
    }
    
    public void setIdPerfilUsuario(int iidPerfilUsuario) {
        this.iidPerfilUsuario = iidPerfilUsuario;
    }
    
    public void setPerfilUsuario(String sperfilUsuario) {
        this.sperfilUsuario = sperfilUsuario;
    }
    
    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setEstadoRegistro(short hestado) {
        this.hestado = hestado;
    }
    
    public void setUsuarioAnterior(String susuarioAnterior) {
        this.susuarioAnterior = susuarioAnterior.toLowerCase();
    }
    
    public int getId() {
        return this.iid;
    }
    
    public int getIdFuncionario() {
        return this.iidFuncionario;
    }
    
    public String getFuncionario() {
        return this.sfuncionario;
    }
    
    public String getUsuario() {
        return this.susuario;
    }
    
    public String getContrasena() {
        return this.scontrasena;
    }
    
    public String getConfirmacion() {
        return this.sconfirmacion;
    }
    
    public int getIdNivelUsuario() {
        return this.iidNivelUsuario;
    }
    
    public String getNivelUsuario() {
        return this.snivelUsuario;
    }
    
    public int getIdPerfilUsuario() {
        return this.iidPerfilUsuario;
    }
    
    public String getPerfilUsuario() {
        return this.sperfilUsuario;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }
    
    public String getFilial() {
        return this.sfilial;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }
    
    public short getEstadoRegistro() {
        return this.hestado;
    }
    
    public String getUsuarioAnterior() {
        return this.susuarioAnterior;
    }
    
    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido(boolean existeUsuarioBD, short estado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdFuncionario() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO;
            bvalido = false;
        }
        if (this.getUsuario().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_USUARIO;
            bvalido = false;
        }
        if (this.getContrasena().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CONTRASENA;
            bvalido = false;
        }
        if (this.getConfirmacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CONFIRMACION;
            bvalido = false;
        }
        if (this.getIdNivelUsuario() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NIVEL_USUARIO;
            bvalido = false;
        }
        if (!this.getContrasena().equals(this.getConfirmacion())) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Contraseña incorrecta, vuelva a ingresar";
            bvalido = false;
        }
        if (estado==generico.entConstante.estadoregistro_pendiente && existeUsuarioBD) { // valida sólo cuando se inserta, no valida cuando se modifica
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Usuario ya existe en la Base de Datos";
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoCambiarContrasena() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getContrasena().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CONTRASENA;
            bvalido = false;
        }
        if (this.getConfirmacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CONFIRMACION;
            bvalido = false;
        }
        if (!this.getContrasena().equals(this.getConfirmacion())) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Contraseña incorrecta, vuelva a ingresar";
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT usuario("+
            this.getId()+","+
            this.getIdFuncionario()+","+
            utilitario.utiCadena.getTextoGuardado(this.getUsuario())+","+
            utilitario.utiCadena.getTextoGuardado(this.getContrasena())+","+
            this.getIdNivelUsuario()+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public String getCrearUsuarioBD(String susuario, String scontrasena) {
        String sentencia = "CREATE USER "+susuario.toLowerCase().trim()+" PASSWORD '"+scontrasena.trim()+"';";
        sentencia += "ALTER ROLE "+susuario.toLowerCase().trim()+" WITH SUPERUSER;";
        return sentencia;
    }
    
    public String getEliminarUsuarioBD(String susuario) {
        return "DROP ROLE "+susuario+";";
    }
    
    public String getConsultaUsuarioBD(String susuario) {
        return "SELECT usename FROM pg_user WHERE usename='"+susuario+"';";
    }
    
    public String getCambiarContrasenaUsuarioBD(String susuario, String scontrasena) {
        return "ALTER ROLE "+susuario+" WITH PASSWORD '"+scontrasena+"';";
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Funcionario", "apellidonombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Usuario", "usuario", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Nivel Usuario", "nivelusuario", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Activo", "activo2", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Dependencia", "dependencia", generico.entLista.tipo_texto));
        return lst;
    }
    
}
