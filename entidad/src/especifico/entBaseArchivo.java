/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entBaseArchivo {

    private int iid;
    private int inumeroOperacion;
    private java.util.Date dfecha;
    private java.util.Date dfechaProceso;
    private java.util.Date dfechaAprobacion;
    private java.util.Date dfechaSolicitud;
    private java.util.Date dfechaVencimiento;
    private String scedula;
    private String snombre;
    private String sapellido;
    private String stipoPrestamo;
    private double umonto;
    private double umontoCapital;
    private double umontoInteres;
    private int iplazo;
    private String scheque;
    private String sobservacion;
    private String sregion;
    private String scomercio;

    private short hestadoRegistro;
    private String smensaje;

    public entBaseArchivo() {
        this.iid = 0;
        this.inumeroOperacion = 0;
        this.dfecha = null;
        this.dfechaProceso = null;
        this.dfechaAprobacion = null;
        this.dfechaSolicitud = null;
        this.dfechaVencimiento = null;
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.stipoPrestamo = "";
        this.umonto = 0.0;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.iplazo = 0;
        this.scheque = "";
        this.sobservacion = "";
        this.sregion = "";
        this.scomercio = "";
       this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entBaseArchivo(int iid, int inumeroOperacion, java.util.Date dfecha, java.util.Date dfechaProceso, java.util.Date dfechaAprobacion, java.util.Date dfechaSolicitud, java.util.Date dfechaVencimiento, String scedula, String snombre, String sapellido, String stipoPrestamo, double umonto, double umontoCapital, double umontoInteres, int iplazo, String scheque, String sobservacion, String sregion, String scomercio, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOperacion = inumeroOperacion;
        this.dfecha = dfecha;
        this.dfechaProceso = dfechaProceso;
        this.dfechaAprobacion = dfechaAprobacion;
        this.dfechaSolicitud = dfechaSolicitud;
        this.dfechaVencimiento = dfechaVencimiento;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.stipoPrestamo = stipoPrestamo;
        this.umonto = umonto;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.iplazo = iplazo;
        this.scheque = scheque;
        this.sobservacion = sobservacion;
        this.sregion = sregion;
        this.scomercio = scomercio;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroOperacion, java.util.Date dfecha, java.util.Date dfechaProceso, java.util.Date dfechaAprobacion, java.util.Date dfechaSolicitud, java.util.Date dfechaVencimiento, String scedula, String snombre, String sapellido, String stipoPrestamo, double umonto, double umontoCapital, double umontoInteres, int iplazo, String scheque, String sobservacion, String sregion, String scomercio, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOperacion = inumeroOperacion;
        this.dfecha = dfecha;
        this.dfechaProceso = dfechaProceso;
        this.dfechaAprobacion = dfechaAprobacion;
        this.dfechaSolicitud = dfechaSolicitud;
        this.dfechaVencimiento = dfechaVencimiento;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.stipoPrestamo = stipoPrestamo;
        this.umonto = umonto;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.iplazo = iplazo;
        this.scheque = scheque;
        this.sobservacion = sobservacion;
        this.sregion = sregion;
        this.scomercio = scomercio;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entBaseArchivo copiar(entBaseArchivo destino) {
        destino.setEntidad(this.getId(), this.getNumeroOperacion(), this.getFecha(), this.getFechaProceso(), this.getFechaAprobacion(), this.getFechaSolicitud(), this.getFechaVencimiento(), this.getCedula(), this.getNombre(), this.getApellido(), this.getTipoPrestamo(), this.getMonto(), this.getMontoCapital(), this.getMontoInteres(), this.getPlazo(), this.getCheque(), this.getObservacion(), this.getRegion(), this.getComercio(), this.getEstadoRegistro());
        return destino;
    }
    
    public entBaseArchivo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaProceso(rs.getDate("fechaproceso")); }
        catch(Exception e) {}
        try { this.setFechaAprobacion(rs.getDate("fechaaprobacion")); }
        catch(Exception e) {}
        try { this.setFechaSolicitud(rs.getDate("fechasolicitud")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setTipoPrestamo(rs.getString("tipoprestamo")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        try { this.setMontoCapital(rs.getDouble("montocapital")); }
        catch(Exception e) {}
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setCheque(rs.getString("cheque")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setRegion(rs.getString("region")); }
        catch(Exception e) {}
        try { this.setComercio(rs.getString("comercio")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setFechaProceso(java.util.Date dfechaProceso) {
        this.dfechaProceso = dfechaProceso;
    }

    public void setFechaAprobacion(java.util.Date dfechaAprobacion) {
        this.dfechaAprobacion = dfechaAprobacion;
    }

    public void setFechaSolicitud(java.util.Date dfechaSolicitud) {
        this.dfechaSolicitud = dfechaSolicitud;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombre(String snombre) {
        this.snombre = snombre;
    }

    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }

    public void setTipoPrestamo(String stipoPrestamo) {
        this.stipoPrestamo = stipoPrestamo;
    }

    public void setMonto(double umonto) {
        this.umonto = umonto;
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setCheque(String scheque) {
        this.scheque = scheque;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setRegion(String sregion) {
        this.sregion = sregion;
    }

    public void setComercio(String scomercio) {
        this.scomercio = scomercio;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public java.util.Date getFechaProceso() {
        return this.dfechaProceso;
    }
    
    public java.util.Date getFechaAprobacion() {
        return this.dfechaAprobacion;
    }
    
    public java.util.Date getFechaSolicitud() {
        return this.dfechaSolicitud;
    }
    
    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }
    
    public String getCedula() {
        return this.scedula;
    }

    public String getNombre() {
        return this.snombre;
    }

    public String getApellido() {
        return this.sapellido;
    }

    public String getTipoPrestamo() {
        return this.stipoPrestamo;
    }

    public double getMonto() {
        return this.umonto;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public String getCheque() {
        return this.scheque;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public String getRegion() {
        return this.sregion;
    }

    public String getComercio() {
        return this.scomercio;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public String getSentencia() {
        return "SELECT basearchivo("+
            this.getId()+","+
            this.getNumeroOperacion()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaProceso())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAprobacion())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaSolicitud())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaVencimiento())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTipoPrestamo())+","+
            this.getMonto()+","+
            this.getMontoCapital()+","+
            this.getMontoInteres()+","+
            this.getPlazo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCheque())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getRegion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getComercio())+","+
            this.getEstadoRegistro()+")";
    }

}
