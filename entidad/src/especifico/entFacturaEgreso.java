/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entFacturaEgreso {
    
    private int iid;
    private String snumeroFactura;
    private java.util.Date dfechaProceso;
    private java.util.Date dfechaFactura;
    private String sruc;
    private int iidProveedor;
    private String sproveedor;
    private int iidTipoFactura;
    private String stipoFactura;
    private java.util.Date dfechaVencimiento;
    private double uimporteTotal;
    private double utotalExenta;
    private double utotalImpuesto5;
    private double utotalImpuesto10;
    private double uexenta;
    private double uimpuesto5;
    private double uimpuesto10;
    private double utotalImpuesto;
    private double usaldo;
    private int itimbrado;
    private int iidEstado;
    private String sestado;
    private String sobservacion;
    private int iidCentroCosto;
    private String scentroCosto;
    
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_OBSERVACION = 250;
    public static final int LONGITUD_OBSERVACION_ANULADO = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ESTADO_FACTURA = "Estado de Factura (no vacío)";
    public static final String TEXTO_NUMERO_FACTURA = "Número de Factura (no vacío)";
    public static final String TEXTO_FECHA_PROCESO = "Fecha de proceso (no vacía)";
    public static final String TEXTO_FECHA_FACTURA = "Fecha de Factura (no vacía)";
    public static final String TEXTO_PROVEEDOR = "Proveedor (no vacío)";
    public static final String TEXTO_TIPO_FACTURA = "Tipo de Factura (no vacío)";
    public static final String TEXTO_FECHA_VENCIMIENTO = "Fecha de Vencimiento (no vacía)";
    public static final String TEXTO_TOTAL = "Total (no editable)";
    public static final String TEXTO_EXENTA = "Exenta (no editable)";
    public static final String TEXTO_IMPUESTO5 = "Impuesto 5% (no editable)";
    public static final String TEXTO_IMPUESTO10 = "Impuesto 10% (no editable)";
    public static final String TEXTO_TOTAL_IMPUESTO5 = "Total Impuesto 5% (no editable)";
    public static final String TEXTO_TOTAL_IMPUESTO10 = "Total Impuesto 10% (no editable)";
    public static final String TEXTO_TOTAL_IMPUESTO = "Total Impuesto (no editable)";
    public static final String TEXTO_SALDO = "Saldo (no vacío)";
    public static final String TEXTO_TIMBRADO = "Timbrado (no vacío)";
    public static final String TEXTO_OBSERVACION = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_CENTRO_COSTO = "Centro de Costo (no vacía)";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación de anulación (no vacía, hasta " + LONGITUD_OBSERVACION_ANULADO + " caracteres)";
    
    public entFacturaEgreso() {
        this.iid = 0;
        this.snumeroFactura = "";
        this.dfechaProceso = null;
        this.dfechaFactura = null;
        this.sruc = "";
        this.iidProveedor = 0;
        this.sproveedor = "";
        this.iidTipoFactura = 0;
        this.stipoFactura = "";
        this.dfechaVencimiento = null;
        this.uimporteTotal = 0.0;
        this.utotalExenta = 0.0;
        this.utotalImpuesto5 = 0.0;
        this.utotalImpuesto10 = 0.0;
        this.uexenta = 0.0;
        this.uimpuesto5 = 0.0;
        this.uimpuesto10 = 0.0;
        this.utotalImpuesto = 0.0;
        this.usaldo = 0.0;
        this.itimbrado = 0;
        this.iidEstado = 0;
        this.sestado = "";
        this.sobservacion = "";
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entFacturaEgreso(int iid, String snumeroFactura, java.util.Date dfechaProceso, java.util.Date dfechaFactura, String sruc, int iidProveedor, String sproveedor, int iidTipoFactura, String stipoFactura, java.util.Date dfechaVencimiento, double uimporteTotal, double utotalExenta, double utotalImpuesto5, double utotalImpuesto10, double uexenta, double uimpuesto5, double uimpuesto10, double utotalImpuesto, double usaldo, int itimbrado, int iidEstado, String sestado, String sobservacion, int iidCentroCosto, String scentroCosto, java.util.Date dfechaAnulado, String sobservacionAnulado, short hestadoRegistro) {
        this.iid = iid;
        this.snumeroFactura = snumeroFactura;
        this.dfechaProceso = dfechaProceso;
        this.dfechaFactura = dfechaFactura;
        this.sruc = sruc;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.iidTipoFactura = iidTipoFactura;
        this.stipoFactura = stipoFactura;
        this.dfechaVencimiento = dfechaVencimiento;
        this.uimporteTotal = uimporteTotal;
        this.utotalExenta = utotalExenta;
        this.utotalImpuesto5 = utotalImpuesto5;
        this.utotalImpuesto10 = utotalImpuesto10;
        this.uexenta = uexenta;
        this.uimpuesto5 = uimpuesto5;
        this.uimpuesto10 = uimpuesto10;
        this.utotalImpuesto = utotalImpuesto;
        this.usaldo = usaldo;
        this.itimbrado = itimbrado;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.sobservacion = sobservacion;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, String snumeroFactura, java.util.Date dfechaProceso, java.util.Date dfechaFactura, String sruc, int iidProveedor, String sproveedor, int iidTipoFactura, String stipoFactura, java.util.Date dfechaVencimiento, double uimporteTotal, double utotalExenta, double utotalImpuesto5, double utotalImpuesto10, double uexenta, double uimpuesto5, double uimpuesto10, double utotalImpuesto, double usaldo, int itimbrado, int iidEstado, String sestado, String sobservacion, int iidCentroCosto, String scentroCosto, java.util.Date dfechaAnulado, String sobservacionAnulado, short hestadoRegistro) {
        this.iid = iid;
        this.snumeroFactura = snumeroFactura;
        this.dfechaProceso = dfechaProceso;
        this.dfechaFactura = dfechaFactura;
        this.sruc = sruc;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.iidTipoFactura = iidTipoFactura;
        this.stipoFactura = stipoFactura;
        this.dfechaVencimiento = dfechaVencimiento;
        this.uimporteTotal = uimporteTotal;
        this.utotalExenta = utotalExenta;
        this.utotalImpuesto5 = utotalImpuesto5;
        this.utotalImpuesto10 = utotalImpuesto10;
        this.uexenta = uexenta;
        this.uimpuesto5 = uimpuesto5;
        this.uimpuesto10 = uimpuesto10;
        this.utotalImpuesto = utotalImpuesto;
        this.usaldo = usaldo;
        this.itimbrado = itimbrado;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.sobservacion = sobservacion;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entFacturaEgreso copiar(entFacturaEgreso destino) {
        destino.setEntidad(this.getId(), this.getNumeroFactura(), this.getFechaProceso(), this.getFechaFactura(), this.getRuc(), this.getIdProveedor(), this.getProveedor(), this.getIdTipoFactura(), this.getTipoFactura(), this.getFechaVencimiento(), this.getImporteTotal(), this.getTotalExenta(), this.getTotalImpuesto5(), this.getTotalImpuesto10(), this.getExenta(), this.getImpuesto5(), this.getImpuesto10(), this.getTotalImpuesto(), this.getSaldo(), this.getTimbrado(), this.getIdEstado(), this.getEstado(), this.getObservacion(), this.getIdCentroCosto(), this.getCentroCosto(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getEstadoRegistro());
        return destino;
    }
    
    public entFacturaEgreso cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setNumeroFactura(rs.getString("numerofactura")); }
        catch(Exception e) { }
        try { this.setFechaProceso(rs.getDate("fechaproceso")); }
        catch(Exception e) { }
        try { this.setFechaFactura(rs.getDate("fechafactura")); }
        catch(Exception e) { }
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) { }
        try { this.setIdProveedor(rs.getInt("idproveedor")); }
        catch(Exception e) { }
        try { this.setProveedor(rs.getString("proveedor")); }
        catch(Exception e) { }
        try { this.setIdTipoFactura(rs.getInt("idtipofactura")); }
        catch(Exception e) { }
        try { this.setTipoFactura(rs.getString("tipofactura")); }
        catch(Exception e) { }
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) { }
        try { this.setImporteTotal(rs.getDouble("importetotal")); }
        catch(Exception e) { }
        try { this.setTotalExenta(rs.getDouble("totalexenta")); }
        catch(Exception e) { }
        try { this.setTotalImpuesto5(rs.getDouble("totalimpuesto5")); }
        catch(Exception e) { }
        try { this.setTotalImpuesto10(rs.getDouble("totalimpuesto10")); }
        catch(Exception e) { }
        try { this.setExenta(rs.getDouble("exenta")); }
        catch(Exception e) { }
        try { this.setImpuesto5(rs.getDouble("impuesto5")); }
        catch(Exception e) { }
        try { this.setImpuesto10(rs.getDouble("impuesto10")); }
        catch(Exception e) { }
        try { this.setTotalImpuesto(rs.getDouble("totalimpuesto")); }
        catch(Exception e) { }
        try { this.setSaldo(rs.getDouble("saldo")); }
        catch(Exception e) { }
        try { this.setTimbrado(rs.getInt("timbrado")); }
        catch(Exception e) { }
        try { this.setIdEstado(rs.getInt("idestadofactura")); }
        catch(Exception e) { }
        try { this.setEstado(rs.getString("estadofactura")); }
        catch(Exception e) { }
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) { }
        try { this.setIdCentroCosto(rs.getInt("idcentrocosto")); }
        catch(Exception e) { }
        try { this.setCentroCosto(rs.getString("centrocosto")); }
        catch(Exception e) { }
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) { }
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setNumeroFactura(String snumeroFactura) {
        this.snumeroFactura = snumeroFactura;
    }

    public void setFechaProceso(java.util.Date dfechaProceso) {
        this.dfechaProceso = dfechaProceso;
    }

    public void setFechaFactura(java.util.Date dfechaFactura) {
        this.dfechaFactura = dfechaFactura;
    }

    public void setRuc(String sruc) {
        this.sruc = sruc;
    }
    
    public void setIdProveedor(int iidProveedor) {
        this.iidProveedor = iidProveedor;
    }
    
    public void setProveedor(String sproveedor) {
        this.sproveedor = sproveedor;
    }
    
    public void setIdTipoFactura(int iidTipoFactura) {
        this.iidTipoFactura = iidTipoFactura;
    }
    
    public void setTipoFactura(String stipoFactura) {
        this.stipoFactura = stipoFactura;
    }
    
    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setImporteTotal(double uimporteTotal) {
        this.uimporteTotal = uimporteTotal;
    }

    public void setTotalExenta(double utotalExenta) {
        this.utotalExenta = utotalExenta;
    }

    public void setTotalImpuesto5(double utotalImpuesto5) {
        this.utotalImpuesto5 = utotalImpuesto5;
    }

    public void setTotalImpuesto10(double utotalImpuesto10) {
        this.utotalImpuesto10 = utotalImpuesto10;
    }

    public void setExenta(double uexenta) {
        this.uexenta = uexenta;
    }

    public void setImpuesto5(double uimpuesto5) {
        this.uimpuesto5 = uimpuesto5;
    }

    public void setImpuesto10(double uimpuesto10) {
        this.uimpuesto10 = uimpuesto10;
    }

    public void setTotalImpuesto(double utotalImpuesto) {
        this.utotalImpuesto = utotalImpuesto;
    }

    public void setSaldo(double usaldo) {
        this.usaldo = usaldo;
    }

    public void setTimbrado(int itimbrado) {
        this.itimbrado = itimbrado;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }

    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public String getNumeroFactura() {
        return this.snumeroFactura;
    }

    public java.util.Date getFechaProceso() {
        return this.dfechaProceso;
    }

    public java.util.Date getFechaFactura() {
        return this.dfechaFactura;
    }

    public String getRuc() {
        return this.sruc;
    }
    
    public int getIdProveedor() {
        return this.iidProveedor;
    }
    
    public String getProveedor() {
        return this.sproveedor;
    }
    
    public int getIdTipoFactura() {
        return this.iidTipoFactura;
    }
    
    public String getTipoFactura() {
        return this.stipoFactura;
    }
    
    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getImporteTotal() {
        return this.uimporteTotal;
    }

    public double getTotalExenta() {
        return this.utotalExenta;
    }

    public double getTotalImpuesto5() {
        return this.utotalImpuesto5;
    }

    public double getTotalImpuesto10() {
        return this.utotalImpuesto10;
    }

    public double getExenta() {
        return this.uexenta;
    }

    public double getImpuesto5() {
        return this.uimpuesto5;
    }

    public double getImpuesto10() {
        return this.uimpuesto10;
    }

    public double getTotalImpuesto() {
        return this.utotalImpuesto;
    }

    public double getSaldo() {
        return this.usaldo;
    }

    public int getTimbrado() {
        return this.itimbrado;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }

    public String getCentroCosto() {
        return this.scentroCosto;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNumeroFactura().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_FACTURA;
            bvalido = false;
        }
        if (this.getFechaProceso()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_PROCESO;
            bvalido = false;
        }
        if (this.getFechaFactura()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_FACTURA;
            bvalido = false;
        }
        if (this.getIdProveedor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PROVEEDOR;
            bvalido = false;
        }
        if (this.getIdTipoFactura()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_FACTURA;
            bvalido = false;
        }
        if (this.getImporteTotal()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TOTAL;
            bvalido = false;
        }
        if (this.getSaldo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SALDO;
            bvalido = false;
        }
        if (this.getTimbrado()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIMBRADO;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        if (this.getIdCentroCosto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CENTRO_COSTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaAnulado()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_ANULADO;
            bvalido = false;
        }
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT facturaegreso("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumeroFactura())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaProceso())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaFactura())+","+
            this.getIdProveedor()+","+
            this.getIdTipoFactura()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaVencimiento())+","+
            this.getImporteTotal()+","+
            this.getSaldo()+","+
            this.getTimbrado()+","+
            this.getIdEstado()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getIdCentroCosto()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Nro", "id", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Número Factura", "numerofactura", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Fecha Proceso", "fechaproceso", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(4, "Fecha Factura", "fechafactura", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(5, "Proveedor", "proveedor", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Tipo Factura", "tipofactura", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Fecha Vencimiento", "fechavencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(8, "Estado", "estadofactura", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(9, "Filial", "filial", generico.entLista.tipo_texto));
        return lst;
    }
    
}
