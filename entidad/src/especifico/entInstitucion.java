/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entGenerica;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entInstitucion extends entGenerica {

    protected int iidCiudad;
    protected String sciudad;
    protected int iidRegional;
    protected String sregional;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Institución (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_CIUDAD = "Descripción de Ciudad (no vacía)";
    public static final String TEXTO_REGIONAL = "Descripción de Región Sanitaria a que pertenece (no vacía)";

    public entInstitucion() {
        super();
        this.iidCiudad = 0;
        this.sciudad = "";
        this.iidRegional = 0;
        this.sregional = "";
    }

    public entInstitucion(int iid, String sdescripcion) {
        super(iid, sdescripcion);
    }

    public entInstitucion(int iid, String sdescripcion, int iidCiudad, String sciudad, int iidRegional, String sregional, short hestado) {
        super(iid, sdescripcion, hestado);
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
    }

    public void setEntidad(int iid, String sdescripcion, int iidCiudad, String sciudad, int iidRegional, String sregional, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entInstitucion copiar(entInstitucion destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdCiudad(), this.getCiudad(), this.getIdRegional(), this.getRegional(), this.getEstadoRegistro());
        return destino;
    }

    public entInstitucion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdCiudad(rs.getInt("idciudad")); }
        catch(Exception e) {}
        try { this.setCiudad(rs.getString("ciudad")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdCiudad(int iidCiudad) {
        this.iidCiudad = iidCiudad;
    }
    
    public void setCiudad(String sciudad) {
        this.sciudad = sciudad;
    }
    
    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }
    
    public void setRegional(String sregional) {
        this.sregional = sregional;
    }
    
    public int getIdCiudad() {
        return this.iidCiudad;
    }
    
    public String getCiudad() {
        return this.sciudad;
    }
    
    public int getIdRegional() {
        return this.iidRegional;
    }
    
    public String getRegional() {
        return this.sregional;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdCiudad()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CIUDAD;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT institucion("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdCiudad()+","+
            this.getIdRegional()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Ciudad", "ciudad", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Regional", "regional", generico.entLista.tipo_texto));
        return lst;
    }
    
}
