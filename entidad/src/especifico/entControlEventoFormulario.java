/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entControlEventoFormulario extends generico.entGenerica{
    
    private String sserie;
    private String sversion;
    private int inumeroFormulario;
    private java.util.Date dfechaEvento;
    private String sobservacion;
    private int iidVerificador;
    private java.util.Date dfechaVerificacion;
    private String sverificacionFinal;
    private int iidVerificadorFinal;
    private int iidModalidad;
    private int inumeroSolicitud;
    private int inumeroOperacion;
    private String shoraRecepcion;
    private String shoraRemision;
    private int iidEstado;
    private String sestado;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;

    private java.util.Date dfechaCorreccion;
    private String scorreccion;
    private int iidCorrector;
    
    public static final int LONGITUD_DESCRIPCION = 80;
    public static final int LONGITUD_OBSERVACION = 250;
    public static final int LONGITUD_CORRECCION = 250;
    public static final int LONGITUD_VERIFICACION_FINAL = 250;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Formulario (no editable)";
    public static final String TEXTO_SERIE = "Serie de Formulario (no editable)";
    public static final String TEXTO_VERSION = "Versión de Formulario (no editable)";
    public static final String TEXTO_NUMERO_FORMULARIO = "Número de Formulario (no editable)";
    public static final String TEXTO_FECHA_EVENTO = "Fecha del evento (no vacía)";
    public static final String TEXTO_OBSERVACION = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_VERIFICADOR = "Verificador (no editable)";
    public static final String TEXTO_FECHA_CORRECCION = "Fecha de corrección (no vacía)";
    public static final String TEXTO_CORRECCION = "Corrección (no vacía, hasta " + LONGITUD_CORRECCION + " caracteres)";
    public static final String TEXTO_CORRECTOR = "Corrector (no editable)";
    public static final String TEXTO_FECHA_VERIFICACION = "Fecha de verificación (no vacía)";
    public static final String TEXTO_VERIFICACION_FINAL = "Verificación final (no vacía, hasta " + LONGITUD_VERIFICACION_FINAL + " caracteres)";
    public static final String TEXTO_VERIFICADOR_FINAL = "Verificador final (no editable)";
    public static final String TEXTO_MODALIDAD = "Modalidad (no vacía)";
    public static final String TEXTO_NUMERO_SOLICITUD = "Número de Solicitud (valor positivo)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (valor positivo)";
    public static final String TEXTO_HORA_RECEPCION = "Hora de Recepción (no vacía)";
    public static final String TEXTO_HORA_REMISION = "Hora de Remisión (no vacía)";
    public static final String TEXTO_ESTADO = "Estado (no editable)";

    public entControlEventoFormulario() {
        super();
        this.sserie = "";
        this.sversion = "";
        this.inumeroFormulario = 0;
        this.dfechaEvento = null;
        this.sobservacion = "";
        this.iidVerificador = 0;
        this.dfechaCorreccion = null;
        this.scorreccion = "";
        this.iidCorrector = 0;
        this.dfechaVerificacion = null;
        this.sverificacionFinal = "";
        this.iidVerificadorFinal = 0;
        this.iidModalidad = 0;
        this.inumeroSolicitud = 0;
        this.inumeroOperacion = 0;
        this.shoraRecepcion = "";
        this.shoraRemision = "";
        this.iidEstado = 0;
        this.sestado = "";
    }

    public entControlEventoFormulario(int iid, String sdescripcion, String sserie, String sversion, int inumeroFormulario, java.util.Date dfechaEvento, String sobservacion, int iidVerificador, java.util.Date dfechaCorreccion, String scorreccion, int iidCorrector, java.util.Date dfechaVerificacion, String sverificacionFinal, int iidVerificadorFinal, int iidModalidad, int inumeroSolicitud, int inumeroOperacion, String shoraRecepcion, String shoraRemision, int iidEstado, String sestado, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.sserie = sserie;
        this.sversion = sversion;
        this.inumeroFormulario = inumeroFormulario;
        this.dfechaEvento = dfechaEvento;
        this.sobservacion = sobservacion;
        this.iidVerificador = iidVerificador;
        this.dfechaCorreccion = dfechaCorreccion;
        this.scorreccion = scorreccion;
        this.iidCorrector = iidCorrector;
        this.dfechaVerificacion = dfechaVerificacion;
        this.sverificacionFinal = sverificacionFinal;
        this.iidVerificadorFinal = iidVerificadorFinal;
        this.iidModalidad = iidModalidad;
        this.inumeroSolicitud = inumeroSolicitud;
        this.inumeroOperacion = inumeroOperacion;
        this.shoraRecepcion = shoraRecepcion;
        this.shoraRemision = shoraRemision;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
    }
    
    public void setEntidad(int iid, String sdescripcion, String sserie, String sversion, int inumeroFormulario, java.util.Date dfechaEvento, String sobservacion, int iidVerificador, java.util.Date dfechaCorreccion, String scorreccion, int iidCorrector, java.util.Date dfechaVerificacion, String sverificacionFinal, int iidVerificadorFinal, int iidModalidad, int inumeroSolicitud, int inumeroOperacion, String shoraRecepcion, String shoraRemision, int iidEstado, String sestado, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.sserie = sserie;
        this.sversion = sversion;
        this.inumeroFormulario = inumeroFormulario;
        this.dfechaEvento = dfechaEvento;
        this.sobservacion = sobservacion;
        this.iidVerificador = iidVerificador;
        this.dfechaCorreccion = dfechaCorreccion;
        this.scorreccion = scorreccion;
        this.iidCorrector = iidCorrector;
        this.dfechaVerificacion = dfechaVerificacion;
        this.sverificacionFinal = sverificacionFinal;
        this.iidVerificadorFinal = iidVerificadorFinal;
        this.iidModalidad = iidModalidad;
        this.inumeroSolicitud = inumeroSolicitud;
        this.inumeroOperacion = inumeroOperacion;
        this.shoraRecepcion = shoraRecepcion;
        this.shoraRemision = shoraRemision;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entControlEventoFormulario copiar(entControlEventoFormulario destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getSerie(), this.getVersion(), this.getNumeroFormulario(), this.getFechaEvento(), this.getObservacion(), this.getIdVerificador(), this.getFechaCorreccion(), this.getCorreccion(), this.getIdCorrector(), this.getFechaVerificacion(), this.getVerificacionFinal(), this.getIdVerificadorFinal(), this.getIdModalidad(), this.getNumeroSolicitud(), this.getNumeroOperacion(), this.getHoraRecepcion(), this.getHoraRemision(), this.getIdEstado(), this.getEstado(), this.getEstadoRegistro());
        return destino;
    }

    public entControlEventoFormulario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setSerie(rs.getString("serie")); }
        catch(Exception e) {}
        try { this.setVersion(rs.getString("version")); }
        catch(Exception e) {}
        try { this.setNumeroFormulario(rs.getInt("numeroformulario")); }
        catch(Exception e) {}
        try { this.setFechaEvento(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setIdVerificador(rs.getInt("idverificador")); }
        catch(Exception e) {}
        try { this.setFechaCorreccion(rs.getDate("fechacorreccion")); }
        catch(Exception e) {}
        try { this.setCorreccion(rs.getString("correccion")); }
        catch(Exception e) {}
        try { this.setIdCorrector(rs.getInt("idcorrector")); }
        catch(Exception e) {}
        try { this.setFechaVerificacion(rs.getDate("fechaverificacion")); }
        catch(Exception e) {}
        try { this.setVerificacionFinal(rs.getString("verificacionfinal")); }
        catch(Exception e) {}
        try { this.setIdVerificadorFinal(rs.getInt("idverificadorfinal")); }
        catch(Exception e) {}
        try { this.setIdModalidad(rs.getInt("idmodalidad")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setHoraRecepcion(rs.getString("horarecepcion")); }
        catch(Exception e) {}
        try { this.setHoraRemision(rs.getString("horaremision")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setSerie(String sserie) {
        this.sserie = sserie;
    }

    public void setVersion(String sversion) {
        this.sversion = sversion;
    }

    public void setNumeroFormulario(int inumeroFormulario) {
        this.inumeroFormulario = inumeroFormulario;
    }

    public void setFechaEvento(java.util.Date dfechaEvento) {
        this.dfechaEvento = dfechaEvento;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setIdVerificador(int iidVerificador) {
        this.iidVerificador = iidVerificador;
    }

    public void setFechaCorreccion(java.util.Date dfechaCorreccion) {
        this.dfechaCorreccion = dfechaCorreccion;
    }

    public void setCorreccion(String scorreccion) {
        this.scorreccion = scorreccion;
    }

    public void setIdCorrector(int iidCorrector) {
        this.iidCorrector = iidCorrector;
    }

    public void setFechaVerificacion(java.util.Date dfechaVerificacion) {
        this.dfechaVerificacion = dfechaVerificacion;
    }

    public void setVerificacionFinal(String sverificacionFinal) {
        this.sverificacionFinal = sverificacionFinal;
    }

    public void setIdVerificadorFinal(int iidVerificadorFinal) {
        this.iidVerificadorFinal = iidVerificadorFinal;
    }

    public void setIdModalidad(int iidModalidad) {
        this.iidModalidad = iidModalidad;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setHoraRecepcion(String shoraRecepcion) {
        this.shoraRecepcion = shoraRecepcion;
    }

    public void setHoraRemision(String shoraRemision) {
        this.shoraRemision = shoraRemision;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public String getSerie() {
        return this.sserie;
    }

    public String getVersion() {
        return this.sversion;
    }

    public int getNumeroFormulario() {
        return this.inumeroFormulario;
    }

    public java.util.Date getFechaEvento() {
        return this.dfechaEvento;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public int getIdVerificador() {
        return this.iidVerificador;
    }

    public java.util.Date getFechaCorreccion() {
        return this.dfechaCorreccion;
    }

    public String getCorreccion() {
        return this.scorreccion;
    }

    public int getIdCorrector() {
        return this.iidCorrector;
    }

    public java.util.Date getFechaVerificacion() {
        return this.dfechaVerificacion;
    }

    public String getVerificacionFinal() {
        return this.sverificacionFinal;
    }

    public int getIdVerificadorFinal() {
        return this.iidVerificadorFinal;
    }

    public int getIdModalidad() {
        return this.iidModalidad;
    }

    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public String getHoraRecepcion() {
        return this.shoraRecepcion;
    }

    public String getHoraRemision() {
        return this.shoraRemision;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaEvento()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_EVENTO;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        if (this.getFechaCorreccion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_CORRECCION;
            bvalido = false;
        }
        if (this.getCorreccion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CORRECCION;
            bvalido = false;
        }
        if (this.getFechaVerificacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_VERIFICACION;
            bvalido = false;
        }
        if (this.getIdModalidad()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MODALIDAD;
            bvalido = false;
        }
        if (this.getNumeroSolicitud()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_SOLICITUD;
            bvalido = false;
        }
        if (this.getNumeroOperacion()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_OPERACION;
            bvalido = false;
        }
        if (this.getHoraRecepcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_RECEPCION;
            bvalido = false;
        }
        if (this.getHoraRemision().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_REMISION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT eventoformulario("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getSerie())+","+
            utilitario.utiCadena.getTextoGuardado(this.getVersion())+","+
            this.getNumeroFormulario()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaEvento())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getIdVerificador()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaCorreccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCorreccion())+","+
            this.getIdCorrector()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaVerificacion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getVerificacionFinal())+","+
            this.getIdVerificadorFinal()+","+
            this.getIdModalidad()+","+
            this.getNumeroSolicitud()+","+
            this.getNumeroOperacion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraRecepcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraRemision())+","+
            this.getIdEstado()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Serie", "serie", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Número Formulario", "numeroformulario", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "Fecha Evento", "fecha", generico.entLista.tipo_fecha));
        return lst;
    }
}
