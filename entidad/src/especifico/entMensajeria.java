/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMensajeria extends generico.entGenericaPersona {
    
    protected String snombreApellido;
    protected double uatraso;
    protected String stelefonoCelular;
    protected String smensaje;
    protected java.util.Date dfechaNacimiento;
   
    public static final int LONGITUD_MENSAJE = 150;

    public static final String TEXTO_MENSAJE = "Mensaje a enviar (no vacío, hasta " + LONGITUD_MENSAJE + " caracteres)";
    public static final String TEXTO_SEPARACION = ". ";
    public static final String TEXTO_PIE = "Mutual MSP. Tel.0986197140. Omitir en caso de pago";
    
    public entMensajeria() {
        super();
        this.uatraso = 0.0;
        this.stelefonoCelular = "";
        this.smensaje = "";
        this.dfechaNacimiento = null;
    }

    public entMensajeria(int iid, String snombre, String sapellido, String scedula, String snombreApellido, double uatraso, String stelefonoCelular, String smensaje, java.util.Date dfechaNacimiento, short hestadoRegistro) {
        super(iid, snombre, sapellido, scedula, hestadoRegistro);
        this.snombreApellido = snombreApellido.trim();
        this.uatraso = uatraso;
        this.stelefonoCelular = stelefonoCelular.trim();
        this.smensaje = smensaje;
        this.dfechaNacimiento = dfechaNacimiento;
    }

    public void setEntidad(int iid, String snombre, String sapellido, String scedula, String snombreApellido, double uatraso, String stelefonoCelular, String smensaje, java.util.Date dfechaNacimiento, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre.trim().toUpperCase();
        this.sapellido = sapellido.trim().toUpperCase();
        this.scedula = scedula;
        this.snombreApellido = snombreApellido.trim();
        this.uatraso = uatraso;
        this.stelefonoCelular = stelefonoCelular.trim();
        this.smensaje = smensaje;
        this.dfechaNacimiento = dfechaNacimiento;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entMensajeria copiar(entMensajeria destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getNombreApellido(), this.getAtraso(), this.getTelefonoCelular(), this.getMensaje(), this.getFechaNacimiento(), this.getEstadoRegistro());
        return destino;
    }
    
    public entMensajeria cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) { }
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) { }
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) { }
        try { this.setNombreApellido(rs.getString("nombre")+" "+rs.getString("apellido")); }
        catch(Exception e) { }
        try { this.setAtraso(rs.getDouble("atrasomax")); }
        catch(Exception e) { }
        try { this.setTelefonoCelular(rs.getString("telefonocelularprincipal")); }
        catch(Exception e) { }
        try { this.setMensaje(rs.getString("mensaje")); }
        catch(Exception e) { }
        try { this.setFechaNacimiento(rs.getDate("fechanacimiento")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setNombreApellido(String snombreApellido) {
        this.snombreApellido = snombreApellido.trim();
    }
    
    public void setAtraso(double uatraso) {
        this.uatraso = uatraso;
    }
    
    public void setTelefonoCelular(String stelefonoCelular) {
        this.stelefonoCelular = stelefonoCelular.trim();
    }
    
    public void setMensaje(String smensaje) {
        this.smensaje = smensaje;
    }
    
    public void setFechaNacimiento(java.util.Date dfechaNacimiento) {
        this.dfechaNacimiento = dfechaNacimiento;
    }

    public String getNombreApellido() {
        return this.snombreApellido;
    }
    
    public double getAtraso() {
        return this.uatraso;
    }
    
    public String getTelefonoCelular() {
        return this.stelefonoCelular;
    }
    
    public String getMensaje() {
        return this.smensaje;
    }
    
    public java.util.Date getFechaNacimiento() {
        return this.dfechaNacimiento;
    }

    public String getSentencia() {
        return "SELECT sms.programar_sms("+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoCelular().substring(0,4)+this.getTelefonoCelular().substring(5,11))+","+
            utilitario.utiCadena.getTextoGuardado(this.getMensaje())+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
}
