/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPrograma extends generico.entGenerica{
    
    protected String scodigo;
    protected String sinterfaz;
    
    public static final int LONGITUD_CODIGO = 10;
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final int LONGITUD_INTERFAZ = 30;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CODIGO = "Código de Programa (no vacía, hasta " + LONGITUD_CODIGO + " caracteres)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Programa (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_INTERFAZ = "Interfaz del Programa (no vacía, hasta " + LONGITUD_INTERFAZ + " caracteres)";

    public entPrograma() {
        super();
        this.scodigo = "";
        this.sinterfaz = "";
    }

    public entPrograma(int iid, String scodigo, String sdescripcion, String sinterfaz, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.scodigo = scodigo;
        this.sinterfaz = sinterfaz;
    }
    
    public void setEntidad(int iid, String scodigo, String sdescripcion, String sinterfaz, short hestadoRegistro) {
        this.iid = iid;
        this.scodigo = scodigo;
        this.sdescripcion = sdescripcion;
        this.sinterfaz = sinterfaz;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entPrograma copiar(entPrograma destino) {
        destino.setEntidad(this.getId(), this.getCodigo(), this.getDescripcion(), this.getInterfaz(), this.getEstadoRegistro());
        return destino;
    }

    public entPrograma cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setCodigo(rs.getString("codigo")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setInterfaz(rs.getString("interfaz")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setCodigo(String scodigo) {
        this.scodigo = scodigo;
    }
    
    public void setInterfaz(String sinterfaz) {
        this.sinterfaz = sinterfaz;
    }
    
    public String getCodigo() {
        return this.scodigo;
    }
    
    public String getInterfaz() {
        return this.sinterfaz;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getCodigo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CODIGO;
            bvalido = false;
        }
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getInterfaz().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_INTERFAZ;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT programa("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCodigo())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getInterfaz())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Código", "codigo", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Interfaz", "interfaz", generico.entLista.tipo_texto));
        return lst;
    }
}
