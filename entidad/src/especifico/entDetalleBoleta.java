/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleBoleta {

    private int iid;
    private int iidBoleta;
    private String sboleta;
    private String scedula;
    private int icuota;
    private double uimporte;
    private int ilinea;
    private String sarchivo;
    private short hestadoRegistro;
    private String smensaje;

    public entDetalleBoleta() {
        this.iid = 0;
        this.iidBoleta = 0;
        this.sboleta = "";
        this.scedula = "";
        this.icuota = 0;
        this.uimporte = 0.0;
        this.ilinea = 0;
        this.sarchivo = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleBoleta(int iid, int iidBoleta, String sboleta, String scedula, int icuota, double uimporte, int ilinea, String sarchivo, short hestadoRegistro) {
        this.iid = iid;
        this.iidBoleta = iidBoleta;
        this.sboleta = sboleta;
        this.scedula = scedula;
        this.icuota = icuota;
        this.uimporte = uimporte;
        this.ilinea = ilinea;
        this.sarchivo = sarchivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidBoleta, String sboleta, String scedula, int icuota, double uimporte, int ilinea, String sarchivo, short hestadoRegistro) {
        this.iid = iid;
        this.iidBoleta = iidBoleta;
        this.sboleta = sboleta;
        this.scedula = scedula;
        this.icuota = icuota;
        this.uimporte = uimporte;
        this.ilinea = ilinea;
        this.sarchivo = sarchivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public entDetalleBoleta copiar(entDetalleBoleta destino) {
        destino.setEntidad(this.getId(), this.getIdBoleta(), this.getBoleta(), this.getCedula(), this.getCuota(), this.getImporte(), this.getLinea(), this.getArchivo(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleBoleta cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdBoleta(rs.getInt("idboleta")); }
        catch(Exception e) {}
        try { this.setBoleta(rs.getString("boleta")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setCuota(rs.getInt("cuota")); }
        catch(Exception e) {}
        try { this.setImporte(rs.getDouble("importe")); }
        catch(Exception e) {}
        try { this.setLinea(0); }
        catch(Exception e) {}
        try { this.setArchivo(""); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdBoleta(int iidBoleta) {
        this.iidBoleta = iidBoleta;
    }

    public void setBoleta(String sboleta) {
        this.sboleta = sboleta;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }

    public void setImporte(double uimporte) {
        this.uimporte = uimporte;
    }

    public void setLinea(int ilinea) {
        this.ilinea = ilinea;
    }

    public void setArchivo(String sarchivo) {
        this.sarchivo = sarchivo;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdBoleta() {
        return this.iidBoleta;
    }

    public String getBoleta() {
        return this.sboleta;
    }

    public String getCedula() {
        return this.scedula;
    }

    public int getCuota() {
        return this.icuota;
    }

    public double getImporte() {
        return this.uimporte;
    }

    public int getLinea() {
        return this.ilinea;
    }

    public String getArchivo() {
        return this.sarchivo;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getCuota()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "";
            bvalido = false;
        }
        if (this.getImporte()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        if (this.getEstadoRegistro() == generico.entConstante.estadoregistro_modificado) return this.getCampoModificacion();
        if (this.getEstadoRegistro() == generico.entConstante.estadoregistro_insertado) return this.getCampoInsercion();
        if (this.getEstadoRegistro() == generico.entConstante.estadoregistro_eliminado) return this.getCampoEliminacion();
        return "";
    }

    // métodos para la conexión
    private String getCampoInsercion() {
        return "SELECT insertardetalleboleta("+
            this.getIdBoleta()+","+
            this.getCuota()+","+
            this.getImporte()+")";
    }

    private String getCampoEliminacion() {
        return "SELECT eliminar("+
            this.getId()+","+
            "'detalleboleta'"+")";
    }
    
    private String getCampoModificacion() {
        return "SELECT modificardetalleboleta("+
            this.getId()+","+
            this.getIdBoleta()+","+
            this.getCuota()+","+
            this.getImporte()+")";
    }
    
    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
