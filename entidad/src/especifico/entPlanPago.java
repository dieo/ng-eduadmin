/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.util.Date;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entPlanPago {

    private int iid;
    private int icantidadcuotas;
    private java.util.Date dfecha;
    private java.util.Date dfechavencimiento;
    private double dmontoinicial;
    private double dmontomensual;
    private double dmontototal;
    private double dsaldo;
    private int iidEstado;
    private String sestado;
    private java.util.Date dfechaanulado;
    private String sobservacionanulado;

    private int iidpersona;
    private String scedularuc;
    private String srazonsocial;

    //General
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_CONCEPTO = 100;
    public static final int LONGITUD_OBSERVACION = 100;
    ;;;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CANTIDAD_CUOTAS = "Cantidad de cuotas (no vacía)";
    public static final String TEXTO_MONTO_MENSUAL = "Monto Mensual (no vacía)";
//    public static final String TEXTO_FACTURA = "Factura (De acuerdo al Tipo de Servicio)";
//    public static final String TEXTO_FECHA = "Fecha de Orden de Pago (no vacía)";
//    public static final String TEXTO_TOTAL = "Importe Total de la operación (no editable)";
//    public static final String TEXTO_RECEPTOR = "Receptor (no vacía)";
//    public static final String TEXTO_REMISION = "Representante para remisión del cheque";
//    public static final String TEXTO_PROMOTOR = "Promotor asignado a la operación.";
//    public static final String TEXTO_RECEPTOR_CARGADO = "Receptor del cheque según solicitud";
//    public static final String TEXTO_CENTRO_COSTO = "Centro de Costo (no vacío)";
//    public static final String TEXTO_BANCO_CUENTA = "Cuenta Bancaria para la Orden de Pago (no vacía)";
//    public static final String TEXTO_NUMERO_ORDEN = "Número de la boleta de Orden de Pago (no vacía)";
//    public static final String TEXTO_CONCEPTO = "Descripción de Orden de Pago (no vacía)";
//    public static final String TEXTO_REGIONAL = "Regional para la que se emite la orden";
//    public static final String TEXTO_ASIENTO = "Si fue asentado o no el depósito";
//    public static final String TEXTO_FILIAL = "Filial (no vacía)";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public static final String TEXTO_FECHA_CONFIRMADO = "Fecha de Confirmación (no editable)";
    public static final String TEXTO_OBSERVACION_CONFIRMADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public entPlanPago() {
        this.iid = 0;
        this.icantidadcuotas = 0;
        this.dfecha = null;
        this.dfechavencimiento = null;
        this.dmontoinicial = 0.0;
        this.dmontomensual = 0.0;
        this.dmontototal = 0.0;
        this.dsaldo = 0.0;
        this.iidEstado = 0;
        this.sestado = "";
        this.iidpersona = 0;
        this.scedularuc = "";
        this.srazonsocial = "";
        this.dfechaanulado = null;
        this.sobservacionanulado = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entPlanPago(int iid, int icantidadcuotas, java.util.Date dfecha, java.util.Date dfechavencimiento, double dmontoinicial, double dmontomensual, double dsaldo, int iidEstado, String sestado, int iidpersona, String scedula, String srazonsocial, double dmontototal, java.util.Date dfechaanulado, String sobservacionanulado, short hestadoRegistro) {
        this.iid = iid;
        this.icantidadcuotas = icantidadcuotas;
        this.dfecha = dfecha;
        this.dfechavencimiento = dfechavencimiento;
        this.dmontoinicial = dmontoinicial;
        this.dmontomensual = dmontomensual;
        this.dmontototal = dmontototal;
        this.dsaldo = dsaldo;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidpersona = iidpersona;
        this.scedularuc = scedula;
        this.srazonsocial = srazonsocial;
        this.dfechaanulado = dfechaanulado;
        this.sobservacionanulado = sobservacionanulado;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int icantidadcuotas, java.util.Date dfecha, java.util.Date dfechavencimiento, double dmontoinicial, double dmontomensual, double dsaldo, int iidEstado, String sestado, int iidpersona, String scedula, String srazonsocial, double dmontototal, java.util.Date dfechaanulado, String sobservacionanulado, short hestadoRegistro) {
        this.iid = iid;
        this.icantidadcuotas = icantidadcuotas;
        this.dfecha = dfecha;
        this.dfechavencimiento = dfechavencimiento;
        this.dmontoinicial = dmontoinicial;
        this.dmontomensual = dmontomensual;
        this.dmontototal = dmontototal;
        this.dsaldo = dsaldo;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidpersona = iidpersona;
        this.scedularuc = scedula;
        this.srazonsocial = srazonsocial;
        this.dfechaanulado = dfechaanulado;
        this.sobservacionanulado = sobservacionanulado;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entPlanPago copiar(entPlanPago destino) {
        destino.setEntidad(this.getId(), this.getCantidadcuotas(), this.getFecha(), this.getFechavencimiento(),
                this.getMontoinicial(), this.getMontoMensual(), this.getSaldo(), this.getIdEstado(), this.getEstado(), this.getIdPersona(), this.getCedularuc(), this.getRazonsocial(), this.getMontoTotal(), this.getFechaanulado(), this.getObservacionanulado(), this.getEstadoRegistro());
        return destino;
    }

    public entPlanPago cargar(java.sql.ResultSet rs) {
        try {
            this.setId(rs.getInt("id"));
        } catch (Exception e) {
        }
        try {
            this.setCantidadcuotas(rs.getInt("cantidadcuotas"));
        } catch (Exception e) {
        }
        try {
            this.setFecha(rs.getDate("fecha"));
        } catch (Exception e) {
        }
        try {
            this.setFechavencimiento(rs.getDate("fechavencimiento"));
        } catch (Exception e) {
        }
        try {
            this.setMontoinicial(rs.getDouble("montoinicial"));
        } catch (Exception e) {
        }
        try {
            this.setSaldo(rs.getDouble("saldo"));
        } catch (Exception e) {
        }
        try {
            this.setMontoMensual(rs.getDouble("montomensual"));
        } catch (Exception e) {
        }
        try {
            this.setMontoTotal(rs.getDouble("montototal"));
        } catch (Exception e) {
        }
        try {
            this.setIdPersona(rs.getInt("idpersona"));
        } catch (Exception e) {
        }

        try {
            this.setIdEstado(rs.getInt("idestado"));
        } catch (Exception e) {
        }
        try {
            this.setEstado(rs.getString("estadoplan"));
        } catch (Exception e) {
        }
        try {
            this.setCedularuc(rs.getString("cedularuc"));
        } catch (Exception e) {
        }
        try {
            this.setFechaanulado(rs.getDate("fechaanulado"));
        } catch (Exception e) {
        }
        try {
            this.setObservacionanulado(rs.getString("observacionanulado"));
        } catch (Exception e) {
        }
        try {
            this.setRazonsocial(rs.getString("razonsocial"));
        } catch (Exception e) {
        }
        return this;
    }

    public int getId() {
        return iid;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public int getCantidadcuotas() {
        return icantidadcuotas;
    }

    public void setCantidadcuotas(int icantidadcuotas) {
        this.icantidadcuotas = icantidadcuotas;
    }

    public Date getFecha() {
        return dfecha;
    }

    public void setFecha(Date dfecha) {
        this.dfecha = dfecha;
    }

    public Date getFechavencimiento() {
        return dfechavencimiento;
    }

    public void setFechavencimiento(Date dfechavencimiento) {
        this.dfechavencimiento = dfechavencimiento;
    }

    public double getMontoinicial() {
        return dmontoinicial;
    }

    public void setMontoinicial(double dmontoinicial) {
        this.dmontoinicial = dmontoinicial;
    }

    public double getMontoMensual() {
        return dmontomensual;
    }

    public void setMontoMensual(double dmontomensual) {
        this.dmontomensual = dmontomensual;
    }

    public double getMontoTotal() {
        return dmontototal;
    }

    public void setMontoTotal(double dmontototal) {
        this.dmontototal = dmontototal;
    }

    public double getSaldo() {
        return dsaldo;
    }

    public void setSaldo(double dsaldo) {
        this.dsaldo = dsaldo;
    }

    public int getIdEstado() {
        return iidEstado;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public String getEstado() {
        return sestado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public int getIdPersona() {
        return iidpersona;
    }

    public void setIdPersona(int iidpersona) {
        this.iidpersona = iidpersona;
    }

    public short getEstadoRegistro() {
        return hestadoRegistro;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public String getMensaje() {
        return smensaje;
    }

    public void setMensaje(String smensaje) {
        this.smensaje = smensaje;
    }

    public String getRazonsocial() {
        return srazonsocial;
    }

    public void setRazonsocial(String srazonsocial) {
        this.srazonsocial = srazonsocial;
    }

    public String getCedularuc() {
        return scedularuc;
    }

    public void setCedularuc(String scedularuc) {
        this.scedularuc = scedularuc;
    }

    public Date getFechaanulado() {
        return dfechaanulado;
    }

    public void setFechaanulado(Date dfechaanulado) {
        this.dfechaanulado = dfechaanulado;
    }

    public String getObservacionanulado() {
        return sobservacionanulado;
    }

    public void setObservacionanulado(String sobservacionanulado) {
        this.sobservacionanulado = sobservacionanulado;
    }

    public boolean esValidoSolicitar() {
        boolean bvalido = true;
        this.smensaje = "";
//        if (this.getIdCentroCosto()<=0) {
//            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
//            this.smensaje += this.TEXTO_CENTRO_COSTO;
//            bvalido = false;
//        }
//        if (this.getFecha() == null) {
//            if (!this.smensaje.isEmpty()) {
//                this.smensaje += "\n";
//            }
//            this.smensaje += this.TEXTO_FECHA;
//            bvalido = false;
//        }
        if (this.getCantidadcuotas() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_CANTIDAD_CUOTAS;
            bvalido = false;
        }
        if (this.getMontoMensual() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_MONTO_MENSUAL;
            bvalido = false;
        }
//        if (this.getIdfacturaingreso() <= 0) {
//            if (!this.smensaje.isEmpty()) {
//                this.smensaje += "\n";
//            }
//            this.smensaje += this.TEXTO_FACTURA;
//            bvalido = false;
//        }
        return bvalido;
    }

//    public boolean esValidoAnular() {
//        boolean bvalido = true;
//        this.smensaje = "";
//        if (this.getObservacionAnulado().isEmpty()) {
//            if (!this.smensaje.isEmpty()) {
//                this.smensaje += "\n";
//            }
//            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
//            bvalido = false;
//        }
//        return bvalido;
//    }
    public String getSentencia() {
        return "SELECT public.planpago("
                + this.getId() + ","
                + this.getCantidadcuotas() + ","
                + utilitario.utiFecha.getFechaGuardadoMac(this.getFecha()) + ","
                + utilitario.utiFecha.getFechaGuardadoMac(this.getFechavencimiento()) + ","
                + this.getMontoinicial() + ","
                + this.getMontoMensual() + ","
                + this.getSaldo() + ","
                + this.getIdEstado() + ","
                + this.getMontoTotal() + ","
                + this.getIdPersona() + ","
                + utilitario.utiFecha.getFechaGuardadoMac(this.getFechaanulado()) + ","
                + utilitario.utiCadena.getTextoGuardado(this.getObservacionanulado()) + ","
                + this.getEstadoRegistro() + ")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
//        lst.add(new generico.entLista(2, "Número de Orden", "nroordenpago", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(1, "Nro", "id", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Fecha", "fecha", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Fecha Vencimiento", "fechavencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(4, "Cédula o RUC", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Razón Social", "razonsocial", generico.entLista.tipo_texto));
        return lst;
    }

}
