/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDeposito {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String sapellidoNombre;
    private int iidBanco;
    private String sbanco;
    private int iidCiudad;
    private String sciudad;
    private int iidTipoDeposito;
    private String stipoDeposito;
    private int iidDetalleValor;
    private int iidDocumento;
    private String sdocumento;
    private String snumeroDocumento;
    private java.util.Date dfechaDocumento;
    private int iidTipoDocumento;
    private String stipoDocumento;
    private String scuentaDeposito;
    private String scuentaOrigen;
    private double uimporte;
    private int iidMoneda;
    private String smoneda;
    private java.util.Date dfechaRegistro;
    private int iidEstado;
    private String sestado;
    private java.util.Date dfechaProceso;
    private String sobservacionProceso;
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    private int iidUsuario;
    private String sapellidoNombreUsuario;
    private int iidDependencia;
    private String sdependencia;
    private java.util.Date dfechaReciboDocumento;
    private String sobservacionReciboDocumento;
    private java.util.Date dfechaConciliado;
    private String sobservacionConciliado;
    private int iestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_NUMERO_DOCUMENTO = 10;
    public static final int LONGITUD_CUENTA = 15;
    public static final int LONGITUD_OBSERVACION = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_BANCO = "Banco del depósito (no vacío)";
    public static final String TEXTO_CIUDAD = "Ciudad del depósito (no vacío)";
    public static final String TEXTO_TIPO_DEPOSITO = "Tipo de depósito (no vacío)";
    public static final String TEXTO_DOCUMENTO = "Documento (no vacío)";
    public static final String TEXTO_NUMERO_DOCUMENTO = "Número de documento (no vacío, hasta " + LONGITUD_NUMERO_DOCUMENTO + " caracteres)";
    public static final String TEXTO_FECHA_DOCUMENTO = "Fecha del documento";
    public static final String TEXTO_TIPO_DOCUMENTO = "Tipo de documento (no vacío)";
    public static final String TEXTO_CUENTA_DEPOSITO = "Número de Cuenta Bancaria de depósito (no vacío, hasta " + LONGITUD_CUENTA + " caracteres)";
    public static final String TEXTO_CUENTA_ORIGEN = "Número de Cuenta Bancaria origen (hasta " + LONGITUD_CUENTA + " caracteres)";
    public static final String TEXTO_IMPORTE = "Importe (valor positivo, mayor o igual al Importe base)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_FECHA_REGISTRO = "Fecha de registro (no vacía)";
    public static final String TEXTO_ESTADO = "Estado de Depósito (no editable)";
    public static final String TEXTO_FECHA_PROCESO = "Fecha de Proceso (no vacía)";
    public static final String TEXTO_OBSERVACION_PROCESO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_USUARIO = "Usuario (no editable)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia (no editable)";
    public static final String TEXTO_FECHA_RECIBO_DOCUMENTO = "Fecha de Recibo de documento (no editable)";
    public static final String TEXTO_OBSERVACION_RECIBO_DOCUMENTO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_CONCILIADO = "Fecha de Conciliación (no editable)";
    public static final String TEXTO_OBSERVACION_CONCILIADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public entDeposito() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.sapellidoNombre = "";
        this.iidBanco = 0;
        this.sbanco = "";
        this.iidCiudad = 0;
        this.sciudad = "";
        this.iidTipoDeposito = 0;
        this.stipoDeposito = "";
        this.iidDetalleValor = 0;
        this.iidDocumento = 0;
        this.sdocumento = "";
        this.snumeroDocumento = "";
        this.dfechaDocumento = null;
        this.iidTipoDocumento = 0;
        this.stipoDocumento = "";
        this.scuentaDeposito = "";
        this.scuentaOrigen = "";
        this.uimporte = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.dfechaRegistro = null;
        this.iidEstado = 0;
        this.sestado = "";
        this.dfechaProceso = null;
        this.sobservacionProceso = "";
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.iidUsuario = 0;
        this.sapellidoNombreUsuario = "";
        this.iidDependencia = 0;
        this.sdependencia = "";
        this.dfechaReciboDocumento = null;
        this.sobservacionReciboDocumento = "";
        this.dfechaConciliado = null;
        this.sobservacionConciliado = "";
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDeposito(int iid, int iidSocio, String scedula, String sapellidoNombre, int iidBanco, String sbanco, int iidCiudad, String sciudad, int iidTipoDeposito, String stipoDeposito, int iidDetalleValor, int iidDocumento, String sdocumento, String snumeroDocumento, java.util.Date dfechaDocumento, int iidTipoDocumento, String stipoDocumento, String scuentaDeposito, String scuentaOrigen, double uimporte, int iidMoneda, String smoneda, java.util.Date dfechaRegistro, int iidEstado, String sestado, java.util.Date dfechaProceso, String sobservacionProceso, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidUsuario, String sapellidoNombreUsuario, int iidDependencia, String sdependencia, java.util.Date dfechaReciboDocumento, String sobservacionReciboDocumento, java.util.Date dfechaConciliado, String sobservacionConciliado, int iestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.iidTipoDeposito = iidTipoDeposito;
        this.stipoDeposito = stipoDeposito;
        this.iidDetalleValor = iidDetalleValor;
        this.iidDocumento = iidDocumento;
        this.sdocumento = sdocumento;
        this.snumeroDocumento = snumeroDocumento;
        this.dfechaDocumento = dfechaDocumento;
        this.iidTipoDocumento = iidTipoDocumento;
        this.stipoDocumento = stipoDocumento;
        this.scuentaDeposito = scuentaDeposito;
        this.scuentaOrigen = scuentaOrigen;
        this.uimporte = uimporte;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.dfechaRegistro = dfechaRegistro;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.dfechaProceso = dfechaProceso;
        this.sobservacionProceso = sobservacionProceso;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidUsuario = iidUsuario;
        this.sapellidoNombreUsuario = sapellidoNombreUsuario;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.dfechaReciboDocumento = dfechaReciboDocumento;
        this.sobservacionReciboDocumento = sobservacionReciboDocumento;
        this.dfechaConciliado = dfechaConciliado;
        this.sobservacionConciliado = sobservacionConciliado;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, String sapellidoNombre, int iidBanco, String sbanco, int iidCiudad, String sciudad, int iidTipoDeposito, String stipoDeposito, int iidDetalleValor, int iidDocumento, String sdocumento, String snumeroDocumento, java.util.Date dfechaDocumento, int iidTipoDocumento, String stipoDocumento, String scuentaDeposito, String scuentaOrigen, double uimporte, int iidMoneda, String smoneda, java.util.Date dfechaRegistro, int iidEstado, String sestado, java.util.Date dfechaProceso, String sobservacionProceso, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidUsuario, String sapellidoNombreUsuario, int iidDependencia, String sdependencia, java.util.Date dfechaReciboDocumento, String sobservacionReciboDocumento, java.util.Date dfechaConciliado, String sobservacionConciliado, int iestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.iidTipoDeposito = iidTipoDeposito;
        this.stipoDeposito = stipoDeposito;
        this.iidDetalleValor = iidDetalleValor;
        this.iidDocumento = iidDocumento;
        this.sdocumento = sdocumento;
        this.snumeroDocumento = snumeroDocumento;
        this.dfechaDocumento = dfechaDocumento;
        this.iidTipoDocumento = iidTipoDocumento;
        this.stipoDocumento = stipoDocumento;
        this.scuentaDeposito = scuentaDeposito;
        this.scuentaOrigen = scuentaOrigen;
        this.uimporte = uimporte;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.dfechaRegistro = dfechaRegistro;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.dfechaProceso = dfechaProceso;
        this.sobservacionProceso = sobservacionProceso;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidUsuario = iidUsuario;
        this.sapellidoNombreUsuario = sapellidoNombreUsuario;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.dfechaReciboDocumento = dfechaReciboDocumento;
        this.sobservacionReciboDocumento = sobservacionReciboDocumento;
        this.dfechaConciliado = dfechaConciliado;
        this.sobservacionConciliado = sobservacionConciliado;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entDeposito copiar(entDeposito destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getApellidoNombre(), this.getIdBanco(), this.getBanco(), this.getIdCiudad(), this.getCiudad(), this.getIdTipoDeposito(), this.getTipoDeposito(), this.getIdDetalleValor(), this.getIdDocumento(), this.getDocumento(), this.getNumeroDocumento(), this.getFechaDocumento(), this.getIdTipoDocumento(), this.getTipoDocumento(), this.getCuentaDeposito(), this.getCuentaOrigen(), this.getImporte(), this.getIdMoneda(), this.getMoneda(), this.getFechaRegistro(), this.getIdEstado(), this.getEstado(), this.getFechaProceso(), this.getObservacionProceso(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getIdUsuario(), this.getApellidoNombreUsuario(), this.getIdDependencia(), this.getDependencia(), this.getFechaReciboDocumento(), this.getObservacionReciboDocumento(), this.getFechaConciliado(), this.getObservacionConciliado(), this.getEstadoRegistro());
        return destino;
    }
    
    public entDeposito cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setApellidoNombre(rs.getString("apellido")+", "+rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setIdBanco(rs.getInt("idbanco")); }
        catch(Exception e) {}
        try { this.setBanco(rs.getString("banco")); }
        catch(Exception e) {}
        try { this.setIdCiudad(rs.getInt("idciudad")); }
        catch(Exception e) {}
        try { this.setCiudad(rs.getString("ciudad")); }
        catch(Exception e) {}
        try { this.setIdTipoDeposito(rs.getInt("idtipodeposito")); }
        catch(Exception e) {}
        try { this.setTipoDeposito(rs.getString("tipodeposito")); }
        catch(Exception e) {}
        try { this.setImporte(rs.getDouble("importe")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setIdDetalleValor(rs.getInt("iddetallevalor")); }
        catch(Exception e) {}
        try { this.setIdDocumento(rs.getInt("iddocumento")); }
        catch(Exception e) {}
        try { this.setDocumento(rs.getString("documento")); }
        catch(Exception e) {}
        try { this.setNumeroDocumento(rs.getString("numerodocumento")); }
        catch(Exception e) {}
        try { this.setFechaDocumento(rs.getDate("fechadocumento")); }
        catch(Exception e) {}
        try { this.setIdTipoDocumento(rs.getInt("idtipodocumento")); }
        catch(Exception e) {}
        try { this.setTipoDocumento(rs.getString("tipodocumento")); }
        catch(Exception e) {}
        try { this.setCuentaDeposito(rs.getString("cuentadeposito")); }
        catch(Exception e) {}
        try { this.setCuentaOrigen(rs.getString("cuentaorigen")); }
        catch(Exception e) {}
        try { this.setImporte(rs.getDouble("importe")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setFechaRegistro(rs.getDate("fecharegistro")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setFechaProceso(rs.getDate("fechaproceso")); }
        catch(Exception e) {}
        try { this.setObservacionProceso(rs.getString("observacionproceso")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setApellidoNombreUsuario(rs.getString("apellidousuario")+", "+rs.getString("nombreusuario")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        try { this.setFechaReciboDocumento(rs.getDate("fecharecibodocumento")); }
        catch(Exception e) {}
        try { this.setObservacionReciboDocumento(rs.getString("observacionrecibodocumento")); }
        catch(Exception e) {}
        try { this.setFechaConciliado(rs.getDate("fechaconciliado")); }
        catch(Exception e) {}
        try { this.setObservacionConciliado(rs.getString("observacionconciliado")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setApellidoNombre(String sapellidoNombre) {
        this.sapellidoNombre = sapellidoNombre;
    }

    public void setIdBanco(int iidBanco) {
        this.iidBanco = iidBanco;
    }

    public void setBanco(String sbanco) {
        this.sbanco = sbanco;
    }

    public void setIdCiudad(int iidCiudad) {
        this.iidCiudad = iidCiudad;
    }

    public void setCiudad(String sciudad) {
        this.sciudad = sciudad;
    }

    public void setIdTipoDeposito(int iidTipoDeposito) {
        this.iidTipoDeposito = iidTipoDeposito;
    }

    public void setTipoDeposito(String stipoDeposito) {
        this.stipoDeposito = stipoDeposito;
    }
    
    public void setIdDetalleValor(int iidDetalleValor) {
        this.iidDetalleValor = iidDetalleValor;
    }

    public void setIdDocumento(int iidDocumento) {
        this.iidDocumento = iidDocumento;
    }

    public void setDocumento(String sdocumento) {
        this.sdocumento = sdocumento;
    }

    public void setNumeroDocumento(String snumeroDocumento) {
        this.snumeroDocumento = snumeroDocumento;
    }

    public void setFechaDocumento(java.util.Date dfechaDocumento) {
        this.dfechaDocumento = dfechaDocumento;
    }

    public void setIdTipoDocumento(int iidTipoDocumento) {
        this.iidTipoDocumento = iidTipoDocumento;
    }

    public void setTipoDocumento(String stipoDocumento) {
        this.stipoDocumento = stipoDocumento;
    }

    public void setCuentaDeposito(String scuentaDeposito) {
        this.scuentaDeposito = scuentaDeposito;
    }

    public void setCuentaOrigen(String scuentaOrigen) {
        this.scuentaOrigen = scuentaOrigen;
    }

    public void setImporte(double uimporte) {
        this.uimporte = uimporte;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }

    public void setFechaRegistro(java.util.Date dfechaRegistro) {
        this.dfechaRegistro = dfechaRegistro;
    }
    
    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }
    
    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setFechaProceso(java.util.Date dfechaProceso) {
        this.dfechaProceso = dfechaProceso;
    }

    public void setObservacionProceso(String sobservacionProceso) {
        this.sobservacionProceso = sobservacionProceso;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }

    public void setApellidoNombreUsuario(String sapellidoNombreUsuario) {
        this.sapellidoNombreUsuario = sapellidoNombreUsuario;
    }

    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }

    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public void setFechaReciboDocumento(java.util.Date dfechaReciboDocumento) {
        this.dfechaReciboDocumento = dfechaReciboDocumento;
    }

    public void setObservacionReciboDocumento(String sobservacionReciboDocumento) {
        this.sobservacionReciboDocumento = sobservacionReciboDocumento;
    }

    public void setFechaConciliado(java.util.Date dfechaConciliado) {
        this.dfechaConciliado = dfechaConciliado;
    }

    public void setObservacionConciliado(String sobservacionConciliado) {
        this.sobservacionConciliado = sobservacionConciliado;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getApellidoNombre() {
        return this.sapellidoNombre;
    }

    public int getIdBanco() {
        return this.iidBanco;
    }

    public String getBanco() {
        return this.sbanco;
    }

    public int getIdCiudad() {
        return this.iidCiudad;
    }

    public String getCiudad() {
        return this.sciudad;
    }

    public int getIdTipoDeposito() {
        return this.iidTipoDeposito;
    }

    public String getTipoDeposito() {
        return this.stipoDeposito;
    }
    
    public int getIdDetalleValor() {
        return this.iidDetalleValor;
    }

    public int getIdDocumento() {
        return this.iidDocumento;
    }

    public String getDocumento() {
        return this.sdocumento;
    }

    public String getNumeroDocumento() {
        return this.snumeroDocumento;
    }

    public java.util.Date getFechaDocumento() {
        return this.dfechaDocumento;
    }

    public int getIdTipoDocumento() {
        return this.iidTipoDocumento;
    }

    public String getTipoDocumento() {
        return this.stipoDocumento;
    }

    public String getCuentaDeposito() {
        return this.scuentaDeposito;
    }

    public String getCuentaOrigen() {
        return this.scuentaOrigen;
    }

    public double getImporte() {
        return this.uimporte;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }
    
    public String getMoneda() {
        return this.smoneda;
    }

    public java.util.Date getFechaRegistro() {
        return this.dfechaRegistro;
    }
    
    public int getIdEstado() {
        return this.iidEstado;
    }
    
    public String getEstado() {
        return this.sestado;
    }

    public java.util.Date getFechaProceso() {
        return this.dfechaProceso;
    }

    public String getObservacionProceso() {
        return this.sobservacionProceso;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }

    public String getApellidoNombreUsuario() {
        return this.sapellidoNombreUsuario;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }

    public String getDependencia() {
        return this.sdependencia;
    }

    public java.util.Date getFechaReciboDocumento() {
        return this.dfechaReciboDocumento;
    }

    public String getObservacionReciboDocumento() {
        return this.sobservacionReciboDocumento;
    }

    public java.util.Date getFechaConciliado() {
        return this.dfechaConciliado;
    }

    public String getObservacionConciliado() {
        return this.sobservacionConciliado;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        this.smensaje = "";
        if (this.getIdSocio()<=0) this.smensaje += TEXTO_SOCIO + "\n";
        if (this.getIdBanco()<=0) this.smensaje += TEXTO_BANCO + "\n";
        if (this.getIdCiudad()<=0) this.smensaje += TEXTO_CIUDAD + "\n";
        if (this.getIdTipoDeposito()<=0) this.smensaje += TEXTO_TIPO_DEPOSITO + "\n";
        if (this.getIdDocumento()<=0) this.smensaje += TEXTO_DOCUMENTO + "\n";
        if (this.getNumeroDocumento().isEmpty()) this.smensaje += TEXTO_NUMERO_DOCUMENTO + "\n";
        if (this.getIdTipoDocumento()<=0) this.smensaje += TEXTO_TIPO_DOCUMENTO + "\n";
        if (this.getCuentaDeposito().isEmpty()) this.smensaje += TEXTO_CUENTA_DEPOSITO + "\n";
        if (this.getCuentaOrigen().isEmpty()) this.smensaje += TEXTO_CUENTA_ORIGEN + "\n";
        if (this.getImporte()<0) this.smensaje += TEXTO_IMPORTE + "\n";
        if (this.getIdMoneda()<=0) this.smensaje += TEXTO_MONEDA + "\n";
        if (this.getFechaRegistro()==null) this.smensaje += TEXTO_FECHA_REGISTRO + "\n";
        if (this.getIdEstado()<=0) this.smensaje += TEXTO_ESTADO + "\n";
        if (this.getIdUsuario()<=0) this.smensaje += TEXTO_USUARIO + "\n";
        if (this.getIdDependencia()<=0) this.smensaje += TEXTO_DEPENDENCIA + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }

    public boolean esValidoProcesar() {
        this.smensaje = "";
        if (this.getObservacionProceso().isEmpty()) this.smensaje += TEXTO_OBSERVACION_PROCESO + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public boolean esValidoAnular() {
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) this.smensaje += TEXTO_OBSERVACION_ANULADO + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public boolean esValidoRecibirDocumento() {
        this.smensaje = "";
        if (this.getObservacionReciboDocumento().isEmpty()) this.smensaje += TEXTO_OBSERVACION_RECIBO_DOCUMENTO + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public boolean esValidoConciliar() {
        this.smensaje = "";
        if (this.getObservacionConciliado().isEmpty()) this.smensaje += TEXTO_OBSERVACION_CONCILIADO + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }

    public String getSentencia() {
        return "SELECT deposito("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdBanco()+","+
            this.getIdCiudad()+","+
            this.getIdTipoDeposito()+","+
            this.getIdDetalleValor()+","+
            this.getIdDocumento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumeroDocumento())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaDocumento())+","+
            this.getIdTipoDocumento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCuentaDeposito())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCuentaOrigen())+","+
            this.getImporte()+","+
            this.getIdMoneda()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaRegistro())+","+
            this.getIdEstado()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaProceso())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionProceso())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            this.getIdUsuario()+","+
            this.getIdDependencia()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaReciboDocumento())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionReciboDocumento())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaConciliado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionConciliado())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Apellido y Nombre", "apellidonombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Contrato", "contrato", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Número Operacion", "numerooperacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(5, "Plan", "plan", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Fecha Operación", "fechaoperacion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(7, "Fecha Primer Vencimiento", "fechaprimervencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(8, "Fecha Vencimiento", "fechavencimiento", generico.entLista.tipo_fecha));
        return lst;
    }

}
