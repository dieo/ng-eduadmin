/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entRepresentante extends generico.entGenericaPersona {

    protected int iidLocalidad;
    protected String slocalidad;
    protected int iidRegional;
    protected String sregional;
    protected int iidTipoRepresentante;
    protected String stipoRepresentante;
    protected String scedula;
    protected String stelefono;
    protected boolean bdestinatario;

    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final int LONGITUD_CEDULA = 12;
    public static final int LONGITUD_TELEFONO = 40;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NOMBRE = "Nombre del Representante (no vacía, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido del Representante (no vacía, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_CEDULA = "Cédula del Representante (no vacía, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_LOCALIDAD = "Localidad (no vacía)";
    public static final String TEXTO_REGIONAL = "Regional (no vacío)";
    public static final String TEXTO_TIPO_REPRESENTANTE = "Tipo Representante (no vacío)";
    public static final String TEXTO_TELEFONO = "Teléfono";
    public static final String TEXTO_DESTINATARIO = "Destinatario";

    public entRepresentante() {
        super();
        this.iidLocalidad = 0;
        this.slocalidad = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.iidTipoRepresentante = 0;
        this.stipoRepresentante = "";
        this.scedula = "";
        this.stelefono = "";
        this.bdestinatario = false;
    }

    public entRepresentante(int iid, String snombre, String sapellido, int iidLocalidad, String slocalidad, int iidRegional, String sregional, int iidTipoRepresentante, String stipoRepresentante, String scedula, String stelefono, boolean bdestinatario) {
        super(iid, snombre, sapellido, "", (short)0);
        this.iidLocalidad = iidLocalidad;
        this.slocalidad = slocalidad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidTipoRepresentante = iidTipoRepresentante;
        this.stipoRepresentante = stipoRepresentante;
        this.scedula = scedula;
        this.stelefono = stelefono;
        this.bdestinatario = bdestinatario;
    }

    public entRepresentante(int iid, String snombre, String sapellido, int iidLocalidad, String slocalidad, int iidRegional, String sregional, int iidTipoRepresentante, String stipoRepresentante, String scedula, String stelefono, boolean bdestinatario, short hestadoRegistro) {
        super(iid, snombre, sapellido, "", hestadoRegistro);
        this.iidLocalidad = iidLocalidad;
        this.slocalidad = slocalidad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidTipoRepresentante = iidTipoRepresentante;
        this.stipoRepresentante = stipoRepresentante;
        this.scedula = scedula;
        this.stelefono = stelefono;
        this.bdestinatario = bdestinatario;
    }

    public void setEntidad(int iid, String snombre, String sapellido, int iidLocalidad, String slocalidad, int iidRegional, String sregional, int iidTipoRepresentante, String stipoRepresentante, String scedula, String stelefono, boolean bdestinatario, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.iidLocalidad = iidLocalidad;
        this.slocalidad = slocalidad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidTipoRepresentante = iidTipoRepresentante;
        this.stipoRepresentante = stipoRepresentante;
        this.scedula = scedula;
        this.stelefono = stelefono;
        this.bdestinatario = bdestinatario;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entRepresentante copiar(entRepresentante destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getIdLocalidad(), this.getLocalidad(), this.getIdRegional(), this.getRegional(), this.getIdTipoRepresentante(), this.getTipoRepresentante(), this.getCedula(), this.getTelefono(), this.getDestinatario(), this.getEstadoRegistro());
        return destino;
    }

    public entRepresentante cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setIdLocalidad(rs.getInt("idlocalidad")); }
        catch(Exception e) {}
        try { this.setLocalidad(rs.getString("localidad")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setIdTipoRepresentante(rs.getInt("idtiporepresentante")); }
        catch(Exception e) {}
        try { this.setTipoRepresentante(rs.getString("tiporepresentante")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setDestinatario(rs.getBoolean("destinatario")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdLocalidad(int iidLocalidad) {
        this.iidLocalidad = iidLocalidad;
    }

    public void setLocalidad(String slocalidad) {
        this.slocalidad = slocalidad;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setIdTipoRepresentante(int iidTipoRepresentante) {
        this.iidTipoRepresentante = iidTipoRepresentante;
    }

    public void setTipoRepresentante(String stipoRepresentante) {
        this.stipoRepresentante = stipoRepresentante;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setDestinatario(boolean bdestinatario) {
        this.bdestinatario = bdestinatario;
    }

    public int getIdLocalidad() {
        return this.iidLocalidad;
    }

    public String getLocalidad() {
        return this.slocalidad;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public int getIdTipoRepresentante() {
        return this.iidTipoRepresentante;
    }

    public String getTipoRepresentante() {
        return this.stipoRepresentante;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getTelefono() {
        return this.stelefono;
    }

    public boolean getDestinatario() {
        return this.bdestinatario;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_APELLIDO;
            bvalido = false;
        }
        if (this.getIdLocalidad()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_LOCALIDAD;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        if (this.getIdTipoRepresentante()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_REPRESENTANTE;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT representante("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            this.getIdLocalidad()+","+
            this.getIdRegional()+","+
            this.getIdTipoRepresentante()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getDestinatario()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Apellido", "apellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Localidad", "localidad", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Regional", "regional", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Tipo Representante", "tiporepresentante", generico.entLista.tipo_texto));
        return lst;
    }
    
}
