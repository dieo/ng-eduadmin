/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entcInventario {

    private int iid;
    private int iidPresupuesto;
    private int iidUsuario;
    private String susuario;
    private int iidAutorizador;
    private String sautorizador;
    private int iidProveedor;
    private String sproveedor;
    private int iidFilial;
    private String sfilial;
    private int iidDependencia;
    private String sdependencia;
    private int iidTipoPedido;
    private String stipoPedido;
    private java.util.Date dfecha;
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    private int iidMoneda;
    private String smoneda;
    private int iidEstadoInventario;
    private String sestadoInventario;
    private double utotalOrdenCompra;
    private double ucambio;
    private double utotalGs;
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_IDPRESUPUESTO = "Nº de Presupuesto en que se basa la OC (0 si no esta relacionado con Presupuesto)";
    public static final String TEXTO_USUARIO = "Usuario que realiza el inventario (no vacía)";
    public static final String TEXTO_AUTORIZADOR = "Persona que autoriza la Compra";
    public static final String TEXTO_FILIAL = "Filial para la que se realiza el inventario (no vacía)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia del usuario (no vacía)";
    public static final String TEXTO_PROVEEDOR = "Proveedor del artículo (no vacía)";
    public static final String TEXTO_FECHA = "Fecha del Inventario (no vacía)";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación del Inventario (no editable)";
    public static final String TEXTO_TOTAL_ORDEN_COMPRA = "Monto total de la OC (no editable)";
    public static final String TEXTO_CAMBIO = "Monto del cambio del día de la moneda (no vacía, 1 si en Gs)";
    public static final String TEXTO_TOTAL_GS = "Monto total de la OC en Gs (no editable)";
    public static final String TEXTO_MONEDA = "Moneda en que se cotiza la OC (no vacía)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Motivo de la Anulación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_TIPO_PEDIDO = "Tipo de Pedido de la OC (no vacía)";
    public static final String TEXTO_ESTADO = "Estado de la OC (no editable)";
    
    
    public entcInventario() {
        this.iid = 0;
        this.iidPresupuesto = 0;
        this.iidUsuario = 0;
        this.susuario = "";
        this.iidAutorizador = 0;
        this.sautorizador = "";
        this.iidProveedor = 0;
        this.sproveedor = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.iidDependencia = 0;
        this.sdependencia = "";
        this.iidTipoPedido = 0;
        this.stipoPedido = "";
        this.dfecha = null;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iidEstadoInventario = 0;
        this.sestadoInventario = "";
        this.utotalOrdenCompra = 0.0;
        this.ucambio = 0.0;
        this.utotalGs = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entcInventario(int iid, int iidPresupuesto, int iidUsuario, String susuario, int iidAutorizador, String sautorizador, int iidProveedor, String sproveedor, int iidFilial, String sfilial, int iidDependencia, String sdependencia, int iidTipoPedido, String stipoPedido, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidMoneda,  String smoneda, int iidEstadoInventario, String sestadoInventario, double utotalOrdenCompra, double ucambio, double utotalGs, short hestadoRegistro) {
        this.iid = iid;
        this.iidPresupuesto = iidPresupuesto;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidTipoPedido = iidTipoPedido;
        this.stipoPedido = stipoPedido;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidEstadoInventario = iidEstadoInventario;
        this.sestadoInventario = sestadoInventario;
        this.utotalOrdenCompra = utotalOrdenCompra;
        this.ucambio = ucambio;
        this.utotalGs = utotalGs;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidPresupuesto, int iidUsuario, String susuario, int iidAutorizador, String sautorizador, int iidProveedor, String sproveedor, int iidFilial, String sfilial, int iidDependencia, String sdependencia, int iidTipoPedido, String stipoPedido, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidMoneda,  String smoneda, int iidEstadoInventario, String sestadoInventario, double utotalOrdenCompra, double ucambio, double utotalGs, short hestadoRegistro) {
        this.iid = iid;
        this.iidPresupuesto = iidPresupuesto;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidTipoPedido = iidTipoPedido;
        this.stipoPedido = stipoPedido;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidEstadoInventario = iidEstadoInventario;
        this.sestadoInventario = sestadoInventario;
        this.utotalOrdenCompra = utotalOrdenCompra;
        this.ucambio = ucambio;
        this.utotalGs = utotalGs;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entcInventario copiar(entcInventario destino) {
        destino.setEntidad(this.getId(), this.getIdPresupuesto(), this.getIdUsuario(), this.getUsuario(), this.getIdAutorizador(), this.getAutorizador(), this.getIdProveedor(), this.getProveedor(), this.getIdFilial(), this.getFilial(), this.getIdDependencia(), this.getDependencia(), this.getIdTipoPedido(), this.getTipoPedido(), this.getFecha(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getIdMoneda(), this.getMoneda(), this.getIdEstadoInventario(), this.getEstadoInventario(), this.getTotalOrdenCompra(), this.getCambio(), this.getTotalGs(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcInventario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        //try { this.setIdPresupuesto(rs.getInt("idpresupuesto")); }
        //catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuario(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        //try { this.setIdAutorizador(rs.getInt("idautorizador")); }
        //catch(Exception e) {}
        //try { this.setAutorizador(rs.getString("autorizador")); }
        //catch(Exception e) {}
        //try { this.setIdProveedor(rs.getInt("idproveedor")); }
        //catch(Exception e) {}
        //try { this.setProveedor(rs.getString("proveedor")); }
        //catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        //try { this.setIdTipoPedido(rs.getInt("idtipopedido")); }
        //catch(Exception e) {}
        //try { this.setTipoPedido(rs.getString("tipopedido")); }
        //catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        //try { this.setIdMoneda(rs.getInt("idmoneda")); }
        //catch(Exception e) {}
        //try { this.setMoneda(rs.getString("moneda")); }
        //catch(Exception e) {}
        try { this.setIdEstadoInventario(rs.getInt("idestadoinventario")); }
        catch(Exception e) {}
        try { this.setEstadoInventario(rs.getString("estadoinventario")); }
        catch(Exception e) {}
        //try { this.setTotalOrdenCompra(rs.getDouble("totalordencompra")); }
        //catch(Exception e) {}
        //try { this.setCambio(rs.getDouble("cambio")); }
        //catch(Exception e) {}
        //try { this.setTotalGs(rs.getDouble("totalgs")); }
        //catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdPresupuesto(int iidPresupuesto) {
        this.iidPresupuesto = iidPresupuesto;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }
    
    public void setUsuario(String susuario) {
        this.susuario = susuario;
    }

    public void setIdAutorizador(int iidAutorizador) {
        this.iidAutorizador = iidAutorizador;
    }
    
    public void setAutorizador(String sautorizador) {
        this.sautorizador = sautorizador;
    }

    public void setIdProveedor(int iidProveedor) {
        this.iidProveedor = iidProveedor;
    }
    
    public void setProveedor(String sproveedor) {
        this.sproveedor = sproveedor;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }
    
    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public void setIdTipoPedido(int iidTipoPedido) {
        this.iidTipoPedido = iidTipoPedido;
    }
    
    public void setTipoPedido(String stipoPedido) {
        this.stipoPedido = stipoPedido;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }
    
    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setIdEstadoInventario(int iidEstadoInventario) {
        this.iidEstadoInventario = iidEstadoInventario;
    }

    public void setEstadoInventario(String sestadoInventario) {
        this.sestadoInventario = sestadoInventario;
    }
    
    public void setTotalOrdenCompra(double utotalOrdenCompra) {
        this.utotalOrdenCompra = utotalOrdenCompra;
    }
        
    public void setCambio(double ucambio) {
        this.ucambio = ucambio;
    }
        
    public void setTotalGs(double utotalGs) {
        this.utotalGs = utotalGs;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdPresupuesto() {
        return this.iidPresupuesto;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }
    
    public String getUsuario() {
        return this.susuario;
    }

    public int getIdAutorizador() {
        return this.iidAutorizador;
    }
    
    public String getAutorizador() {
        return this.sautorizador;
    }

    public int getIdProveedor() {
        return this.iidProveedor;
    }
    
    public String getProveedor() {
        return this.sproveedor;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }
    
    public String getFilial() {
        return this.sfilial;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }
    
    public String getDependencia() {
        return this.sdependencia;
    }

    public int getIdTipoPedido() {
        return this.iidTipoPedido;
    }
    
    public String getTipoPedido() {
        return this.stipoPedido;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public int getIdEstadoInventario() {
        return this.iidEstadoInventario;
    }

    public String getEstadoInventario() {
        return this.sestadoInventario;
    }

    public double getTotalOrdenCompra() {
        return this.utotalOrdenCompra;
    }

    public double getCambio() {
        return this.ucambio;
    }

    public double getTotalGs() {
        return this.utotalGs;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getIdFilial()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FILIAL;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.inventario("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            this.getIdUsuario()+","+
            this.getIdFilial()+","+
            this.getIdDependencia()+","+
            this.getIdEstadoInventario()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha", "fecha", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Usuario", "apellidonombre", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(3, "Filial", "filial", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(4, "Estado", "estadoinventario", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
