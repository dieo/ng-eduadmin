/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;



/**
 *
 * @author Lic. Didier Barreto
 */
public class entbDepositoCuentaDetalle {
    
    protected int iid;
    protected int iidDepositoCuenta;
    protected int inumerocheque;
    protected int iidBanco;
    protected String sbanco;
    protected double dmontootro;
    protected double dmontopropio;
    protected short cestado;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMEROCHEQUE = "Número del cheque depositado (no vacía)";
    public static final String TEXTO_BANCO = "Banco del que procede el cheque depositado (no vacía)";
    public static final String TEXTO_MONTO_OTRO = "Monto si el cheque depositado es de otro banco";
    public static final String TEXTO_MONTO_PROPIO = "Monto si el cheque depositado es del mismo banco";
    
    public entbDepositoCuentaDetalle() {
        this.iid = 0;
        this.iidDepositoCuenta = 0;
        this.inumerocheque = 0;
        this.iidBanco = 0;
        this.sbanco = "";
        this.dmontootro = 0;
        this.dmontopropio = 0;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entbDepositoCuentaDetalle(int iid, int iidDepositoCuenta, int inumerocheque, int iidBanco, String sbanco, double dmontootro, double dmontopropio, short cestado) {
        this.iid = iid;
        this.iidDepositoCuenta = iidDepositoCuenta;
        this.inumerocheque = inumerocheque;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.dmontootro = dmontootro;
        this.dmontopropio = dmontopropio;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidDepositoCuenta, int inumerocheque, int iidBanco, String sbanco, double dmontootro, double dmontopropio, short cestado) {
        this.iid = iid;
        this.iidDepositoCuenta = iidDepositoCuenta;
        this.inumerocheque = inumerocheque;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.dmontootro = dmontootro;
        this.dmontopropio = dmontopropio;
        this.cestado = cestado;
    }

    public entbDepositoCuentaDetalle copiar(entbDepositoCuentaDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdDepositoCuenta(), this.getNumeroCheque(), this.getIdBanco(), this.getBanco(), this.getMontoOtro(), this.getMontoPropio(),  this.getEstadoRegistro());
        return destino;
    }
    
    public entbDepositoCuentaDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdDepositoCuenta(rs.getInt("iddeposito")); }
        catch(java.sql.SQLException e) { }
        try { this.setNumeroCheque(rs.getInt("numerocheque")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdBanco(rs.getInt("idbanco")); }
        catch(java.sql.SQLException e) { }
        try { this.setBanco(rs.getString("banco")); }
        catch(java.sql.SQLException e) { }
        try { this.setMontoOtro(rs.getDouble("montootrobanco")); }
        catch(java.sql.SQLException e) { }
        try { this.setMontoPropio(rs.getDouble("montopropiobanco")); }
        catch(java.sql.SQLException e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdDepositoCuenta(int iidDepositoCuenta) {
        this.iidDepositoCuenta = iidDepositoCuenta;
    }

    public void setNumeroCheque(int inumerocheque) {
        this.inumerocheque = inumerocheque;
    }

    public void setIdBanco(int iidBanco) {
        this.iidBanco = iidBanco;
    }

    public void setBanco(String sbanco) {
        this.sbanco = sbanco;
    }
    
    public void setMontoOtro(double dmontootro) {
        this.dmontootro = dmontootro;
    }

    public void setMontoPropio(double dmontopropio) {
        this.dmontopropio = dmontopropio;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdDepositoCuenta() {
        return this.iidDepositoCuenta;
    }
        
    public int getNumeroCheque() {
        return this.inumerocheque;
    }
        
    public int getIdBanco() {
        return this.iidBanco;
    }
   
    public String getBanco() {
        return this.sbanco;
    }
    
    public double getMontoOtro() {
        return this.dmontootro;
    }

    public double getMontoPropio() {
        return this.dmontopropio;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNumeroCheque()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_NUMEROCHEQUE;
            bvalido = false;
        }
        if (this.getIdBanco()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_BANCO;
            bvalido = false;
        }
        if ((this.getMontoOtro()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO_OTRO;
            bvalido = false;
        }
        if ((this.getMontoPropio()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO_PROPIO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.detalledeposito("+
            this.getId()+","+
            this.getIdDepositoCuenta()+","+
            this.getNumeroCheque()+","+
            this.getIdBanco()+","+
            this.getMontoOtro()+","+
            this.getMontoPropio()+","+
            this.getEstadoRegistro()+")";
    }

}
