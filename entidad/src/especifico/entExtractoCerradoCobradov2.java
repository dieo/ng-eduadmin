/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entExtractoCerradoCobradov2 {

    private int iidSocio;
    private int iidMovimiento;
    private String speriodo;
    private int iidCuenta;
    private String scuenta;
    private String stipoCredito;
    private int inumeroOperacion;
    private java.util.Date dfechaOperacion;
    private int iplazo;
    private int icuotaMes;
    private double umontoCuota;
    private double umontoAtraso;
    private double umontoCierre;
    private double umontoGiraduria;
    private double umontoVentanilla;
    private double umontoAsignacion;
    private double umontoReembolso;
    private double umontoDiferencia;
    private String srubro;
    private int iestadoRegistro;
    
    public entExtractoCerradoCobradov2() {
        this.iidSocio = 0;
        this.iidMovimiento = 0;
        this.speriodo = "";
        this.iidCuenta = 0;
        this.scuenta = "";
        this.stipoCredito = "";
        this.inumeroOperacion = 0;
        this.dfechaOperacion = null;
        this.iplazo = 0;
        this.icuotaMes = 0;
        this.umontoCuota = 0.0;
        this.umontoAtraso = 0.0;
        this.umontoCierre = 0.0;
        this.umontoGiraduria = 0.0;
        this.umontoVentanilla = 0.0;
        this.umontoAsignacion = 0.0;
        this.umontoReembolso = 0.0;
        this.umontoDiferencia = 0.0;
        this.srubro = "";
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entExtractoCerradoCobradov2(int iidSocio, int iidMovimiento, String speriodo, int iidCuenta, String scuenta, String stipoCredito, int inumeroOperacion, java.util.Date dfechaOperacion, int iplazo, int icuotaMes, double umontoCuota, double umontoAtraso, double umontoCierre, double umontoGiraduria, double umontoVentanilla, double umontoAsignacion, double umontoReembolso, double umontoDiferencia, String srubro, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.speriodo = speriodo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.stipoCredito = stipoCredito;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaOperacion = dfechaOperacion;
        this.iplazo = iplazo;
        this.icuotaMes = icuotaMes;
        this.umontoCuota = umontoCuota;
        this.umontoAtraso = umontoAtraso;
        this.umontoCierre = umontoCierre;
        this.umontoGiraduria = umontoGiraduria;
        this.umontoVentanilla = umontoVentanilla;
        this.umontoAsignacion = umontoAsignacion;
        this.umontoReembolso = umontoReembolso;
        this.umontoDiferencia = umontoDiferencia;
        this.srubro = srubro;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iidSocio, int iidMovimiento, String speriodo, int iidCuenta, String scuenta, String stipoCredito, int inumeroOperacion, java.util.Date dfechaOperacion, int iplazo, int icuotaMes, double umontoCuota, double umontoAtraso, double umontoCierre, double umontoGiraduria, double umontoVentanilla, double umontoAsignacion, double umontoReembolso, double umontoDiferencia, String srubro, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.speriodo = speriodo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.stipoCredito = stipoCredito;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaOperacion = dfechaOperacion;
        this.iplazo = iplazo;
        this.icuotaMes = icuotaMes;
        this.umontoCuota = umontoCuota;
        this.umontoAtraso = umontoAtraso;
        this.umontoCierre = umontoCierre;
        this.umontoGiraduria = umontoGiraduria;
        this.umontoVentanilla = umontoVentanilla;
        this.umontoAsignacion = umontoAsignacion;
        this.umontoReembolso = umontoReembolso;
        this.umontoDiferencia = umontoDiferencia;
        this.srubro = srubro;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entExtractoCerradoCobradov2 copiar(entExtractoCerradoCobradov2 destino) {
        destino.setEntidad(this.getIdSocio(), this.getIdMovimiento(), this.getPeriodo(), this.getIdCuenta(), this.getCuenta(), this.getTipoCredito(), this.getNumeroOperacion(), this.getFechaOperacion(), this.getPlazo(), this.getCuotaMes(), this.getMontoCuota(), this.getMontoAtraso(), this.getMontoCierre(), this.getMontoGiraduria(), this.getMontoVentanilla(), this.getMontoAsignacion(), this.getMontoReembolso(), this.getMontoDiferencia(), this.getRubro(), this.getEstadoRegistro());
        return destino;
    }

    public entExtractoCerradoCobradov2 cargar(java.sql.ResultSet rs) {
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setPeriodo(rs.getString("periodo")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setTipoCredito(rs.getString("tipocredito")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setCuotaMes(rs.getInt("cuotames")); }
        catch(Exception e) {}
        try { this.setMontoCuota(rs.getDouble("montocuota")); }
        catch(Exception e) {}
        try { this.setMontoAtraso(rs.getDouble("montoatraso")); }
        catch(Exception e) {}
        try { this.setMontoCierre(rs.getDouble("montocierre")); }
        catch(Exception e) {}
        try { this.setMontoGiraduria(rs.getDouble("montogiraduria")); }
        catch(Exception e) {}
        try { this.setMontoVentanilla(rs.getDouble("montoventanilla")); }
        catch(Exception e) {}
        try { this.setMontoAsignacion(rs.getDouble("montoasignacion")); }
        catch(Exception e) {}
        try { this.setMontoReembolso(rs.getDouble("montoreembolso")); }
        catch(Exception e) {}
        try { this.setMontoDiferencia(rs.getDouble("montodiferencia")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        String scomplemento = "";
        if (!this.getTipoCredito().equals("-")) scomplemento += " - "+utilitario.utiCadena.convertirMayusMinus(this.getTipoCredito());
        if (this.getNumeroOperacion()!=0) scomplemento += " - Op."+utilitario.utiNumero.getMascara(this.getNumeroOperacion(),0);
        if (this.getFechaOperacion()!=null) scomplemento += " - "+utilitario.utiFecha.convertirToStringDMACorto(this.getFechaOperacion());
        if (this.getMontoCuota()>0) scomplemento += " - Cuo."+utilitario.utiNumero.getMascara(this.getMontoCuota(),0);
        if (this.getPlazo()>0) scomplemento += " - Cu./Pl."+this.getCuotaMes()+"/"+this.getPlazo();
        this.setCuenta(utilitario.utiCadena.convertirMayusMinus(this.getCuenta())+scomplemento);
        return this;
    }
    
    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setPeriodo(String speriodo) {
        this.speriodo = speriodo;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setTipoCredito(String stipoCredito) {
        this.stipoCredito = stipoCredito;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setCuotaMes(int icuotaMes) {
        this.icuotaMes = icuotaMes;
    }

    public void setMontoCuota(double umontoCuota) {
        this.umontoCuota = umontoCuota;
    }

    public void setMontoAtraso(double umontoAtraso) {
        this.umontoAtraso = umontoAtraso;
    }

    public void setMontoCierre(double umontoCierre) {
        this.umontoCierre = umontoCierre;
    }

    public void setMontoGiraduria(double umontoGiraduria) {
        this.umontoGiraduria = umontoGiraduria;
    }

    public void setMontoVentanilla(double umontoVentanilla) {
        this.umontoVentanilla = umontoVentanilla;
    }

    public void setMontoAsignacion(double umontoAsignacion) {
        this.umontoAsignacion = umontoAsignacion;
    }

    public void setMontoReembolso(double umontoReembolso) {
        this.umontoReembolso = umontoReembolso;
    }

    public void setMontoDiferencia(double umontoDiferencia) {
        this.umontoDiferencia = umontoDiferencia;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public String getPeriodo() {
        return this.speriodo;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getTipoCredito() {
        return this.stipoCredito;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public int getCuotaMes() {
        return this.icuotaMes;
    }

    public Double getMontoCuota() {
        return this.umontoCuota;
    }

    public Double getMontoAtraso() {
        return this.umontoAtraso;
    }

    public Double getMontoCierre() {
        return this.umontoCierre;
    }

    public Double getMontoGiraduria() {
        return this.umontoGiraduria;
    }

    public Double getMontoVentanilla() {
        return this.umontoVentanilla;
    }

    public double getMontoAsignacion() {
        return this.umontoAsignacion;
    }

    public double getMontoReembolso() {
        return this.umontoReembolso;
    }

    public double getMontoDiferencia() {
        return this.umontoDiferencia;
    }

    public String getRubro() {
        return this.srubro;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    public String getConsultaCerrado(java.util.Date dfechaInicial, java.util.Date dfechaFinal, int iidSocio) {
        return 
           "SELECT * FROM ("+
           "SELECT "+
           "   (CASE WHEN cie.periodo IS NULL THEN cob.periodo ELSE cie.periodo END) AS periodo, "+
           "   (CASE WHEN cie.idcuenta IS NULL THEN cob.idcuenta ELSE cie.idcuenta END) AS idcuenta, "+
           "   (CASE WHEN cie.cuenta IS NULL THEN cob.cuenta ELSE cie.cuenta END) AS cuenta, "+
           "   (CASE WHEN cie.tipocredito IS NULL THEN '-' ELSE cie.tipocredito END) AS tipocredito, "+
           "   (CASE WHEN cie.periodoorden IS NULL THEN cob.periodoorden ELSE cie.periodoorden END) AS periodoorden, "+
           "   (CASE WHEN cie.orden IS NULL THEN 'X' ELSE cie.orden END) AS orden, "+
           "   (CASE WHEN cie.rubro IS NULL THEN '-' ELSE cie.rubro END) AS rubro, "+
           "   (CASE WHEN cie.idmovimiento IS NULL THEN cob.idmovimiento ELSE cie.idmovimiento END) AS idmovimiento, "+
           "   (CASE WHEN cie.numerooperacion IS NULL THEN cob.numerooperacion ELSE cie.numerooperacion END) AS numerooperacion, "+
           "   (CASE WHEN cie.fechaoperacion IS NULL THEN cob.fechaoperacion ELSE cie.fechaoperacion END) AS fechaoperacion, "+
           "   (CASE WHEN cie.plazo IS NULL THEN 0 ELSE cie.plazo END) AS plazo, "+
           "   (CASE WHEN cie.tabla IS NULL THEN '-' ELSE cie.tabla END) AS tabla, "+
           "   (CASE WHEN cie.cuotames IS NULL THEN 0 ELSE cie.cuotames END) AS cuotames, "+
           "   (CASE WHEN cie.montocuota IS NULL THEN 0 ELSE cie.montocuota END) AS montocuota, "+
           "   (CASE WHEN cie.montoatraso IS NULL THEN 0 ELSE cie.montoatraso END) AS montoatraso, "+
           "   (CASE WHEN cie.montocierre IS NULL THEN 0 ELSE cie.montocierre END) AS montocierre, "+
           "   SUM(CASE WHEN cob.montogir IS NULL THEN 0 ELSE cob.montogir END) AS montogiraduria, "+
           "   SUM(CASE WHEN cob.montoven IS NULL THEN 0 ELSE cob.montoven END) AS montoventanilla, "+
           "   SUM(CASE WHEN cob.montoree IS NULL THEN 0 ELSE cob.montoree END) AS montoreembolso, "+
           "   SUM(CASE WHEN cob.montoasi IS NULL THEN 0 ELSE cob.montoasi END) AS montoasignacion "+
           "FROM (SELECT ci.idcuenta, "+
           "   (CASE WHEN TO_CHAR(ci.fechacierre,'MM')='01' THEN 'ENERO' WHEN TO_CHAR(ci.fechacierre,'MM')='02' THEN 'FEBRERO' WHEN TO_CHAR(ci.fechacierre,'MM')='03' THEN 'MARZO' WHEN TO_CHAR(ci.fechacierre,'MM')='04' THEN 'ABRIL' "+
           "      WHEN TO_CHAR(ci.fechacierre,'MM')='05' THEN 'MAYO' WHEN TO_CHAR(ci.fechacierre,'MM')='06' THEN 'JUNIO' WHEN TO_CHAR(ci.fechacierre,'MM')='07' THEN 'JULIO' WHEN TO_CHAR(ci.fechacierre,'MM')='08' THEN 'AGOSTO' "+
           "      WHEN TO_CHAR(ci.fechacierre,'MM')='09' THEN 'SETIEMBRE' WHEN TO_CHAR(ci.fechacierre,'MM')='10' THEN 'OCTUBRE' WHEN TO_CHAR(ci.fechacierre,'MM')='11' THEN 'NOVIEMBRE' ELSE 'DICIEMBRE' END)||TO_CHAR(ci.fechacierre,'/YYYY') AS periodo, "+
           "   TO_CHAR(ci.fechacierre,'MM/YYYY') AS periodoorden, "+
           "   cu.cuenta, tc.descripcionbreve AS tipocredito, cu.orden, gi.descripcionbreve||' '||ru.descripcionbreve AS rubro, ci.idmovimiento, ci.numerooperacion, ci.fechaoperacion, ci.plazo, ci.tabla, "+
           "   SUM(CASE WHEN TO_CHAR(ci.fechavencimiento,'YYYYMM00')=TO_CHAR(ci.fechacierre,'YYYYMM00') THEN ci.cuota ELSE 0 END)::NUMERIC(15,0) AS cuotames, "+
           "   SUM(CASE WHEN TO_CHAR(ci.fechavencimiento,'YYYYMM00')=TO_CHAR(ci.fechacierre,'YYYYMM00') AND (ci.idcuenta=35 OR ci.idcuenta=104) THEN ci.montocapital+ci.montointeres ELSE 0 END)::NUMERIC(15,0) AS montocuota, "+
           "   SUM(CASE WHEN TO_CHAR(ci.fechavencimiento,'YYYYMM00')<>TO_CHAR(ci.fechacierre,'YYYYMM00') THEN ci.saldo ELSE 0 END)::NUMERIC(15,0) AS montoatraso, "+
           "   SUM(CASE WHEN TO_CHAR(ci.fechavencimiento,'YYYYMM00')=TO_CHAR(ci.fechacierre,'YYYYMM00') THEN ci.saldo ELSE 0 END)::NUMERIC(15,0) AS montocierre "+
           "FROM cierre AS ci "+
           "LEFT JOIN "+
           "   (SELECT cu.id, cu.descripcion AS cuenta, "+
           "      (CASE WHEN cu.id<=5 THEN 'A' WHEN cu.id=83 THEN 'B' WHEN cu.id=17 OR cu.id=85 THEN 'C' WHEN cu.id=47 THEN 'D' "+
           "      WHEN cu.id=35 OR cu.id=104 THEN 'E' WHEN cu.id=105 THEN 'F' WHEN cu.id=98 THEN 'G' WHEN cu.idtipocuenta=26 THEN 'Z' ELSE 'X' END) AS orden "+
           "   FROM cuenta as cu "+
           "   GROUP BY cu.id, cu.descripcion, orden) AS cu ON ci.idcuenta=cu.id "+
           "   LEFT JOIN giraduria AS gi ON ci.idgiraduria=gi.id "+
           "   LEFT JOIN rubro AS ru ON ci.idrubro=ru.id "+
           "   LEFT JOIN movimiento AS mo ON ci.idmovimiento=mo.id AND ci.tabla='mo' "+
           "   LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
           "   WHERE TO_CHAR(ci.fechacierre,'YYYYMM00')>='"+utilitario.utiFecha.getAM(dfechaInicial)+"' AND TO_CHAR(ci.fechacierre,'YYYYMM00')<='"+utilitario.utiFecha.getAM(dfechaFinal)+"' AND ci.idsocio="+iidSocio+" "+
           "   GROUP BY ci.fechacierre, ci.idcuenta, cu.cuenta, tc.descripcionbreve, cu.orden, gi.descripcionbreve, ru.descripcionbreve, ci.idmovimiento, ci.numerooperacion, ci.fechaoperacion, ci.plazo, ci.tabla "+
           "   ORDER BY cu.orden) AS cie "+
           "FULL OUTER JOIN "+ // ingresos
           "   (SELECT di.idcuenta, cu.descripcion AS cuenta, "+
           "      (CASE WHEN TO_CHAR(ing.fechaaaplicar,'MM')='01' THEN 'ENERO' WHEN TO_CHAR(ing.fechaaaplicar,'MM')='02' THEN 'FEBRERO' WHEN TO_CHAR(ing.fechaaaplicar,'MM')='03' THEN 'MARZO' WHEN TO_CHAR(ing.fechaaaplicar,'MM')='04' THEN 'ABRIL' "+
           "         WHEN TO_CHAR(ing.fechaaaplicar,'MM')='05' THEN 'MAYO' WHEN TO_CHAR(ing.fechaaaplicar,'MM')='06' THEN 'JUNIO' WHEN TO_CHAR(ing.fechaaaplicar,'MM')='07' THEN 'JULIO' WHEN TO_CHAR(ing.fechaaaplicar,'MM')='08' THEN 'AGOSTO' "+
           "         WHEN TO_CHAR(ing.fechaaaplicar,'MM')='09' THEN 'SETIEMBRE' WHEN TO_CHAR(ing.fechaaaplicar,'MM')='10' THEN 'OCTUBRE' WHEN TO_CHAR(ing.fechaaaplicar,'MM')='11' THEN 'NOVIEMBRE' ELSE 'DICIEMBRE' END)||TO_CHAR(ing.fechaaaplicar,'/YYYY') AS periodo, "+
           "      TO_CHAR(ing.fechaaaplicar,'MM/YYYY') AS periodoorden, "+
           "      di.idmovimiento, "+
           "      CASE WHEN ope.numerooperacion IS NULL THEN 0 "+
           "      ELSE ope.numerooperacion END AS numerooperacion, "+
           "      ope.fechaoperacion, di.tabla, "+
           "      SUM(CASE WHEN pi.idviacobro=38 THEN di.montocobro ELSE 0 END)::NUMERIC(15,0) AS montogir, "+
           "      SUM(CASE WHEN pi.idviacobro=39 THEN di.montocobro ELSE 0 END)::NUMERIC(15,0) AS montoven, "+
           "      SUM(CASE WHEN pi.idviacobro=222 THEN di.montocobro ELSE 0 END)::NUMERIC(15,0) AS montoree, "+
           "      SUM(CASE WHEN pi.idviacobro=221 THEN di.montocobro ELSE 0 END)::NUMERIC(15,0) AS montoasi "+
           "   FROM detalleingreso AS di "+
           "   LEFT JOIN ingreso AS ing ON di.idingreso=ing.id "+
           "   LEFT JOIN cuenta AS cu ON di.idcuenta=cu.id "+
           "   LEFT JOIN planillaingreso AS pi ON ing.idplanilla=pi.id "+
           "   LEFT JOIN detalleoperacion AS dop ON di.iddetalleoperacion=dop.id "+
           "   LEFT JOIN (SELECT id, numerooperacion, fechaoperacion, tabla FROM ( "+
           "      SELECT id, numerooperacion, fechaaprobado AS fechaoperacion, 'mo' AS tabla FROM movimiento WHERE idsocio="+iidSocio+" "+
           "      UNION "+
           "      SELECT id, numerooperacion, fechaoperacion, 'of' AS tabla FROM operacionfija WHERE idsocio="+iidSocio+" "+
           "      UNION "+
           "      SELECT id, numerooperacion, fechaoperacion, 'ap' AS tabla FROM ahorroprogramado WHERE idsocio="+iidSocio+" "+
           "      UNION "+
           "      SELECT id, numerooperacion, fechaoperacion, 'js' AS tabla FROM fondojuridicosepelio WHERE idsocio="+iidSocio+") AS ope) AS ope ON dop.idmovimiento=ope.id AND dop.tabla=ope.tabla "+
           "   WHERE ing.idestado=146 AND ing.idsocio="+iidSocio+" AND TO_CHAR(ing.fechaaaplicar,'YYYYMM00')>='"+utilitario.utiFecha.getAM(dfechaInicial)+"' AND TO_CHAR(ing.fechaaaplicar,'YYYYMM00')<='"+utilitario.utiFecha.getAM(dfechaFinal)+"' "+
           "   GROUP BY ing.fechaaaplicar, di.idcuenta, cu.descripcion, di.idmovimiento, ope.numerooperacion, ope.fechaoperacion, di.tabla "+
           "   ORDER BY di.idcuenta) AS cob "+
           "ON cie.periodo=cob.periodo AND cie.idcuenta=cob.idcuenta AND cie.idmovimiento=cob.idmovimiento "+
           "GROUP BY cie.periodo, cob.periodo, cie.periodoorden, cob.periodoorden, cie.idcuenta, cob.idcuenta, cie.cuenta, cob.cuenta, cie.tipocredito, cie.orden, cie.rubro, cie.idmovimiento, cob.idmovimiento, cie.numerooperacion, cob.numerooperacion, cie.fechaoperacion, cob.fechaoperacion, cie.tabla, cie.cuotames, cie.plazo, cie.montocuota, cie.montoatraso, cie.montocierre "+
           ") AS resultado "+
           "ORDER BY periodoorden, orden, idcuenta";
    }
    
    public String getConsultaCerradoFuncionarioMSP(java.util.Date dfecha, String scedula, String sorden) {
        String speriodo = "PERIODO "+utilitario.utiFecha.getMeses()[dfecha.getMonth()]+"/"+utilitario.utiFecha.getAnho(dfecha);
        return "SELECT ci.idcuenta, '"+speriodo+"' AS periodo, cu.descripcion AS cuenta, gi.descripcionbreve||' '||ru.descripcionbreve AS rubro, ci.idmovimiento, ci.numerooperacion, ci.plazo, ci.tabla, SUM(ci.saldo) AS montocerrado "+
            "FROM cierre AS ci "+
            "LEFT JOIN cuenta AS cu ON ci.idcuenta=cu.id "+
            "LEFT JOIN giraduria AS gi ON ci.idgiraduria=gi.id "+
            "LEFT JOIN rubro AS ru ON ci.idrubro=ru.id "+
            "WHERE ci.fechacierre='"+utilitario.utiFecha.getUltimoDia(dfecha)+"' AND ci.cedula='"+scedula+"' "+
            "GROUP BY ci.idcuenta, cu.descripcion, gi.descripcionbreve, ru.descripcionbreve, ci.idmovimiento, ci.numerooperacion, ci.plazo, ci.tabla "+
            "ORDER BY ci.idcuenta";
    }
    
    public String getConsultaGiraduriaFuncionarioMSP(java.util.Date dfecha, String scedula) {
        return "SELECT di.idcuenta, cu.descripcion AS cuenta, di.idmovimiento, di.numerooperacion, di.tabla, SUM(di.montocobro) AS montogiraduria "+
            "FROM detalleingreso AS di "+
            "LEFT JOIN ingreso AS ing ON di.idingreso=ing.id "+
            "LEFT JOIN cuenta AS cu ON di.idcuenta=cu.id "+
            "LEFT JOIN planillaingreso AS pi ON ing.idplanilla=pi.id "+
            "WHERE ing.idestado=146 AND pi.idviacobro=38 AND ing.ruc='"+scedula+"' AND di.tabla='ce' AND ing.fechaaaplicar='"+utilitario.utiFecha.getUltimoDia(dfecha)+"' "+
            "GROUP BY di.idcuenta, cu.descripcion, di.idmovimiento, di.numerooperacion, di.tabla "+
            "ORDER BY di.idcuenta";
    }
    
    public String getConsultaEstado(String sfiltro, String sorden) {
        return "SELECT * FROM detalleoperacion WHERE "+sfiltro+sorden;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
