/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entcBajas {
    
    private int iid;
    private int iidFilial;
    private String sfilial;
    private java.util.Date dfecha;
    private int iidUsuario;
    private String susuario;
    private int iidArticulo;
    private String sdescripcion;
    private double ucantidad;
    private String sobservacion;
    private int iidEstadoBaja;
    private String sestadoBaja;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_OBSERVACION = 200;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FILIAL = "Filial del pedido.";
    public static final String TEXTO_ID_DETALLE = "Id del Detalle (no editable)";
    public static final String TEXTO_ESTADO = "Estado de la Devolución (no editable)";
    public static final String TEXTO_OBSERVACION = "Motivo de la devolución (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA = "Fecha de la devolución";
    public static final String TEXTO_USUARIO = "Usuario que realizó el pedido";
    public static final String TEXTO_ARTICULO = "Producto devuelto (no vacía)";
    public static final String TEXTO_CANTIDAD = "Cantidad devuelta del Producto (no vacía, valor positivo, no puede ser mayor que cantidad entregada)";
    
    public entcBajas() {
        this.iid = 0;
        this.iidFilial = 0;
        this.sfilial = "";
        this.dfecha = null;
        this.iidUsuario = 0;
        this.susuario = "";
        this.iidArticulo = 0;
        this.sdescripcion = "";
        this.ucantidad = 0.0;
        this.sobservacion = "";
        this.iidEstadoBaja = 0;
        this.sestadoBaja = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entcBajas(int iid, int iidFilial, String sfilial, java.util.Date dfecha, int iidUsuario, String susuario, int iidArticulo, String sdescripcion, double ucantidad, String sobservacion, int iidEstadoBaja, String sestadoBaja, short hestadoRegistro) {
        this.iid = iid;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.dfecha = dfecha;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.ucantidad = ucantidad;
        this.sobservacion = sobservacion;
        this.iidEstadoBaja = iidEstadoBaja;
        this.sestadoBaja = sestadoBaja;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidFilial, String sfilial, java.util.Date dfecha, int iidUsuario, String susuario, int iidArticulo, String sdescripcion, double ucantidad, String sobservacion, int iidEstadoBaja, String sestadoBaja, short hestadoRegistro) {
        this.iid = iid;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.dfecha = dfecha;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.ucantidad = ucantidad;
        this.sobservacion = sobservacion;
        this.iidEstadoBaja = iidEstadoBaja;
        this.sestadoBaja = sestadoBaja;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entcBajas copiar(entcBajas destino) {
        destino.setEntidad(this.getId(), this.getIdFilial(), this.getFilial(), this.getFecha(), this.getIdUsuario(), this.getUsuario(), this.getIdArticulo(), this.getDescripcion(), this.getCantidad(), this.getObservacion(), this.getIdEstadoBaja(), this.getEstadoBaja(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcBajas cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(java.sql.SQLException e) { }
        try { this.setFilial(rs.getString("filial")); }
        catch(java.sql.SQLException e) { }
        try { this.setFecha(rs.getDate("fecha")); }
        catch(java.sql.SQLException e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(java.sql.SQLException e) {}
        try { this.setUsuario(rs.getString("usuario")); }
        catch(java.sql.SQLException e) {}
        try { this.setIdArticulo(rs.getInt("idarticulo")); }
        catch(java.sql.SQLException e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(java.sql.SQLException e) { }
        try { this.setCantidad(rs.getDouble("cantidad")); }
        catch(java.sql.SQLException e) { }
        try { this.setObservacion(rs.getString("observacion")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdEstadoBaja(rs.getInt("idestadobaja")); }
        catch(java.sql.SQLException e) {}
        try { this.setEstadoBaja(rs.getString("estadobaja")); }
        catch(java.sql.SQLException e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdFilial(int iidPedido) {
        this.iidFilial = iidPedido;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }
    
    public void setUsuario(String susuario) {
        this.susuario = susuario;
    }

    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }
    
    public void setIdEstadoBaja(int iidEstadoBaja) {
        this.iidEstadoBaja = iidEstadoBaja;
    }

    public void setEstadoBaja(String sestadoBaja) {
        this.sestadoBaja = sestadoBaja;
    }
    
    public void setEstadoRegistro(short cestado) {
        this.hestadoRegistro = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }

    public String getFilial() {
        return this.sfilial;
    }
        
    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public int getIdUsuario() {
        return this.iidUsuario;
    }
    
    public String getUsuario() {
        return this.susuario;
    }

    public int getIdArticulo() {
        return this.iidArticulo;
    }
   
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public double getCantidad() {
        return this.ucantidad;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public int getIdEstadoBaja() {
        return this.iidEstadoBaja;
    }

    public String getEstadoBaja() {
        return this.sestadoBaja;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdArticulo()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_ARTICULO;
            bvalido = false;
        }
        if ((this.getCantidad()<=0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CANTIDAD;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.baja("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            this.getIdArticulo()+","+
            this.getIdFilial()+","+
            this.getIdUsuario()+","+
            this.getCantidad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getIdEstadoBaja()+","+
            this.getEstadoRegistro()+")";
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha", "fecha", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Nº de Pedido", "idpedido", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(3, "Usuario", "usuario", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(4, "Articulo", "descripcion", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(5, "Estado", "estadodevolucion", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(6, "Filial", "filial", new generico.entLista().tipo_texto));
        return lst;
    }

}
