/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMovimientoArticulo {

    private int iid;
    private int iidBoleta;
    private int iidArticulo;
    private String sarticulo;
    private int icantidad;
    private int iidUnidadMedida;
    private String sunidadMedida;
    private double uimporte;
    private String sdescripcion;
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_DESCRIPCION = 50;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ARTICULO = "Artículo (no vacío)";
    public static final String TEXTO_CANTIDAD = "Cantidad (valor positivo)";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de Medida (no vacía)";
    public static final String TEXTO_IMPORTE = "Importe (valor positivo)";
    public static final String TEXTO_DESCRIPCION = "Descripción del artículo (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres))";

    public entMovimientoArticulo() {
        this.iid = 0;
        this.iidBoleta = 0;
        this.iidArticulo = 0;
        this.sarticulo = "";
        this.icantidad = 0;
        this.iidUnidadMedida = 0;
        this.sunidadMedida = "";
        this.uimporte = 0.0;
        this.sdescripcion = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entMovimientoArticulo(int iid, int iidBoleta, int iidArticulo, String sarticulo, int icantidad, int iidUnidadMedida, String sunidadMedida, double uimporte, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.iidBoleta = iidBoleta;
        this.iidArticulo = iidArticulo;
        this.sarticulo = sarticulo;
        this.icantidad = icantidad;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.uimporte = uimporte;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidBoleta, int iidArticulo, String sarticulo, int icantidad, int iidUnidadMedida, String sunidadMedida, double uimporte, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.iidBoleta = iidBoleta;
        this.iidArticulo = iidArticulo;
        this.sarticulo = sarticulo;
        this.icantidad = icantidad;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.uimporte = uimporte;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public entMovimientoArticulo copiar(entMovimientoArticulo destino) {
        destino.setEntidad(this.getId(), this.getIdBoleta(), this.getIdArticulo(), this.getArticulo(), this.getCantidad(), this.getIdUnidadMedida(), this.getUnidadMedida(), this.getImporte(), this.getDescripcion(), this.getEstadoRegistro());
        return destino;
    }

    public entMovimientoArticulo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdBoleta(rs.getInt("idboleta")); }
        catch(Exception e) {}
        try { this.setIdArticulo(rs.getInt("idarticulo")); }
        catch(Exception e) {}
        try { this.setArticulo(rs.getString("articulo")); }
        catch(Exception e) {}
        try { this.setCantidad(rs.getInt("cantidad")); }
        catch(Exception e) {}
        try { this.setIdUnidadMedida(rs.getInt("idunidadmedida")); }
        catch(Exception e) {}
        try { this.setUnidadMedida(rs.getString("unidadmedida")); }
        catch(Exception e) {}
        try { this.setImporte(rs.getDouble("importe")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdBoleta(int iidBoleta) {
        this.iidBoleta = iidBoleta;
    }

    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setArticulo(String sarticulo) {
        this.sarticulo = sarticulo;
    }

    public void setCantidad(int icantidad) {
        this.icantidad = icantidad;
    }

    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }

    public void setUnidadMedida(String sunidadMedida) {
        this.sunidadMedida = sunidadMedida;
    }

    public void setImporte(double uimporte) {
        this.uimporte = uimporte;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdBoleta() {
        return this.iidBoleta;
    }

    public int getIdArticulo() {
        return this.iidArticulo;
    }

    public String getArticulo() {
        return this.sarticulo;
    }

    public int getCantidad() {
        return this.icantidad;
    }

    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }

    public String getUnidadMedida() {
        return this.sunidadMedida;
    }

    public double getImporte() {
        return this.uimporte;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdArticulo()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ARTICULO;
            bvalido = false;
        }
        if (this.getCantidad()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CANTIDAD;
            bvalido = false;
        }
        if (this.getIdUnidadMedida()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_UNIDAD_MEDIDA;
            bvalido = false;
        }
        if (this.getImporte()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPORTE;
            bvalido = false;
        }
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT movimientoarticulo("+
            this.getId()+","+
            this.getIdBoleta()+","+
            this.getIdArticulo()+","+
            this.getCantidad()+","+
            this.getIdUnidadMedida()+","+
            this.getImporte()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Articulo", "articulo", generico.entLista.tipo_texto));
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Articulo", "articulo", generico.entLista.tipo_texto));
        return lst;
    }

}
