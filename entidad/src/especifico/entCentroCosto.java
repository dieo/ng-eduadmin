/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entCentroCosto extends generico.entGenerica{
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Centro de Costo (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";

    public entCentroCosto() {
        super();
    }

    public entCentroCosto(int iid, String sdescripcion) {
        super(iid, sdescripcion);
    }

    public entCentroCosto(int iid, String sdescripcion, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
    }

    public void setEntidad(int iid, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entCentroCosto copiar(entCentroCosto destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entCentroCosto cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        return this;
    }
    
    public String getCampoConcatenado() {
        return this.getId()+" "+this.getDescripcion();
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT centrocosto("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", (short)0));
        return lst;
    }

}