/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entListaDetalle {

    private int iid;
    private int icuota;
    private java.util.Date dfechaVencimiento;
    private double umontoCapital;
    private double umontoInteres;
    private double ucobroCapital;
    private double ucobroInteres;
    private double usaldoCapital;
    private double usaldoInteres;
    private double uexoneracion;
    private double uinteresMoratorio;
    private double uinteresPunitorio;
    private String sestado;
    private String snroingreso;
    private String smensaje;
    private int iestadoRegistro;

    public entListaDetalle() {
        this.iid = 0;
        this.icuota = 0;
        this.dfechaVencimiento = null;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.ucobroCapital = 0.0;
        this.ucobroInteres = 0.0;
        this.usaldoCapital = 0.0;
        this.usaldoInteres = 0.0;
        this.uexoneracion = 0.0;
        this.uinteresMoratorio = 0.0;
        this.uinteresPunitorio = 0.0;
        this.sestado = "";
        this.snroingreso = "";
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entListaDetalle(int iid, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double ucobroCapital, double ucobroInteres, double usaldoCapital, double usaldoInteres, double uexoneracion, double uinteresMoratorio, double uinteresPunitorio, String sestado, String snroingreso, int iestadoRegistro) {
        this.iid = iid;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.ucobroCapital = ucobroCapital;
        this.ucobroInteres = ucobroInteres;
        this.usaldoCapital = usaldoCapital;
        this.usaldoInteres = usaldoInteres;
        this.uexoneracion = uexoneracion;
        this.uinteresMoratorio = uinteresMoratorio;
        this.uinteresPunitorio = uinteresPunitorio;
        this.sestado = sestado;
        this.snroingreso = snroingreso;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iid, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double ucobroCapital, double ucobroInteres, double usaldoCapital, double usaldoInteres, double uexoneracion, double uinteresMoratorio, double uinteresPunitorio, String sestado,String snroingreso, int iestadoRegistro) {
        this.iid = iid;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.ucobroCapital = ucobroCapital;
        this.ucobroInteres = ucobroInteres;
        this.usaldoCapital = usaldoCapital;
        this.usaldoInteres = usaldoInteres;
        this.uexoneracion = uexoneracion;
        this.uinteresMoratorio = uinteresMoratorio;
        this.uinteresPunitorio = uinteresPunitorio;
        this.sestado = sestado;
        this.snroingreso = snroingreso;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entListaDetalle copiar(entListaDetalle destino) {
        destino.setEntidad(this.getId(), this.getCuota(), this.getFechaVencimiento(), this.getMontoCapital(), this.getMontoInteres(), this.getCobroCapital(), this.getCobroInteres(), this.getSaldoCapital(), this.getSaldoInteres(), this.getExoneracion(), this.getInteresMoratorio(), this.getInteresPunitorio(), this.getEstado(), this.getNroingreso(), this.getEstadoRegistro());
        return destino;
    }

    public entListaDetalle cargar(java.sql.ResultSet rs) {
        try {
            this.setId(rs.getInt("id"));
        } catch (Exception e) {
        }
        try {
            this.setCuota(rs.getInt("numerocuota"));
        } catch (Exception e) {
        }
        try {
            this.setNroingreso(rs.getString("nroingreso"));
        } catch (Exception e) {
        }
        try {
            this.setFechaVencimiento(rs.getDate("fechavencimiento"));
        } catch (Exception e) {
        }
        try {
            this.setMontoCapital(rs.getDouble("montocapital"));
        } catch (Exception e) {
        }
        try {
            this.setMontoInteres(rs.getDouble("montointeres"));
        } catch (Exception e) {
        }
        try {
            this.setSaldoCapital(rs.getDouble("saldocapital"));
        } catch (Exception e) {
        }
        try {
            this.setSaldoInteres(rs.getDouble("saldointeres"));
        } catch (Exception e) {
        }
        try {
            this.setCobroCapital(rs.getDouble("montocapital") - rs.getDouble("saldocapital"));
        } catch (Exception e) {
        }
        try {
            this.setCobroInteres(rs.getDouble("montointeres") - rs.getDouble("saldointeres"));
        } catch (Exception e) {
        }
        try {
            this.setExoneracion(rs.getDouble("exoneracion"));
        } catch (Exception e) {
        }
        try {
            this.setInteresMoratorio(0.0);
        } catch (Exception e) {
        }
        try {
            this.setInteresPunitorio(0.0);
        } catch (Exception e) {
        }
        try {
            this.setEstado("");
        } catch (Exception e) {
        }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public String getNroingreso() {
        return snroingreso;
    }

    public void setNroingreso(String snroingreso) {
        this.snroingreso = snroingreso;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = (java.util.Date) dfechaVencimiento.clone();
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setCobroCapital(double ucobroCapital) {
        this.ucobroCapital = ucobroCapital;
    }

    public void setCobroInteres(double ucobroInteres) {
        this.ucobroInteres = ucobroInteres;
    }

    public void setSaldoCapital(double usaldoCapital) {
        this.usaldoCapital = usaldoCapital;
    }

    public void setSaldoInteres(double usaldoInteres) {
        this.usaldoInteres = usaldoInteres;
    }

    public void setExoneracion(double uexoneracion) {
        this.uexoneracion = uexoneracion;
    }

    public void setInteresMoratorio(double uinteresMoratorio) {
        this.uinteresMoratorio = uinteresMoratorio;
    }

    public void setInteresPunitorio(double uinteresPunitorio) {
        this.uinteresPunitorio = uinteresPunitorio;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getCuota() {
        return this.icuota;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public double getCobroCapital() {
        return this.ucobroCapital;
    }

    public double getCobroInteres() {
        return this.ucobroInteres;
    }

    public double getSaldoCapital() {
        return this.usaldoCapital;
    }

    public double getSaldoInteres() {
        return this.usaldoInteres;
    }

    public double getExoneracion() {
        return this.uexoneracion;
    }

    public double getInteresMoratorio() {
        return this.uinteresMoratorio;
    }

    public double getInteresPunitorio() {
        return this.uinteresPunitorio;
    }

    public String getEstado() {
        return this.sestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    // métodos para la conexión
    public String getSentencia() {
        return "SELECT detalleoperacion("
                + this.getId() + ","
                + this.getCuota() + ","
                + utilitario.utiFecha.getFechaGuardado(this.getFechaVencimiento()) + ","
                + this.getMontoCapital() + ","
                + this.getMontoInteres() + ","
                + this.getSaldoCapital() + ","
                + this.getSaldoInteres() + ","
                + utilitario.utiCadena.getTextoGuardado(this.getEstado()) + ","
                + this.getEstadoRegistro() + ")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
