/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entConceptoDeposito extends generico.entGenerica{
    
    public static final int LONGITUD_DESCRIPCION = 80;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Concepto de Depósito (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";

    public entConceptoDeposito() {
        super();
    }

    public entConceptoDeposito(int iid, String sdescripcion, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
    }
    
    public void setEntidad(int iid, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entConceptoDeposito copiar(entConceptoDeposito destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getEstadoRegistro());
        return destino;
    }

    public entConceptoDeposito cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        return this;
    }
    
    public boolean esValido() {
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) this.smensaje += TEXTO_DESCRIPCION + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public String getSentencia() {
        return "SELECT conceptodeposito("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
    public void setCampo(java.util.ArrayList<Object> lst) {
        lst.add(this.getId());
        lst.add(this.getDescripcion());
    }
   
}
