/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entPersonaDetalle {
    
    protected int iid;
    protected int iidPersona;
    protected int itimbrado;
    protected java.util.Date dinicioVigencia;
    protected java.util.Date dfinVigencia;
    protected boolean bactivo;
    protected short cestado;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_TIMBRADO = "Nº del timbrado (no vacío)";
    public static final String TEXTO_INICIO_VIGENCIA = "Fecha de inicio de Vigencia (no vacía)";
    public static final String TEXTO_FIN_VIGENCIA = "Fecha de fin de Vigencia (no vacía)";
    public static final String TEXTO_ACTIVO = "Timbrado activo o no";
    
    public entPersonaDetalle() {
        this.iid = 0;
        this.iidPersona = 0;
        this.itimbrado = 0;
        this.dinicioVigencia = null;
        this.dfinVigencia = null;
        this.bactivo = false;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entPersonaDetalle(int iid, int iidPersona, int itimbrado, java.util.Date diniciovigencia, java.util.Date dfinvigencia, boolean bactivo, short cestado) {
        this.iid = iid;
        this.iidPersona = iidPersona;
        this.itimbrado = itimbrado;
        this.dinicioVigencia = diniciovigencia;
        this.dfinVigencia = dfinvigencia;
        this.bactivo = bactivo;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidPersona, int itimbrado, java.util.Date diniciovigencia, java.util.Date dfinvigencia, boolean bactivo, short cestado) {
        this.iid = iid;
        this.iidPersona = iidPersona;
        this.itimbrado = itimbrado;
        this.dinicioVigencia = diniciovigencia;
        this.dfinVigencia = dfinvigencia;
        this.bactivo = bactivo;
        this.cestado = cestado;
    }

    public entPersonaDetalle copiar(entPersonaDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdPersona(), this.getTimbrado(), this.getInicioVigencia(), this.getFinVigencia(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }
    
    public entPersonaDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(java.sql.SQLException e) { }
        try { this.setIdPersona(rs.getInt("idpersona")); }
        catch(java.sql.SQLException e) { }
        try { this.setTimbrado(rs.getInt("timbrado")); }
        catch(java.sql.SQLException e) { }
        try { this.setInicioVigencia(rs.getDate("iniciovigencia")); }
        catch(java.sql.SQLException e) { }
        try { this.setFinVigencia(rs.getDate("finvigencia")); }
        catch(java.sql.SQLException e) { }
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(java.sql.SQLException e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdPersona(int iidPersona) {
        this.iidPersona = iidPersona;
    }

    public void setFinVigencia(java.util.Date dfinvigencia) {
        this.dfinVigencia = dfinvigencia;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setTimbrado(int itimbrado) {
        this.itimbrado = itimbrado;
    }

    public void setInicioVigencia(java.util.Date diniciovigencia) {
        this.dinicioVigencia = diniciovigencia;
    }
    
    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdPersona() {
        return this.iidPersona;
    }
        
    public java.util.Date getFinVigencia() {
        return this.dfinVigencia;
    }
   
    public boolean getActivo() {
        return this.bactivo;
    }
    
    public int getTimbrado() {
        return this.itimbrado;
    }
   
    public java.util.Date getInicioVigencia() {
        return this.dinicioVigencia;
    }
    
    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getTimbrado()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_TIMBRADO;
            bvalido = false;
        }
        if (this.getInicioVigencia()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_INICIO_VIGENCIA;
            bvalido = false;
        }
        if (this.getFinVigencia()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FIN_VIGENCIA;
            bvalido = false;
        }
        if(utilitario.utiFecha.getAMD(this.getInicioVigencia())>utilitario.utiFecha.getAMD(this.getFinVigencia())) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La fecha de inicio de vigencia es mayor que la fecha fin. Verifique.";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT personadetalle("+
            this.getId()+","+
            this.getIdPersona()+","+
            this.getTimbrado()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getInicioVigencia())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFinVigencia())+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

}
