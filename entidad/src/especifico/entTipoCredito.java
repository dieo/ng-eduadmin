/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entTipoCredito extends generico.entGenerica {

    protected boolean bactivo;
    protected String sdescripcionBreve;
    protected boolean bcancela;
    protected double uexoneracion;
    protected boolean bprestamo;
    protected boolean bgeneraRetencion;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final int LONGITUD_DESCRIPCION_BREVE = 3;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Tipo de Crédito (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_ACTIVO = "Activo";
    public static final String TEXTO_DESCRIPCION_BREVE = "Descripción breve de Tipo de Crédito (no vacía, hasta " + LONGITUD_DESCRIPCION_BREVE + " caracteres)";
    public static final String TEXTO_CANCELA = "Cancela";
    public static final String TEXTO_EXONERACION = "Porcentaje de Exoneración (valor positivo)";
    public static final String TEXTO_PRESTAMO = "Préstamo";
    public static final String TEXTO_GENERA_RETENCION = "Genera Retención";

    public entTipoCredito() {
        super();
        this.bactivo = false;
        this.sdescripcionBreve = "";
        this.bcancela = false;
        this.uexoneracion = 0.0;
        this.bprestamo = false;
        this.bgeneraRetencion = false;
    }

    public entTipoCredito(int iid, String sdescripcion, boolean bactivo, String sdescripcionBreve, boolean bcancela, double uexoneracion, boolean bprestamo, boolean bgeneraRetencion) {
        super(iid, sdescripcion);
        this.bactivo = bactivo;
        this.sdescripcionBreve = sdescripcionBreve;
        this.bcancela = bcancela;
        this.uexoneracion = uexoneracion;
        this.bprestamo = bprestamo;
        this.bgeneraRetencion = bgeneraRetencion;
    }

    public entTipoCredito(int iid, String sdescripcion, boolean bactivo, String sdescripcionBreve, boolean bcancela, double uexoneracion, boolean bprestamo, boolean bgeneraRetencion, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.bactivo = bactivo;
        this.sdescripcionBreve = sdescripcionBreve;
        this.bcancela = bcancela;
        this.uexoneracion = uexoneracion;
        this.bprestamo = bprestamo;
        this.bgeneraRetencion = bgeneraRetencion;
    }

    public void setEntidad(int iid, String sdescripcion, boolean bactivo, String sdescripcionBreve, boolean bcancela, double uexoneracion, boolean bprestamo, boolean bgeneraRetencion, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.bactivo = bactivo;
        this.sdescripcionBreve = sdescripcionBreve;
        this.bcancela = bcancela;
        this.uexoneracion = uexoneracion;
        this.bprestamo = bprestamo;
        this.bgeneraRetencion = bgeneraRetencion;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entTipoCredito copiar(entTipoCredito destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getActivo(), this.getDescripcionBreve(), this.getCancela(), this.getExoneracion(), this.getPrestamo(), this.getGeneraRetencion(), this.getEstadoRegistro());
        return destino;
    }

    public entTipoCredito cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setDescripcionBreve(rs.getString("descripcionbreve")); }
        catch(Exception e) {}
        try { this.setCancela(rs.getBoolean("cancela")); }
        catch(Exception e) {}
        try { this.setExoneracion(rs.getDouble("exoneracion")); }
        catch(Exception e) {}
        try { this.setPrestamo(rs.getBoolean("prestamo")); }
        catch(Exception e) {}
        try { this.setGeneraRetencion(rs.getBoolean("generaretencion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setDescripcionBreve(String sdescripcionBreve) {
        this.sdescripcionBreve = sdescripcionBreve;
    }

    public void setCancela(boolean bcancela) {
        this.bcancela = bcancela;
    }

    public void setExoneracion(double uexoneracion) {
        this.uexoneracion = uexoneracion;
    }

    public void setPrestamo(boolean bprestamo) {
        this.bprestamo = bprestamo;
    }

    public void setGeneraRetencion(boolean bgeneraRetencion) {
        this.bgeneraRetencion = bgeneraRetencion;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public String getDescripcionBreve() {
        return this.sdescripcionBreve;
    }

    public boolean getCancela() {
        return this.bcancela;
    }

    public double getExoneracion() {
        return this.uexoneracion;
    }

    public boolean getPrestamo() {
        return this.bprestamo;
    }

    public boolean getGeneraRetencion() {
        return this.bgeneraRetencion;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getDescripcionBreve().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION_BREVE;
            bvalido = false;
        }
        if (this.getExoneracion() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_EXONERACION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT tipocredito("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getActivo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcionBreve())+","+
            this.getCancela()+","+
            this.getExoneracion()+","+
            this.getPrestamo()+","+
            this.getGeneraRetencion()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
}
