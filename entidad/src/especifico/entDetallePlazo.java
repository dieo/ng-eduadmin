/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetallePlazo {

    private int iid;
    private int iplazoInicial;
    private int iplazoFinal;
    private double utasa;
    private int iidTipoCredito;
    private boolean bactivo;
    
    private short hestadoRegistro;
    private String smensaje;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_PLAZO_INICIAL = "Plazo inicial (valor positivo)";
    public static final String TEXTO_PLAZO_FINAL = "Plazo final (valor positivo)";
    public static final String TEXTO_TASA = "Tasa (valor positivo)";
    public static final String TEXTO_ACTIVO = "Activo";

    public entDetallePlazo() {
        this.iid = 0;
        this.iplazoInicial = 0;
        this.iplazoFinal = 0;
        this.utasa = 0.0;
        this.iidTipoCredito = 0;
        this.bactivo = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetallePlazo(int iid, int iplazoInicial, int iplazoFinal, double utasa, int iidTipoCredito, boolean bactivo) {
        this.iid = iid;
        this.iplazoInicial = iplazoInicial;
        this.iplazoFinal = iplazoFinal;
        this.utasa = utasa;
        this.iidTipoCredito = iidTipoCredito;
        this.bactivo = bactivo;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetallePlazo(int iid, int iplazoInicial, int iplazoFinal, double utasa, int iidTipoCredito, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.iplazoInicial = iplazoInicial;
        this.iplazoFinal = iplazoFinal;
        this.utasa = utasa;
        this.iidTipoCredito = iidTipoCredito;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iplazoInicial, int iplazoFinal, double utasa, int iidTipoCredito, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.iplazoInicial = iplazoInicial;
        this.iplazoFinal = iplazoFinal;
        this.utasa = utasa;
        this.iidTipoCredito = iidTipoCredito;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entDetallePlazo copiar(entDetallePlazo destino) {
        destino.setEntidad(this.getId(), this.getPlazoInicial(), this.getPlazoFinal(), this.getTasa(), this.getIdTipoCredito(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }

    public entDetallePlazo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setPlazoInicial(rs.getInt("plazoinicial")); }
        catch(Exception e) {}
        try { this.setPlazoFinal(rs.getInt("plazofinal")); }
        catch(Exception e) {}
        try { this.setTasa(rs.getDouble("tasa")); }
        catch(Exception e) {}
        try { this.setIdTipoCredito(rs.getInt("idtipocredito")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setPlazoInicial(int iplazoInicial) {
        this.iplazoInicial = iplazoInicial;
    }
    
    public void setPlazoFinal(int iplazoFinal) {
        this.iplazoFinal = iplazoFinal;
    }
    
    public void setTasa(double utasa) {
        this.utasa = utasa;
    }
    
    public void setIdTipoCredito(int iidTipoCredito) {
        this.iidTipoCredito = iidTipoCredito;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getPlazoInicial() {
        return this.iplazoInicial;
    }
    
    public int getPlazoFinal() {
        return this.iplazoFinal;
    }
    
    public double getTasa() {
        return this.utasa;
    }
    
    public int getIdTipoCredito() {
        return this.iidTipoCredito;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getPlazoInicial()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO_INICIAL;
            bvalido = false;
        }
        if (this.getPlazoFinal()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO_FINAL;
            bvalido = false;
        }
        if (this.getPlazoFinal()<=this.getPlazoInicial()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Plazo final debe ser mayor a Plazo inicial";
            bvalido = false;
        }
        if (this.getTasa()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TASA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detalleplazo("+
            this.getId()+","+
            this.getPlazoInicial()+","+
            this.getPlazoFinal()+","+
            this.getTasa()+","+
            this.getIdTipoCredito()+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

}
