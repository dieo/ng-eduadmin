/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDepuracionOperacion {

    private int iid;
    private String scuenta;
    private int inumeroOperacion;
    private java.util.Date dfechaOperacion;
    private String stabla;
    private int iplazo;
    private short hestadoRegistro;
    
    public entDepuracionOperacion() {
        this.iid = 0;
        this.scuenta = "";
        this.inumeroOperacion = 0;
        this.dfechaOperacion = null;
        this.stabla = "";
        this.iplazo = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDepuracionOperacion(int iid, String scuenta, int inumeroOperacion, java.util.Date dfechaOperacion, String stabla, int iplazo, short hestadoRegistro) {
        this.iid = iid;
        this.scuenta = scuenta;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaOperacion = dfechaOperacion;
        this.stabla = stabla;
        this.iplazo = iplazo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, String scuenta, int inumeroOperacion, java.util.Date dfechaOperacion, String stabla, int iplazo, short hestadoRegistro) {
        this.iid = iid;
        this.scuenta = scuenta;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaOperacion = dfechaOperacion;
        this.stabla = stabla;
        this.iplazo = iplazo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDepuracionOperacion copiar(entDepuracionOperacion destino) {
        destino.setEntidad(this.getId(), this.getCuenta(), this.getNumeroOperacion(), this.getFechaOperacion(), this.getTabla(), this.getPlazo(), this.getEstadoRegistro());
        return destino;
    }

    public entDepuracionOperacion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) { }
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) { }
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) { }
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) { }
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) { }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }
    
    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public String getCuenta() {
        return this.scuenta;
    }
    
    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public String getTabla() {
        return this.stabla;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getSentencia() {
        return "";
    }
    
    public String getConsulta(int iidSocio) {
        return "SELECT ope.tipo, ope.id, ope.idcuenta, cu.descripcion||' '||ope.descripcion AS cuenta, ope.numerooperacion, ope.fechaoperacion, ope.tabla, ope.plazo, ope.saldo FROM ( " +
            "   SELECT ope.tipo, ope.id, ope.idcuenta, ope.descripcion, ope.numerooperacion, ope.fechaoperacion, ope.tabla, ope.plazo, SUM(det.saldocapital+det.saldointeres) AS saldo FROM ( " +
            "      SELECT 'MO' AS tipo, ope.id, ope.idcuenta, tc.descripcionbreve||' ('||ent.descripcion||')' AS descripcion, ope.numerooperacion, ope.fechaaprobado AS fechaoperacion, 'mo' AS tabla, ope.plazoaprobado AS plazo " +
            "      FROM socio AS so " +
            "      LEFT JOIN movimiento AS ope ON so.id=ope.idsocio " +
            "      LEFT JOIN entidad AS ent ON ope.identidad=ent.id " +
            "      LEFT JOIN tipocredito AS tc ON ope.idtipocredito=tc.id " +
            "      WHERE so.id="+iidSocio+" AND (ope.idestado=179 OR ope.idestado=52) " +
            "      UNION " +
            "      SELECT " +
            "      (CASE WHEN ope.idcuenta=1 OR ope.idcuenta=2 OR ope.idcuenta=3 OR ope.idcuenta=4 OR ope.idcuenta=5 THEN 'AP' ELSE 'OF' END) AS tipo, " +
            "      ope.id, ope.idcuenta, '' AS descripcion, ope.numerooperacion, ope.fechaoperacion, 'of' AS tabla, ope.plazo " +
            "      FROM socio AS so " +
            "      LEFT JOIN operacionfija AS ope ON so.id=ope.idsocio " +
            "      WHERE so.id="+iidSocio+" AND ope.idestado=86 " +
            "      UNION " +
            "      SELECT 'OF' AS tipo, ope.id, ope.idcuenta, pla.descripcion AS descripcion, ope.numerooperacion, ope.fechaoperacion, 'ap' AS tabla, ope.plazo " +
            "      FROM socio AS so " +
            "      LEFT JOIN ahorroprogramado AS ope ON so.id=ope.idsocio " +
            "      LEFT JOIN planahorroprogramado AS pla ON ope.idplan=pla.id " +
            "      WHERE so.id="+iidSocio+" AND ope.idestado=53 " +
            "      UNION " +
            "      SELECT 'OF' AS tipo, ope.id, ope.idcuenta, '' AS descripcion, ope.numerooperacion, ope.fechaoperacion, 'js' AS tabla, 0 AS plazo " +
            "      FROM socio AS so " +
            "      LEFT JOIN fondojuridicosepelio AS ope ON so.id=ope.idsocio " +
            "      WHERE so.id="+iidSocio+" AND ope.idestado=57 " +
            "   ) AS ope " +
            "   LEFT JOIN detalleoperacion AS det ON (ope.id=det.idmovimiento AND ope.tabla=det.tabla) " +
            "   GROUP BY ope.tipo, ope.id, ope.idcuenta, ope.descripcion, ope.numerooperacion, ope.fechaoperacion, ope.tabla, ope.plazo " +
            ") AS ope " +
            "LEFT JOIN cuenta AS cu ON ope.idcuenta=cu.id " +
            "WHERE (ope.saldo>0 AND ope.plazo>0) OR (ope.plazo=0)" +
            "ORDER BY ope.idcuenta";
    }

}
