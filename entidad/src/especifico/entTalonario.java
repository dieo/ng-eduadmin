/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entTalonario {
    
    private int iid;
    private int iidTipoTalonario;
    private String stipoTalonario;
    private int iidSucursal;
    private String ssucursal;
    private String snumeroSucursal;
    private int iidEmisorTalonario;
    private String semisorTalonario;
    private int iidExpedicion;
    private String sexpedicion;
    private String snumeroExpedicion;
    private java.util.Date dfechaOperacion;
    private String sserie;
    private int inumeroInicial;
    private int inumeroFinal;
    private java.util.Date dfechaVencimiento;
    private int iidEstado;
    private String sestado;
    private int isecuencia;
    private int iidTipoPermiso;
    private String stipoPermiso;
    private String sdescripcion;
    private short hestadoRegistro;
    private String smensaje;

    private java.util.Date dfechaActivo;
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    public static final int LONGITUD_SERIE = 2;
    public static final int LONGITUD_OBSERVACION = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_TIPO_TALONARIO = "Tipo de Talonario (no vacío)";
    public static final String TEXTO_SUCURSAL = "Sucursal (no vacía)";
    public static final String TEXTO_EMISOR_TALONARIO = "Emisor de Talonario (no vacío)";
    public static final String TEXTO_EXPEDICION = "Punto de Expedición del Talonario (no vacío)";
    public static final String TEXTO_FECHA_OPERACION = "Fecha de Operación (no vacía)";
    public static final String TEXTO_SERIE = "Serie";
    public static final String TEXTO_NUMERO_INICIAL = "Número Inicial (valor positivo, menor al Número Final)";
    public static final String TEXTO_NUMERO_FINAL = "Número Final (valor positivo, mayor al Número Inicial)";
    public static final String TEXTO_FECHA_VENCIMIENTO = "Fecha de Vencimiento (no vacía)";
    public static final String TEXTO_SECUENCIA = "Secuencia (no editable)";
    public static final String TEXTO_ESTADO = "Estado del Talonario";
    public static final String TEXTO_DESCRIPCION = "Descripción del Talonario (no editable)";

    public static final String TEXTO_TIPO_PERMISO = "Tipo de Permiso del Talonario (no vacío)";
    public static final String TEXTO_PERFIL = "Perfiles";
    public static final String TEXTO_PERFIL_PERMITIDO = "Perfiles permitidos";
    public static final String TEXTO_USUARIO = "Usuarios";
    public static final String TEXTO_USUARIO_PERMITIDO = "Usuarios permitidos";
    
    public static final String TEXTO_FECHA_ACTIVO = "Fecha de Activación (no editable)";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    
    public entTalonario() {
        this.iid = 0;
        this.iidTipoTalonario = 0;
        this.stipoTalonario = "";
        this.iidSucursal = 0;
        this.ssucursal = "";
        this.snumeroSucursal = "";
        this.iidEmisorTalonario = 0;
        this.semisorTalonario = "";
        this.iidExpedicion = 0;
        this.sexpedicion = "";
        this.snumeroExpedicion = "";
        this.dfechaOperacion = new java.util.Date();
        this.sserie = "";
        this.inumeroInicial = 0;
        this.inumeroFinal = 0;
        this.dfechaVencimiento = null;
        this.iidEstado = 0;
        this.sestado = "";
        this.isecuencia = 0;
        this.iidTipoPermiso = 0;
        this.stipoPermiso = "";
        this.dfechaActivo = null;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.sdescripcion = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entTalonario(int iid, int iidTipoTalonario, String stipoTalonario, int iidSucursal, String ssucursal, String snumeroSucursal, int iidEmisorTalonario, String semisorTalonario, int iidExpedicion, String sexpedicion, String snumeroExpedicion, java.util.Date dfechaOperacion, String sserie, int inumeroInicial, int inumeroFinal, java.util.Date dfechaVencimiento, int iidEstado, String sestado, int isecuencia, int iidTipoPermiso, String stipoPermiso, java.util.Date dfechaActivo, java.util.Date dfechaAnulado, String sobservacionAnulado, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.iidTipoTalonario = iidTipoTalonario;
        this.stipoTalonario = stipoTalonario;
        this.iidSucursal = iidSucursal;
        this.ssucursal = ssucursal;
        this.snumeroSucursal = snumeroSucursal;
        this.iidEmisorTalonario = iidEmisorTalonario;
        this.semisorTalonario = semisorTalonario;
        this.iidExpedicion = iidExpedicion;
        this.sexpedicion = sexpedicion;
        this.snumeroExpedicion = snumeroExpedicion;
        this.dfechaOperacion = dfechaOperacion;
        this.sserie = sserie;
        this.inumeroInicial = inumeroInicial;
        this.inumeroFinal = inumeroFinal;
        this.dfechaVencimiento = dfechaVencimiento;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.isecuencia = isecuencia;
        this.iidTipoPermiso = iidTipoPermiso;
        this.stipoPermiso = stipoPermiso;
        this.dfechaActivo = dfechaActivo;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
     public void setEntidad(int iid, int iidTipoTalonario, String stipoTalonario, int iidSucursal, String ssucursal, String snumeroSucursal, int iidEmisorTalonario, String semisorTalonario, int iidExpedicion, String sexpedicion, String snumeroExpedicion, java.util.Date dfechaOperacion, String sserie, int inumeroInicial, int inumeroFinal, java.util.Date dfechaVencimiento, int iidEstado, String sestado, int isecuencia, int iidTipoPermiso, String stipoPermiso, java.util.Date dfechaActivo, java.util.Date dfechaAnulado, String sobservacionAnulado, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.iidTipoTalonario = iidTipoTalonario;
        this.stipoTalonario = stipoTalonario;
        this.iidSucursal = iidSucursal;
        this.ssucursal = ssucursal;
        this.snumeroSucursal = snumeroSucursal;
        this.iidEmisorTalonario = iidEmisorTalonario;
        this.semisorTalonario = semisorTalonario;
        this.iidExpedicion = iidExpedicion;
        this.sexpedicion = sexpedicion;
        this.snumeroExpedicion = snumeroExpedicion;
        this.dfechaOperacion = dfechaOperacion;
        this.sserie = sserie;
        this.inumeroInicial = inumeroInicial;
        this.inumeroFinal = inumeroFinal;
        this.dfechaVencimiento = dfechaVencimiento;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.isecuencia = isecuencia;
        this.iidTipoPermiso = iidTipoPermiso;
        this.stipoPermiso = stipoPermiso;
        this.dfechaActivo = dfechaActivo;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entTalonario copiar(entTalonario destino) {
        destino.setEntidad(this.getId(), this.getIdTipoTalonario(), this.getTipoTalonario(), this.getIdSucursal(), this.getSucursal(), this.getNumeroSucursal(), this.getIdEmisorTalonario(), this.getEmisorTalonario(), this.getIdExpedicion(), this.getExpedicion(), this.getNumeroExpedicion(), this.getFechaOperacion(), this.getSerie(), this.getNumeroInicial(), this.getNumeroFinal(), this.getFechaVencimiento(), this.getIdEstado(), this.getEstado(), this.getSecuencia(), this.getIdTipoPermiso(), this.getTipoPermiso(), this.getFechaActivo(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getDescripcion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entTalonario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdTipoTalonario(rs.getInt("idtipotalonario")); }
        catch(Exception e) {}
        try { this.setTipoTalonario(rs.getString("tipotalonario")); }
        catch(Exception e) {}
        try { this.setIdSucursal(rs.getInt("idsucursal")); }
        catch(Exception e) {}
        try { this.setSucursal(rs.getString("sucursal")); }
        catch(Exception e) {}
        try { this.setNumeroSucursal(rs.getString("numerosucursal")); }
        catch(Exception e) {}
        try { this.setIdEmisorTalonario(rs.getInt("idemisortalonario")); }
        catch(Exception e) {}
        try { this.setEmisorTalonario(rs.getString("emisortalonario")); }
        catch(Exception e) {}
        try { this.setIdExpedicion(rs.getInt("idexpedicion")); }
        catch(Exception e) {}
        try { this.setExpedicion(rs.getString("expedicion")); }
        catch(Exception e) {}
        try { this.setNumeroExpedicion(rs.getString("numeroexpedicion")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setSerie(rs.getString("serie")); }
        catch(Exception e) {}
        try { this.setNumeroInicial(rs.getInt("numeroinicial")); }
        catch(Exception e) {}
        try { this.setNumeroFinal(rs.getInt("numerofinal")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setSecuencia(rs.getInt("secuencia")); }
        catch(Exception e) {}
        try { this.setIdTipoPermiso(rs.getInt("idtipopermiso")); }
        catch(Exception e) {}
        try { this.setTipoPermiso(rs.getString("tipopermiso")); }
        catch(Exception e) {}
        try { this.setFechaActivo(rs.getDate("fechaactivo")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdTipoTalonario(int iidTipoTalonario) {
        this.iidTipoTalonario = iidTipoTalonario;
    }
    
    public void setTipoTalonario(String stipoTalonario) {
        this.stipoTalonario = stipoTalonario;
    }
    
    public void setIdSucursal(int iidSucursal) {
        this.iidSucursal = iidSucursal;
    }
    
    public void setSucursal(String ssucursal) {
        this.ssucursal = ssucursal;
    }
    
    public void setNumeroSucursal(String snumeroSucursal) {
        this.snumeroSucursal = snumeroSucursal;
    }
    
    public void setIdEmisorTalonario(int iidEmisorTalonario) {
        this.iidEmisorTalonario = iidEmisorTalonario;
    }
    
    public void setEmisorTalonario(String semisorTalonario) {
        this.semisorTalonario = semisorTalonario;
    }
    
    public void setIdExpedicion(int iidExpedicion) {
        this.iidExpedicion = iidExpedicion;
    }
    
    public void setExpedicion(String sexpedicion) {
        this.sexpedicion = sexpedicion;
    }
    
    public void setNumeroExpedicion(String snumeroExpedicion) {
        this.snumeroExpedicion = snumeroExpedicion;
    }
    
    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setSerie(String sserie) {
        this.sserie = sserie.trim();
    }
    
    public void setNumeroInicial(int inumeroInicial) {
        this.inumeroInicial = inumeroInicial;
    }
    
    public void setNumeroFinal(int inumeroFinal) {
        this.inumeroFinal = inumeroFinal;
    }
    
    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }
    
    public void setEstado(String sestado) {
        this.sestado = sestado;
    }
    
    public void setSecuencia(int isecuencia) {
        this.isecuencia = isecuencia;
    }

    public void setIdTipoPermiso(int iidTipoPermiso) {
        this.iidTipoPermiso = iidTipoPermiso;
    }
    
    public void setTipoPermiso(String stipoPermiso) {
        this.stipoPermiso = stipoPermiso;
    }
    
    public void setFechaActivo(java.util.Date dfechaActivo) {
        this.dfechaActivo = dfechaActivo;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getIdTipoTalonario() {
        return this.iidTipoTalonario;
    }

    public String getTipoTalonario() {
        return this.stipoTalonario;
    }
    
    public int getIdSucursal() {
        return this.iidSucursal;
    }
    
    public String getSucursal() {
        return this.ssucursal;
    }
    
    public String getNumeroSucursal() {
        return this.snumeroSucursal;
    }
    
    public int getIdEmisorTalonario() {
        return this.iidEmisorTalonario;
    }
    
    public String getEmisorTalonario() {
        return this.semisorTalonario;
    }
    
    public int getIdExpedicion() {
        return this.iidExpedicion;
    }
    
    public String getExpedicion() {
        return this.sexpedicion;
    }
    
    public String getNumeroExpedicion() {
        return this.snumeroExpedicion;
    }
    
    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public String getSerie() {
        return this.sserie;
    }
    
    public int getNumeroInicial() {
        return this.inumeroInicial;
    }
    
    public int getNumeroFinal() {
        return this.inumeroFinal;
    }
    
    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }
    
    public String getEstado() {
        return this.sestado;
    }
    
    public int getSecuencia() {
        return this.isecuencia;
    }

    public int getIdTipoPermiso() {
        return this.iidTipoPermiso;
    }
    
    public String getTipoPermiso() {
        return this.stipoPermiso;
    }
    
    public java.util.Date getFechaActivo() {
        return this.dfechaActivo;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar(int iidCheque, int iidOrdenCredito) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdTipoTalonario() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_TALONARIO;
            bvalido = false;
        }
       if (this.getIdEmisorTalonario() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_EMISOR_TALONARIO;
            bvalido = false;
        }
        if (this.getFechaOperacion() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getNumeroInicial() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_INICIAL;
            bvalido = false;
        }
        if (this.getNumeroFinal() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_FINAL;
            bvalido = false;
        }
        if (this.getIdTipoTalonario() != iidCheque && this.getIdTipoTalonario() != iidOrdenCredito) {
            if (this.getFechaVencimiento() == null) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += TEXTO_FECHA_VENCIMIENTO;
                bvalido = false;
            }
        }
        if (this.getNumeroInicial()>0 && this.getNumeroFinal()>0) {
            if (this.getNumeroFinal() < this.getNumeroInicial()) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Número Final debe ser mayor a Numero Final";
                bvalido = false;
            }
        }
        if (this.getFechaOperacion()!=null && this.getFechaVencimiento()!=null) {
            if (this.getFechaVencimiento().getTime() <= this.getFechaOperacion().getTime()) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Fecha de Vencimiento debe ser mayor a la Fecha de Operación";
                bvalido = false;
            }
        }
        return bvalido;
    }

    public boolean esValidoOtorgarPermiso(int cantidadPerfil, int cantidadUsuario) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdTipoPermiso() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_PERMISO;
            bvalido = false;
        }
        if (cantidadPerfil+cantidadUsuario <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Debe haber un Perfil o un Usuario cargado";
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT talonario("+
            this.getId()+","+
            this.getIdTipoTalonario()+","+
            this.getNumeroInicial()+","+
            this.getNumeroFinal()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaVencimiento())+","+
            this.getIdEstado()+","+
            utilitario.utiCadena.getTextoGuardado(this.getSerie())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaOperacion())+","+
            this.getIdTipoPermiso()+","+
            this.getIdEmisorTalonario()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaActivo())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumeroSucursal())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumeroExpedicion())+","+
            this.getEstadoRegistro()+")";
    }

    public String getCampoTalonarioActualizaEstado(utilitario.conConexion conexion) {
        // pasa a vencido todos los inactivos o activos que han vencido y a terminado todos los activos que han terminado
        return "SELECT talonarioactualizaestado("+
            conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "ACTIVO")+","+
            conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "INACTIVO")+","+
            conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "VENCIDO")+","+
            conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "TERMINADO")+")";
    }

    public String getCampoTalonarioPermiso(int iid, int iidTalonario, int iidPerfil, int iidUsuario, short hoperador) {
        return "SELECT talonariopermiso("+
            iid+","+
            iidTalonario+","+
            iidPerfil+","+
            iidUsuario+","+
            hoperador+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Estado", "estado", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Emisor Talonario", "emisortalonario", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Fecha de Operación", "fechaoperacion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(5, "Fecha de Vencimiento", "fechavencimiento", generico.entLista.tipo_fecha));
        return lst;
    }

}
