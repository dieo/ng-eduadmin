/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;

/**
 *
 * @author Lic. Didier Barreto
 */
public class entCDATitular {
    
    private int iid;
    private String scedula;
    private String sdescripcion;
    
    protected short hestado;
    protected String smensaje;
    
    public static final int LONGITUD_CEDULA = 12;
    public static final int LONGITUD_DESCRIPCION = 100;
        
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CEDULA = "Nº de Cédula (no vacía, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_DESCRIPCION = "Descripción (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    
    public entCDATitular() {
        this.iid = 0;
        this.scedula = "";
        this.sdescripcion = "";
        this.hestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entCDATitular(int iid, String scedula, String sdescripcion, short hestado) {
        this.iid = iid;
        this.scedula = scedula;
        this.sdescripcion = sdescripcion;
        this.hestado = hestado;
    }
    
     public void setEntidad(int iid, String scedula, String sdescripcion, short hestado) {
        this.iid = iid;
        this.scedula = scedula;
        this.sdescripcion = sdescripcion;
        this.hestado = hestado;
    }

    public entCDATitular copiar(entCDATitular destino) {
        destino.setEntidad(this.getId(), this.getCedula(), this.getDescripcion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entCDATitular cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(SQLException e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(SQLException e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(SQLException e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }
    
    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setEstadoRegistro(short hestado) {
        this.hestado = hestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public String getCedula() {
        return this.scedula;
    }
    
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public short getEstadoRegistro() {
        return this.hestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT cdatitular("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número de Cédula", "cedula", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Nombre Y Apellido", "descripcion", new generico.entLista().tipo_texto));
        return lst;
    }

}
