/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbTransferencia {
    
    private int icodigo;
    private java.util.Date dfecha;
    private int iorden;
    protected String stipocomp;
    private int inrocompro;
    private int imoneda;
    private int icotizacion;
    private int imodelo;
    private int ixcodigo;
    private short hestadoRegistro;
    private String smensaje;
        
    
    public entbTransferencia() {
        this.icodigo = 0;
        this.iorden = 0;
        this.dfecha = null;
        this.stipocomp = "";
        this.inrocompro = 0;
        this.imoneda = 0;
        this.icotizacion = 0;
        this.imodelo = 0;
        this.ixcodigo = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbTransferencia(int icodigo, int iorden, java.util.Date dfecha, String stipocomp, int inrocompro, int imoneda, int icotizacion, int imodelo, int ixcodigo, short hestadoRegistro) {
        this.icodigo = icodigo;
        this.iorden = iorden;
        this.dfecha = dfecha;
        this.stipocomp = stipocomp;
        this.inrocompro = inrocompro;
        this.imoneda = imoneda;
        this.icotizacion = icotizacion;
        this.imodelo = imodelo;
        this.ixcodigo = ixcodigo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int icodigo, int iorden, java.util.Date dfecha, String stipocomp, int inrocompro, int imoneda, int icotizacion, int imodelo, int ixcodigo, short hestadoRegistro) {
        this.icodigo = icodigo;
        this.iorden = iorden;
        this.dfecha = dfecha;
        this.stipocomp = stipocomp;
        this.inrocompro = inrocompro;
        this.imoneda = imoneda;
        this.icotizacion = icotizacion;
        this.imodelo = imodelo;
        this.ixcodigo = ixcodigo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entbTransferencia copiar(entbTransferencia destino) {
        destino.setEntidad(this.getCodigo(), this.getOrden(), this.getFecha(), this.getTipoComp(), this.getComprobante(), this.getMoneda(), this.getCotizacion(), this.getModelo(),this.getXcodigo(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbTransferencia cargar(java.sql.ResultSet rs) {
        try { this.setCodigo(rs.getInt("codigo")); }
        catch(Exception e) {}
        try { this.setOrden(rs.getInt("orden")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setTipoComp(rs.getString("tipocomp")); }
        catch(Exception e) {}
        try { this.setComprobante(rs.getInt("nrocompro")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setCodigo(int icodigo) {
        this.icodigo = icodigo;
    }

    public void setOrden(int iorden) {
        this.iorden = iorden;
    }
    
    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }
    
    public void setTipoComp(String stipocomp) {
        this.stipocomp = stipocomp;
    }
    
    public void setComprobante(int inrocompro) {
        this.inrocompro = inrocompro;
    }
    
    public void setMoneda(int imoneda) {
        this.imoneda = imoneda;
    }
    
    public void setCotizacion(int icotizacion) {
        this.icotizacion = icotizacion;
    }
    
    public void setModelo(int imodelo) {
        this.imodelo = imodelo;
    }
    
    public void setXCodigo(int ixcodigo) {
        this.ixcodigo = ixcodigo;
    } 
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public int getCodigo() {
        return this.icodigo;
    }

    public int getOrden() {
        return this.iorden;
    }
    
    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public String getTipoComp() {
        return this.stipocomp;
    }
    
    public int getComprobante() {
        return this.inrocompro;
    }
     
     public int getMoneda() {
        return this.imoneda;
    }
    
    public int getCotizacion() {
        return this.icotizacion;
    }
    
    public int getModelo() {
        return this.imodelo;
    }
    
    public int getXcodigo() {
        return this.ixcodigo;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        
        return bvalido;
    }

    /*public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cuenta Banco", "descripcion", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Concepto", "concepto", new generico.entLista().tipo_texto));
        return lst;
    }*/
    
}