/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMovimientoDescuento {

    private int iid;
    private int iidAnalisisDisponibilidad;
    private int iidDescuento;
    private String sdescuento;
    private double uimporteActual;
    private double uimporteSiguiente;
    private short hestadoRegistro;
    private String smensaje;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCUENTO = "Descuento (no vacío)";
    public static final String TEXTO_IMPORTE_ACTUAL = "Importe actual (valor positivo)";
    public static final String TEXTO_IMPORTE_SIGUIENTE = "Importe siguiente (valor positivo)";

    public entMovimientoDescuento() {
        this.iid = 0;
        this.iidAnalisisDisponibilidad = 0;
        this.iidDescuento = 0;
        this.sdescuento = "";
        this.uimporteActual = 0.0;
        this.uimporteSiguiente = 0.0;
        this.uimporteSiguiente = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entMovimientoDescuento(int iid, int iidAnalisisDisponibilidad, int iidDescuento, String sdescuento, double uimporteActual, double uimporteSiguiente, short hestadoRegistro) {
        this.iid = iid;
        this.iidAnalisisDisponibilidad = iidAnalisisDisponibilidad;
        this.iidDescuento = iidDescuento;
        this.sdescuento = sdescuento;
        this.uimporteActual = uimporteActual;
        this.uimporteSiguiente = uimporteSiguiente;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidAnalisisDisponibilidad, int iidDescuento, String sdescuento, double uimporteActual, double uimporteSiguiente, short hestadoRegistro) {
        this.iid = iid;
        this.iidAnalisisDisponibilidad = iidAnalisisDisponibilidad;
        this.iidDescuento = iidDescuento;
        this.sdescuento = sdescuento;
        this.uimporteActual = uimporteActual;
        this.uimporteSiguiente = uimporteSiguiente;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public entMovimientoDescuento copiar(entMovimientoDescuento destino) {
        destino.setEntidad(this.getId(), this.getIdAnalisisDisponibilidad(), this.getIdDescuento(), this.getDescuento(), this.getImporteActual(), this.getImporteSiguiente(), this.getEstadoRegistro());
        return destino;
    }

    public entMovimientoDescuento cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdAnalisisDisponibilidad(rs.getInt("idanalisisdisponibilidad")); }
        catch(Exception e) {}
        try { this.setIdDescuento(rs.getInt("iddescuento")); }
        catch(Exception e) {}
        try { this.setDescuento(rs.getString("descuento")); }
        catch(Exception e) {}
        try { this.setImporteActual(rs.getDouble("importeactual")); }
        catch(Exception e) {}
        try { this.setImporteSiguiente(rs.getDouble("importesiguiente")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdAnalisisDisponibilidad(int iidAnalisisDisponibilidad) {
        this.iidAnalisisDisponibilidad = iidAnalisisDisponibilidad;
    }

    public void setIdDescuento(int iidDescuento) {
        this.iidDescuento = iidDescuento;
    }

    public void setDescuento(String sdescuento) {
        this.sdescuento = sdescuento;
    }

    public void setImporteActual(double uimporteActual) {
        this.uimporteActual = uimporteActual;
    }

    public void setImporteSiguiente(double uimporteSiguiente) {
        this.uimporteSiguiente = uimporteSiguiente;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdAnalisisDisponibilidad() {
        return this.iidAnalisisDisponibilidad;
    }

    public int getIdDescuento() {
        return this.iidDescuento;
    }

    public String getDescuento() {
        return this.sdescuento;
    }

    public double getImporteActual() {
        return this.uimporteActual;
    }

    public double getImporteSiguiente() {
        return this.uimporteSiguiente;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdDescuento()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCUENTO;
            bvalido = false;
        }
        if (this.getImporteActual()<0 && this.getImporteSiguiente()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Uno de los Importes debe ser mayor a 0";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT movimientodescuento("+
            this.getId()+","+
            this.getIdAnalisisDisponibilidad()+","+
            this.getIdDescuento()+","+
            this.getImporteActual()+","+
            this.getImporteSiguiente()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descuento", "descuento", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Importe actual", "importeactual", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(3, "Importe siguiente", "importesiguiente", generico.entLista.tipo_numero));
        return lst;
    }
    
}
