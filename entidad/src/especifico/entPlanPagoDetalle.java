/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.util.Date;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entPlanPagoDetalle {

    private int iid;
    private java.util.Date dfechapago;
    private java.util.Date dfechavencimiento;
    private double dmonto;
    private int inumero;
    private double dsaldo;
    private int iidEstado;
    private String sestado;
    private int iidplanpago;
    private int iidfacturaegreso;
    //General
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_CONCEPTO = 100;
    public static final int LONGITUD_OBSERVACION = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMERO = "Número (no vacía)";
    public static final String TEXTO_MONTO = "Monto (no vacía)";
    public static final String TEXTO_FECHA_PAGO = "Fecha de Pago (no vacía)";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public static final String TEXTO_FECHA_CONFIRMADO = "Fecha de Confirmación (no editable)";
    public static final String TEXTO_OBSERVACION_CONFIRMADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public entPlanPagoDetalle() {
        this.iid = 0;
        this.dfechapago = null;
        this.dfechavencimiento = null;
        this.dmonto = 0.0;
        this.inumero = 0;
        this.dsaldo = 0.0;
        this.iidEstado = 0;
        this.sestado = "";
        this.iidplanpago = 0;
        this.iidfacturaegreso = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entPlanPagoDetalle(int iid, java.util.Date dfechapago, java.util.Date fechavencimiento, double dmonto, int inumero, double dsaldo, int iidEstado, String sestado, int iidplanpago, int iidfacturaegreso, short hestadoRegistro) {
        this.iid = iid;
        this.dfechapago = dfechapago;
        this.dfechavencimiento = fechavencimiento;
        this.dmonto = dmonto;
        this.inumero = inumero;
        this.dsaldo = dsaldo;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidplanpago = iidplanpago;
        this.iidfacturaegreso = iidfacturaegreso;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, java.util.Date dfechapago, java.util.Date fechavencimiento, double dmonto, int inumero, double dsaldo, int iidEstado, String sestado, int iidplanpago, int iidfacturaegreso, short hestadoRegistro) {
        this.iid = iid;
        this.dfechapago = dfechapago;
        this.dfechavencimiento = fechavencimiento;
        this.dmonto = dmonto;
        this.inumero = inumero;
        this.dsaldo = dsaldo;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidplanpago = iidplanpago;
        this.iidfacturaegreso = iidfacturaegreso;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entPlanPagoDetalle copiar(entPlanPagoDetalle destino) {
        destino.setEntidad(this.getId(), this.getFechapago(), this.getFechavencimiento(), this.getMonto(),
                this.getNumero(), this.getSaldo(), this.getIdEstado(), this.getEstado(), this.getIdplanpago(), this.getIdFacturaEgreso(), this.getEstadoRegistro());
        return destino;
    }

    public entPlanPagoDetalle cargar(java.sql.ResultSet rs) {
        try {
            this.setId(rs.getInt("id"));
        } catch (Exception e) {
        }
        try {
            this.setFechapago(rs.getDate("fechapago"));
        } catch (Exception e) {
        }
        try {
            this.setFechavencimiento(rs.getDate("fechavencimiento"));
        } catch (Exception e) {
        }
        try {
            this.setMonto(rs.getDouble("monto"));
        } catch (Exception e) {
        }
        try {
            this.setNumero(rs.getInt("numero"));
        } catch (Exception e) {
        }
        try {
            this.setSaldo(rs.getDouble("saldo"));
        } catch (Exception e) {
        }
        try {
            this.setIdEstado(rs.getInt("idestado"));
        } catch (Exception e) {
        }
        try {
            this.setEstado(rs.getString("estadoplan"));
        } catch (Exception e) {
        }
        try {
            this.setIdplanpago(rs.getInt("idplanpago"));
        } catch (Exception e) {
        }
        try {
            this.setIdFacturaEgreso(rs.getInt("idfacturaegreso"));
        } catch (Exception e) {
        }
        return this;
    }

    public int getId() {
        return iid;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public Date getFechapago() {
        return dfechapago;
    }

    public void setFechapago(Date dfechapago) {
        this.dfechapago = dfechapago;
    }

    public Date getFechavencimiento() {
        return dfechavencimiento;
    }

    public void setFechavencimiento(Date dfechavencimiento) {
        this.dfechavencimiento = dfechavencimiento;
    }

    public double getMonto() {
        return dmonto;
    }

    public void setMonto(double dmonto) {
        this.dmonto = dmonto;
    }

    public int getNumero() {
        return inumero;
    }

    public void setNumero(int inumero) {
        this.inumero = inumero;
    }

    public double getSaldo() {
        return dsaldo;
    }

    public void setSaldo(double dsaldo) {
        this.dsaldo = dsaldo;
    }

    public int getIdEstado() {
        return iidEstado;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public String getEstado() {
        return sestado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public int getIdplanpago() {
        return iidplanpago;
    }

    public void setIdplanpago(int iidplanpago) {
        this.iidplanpago = iidplanpago;
    }
    
    public void setIdFacturaEgreso(int iidfactura) {
        this.iidfacturaegreso = iidfactura;
    }
    
    public int getIdFacturaEgreso() {
        return iidfacturaegreso;
    }

    public short getEstadoRegistro() {
        return hestadoRegistro;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public String getMensaje() {
        return smensaje;
    }

    public void setMensaje(String smensaje) {
        this.smensaje = smensaje;
    }

    public boolean esValidoSolicitar() {
        boolean bvalido = true;
        this.smensaje = "";
//        if (this.getIdCentroCosto()<=0) {
//            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
//            this.smensaje += this.TEXTO_CENTRO_COSTO;
//            bvalido = false;
//        }
//        if (this.getFecha() == null) {
//            if (!this.smensaje.isEmpty()) {
//                this.smensaje += "\n";
//            }
//            this.smensaje += this.TEXTO_FECHA;
//            bvalido = false;
//        }
        if (this.getFechapago() == null) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_FECHA_PAGO;
            bvalido = false;
        }
        if (this.getMonto() < 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_MONTO;
            bvalido = false;
        }
        if (this.getNumero() < 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_NUMERO;
            bvalido = false;
        }
//        if (this.getIdfacturaingreso() <= 0) {
//            if (!this.smensaje.isEmpty()) {
//                this.smensaje += "\n";
//            }
//            this.smensaje += this.TEXTO_FACTURA;
//            bvalido = false;
//        }
        return bvalido;
    }

//    public boolean esValidoAnular() {
//        boolean bvalido = true;
//        this.smensaje = "";
//        if (this.getObservacionAnulado().isEmpty()) {
//            if (!this.smensaje.isEmpty()) {
//                this.smensaje += "\n";
//            }
//            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
//            bvalido = false;
//        }
//        return bvalido;
//    }
    public String getSentencia() {
        return "SELECT public.planpagodetalle("
                + this.getId() + ","
                + utilitario.utiFecha.getFechaGuardadoMac(this.getFechapago()) + ","
                + utilitario.utiFecha.getFechaGuardadoMac(this.getFechavencimiento()) + ","
                + this.getMonto() + ","
                + this.getNumero() + ","
                + this.getSaldo() + ","
                + this.getIdEstado() + ","
                + this.getIdplanpago() + ","
                + this.getIdFacturaEgreso() + ","
                + this.getEstadoRegistro() + ")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
//        lst.add(new generico.entLista(2, "Número de Orden", "nroordenpago", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(1, "Nro", "id", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Fecha Pago", "fechapago", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Fecha Vencimiento", "fechavencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(4, "Nro Plan Cobro", "idplancobro", generico.entLista.tipo_numero));
        return lst;
    }

}
