/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entAhorroProgramado {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String sapellidoNombre;
    private String scontrato;
    private int iidEstadoAhorro;
    private String sestadoAhorro;
    private int iidPlan;
    private String splan;
    private double uimporte;
    private int iidMoneda;
    private String smoneda;
    private int iplazo;
    private double utasaInteres;
    private java.util.Date dfechaOperacion;
    private int iidPromotor;
    private String spromotor;
    private java.util.Date dfechaPrimerVencimiento;
    private java.util.Date dfechaVencimiento;
    private int iidRegional;
    private String sregional;
    private int inumeroOperacion;
    private int iidCuenta;
    private String scuenta;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    private java.util.Date dfechaRetirado;
    private String sobservacionRetirado;
    private double umontoAportado;
    private double umontoLiquidacion;
    private double umontoInteres;
    private double ugastoAdministrativo;
    private double umontoRetirado;
    private double utasaInteresRetirado;
    
    private java.util.Date dfechaCumplido;
    private String sobservacionCumplido;
    
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_CONTRATO = 10;
    public static final int LONGITUD_OBSERVACION = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ESTADO = "Estado de Ahorro Programado (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_CONTRATO = "Contrato (no vacío, hasta " + LONGITUD_CONTRATO + " caracteres)";
    public static final String TEXTO_IMPORTE = "Importe (valor positivo, mayor o igual al Importe base)";
    public static final String TEXTO_PLAN = "Plan de Ahorro Programado (no vacío)";
    public static final String TEXTO_FECHA_OPERACION = "Fecha de Operación (no vacía)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_PLAZO = "Plazo (no editable)";
    public static final String TEXTO_TASA_INTERES = "Tasa de Interés (no editable)";
    public static final String TEXTO_FECHA_PRIMER_VENCIMIENTO = "Fecha del Primer Vencimiento (no nulo, menor o igual a la próxima Fecha de Cierre)";
    public static final String TEXTO_FECHA_VENCIMIENTO = "Fecha de Vencimiento de la Operación (no editable)";
    public static final String TEXTO_PROMOTOR = "Promotor (no vacío)";
    public static final String TEXTO_REGIONAL = "Regional (no vacío)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (no editable)";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_RETIRADO = "Fecha de Operación (no editable)";
    public static final String TEXTO_OBSERVACION_RETIRADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_CUMPLIDO = "Fecha de Cumplimiento (no editable)";
    public static final String TEXTO_OBSERVACION_CUMPLIDO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public static final String TEXTO_MONTO_APORTADO = "Monto del Aporte (no editable)";
    public static final String TEXTO_MONTO_LIQUIDACION = "Monto de la Liquidación (valor positivo)";
    public static final String TEXTO_MONTO_INTERES = "Monto del Interés (valor positivo)";
    public static final String TEXTO_GASTO_ADMINISTRATIVO = "Gasto Administrativo (valor positivo)";
    public static final String TEXTO_MONTO_RETIRADO = "Monto Retirado (no editable)";
    public static final String TEXTO_TASA_INTERES_RETIRADO = "Tasa de Interés de retiro (valor positivo)";
    
    public entAhorroProgramado() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.sapellidoNombre = "";
        this.scontrato = "";
        this.iidEstadoAhorro = 0;
        this.sestadoAhorro = "";
        this.iidPlan = 0;
        this.splan = "";
        this.uimporte = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iplazo = 0;
        this.utasaInteres = 0.0;
        this.dfechaOperacion = null;
        this.iidPromotor = 0;
        this.spromotor = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.inumeroOperacion = 0;
        this.iidCuenta = 0;
        this.scuenta = "";
        this.dfechaPrimerVencimiento = null;
        this.dfechaVencimiento = null;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.dfechaRetirado = null;
        this.sobservacionRetirado = "";
        this.umontoAportado = 0.0;
        this.umontoLiquidacion = 0.0;
        this.umontoInteres = 0.0;
        this.ugastoAdministrativo = 0.0;
        this.umontoRetirado = 0.0;
        this.utasaInteresRetirado = 0.0;
        this.dfechaCumplido = null;
        this.sobservacionCumplido = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entAhorroProgramado(int iid, int iidSocio, String scedula, String sapellidoNombre, String scontrato, int iidEstadoAhorro, String sestadoAhorro, int iidPlan, String splan, double uimporte, int iidMoneda, String smoneda, int iplazo, double utasaInteres, java.util.Date dfechaOperacion, int iidPromotor, String spromotor, int iidRegional, String sregional, int inumeroOperacion, int iidCuenta, String scuenta, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRetirado, String sobservacionRetirado, double umontoAportado, double umontoLiquidacion, double umontoInteres, double ugastoAdministrativo, double umontoRetirado, double utasaInteresRetirado, java.util.Date dfechaCumplido, String sobservacionCumplido, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.scontrato = scontrato;
        this.iidEstadoAhorro = iidEstadoAhorro;
        this.sestadoAhorro = sestadoAhorro;
        this.iidPlan = iidPlan;
        this.splan = splan;
        this.uimporte = uimporte;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iplazo = iplazo;
        this.utasaInteres = utasaInteres;
        this.dfechaOperacion = dfechaOperacion;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.inumeroOperacion = inumeroOperacion;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRetirado = dfechaRetirado;
        this.sobservacionRetirado = sobservacionRetirado;
        this.umontoAportado = umontoAportado;
        this.umontoLiquidacion = umontoLiquidacion;
        this.umontoInteres = umontoInteres;
        this.ugastoAdministrativo = ugastoAdministrativo;
        this.umontoRetirado = umontoRetirado;
        this.utasaInteresRetirado = utasaInteresRetirado;
        this.dfechaCumplido = dfechaCumplido;
        this.sobservacionCumplido = sobservacionCumplido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, String sapellidoNombre, String scontrato, int iidEstadoAhorro, String sestadoAhorro, int iidPlan, String splan, double uimporte, int iidMoneda, String smoneda, int iplazo, double utasaInteres, java.util.Date dfechaOperacion, int iidPromotor, String spromotor, int iidRegional, String sregional, int inumeroOperacion, int iidCuenta, String scuenta, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRetirado, String sobservacionRetirado, double umontoAportado, double umontoLiquidacion, double umontoInteres, double ugastoAdministrativo, double umontoRetirado, double utasaInteresRetirado, java.util.Date dfechaCumplido, String sobservacionCumplido, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.scontrato = scontrato;
        this.iidEstadoAhorro = iidEstadoAhorro;
        this.sestadoAhorro = sestadoAhorro;
        this.iidPlan = iidPlan;
        this.splan = splan;
        this.uimporte = uimporte;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iplazo = iplazo;
        this.utasaInteres = utasaInteres;
        this.dfechaOperacion = dfechaOperacion;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.inumeroOperacion = inumeroOperacion;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRetirado = dfechaRetirado;
        this.sobservacionRetirado = sobservacionRetirado;
        this.umontoAportado = umontoAportado;
        this.umontoLiquidacion = umontoLiquidacion;
        this.umontoInteres = umontoInteres;
        this.ugastoAdministrativo = ugastoAdministrativo;
        this.umontoRetirado = umontoRetirado;
        this.utasaInteresRetirado = utasaInteresRetirado;
        this.dfechaCumplido = dfechaCumplido;
        this.sobservacionCumplido = sobservacionCumplido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entAhorroProgramado copiar(entAhorroProgramado destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getApellidoNombre(), this.getContrato(), this.getIdEstadoAhorro(), this.getEstadoAhorro(), this.getIdPlan(), this.getPlan(), this.getImporte(), this.getIdMoneda(), this.getMoneda(), this.getPlazo(), this.getTasaInteres(), this.getFechaOperacion(), this.getIdPromotor(), this.getPromotor(), this.getIdRegional(), this.getRegional(), this.getNumeroOperacion(), this.getIdCuenta(), this.getCuenta(), this.getFechaPrimerVencimiento(), this.getFechaVencimiento(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getFechaRetirado(), this.getObservacionRetirado(), this.getMontoAportado(), this.getMontoLiquidacion(), this.getMontoInteres(), this.getGastoAdministrativo(), this.getMontoRetirado(), this.getTasaInteresRetirado(), this.getFechaCumplido(), this.getObservacionCumplido(), this.getEstadoRegistro());
        return destino;
    }
    
    public entAhorroProgramado cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setApellidoNombre(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setContrato(rs.getString("contrato")); }
        catch(Exception e) {}
        try { this.setIdEstadoAhorro(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstadoAhorro(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setIdPlan(rs.getInt("idplan")); }
        catch(Exception e) {}
        try { this.setPlan(rs.getString("plan")); }
        catch(Exception e) {}
        try { this.setImporte(rs.getDouble("importe")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setIdPromotor(rs.getInt("idpromotor")); }
        catch(Exception e) {}
        try { this.setPromotor(rs.getString("promotor")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setFechaPrimerVencimiento(rs.getDate("fechaprimervencimiento")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setFechaRetirado(rs.getDate("fecharetirado")); }
        catch(Exception e) {}
        try { this.setObservacionRetirado(rs.getString("observacionretirado")); }
        catch(Exception e) {}
        try { this.setMontoAportado(rs.getDouble("montoaportado")); }
        catch(Exception e) {}
        try { this.setMontoLiquidacion(rs.getDouble("montoliquidacion")); }
        catch(Exception e) {}
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) {}
        try { this.setGastoAdministrativo(rs.getDouble("gastoadministrativo")); }
        catch(Exception e) {}
        try { this.setMontoRetirado(rs.getDouble("montoretirado")); }
        catch(Exception e) {}
        try { this.setTasaInteresRetirado(rs.getDouble("tasainteresretirado")); }
        catch(Exception e) {}
        try { this.setFechaCumplido(rs.getDate("fechacumplido")); }
        catch(Exception e) {}
        try { this.setObservacionCumplido(rs.getString("observacioncumplido")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setApellidoNombre(String sapellidoNombre) {
        this.sapellidoNombre = sapellidoNombre;
    }

    public void setContrato(String scontrato) {
        this.scontrato = scontrato;
    }

    public void setIdEstadoAhorro(int iidEstadoAhorro) {
        this.iidEstadoAhorro = iidEstadoAhorro;
    }

    public void setEstadoAhorro(String sestadoAhorro) {
        this.sestadoAhorro = sestadoAhorro;
    }

    public void setIdPlan(int iidPlan) {
        this.iidPlan = iidPlan;
    }

    public void setPlan(String splan) {
        this.splan = splan;
    }

    public void setImporte(double uimporte) {
        this.uimporte = uimporte;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setTasaInteres(double utasaInteres) {
        this.utasaInteres = utasaInteres;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }

    public void setPromotor(String spromotor) {
        this.spromotor = spromotor;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setFechaPrimerVencimiento(java.util.Date dfechaPrimerVencimiento) {
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setFechaRetirado(java.util.Date dfechaRetirado) {
        this.dfechaRetirado = dfechaRetirado;
    }

    public void setObservacionRetirado(String sobservacionRetirado) {
        this.sobservacionRetirado = sobservacionRetirado;
    }

    public void setMontoAportado(double umontoAportado) {
        this.umontoAportado = umontoAportado;
    }

    public void setMontoLiquidacion(double umontoLiquidacion) {
        this.umontoLiquidacion = umontoLiquidacion;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setGastoAdministrativo(double ugastoAdministrativo) {
        this.ugastoAdministrativo = ugastoAdministrativo;
    }

    public void setMontoRetirado(double umontoRetirado) {
        this.umontoRetirado = umontoRetirado;
    }

    public void setTasaInteresRetirado(double utasaInteresRetirado) {
        this.utasaInteresRetirado = utasaInteresRetirado;
    }

    public void setFechaCumplido(java.util.Date dfechaCumplido) {
        this.dfechaCumplido = dfechaCumplido;
    }

    public void setObservacionCumplido(String sobservacionCumplido) {
        this.sobservacionCumplido = sobservacionCumplido;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getApellidoNombre() {
        return this.sapellidoNombre;
    }

    public String getContrato() {
        return this.scontrato;
    }

    public int getIdEstadoAhorro() {
        return this.iidEstadoAhorro;
    }

    public String getEstadoAhorro() {
        return this.sestadoAhorro;
    }

    public int getIdPlan() {
        return this.iidPlan;
    }

    public String getPlan() {
        return this.splan;
    }

    public double getImporte() {
        return this.uimporte;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public double getTasaInteres() {
        return this.utasaInteres;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public String getPromotor() {
        return this.spromotor;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public java.util.Date getFechaPrimerVencimiento() {
        return this.dfechaPrimerVencimiento;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public java.util.Date getFechaRetirado() {
        return this.dfechaRetirado;
    }

    public String getObservacionRetirado() {
        return this.sobservacionRetirado;
    }

    public double getMontoAportado() {
        return this.umontoAportado;
    }

    public double getMontoLiquidacion() {
        return this.umontoLiquidacion;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public double getGastoAdministrativo() {
        return this.ugastoAdministrativo;
    }

    public double getMontoRetirado() {
        return this.umontoRetirado;
    }

    public double getTasaInteresRetirado() {
        return this.utasaInteresRetirado;
    }

    public java.util.Date getFechaCumplido() {
        return this.dfechaCumplido;
    }

    public String getObservacionCumplido() {
        return this.sobservacionCumplido;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar(double dimporteBase) {
        this.smensaje = "";
        if (this.getIdSocio()<=0) this.smensaje += TEXTO_SOCIO + "\n";
        if (this.getContrato().isEmpty()) this.smensaje += TEXTO_CONTRATO + "\n";
        if (this.getIdPlan()<=0) this.smensaje += TEXTO_PLAN + "\n";
        if (this.getImporte()<0) this.smensaje += TEXTO_IMPORTE + "\n";
        if (this.getPlazo()<=0) this.smensaje += TEXTO_PLAZO + "\n";
        if (this.getTasaInteres()<0) this.smensaje += TEXTO_TASA_INTERES + "\n";
        if (this.getFechaOperacion()==null) this.smensaje += TEXTO_FECHA_OPERACION + "\n";
        if (this.getIdPromotor()<=0) this.smensaje += TEXTO_PROMOTOR + "\n";
        if (this.getIdRegional()<=0) this.smensaje += TEXTO_REGIONAL + "\n";
        if (this.getFechaPrimerVencimiento()==null) this.smensaje += TEXTO_FECHA_PRIMER_VENCIMIENTO + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }

    public boolean esValidoAnular() {
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) this.smensaje += TEXTO_OBSERVACION_ANULADO + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public boolean esValidoRetirar() {
        this.smensaje = "";
        if (this.getFechaRetirado()==null) this.smensaje += TEXTO_FECHA_RETIRADO + "\n";
        if (this.getObservacionRetirado().isEmpty()) this.smensaje += TEXTO_OBSERVACION_RETIRADO + "\n";
        if (this.getMontoLiquidacion()<0) this.smensaje += TEXTO_MONTO_LIQUIDACION + "\n";
        if (this.getTasaInteresRetirado()<0) this.smensaje += TEXTO_TASA_INTERES_RETIRADO + "\n";
        if (this.getMontoInteres()<0) this.smensaje += TEXTO_MONTO_INTERES + "\n";
        if (this.getGastoAdministrativo()<0) this.smensaje += TEXTO_GASTO_ADMINISTRATIVO + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public String getSentencia() {
        return "SELECT ahorroprogramado("+
            this.getId()+","+
            this.getIdSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getContrato())+","+
            this.getIdEstadoAhorro()+","+
            this.getIdPlan()+","+
            this.getImporte()+","+
            this.getIdMoneda()+","+
            this.getPlazo()+","+
            this.getTasaInteres()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaOperacion())+","+
            this.getIdPromotor()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaPrimerVencimiento())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaVencimiento())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaRetirado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionRetirado())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaCumplido())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionCumplido())+","+
            this.getIdRegional()+","+
            this.getNumeroOperacion()+","+
            this.getIdCuenta()+","+
            this.getMontoAportado()+","+
            this.getMontoLiquidacion()+","+
            this.getMontoInteres()+","+
            this.getGastoAdministrativo()+","+
            this.getMontoRetirado()+","+
            this.getTasaInteresRetirado()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Apellido y Nombre", "apellidonombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Contrato", "contrato", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Número Operacion", "numerooperacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(5, "Plan", "plan", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Fecha Operación", "fechaoperacion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(7, "Fecha Primer Vencimiento", "fechaprimervencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(8, "Fecha Vencimiento", "fechavencimiento", generico.entLista.tipo_fecha));
        return lst;
    }

}
