/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbOrdenPago {

    private int iid;
    private int iidmovimiento;
    private int inumeroOrden;
    private int inumerosolicitud;
    private java.util.Date dfecha;
    private double dtotal;
    private int iidReceptorCheque;
    private String sreceptorCheque;
    private String scedulaRuc;
    private int iidEstado;
    private String sestado;
    private int iidCentroCosto;
    private String scentroCosto;
    private int iidBancoCuenta;
    private String snumerocuenta;
    private String sbancocuenta;
    private String snroPlanBancoCuenta;
    private String snroPlanBancoCuenta2;
    private String sdescPlanBancoCuenta;
    private int iidChequeEmitido;
    private int inumeroOperacion;
    private int inumeroCheque;
    private String sconcepto;
    private int iidRegional;
    private String sregional;
    private java.util.Date dfechaEmision;
    private boolean basiento;
    private int iidFilial;
    private String sfilial;
    private int iidTipoServicio;
    private String stipoServicio;
    private int iidRemision;
    private String sremision;
    private int iidPromotor;
    private String spromotor;
    private String snrodocumento;

    //Para anulacion de Orden de Pago
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;

    //Para confirmacion de Orden de Pago
    private java.util.Date dfechaConfirmado;
    private String sobservacionConfirmado;

    //Para intefaz de Emision de Cheques
    private boolean bconfirma;

    //General
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_CONCEPTO = 100;
    public static final int LONGITUD_OBSERVACION = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ESTADO = "Estado de Orden de pago (no editable)";
    public static final String TEXTO_TIPO_SERVICIO = "Tipo de Servicio Solicitado";
    public static final String TEXTO_NUMERO_SOLICITUD = "Número de Solicitud (De acuerdo al Tipo de Servicio)";
    public static final String TEXTO_FECHA = "Fecha de Orden de Pago (no vacía)";
    public static final String TEXTO_TOTAL = "Importe Total de la operación (no editable)";
    public static final String TEXTO_RECEPTOR = "Receptor (no vacía)";
    public static final String TEXTO_REMISION = "Representante para remisión del cheque";
    public static final String TEXTO_PROMOTOR = "Promotor asignado a la operación.";
    public static final String TEXTO_RECEPTOR_CARGADO = "Receptor del cheque según solicitud";
    public static final String TEXTO_CENTRO_COSTO = "Centro de Costo (no vacío)";
    public static final String TEXTO_BANCO_CUENTA = "Cuenta Bancaria para la Orden de Pago (no vacía)";
    public static final String TEXTO_NUMERO_ORDEN = "Número de la boleta de Orden de Pago (no vacía)";
    public static final String TEXTO_CONCEPTO = "Descripción de Orden de Pago (no vacía)";
    public static final String TEXTO_REGIONAL = "Regional para la que se emite la orden";
    public static final String TEXTO_ASIENTO = "Si fue asentado o no el depósito";
    public static final String TEXTO_FILIAL = "Filial (no vacía)";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public static final String TEXTO_FECHA_CONFIRMADO = "Fecha de Confirmación (no editable)";
    public static final String TEXTO_OBSERVACION_CONFIRMADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public entbOrdenPago() {
        this.iid = 0;
        this.iidReceptorCheque = 0;
        this.sreceptorCheque = "";
        this.scedulaRuc = "";
        this.iidmovimiento = 0;
        this.inumeroOrden = 0;
        this.inumerosolicitud = 0;
        this.dfecha = null;
        this.dtotal = 0.0;
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.iidChequeEmitido = 0;
        this.inumeroOperacion = 0;
        this.inumeroCheque = 0;
        this.iidEstado = 0;
        this.sestado = "";
        this.iidBancoCuenta = 0;
        this.snumerocuenta = "";
        this.sbancocuenta = "";
        this.snroPlanBancoCuenta = "";
        this.snroPlanBancoCuenta2 = "";
        this.sdescPlanBancoCuenta = "";
        this.sconcepto = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.bconfirma = false;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.dfechaConfirmado = null;
        this.dfechaEmision = null;
        this.sobservacionConfirmado = "";
        this.basiento = false;
        this.iidFilial = 0;
        this.sfilial = "";
        this.iidTipoServicio = 0;
        this.stipoServicio = "";
        this.iidRemision = 0;
        this.sremision = "";
        this.iidPromotor = 0;
        this.spromotor = "";
        this.snrodocumento = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbOrdenPago(int iid, int inumeroOrden, java.util.Date dfecha, double dtotal, int iidEstado, String sestado, int iidCentroCosto, String scentroCosto, int iidChequeEmitido, int iidBancoCuenta, String snumerocuenta, String sbancocuenta, String snroPlanBancoCuenta, String snroPlanBancoCuenta2, String sdescPlanBancoCuenta, int iidReceptorCheque, String sreceptorCheque, String scedulaRuc, int iidmovimiento, int inumerosolicitud, int inumeroOperacion, int inumeroCheque, String sconcepto, int iidRegional, String sregional, boolean bconfirma, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaConfirmado, java.util.Date dfechaEmision, String sobservacionConfirmado, boolean basiento, int iidFilial, String sfilial, int iidTipoServicio, String stipoServicio, int iidRemision, String sremision, int iidPromotor, String spromotor, String snrodocumento, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOrden = inumeroOrden;
        this.dfecha = dfecha;
        this.dtotal = dtotal;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidChequeEmitido = iidChequeEmitido;
        this.iidBancoCuenta = iidBancoCuenta;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.snroPlanBancoCuenta = snroPlanBancoCuenta;
        this.snroPlanBancoCuenta2 = snroPlanBancoCuenta2;
        this.sdescPlanBancoCuenta = sdescPlanBancoCuenta;
        this.iidReceptorCheque = iidReceptorCheque;
        this.sreceptorCheque = sreceptorCheque;
        this.scedulaRuc = scedulaRuc;
        this.iidmovimiento = iidmovimiento;
        this.inumerosolicitud = inumerosolicitud;
        this.inumeroOperacion = inumeroOperacion;
        this.inumeroCheque = inumeroCheque;
        this.sconcepto = sconcepto;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.bconfirma = bconfirma;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaConfirmado = dfechaConfirmado;
        this.dfechaEmision = dfechaEmision;
        this.sobservacionConfirmado = sobservacionConfirmado;
        this.basiento = basiento;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidTipoServicio = iidTipoServicio;
        this.stipoServicio = stipoServicio;
        this.iidRemision = iidRemision;
        this.sremision = sremision;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.snrodocumento = snrodocumento;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroOrden, java.util.Date dfecha, double dtotal, int iidEstado, String sestado, int iidCentroCosto, String scentroCosto, int iidChequeEmitido, int iidBancoCuenta, String snumerocuenta, String sbancocuenta, String snroPlanBancoCuenta, String snroPlanBancoCuenta2, String sdescPlanBancoCuenta, int iidReceptorCheque, String sreceptorCheque, String scedulaRuc, int iidmovimiento, int inumerosolicitud, int inumeroOperacion, int inumeroCheque, String sconcepto, int iidRegional, String sregional, boolean bconfirma, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaConfirmado, java.util.Date dfechaEmision, String sobservacionConfirmado, boolean basiento, int iidFilial, String sfilial, int iidTipoServicio, String stipoServicio, int iidRemision, String sremision, int iidPromotor, String spromotor, String snrodocumento, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOrden = inumeroOrden;
        this.dfecha = dfecha;
        this.dtotal = dtotal;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidChequeEmitido = iidChequeEmitido;
        this.iidBancoCuenta = iidBancoCuenta;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.snroPlanBancoCuenta = snroPlanBancoCuenta;
        this.snroPlanBancoCuenta2 = snroPlanBancoCuenta2;
        this.sdescPlanBancoCuenta = sdescPlanBancoCuenta;
        this.iidReceptorCheque = iidReceptorCheque;
        this.sreceptorCheque = sreceptorCheque;
        this.scedulaRuc = scedulaRuc;
        this.iidmovimiento = iidmovimiento;
        this.inumerosolicitud = inumerosolicitud;
        this.inumeroOperacion = inumeroOperacion;
        this.inumeroCheque = inumeroCheque;
        this.sconcepto = sconcepto;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.bconfirma = bconfirma;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaConfirmado = dfechaConfirmado;
        this.dfechaEmision = dfechaEmision;
        this.sobservacionConfirmado = sobservacionConfirmado;
        this.basiento = basiento;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidTipoServicio = iidTipoServicio;
        this.stipoServicio = stipoServicio;
        this.iidRemision = iidRemision;
        this.sremision = sremision;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.snrodocumento = snrodocumento;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entbOrdenPago copiar(entbOrdenPago destino) {
        destino.setEntidad(this.getId(), this.getNumeroOrden(), this.getFecha(), this.getTotal(), this.getIdEstado(), this.getEstado(), this.getIdCentroCosto(), this.getCentroCosto(), this.getIdChequeEmitido(), this.getIdBancoCuenta(), this.getNumeroCuenta(), this.getBancoCuenta(), this.getNroPlanBancoCuenta(), this.getNroPlanBancoCuenta2(), this.getDescPlanBancoCuenta(), this.getIdReceptor(), this.getReceptor(), this.getCedulaRuc(), this.getIdMovimiento(), this.getNumeroSolicitud(), this.getNumeroOperacion(), this.getNumeroCheque(), this.getConcepto(), this.getIdRegional(), this.getRegional(), this.getConfirma(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getFechaConfirmado(), this.getFechaEmision(), this.getObservacionConfirmado(), this.getAsiento(), this.getIdFilial(), this.getFilial(), this.getIdTipoServicio(), this.getTipoServicio(), this.getIdRemision(), this.getRemision(), this.getIdPromotor(), this.getPromotor(), this.getNrodocumento(), this.getEstadoRegistro());
        return destino;
    }

    public entbOrdenPago cargar(java.sql.ResultSet rs) {
        try {
            this.setId(rs.getInt("id"));
        } catch (Exception e) {
        }
        try {
            this.setNumeroOrden(rs.getInt("nroordenpago"));
        } catch (Exception e) {
        }
        try {
            this.setNrodocumento(rs.getString("nrodocumento"));
        } catch (Exception e) {
        }
        try {
            this.setFecha(rs.getDate("fecha"));
        } catch (Exception e) {
        }
        try {
            this.setTotal(rs.getDouble("total"));
        } catch (Exception e) {
        }
        try {
            this.setIdEstado(rs.getInt("idestado"));
        } catch (Exception e) {
        }
        try {
            this.setEstado(rs.getString("estado"));
        } catch (Exception e) {
        }
        try {
            this.setIdCentroCosto(rs.getInt("idcentrocosto"));
        } catch (Exception e) {
        }
        try {
            this.setCentroCosto(rs.getString("centrocosto"));
        } catch (Exception e) {
        }
        try {
            this.setIdChequeEmitido(rs.getInt("idchequeemitido"));
        } catch (Exception e) {
        }
        try {
            this.setIdBancoCuenta(rs.getInt("idbancocuenta"));
        } catch (Exception e) {
        }
        try {
            this.setNumeroCuenta(rs.getString("numerocuenta"));
        } catch (Exception e) {
        }
        try {
            this.setBancoCuenta(rs.getString("cuentabanco"));
        } catch (Exception e) {
        }
        try {
            this.setNroPlanBancoCuenta(rs.getString("nroplancuentacheque"));
        } catch (Exception e) {
        }
        try {
            this.setNroPlanBancoCuenta2(rs.getString("nroplancuentacheque2"));
        } catch (Exception e) {
        }
        try {
            this.setDescPlanBancoCuenta(rs.getString("plancuentacheque"));
        } catch (Exception e) {
        }
        try {
            this.setIdReceptor(rs.getInt("idreceptorcheque"));
        } catch (Exception e) {
        }
        try {
            this.setReceptor(rs.getString("receptor"));
        } catch (Exception e) {
        }
        try {
            this.setCedulaRuc(rs.getString("cedularuc"));
        } catch (Exception e) {
        }
        try {
            this.setIdMovimiento(rs.getInt("idmovimiento"));
        } catch (Exception e) {
        }
        try {
            this.setNumeroSolicitud(rs.getInt("nrosolicitud"));
        } catch (Exception e) {
        }
        try {
            this.setNumeroOperacion(rs.getInt("numerooperacion"));
        } catch (Exception e) {
        }
        try {
            this.setNumeroCheque(rs.getInt("numerocheque"));
        } catch (Exception e) {
        }
        try {
            this.setConcepto(rs.getString("descripcion"));
        } catch (Exception e) {
        }
        try {
            this.setIdRegional(rs.getInt("idregional"));
        } catch (Exception e) {
        }
        try {
            this.setRegional(rs.getString("regional"));
        } catch (Exception e) {
        }
        try {
            this.setFechaAnulado(rs.getDate("fechaanulado"));
        } catch (Exception e) {
        }
        try {
            this.setObservacionAnulado(rs.getString("observacionanulado"));
        } catch (Exception e) {
        }
        try {
            this.setFechaConfirmado(rs.getDate("fechaconfirmado"));
        } catch (Exception e) {
        }
        try {
            this.setFechaEmision(rs.getDate("fechaemisioncheque"));
        } catch (Exception e) {
        }
        try {
            this.setObservacionConfirmado(rs.getString("observacionconfirmado"));
        } catch (Exception e) {
        }
        try {
            this.setAsiento(rs.getBoolean("asiento"));
        } catch (Exception e) {
        }
        try {
            this.setIdFilial(rs.getInt("idfilial"));
        } catch (Exception e) {
        }
        try {
            this.setFilial(rs.getString("filial"));
        } catch (Exception e) {
        }
        try {
            this.setIdTipoServicio(rs.getInt("idtiposervicio"));
        } catch (Exception e) {
        }
        try {
            this.setTipoServicio(rs.getString("tiposervicio"));
        } catch (Exception e) {
        }
        try {
            this.setIdRemision(rs.getInt("idremision"));
        } catch (Exception e) {
        }
        try {
            this.setRemision(rs.getString("remision"));
        } catch (Exception e) {
        }
        try {
            this.setIdPromotor(rs.getInt("idpromotor"));
        } catch (Exception e) {
        }
        try {
            this.setPromotor(rs.getString("promotor"));
        } catch (Exception e) {
        }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroOrden(int inumeroOrden) {
        this.inumeroOrden = inumeroOrden;
    }

    public void setTotal(double dtotal) {
        this.dtotal = dtotal;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setIdMovimiento(int iidmovimiento) {
        this.iidmovimiento = iidmovimiento;
    }

    public void setNumeroSolicitud(int inumerosolicitud) {
        this.inumerosolicitud = inumerosolicitud;
    }

    public void setIdChequeEmitido(int iidChequeEmitido) {
        this.iidChequeEmitido = iidChequeEmitido;
    }

    public void setConfirma(boolean bconfirma) {
        this.bconfirma = bconfirma;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }

    public void setIdBancoCuenta(int iidBancoCuenta) {
        this.iidBancoCuenta = iidBancoCuenta;
    }

    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }

    public void setNumeroCuenta(String snumerocuenta) {
        this.snumerocuenta = snumerocuenta;
    }

    public void setBancoCuenta(String sbancocuenta) {
        this.sbancocuenta = sbancocuenta;
    }

    public void setNroPlanBancoCuenta(String snroPlanBancoCuenta) {
        this.snroPlanBancoCuenta = snroPlanBancoCuenta;
    }

    public void setNroPlanBancoCuenta2(String snroPlanBancoCuenta2) {
        this.snroPlanBancoCuenta2 = snroPlanBancoCuenta2;
    }

    public void setDescPlanBancoCuenta(String sdescPlanBancoCuenta) {
        this.sdescPlanBancoCuenta = sdescPlanBancoCuenta;
    }

    public void setIdReceptor(int iidReceptorCheque) {
        this.iidReceptorCheque = iidReceptorCheque;
    }

    public void setReceptor(String sreceptorCheque) {
        this.sreceptorCheque = sreceptorCheque;
    }

    public void setCedulaRuc(String scedulaRuc) {
        this.scedulaRuc = scedulaRuc;
    }

    public void setConcepto(String sconcepto) {
        this.sconcepto = sconcepto;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setNumeroCheque(int inumeroCheque) {
        this.inumeroCheque = inumeroCheque;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setFechaConfirmado(java.util.Date dfechaConfirmado) {
        this.dfechaConfirmado = dfechaConfirmado;
    }

    public void setFechaEmision(java.util.Date dfechaEmision) {
        this.dfechaEmision = dfechaEmision;
    }

    public void setObservacionConfirmado(String sobservacionConfirmado) {
        this.sobservacionConfirmado = sobservacionConfirmado;
    }

    public void setAsiento(boolean basiento) {
        this.basiento = basiento;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }

    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setIdTipoServicio(int iidTipoServicio) {
        this.iidTipoServicio = iidTipoServicio;
    }

    public void setTipoServicio(String stipoServicio) {
        this.stipoServicio = stipoServicio;
    }

    public void setNrodocumento(String snrodocumento) {
        this.snrodocumento = snrodocumento;
    }

    public void setIdRemision(int iidRemision) {
        this.iidRemision = iidRemision;
    }

    public void setRemision(String sremision) {
        this.sremision = sremision;
    }

    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }

    public void setPromotor(String spromotor) {
        this.spromotor = spromotor;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getNumeroOrden() {
        return this.inumeroOrden;
    }

    public int getIdMovimiento() {
        return this.iidmovimiento;
    }

    public int getNumeroSolicitud() {
        return this.inumerosolicitud;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public double getTotal() {
        return this.dtotal;
    }

    public int getIdChequeEmitido() {
        return this.iidChequeEmitido;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public String getNrodocumento() {
        return this.snrodocumento;
    }

    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }

    public String getCentroCosto() {
        return this.scentroCosto;
    }

    public int getIdBancoCuenta() {
        return this.iidBancoCuenta;
    }

    public String getNumeroCuenta() {
        return this.snumerocuenta;
    }

    public String getBancoCuenta() {
        return this.sbancocuenta;
    }

    public String getNroPlanBancoCuenta() {
        return this.snroPlanBancoCuenta;
    }

    public String getNroPlanBancoCuenta2() {
        return this.snroPlanBancoCuenta2;
    }

    public String getDescPlanBancoCuenta() {
        return this.sdescPlanBancoCuenta;
    }

    public int getIdReceptor() {
        return this.iidReceptorCheque;
    }

    public String getReceptor() {
        return this.sreceptorCheque;
    }

    public String getCedulaRuc() {
        return this.scedulaRuc;
    }

    public String getConcepto() {
        return this.sconcepto;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getNumeroCheque() {
        return this.inumeroCheque;
    }

    public boolean getConfirma() {
        return this.bconfirma;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public java.util.Date getFechaConfirmado() {
        return this.dfechaConfirmado;
    }

    public java.util.Date getFechaEmision() {
        return this.dfechaEmision;
    }

    public String getObservacionConfirmado() {
        return this.sobservacionConfirmado;
    }

    public boolean getAsiento() {
        return this.basiento;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }

    public String getFilial() {
        return this.sfilial;
    }

    public int getIdTipoServicio() {
        return this.iidTipoServicio;
    }

    public String getTipoServicio() {
        return this.stipoServicio;
    }

    public int getIdRemision() {
        return this.iidRemision;
    }

    public String getRemision() {
        return this.sremision;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public String getPromotor() {
        return this.spromotor;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoSolicitar() {
        boolean bvalido = true;
        this.smensaje = "";
//        if (this.getIdCentroCosto()<=0) {
//            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
//            this.smensaje += this.TEXTO_CENTRO_COSTO;
//            bvalido = false;
//        }
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getIdBancoCuenta() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_BANCO_CUENTA;
            bvalido = false;
        }
        if (this.getTotal() < 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_TOTAL;
            bvalido = false;
        }
        if (this.getIdReceptor() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_RECEPTOR;
            bvalido = false;
        }
        if (this.getIdMovimiento() < 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_NUMERO_SOLICITUD;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT banco.ordenpago("
                + this.getId() + ","
                + utilitario.utiFecha.getFechaGuardadoMac(this.getFecha()) + ","
                + this.getTotal() + ","
                + this.getIdEstado() + ","
                + this.getIdCentroCosto() + ","
                + this.getIdChequeEmitido() + ","
                + this.getIdBancoCuenta() + ","
                + this.getIdReceptor() + ","
                + this.getIdMovimiento() + ","
                + this.getNumeroSolicitud() + ","
                + utilitario.utiCadena.getTextoGuardado(this.getConcepto()) + ","
                + utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado()) + ","
                + utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado()) + ","
                + this.getIdRegional() + ","
                + utilitario.utiFecha.getFechaGuardadoMac(this.getFechaConfirmado()) + ","
                + utilitario.utiCadena.getTextoGuardado(this.getObservacionConfirmado()) + ","
                + utilitario.utiFecha.getFechaGuardadoMac(this.getFechaEmision()) + ","
                + this.getAsiento() + ","
                + this.getIdFilial() + ","
                + this.getIdTipoServicio() + ","
                + this.getIdRemision() + ","
                + this.getNumeroOrden() + ","
                + this.getIdPromotor() + ","
                + utilitario.utiCadena.getTextoGuardado(this.getNrodocumento()) + ","
                + this.getEstadoRegistro() + ")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
//        lst.add(new generico.entLista(2, "Número de Orden", "nroordenpago", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(1, "Nro", "id", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Fecha Orden", "fecha", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Número Factura", "nrosolicitud", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "Cédula o RUC Receptor", "cedularuc", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Receptor", "receptor", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Estado Orden", "estado", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(8, "Número de cuenta", "numerocuenta", new generico.entLista().tipo_texto));
//        lst.add(new generico.entLista(9, "Centro de Costo", "centrocosto", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(9, "Tipo de Servicio", "tiposervicio", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(10, "Filial", "filial", new generico.entLista().tipo_texto));
        return lst;
    }

}
