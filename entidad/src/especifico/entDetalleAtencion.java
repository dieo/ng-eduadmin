/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleAtencion {

    private int iid;
    private int iidAtencion;
    private int iidTipoAtencion;
    private String stipoAtencion;
    private int iidMotivoAtencion;
    private String smotivoAtencion;
    private java.util.Date dfechaAtencion;
    private String shoraAtencion;
    private String sobservacion;
    private int iduracion;
    private String sregional;
    private String stelefonoSocio;
    private int iidTelefonoOrigen;
    private String stelefonoOrigen;
    private String stelefonoDestino;
    private int iidDependencia;
    private String sdependencia;
    private int iidIntermediario;
    private String sintermediario;
    private int iidEstadoGestion;
    private String sestadoGestion;
    private int iidUsuario;
    private String susuarioNombreApellido;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_TELEFONO_DESTINO = 12;
    public static final int LONGITUD_OBSERVACION = 1000;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_TIPO_ATENCION = "Tipo de la Atención (no vacío)";
    public static final String TEXTO_MOTIVO_ATENCION = "Motivo de la Atención (no vacío)";
    public static final String TEXTO_FECHA_ATENCION = "Fecha de la Atención (no vacía)";
    public static final String TEXTO_HORA_ATENCION = "Hora de la Atención (no vacía)";
    public static final String TEXTO_OBSERVACION = "Observación (no vacía)";
    public static final String TEXTO_DURACION = "Duración de la Atención en minutos (no vacía, valor positivo hasta 60)";
    public static final String TEXTO_REGIONAL = "Regional del socio (no editable)";
    public static final String TEXTO_TELEFONO_SOCIO = "Teléfonos del socio (no editable)";
    public static final String TEXTO_TELEFONO_ORIGEN = "Teléfono Origen (no vacío)";
    public static final String TEXTO_TELEFONO_DESTINO = "Teléfono Destino (no vacío, hasta " + LONGITUD_TELEFONO_DESTINO + " caracteres)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia (no editable)";
    public static final String TEXTO_INTERMEDIARIO = "Intermediario (no vacío)";
    public static final String TEXTO_ESTADO_GESTION = "Estado de Gestión (no vacío)";
    public static final String TEXTO_USUARIO = "Usuario (no editable)";

    public entDetalleAtencion() {
        this.iid = 0;
        this.iidAtencion = 0;
        this.iidTipoAtencion = 0;
        this.stipoAtencion = "";
        this.iidMotivoAtencion = 0;
        this.smotivoAtencion = "";
        this.dfechaAtencion = null;
        this.shoraAtencion = "";
        this.sobservacion = "";
        this.iduracion = 0;
        this.sregional = "";
        this.stelefonoSocio = "";
        this.iidTelefonoOrigen = 0;
        this.stelefonoOrigen = "";
        this.stelefonoDestino = "";
        this.iidDependencia = 0;
        this.sdependencia = "";
        this.iidIntermediario = 0;
        this.sintermediario = "";
        this.iidEstadoGestion = 0;
        this.sestadoGestion = "";
        this.iidUsuario = 0;
        this.susuarioNombreApellido = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetalleAtencion(int iid, int iidAtencion, int iidTipoAtencion, String stipoAtencion, int iidMotivoAtencion, String smotivoAtencion, java.util.Date dfechaAtencion, String shoraAtencion, String sobservacion, int iduracion, String sregional, String stelefonoSocio, int iidTelefonoOrigen, String stelefonoOrigen, String stelefonoDestino, int iidDependencia, String sdependencia, int iidIntermediario, String sintermediario, int iidEstadoGestion, String sestadoGestion, int iidUsuario, String susuarioNombreApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidAtencion = iidAtencion;
        this.iidTipoAtencion = iidTipoAtencion;
        this.stipoAtencion = stipoAtencion;
        this.iidMotivoAtencion = iidMotivoAtencion;
        this.smotivoAtencion = smotivoAtencion;
        this.dfechaAtencion = dfechaAtencion;
        this.shoraAtencion = shoraAtencion;
        this.sobservacion = sobservacion;
        this.iduracion = iduracion;
        this.sregional = sregional;
        this.stelefonoSocio = stelefonoSocio;
        this.iidTelefonoOrigen = iidTelefonoOrigen;
        this.stelefonoOrigen = stelefonoOrigen;
        this.stelefonoDestino = stelefonoDestino;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidIntermediario = iidIntermediario;
        this.sintermediario = sintermediario;
        this.iidEstadoGestion = iidEstadoGestion;
        this.sestadoGestion = sestadoGestion;
        this.iidUsuario = iidUsuario;
        this.susuarioNombreApellido = susuarioNombreApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidAtencion, int iidTipoAtencion, String stipoAtencion, int iidMotivoAtencion, String smotivoAtencion, java.util.Date dfechaAtencion, String shoraAtencion, String sobservacion, int iduracion, String sregional, String stelefonoSocio, int iidTelefonoOrigen, String stelefonoOrigen, String stelefonoDestino, int iidDependencia, String sdependencia, int iidIntermediario, String sintermediario, int iidEstadoGestion, String sestadoGestion, int iidUsuario, String susuarioNombreApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidAtencion = iidAtencion;
        this.iidTipoAtencion = iidTipoAtencion;
        this.stipoAtencion = stipoAtencion;
        this.iidMotivoAtencion = iidMotivoAtencion;
        this.smotivoAtencion = smotivoAtencion;
        this.dfechaAtencion = dfechaAtencion;
        this.shoraAtencion = shoraAtencion;
        this.sobservacion = sobservacion;
        this.iduracion = iduracion;
        this.sregional = sregional;
        this.stelefonoSocio = stelefonoSocio;
        this.iidTelefonoOrigen = iidTelefonoOrigen;
        this.stelefonoOrigen = stelefonoOrigen;
        this.stelefonoDestino = stelefonoDestino;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidIntermediario = iidIntermediario;
        this.sintermediario = sintermediario;
        this.iidEstadoGestion = iidEstadoGestion;
        this.sestadoGestion = sestadoGestion;
        this.iidUsuario = iidUsuario;
        this.susuarioNombreApellido = susuarioNombreApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleAtencion copiar(entDetalleAtencion destino) {
        destino.setEntidad(this.getId(), this.getIdAtencion(), this.getIdTipoAtencion(), this.getTipoAtencion(), this.getIdMotivoAtencion(), this.getMotivoAtencion(), this.getFechaAtencion(), this.getHoraAtencion(), this.getObservacion(), this.getDuracion(), this.getRegional(), this.getTelefonoSocio(), this.getIdTelefonoOrigen(), this.getTelefonoOrigen(), this.getTelefonoDestino(), this.getIdDependencia(), this.getDependencia(), this.getIdIntermediario(), this.getIntermediario(), this.getIdEstadoGestion(), this.getEstadoGestion(), this.getIdUsuario(), this.getUsuarioNombreApellido(), this.getEstadoRegistro());
        return destino;
    }
    
    public entDetalleAtencion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdAtencion(rs.getInt("idatencion")); }
        catch(Exception e) {}
        try { this.setIdTipoAtencion(rs.getInt("idtipoatencion")); }
        catch(Exception e) {}
        try { this.setTipoAtencion(rs.getString("tipoatencion")); }
        catch(Exception e) {}
        try { this.setIdMotivoAtencion(rs.getInt("idmotivoatencion")); }
        catch(Exception e) {}
        try { this.setMotivoAtencion(rs.getString("motivoatencion")); }
        catch(Exception e) {}
        try { this.setFechaAtencion(rs.getDate("fechaatencion")); }
        catch(Exception e) {}
        try { this.setHoraAtencion(rs.getString("horaatencion")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setDuracion(rs.getInt("duracion")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setTelefonoSocio(rs.getString("telefonosocio")); }
        catch(Exception e) {}
        try { this.setIdTelefonoOrigen(rs.getInt("idtelefonoorigen")); }
        catch(Exception e) {}
        try { this.setTelefonoOrigen(rs.getString("telefonoorigen")); }
        catch(Exception e) {}
        try { this.setTelefonoDestino(rs.getString("telefonodestino")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        try { this.setIdIntermediario(rs.getInt("idintermediario")); }
        catch(Exception e) {}
        try { this.setIntermediario(rs.getString("intermediario")); }
        catch(Exception e) {}
        try { this.setIdEstadoGestion(rs.getInt("idestadogestion")); }
        catch(Exception e) {}
        try { this.setEstadoGestion(rs.getString("estadogestion")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuarioNombreApellido(rs.getString("usuarionombre")+" "+rs.getString("usuarioapellido")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdAtencion(int iidAtencion) {
        this.iidAtencion = iidAtencion;
    }

    public void setIdTipoAtencion(int iidTipoAtencion) {
        this.iidTipoAtencion = iidTipoAtencion;
    }

    public void setTipoAtencion(String stipoAtencion) {
        this.stipoAtencion = stipoAtencion;
    }

    public void setIdMotivoAtencion(int iidMotivoAtencion) {
        this.iidMotivoAtencion = iidMotivoAtencion;
    }

    public void setMotivoAtencion(String smotivoAtencion) {
        this.smotivoAtencion = smotivoAtencion;
    }

    public void setFechaAtencion(java.util.Date dfechaAtencion) {
        this.dfechaAtencion = dfechaAtencion;
    }

    public void setHoraAtencion(String shoraAtencion) {
        this.shoraAtencion = shoraAtencion;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setDuracion(int iduracion) {
        this.iduracion = iduracion;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setTelefonoSocio(String stelefonoSocio) {
        this.stelefonoSocio = stelefonoSocio;
    }

    public void setIdTelefonoOrigen(int iidTelefonoOrigen) {
        this.iidTelefonoOrigen = iidTelefonoOrigen;
    }

    public void setTelefonoOrigen(String stelefonoOrigen) {
        this.stelefonoOrigen = stelefonoOrigen;
    }

    public void setTelefonoDestino(String stelefonoDestino) {
        this.stelefonoDestino = stelefonoDestino;
    }

    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }

    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public void setIdIntermediario(int iidIntermediario) {
        this.iidIntermediario = iidIntermediario;
    }

    public void setIntermediario(String sintermediario) {
        this.sintermediario = sintermediario;
    }

    public void setIdEstadoGestion(int iidEstadoGestion) {
        this.iidEstadoGestion = iidEstadoGestion;
    }

    public void setEstadoGestion(String sestadoGestion) {
        this.sestadoGestion = sestadoGestion;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }

    public void setUsuarioNombreApellido(String susuarioNombreApellido) {
        this.susuarioNombreApellido = susuarioNombreApellido;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdAtencion() {
        return this.iidAtencion;
    }

    public int getIdTipoAtencion() {
        return this.iidTipoAtencion;
    }

    public String getTipoAtencion() {
        return this.stipoAtencion;
    }

    public int getIdMotivoAtencion() {
        return this.iidMotivoAtencion;
    }

    public String getMotivoAtencion() {
        return this.smotivoAtencion;
    }

    public java.util.Date getFechaAtencion() {
        return this.dfechaAtencion;
    }

    public String getHoraAtencion() {
        return this.shoraAtencion;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public int getDuracion() {
        return this.iduracion;
    }

    public String getRegional() {
        return this.sregional;
    }

    public String getTelefonoSocio() {
        return this.stelefonoSocio;
    }

    public int getIdTelefonoOrigen() {
        return this.iidTelefonoOrigen;
    }

    public String getTelefonoOrigen() {
        return this.stelefonoOrigen;
    }

    public String getTelefonoDestino() {
        return this.stelefonoDestino;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }

    public String getDependencia() {
        return this.sdependencia;
    }

    public int getIdIntermediario() {
        return this.iidIntermediario;
    }

    public String getIntermediario() {
        return this.sintermediario;
    }

    public int getIdEstadoGestion() {
        return this.iidEstadoGestion;
    }

    public String getEstadoGestion() {
        return this.sestadoGestion;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }

    public String getUsuarioNombreApellido() {
        return this.susuarioNombreApellido;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdUsuario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_USUARIO;
            bvalido = false;
        }
        if (this.getIdDependencia()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DEPENDENCIA;
            bvalido = false;
        }
        if (this.getIdIntermediario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_INTERMEDIARIO;
            bvalido = false;
        }
        if (this.getIdTipoAtencion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_ATENCION;
            bvalido = false;
        }
        if (this.getIdTelefonoOrigen()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO_ORIGEN;
            bvalido = false;
        }
        if (this.getTelefonoDestino().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO_DESTINO;
            bvalido = false;
        }
        if (this.getIdMotivoAtencion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MOTIVO_ATENCION;
            bvalido = false;
        }
        if (this.getFechaAtencion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_ATENCION;
            bvalido = false;
        }
        if (this.getHoraAtencion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_ATENCION;
            bvalido = false;
        }
        if (this.getDuracion()<0 || this.getDuracion()>60) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DURACION;
            bvalido = false;
        }
        if (this.getIdEstadoGestion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ESTADO_GESTION;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT detalleatencion("+
            this.getId()+","+
            this.getIdAtencion()+","+
            this.getIdTipoAtencion()+","+
            this.getIdMotivoAtencion()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAtencion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraAtencion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getDuracion()+","+
            this.getIdTelefonoOrigen()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoDestino())+","+
            this.getIdDependencia()+","+
            this.getIdIntermediario()+","+
            this.getIdUsuario()+","+
            this.getIdEstadoGestion()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
