/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entLista;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entCDA {

    private int iid;
    private int inumeroCDA;
    private String sserie;
    private String scedula;
    private String snombre;
    private int iidTipoCDA;
    private String stipoCDA;
    private java.util.Date dfechaInicio;
    private java.util.Date dfechaIngreso;
    private java.util.Date dfechaFin;
    private int idias;
    private double dcapitalinicial;
    private double dtasaInteres;
    private double dtotalInteres;
    private int iidEstadoCDA;
    private String sestadoCDA;
    private short hidTipoCapitalizacion;
    private String stipoCapitalizacion;
    private String sobservacion;
    
    private java.util.Date dfechaPrimerVencimiento;
    private java.util.Date dfechaVencimiento;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    private java.util.Date dfechaRetirado;
    private String sobservacionRetirado;
    
    private java.util.Date dfechaCumplido;
    private String sobservacionCumplido;
    
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_OBSERVACION = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMERO = "Nº de CDA";
    public static final String TEXTO_SERIE = "Serie de Ahorro";
    public static final String TEXTO_TIPOCDA = "Tipo de CDA (no vacío)";
    public static final String TEXTO_FECHA_INICIO = "Fecha de Apertura (no vacía)";
    public static final String TEXTO_FECHA_INGRESO = "Fecha de Carga del CDA (feche actual, no editable)";
    public static final String TEXTO_FECHA_FIN = "Fecha de Finalización del contrato (no vacía)";
    public static final String TEXTO_DIAS = "Plazo del ahorro en días (no vacío)";
    public static final String TEXTO_CAPITAL_INICIAL = "Capital Inicial del CDA (mayor a 0)";
    public static final String TEXTO_TASA_INTERES = "Tasa de Interés (no editable)";
    public static final String TEXTO_TOTAL_INTERES = "Total de Interés a cobrar (cálculo, no editable)";
    public static final String TEXTO_ESTADO = "Estado de la Operación (no vacío)";
    public static final String TEXTO_OBSERVACION = "Observación, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_TIPO_CAPITALIZACION = "Si los intereses se capitalizan mensualmente o al finalizar el CDA";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_RETIRADO = "Fecha de Retiro (no editable)";
    public static final String TEXTO_OBSERVACION_RETIRADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_CUMPLIDO = "Fecha de Cumplimiento (no editable)";
    public static final String TEXTO_OBSERVACION_CUMPLIDO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    
    public entCDA() {
        this.iid = 0;
        this.inumeroCDA = 0;
        this.scedula = "";
        this.snombre = "";
        this.sserie = "";
        this.iidTipoCDA = 0;
        this.stipoCDA = "";
        this.dfechaInicio = null;
        this.dfechaIngreso = null;
        this.dfechaFin = null;
        this.idias = 0;
        this.dcapitalinicial = 0.0;
        this.dtasaInteres = 0.0;
        this.dtotalInteres = 0.0;
        this.iidEstadoCDA = 0;
        this.sestadoCDA = "";
        this.hidTipoCapitalizacion = (short)0;
        this.stipoCapitalizacion = "";
        this.sobservacion = "";
        this.dfechaPrimerVencimiento = null;
        this.dfechaVencimiento = null;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.dfechaRetirado = null;
        this.sobservacionRetirado = "";
        this.dfechaCumplido = null;
        this.sobservacionCumplido = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entCDA(int iid, int inumeroCDA, String sserie, String scedula, String snombre, int iidTipoCDA, String stipoCDA, java.util.Date dfechaInicio, java.util.Date dfechaIngreso, java.util.Date dfechaFin, int idias, double dcapitalinicial, double dtasaInteres, double dtotalInteres, int iidEstadoCDA, String sestadoCDA, short hidTipoCapitalizacion, String stipoCapitalizacion, String sobservacion, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRetirado, String sobservacionRetirado, java.util.Date dfechaCumplido, String sobservacionCumplido, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroCDA = inumeroCDA;
        this.sserie = sserie;
        this.scedula = scedula;
        this.snombre = snombre;
        this.iidTipoCDA = iidTipoCDA;
        this.stipoCDA = stipoCDA;
        this.dfechaInicio = dfechaInicio;
        this.dfechaIngreso = dfechaIngreso;
        this.dfechaFin = dfechaFin;
        this.idias = idias;
        this.dcapitalinicial = dcapitalinicial;
        this.dtasaInteres = dtasaInteres;
        this.dtotalInteres = dtotalInteres;
        this.iidEstadoCDA = iidEstadoCDA;
        this.sestadoCDA = sestadoCDA;
        this.hidTipoCapitalizacion = hidTipoCapitalizacion;
        this.stipoCapitalizacion = stipoCapitalizacion;
        this.sobservacion = sobservacion;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRetirado = dfechaRetirado;
        this.sobservacionRetirado = sobservacionRetirado;
        this.dfechaCumplido = dfechaCumplido;
        this.sobservacionCumplido = sobservacionCumplido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroCDA, String sserie, String scedula, String snombre, int iidTipoCDA, String stipoCDA, java.util.Date dfechaInicio, java.util.Date dfechaIngreso, java.util.Date dfechaFin, int idias, double dcapitalinicial, double dtasaInteres, double dtotalInteres, int iidEstadoCDA, String sestadoCDA, short hidTipoCapitalizacion, String stipoCapitalizacion, String sobservacion, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRetirado, String sobservacionRetirado, java.util.Date dfechaCumplido, String sobservacionCumplido, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroCDA = inumeroCDA;
        this.sserie = sserie;
        this.scedula = scedula;
        this.snombre = snombre;
        this.iidTipoCDA = iidTipoCDA;
        this.stipoCDA = stipoCDA;
        this.dfechaInicio = dfechaInicio;
        this.dfechaIngreso = dfechaIngreso;
        this.dfechaFin = dfechaFin;
        this.idias = idias;
        this.dcapitalinicial = dcapitalinicial;
        this.dtasaInteres = dtasaInteres;
        this.dtotalInteres = dtotalInteres;
        this.iidEstadoCDA = iidEstadoCDA;
        this.sestadoCDA = sestadoCDA;
        this.hidTipoCapitalizacion = hidTipoCapitalizacion;
        this.stipoCapitalizacion = stipoCapitalizacion;
        this.sobservacion = sobservacion;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRetirado = dfechaRetirado;
        this.sobservacionRetirado = sobservacionRetirado;
        this.dfechaCumplido = dfechaCumplido;
        this.sobservacionCumplido = sobservacionCumplido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entCDA copiar(entCDA destino) {
        destino.setEntidad(this.getId(), this.getNumeroCDA(), this.getSerie(), this.getCedula(), this.getNombre(), this.getIdTipoCDA(), this.getTipoCDA(), this.getFechaInicio(), this.getFechaIngreso(),this.getFechaFin(), this.getDias(), this.getCapitalInicial(), this.getTasaInteres(), this.getTotalInteres(), this.getIdEstadoCDA(), this.getEstadoCDA(), this.hidTipoCapitalizacion, this.stipoCapitalizacion, this.getObservacion(), this.getFechaPrimerVencimiento(), this.getFechaVencimiento(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getFechaRetirado(), this.getObservacionRetirado(), this.getFechaCumplido(), this.getObservacionCumplido(), this.getEstadoRegistro());
        return destino;
    }
    
    public entCDA cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(java.sql.SQLException e) {}
        try { this.setNumeroCDA(rs.getInt("numerocda")); }
        catch(java.sql.SQLException e) {}
        try { this.setSerie(rs.getString("serie")); }
        catch(java.sql.SQLException e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(java.sql.SQLException e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(java.sql.SQLException e) {}
        try { this.setIdTipoCDA(rs.getInt("idtipocda")); }
        catch(java.sql.SQLException e) {}
        try { this.setTipoCDA(rs.getString("tipocda")); }
        catch(java.sql.SQLException e) {}
        try { this.setFechaInicio(rs.getDate("fechainicio")); }
        catch(java.sql.SQLException e) {}
        try { this.setFechaIngreso(rs.getDate("fechaingreso")); }
        catch(java.sql.SQLException e) {}
        try { this.setFechaFin(rs.getDate("fechafin")); }
        catch(java.sql.SQLException e) {}
        try { this.setDias(rs.getInt("dias")); }
        catch(java.sql.SQLException e) {}
        try { this.setCapitalInicial(rs.getDouble("capitalinicial")); }
        catch(java.sql.SQLException e) {}
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(java.sql.SQLException e) {}
        try { this.setTotalInteres(rs.getDouble("total")); }
        catch(java.sql.SQLException e) {}
        try { this.setIdEstadoCDA(rs.getInt("idestadocda")); }
        catch(java.sql.SQLException e) {}
        try { this.setEstadoCDA(rs.getString("estadocda")); }
        catch(java.sql.SQLException e) {}
        try { this.setIdTipoCapitalizacion(rs.getShort("idtipocapitalizacion")); }
        catch(java.sql.SQLException e) {}
        try { this.setTipoCapitalizacion(rs.getString("tipocapitalizacion")); }
        catch(java.sql.SQLException e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(java.sql.SQLException e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setNumeroCDA(int inumeroCDA) {
        this.inumeroCDA = inumeroCDA;
    }

    public void setSerie(String sserie) {
        this.sserie = sserie;
    }
        
    public void setNombre(String snombre) {
        this.snombre = snombre;
    }
        
    public void setCedula(String scedula) {
        this.scedula = scedula;
    }
        
    public void setIdTipoCDA(int iidTipoCDA) {
        this.iidTipoCDA = iidTipoCDA;
    }

    public void setTipoCDA(String stipoCDA) {
        this.stipoCDA = stipoCDA;
    }
    
    public void setFechaInicio(java.util.Date dfechaInicio) {
        this.dfechaInicio = dfechaInicio;
    }
    
    public void setFechaIngreso(java.util.Date dfechaIngreso) {
        this.dfechaIngreso = dfechaIngreso;
    }

    public void setFechaFin(java.util.Date dfechaFin) {
        this.dfechaFin = dfechaFin;
    }

    public void setDias(int idias) {
        this.idias = idias;
    }
    
    public void setCapitalInicial(double dcapitalinicial) {
        this.dcapitalinicial = dcapitalinicial;
    }
    
    public void setTasaInteres(double dtasaInteres) {
        this.dtasaInteres = dtasaInteres;
    }
    
    public void setTotalInteres(double dtotalInteres) {
        this.dtotalInteres = dtotalInteres;
    }

    public void setIdEstadoCDA(int iidEstadoCDA) {
        this.iidEstadoCDA = iidEstadoCDA;
    }

    public void setEstadoCDA(String sestadoCDA) {
        this.sestadoCDA = sestadoCDA;
    }    
    
    public void setIdTipoCapitalizacion(short hidTipoCapitalizacion) {
        this.hidTipoCapitalizacion = hidTipoCapitalizacion;
    }

    public void setTipoCapitalizacion(String stipoCapitalizacion) {
        this.stipoCapitalizacion = stipoCapitalizacion;
    }    

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setFechaPrimerVencimiento(java.util.Date dfechaPrimerVencimiento) {
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setFechaRetirado(java.util.Date dfechaRetirado) {
        this.dfechaRetirado = dfechaRetirado;
    }

    public void setObservacionRetirado(String sobservacionRetirado) {
        this.sobservacionRetirado = sobservacionRetirado;
    }

    public void setFechaCumplido(java.util.Date dfechaCumplido) {
        this.dfechaCumplido = dfechaCumplido;
    }

    public void setObservacionCumplido(String sobservacionCumplido) {
        this.sobservacionCumplido = sobservacionCumplido;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getNumeroCDA() {
        return this.inumeroCDA;
    }

    public String getSerie() {
        return this.sserie;
    }
    
    public String getCedula() {
        return this.scedula;
    }
    
    public String getNombre() {
        return this.snombre;
    }
    
    public int getIdTipoCDA() {
        return this.iidTipoCDA;
    }

    public String getTipoCDA() {
        return this.stipoCDA;
    }

    public java.util.Date getFechaInicio() {
        return this.dfechaInicio;
    }
    
    public java.util.Date getFechaIngreso() {
        return this.dfechaIngreso;
    }
    
    public java.util.Date getFechaFin() {
        return this.dfechaFin;
    }
      
    public int getDias() {
        return this.idias;
    }

    public double getCapitalInicial() {
        return this.dcapitalinicial;
    }

    public double getTasaInteres() {
        return this.dtasaInteres;
    }
    
    public double getTotalInteres() {
        return this.dtotalInteres;
    }
    
    public int getIdEstadoCDA() {
        return this.iidEstadoCDA;
    }

    public String getEstadoCDA() {
        return this.sestadoCDA;
    }
    
    public short getIdTipoCapitalizacion() {
        return this.hidTipoCapitalizacion;
    }

    public String getTipoCapitalizacion() {
        return this.stipoCapitalizacion;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public java.util.Date getFechaPrimerVencimiento() {
        return this.dfechaPrimerVencimiento;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public java.util.Date getFechaRetirado() {
        return this.dfechaRetirado;
    }

    public String getObservacionRetirado() {
        return this.sobservacionRetirado;
    }

    public java.util.Date getFechaCumplido() {
        return this.dfechaCumplido;
    }

    public String getObservacionCumplido() {
        return this.sobservacionCumplido;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        /*if (this.getIdTipoCDA()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += entCDA.TEXTO_TIPOCDA;
            bvalido = false;
        }*/
        if (this.getCapitalInicial()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += entCDA.TEXTO_CAPITAL_INICIAL;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += entCDA.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoRetirar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionRetirado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += entCDA.TEXTO_OBSERVACION_RETIRADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    // métodos para la conexión
    public String getSentencia() {
        return "SELECT cda("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getSerie())+","+
            this.getIdTipoCDA()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaInicio())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaFin())+","+
            this.getDias()+","+
            this.getCapitalInicial()+","+
            this.getTasaInteres()+","+
            this.getTotalInteres()+","+
            this.getIdEstadoCDA()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            this.getNumeroCDA()+","+
            this.getIdTipoCapitalizacion()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaIngreso())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Nº CDA", "numerocda", entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Serie", "serie", entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Cedula", "cedula", entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Nombre", "nombre", entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Tipo", "tipocda", entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Estado", "estadocda", entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Fecha Ingreso", "fechaingreso", entLista.tipo_fecha));
        lst.add(new generico.entLista(8, "Fecha Inicio", "fechainicio", entLista.tipo_fecha));
        lst.add(new generico.entLista(9, "Fecha Fin", "fechafin", entLista.tipo_fecha));
        lst.add(new generico.entLista(10, "Capital inicial", "capitalinicial", entLista.tipo_numero));
        lst.add(new generico.entLista(11, "Plazo", "dias", entLista.tipo_numero));
        return lst;
    }

}
