/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entListaOperacionSeleccion {
    
    private int iid;
    private String sdescripcion;
    private int inumero;
    private java.util.Date dfecha;
    private String stabla;
    private boolean bacepta;
    
    public static final int LONGITUD_DESCRIPCION = 50;

    public static final String TEXTO_TEXTO = "Texto a buscar";

    public static final String TEXTO_DESCRIPCION = "Descripción";
    public static final String TEXTO_NUMERO = "Número";
    public static final String TEXTO_FECHA = "Fecha";
    public static final String TEXTO_ACEPTA = "Acepta";

    public entListaOperacionSeleccion() {
        this.iid = 0;
        this.sdescripcion = "";
        this.inumero = 0;
        this.dfecha = null;
        this.stabla = "";
        this.bacepta = false;
    }

    public entListaOperacionSeleccion(int iid, String sdescripcion, int inumero, java.util.Date dfecha, String stabla, boolean bacepta) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.inumero = inumero;
        this.dfecha = dfecha;
        this.stabla = stabla;
        this.bacepta = bacepta;
    }

    public void setEntidad(int iid, String sdescripcion, int inumero, java.util.Date dfecha, String stabla, boolean bacepta) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.inumero = inumero;
        this.dfecha = dfecha;
        this.stabla = stabla;
        this.bacepta = bacepta;
    }

    public entListaOperacionSeleccion copiar(entListaOperacionSeleccion destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getNumero(), this.getFecha(), this.getTabla(), this.getAcepta());
        return destino;
    }
    
    public entListaOperacionSeleccion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setNumero(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setAcepta(false); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setNumero(int inumero) {
        this.inumero = inumero;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setAcepta(boolean bacepta) {
        this.bacepta = bacepta;
    }
    
    public int getId() {
        return this.iid;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public int getNumero() {
        return this.inumero;
    }
    
    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public String getTabla() {
        return this.stabla;
    }
    
    public boolean getAcepta() {
        return this.bacepta;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Número", "numerooperacion", generico.entLista.tipo_numero));
        return lst;
    }
    
    public String getConsulta(int iidSocio, String stexto) {
        if (stexto.equals("0")) stexto="";
        String stextoNuevo = "%"+stexto.trim().toUpperCase()+"%";
        return "SELECT * FROM (" +
            "SELECT id, tipocredito||' '||entidad AS descripcion, numerooperacion, fechaaprobado AS fecha, 'mo' AS tabla FROM "+generico.vista.getNombre(generico.vista.vwmovimiento1)+" WHERE idsocio="+iidSocio+" AND estado LIKE 'GENERADO' AND tipocredito||' '||entidad||' '||numerooperacion LIKE '%"+stextoNuevo+"%' " +
            "UNION " +
            "SELECT id, cuenta as descripcion, numerooperacion, fechaoperacion AS fecha, 'of' AS tabla FROM "+generico.vista.getNombre(generico.vista.vwoperacionfija1)+" WHERE idsocio="+iidSocio+" AND estado LIKE 'ACTIVO' AND cuenta||' '||numerooperacion LIKE '%"+stextoNuevo+"%' " +
            "UNION " +
            "SELECT id, cuenta as descripcion, numerooperacion, fechaoperacion AS fecha, 'ap' AS tabla FROM "+generico.vista.getNombre(generico.vista.vwahorroprogramado1)+" WHERE idsocio="+iidSocio+" AND estado LIKE 'ACTIVO' AND cuenta||' '||numerooperacion LIKE '%"+stextoNuevo+"%' " +
            "UNION " +
            "SELECT id, cuenta as descripcion, numerooperacion, fechaoperacion AS fecha, 'js' AS tabla FROM "+generico.vista.getNombre(generico.vista.vwfondojuridicosepelio1)+" WHERE idsocio="+iidSocio+" AND estado LIKE 'ACTIVO' AND cuenta||' '||numerooperacion LIKE '%"+stextoNuevo+"%' " +
            ") AS operacion ORDER BY fecha";
    }
    
    public String getConsulta(int iidOperacion, String stabla, String sconsulta) {
        if (stabla.contains("mo")) sconsulta = "SELECT id, tipocredito||' '||entidad AS descripcion, numerooperacion, fechaaprobado AS fecha, 'mo' AS tabla FROM "+generico.vista.getNombre(generico.vista.vwmovimiento1)+" WHERE id="+iidOperacion+" ";
        if (stabla.contains("of")) sconsulta = "SELECT id, cuenta as descripcion, numerooperacion, fechaoperacion AS fecha, 'of' AS tabla FROM "+generico.vista.getNombre(generico.vista.vwoperacionfija1)+" WHERE id="+iidOperacion+" ";
        if (stabla.contains("ap")) sconsulta = "SELECT id, cuenta as descripcion, numerooperacion, fechaoperacion AS fecha, 'ap' AS tabla FROM "+generico.vista.getNombre(generico.vista.vwahorroprogramado1)+" WHERE id="+iidOperacion+" ";
        if (stabla.contains("js")) sconsulta = "SELECT id, cuenta as descripcion, numerooperacion, fechaoperacion AS fecha, 'js' AS tabla FROM "+generico.vista.getNombre(generico.vista.vwfondojuridicosepelio1)+" WHERE id="+iidOperacion+" ";
        return sconsulta;
    }
    
}
