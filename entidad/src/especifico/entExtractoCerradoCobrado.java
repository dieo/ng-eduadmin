/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entExtractoCerradoCobrado {

    private int iidSocio;
    private int iidMovimiento;
    private String speriodo;
    private int iidCuenta;
    private String scuenta;
    private String sdenominacion;
    private int inumeroOperacion;
    private int iplazo;
    private int inumeroCuota;
    private double umontoCerrado;
    private double umontoGiraduria;
    private double umontoVentanilla;
    private double umontoAsignacion;
    private double umontoReembolso;
    private double umontoDiferencia;
    private String stabla;
    private String srubro;
    private int iestadoRegistro;
    
    public entExtractoCerradoCobrado() {
        this.iidSocio = 0;
        this.iidMovimiento = 0;
        this.speriodo = "";
        this.iidCuenta = 0;
        this.scuenta = "";
        this.sdenominacion = "";
        this.inumeroOperacion = 0;
        this.iplazo = 0;
        this.inumeroCuota = 0;
        this.umontoCerrado = 0.0;
        this.umontoGiraduria = 0.0;
        this.umontoVentanilla = 0.0;
        this.umontoAsignacion = 0.0;
        this.umontoReembolso = 0.0;
        this.umontoDiferencia = 0.0;
        this.stabla = "";
        this.srubro = "";
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entExtractoCerradoCobrado(int iidSocio, int iidMovimiento, String speriodo, int iidCuenta, String scuenta, String sdenominacion, int inumeroOperacion, int iplazo, int inumeroCuota, double umontoCerrado, double umontoGiraduria, double umontoVentanilla, double umontoAsignacion, double umontoReembolso, double umontoDiferencia, String stabla, String srubro, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.speriodo = speriodo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdenominacion = sdenominacion;
        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.inumeroCuota = inumeroCuota;
        this.umontoCerrado = umontoCerrado;
        this.umontoGiraduria = umontoGiraduria;
        this.umontoVentanilla = umontoVentanilla;
        this.umontoAsignacion = umontoAsignacion;
        this.umontoReembolso = umontoReembolso;
        this.umontoDiferencia = umontoDiferencia;
        this.stabla = stabla;
        this.srubro = srubro;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iidSocio, int iidMovimiento, String speriodo, int iidCuenta, String scuenta, String sdenominacion, int inumeroOperacion, int iplazo, int inumeroCuota, double umontoCerrado, double umontoGiraduria, double umontoVentanilla, double umontoAsignacion, double umontoReembolso, double umontoDiferencia, String stabla, String srubro, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.speriodo = speriodo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdenominacion = sdenominacion;
        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.inumeroCuota = inumeroCuota;
        this.umontoCerrado = umontoCerrado;
        this.umontoGiraduria = umontoGiraduria;
        this.umontoVentanilla = umontoVentanilla;
        this.umontoAsignacion = umontoAsignacion;
        this.umontoReembolso = umontoReembolso;
        this.umontoDiferencia = umontoDiferencia;
        this.stabla = stabla;
        this.srubro = srubro;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entExtractoCerradoCobrado copiar(entExtractoCerradoCobrado destino) {
        destino.setEntidad(this.getIdSocio(), this.getIdMovimiento(), this.getPeriodo(), this.getIdCuenta(), this.getCuenta(), this.getDenominacion(), this.getNumeroOperacion(), this.getPlazo(), this.getNumeroCuota(), this.getMontoCerrado(), this.getMontoGiraduria(), this.getMontoVentanilla(), this.getMontoAsignacion(), this.getMontoReembolso(), this.getMontoDiferencia(), this.getTabla(), this.getRubro(), this.getEstadoRegistro());
        return destino;
    }

    public entExtractoCerradoCobrado cargar(java.sql.ResultSet rs) {
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setPeriodo(rs.getString("periodo")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setDenominacion(rs.getString("denominacion")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setNumeroCuota(rs.getInt("numerocuota")); }
        catch(Exception e) {}
        try { this.setMontoCerrado(rs.getDouble("montocerrado")); }
        catch(Exception e) {}
        try { this.setMontoGiraduria(rs.getDouble("montogiraduria")); }
        catch(Exception e) {}
        try { this.setMontoVentanilla(rs.getDouble("montoventanilla")); }
        catch(Exception e) {}
        try { this.setMontoAsignacion(rs.getDouble("montoasignacion")); }
        catch(Exception e) {}
        try { this.setMontoReembolso(rs.getDouble("montoreembolso")); }
        catch(Exception e) {}
        try { this.setMontoDiferencia(rs.getDouble("montodiferencia")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setPeriodo(String speriodo) {
        this.speriodo = speriodo;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setDenominacion(String sdenominacion) {
        this.sdenominacion = sdenominacion;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setNumeroCuota(int inumeroCuota) {
        this.inumeroCuota = inumeroCuota;
    }

    public void setMontoCerrado(double umontoCerrado) {
        this.umontoCerrado = umontoCerrado;
    }

    public void setMontoGiraduria(double umontoGiraduria) {
        this.umontoGiraduria = umontoGiraduria;
    }

    public void setMontoVentanilla(double umontoVentanilla) {
        this.umontoVentanilla = umontoVentanilla;
    }

    public void setMontoAsignacion(double umontoAsignacion) {
        this.umontoAsignacion = umontoAsignacion;
    }

    public void setMontoReembolso(double umontoReembolso) {
        this.umontoReembolso = umontoReembolso;
    }

    public void setMontoDiferencia(double umontoDiferencia) {
        this.umontoDiferencia = umontoDiferencia;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public String getPeriodo() {
        return this.speriodo;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getDenominacion() {
        return this.sdenominacion;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public int getNumeroCuota() {
        return this.inumeroCuota;
    }

    public Double getMontoCerrado() {
        return this.umontoCerrado;
    }

    public Double getMontoGiraduria() {
        return this.umontoGiraduria;
    }

    public Double getMontoVentanilla() {
        return this.umontoVentanilla;
    }

    public double getMontoAsignacion() {
        return this.umontoAsignacion;
    }

    public double getMontoReembolso() {
        return this.umontoReembolso;
    }

    public double getMontoDiferencia() {
        return this.umontoDiferencia;
    }

    public String getTabla() {
        return this.stabla;
    }

    public String getRubro() {
        return this.srubro;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    public String getConsultaCerrado(java.util.Date dfecha, int iidSocio) {
        String speriodo = "PERIODO "+utilitario.utiFecha.getMeses()[dfecha.getMonth()]+"/"+utilitario.utiFecha.getAnho(dfecha);
        return "SELECT ci.idcuenta, '"+speriodo+"' AS periodo, cu.descripcion AS cuenta, gi.descripcionbreve||' '||ru.descripcionbreve AS rubro, ci.idmovimiento, ci.numerooperacion, ci.plazo, ci.tabla, SUM(ci.saldo) AS montocerrado "+
            "FROM cierre AS ci "+
            "LEFT JOIN cuenta AS cu ON ci.idcuenta=cu.id "+
            "LEFT JOIN giraduria AS gi ON ci.idgiraduria=gi.id "+
            "LEFT JOIN rubro AS ru ON ci.idrubro=ru.id "+
            "WHERE ci.fechacierre='"+utilitario.utiFecha.getUltimoDia(dfecha)+"' AND ci.idsocio="+iidSocio+" "+
            "GROUP BY ci.idcuenta, cu.descripcion, gi.descripcionbreve, ru.descripcionbreve, ci.idmovimiento, ci.numerooperacion, ci.plazo, ci.tabla "+
            "ORDER BY ci.idcuenta";
    }
    
    public String getConsultaCerradov2(java.util.Date dfecha, int iidSocio) {
        String speriodo = "PERIODO "+utilitario.utiFecha.getMeses()[dfecha.getMonth()]+"/"+utilitario.utiFecha.getAnho(dfecha);
        return "SELECT det.periodo, det.idcuenta, cu.cuenta, cu.orden, ope.numerooperacion, ope.fechaoperacion, ope.plazo, gi.descripcionbreve||' '||ru.descripcionbreve AS rubro, det.idmovimiento, det.tabla, det.cuotames, det.montoatraso, det.montomes, det.montogir, det.montoven, det.montoree, det.montoasi "+
            "FROM ("+
            "   SELECT '"+speriodo+"' AS periodo, SUM(id) AS id, idcuenta, idmovimiento, tabla, SUM(cuotames) AS cuotames, SUM(montoatraso) AS montoatraso, SUM(montomes) AS montomes, SUM(montogir) AS montogir, SUM(montoven) AS montoven, SUM(montoree) AS montoree, SUM(montoasi) AS montoasi "+
            "   FROM ("+
            "      SELECT cie.id, cie.idcuenta, cie.idmovimiento, cie.tabla, cie.cuotames, cie.montoatraso, cie.montomes, 0 AS montogir, 0 AS montoven, 0 AS montoree, 0 AS montoasi "+
            "      FROM (SELECT ci.id, ci.idcuenta, ci.idmovimiento, ci.tabla, "+
            "         SUM(CASE WHEN TO_CHAR(ci.fechavencimiento,'YYYYMM00')=TO_CHAR(ci.fechacierre,'YYYYMM00') THEN ci.cuota ELSE 0 END) AS cuotames, "+
            "         SUM(CASE WHEN TO_CHAR(ci.fechavencimiento,'YYYYMM00')<>TO_CHAR(ci.fechacierre,'YYYYMM00') THEN ci.saldo ELSE 0 END) AS montoatraso, "+
            "         SUM(CASE WHEN TO_CHAR(ci.fechavencimiento,'YYYYMM00')=TO_CHAR(ci.fechacierre,'YYYYMM00') THEN ci.saldo ELSE 0 END) AS montomes "+
            "      FROM cierre AS ci "+
            "      WHERE TO_CHAR(ci.fechacierre,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' AND ci.idsocio="+iidSocio+" "+
            "      GROUP BY ci.id, ci.idcuenta, ci.idmovimiento, ci.tabla ) AS cie "+
            "      UNION "+
            "      SELECT 0 AS id, di.idcuenta, di.idmovimiento, di.tabla, 0 AS cuotames, 0 AS montoatraso, 0 AS montomes, SUM(di.montocobro) AS montogir, 0 AS montoven, 0 AS montoree, 0 AS montoasi "+
            "      FROM detalleingreso AS di "+
            "      LEFT JOIN ingreso AS ing ON di.idingreso=ing.id "+
            "      LEFT JOIN planillaingreso AS pi ON ing.idplanilla=pi.id "+
            "      LEFT JOIN detalleoperacion AS dop ON di.iddetalleoperacion=dop.id "+
            "      WHERE ing.idestado=146 AND pi.idviacobro=38 AND ing.idsocio="+iidSocio+" AND TO_CHAR(ing.fechaaaplicar,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' "+
            "      GROUP BY di.idcuenta, di.idmovimiento, di.tabla "+
            "      UNION "+
            "      SELECT 0 AS id, di.idcuenta, di.idmovimiento, di.tabla, 0 AS cuotames, 0 AS montoatraso, 0 AS montomes, 0 AS montogir, SUM(di.montocobro) AS montoven, 0 AS montoree, 0 AS montoasi "+
            "      FROM detalleingreso AS di "+
            "      LEFT JOIN ingreso AS ing ON di.idingreso=ing.id "+
            "      LEFT JOIN planillaingreso AS pi ON ing.idplanilla=pi.id "+
            "      LEFT JOIN detalleoperacion AS dop ON di.iddetalleoperacion=dop.id "+
            "      WHERE ing.idestado=146 AND pi.idviacobro=39 AND ing.idsocio="+iidSocio+" AND TO_CHAR(ing.fechaaaplicar,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' "+
            "      GROUP BY di.idcuenta, di.idmovimiento, di.tabla "+
            "      UNION "+
            "      SELECT 0 AS id, di.idcuenta, di.idmovimiento, di.tabla, 0 AS cuotames, 0 AS montoatraso, 0 AS montomes, 0 AS montogir, 0 AS montoven, SUM(di.montocobro) AS montoree, 0 AS montoasi "+
            "      FROM detalleingreso AS di "+
            "      LEFT JOIN ingreso AS ing ON di.idingreso=ing.id "+
            "      LEFT JOIN planillaingreso AS pi ON ing.idplanilla=pi.id "+
            "      LEFT JOIN detalleoperacion AS dop ON di.iddetalleoperacion=dop.id "+
            "      WHERE ing.idestado=146 AND pi.idviacobro=222 AND ing.idsocio="+iidSocio+" AND TO_CHAR(ing.fechaaaplicar,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' "+
            "      GROUP BY di.idcuenta, di.idmovimiento, di.tabla "+
            "      UNION "+
            "      SELECT 0 AS id, di.idcuenta, di.idmovimiento, di.tabla, 0 AS cuotames, 0 AS montoatraso, 0 AS montomes, 0 AS montogir, 0 AS montoven, 0 AS montoree, SUM(di.montocobro) AS montoasi "+
            "      FROM detalleingreso AS di "+
            "      LEFT JOIN ingreso AS ing ON di.idingreso=ing.id "+
            "      LEFT JOIN planillaingreso AS pi ON ing.idplanilla=pi.id "+
            "      LEFT JOIN detalleoperacion AS dop ON di.iddetalleoperacion=dop.id "+
            "      WHERE ing.idestado=146 AND pi.idviacobro=221 AND ing.idsocio="+iidSocio+" AND TO_CHAR(ing.fechaaaplicar,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' "+
            "      GROUP BY di.idcuenta, di.idmovimiento, di.tabla "+
            "   ) AS detalle "+
            "   GROUP BY periodo, idcuenta, idmovimiento, tabla "+
            ") AS det "+
            "LEFT JOIN cierre AS ci ON det.id=ci.id "+
            "LEFT JOIN giraduria AS gi ON ci.idgiraduria=gi.id "+
            "LEFT JOIN rubro AS ru ON ci.idrubro=ru.id "+
            "LEFT JOIN "+
            "   (SELECT cu.id, cu.descripcion AS cuenta, "+
            "      (CASE WHEN cu.id<=5 THEN 'A' "+
            "      WHEN cu.id=83 THEN 'B' "+
            "      WHEN cu.id=17 OR cu.id=85 THEN 'C' "+
            "      WHEN cu.id=47 THEN 'D' "+
            "      WHEN cu.id=35 OR cu.id=104 THEN 'E' "+
            "      WHEN cu.id=105 THEN 'F' "+
            "      WHEN cu.id=98 THEN 'G' "+
            "      WHEN cu.idtipocuenta=26 THEN 'Z' "+
            "      ELSE 'X' END) AS orden "+
            "   FROM cuenta as cu "+
            "   GROUP BY cu.id, cu.descripcion, orden) AS cu ON det.idcuenta=cu.id "+
            "LEFT JOIN "+
            "   (SELECT id, numerooperacion, fechaoperacion, plazo, tabla "+
            "      FROM (SELECT id, numerooperacion, fechaaprobado AS fechaoperacion, plazoaprobado AS plazo, 'mo' AS tabla FROM movimiento WHERE idsocio="+iidSocio+" "+
            "         UNION "+
            "         SELECT id, numerooperacion, fechaoperacion, plazo, 'of' AS tabla FROM operacionfija WHERE idsocio="+iidSocio+" "+
            "         UNION "+
            "         SELECT id, numerooperacion, fechaoperacion, plazo, 'ap' AS tabla FROM ahorroprogramado WHERE idsocio="+iidSocio+" "+
            "         UNION "+
            "         SELECT id, numerooperacion, fechaoperacion, 0 AS plazo, 'js' AS tabla FROM fondojuridicosepelio WHERE idsocio="+iidSocio+") AS ope "+
            "      ) AS ope ON det.idmovimiento=ope.id AND det.tabla=ope.tabla "+
            "ORDER BY cu.orden ";
    }
    
    public String getConsultaGiraduria(java.util.Date dfecha, int iidSocio) {
        return "SELECT di.idcuenta, cu.descripcion AS cuenta, dop.idmovimiento, "+
                "CASE WHEN ope.numerooperacion IS NULL THEN 0 "+
                "     ELSE ope.numerooperacion "+
                "END AS numerooperacion, "+
                "di.tabla, SUM(di.montocobro) AS montogiraduria "+
            "FROM detalleingreso AS di "+
            "LEFT JOIN ingreso AS ing ON di.idingreso=ing.id "+
            "LEFT JOIN cuenta AS cu ON di.idcuenta=cu.id "+
            "LEFT JOIN planillaingreso AS pi ON ing.idplanilla=pi.id "+
            "LEFT JOIN detalleoperacion AS dop ON di.iddetalleoperacion=dop.id "+
            "LEFT JOIN "+
            "(SELECT id, numerooperacion, tabla FROM ("+
            "   SELECT id, numerooperacion, 'mo' AS tabla FROM movimiento WHERE idsocio="+iidSocio+" "+
            "   UNION "+
            "   SELECT id, numerooperacion, 'of' AS tabla FROM operacionfija WHERE idsocio="+iidSocio+" "+
            "   UNION "+
            "   SELECT id, numerooperacion, 'ap' AS tabla FROM ahorroprogramado WHERE idsocio="+iidSocio+" "+
            "   UNION "+
            "   SELECT id, numerooperacion, 'js' AS tabla FROM fondojuridicosepelio WHERE idsocio="+iidSocio+" "+
            ") AS ope) AS ope ON dop.idmovimiento=ope.id AND dop.tabla=ope.tabla "+
            "WHERE ing.idestado=146 AND pi.idviacobro=38 AND ing.idsocio="+iidSocio+" AND ing.fechaaaplicar='"+utilitario.utiFecha.getUltimoDia(dfecha)+"' "+
            "GROUP BY di.idcuenta, cu.descripcion, dop.idmovimiento, ope.numerooperacion, di.tabla "+
            "ORDER BY di.idcuenta";
    }
    
    public String getConsultaCerradoFuncionarioMSP(java.util.Date dfecha, String scedula, String sorden) {
        String speriodo = "PERIODO "+utilitario.utiFecha.getMeses()[dfecha.getMonth()]+"/"+utilitario.utiFecha.getAnho(dfecha);
        return "SELECT ci.idcuenta, '"+speriodo+"' AS periodo, cu.descripcion AS cuenta, gi.descripcionbreve||' '||ru.descripcionbreve AS rubro, ci.idmovimiento, ci.numerooperacion, ci.plazo, ci.tabla, SUM(ci.saldo) AS montocerrado "+
            "FROM cierre AS ci "+
            "LEFT JOIN cuenta AS cu ON ci.idcuenta=cu.id "+
            "LEFT JOIN giraduria AS gi ON ci.idgiraduria=gi.id "+
            "LEFT JOIN rubro AS ru ON ci.idrubro=ru.id "+
            "WHERE ci.fechacierre='"+utilitario.utiFecha.getUltimoDia(dfecha)+"' AND ci.cedula='"+scedula+"' "+
            "GROUP BY ci.idcuenta, cu.descripcion, gi.descripcionbreve, ru.descripcionbreve, ci.idmovimiento, ci.numerooperacion, ci.plazo, ci.tabla "+
            "ORDER BY ci.idcuenta";
    }
    
    public String getConsultaGiraduriaFuncionarioMSP(java.util.Date dfecha, String scedula) {
        return "SELECT di.idcuenta, cu.descripcion AS cuenta, di.idmovimiento, di.numerooperacion, di.tabla, SUM(di.montocobro) AS montogiraduria "+
            "FROM detalleingreso AS di "+
            "LEFT JOIN ingreso AS ing ON di.idingreso=ing.id "+
            "LEFT JOIN cuenta AS cu ON di.idcuenta=cu.id "+
            "LEFT JOIN planillaingreso AS pi ON ing.idplanilla=pi.id "+
            "WHERE ing.idestado=146 AND pi.idviacobro=38 AND ing.ruc='"+scedula+"' AND di.tabla='ce' AND ing.fechaaaplicar='"+utilitario.utiFecha.getUltimoDia(dfecha)+"' "+
            "GROUP BY di.idcuenta, cu.descripcion, di.idmovimiento, di.numerooperacion, di.tabla "+
            "ORDER BY di.idcuenta";
    }
    
    public String getConsultaEstado(String sfiltro, String sorden) {
        return "SELECT * FROM detalleoperacion WHERE "+sfiltro+sorden;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
