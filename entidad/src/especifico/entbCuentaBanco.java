/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbCuentaBanco {

    protected int iid;
    protected String snumerocuenta;
    protected int iidBanco;
    protected String sbanco;
    protected int iidMoneda;
    protected String smoneda;
    protected int iidTipoCuenta;
    protected String stipocuenta;
    protected String scuentabanco;
    protected int iidPlanCuenta;
    private java.util.Date dfechaapertura;
    private java.util.Date dfechacierre;
    private double dsaldo;
    private double dsaldoinicial;
    private boolean bactivo;
    private String sdescripcion;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_NUMERO_CUENTA = 20;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMERO_CUENTA = "Número de la cuenta (no vacía, hasta " + LONGITUD_NUMERO_CUENTA + " caracteres)";
    public static final String TEXTO_BANCO = "Banco (no vacío)";
    public static final String TEXTO_TIPO_CUENTA = "Tipo de cuenta (no vacío)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_FECHA_APERTURA = "Fecha de Apertura (no vacía)";
    public static final String TEXTO_FECHA_CIERRE = "Fecha de Cierre de cuenta";
    public static final String TEXTO_SALDO = "Saldo Actual de la Cuenta (no editable)";
    public static final String TEXTO_SALDO_INICIAL = "Saldo Inicial a la apertura de la Cuenta";
    public static final String TEXTO_PLAN_CUENTA = "Cuenta contable a la que pertenece la Cuenta (no vacía)";
    public static final String TEXTO_ACTIVO = "Activo/Inactivo";
    public static final String TEXTO_DESCRIPCION = "Descripción de la Cuenta (no editable)";

    public entbCuentaBanco() {
        this.iid = 0;
        this.iidBanco = 0;
        this.sbanco = "";
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iidTipoCuenta = 0;
        this.stipocuenta = "";
        this.scuentabanco = "";
        this.iidPlanCuenta = 0;
        this.snumerocuenta = "";
        this.dfechaapertura = null;
        this.dfechacierre = null;
        this.dsaldo = 0.0;
        this.dsaldoinicial = 0.0;
        this.bactivo = false;
        this.sdescripcion = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbCuentaBanco(int iid, String snumerocuenta, int iidBanco, String sbanco, int iidMoneda, String smoneda, int iidTipoCuenta, String stipocuenta, String scuentabanco, java.util.Date dfechaapertura, java.util.Date dfechacierre,double dsaldo, int iidPlanCuenta, double dsaldoinicial, boolean bactivo, String sdescripcion, short hestadoRegistro) {        
        this.iid = iid;
        this.snumerocuenta = snumerocuenta;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidTipoCuenta = iidTipoCuenta;
        this.stipocuenta = stipocuenta;
        this.scuentabanco = scuentabanco;
        this.dfechaapertura = dfechaapertura;
        this.dfechacierre = dfechacierre;
        this.dsaldo = dsaldo;
        this.iidPlanCuenta = iidPlanCuenta;
        this.dsaldoinicial = dsaldoinicial;
        this.bactivo = bactivo;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, String snumerocuenta, int iidBanco, String sbanco, int iidMoneda, String smoneda, int iidTipoCuenta, String stipocuenta, String scuentabanco, java.util.Date dfechaapertura, java.util.Date dfechacierre, double dsaldo, int iidPlanCuenta, double dsaldoinicial, boolean bactivo, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.snumerocuenta = snumerocuenta;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidTipoCuenta = iidTipoCuenta;
        this.stipocuenta = stipocuenta;
        this.scuentabanco = scuentabanco;
        this.dfechaapertura = dfechaapertura;
        this.dsaldo = dsaldo;
        this.dsaldoinicial = dsaldoinicial;
        this.iidPlanCuenta = iidPlanCuenta;
        this.bactivo = bactivo;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entbCuentaBanco copiar(entbCuentaBanco destino) {
        destino.setEntidad(this.getId(), this.getNumeroCuenta(), this.getIdBanco(), this.getBanco(), this.getIdMoneda(), this.getMoneda(), this.getIdTipoCuenta(), this.getTipoCuenta(), this.getCuentaBanco(), this.getFechaApertura(), this.getFechaCierre(), this.getSaldo(), this.getIdPlanCuenta(), this.getSaldoInicial(), this.getActivo(), this.getDescripcion(), this.getEstadoRegistro());
        return destino;
    }

    public entbCuentaBanco cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroCuenta(rs.getString("numerocuenta")); }
        catch(Exception e) {}
        try { this.setIdBanco(rs.getInt("idbanco")); }
        catch(Exception e) {}
        try { this.setBanco(rs.getString("banco")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setIdTipoCuenta(rs.getInt("idtipocuenta")); }
        catch(Exception e) {}
        try { this.setTipoCuenta(rs.getString("tipocuenta")); }
        catch(Exception e) {}
        try { this.setCuentaBanco(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) {}
        try { this.setFechaApertura(rs.getDate("fechaapertura")); }
        catch(Exception e) {}
        try { this.setFechaCierre(rs.getDate("fechacierre")); }
        catch(Exception e) {}
        try { this.setSaldo(rs.getDouble("saldo")); }
        catch(Exception e) {}
        try { this.setSaldoInicial(rs.getDouble("saldoinicial")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdBanco(int iidBanco) {
        this.iidBanco = iidBanco;
    }

    public void setBanco(String sbanco) {
        this.sbanco = sbanco;
    }
    
    public void setNumeroCuenta(String snumerocuenta) {
        this.snumerocuenta = snumerocuenta;
    }
    
    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setIdTipoCuenta(int iidTipoCuenta) {
        this.iidTipoCuenta = iidTipoCuenta;
    }
    
    public void setTipoCuenta(String stipocuenta) {
        this.stipocuenta = stipocuenta;
    }
    
    public void setCuentaBanco(String scuentabanco) {
        this.scuentabanco = scuentabanco;
    }
    
    public void setFechaApertura(java.util.Date dfechaapertura) {
        this.dfechaapertura = dfechaapertura;
    }
    
    public void setFechaCierre(java.util.Date dfechacierre) {
        this.dfechacierre = dfechacierre;
    }
    
    public void setSaldo(double dsaldo) {
        this.dsaldo = dsaldo;
    }
    
    public void setSaldoInicial(double dsaldoinicial) {
        this.dsaldoinicial = dsaldoinicial;
    }
    
    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getIdBanco() {
        return this.iidBanco;
    }
    
    public String getBanco() {
        return this.sbanco;
    }

    public String getNumeroCuenta() {
        return this.snumerocuenta;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }
    
    public String getMoneda() {
        return this.smoneda;
    }
    
    public int getIdTipoCuenta() {
        return this.iidTipoCuenta;
    }
    
    public String getTipoCuenta() {
        return this.stipocuenta;
    }
    
    public String getCuentaBanco() {
        return this.scuentabanco;
    }
    
    public java.util.Date getFechaApertura() {
        return this.dfechaapertura;
    }
    
    public java.util.Date getFechaCierre() {
        return this.dfechacierre;
    }
    
    public double getSaldo() {
        return this.dsaldo;
    }
    
    public double getSaldoInicial() {
        return this.dsaldoinicial;
    }
    
    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }
    
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    } 
     
     public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido(int cantidadActivo) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNumeroCuenta().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_NUMERO_CUENTA;
            bvalido = false;
        }
        if (this.getIdBanco()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_BANCO;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONEDA;
            bvalido = false;
        }
        //if (this.getIdPlanCuenta()<=0) {
        //    if (!this.smensaje.isEmpty()) this.smensaje += "\n";
        //    this.smensaje += this.TEXTO_PLAN_CUENTA;
         //   bvalido = false;
        //}
        if (this.getFechaApertura() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA_APERTURA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.bancocuenta("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumeroCuenta())+","+
            this.getIdBanco()+","+
            this.getIdMoneda()+","+
            this.getIdTipoCuenta()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaApertura())+","+
            this.getSaldo()+","+
            this.getIdPlanCuenta()+","+
            this.getSaldoInicial()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaCierre())+","+
            this.getActivo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripcion", "descripcion", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Tipo de Cuenta", "tipocuenta", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(3, "Activa", "activo2", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
