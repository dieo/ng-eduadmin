/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entFacturaProducto extends generico.entGenerica {

    protected int iidUnidadMedida;
    protected String sunidadMedida;
    protected int iidImpuesto;
    protected String simpuesto;
    protected int iidTipoProducto;
    protected String stipoProducto;
    protected short hidClaseProducto;
    protected String sclaseProducto;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Ciudad (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de Medida (no vacío)";
    public static final String TEXTO_IMPUESTO = "Impuesto (no vacío)";
    public static final String TEXTO_TIPO_PRODUCTO = "Tipo de Producto (no vacío)";
    public static final String TEXTO_CLASE_PRODUCTO = "Clase de Producto";

    public entFacturaProducto() {
        super();
        this.iidUnidadMedida = 0;
        this.sunidadMedida = "";
        this.iidImpuesto = 0;
        this.simpuesto = "";
        this.iidTipoProducto = 0;
        this.stipoProducto = "";
        this.hidClaseProducto = 0;
        this.sclaseProducto = "";
    }

    public entFacturaProducto(int iid, String sdescripcion, int iidUnidadMedida, String sunidadMedida, int iidImpuesto, String simpuesto, int iidTipoProducto, String stipoProducto, short hidClaseProducto, String sclaseProducto) {
        super(iid, sdescripcion);
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.iidTipoProducto = iidTipoProducto;
        this.stipoProducto = stipoProducto;
        this.hidClaseProducto = hidClaseProducto;
        this.sclaseProducto = sclaseProducto;
    }

    public entFacturaProducto(int iid, String sdescripcion, int iidUnidadMedida, String sunidadMedida, int iidImpuesto, String simpuesto, int iidTipoProducto, String stipoProducto, short hidClaseProducto, String sclaseProducto, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.iidTipoProducto = iidTipoProducto;
        this.stipoProducto = stipoProducto;
        this.hidClaseProducto = hidClaseProducto;
        this.sclaseProducto = sclaseProducto;
    }

    public void setEntidad(int iid, String sdescripcion, int iidUnidadMedida, String sunidadMedida, int iidImpuesto, String simpuesto, int iidTipoProducto, String stipoProducto, short hidClaseProducto, String sclaseProducto, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.iidTipoProducto = iidTipoProducto;
        this.stipoProducto = stipoProducto;
        this.hidClaseProducto = hidClaseProducto;
        this.sclaseProducto = sclaseProducto;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entFacturaProducto copiar(entFacturaProducto destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdUnidadMedida(), this.getUnidadMedida(), this.getIdImpuesto(), this.getImpuesto(), this.getIdTipoProducto(), this.getTipoProducto(), this.getIdClaseProducto(), this.getClaseProducto(), this.getEstadoRegistro());
        return destino;
    }

    public entFacturaProducto cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdUnidadMedida(rs.getInt("idunidadmedida")); }
        catch(Exception e) {}
        try { this.setUnidadMedida(rs.getString("unidadmedida")); }
        catch(Exception e) {}
        try { this.setIdImpuesto(rs.getInt("idimpuesto")); }
        catch(Exception e) {}
        try { this.setImpuesto(rs.getString("impuesto")); }
        catch(Exception e) {}
        try { this.setIdTipoProducto(rs.getInt("idtipoproducto")); }
        catch(Exception e) {}
        try { this.setTipoProducto(rs.getString("tipoproducto")); }
        catch(Exception e) {}
        try { this.setIdClaseProducto(rs.getShort("idclaseproducto")); }
        catch(Exception e) {}
        try { this.setClaseProducto(rs.getString("claseproducto")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }

    public void setUnidadMedida(String sunidadMedida) {
        this.sunidadMedida = sunidadMedida;
    }

    public void setIdImpuesto(int iidImpuesto) {
        this.iidImpuesto = iidImpuesto;
    }

    public void setImpuesto(String simpuesto) {
        this.simpuesto = simpuesto;
    }

    public void setIdTipoProducto(int iidTipoProducto) {
        this.iidTipoProducto = iidTipoProducto;
    }

    public void setTipoProducto(String stipoProducto) {
        this.stipoProducto = stipoProducto;
    }

    public void setIdClaseProducto(short hidClaseProducto) {
        this.hidClaseProducto = hidClaseProducto;
    }

    public void setClaseProducto(String sclaseProducto) {
        this.sclaseProducto = sclaseProducto;
    }

    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }

    public String getUnidadMedida() {
        return this.sunidadMedida;
    }

    public int getIdImpuesto() {
        return this.iidImpuesto;
    }

    public String getImpuesto() {
        return this.simpuesto;
    }

    public int getIdTipoProducto() {
        return this.iidTipoProducto;
    }

    public String getTipoProducto() {
        return this.stipoProducto;
    }

    public short getIdClaseProducto() {
        return this.hidClaseProducto;
    }

    public String getClaseProducto() {
        return this.sclaseProducto;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdUnidadMedida()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_UNIDAD_MEDIDA;
            bvalido = false;
        }
        if (this.getIdImpuesto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPUESTO;
            bvalido = false;
        }
        if (this.getIdTipoProducto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_PRODUCTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT producto("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdUnidadMedida()+","+
            this.getIdImpuesto()+","+
            this.getIdTipoProducto()+","+
            this.getIdClaseProducto()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Unidad Medida", "unidadmedida", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Impuesto", "impuesto", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Tipo Producto", "tipoproducto", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Clase Producto", "claseproducto", generico.entLista.tipo_texto));
        return lst;
    }
    
}
