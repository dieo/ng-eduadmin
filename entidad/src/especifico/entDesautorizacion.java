/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDesautorizacion {

    private int iid;
    private int iidCuenta;
    private String scuenta;
    private int iidSocio;
    private String scedula;
    private int inumeroSocio;
    private String sapellidoNombre;
    private java.util.Date dfecha;
    private String smotivo;
    private boolean bactivo;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_MOTIVO = 100;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CUENTA = "Cuenta (no vacía)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_FECHA = "Fecha (no vacía)";
    public static final String TEXTO_MOTIVO = "Motivo (no vacío, hasta " + LONGITUD_MOTIVO+ " caracteres)";
    public static final String TEXTO_ACTIVO = "Activo";

    public entDesautorizacion() {
        this.iid = 0;
        this.iidCuenta = 0;
        this.scuenta = "";
        this.iidSocio = 0;
        this.scedula = "";
        this.inumeroSocio = 0;
        this.sapellidoNombre = "";
        this.dfecha = null;
        this.smotivo = "";
        this.bactivo = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDesautorizacion(int iid, int iidCuenta, String scuenta, int iidSocio, String scedula, int inumeroSocio, String sapellidoNombre, java.util.Date dfecha, String smotivo, boolean bactivo) {
        this.iid = iid;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.inumeroSocio = inumeroSocio;
        this.sapellidoNombre = sapellidoNombre;
        this.dfecha = dfecha;
        this.smotivo = smotivo;
        this.bactivo = bactivo;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDesautorizacion(int iid, int iidCuenta, String scuenta, int iidSocio, String scedula, int inumeroSocio, String sapellidoNombre, java.util.Date dfecha, String smotivo, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.inumeroSocio = inumeroSocio;
        this.sapellidoNombre = sapellidoNombre;
        this.dfecha = dfecha;
        this.smotivo = smotivo;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidCuenta, String scuenta, int iidSocio, String scedula, int inumeroSocio, String sapellidoNombre, java.util.Date dfecha, String smotivo, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.inumeroSocio = inumeroSocio;
        this.sapellidoNombre = sapellidoNombre;
        this.dfecha = dfecha;
        this.smotivo = smotivo;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public entDesautorizacion copiar(entDesautorizacion destino) {
        destino.setEntidad(this.getId(), this.getIdCuenta(), this.getCuenta(), this.getIdSocio(), this.getCedula(), this.getNumeroSocio(), this.getApellidoNombre(), this.getFecha(), this.getMotivo(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }

    public entDesautorizacion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNumeroSocio(rs.getInt("numerosocio")); }
        catch(Exception e) {}
        try { this.setApellidoNombre(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setMotivo(rs.getString("motivo")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNumeroSocio(int inumeroSocio) {
        this.inumeroSocio = inumeroSocio;
    }

    public void setApellidoNombre(String sapellidoNombre) {
        this.sapellidoNombre = sapellidoNombre;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setMotivo(String smotivo) {
        this.smotivo = smotivo;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public int getNumeroSocio() {
        return this.inumeroSocio;
    }

    public String getApellidoNombre() {
        return this.sapellidoNombre;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public String getMotivo() {
        return this.smotivo;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdCuenta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CUENTA;
            bvalido = false;
        }
        if (this.getIdSocio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getMotivo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MOTIVO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT desautorizacion("+
            this.getId()+","+
            this.getIdCuenta()+","+
            this.getIdSocio()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFecha())+","+
            utilitario.utiCadena.getTextoGuardado(this.getMotivo())+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cuenta", "cuenta", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Apellido, Nombre", "apellidonombre", generico.entLista.tipo_texto));
        return lst;
    }
    
}
