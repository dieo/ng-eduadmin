/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleFacturaIngreso {

    private int iid;
    private int iidFacturaIngreso;
    private int iitem;
    private int iidProducto;
    private String sproducto;
    private double ucantidad;
    private double uprecio;
    private int iidUnidadMedida;
    private String sunidadMedida;
    private int iidImpuesto;
    private String simpuesto;
    private double uexenta;
    private double ugravada10;
    private double ugravada5;
    private String sreferencia;
    private int iidOperacion;
    private String stabla;
    private short hestadoRegistro;
    private double porcdto;
    private double ventadto;
    private boolean bprestamo;
    private String smensaje;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ITEM = "Item (no editable)";
    public static final String TEXTO_PRODUCTO = "Producto (no vacía)";
    public static final String TEXTO_CANTIDAD = "Cantidad (no vacía, valor positivo)";
    public static final String TEXTO_PORCENTAJE = "Porcentaje (no vacía, valor positivo)";
    public static final String TEXTO_PRECIO = "Precio (no vacío, valor positivo)";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de Medida (no vacía)";
    public static final String TEXTO_IMPUESTO = "Impuesto (valor positivo)";
    public static final String TEXTO_EXENTA = "Exenta (valor positivo)";
    public static final String TEXTO_GRAVADA10 = "Gravada 10% (valor positivo)";
    public static final String TEXTO_GRAVADA5 = "Gravada 5% (valor positivo)";

    public entDetalleFacturaIngreso() {
        this.iid = 0;
        this.iidFacturaIngreso = 0;
        this.iitem = 0;
        this.iidProducto = 0;
        this.sproducto = "";
        this.ucantidad = 0.0;
        this.uprecio = 0.0;
        this.iidUnidadMedida = 0;
        this.sunidadMedida = "";
        this.iidImpuesto = 0;
        this.simpuesto = "";
        this.uexenta = 0.0;
        this.ugravada10 = 0.0;
        this.ugravada5 = 0.0;
        this.sreferencia = "";
        this.iidOperacion = 0;
        this.stabla = "";
        this.porcdto = 0.0;
        this.ventadto = 0.0;
        this.bprestamo = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleFacturaIngreso(int iid, int iidFacturaIngreso, int iitem, int iidProducto, String sproducto, double ucantidad, int uprecio, int iidUnidadMedida, String sunidadMedida, int iidImpuesto, String simpuesto, double uexenta, double ugravada10, double ugravada5, String sreferencia, int iidOperacion, String stabla, double porcdto, double ventadto, boolean prestamo, short hestadoRegistro) {
        this.iid = iid;
        this.iidFacturaIngreso = iidFacturaIngreso;
        this.iitem = iitem;
        this.iidProducto = iidProducto;
        this.sproducto = sproducto;
        this.ucantidad = ucantidad;
        this.uprecio = uprecio;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.uexenta = uexenta;
        this.ugravada10 = ugravada10;
        this.ugravada5 = ugravada5;
        this.sreferencia = sreferencia;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.porcdto = porcdto;
        this.ventadto = ventadto;
        this.bprestamo = prestamo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidFacturaIngreso, int iitem, int iidProducto, String sproducto, double ucantidad, double uprecio, int iidUnidadMedida, String sunidadMedida, int iidImpuesto, String simpuesto, double uexenta, double ugravada10, double ugravada5, String sreferencia, int iidOperacion, String stabla, double porcdto, double ventadto, boolean prestamo, short hestadoRegistro) {
        this.iid = iid;
        this.iidFacturaIngreso = iidFacturaIngreso;
        this.iitem = iitem;
        this.iidProducto = iidProducto;
        this.sproducto = sproducto;
        this.ucantidad = ucantidad;
        this.uprecio = uprecio;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.porcdto = porcdto;
        this.ventadto = ventadto;
        this.uexenta = uexenta;
        this.ugravada10 = ugravada10;
        this.ugravada5 = ugravada5;
        this.sreferencia = sreferencia;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.bprestamo = prestamo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleFacturaIngreso copiar(entDetalleFacturaIngreso destino) {
        destino.setEntidad(this.getId(), this.getIdFacturaIngreso(), this.getItem(), this.getIdProducto(), this.getProducto(), this.getCantidad(), this.getPrecio(), this.getIdUnidadMedida(), this.getUnidadMedida(), this.getIdImpuesto(), this.getImpuesto(), this.getExenta(), this.getGravada10(), this.getGravada5(), this.getReferencia(), this.getIdOperacion(), this.getTabla(), this.getPorcDto(), this.getVentaDto(), this.isPrestamo(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleFacturaIngreso cargar(java.sql.ResultSet rs) {
        try {
            this.setId(rs.getInt("id"));
        } catch (Exception e) {
        }
        try {
            this.setIdFacturaIngreso(rs.getInt("idfacturaingreso"));
        } catch (Exception e) {
        }
        try {
            this.setItem(rs.getInt("item"));
        } catch (Exception e) {
        }
        try {
            this.setIdProducto(rs.getInt("idproducto"));
        } catch (Exception e) {
        }
        try {
            this.setProducto(rs.getString("producto"));
        } catch (Exception e) {
        }
        try {
            this.setCantidad(rs.getDouble("cantidad"));
        } catch (Exception e) {
        }
        try {
            this.setPrecio(rs.getDouble("precio"));
        } catch (Exception e) {
        }
        try {
            this.setPorcDto(rs.getDouble("porcdto"));
        } catch (Exception e) {
        }
        try {
            this.setVentaDto(rs.getDouble("ventadto"));
        } catch (Exception e) {
        }
        try {
            this.setIdUnidadMedida(rs.getInt("idunidadmedida"));
        } catch (Exception e) {
        }
        try {
            this.setUnidadMedida(rs.getString("unidadmedida"));
        } catch (Exception e) {
        }
        try {
            this.setIdImpuesto(rs.getInt("idimpuesto"));
        } catch (Exception e) {
        }
        try {
            this.setImpuesto(rs.getString("impuesto"));
        } catch (Exception e) {
        }
        try {
            this.setExenta(rs.getDouble("exenta"));
        } catch (Exception e) {
        }
        try {
            this.setGravada10(rs.getDouble("gravada10"));
        } catch (Exception e) {
        }
        try {
            this.setGravada5(rs.getDouble("gravada5"));
        } catch (Exception e) {
        }
        try {
            this.setReferencia(rs.getString("referencia"));
        } catch (Exception e) {
        }
        try {
            this.setPrestamo(rs.getBoolean("prestamo"));
        } catch (Exception e) {
        }
        try {
            this.setIdOperacion(rs.getInt("idoperacion"));
        } catch (Exception e) {
        }
        try {
            this.setTabla(rs.getString("tabla"));
        } catch (Exception e) {
        }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public boolean isPrestamo() {
        return bprestamo;
    }

    public void setPrestamo(boolean bprestamo) {
        this.bprestamo = bprestamo;
    }

    public void setIdFacturaIngreso(int iidFacturaIngreso) {
        this.iidFacturaIngreso = iidFacturaIngreso;
    }

    public void setItem(int iitem) {
        this.iitem = iitem;
    }

    public void setIdProducto(int iidProducto) {
        this.iidProducto = iidProducto;
    }

    public void setPorcDto(double porcentaje) {
        this.porcdto = porcentaje;
    }

    public void setVentaDto(double ventaDto) {
        this.ventadto = ventaDto;
    }

    public void setProducto(String sproducto) {
        this.sproducto = sproducto;
    }

    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setPrecio(double uprecio) {
        this.uprecio = uprecio;
    }

    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }

    public void setUnidadMedida(String sunidadMedida) {
        this.sunidadMedida = sunidadMedida;
    }

    public void setIdImpuesto(int iidImpuesto) {
        this.iidImpuesto = iidImpuesto;
    }

    public void setImpuesto(String simpuesto) {
        this.simpuesto = simpuesto;
    }

    public void setExenta(double uexenta) {
        this.uexenta = uexenta;
    }

    public void setGravada10(double ugravada10) {
        this.ugravada10 = ugravada10;
    }

    public void setGravada5(double ugravada5) {
        this.ugravada5 = ugravada5;
    }

    public void setReferencia(String sreferencia) {
        this.sreferencia = sreferencia;
    }

    public void setIdOperacion(int iidOperacion) {
        this.iidOperacion = iidOperacion;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdFacturaIngreso() {
        return this.iidFacturaIngreso;
    }

    public int getItem() {
        return this.iitem;
    }

    public int getIdProducto() {
        return this.iidProducto;
    }

    public String getProducto() {
        return this.sproducto;
    }

    public double getCantidad() {
        return this.ucantidad;
    }

    public double getPrecio() {
        return this.uprecio;
    }

    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }

    public String getUnidadMedida() {
        return this.sunidadMedida;
    }

    public int getIdImpuesto() {
        return this.iidImpuesto;
    }

    public String getImpuesto() {
        return this.simpuesto;
    }

    public Double getPorcDto() {
        return this.porcdto;
    }

    public Double getVentaDto() {
        return this.ventadto;
    }

    public double getExenta() {
        return this.uexenta;
    }

    public double getGravada10() {
        return this.ugravada10;
    }

    public double getGravada5() {
        return this.ugravada5;
    }

    public String getReferencia() {
        return this.sreferencia;
    }

    public int getIdOperacion() {
        return this.iidOperacion;
    }

    public String getTabla() {
        return this.stabla;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido(String detalleIva) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdProducto() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += TEXTO_PRODUCTO;
            bvalido = false;
        }
        if (this.getCantidad() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += TEXTO_CANTIDAD;
            bvalido = false;
        }
        if (this.getPrecio() < 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += TEXTO_PRECIO;
            bvalido = false;
        }
        if (this.getIdUnidadMedida() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += TEXTO_UNIDAD_MEDIDA;
            bvalido = false;
        }
        if (this.getIdImpuesto() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += TEXTO_IMPUESTO;
            bvalido = false;
        }
        if (detalleIva.equalsIgnoreCase("")) {
            if (this.getExenta() <= 0) {
                if (!this.smensaje.isEmpty()) {
                    this.smensaje += "\n";
                }
                this.smensaje += TEXTO_EXENTA;
                bvalido = false;
            }
            if (this.getGravada10() <= 0) {
                if (!this.smensaje.isEmpty()) {
                    this.smensaje += "\n";
                }
                this.smensaje += TEXTO_GRAVADA10;
                bvalido = false;
            }
            if (this.getGravada5() <= 0) {
                if (!this.smensaje.isEmpty()) {
                    this.smensaje += "\n";
                }
                this.smensaje += TEXTO_GRAVADA5;
                bvalido = false;
            }
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT detallefacturaingreso("
                + this.getId() + ","
                + this.getIdFacturaIngreso() + ","
                + this.getItem() + ","
                + this.getIdProducto() + ","
                + this.getCantidad() + ","
                + this.getPrecio() + ","
                + this.getIdUnidadMedida() + ","
                + this.getIdImpuesto() + ","
                + this.getExenta() + ","
                + this.getGravada10() + ","
                + this.getGravada5() + ","
                + utilitario.utiCadena.getTextoGuardado(this.getReferencia()) + ","
                + this.getIdOperacion() + ","
                + utilitario.utiCadena.getTextoGuardado(this.getTabla()) + ","
                + this.getPorcDto() + ","
                + this.getVentaDto() + ","
                + this.isPrestamo() + ","
                + this.getEstadoRegistro() + ")";
    }

}
