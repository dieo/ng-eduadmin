/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleRecibo {

    private int iid;
    private int iidRecibo;
    private int iitem;
    private int iidProducto;
    private String sproducto;
    private double ucantidad;
    private double uprecio;
    private int iidUnidadMedida;
    private String sunidadMedida;
    private double umonto;
    private String sreferencia;
    private int iidOperacion;
    private String stabla;
    private double uatraso;
    private double uactual;
    private double usaldo;
    private double uexoneracion;
    private double porcdto;
    private double ventadto;
    private short hestadoRegistro;
    private String smensaje;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ITEM = "Item (no editable)";
    public static final String TEXTO_PRODUCTO = "Producto (no vacía)";
    public static final String TEXTO_CANTIDAD = "Cantidad (no vacía, valor positivo)";
    public static final String TEXTO_PRECIO = "Precio (no vacío, valor positivo)";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de Medida (no vacía)";
    public static final String TEXTO_MONTO = "Monto (valor positivo)";

    public entDetalleRecibo() {
        this.iid = 0;
        this.iidRecibo = 0;
        this.iitem = 0;
        this.iidProducto = 0;
        this.sproducto = "";
        this.ucantidad = 0.0;
        this.uprecio = 0.0;
        this.iidUnidadMedida = 0;
        this.sunidadMedida = "";
        this.umonto = 0.0;
        this.sreferencia = "";
        this.iidOperacion = 0;
        this.stabla = "";
        this.uatraso = 0.0;
        this.uactual = 0.0;
        this.usaldo = 0.0;
        this.uexoneracion = 0.0;
        this.porcdto = 0.0;
        this.ventadto = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleRecibo(int iid, int iidRecibo, int iitem, int iidProducto, String sproducto, double ucantidad, double uprecio, int iidUnidadMedida, String sunidadMedida, double umonto, String sreferencia, int iidOperacion, String stabla, double uatraso, double uactual, double usaldo, double uexoneracion, double porcdto, double ventadto, short hestadoRegistro) {
        this.iid = iid;
        this.iidRecibo = iidRecibo;
        this.iitem = iitem;
        this.iidProducto = iidProducto;
        this.sproducto = sproducto;
        this.ucantidad = ucantidad;
        this.uprecio = uprecio;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.umonto = umonto;
        this.sreferencia = sreferencia;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.uatraso = uatraso;
        this.uactual = uactual;
        this.usaldo = usaldo;
        this.uexoneracion = uexoneracion;
        this.porcdto = porcdto;
        this.ventadto = ventadto;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidRecibo, int iitem, int iidProducto, String sproducto, double ucantidad, double uprecio, int iidUnidadMedida, String sunidadMedida, double umonto, String sreferencia, int iidOperacion, String stabla, double uatraso, double uactual, double usaldo, double uexoneracion, double porcdto, double ventadto, short hestadoRegistro) {
        this.iid = iid;
        this.iidRecibo = iidRecibo;
        this.iitem = iitem;
        this.iidProducto = iidProducto;
        this.sproducto = sproducto;
        this.ucantidad = ucantidad;
        this.uprecio = uprecio;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.umonto = umonto;
        this.sreferencia = sreferencia;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.stabla = stabla;
        this.uatraso = uatraso;
        this.uactual = uactual;
        this.porcdto = porcdto;
        this.ventadto = ventadto;
        this.usaldo = usaldo;
        this.uexoneracion = uexoneracion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleRecibo copiar(entDetalleRecibo destino) {
        destino.setEntidad(this.getId(), this.getIdRecibo(), this.getItem(), this.getIdProducto(), this.getProducto(), this.getCantidad(), this.getPrecio(), this.getIdUnidadMedida(), this.getUnidadMedida(), this.getMonto(), this.getReferencia(), this.getIdOperacion(), this.getTabla(), this.getAtraso(), this.getActual(), this.getSaldo(), this.getExoneracion(), this.getPorcDto(), this.getVentaDto(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleRecibo cargar(java.sql.ResultSet rs) {
        try {
            this.setId(rs.getInt("id"));
        } catch (Exception e) {
        }
        try {
            this.setIdRecibo(rs.getInt("idrecibo"));
        } catch (Exception e) {
        }
        try {
            this.setItem(rs.getInt("item"));
        } catch (Exception e) {
        }
        try {
            this.setIdProducto(rs.getInt("idproducto"));
        } catch (Exception e) {
        }
        try {
            this.setProducto(rs.getString("producto"));
        } catch (Exception e) {
        }
        try {
            this.setCantidad(rs.getDouble("cantidad"));
        } catch (Exception e) {
        }
        try {
            this.setPrecio(rs.getDouble("precio"));
        } catch (Exception e) {
        }
        try {
            this.setIdUnidadMedida(rs.getInt("idunidadmedida"));
        } catch (Exception e) {
        }
        try {
            this.setUnidadMedida(rs.getString("unidadmedida"));
        } catch (Exception e) {
        }
        try {
            this.setMonto(rs.getDouble("monto"));
        } catch (Exception e) {
        }
        try {
            this.setReferencia(rs.getString("referencia"));
        } catch (Exception e) {
        }
        try {
            this.setIdOperacion(rs.getInt("idoperacion"));
        } catch (Exception e) {
        }
        try {
            this.setTabla(rs.getString("tabla"));
        } catch (Exception e) {
        }
        try {
            this.setAtraso(rs.getDouble("atraso"));
        } catch (Exception e) {
        }
        try {
            this.setActual(rs.getDouble("actual"));
        } catch (Exception e) {
        }
        try {
            this.setSaldo(rs.getDouble("saldo"));
        } catch (Exception e) {
        }
        try {
            this.setExoneracion(rs.getDouble("exoneracion"));
        } catch (Exception e) {
        }
        try {
            this.setPorcDto(rs.getDouble("porcdto"));
        } catch (Exception e) {
        }
        try {
            this.setVentaDto(rs.getDouble("ventadto"));
        } catch (Exception e) {
        }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setPorcDto(double porcentaje) {
        this.porcdto = porcentaje;
    }

    public void setVentaDto(double ventaDto) {
        this.ventadto = ventaDto;
    }

    public void setIdRecibo(int iidRecibo) {
        this.iidRecibo = iidRecibo;
    }

    public void setItem(int iitem) {
        this.iitem = iitem;
    }

    public void setIdProducto(int iidProducto) {
        this.iidProducto = iidProducto;
    }

    public void setProducto(String sproducto) {
        this.sproducto = sproducto;
    }

    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setPrecio(double uprecio) {
        this.uprecio = uprecio;
    }

    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }

    public void setUnidadMedida(String sunidadMedida) {
        this.sunidadMedida = sunidadMedida;
    }

    public void setMonto(double umonto) {
        this.umonto = umonto;
    }

    public void setReferencia(String sreferencia) {
        this.sreferencia = sreferencia;
    }

    public void setIdOperacion(int iidOperacion) {
        this.iidOperacion = iidOperacion;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setAtraso(double uatraso) {
        this.uatraso = uatraso;
    }

    public void setActual(double uactual) {
        this.uactual = uactual;
    }

    public void setSaldo(double usaldo) {
        this.usaldo = usaldo;
    }

    public void setExoneracion(double uexoneracion) {
        this.uexoneracion = uexoneracion;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdRecibo() {
        return this.iidRecibo;
    }

    public int getItem() {
        return this.iitem;
    }

    public int getIdProducto() {
        return this.iidProducto;
    }

    public String getProducto() {
        return this.sproducto;
    }

    public double getCantidad() {
        return this.ucantidad;
    }

    public Double getPorcDto() {
        return this.porcdto;
    }

    public Double getVentaDto() {
        return this.ventadto;
    }

    public double getPrecio() {
        return this.uprecio;
    }

    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }

    public String getUnidadMedida() {
        return this.sunidadMedida;
    }

    public double getMonto() {
        return this.umonto;
    }

    public String getReferencia() {
        return this.sreferencia;
    }

    public int getIdOperacion() {
        return this.iidOperacion;
    }

    public String getTabla() {
        return this.stabla;
    }

    public double getAtraso() {
        return this.uatraso;
    }

    public double getActual() {
        return this.uactual;
    }

    public double getSaldo() {
        return this.usaldo;
    }

    public double getExoneracion() {
        return this.uexoneracion;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdProducto() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += TEXTO_PRODUCTO;
            bvalido = false;
        }
        if (this.getCantidad() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += TEXTO_CANTIDAD;
            bvalido = false;
        }
         if (this.getPrecio() < 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += TEXTO_PRECIO;
            bvalido = false;
        }
        if (this.getIdUnidadMedida() <= 0) {
            if (!this.smensaje.isEmpty()) {
                this.smensaje += "\n";
            }
            this.smensaje += TEXTO_UNIDAD_MEDIDA;
            bvalido = false;
        }
//        if (this.getMonto() <= 0) {
//            if (!this.smensaje.isEmpty()) {
//                this.smensaje += "\n";
//            }
//            this.smensaje += TEXTO_MONTO;
//            bvalido = false;
//        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT detallerecibo("
                + this.getId() + ","
                + this.getIdRecibo() + ","
                + this.getItem() + ","
                + this.getIdProducto() + ","
                + this.getCantidad() + ","
                + this.getPrecio() + ","
                + this.getIdUnidadMedida() + ","
                + this.getMonto() + ","
                + utilitario.utiCadena.getTextoGuardado(this.getReferencia()) + ","
                + this.getIdOperacion() + ","
                + utilitario.utiCadena.getTextoGuardado(this.getTabla()) + ","
                + this.getAtraso() + ","
                + this.getActual() + ","
                + this.getSaldo() + ","
                + this.getExoneracion() + ","
                + this.getPorcDto() + ","
                + this.getVentaDto() + ","
                + this.getEstadoRegistro() + ")";
    }

}
