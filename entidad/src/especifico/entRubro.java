/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entRubro extends generico.entGenerica{
    
    protected double ujubilacion;
    protected double uiva;
    protected String sdescripcionBreve;

    public static final int LONGITUD_DESCRIPCION = 40;
    public static final int LONGITUD_DESCRIPCION_BREVE = 1;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Rubro (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_JUBILACION = "Porcentaje de Jubilación del Rubro (valor positivo)";
    public static final String TEXTO_IVA = "IVA del Rubro (valor positivo)";
    public static final String TEXTO_DESCRIPCION_BREVE = "Descripción breve de Rubro (no vacía, hasta " + LONGITUD_DESCRIPCION_BREVE + " caracter)";
    
    public entRubro() {
        super();
        this.ujubilacion = 0.0;
        this.uiva = 0.0;
        this.sdescripcionBreve = "";
    }

    public entRubro(int iid, String sdescripcion, double ujubilacion, double uiva, String sdescripcionBreve, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.ujubilacion = ujubilacion;
        this.uiva = uiva;
        this.sdescripcionBreve = sdescripcionBreve;
    }

    public void setEntidad(int iid, String sdescripcion, double ujubilacion, double uiva, String sdescripcionBreve, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.ujubilacion = ujubilacion;
        this.uiva = uiva;
        this.sdescripcionBreve = sdescripcionBreve;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entRubro copiar(entRubro destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getJubilacion(), this.getIva(), this.getDescripcionBreve(), this.getEstadoRegistro());
        return destino;
    }
    
    public entRubro cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setJubilacion(rs.getDouble("jubilacion")); }
        catch(Exception e) {}
        try { this.setIva(rs.getDouble("iva")); }
        catch(Exception e) {}
        try { this.setDescripcionBreve(rs.getString("descripcionbreve")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setJubilacion(double ujubilacion) {
        this.ujubilacion = ujubilacion;
    }

    public void setIva(double uiva) {
        this.uiva = uiva;
    }

    public void setDescripcionBreve(String sdescripcionBreve) {
        this.sdescripcionBreve = sdescripcionBreve;
    }
    
    public double getJubilacion() {
        return this.ujubilacion;
    }

    public double getIva() {
        return this.uiva;
    }

    public String getDescripcionBreve() {
        return this.sdescripcionBreve;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getJubilacion()<0 || this.getJubilacion()>100) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_JUBILACION;
            bvalido = false;
        }
        if (this.getIva()<0 || this.getIva()>100) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IVA;
            bvalido = false;
        }
        if (this.getDescripcionBreve().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION_BREVE;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT rubro("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getJubilacion()+","+
            this.getIva()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcionBreve())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Jubilación", "jubilacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(3, "Iva", "iva", generico.entLista.tipo_numero));
        return lst;
    }
    
}
