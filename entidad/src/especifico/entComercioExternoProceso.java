/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entComercioExternoProceso {
    
    private String sarchivo;
    private String sruta;
    private boolean bverificado;
    private java.util.Date dfecha;
    
    private String smensaje;

    public entComercioExternoProceso() {
        this.sarchivo = "";
        this.sruta = "";
        this.bverificado = false;
        this.dfecha = null;
        this.smensaje = "";
    }

    public entComercioExternoProceso(String sarchivo, String sruta, boolean bverificado, java.util.Date dfecha) {
        this.sarchivo = sarchivo;
        this.sruta = sruta;
        this.bverificado = bverificado;
        this.dfecha = (java.util.Date)dfecha.clone();
        this.smensaje = "";
    }
    
    public void setEntidad(String sarchivo, String sruta, boolean bverificado, java.util.Date dfecha) {
        this.sarchivo = sarchivo;
        this.sruta = sruta;
        this.bverificado = bverificado;
        this.dfecha = (java.util.Date)dfecha.clone();
        this.smensaje = "";
    }

    public entComercioExternoProceso copiar(entComercioExternoProceso destino) {
        destino.setEntidad(this.getArchivo(), this.getRuta(), this.getVerificado(), this.getFecha());
        return destino;
    }

    public void setArchivo(String sarchivo) {
        this.sarchivo = sarchivo;
    }
    
    public void setRuta(String sruta) {
        this.sruta = sruta;
    }
    
    public void setVerificado(boolean bverificado) {
        this.bverificado = bverificado;
    }
    
    public void setFecha(java.util.Date dfecha) {
        this.dfecha = (java.util.Date)dfecha.clone();
    }
    
    public String getArchivo() {
        return this.sarchivo;
    }
    
    public String getRuta() {
        return this.sruta;
    }
    
    public boolean getVerificado() {
        return this.bverificado;
    }
    
    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getArchivo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Archivo debe contener valor";
            bvalido = false;
        }
        if (!this.archivoValido()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Archivo debe ser del tipo 'xls'";
            bvalido = false;
        }
        return bvalido;
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

    private boolean archivoValido() {
        if (this.getArchivo().substring(this.getArchivo().length()-4, this.getArchivo().length()).toUpperCase().equals(".XLS")) return true;
        return false;
    }
    
    public void separarRutaArchivo(String srutaArchivo) {
        int i=srutaArchivo.length();
        while (!srutaArchivo.substring(i-1, i).equals("\\")) i--;
        this.sruta = srutaArchivo.substring(0, i);
        this.sarchivo = srutaArchivo.substring(i, srutaArchivo.length());
    }
    
}
