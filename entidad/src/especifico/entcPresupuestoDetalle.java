/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entcPresupuestoDetalle {
    
    protected int iid;
    protected int iidPresupuesto;
    protected int iidArticulo;
    protected String sdescripcion;
    protected double ucantidad;
    protected double ucosto;
    protected double utotal;
    protected short cestado;
    protected String smensaje;
    
    public static final int LONGITUD_DESCRIPCION = 200;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción del producto (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_TOTAL = "Monto Total (no editable)";
    public static final String TEXTO_ARTICULO = "Producto solicitado (no vacía)";
    public static final String TEXTO_CANTIDAD = "Cantidad olicitada del Producto (no vacía, valor positivo)";
    public static final String TEXTO_COSTO = "Costo unitario del Producto solicitado (no vacío, valor positivo)";
    
    public entcPresupuestoDetalle() {
        this.iid = 0;
        this.iidPresupuesto = 0;
        this.utotal = 0.0;
        this.iidArticulo = 0;
        this.sdescripcion = "";
        this.ucantidad = 0.0;
        this.ucosto = 0.0;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entcPresupuestoDetalle(int iid, int iidPresupuesto, double utotal, int iidArticulo, String sdescripcion, double ucantidad, double ucosto, short cestado) {
        this.iid = iid;
        this.iidPresupuesto = iidPresupuesto;
        this.utotal = utotal;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.ucantidad = ucantidad;
        this.ucosto = ucosto;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidPresupuesto, double utotal, int iidArticulo, String sdescripcion, double ucantidad, double ucosto, short cestado) {
        this.iid = iid;
        this.iidPresupuesto = iidPresupuesto;
        this.utotal = utotal;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.ucantidad = ucantidad;
        this.ucosto = ucosto;
        this.cestado = cestado;
    }

    public entcPresupuestoDetalle copiar(entcPresupuestoDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdPresupuesto(), this.getTotal(), this.getIdArticulo(), this.getDescripcion(), this.getCantidad(), this.getCosto(),  this.getEstadoRegistro());
        return destino;
    }
    
    public entcPresupuestoDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(SQLException e) { }
        try { this.setIdPresupuesto(rs.getInt("idpresupuesto")); }
        catch(SQLException e) { }
        try { this.setIdArticulo(rs.getInt("idarticulo")); }
        catch(SQLException e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(SQLException e) { }
        try { this.setCantidad(rs.getDouble("cantidad")); }
        catch(SQLException e) { }
        try { this.setCosto(rs.getDouble("costo")); }
        catch(SQLException e) { }
        try { this.setTotal(rs.getDouble("total")); }
        catch(SQLException e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdPresupuesto(int iidPresupuesto) {
        this.iidPresupuesto = iidPresupuesto;
    }

    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setCosto(double ucosto) {
        this.ucosto = ucosto;
    }

    public void setTotal(double utotal) {
        this.utotal = utotal;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdPresupuesto() {
        return this.iidPresupuesto;
    }
        
    public int getIdArticulo() {
        return this.iidArticulo;
    }
   
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public double getCantidad() {
        return this.ucantidad;
    }

    public double getCosto() {
        return this.ucosto;
    }

    public double getTotal() {
        return this.utotal;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdArticulo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_ARTICULO;
            bvalido = false;
        }
        if ("".equals(this.getDescripcion())) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if ((this.getCantidad()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CANTIDAD;
            bvalido = false;
        }
        if ((this.getCosto()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_COSTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.presupuestodetalle("+
            this.getId()+","+
            this.getIdPresupuesto()+","+
            this.getCantidad()+","+
            this.getIdArticulo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getCosto()+","+
            this.getTotal()+","+
            this.getEstadoRegistro()+")";
    }

}
