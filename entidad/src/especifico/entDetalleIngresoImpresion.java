/*0
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleIngresoImpresion {
    
    private String sinforme;
    private String sruc;
    private String srazonSocial;
    private int iidIngreso;
    private int inumeroOperacion;
    private java.util.Date dfechaOperacion;
    private String sestado;
    private double uimporteTotal;
    private String scuenta;
    private String sdocumento;
    private String snumeroDocumento;
    private String sgrupo;
    private double uimporte;
    private String svalor;
    
    private int iidPlanilla;
    private int inumeroPlanilla;
    private String splanilla;
    private String sfuncionario;
    private String ssucursal;
    private double umontoInicial;
    private String sapertura;
    private String scierre;
    
    public entDetalleIngresoImpresion() {
        this.sinforme = "";
        this.sruc = "";
        this.srazonSocial = "";
        this.iidIngreso = 0;
        this.inumeroOperacion = 0;
        this.dfechaOperacion = null;
        this.sestado = "";
        this.uimporteTotal = 0.0;
        this.scuenta = "";
        this.sdocumento = "";
        this.snumeroDocumento = "";
        this.sgrupo = "";
        this.uimporte = 0.0;
        this.svalor = "";
        
        this.iidPlanilla = 0;
        this.inumeroPlanilla = 0;
        this.splanilla = "";
        this.sfuncionario = "";
        this.ssucursal = "";
        this.umontoInicial = 0.0;
        this.sapertura = "";
        this.scierre = "";
    }

    public entDetalleIngresoImpresion(String sinforme, String sruc, String srazonSocial, int iidIngreso, int inumeroOperacion, java.util.Date dfechaOperacion, String sestado, double uimporteTotal, String scuenta, String sdocumento, String snumeroDocumento, String sgrupo, double uimporte, String svalor, int iidPlanilla, int inumeroPlanilla, String splanilla, String sfuncionario, String ssucursal, double umontoInicial, String sapertura, String scierre) {
        this.sinforme = sinforme;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.iidIngreso = iidIngreso;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaOperacion = dfechaOperacion;
        this.sestado = sestado;
        this.uimporteTotal = uimporteTotal;
        this.scuenta = scuenta;
        this.sdocumento = sdocumento;
        this.snumeroDocumento = snumeroDocumento;
        this.sgrupo = sgrupo;
        this.uimporte = uimporte;
        this.svalor = svalor;

        this.iidPlanilla = iidPlanilla;
        this.inumeroPlanilla = inumeroPlanilla;
        this.splanilla = splanilla;
        this.sfuncionario = sfuncionario;
        this.ssucursal = ssucursal;
        this.umontoInicial = umontoInicial;
        this.sapertura = sapertura;
        this.scierre = scierre;
    }

    public void setEntidad(String sinforme, String sruc, String srazonSocial, int iidIngreso, int inumeroOperacion, java.util.Date dfechaOperacion, String sestado, double uimporteTotal, String scuenta, String sdocumento, String snumeroDocumento, String sgrupo, double uimporte, String svalor, int iidPlanilla, int inumeroPlanilla, String splanilla, String sfuncionario, String ssucursal, double umontoInicial, String sapertura, String scierre) {
        this.sinforme = sinforme;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.iidIngreso = iidIngreso;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaOperacion = dfechaOperacion;
        this.sestado = sestado;
        this.uimporteTotal = uimporteTotal;
        this.scuenta = scuenta;
        this.sdocumento = sdocumento;
        this.snumeroDocumento = snumeroDocumento;
        this.sgrupo = sgrupo;
        this.uimporte = uimporte;
        this.svalor = svalor;

        this.iidPlanilla = iidPlanilla;
        this.inumeroPlanilla = inumeroPlanilla;
        this.splanilla = splanilla;
        this.sfuncionario = sfuncionario;
        this.ssucursal = ssucursal;
        this.umontoInicial = umontoInicial;
        this.sapertura = sapertura;
        this.scierre = scierre;
    }

    public entDetalleIngresoImpresion copiar(entDetalleIngresoImpresion destino) {
        destino.setEntidad(this.getInforme(), this.getRuc(), this.getRazonSocial(), this.getIdIngreso(), this.getNumeroOperacion(), this.getFechaOperacion(), this.getEstado(), this.getImporteTotal(), this.getCuenta(), this.getDocumento(), this.getNumeroDocumento(), this.getGrupo(), this.getImporte(), this.getValor(), this.getIdPlanilla(), this.getNumeroPlanilla(), this.getPlanilla(), this.getFuncionario(), this.getSucursal(), this.getMontoInicial(), this.getApertura(), this.getCierre());
        return destino;
    }

    public entDetalleIngresoImpresion cargar(java.sql.ResultSet rs) {
        try { this.setInforme(rs.getString("informe")); }
        catch(Exception e) {}
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) {}
        try { this.setRazonSocial(rs.getString("razonsocial")); }
        catch (Exception e) {}
        try { this.setIdIngreso(rs.getInt("idingreso")); }
        catch (Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch (Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch (Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch (Exception e) {}
        try { this.setImporteTotal(rs.getDouble("importetotal")); }
        catch (Exception e) {}
        try { this.setDocumento(rs.getString("documento")); }
        catch (Exception e) {}
        try { this.setNumeroDocumento(utilitario.utiCadena.getNumeroDocumento(rs.getString("local"), rs.getString("puntoexpedicion"), rs.getInt("numero"))); }
        catch (Exception e) {}
        try { this.setGrupo(rs.getString("grupo")); }
        catch (Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch (Exception e) {}
        try { this.setImporte(rs.getDouble("importe")); }
        catch (Exception e) {}
        try { this.setValor(rs.getString("tipovalor")); }
        catch (Exception e) {}

        try { this.setIdPlanilla(rs.getInt("id")); }
        catch (Exception e) {}
        try { this.setNumeroPlanilla(rs.getInt("numeroplanilla")); }
        catch (Exception e) {}
        try { this.setPlanilla(rs.getString("descripcion")); }
        catch (Exception e) {}
        try { this.setFuncionario(rs.getString("funcionario")); }
        catch (Exception e) {}
        try { this.setSucursal(rs.getString("sucursal")); }
        catch (Exception e) {}
        try { this.setMontoInicial(rs.getDouble("montoinicial")); }
        catch (Exception e) {}
        try { this.setApertura(utilitario.utiFecha.convertirToStringDMA(rs.getDate("fechaapertura"))+" "+rs.getString("horaapertura").substring(0,5)); }
        catch (Exception e) {}
        try { this.setCierre(utilitario.utiFecha.convertirToStringDMA(rs.getDate("fechacierre"))+" "+rs.getString("horacierre").substring(0,5)); }
        catch (Exception e) {}
        return this;
    }
    
    public void setInforme(String sinforme) {
        this.sinforme = sinforme;
    }

    public void setRuc(String sruc) {
        this.sruc = sruc;
    }

    public void setRazonSocial(String srazonSocial) {
        this.srazonSocial = srazonSocial;
    }

    public void setIdIngreso(int iidIngreso) {
        this.iidIngreso = iidIngreso;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setImporteTotal(double uimporteTotal) {
        this.uimporteTotal = uimporteTotal;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setDocumento(String sdocumento) {
        this.sdocumento = sdocumento;
    }

    public void setNumeroDocumento(String snumeroDocumento) {
        this.snumeroDocumento = snumeroDocumento;
    }

    public void setGrupo(String sgrupo) {
        this.sgrupo = sgrupo;
    }

    public void setImporte(double uimporte) {
        this.uimporte = uimporte;
    }

    public void setValor(String svalor) {
        this.svalor = svalor;
    }

    public void setIdPlanilla(int iidPlanilla) {
        this.iidPlanilla = iidPlanilla;
    }

    public void setNumeroPlanilla(int inumeroPlanilla) {
        this.inumeroPlanilla = inumeroPlanilla;
    }

    public void setPlanilla(String splanilla) {
        this.splanilla = splanilla;
    }

    public void setFuncionario(String sfuncionario) {
        this.sfuncionario = sfuncionario;
    }

    public void setSucursal(String ssucursal) {
        this.ssucursal = ssucursal;
    }

    public void setMontoInicial(double umontoInicial) {
        this.umontoInicial = umontoInicial;
    }

    public void setApertura(String sapertura) {
        this.sapertura = sapertura;
    }

    public void setCierre(String scierre) {
        this.scierre = scierre;
    }
    
    public String getInforme() {
        return this.sinforme;
    }

    public String getRuc() {
        return this.sruc;
    }

    public String getRazonSocial() {
        return this.srazonSocial;
    }

    public int getIdIngreso() {
        return this.iidIngreso;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public String getEstado() {
        return this.sestado;
    }

    public double getImporteTotal() {
        return this.uimporteTotal;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getDocumento() {
        return this.sdocumento;
    }

    public String getNumeroDocumento() {
        return this.snumeroDocumento;
    }

    public String getGrupo() {
        return this.sgrupo;
    }

    public double getImporte() {
        return this.uimporte;
    }

    public String getValor() {
        return this.svalor;
    }

    public int getIdPlanilla() {
        return this.iidPlanilla;
    }

    public int getNumeroPlanilla() {
        return this.inumeroPlanilla;
    }

    public String getPlanilla() {
        return this.splanilla;
    }

    public String getFuncionario() {
        return this.sfuncionario;
    }

    public String getSucursal() {
        return this.ssucursal;
    }

    public double getMontoInicial() {
        return this.umontoInicial;
    }

    public String getApertura() {
        return this.sapertura;
    }

    public String getCierre() {
        return this.scierre;
    }

}
