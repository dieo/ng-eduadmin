/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entEstadoCivil extends generico.entGenerica {

    public static final int LONGITUD_DESCRIPCION = 30;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Estado Civil (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";

    public entEstadoCivil() {
        super();
    }

    public entEstadoCivil(int iid, String sdescripcion, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
    }

    public void setEntidad(int iid, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entEstadoCivil copiar(entEstadoCivil destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entEstadoCivil cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        return this;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT estadocivil("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }

}