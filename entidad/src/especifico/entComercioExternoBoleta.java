/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entComercioExternoBoleta {

    private int iid;
    private int iidPeriodo;
    private String speriodo;
    private String sboleta;
    private java.util.Date dfechaBoleta;
    private int iidFuncionario;
    private String scedula;
    private String sapellidoNombre;
    private java.util.Date dfechaOperacion;
    private int iidComercio;
    private String scomercio;
    private double uimporteTotal;
    private int iidMoneda;
    private String smoneda;
    private int iplazo;
    private String sconcepto;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_BOLETA = 20;
    public static final int LONGITUD_CONCEPTO = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_PERIODO = "Período (no editable)";
    public static final String TEXTO_BOLETA = "Boleta (no vacía, hasta " + LONGITUD_BOLETA + " caracteres)";
    public static final String TEXTO_FECHA_BOLETA = "Fecha de Boleta (no vacía)";
    public static final String TEXTO_FUNCIONARIO = "Funcionario (no vacío)";
    public static final String TEXTO_FECHA_OPERACION = "Fecha de Operación (no vacía)";
    public static final String TEXTO_COMERCIO = "Comercio (no vacío)";
    public static final String TEXTO_IMPORTE_TOTAL = "Importe Total de la Boleta (valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacío)";
    public static final String TEXTO_PLAZO = "Plazo (no editable)";
    public static final String TEXTO_CONCEPTO = "Concepto (no vacío, hasta " + LONGITUD_CONCEPTO + " caracteres)";

    public entComercioExternoBoleta() {
        this.iid = 0;
        this.iidPeriodo = 0;
        this.speriodo = "";
        this.sboleta = "";
        this.dfechaBoleta = null;
        this.iidFuncionario = 0;
        this.scedula = "";
        this.sapellidoNombre = "";
        this.dfechaOperacion = null;
        this.iidComercio = 0;
        this.scomercio = "";
        this.uimporteTotal = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iplazo = 0;
        this.sconcepto = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entComercioExternoBoleta(int iid, int iidPeriodo, String speriodo, String sboleta, java.util.Date dfechaBoleta, int iidFuncionario, String scedula, String sapellidoNombre, java.util.Date dfechaOperacion, int iidComercio, String scomercio, double uimporteTotal, int iidMoneda, String smoneda, int iplazo, String sconcepto, short hestadoRegistro) {
        this.iid = iid;
        this.iidPeriodo = iidPeriodo;
        this.speriodo = speriodo;
        this.sboleta = sboleta;
        this.dfechaBoleta = dfechaBoleta;
        this.iidFuncionario = iidFuncionario;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.dfechaOperacion = dfechaOperacion;
        this.iidComercio = iidComercio;
        this.scomercio = scomercio;
        this.uimporteTotal = uimporteTotal;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iplazo = iplazo;
        this.sconcepto = sconcepto;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidPeriodo, String speriodo, String sboleta, java.util.Date dfechaBoleta, int iidFuncionario, String scedula, String sapellidoNombre, java.util.Date dfechaOperacion, int iidComercio, String scomercio, double uimporteTotal, int iidMoneda, String smoneda, int iplazo, String sconcepto, short hestadoRegistro) {
        this.iid = iid;
        this.iidPeriodo = iidPeriodo;
        this.speriodo = speriodo;
        this.sboleta = sboleta;
        this.dfechaBoleta = dfechaBoleta;
        this.iidFuncionario = iidFuncionario;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.dfechaOperacion = dfechaOperacion;
        this.iidComercio = iidComercio;
        this.scomercio = scomercio;
        this.uimporteTotal = uimporteTotal;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iplazo = iplazo;
        this.sconcepto = sconcepto;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entComercioExternoBoleta copiar(entComercioExternoBoleta destino) {
        destino.setEntidad(this.getId(), this.getIdPeriodo(), this.getPeriodo(), this.getBoleta(), this.getFechaBoleta(), this.getIdFuncionario(), this.getCedula(), this.getApellidoNombre(), this.getFechaOperacion(), this.getIdComercio(), this.getComercio(), this.getImporteTotal(), this.getIdMoneda(), this.getMoneda(), this.getPlazo(), this.getConcepto(), this.getEstadoRegistro());
        return destino;
    }
    
    public entComercioExternoBoleta cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdPeriodo(rs.getInt("idperiodo")); }
        catch(Exception e) {}
        try { this.setPeriodo(rs.getString("periodo")); }
        catch(Exception e) {}
        try { this.setBoleta(rs.getString("boleta")); }
        catch(Exception e) {}
        try { this.setFechaBoleta(rs.getDate("fechaboleta")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setApellidoNombre(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setIdComercio(rs.getInt("idcomercio")); }
        catch(Exception e) {}
        try { this.setComercio(rs.getString("comercio")); }
        catch(Exception e) {}
        try { this.setImporteTotal(rs.getDouble("importetotal")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setConcepto(rs.getString("concepto")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdPeriodo(int iidPeriodo) {
        this.iidPeriodo = iidPeriodo;
    }

    public void setPeriodo(String speriodo) {
        this.speriodo = speriodo;
    }

    public void setBoleta(String sboleta) {
        this.sboleta = sboleta;
    }

    public void setFechaBoleta(java.util.Date dfechaBoleta) {
        this.dfechaBoleta = dfechaBoleta;
    }

    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setApellidoNombre(String sapellidoNombre) {
        this.sapellidoNombre = sapellidoNombre;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setIdComercio(int iidComercio) {
        this.iidComercio = iidComercio;
    }

    public void setComercio(String scomercio) {
        this.scomercio = scomercio;
    }

    public void setImporteTotal(double uimporteTotal) {
        this.uimporteTotal = uimporteTotal;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setConcepto(String sconcepto) {
        this.sconcepto = sconcepto;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdPeriodo() {
        return this.iidPeriodo;
    }

    public String getPeriodo() {
        return this.speriodo;
    }

    public String getBoleta() {
        return this.sboleta;
    }

    public java.util.Date getFechaBoleta() {
        return this.dfechaBoleta;
    }

    public int getIdFuncionario() {
        return this.iidFuncionario;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getApellidoNombre() {
        return this.sapellidoNombre;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public int getIdComercio() {
        return this.iidComercio;
    }

    public String getComercio() {
        return this.scomercio;
    }

    public double getImporteTotal() {
        return this.uimporteTotal;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public String getConcepto() {
        return this.sconcepto;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getBoleta().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_BOLETA;
            bvalido = false;
        }
        if (this.getFechaBoleta()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_BOLETA;
            bvalido = false;
        }
        if (this.getIdFuncionario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO;
            bvalido = false;
        }
        if (this.getFechaOperacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getIdComercio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_COMERCIO;
            bvalido = false;
        }
        if (this.getImporteTotal()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPORTE_TOTAL;
            bvalido = false;
        }
        if (this.getFechaOperacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getPlazo()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO;
            bvalido = false;
        }
        if (this.getConcepto().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CONCEPTO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT boleta("+
            this.getId()+","+
            this.getIdPeriodo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getBoleta())+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaBoleta())+","+
            this.getIdFuncionario()+","+
            utilitario.utiFecha.getFechaGuardadoMac(this.getFechaOperacion())+","+
            this.getIdComercio()+","+
            this.getImporteTotal()+","+
            this.getIdMoneda()+","+
            this.getPlazo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getConcepto())+","+
            this.getEstadoRegistro()+")";
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Período", "periodo", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Número Boleta", "numeroboleta", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(3, "Fecha Boleta", "fechaboleta", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(4, "Apellido y Nombre", "apellidonombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Fecha Operación", "fechaoperacion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(6, "Comercio", "comercio", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Importe Total", "importetotal", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(8, "Plazo", "plazo", generico.entLista.tipo_numero));
        return lst;
    }

}
