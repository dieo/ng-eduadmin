/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entbOrdenPagoDetalle {
    
    protected int iid;
    protected int iidOrdenPago;
    protected String sdescripcion;
    protected int iidPlanCuenta;
    protected String snroPlanCuenta;
    protected String snroPlanCuenta2;
    protected String splancuenta;
    protected double dmonto;
    protected short cestado;
    protected String smensaje;
    
    public static final int LONGITUD_DESCRIPCION = 100;
        
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_PLANCUENTA = "Plan de cuenta (no vacía)";
    public static final String TEXTO_MONTO = "Monto (no vacío)";
    
    public entbOrdenPagoDetalle() {
        this.iid = 0;
        this.iidOrdenPago = 0;
        this.sdescripcion = "";
        this.iidPlanCuenta = 0;
        this.snroPlanCuenta = "";
        this.snroPlanCuenta2 = "";
        this.splancuenta = "";
        this.dmonto = 0.0;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entbOrdenPagoDetalle(int iid, int iidOrdenPago, String sdescripcion, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta2, String splancuenta, double dmonto, short cestado) {
        this.iid = iid;
        this.iidOrdenPago = iidOrdenPago;
        this.sdescripcion = sdescripcion;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta2 = snroPlanCuenta2;
        this.splancuenta = splancuenta;
        this.dmonto = dmonto;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidOrdenPago, String sdescripcion, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta2, String splancuenta, double dmonto, short cestado) {
        this.iid = iid;
        this.iidOrdenPago = iidOrdenPago;
        this.sdescripcion = sdescripcion;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta2 = snroPlanCuenta2;
        this.splancuenta = splancuenta;
        this.dmonto = dmonto;
        this.cestado = cestado;
    }

    public entbOrdenPagoDetalle copiar(entbOrdenPagoDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdOrdenPago(), this.getDescripcion(), this.getIdPlanCuenta(), this.getNroPlanCuenta(), this.getNroPlanCuenta2(), this.getPlanCuenta(), this.getMonto(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbOrdenPagoDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(SQLException e) { }
        try { this.setIdOrdenPago(rs.getInt("idordenpago")); }
        catch(SQLException e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(SQLException e) { }
        try { this.setMonto(rs.getDouble("monto")); }
        catch(SQLException e) { }
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(SQLException e) { }
        try { this.setNroPlanCuenta(rs.getString("nroplancuenta")); }
        catch(SQLException e) { }
        try { this.setNroPlanCuenta2(rs.getString("nroplancuenta2")); }
        catch(SQLException e) { }
        try { this.setPlanCuenta(rs.getString("plancuenta")); }
        catch(SQLException e) { }
        //this.setNroPlanCuenta(""); 
        //this.setPlanCuenta(""); 
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdOrdenPago(int iidOrdenPago) {
        this.iidOrdenPago = iidOrdenPago;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }

    public void setNroPlanCuenta(String snroPlanCuenta) {
        this.snroPlanCuenta = snroPlanCuenta;
    }

    public void setNroPlanCuenta2(String snroPlanCuenta2) {
        this.snroPlanCuenta2 = snroPlanCuenta2;
    }
    
    public void setPlanCuenta(String splancuenta) {
        this.splancuenta = splancuenta;
    }
    
    public void setMonto(double dmonto) {
        this.dmonto = dmonto;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdOrdenPago() {
        return this.iidOrdenPago;
    }
    
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }

    public String getNroPlanCuenta() {
        return this.snroPlanCuenta;
    }

    public String getNroPlanCuenta2() {
        return this.snroPlanCuenta2;
    }
    
    public String getPlanCuenta() {
        return this.splancuenta;
    }
    
    public double getMonto() {
        return this.dmonto;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public String getCampoConcatenado() {
        return this.getId()+" "+this.getIdOrdenPago()+" "+this.getDescripcion()+" "+this.getIdPlanCuenta()+" "+this.getNroPlanCuenta()+" "+this.getPlanCuenta()+" "+this.getMonto();
    }
    
    public boolean esValido(boolean esPrestamo, double dmontoretirar) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdPlanCuenta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_PLANCUENTA;
            bvalido = false;
        }
        if ((this.getMonto()<=0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO;
            bvalido = false;
        }
        if(esPrestamo) {
            if ((this.getMonto()>dmontoretirar)) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Monto ingresado es mayor al saldo disponible";
                bvalido = false;
            }
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.detalleordenpago("+
            this.getId()+","+
            this.getIdOrdenPago()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdPlanCuenta()+","+
            this.getMonto()+","+
            this.getEstadoRegistro()+")";
    }

}
