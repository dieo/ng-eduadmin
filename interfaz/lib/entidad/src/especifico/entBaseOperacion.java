/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entBaseOperacion {

    private int iid;
    private int inumeroOperacion;
    private java.util.Date dfecha;
    private int inumeroSolicitud;
    private String scedula;
    private String snombre;
    private String sapellido;
    private double umonto;
    private String scheque;
    private int iidBanco;
    private String sbanco;
    private int iidProcedencia;
    private String sprocedencia;
    private String sdescripcion;

    private short hestadoRegistro;
    private String smensaje;

    public entBaseOperacion() {
        this.iid = 0;
        this.inumeroOperacion = 0;
        this.dfecha = null;
        this.inumeroSolicitud = 0;
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.umonto = 0.0;
        this.scheque = "";
        this.iidBanco = 0;
        this.sbanco = "";
        this.iidProcedencia = 0;
        this.sprocedencia = "";
        this.sdescripcion = "";
       this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entBaseOperacion(int iid, int inumeroOperacion, java.util.Date dfecha, int inumeroSolicitud, String scedula, String snombre, String sapellido, double umonto, String scheque, int iidBanco, String sbanco, int iidProcedencia, String sprocedencia, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOperacion = inumeroOperacion;
        this.dfecha = dfecha;
        this.inumeroSolicitud = inumeroSolicitud;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.umonto = umonto;
        this.scheque = scheque;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidProcedencia = iidProcedencia;
        this.sprocedencia = sprocedencia;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroOperacion, java.util.Date dfecha, int inumeroSolicitud, String scedula, String snombre, String sapellido, double umonto, String scheque, int iidBanco, String sbanco, int iidProcedencia, String sprocedencia, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOperacion = inumeroOperacion;
        this.dfecha = dfecha;
        this.inumeroSolicitud = inumeroSolicitud;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.umonto = umonto;
        this.scheque = scheque;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidProcedencia = iidProcedencia;
        this.sprocedencia = sprocedencia;
        this.sdescripcion = sdescripcion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entBaseOperacion copiar(entBaseOperacion destino) {
        destino.setEntidad(this.getId(), this.getNumeroOperacion(), this.getFecha(), this.getNumeroSolicitud(), this.getCedula(), this.getNombre(), this.getApellido(), this.getMonto(), this.getCheque(), this.getIdBanco(), this.getBanco(), this.getIdProcedencia(), this.getProcedencia(), this.getDescripcion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entBaseOperacion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        try { this.setCheque(rs.getString("cheque")); }
        catch(Exception e) {}
        try { this.setIdBanco(rs.getInt("idbanco")); }
        catch(Exception e) {}
        try { this.setBanco(rs.getString("banco")); }
        catch(Exception e) {}
        try { this.setIdProcedencia(rs.getInt("idprocedencia")); }
        catch(Exception e) {}
        try { this.setProcedencia(rs.getString("procedencia")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombre(String snombre) {
        this.snombre = snombre;
    }

    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }

    public void setMonto(double umonto) {
        this.umonto = umonto;
    }

    public void setCheque(String scheque) {
        this.scheque = scheque;
    }

    public void setIdBanco(int iidBanco) {
        this.iidBanco = iidBanco;
    }

    public void setBanco(String sbanco) {
        this.sbanco = sbanco;
    }

    public void setIdProcedencia(int iidProcedencia) {
        this.iidProcedencia = iidProcedencia;
    }

    public void setProcedencia(String sprocedencia) {
        this.sprocedencia = sprocedencia;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombre() {
        return this.snombre;
    }

    public String getApellido() {
        return this.sapellido;
    }

    public double getMonto() {
        return this.umonto;
    }

    public String getCheque() {
        return this.scheque;
    }

    public int getIdBanco() {
        return this.iidBanco;
    }

    public String getBanco() {
        return this.sbanco;
    }

    public int getIdProcedencia() {
        return this.iidProcedencia;
    }

    public String getProcedencia() {
        return this.sprocedencia;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public String getSentencia() {
        return "SELECT baseoperacion("+
            this.getId()+","+
            this.getNumeroOperacion()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFecha())+","+
            this.getNumeroSolicitud()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            this.getMonto()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCheque())+","+
            this.getIdBanco()+","+
            utilitario.utiCadena.getTextoGuardado(this.getBanco())+","+
            this.getIdProcedencia()+","+
            utilitario.utiCadena.getTextoGuardado(this.getProcedencia())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }

}
