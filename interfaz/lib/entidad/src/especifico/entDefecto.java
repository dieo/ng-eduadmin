/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDefecto {

    protected int iid;
    protected int iidBarrio;
    protected int iidCargo;
    protected int iidCiudad;
    protected int iidDepartamento;
    protected int iidEstadoCivil;
    protected int iidGiraduria;
    protected int iidMoneda;
    protected int iidPais;
    protected int iidParentesco;
    protected int iidRamo;
    protected int iidRubro;
    private double uaporteCapital;
    private double uaporteCuotaSocial;
    private double uaporteSolidaridad;
    private double ucuotaIngreso;
    private double uimporteSeguroTitular;
    private double uimporteSeguroAdherente;
    private int iidPromotor;
    private int iidUnidadMedida;
    private int iidRegional;
    private int iidEmisorTalonario;
    private int iidSucursal;
    private int iidTipoCredito;
    private int iidTipoValor;
    private int iidProfesion;
    private double uretencionOrdenCredito;
    private double uretencionPrestamoMutual;
    private double uretencionPrestamoFinanciera;
    private double utasaInteresMoratorio;
    private double utasaInteresPunitorio;
    private int icantidadDiaGracia;
    private int iidClaseProducto;
    private int iidEstadoAhorroProgramado;
    private int iidEstadoBeneficiario;
    private int iidEstadoEventoFormulario;
    private int iidEstadoFactura;
    private int iidEstadoFuncionario;
    private int iidEstadoIngreso;
    private int iidEstadoOperacionFija;
    private int iidEstadoOrdenCredito;
    private int iidEstadoOrdenPago;
    private int iidEstadoFondoJuridico;
    private int iidEstadoSocio;
    private int iidEstadoSolidaridad;
    private int iidEstadoTalonario;
    private int iidNivelAprobacion;
    private int iidNivelUsuario;
    private int iidTipoBeneficiario;
    private int iidTipoCasa;
    private int iidTipoCuenta;
    private int iidTipoEntidad;
    private int iidTipoFactura;
    private int iidTipoFuncionario;
    private int iidTipoMovimiento;
    private int iidTipoPermiso;
    private int iidTipoPersoneria;
    private int iidTipoSexo;
    private int iidTipoSucursal;
    private int iidTipoTalonario;
    private int iidViaCobro;
    private int iidDestino;
    private String srutaReporte;
    private double udisponibilidad;
    private int icantidadDiaGraciaPunitorio;
    private double umontoAutorizadoCajaChica;
    private double ufondoPrevision;
   
    protected short hestadoRegistro;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_BARRIO = "Barrio";
    public static final String TEXTO_CARGO = "Cargo";
    public static final String TEXTO_CIUDAD = "Ciudad";
    public static final String TEXTO_DEPARTAMENTO = "Departamento";
    public static final String TEXTO_ESTADO_CIVIL = "Estado Civil";
    public static final String TEXTO_GIRADURIA = "Giraduría";
    public static final String TEXTO_MONEDA = "Moneda";
    public static final String TEXTO_PAIS = "País";
    public static final String TEXTO_PARENTESCO = "Parentesco";
    public static final String TEXTO_RAMO = "Ramo";
    public static final String TEXTO_RUBRO = "Rubro";
    public static final String TEXTO_APORTE_CAPITAL = "Aporte Capital (valor positivo)";
    public static final String TEXTO_APORTE_CUOTA_SOCIAL = "Aporte Cuota Social (valor positivo)";
    public static final String TEXTO_APORTE_SOLIDARIDAD = "Aporte Solidaridad (valor positivo)";
    public static final String TEXTO_CUOTA_INGRESO = "Cuota de Ingreso (valor positivo)";
    public static final String TEXTO_IMPORTE_SEGURO_TITULAR = "Importe Seguro Titular (valor positivo)";
    public static final String TEXTO_IMPORTE_SEGURO_ADHERENTE = "Importe Seguro Adherente (valor positivo)";
    public static final String TEXTO_PROMOTOR = "Promotor";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de Medida";
    public static final String TEXTO_REGIONAL = "Regional";
    public static final String TEXTO_EMISOR_TALONARIO = "Emisor de Talonario";
    public static final String TEXTO_SUCURSAL = "Sucursal";
    public static final String TEXTO_TIPO_CREDITO = "Tipo de Crédito";
    public static final String TEXTO_TIPO_VALOR = "Tipo de Valor";
    public static final String TEXTO_PROFESION = "Profesión";
    public static final String TEXTO_RETENCION_ORDEN_CREDITO = "Porcentaje de Retención Orden de Crédito (valor positivo)";
    public static final String TEXTO_RETENCION_PRESTAMO_MUTUAL = "Porcentaje de Retención Préstamo Mutual (valor positivo)";
    public static final String TEXTO_RETENCION_PRESTAMO_FINANCIERA = "Porcentaje de Retención Préstamo Financiera (valor positivo)";
    public static final String TEXTO_TASA_INTERES_MORATORIO = "Tasa de Interés Moratorio (valor positivo)";
    public static final String TEXTO_TASA_INTERES_PUNITORIO = "Tasa de Interés Punitorio (valor positivo)";
    public static final String TEXTO_CANTIDAD_DIA_GRACIA = "Cantidad de días de gracia (valor positivo)";
    public static final String TEXTO_CLASE_PRODUCTO = "Clase de Producto";
    public static final String TEXTO_ESTADO_AHORRO_PROGRAMADO = "Estado Ahorro Programado";
    public static final String TEXTO_ESTADO_BENEFICIARIO = "Estado Beneficiario";
    public static final String TEXTO_ESTADO_EVENTO_FORMULARIO = "Estado Evento Formulario";
    public static final String TEXTO_ESTADO_FACTURA = "Estado Factura";
    public static final String TEXTO_ESTADO_FUNCIONARIO = "Estado Funcionario";
    public static final String TEXTO_ESTADO_INGRESO = "Estado Ingreso";
    public static final String TEXTO_ESTADO_OPERACION_FIJA = "Estado Operacion Fija";
    public static final String TEXTO_ESTADO_ORDEN_CREDITO = "Estado Orden de Crédito";
    public static final String TEXTO_ESTADO_ORDEN_PAGO = "Estado Orden de Pago";
    public static final String TEXTO_ESTADO_FONDO_JURIDICO = "Estado Fondo Jurídico";
    public static final String TEXTO_ESTADO_SOCIO = "Estado Socio";
    public static final String TEXTO_ESTADO_SOLIDARIDAD = "Estado Solidaridad";
    public static final String TEXTO_ESTADO_TALONARIO = "Estado Talonario";
    public static final String TEXTO_NIVEL_APROBACION = "Nivel de Aprobación";
    public static final String TEXTO_NIVEL_USUARIO = "Nivel de Usuario";
    public static final String TEXTO_TIPO_BENEFICIARIO = "Tipo de Beneficiario";
    public static final String TEXTO_TIPO_CASA = "Tipo de Casa";
    public static final String TEXTO_TIPO_CUENTA = "Tipo de Cuenta";
    public static final String TEXTO_TIPO_ENTIDAD = "Tipo de Entidad";
    public static final String TEXTO_TIPO_FACTURA = "Tipo de Factura";
    public static final String TEXTO_TIPO_FUNCIONARIO = "Tipo de Funcionario";
    public static final String TEXTO_TIPO_MOVIMIENTO = "Tipo de Movimiento";
    public static final String TEXTO_TIPO_PERMISO = "Tipo de Permiso";
    public static final String TEXTO_TIPO_PERSONERIA = "Tipo de Personería";
    public static final String TEXTO_TIPO_SEXO = "Tipo de Sexo";
    public static final String TEXTO_TIPO_SUCURSAL = "Tipo de Sucursal";
    public static final String TEXTO_TIPO_TALONARIO = "Tipo de Talonario";
    public static final String TEXTO_VIA_COBRO = "Vía de Cobro";
    public static final String TEXTO_DESTINO = "Destino";
    public static final String TEXTO_RUTA_REPORTE = "Ruta de Reporte";
    public static final String TEXTO_DISPONIBILIDAD = "Porcentaje de Disponibilidad (valor positivo)";
    public static final String TEXTO_CANTIDAD_DIA_GRACIA_PUNITORIO = "Cantidad de días de gracia Punitorio (valor positivo)";
    public static final String TEXTO_MONTO_AUTORIZADO_CAJA_CHICA = "Monto autorizado Caja Chica (valor positivo)";
    public static final String TEXTO_FONDO_PREVISION = "Fondo y Previsión (valor positivo)";

    public entDefecto() {
        this.iid = 0;
        this.iidBarrio = 0;
        this.iidCargo = 0;
        this.iidCiudad = 0;
        this.iidDepartamento = 0;
        this.iidEstadoCivil = 0;
        this.iidGiraduria = 0;
        this.iidMoneda = 0;
        this.iidPais = 0;
        this.iidParentesco = 0;
        this.iidRamo = 0;
        this.iidRubro = 0;
        this.uaporteCapital = 0.0;
        this.uaporteCuotaSocial = 0.0;
        this.uaporteSolidaridad = 0.0;
        this.ucuotaIngreso = 0.0;
        this.uimporteSeguroTitular = 0.0;
        this.uimporteSeguroAdherente = 0.0;
        this.iidPromotor = 0;
        this.iidUnidadMedida = 0;
        this.iidRegional = 0;
        this.iidEmisorTalonario = 0;
        this.iidSucursal = 0;
        this.iidTipoCredito = 0;
        this.iidTipoValor = 0;
        this.iidProfesion = 0;
        this.uretencionOrdenCredito = 0.0;
        this.uretencionPrestamoMutual = 0.0;
        this.uretencionPrestamoFinanciera = 0.0;
        this.utasaInteresMoratorio = 0.0;
        this.utasaInteresPunitorio = 0.0;
        this.icantidadDiaGracia = 0;
        this.iidClaseProducto = 0;
        this.iidEstadoAhorroProgramado = 0;
        this.iidEstadoBeneficiario = 0;
        this.iidEstadoEventoFormulario = 0;
        this.iidEstadoFactura = 0;
        this.iidEstadoFuncionario = 0;
        this.iidEstadoIngreso = 0;
        this.iidEstadoOperacionFija = 0;
        this.iidEstadoOrdenCredito = 0;
        this.iidEstadoOrdenPago = 0;
        this.iidEstadoFondoJuridico = 0;
        this.iidEstadoSocio = 0;
        this.iidEstadoSolidaridad = 0;
        this.iidEstadoTalonario = 0;
        this.iidNivelAprobacion = 0;
        this.iidNivelUsuario = 0;
        this.iidTipoBeneficiario = 0;
        this.iidTipoCasa = 0;
        this.iidTipoCuenta = 0;
        this.iidTipoEntidad = 0;
        this.iidTipoFactura = 0;
        this.iidTipoFuncionario = 0;
        this.iidTipoMovimiento = 0;
        this.iidTipoPermiso = 0;
        this.iidTipoPersoneria = 0;
        this.iidTipoSexo = 0;
        this.iidTipoSucursal = 0;
        this.iidTipoTalonario = 0;
        this.iidViaCobro = 0;
        this.iidDestino = 0;
        this.srutaReporte = "";
        this.udisponibilidad = 0.0;
        this.icantidadDiaGraciaPunitorio = 0;
        this.umontoAutorizadoCajaChica = 0.0;
        this.ufondoPrevision = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDefecto(int iid, int iidBarrio, int iidCargo, int iidCiudad, int iidDepartamento, int iidEstadoCivil, int iidGiraduria, int iidMoneda, int iidPais, int iidParentesco, int iidRamo, int iidRubro, double uaporteCapital, double uaporteCuotaSocial, double uaporteSolidaridad, double ucuotaIngreso, double uimporteSeguroTitular, double uimporteSeguroAdherente, int iidPromotor, int iidUnidadMedida, int iidRegional, int iidEmisorTalonario, int iidSucursal, int iidTipoCredito, int iidTipoValor, int iidProfesion, double uretencionOrdenCredito, double uretencionPrestamoMutual, double uretencionPrestamoFinanciera, double utasaInteresMoratorio, double utasaInteresPunitorio, int icantidadDiaGracia, int iidClaseProducto, int iidEstadoAhorroProgramado, int iidEstadoBeneficiario, int iidEstadoEventoFormulario, int iidEstadoFactura, int iidEstadoFuncionario, int iidEstadoIngreso, int iidEstadoOperacionFija, int iidEstadoOrdenCredito, int iidEstadoOrdenPago, int iidEstadoFondoJuridico, int iidEstadoSocio, int iidEstadoSolidaridad, int iidEstadoTalonario, int iidNivelAprobacion, int iidNivelUsuario, int iidTipoBeneficiario, int iidTipoCasa, int iidTipoCuenta, int iidTipoEntidad, int iidTipoFactura, int iidTipoFuncionario, int iidTipoMovimiento, int iidTipoPermiso, int iidTipoPersoneria, int iidTipoSexo, int iidTipoSucursal, int iidTipoTalonario, int iidViaCobro, int iidDestino, String srutaReporte, double udisponibilidad, int icantidadDiaGraciaPunitorio, double umontoAutorizadoCajaChica, double ufondoPrevision, short hestadoRegistro) {
        this.iid = iid;
        this.iidBarrio = iidBarrio;
        this.iidCargo = iidCargo;
        this.iidCiudad = iidCiudad;
        this.iidDepartamento = iidDepartamento;
        this.iidEstadoCivil = iidEstadoCivil;
        this.iidGiraduria = iidGiraduria;
        this.iidMoneda = iidMoneda;
        this.iidPais = iidPais;
        this.iidParentesco = iidParentesco;
        this.iidRamo = iidRamo;
        this.iidRubro = iidRubro;
        this.uaporteCapital = uaporteCapital;
        this.uaporteCuotaSocial = uaporteCuotaSocial;
        this.uaporteSolidaridad = uaporteSolidaridad;
        this.ucuotaIngreso = ucuotaIngreso;
        this.uimporteSeguroTitular = uimporteSeguroTitular;
        this.uimporteSeguroAdherente = uimporteSeguroAdherente;
        this.iidPromotor = iidPromotor;
        this.iidUnidadMedida = iidUnidadMedida;
        this.iidRegional = iidRegional;
        this.iidEmisorTalonario = iidEmisorTalonario;
        this.iidSucursal = iidSucursal;
        this.iidTipoCredito = iidTipoCredito;
        this.iidTipoValor = iidTipoValor;
        this.iidProfesion = iidProfesion;
        this.uretencionOrdenCredito = uretencionOrdenCredito;
        this.uretencionPrestamoMutual = uretencionPrestamoMutual;
        this.uretencionPrestamoFinanciera = uretencionPrestamoFinanciera;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.icantidadDiaGracia = icantidadDiaGracia;
        this.iidClaseProducto = iidClaseProducto;
        this.iidEstadoAhorroProgramado = iidEstadoAhorroProgramado;
        this.iidEstadoBeneficiario = iidEstadoBeneficiario;
        this.iidEstadoEventoFormulario = iidEstadoEventoFormulario;
        this.iidEstadoFactura = iidEstadoFactura;
        this.iidEstadoFuncionario = iidEstadoFuncionario;
        this.iidEstadoIngreso = iidEstadoIngreso;
        this.iidEstadoOperacionFija = iidEstadoOperacionFija;
        this.iidEstadoOrdenCredito = iidEstadoOrdenCredito;
        this.iidEstadoOrdenPago = iidEstadoOrdenPago;
        this.iidEstadoFondoJuridico = iidEstadoFondoJuridico;
        this.iidEstadoSocio = iidEstadoSocio;
        this.iidEstadoSolidaridad = iidEstadoSolidaridad;
        this.iidEstadoTalonario = iidEstadoTalonario;
        this.iidNivelAprobacion = iidNivelAprobacion;
        this.iidNivelUsuario = iidNivelUsuario;
        this.iidTipoBeneficiario = iidTipoBeneficiario;
        this.iidTipoCasa = iidTipoCasa;
        this.iidTipoCuenta = iidTipoCuenta;
        this.iidTipoEntidad = iidTipoEntidad;
        this.iidTipoFactura = iidTipoFactura;
        this.iidTipoFuncionario = iidTipoFuncionario;
        this.iidTipoMovimiento = iidTipoMovimiento;
        this.iidTipoPermiso = iidTipoPermiso;
        this.iidTipoPersoneria = iidTipoPersoneria;
        this.iidTipoSexo = iidTipoSexo;
        this.iidTipoSucursal = iidTipoSucursal;
        this.iidTipoTalonario = iidTipoTalonario;
        this.iidViaCobro = iidViaCobro;
        this.iidDestino = iidDestino;
        this.srutaReporte = srutaReporte;
        this.udisponibilidad = udisponibilidad;
        this.icantidadDiaGraciaPunitorio = icantidadDiaGraciaPunitorio;
        this.umontoAutorizadoCajaChica = umontoAutorizadoCajaChica;
        this.ufondoPrevision = ufondoPrevision;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidBarrio, int iidCargo, int iidCiudad, int iidDepartamento, int iidEstadoCivil, int iidGiraduria, int iidMoneda, int iidPais, int iidParentesco, int iidRamo, int iidRubro, double uaporteCapital, double uaporteCuotaSocial, double uaporteSolidaridad, double ucuotaIngreso, double uimporteSeguroTitular, double uimporteSeguroAdherente, int iidPromotor, int iidUnidadMedida, int iidRegional, int iidEmisorTalonario, int iidSucursal, int iidTipoCredito, int iidTipoValor, int iidProfesion, double uretencionOrdenCredito, double uretencionPrestamoMutual, double uretencionPrestamoFinanciera, double utasaInteresMoratorio, double utasaInteresPunitorio, int icantidadDiaGracia, int iidClaseProducto, int iidEstadoAhorroProgramado, int iidEstadoBeneficiario, int iidEstadoEventoFormulario, int iidEstadoFactura, int iidEstadoFuncionario, int iidEstadoIngreso, int iidEstadoOperacionFija, int iidEstadoOrdenCredito, int iidEstadoOrdenPago, int iidEstadoFondoJuridico, int iidEstadoSocio, int iidEstadoSolidaridad, int iidEstadoTalonario, int iidNivelAprobacion, int iidNivelUsuario, int iidTipoBeneficiario, int iidTipoCasa, int iidTipoCuenta, int iidTipoEntidad, int iidTipoFactura, int iidTipoFuncionario, int iidTipoMovimiento, int iidTipoPermiso, int iidTipoPersoneria, int iidTipoSexo, int iidTipoSucursal, int iidTipoTalonario, int iidViaCobro, int iidDestino, String srutaReporte, double udisponibilidad, int icantidadDiaGraciaPunitorio, double umontoAutorizadoCajaChica, double ufondoPrevision, short hestadoRegistro) {
        this.iid = iid;
        this.iidBarrio = iidBarrio;
        this.iidCargo = iidCargo;
        this.iidCiudad = iidCiudad;
        this.iidDepartamento = iidDepartamento;
        this.iidEstadoCivil = iidEstadoCivil;
        this.iidGiraduria = iidGiraduria;
        this.iidMoneda = iidMoneda;
        this.iidPais = iidPais;
        this.iidParentesco = iidParentesco;
        this.iidRamo = iidRamo;
        this.iidRubro = iidRubro;
        this.uaporteCapital = uaporteCapital;
        this.uaporteCuotaSocial = uaporteCuotaSocial;
        this.uaporteSolidaridad = uaporteSolidaridad;
        this.ucuotaIngreso = ucuotaIngreso;
        this.uimporteSeguroTitular = uimporteSeguroTitular;
        this.uimporteSeguroAdherente = uimporteSeguroAdherente;
        this.iidPromotor = iidPromotor;
        this.iidUnidadMedida = iidUnidadMedida;
        this.iidRegional = iidRegional;
        this.iidEmisorTalonario = iidEmisorTalonario;
        this.iidSucursal = iidSucursal;
        this.iidTipoCredito = iidTipoCredito;
        this.iidTipoValor = iidTipoValor;
        this.iidProfesion = iidProfesion;
        this.uretencionOrdenCredito = uretencionOrdenCredito;
        this.uretencionPrestamoMutual = uretencionPrestamoMutual;
        this.uretencionPrestamoFinanciera = uretencionPrestamoFinanciera;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.icantidadDiaGracia = icantidadDiaGracia;
        this.iidClaseProducto = iidClaseProducto;
        this.iidEstadoAhorroProgramado = iidEstadoAhorroProgramado;
        this.iidEstadoBeneficiario = iidEstadoBeneficiario;
        this.iidEstadoEventoFormulario = iidEstadoEventoFormulario;
        this.iidEstadoFactura = iidEstadoFactura;
        this.iidEstadoFuncionario = iidEstadoFuncionario;
        this.iidEstadoIngreso = iidEstadoIngreso;
        this.iidEstadoOperacionFija = iidEstadoOperacionFija;
        this.iidEstadoOrdenCredito = iidEstadoOrdenCredito;
        this.iidEstadoOrdenPago = iidEstadoOrdenPago;
        this.iidEstadoFondoJuridico = iidEstadoFondoJuridico;
        this.iidEstadoSocio = iidEstadoSocio;
        this.iidEstadoSolidaridad = iidEstadoSolidaridad;
        this.iidEstadoTalonario = iidEstadoTalonario;
        this.iidNivelAprobacion = iidNivelAprobacion;
        this.iidNivelUsuario = iidNivelUsuario;
        this.iidTipoBeneficiario = iidTipoBeneficiario;
        this.iidTipoCasa = iidTipoCasa;
        this.iidTipoCuenta = iidTipoCuenta;
        this.iidTipoEntidad = iidTipoEntidad;
        this.iidTipoFactura = iidTipoFactura;
        this.iidTipoFuncionario = iidTipoFuncionario;
        this.iidTipoMovimiento = iidTipoMovimiento;
        this.iidTipoPermiso = iidTipoPermiso;
        this.iidTipoPersoneria = iidTipoPersoneria;
        this.iidTipoSexo = iidTipoSexo;
        this.iidTipoSucursal = iidTipoSucursal;
        this.iidTipoTalonario = iidTipoTalonario;
        this.iidViaCobro = iidViaCobro;
        this.iidDestino = iidDestino;
        this.srutaReporte = srutaReporte;
        this.udisponibilidad = udisponibilidad;
        this.icantidadDiaGraciaPunitorio = icantidadDiaGraciaPunitorio;
        this.umontoAutorizadoCajaChica = umontoAutorizadoCajaChica;
        this.ufondoPrevision = ufondoPrevision;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entDefecto copiar(entDefecto destino) {
        destino.setEntidad(this.getId(), this.getIdBarrio(), this.getIdCargo(), this.getIdCiudad(), this.getIdDepartamento(), this.getIdEstadoCivil(), this.getIdGiraduria(), this.getIdMoneda(), this.getIdPais(), this.getIdParentesco(), this.getIdRamo(), this.getIdRubro(), this.getAporteCapital(), this.getAporteCuotaSocial(), this.getAporteSolidaridad(), this.getCuotaIngreso(), this.getImporteSeguroTitular(), this.getImporteSeguroAdherente(), this.getIdPromotor(), this.getIdUnidadMedida(), this.getIdRegional(), this.getIdEmisorTalonario(), this.getIdSucursal(), this.getIdTipoCredito(), this.getIdTipoValor(), this.getIdProfesion(), this.getRetencionOrdenCredito(), this.getRetencionPrestamoMutual(), this.getRetencionPrestamoFinanciera(), this.getTasaInteresMoratorio(), this.getTasaInteresPunitorio(), this.getCantidadDiaGracia(), this.getIdClaseProducto(), this.getIdEstadoAhorroProgramado(), this.getIdEstadoBeneficiario(), this.getIdEstadoEventoFormulario(), this.getIdEstadoFactura(), this.getIdEstadoFuncionario(), this.getIdEstadoIngreso(), this.getIdEstadoOperacionFija(), this.getIdEstadoOrdenCredito(), this.getIdEstadoOrdenPago(), this.getIdEstadoFondoJuridico(), this.getIdEstadoSocio(), this.getIdEstadoSolidaridad(), this.getIdEstadoTalonario(), this.getIdNivelAprobacion(), this.getIdNivelUsuario(), this.getIdTipoBeneficiario(), this.getIdTipoCasa(), this.getIdTipoCuenta(), this.getIdTipoEntidad(), this.getIdTipoFactura(), this.getIdTipoFuncionario(), this.getIdTipoMovimiento(), this.getIdTipoPermiso(), this.getIdTipoPersoneria(), this.getIdTipoSexo(), this.getIdTipoSucursal(), this.getIdTipoTalonario(), this.getIdViaCobro(), this.getIdDestino(), this.getRutaReporte(), this.getDisponibilidad(), this.getCantidadDiaGraciaPunitorio(), this.getMontoAutorizadoCajaChica(), this.getFondoPrevision(), this.getEstadoRegistro());
        return destino;
    }

    public entDefecto cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdBarrio(rs.getInt("idbarrio")); }
        catch(Exception e) {}
        try { this.setIdCargo(rs.getInt("idcargo")); }
        catch(Exception e) {}
        try { this.setIdCiudad(rs.getInt("idciudad")); }
        catch(Exception e) {}
        try { this.setIdDepartamento(rs.getInt("iddepartamento")); }
        catch(Exception e) {}
        try { this.setIdEstadoCivil(rs.getInt("idestadocivil")); }
        catch(Exception e) {}
        try { this.setIdGiraduria(rs.getInt("idgiraduria")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setIdPais(rs.getInt("idpais")); }
        catch(Exception e) {}
        try { this.setIdParentesco(rs.getInt("idparentesco")); }
        catch(Exception e) {}
        try { this.setIdRamo(rs.getInt("idramo")); }
        catch(Exception e) {}
        try { this.setIdRubro(rs.getInt("idrubro")); }
        catch(Exception e) {}
        try { this.setAporteCapital(rs.getDouble("aportecapital")); }
        catch(Exception e) {}
        try { this.setAporteCuotaSocial(rs.getDouble("aportecuotasocial")); }
        catch(Exception e) {}
        try { this.setAporteSolidaridad(rs.getDouble("aportesolidaridad")); }
        catch(Exception e) {}
        try { this.setCuotaIngreso(rs.getDouble("cuotaingreso")); }
        catch(Exception e) {}
        try { this.setImporteSeguroTitular(rs.getDouble("importesegurotitular")); }
        catch(Exception e) {}
        try { this.setImporteSeguroAdherente(rs.getDouble("importeseguroadherente")); }
        catch(Exception e) {}
        try { this.setIdPromotor(rs.getInt("idpromotor")); }
        catch(Exception e) {}
        try { this.setIdUnidadMedida(rs.getInt("idunidadmedida")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setIdEmisorTalonario(rs.getInt("idemisortalonario")); }
        catch(Exception e) {}
        try { this.setIdSucursal(rs.getInt("idsucursal")); }
        catch(Exception e) {}
        try { this.setIdTipoCredito(rs.getInt("idtipocredito")); }
        catch(Exception e) {}
        try { this.setIdTipoValor(rs.getInt("idtipovalor")); }
        catch(Exception e) {}
        try { this.setIdProfesion(rs.getInt("idprofesion")); }
        catch(Exception e) {}
        try { this.setRetencionOrdenCredito(rs.getDouble("retencionordencredito")); }
        catch(Exception e) {}
        try { this.setRetencionPrestamoMutual(rs.getDouble("retencionprestamomutual")); }
        catch(Exception e) {}
        try { this.setRetencionPrestamoFinanciera(rs.getDouble("retencionprestamofinanciera")); }
        catch(Exception e) {}
        try { this.setTasaInteresMoratorio(rs.getDouble("tasainteresmoratorio")); }
        catch(Exception e) {}
        try { this.setTasaInteresPunitorio(rs.getDouble("tasainterespunitorio")); }
        catch(Exception e) {}
        try { this.setCantidadDiaGracia(rs.getInt("cantidaddiagracia")); }
        catch(Exception e) {}
        try { this.setIdClaseProducto(rs.getInt("idclaseproducto")); }
        catch(Exception e) {}
        try { this.setIdEstadoAhorroProgramado(rs.getInt("idestadoahorroprogramado")); }
        catch(Exception e) {}
        try { this.setIdEstadoBeneficiario(rs.getInt("idestadobeneficiario")); }
        catch(Exception e) {}
        try { this.setIdEstadoEventoFormulario(rs.getInt("idestadoeventoformulario")); }
        catch(Exception e) {}
        try { this.setIdEstadoFactura(rs.getInt("idestadofactura")); }
        catch(Exception e) {}
        try { this.setIdEstadoFuncionario(rs.getInt("idestadofuncionario")); }
        catch(Exception e) {}
        try { this.setIdEstadoIngreso(rs.getInt("idestadoingreso")); }
        catch(Exception e) {}
        try { this.setIdEstadoOperacionFija(rs.getInt("idestadooperacionfija")); }
        catch(Exception e) {}
        try { this.setIdEstadoOrdenCredito(rs.getInt("idestadoordencredito")); }
        catch(Exception e) {}
        try { this.setIdEstadoOrdenPago(rs.getInt("idestadoordenpago")); }
        catch(Exception e) {}
        try { this.setIdEstadoFondoJuridico(rs.getInt("idestadofondojuridico")); }
        catch(Exception e) {}
        try { this.setIdEstadoSocio(rs.getInt("idestadosocio")); }
        catch(Exception e) {}
        try { this.setIdEstadoSolidaridad(rs.getInt("idestadosolidaridad")); }
        catch(Exception e) {}
        try { this.setIdEstadoTalonario(rs.getInt("idestadotalonario")); }
        catch(Exception e) {}
        try { this.setIdNivelAprobacion(rs.getInt("idnivelaprobacion")); }
        catch(Exception e) {}
        try { this.setIdNivelUsuario(rs.getInt("idnivelusuario")); }
        catch(Exception e) {}
        try { this.setIdTipoBeneficiario(rs.getInt("idtipobeneficiario")); }
        catch(Exception e) {}
        try { this.setIdTipoCasa(rs.getInt("idtipocasa")); }
        catch(Exception e) {}
        try { this.setIdTipoCuenta(rs.getInt("idtipocuenta")); }
        catch(Exception e) {}
        try { this.setIdTipoEntidad(rs.getInt("idtipoentidad")); }
        catch(Exception e) {}
        try { this.setIdTipoFactura(rs.getInt("idtipofactura")); }
        catch(Exception e) {}
        try { this.setIdTipoFuncionario(rs.getInt("idtipofuncionario")); }
        catch(Exception e) {}
        try { this.setIdTipoMovimiento(rs.getInt("idtipomovimiento")); }
        catch(Exception e) {}
        try { this.setIdTipoPermiso(rs.getInt("idtipopermiso")); }
        catch(Exception e) {}
        try { this.setIdTipoPersoneria(rs.getInt("idtipopersoneria")); }
        catch(Exception e) {}
        try { this.setIdTipoSexo(rs.getInt("idtiposexo")); }
        catch(Exception e) {}
        try { this.setIdTipoSucursal(rs.getInt("idtiposucursal")); }
        catch(Exception e) {}
        try { this.setIdTipoTalonario(rs.getInt("idtipotalonario")); }
        catch(Exception e) {}
        try { this.setIdViaCobro(rs.getInt("idviacobro")); }
        catch(Exception e) {}
        try { this.setIdDestino(rs.getInt("iddestino")); }
        catch(Exception e) {}
        try { this.setRutaReporte(rs.getString("rutareporte")); }
        catch(Exception e) {}
        try { this.setDisponibilidad(rs.getDouble("disponibilidad")); }
        catch(Exception e) {}
        try { this.setCantidadDiaGraciaPunitorio(rs.getInt("cantidaddiagraciapunitorio")); }
        catch(Exception e) {}
        try { this.setMontoAutorizadoCajaChica(rs.getDouble("montoautorizadocajachica")); }
        catch(Exception e) {}
        try { this.setFondoPrevision(rs.getDouble("fondoprevision")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdBarrio(int iidBarrio) {
        this.iidBarrio = iidBarrio;
    }

    public void setIdCargo(int iidCargo) {
        this.iidCargo = iidCargo;
    }

    public void setIdCiudad(int iidCiudad) {
        this.iidCiudad = iidCiudad;
    }

    public void setIdDepartamento(int iidDepartamento) {
        this.iidDepartamento = iidDepartamento;
    }

    public void setIdEstadoCivil(int iidEstadoCivil) {
        this.iidEstadoCivil = iidEstadoCivil;
    }

    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setIdPais(int iidPais) {
        this.iidPais = iidPais;
    }

    public void setIdParentesco(int iidParentesco) {
        this.iidParentesco = iidParentesco;
    }

    public void setIdRamo(int iidRamo) {
        this.iidRamo = iidRamo;
    }

    public void setIdRubro(int iidRubro) {
        this.iidRubro = iidRubro;
    }

    public void setAporteCapital(double uaporteCapital) {
        this.uaporteCapital = uaporteCapital;
    }

    public void setAporteCuotaSocial(double uaporteCuotaSocial) {
        this.uaporteCuotaSocial = uaporteCuotaSocial;
    }

    public void setAporteSolidaridad(double uaporteSolidaridad) {
        this.uaporteSolidaridad = uaporteSolidaridad;
    }

    public void setCuotaIngreso(double ucuotaIngreso) {
        this.ucuotaIngreso = ucuotaIngreso;
    }

    public void setImporteSeguroTitular(double uimporteSeguroTitular) {
        this.uimporteSeguroTitular = uimporteSeguroTitular;
    }

    public void setImporteSeguroAdherente(double uimporteSeguroAdherente) {
        this.uimporteSeguroAdherente = uimporteSeguroAdherente;
    }

    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }

    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setIdEmisorTalonario(int iidEmisorTalonario) {
        this.iidEmisorTalonario = iidEmisorTalonario;
    }

    public void setIdSucursal(int iidSucursal) {
        this.iidSucursal = iidSucursal;
    }

    public void setIdTipoCredito(int iidTipoCredito) {
        this.iidTipoCredito = iidTipoCredito;
    }

    public void setIdTipoValor(int iidTipoValor) {
        this.iidTipoValor = iidTipoValor;
    }

    public void setIdProfesion(int iidProfesion) {
        this.iidProfesion = iidProfesion;
    }

    public void setRetencionOrdenCredito(double uretencionOrdenCredito) {
        this.uretencionOrdenCredito = uretencionOrdenCredito;
    }

    public void setRetencionPrestamoMutual(double uretencionPrestamoMutual) {
        this.uretencionPrestamoMutual = uretencionPrestamoMutual;
    }

    public void setRetencionPrestamoFinanciera(double uretencionPrestamoFinanciera) {
        this.uretencionPrestamoFinanciera = uretencionPrestamoFinanciera;
    }

    public void setTasaInteresMoratorio(double utasaInteresMoratorio) {
        this.utasaInteresMoratorio = utasaInteresMoratorio;
    }

    public void setTasaInteresPunitorio(double utasaInteresPunitorio) {
        this.utasaInteresPunitorio = utasaInteresPunitorio;
    }

    public void setCantidadDiaGracia(int icantidadDiaGracia) {
        this.icantidadDiaGracia = icantidadDiaGracia;
    }

    public void setIdClaseProducto(int iidClaseProducto) {
        this.iidClaseProducto = iidClaseProducto;
    }

    public void setIdEstadoAhorroProgramado(int iidEstadoAhorroProgramado) {
        this.iidEstadoAhorroProgramado = iidEstadoAhorroProgramado;
    }

    public void setIdEstadoBeneficiario(int iidEstadoBeneficiario) {
        this.iidEstadoBeneficiario = iidEstadoBeneficiario;
    }

    public void setIdEstadoEventoFormulario(int iidEstadoEventoFormulario) {
        this.iidEstadoEventoFormulario = iidEstadoEventoFormulario;
    }

    public void setIdEstadoFactura(int iidEstadoFactura) {
        this.iidEstadoFactura = iidEstadoFactura;
    }

    public void setIdEstadoFuncionario(int iidEstadoFuncionario) {
        this.iidEstadoFuncionario = iidEstadoFuncionario;
    }

    public void setIdEstadoIngreso(int iidEstadoIngreso) {
        this.iidEstadoIngreso = iidEstadoIngreso;
    }

    public void setIdEstadoOperacionFija(int iidEstadoOperacionFija) {
        this.iidEstadoOperacionFija = iidEstadoOperacionFija;
    }

    public void setIdEstadoOrdenCredito(int iidEstadoOrdenCredito) {
        this.iidEstadoOrdenCredito = iidEstadoOrdenCredito;
    }

    public void setIdEstadoOrdenPago(int iidEstadoOrdenPago) {
        this.iidEstadoOrdenPago = iidEstadoOrdenPago;
    }

    public void setIdEstadoFondoJuridico(int iidEstadoFondoJuridico) {
        this.iidEstadoFondoJuridico = iidEstadoFondoJuridico;
    }

    public void setIdEstadoSocio(int iidEstadoSocio) {
        this.iidEstadoSocio = iidEstadoSocio;
    }

    public void setIdEstadoSolidaridad(int iidEstadoSolidaridad) {
        this.iidEstadoSolidaridad = iidEstadoSolidaridad;
    }

    public void setIdEstadoTalonario(int iidEstadoTalonario) {
        this.iidEstadoTalonario = iidEstadoTalonario;
    }

    public void setIdNivelAprobacion(int iidNivelAprobacion) {
        this.iidNivelAprobacion = iidNivelAprobacion;
    }

    public void setIdNivelUsuario(int iidNivelUsuario) {
        this.iidNivelUsuario = iidNivelUsuario;
    }

    public void setIdTipoBeneficiario(int iidTipoBeneficiario) {
        this.iidTipoBeneficiario = iidTipoBeneficiario;
    }

    public void setIdTipoCasa(int iidTipoCasa) {
        this.iidTipoCasa = iidTipoCasa;
    }

    public void setIdTipoCuenta(int iidTipoCuenta) {
        this.iidTipoCuenta = iidTipoCuenta;
    }

    public void setIdTipoEntidad(int iidTipoEntidad) {
        this.iidTipoEntidad = iidTipoEntidad;
    }

    public void setIdTipoFactura(int iidTipoFactura) {
        this.iidTipoFactura = iidTipoFactura;
    }

    public void setIdTipoFuncionario(int iidTipoFuncionario) {
        this.iidTipoFuncionario = iidTipoFuncionario;
    }

    public void setIdTipoMovimiento(int iidTipoMovimiento) {
        this.iidTipoMovimiento = iidTipoMovimiento;
    }

    public void setIdTipoPermiso(int iidTipoPermiso) {
        this.iidTipoPermiso = iidTipoPermiso;
    }

    public void setIdTipoPersoneria(int iidTipoPersoneria) {
        this.iidTipoPersoneria = iidTipoPersoneria;
    }

    public void setIdTipoSexo(int iidTipoSexo) {
        this.iidTipoSexo = iidTipoSexo;
    }

    public void setIdTipoSucursal(int iidTipoSucursal) {
        this.iidTipoSucursal = iidTipoSucursal;
    }

    public void setIdTipoTalonario(int iidTipoTalonario) {
        this.iidTipoTalonario = iidTipoTalonario;
    }

    public void setIdViaCobro(int iidViaCobro) {
        this.iidViaCobro = iidViaCobro;
    }
    
    public void setIdDestino(int iidDestino) {
        this.iidDestino = iidDestino;
    }
    
    public void setRutaReporte(String srutaReporte) {
        this.srutaReporte = srutaReporte;
    }
    
    public void setDisponibilidad(double udisponibilidad) {
        this.udisponibilidad = udisponibilidad;
    }
    
    public void setCantidadDiaGraciaPunitorio(int icantidadDiaGraciaPunitorio) {
        this.icantidadDiaGraciaPunitorio = icantidadDiaGraciaPunitorio;
    }
    
    public void setMontoAutorizadoCajaChica(double umontoAutorizadoCajaChica) {
        this.umontoAutorizadoCajaChica = umontoAutorizadoCajaChica;
    }
    
    public void setFondoPrevision(double ufondoPrevision) {
        this.ufondoPrevision = ufondoPrevision;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdBarrio() {
        return this.iidBarrio;
    }

    public int getIdCargo() {
        return this.iidCargo;
    }

    public int getIdCiudad() {
        return this.iidCiudad;
    }

    public int getIdDepartamento() {
        return this.iidDepartamento;
    }

    public int getIdEstadoCivil() {
        return this.iidEstadoCivil;
    }

    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public int getIdPais() {
        return this.iidPais;
    }

    public int getIdParentesco() {
        return this.iidParentesco;
    }

    public int getIdRamo() {
        return this.iidRamo;
    }

    public int getIdRubro() {
        return this.iidRubro;
    }

    public double getAporteCapital() {
        return this.uaporteCapital;
    }

    public double getAporteCuotaSocial() {
        return this.uaporteCuotaSocial;
    }

    public double getAporteSolidaridad() {
        return this.uaporteSolidaridad;
    }

    public double getCuotaIngreso() {
        return this.ucuotaIngreso;
    }

    public double getImporteSeguroTitular() {
        return this.uimporteSeguroTitular;
    }

    public double getImporteSeguroAdherente() {
        return this.uimporteSeguroAdherente;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public int getIdEmisorTalonario() {
        return this.iidEmisorTalonario;
    }

    public int getIdSucursal() {
        return this.iidSucursal;
    }

    public int getIdTipoCredito() {
        return this.iidTipoCredito;
    }

    public int getIdTipoValor() {
        return this.iidTipoValor;
    }

    public int getIdProfesion() {
        return this.iidProfesion;
    }

    public double getRetencionOrdenCredito() {
        return this.uretencionOrdenCredito;
    }

    public double getRetencionPrestamoMutual() {
        return this.uretencionPrestamoMutual;
    }

    public double getRetencionPrestamoFinanciera() {
        return this.uretencionPrestamoFinanciera;
    }

    public double getTasaInteresMoratorio() {
        return this.utasaInteresMoratorio;
    }

    public double getTasaInteresPunitorio() {
        return this.utasaInteresPunitorio;
    }

    public int getCantidadDiaGracia() {
        return this.icantidadDiaGracia;
    }

    public int getIdClaseProducto() {
        return this.iidClaseProducto;
    }

    public int getIdEstadoAhorroProgramado() {
        return this.iidEstadoAhorroProgramado;
    }

    public int getIdEstadoBeneficiario() {
        return this.iidEstadoBeneficiario;
    }

    public int getIdEstadoEventoFormulario() {
        return this.iidEstadoEventoFormulario;
    }

    public int getIdEstadoFactura() {
        return this.iidEstadoFactura;
    }

    public int getIdEstadoFuncionario() {
        return this.iidEstadoFuncionario;
    }

    public int getIdEstadoIngreso() {
        return this.iidEstadoIngreso;
    }

    public int getIdEstadoOperacionFija() {
        return this.iidEstadoOperacionFija;
    }

    public int getIdEstadoOrdenCredito() {
        return this.iidEstadoOrdenCredito;
    }

    public int getIdEstadoOrdenPago() {
        return this.iidEstadoOrdenPago;
    }

    public int getIdEstadoFondoJuridico() {
        return this.iidEstadoFondoJuridico;
    }

    public int getIdEstadoSocio() {
        return this.iidEstadoSocio;
    }

    public int getIdEstadoSolidaridad() {
        return this.iidEstadoSolidaridad;
    }

    public int getIdEstadoTalonario() {
        return this.iidEstadoTalonario;
    }

    public int getIdNivelAprobacion() {
        return this.iidNivelAprobacion;
    }

    public int getIdNivelUsuario() {
        return this.iidNivelUsuario;
    }

    public int getIdTipoBeneficiario() {
        return this.iidTipoBeneficiario;
    }

    public int getIdTipoCasa() {
        return this.iidTipoCasa;
    }

    public int getIdTipoCuenta() {
        return this.iidTipoCuenta;
    }

    public int getIdTipoEntidad() {
        return this.iidTipoEntidad;
    }

    public int getIdTipoFactura() {
        return this.iidTipoFactura;
    }

    public int getIdTipoFuncionario() {
        return this.iidTipoFuncionario;
    }

    public int getIdTipoMovimiento() {
        return this.iidTipoMovimiento;
    }

    public int getIdTipoPermiso() {
        return this.iidTipoPermiso;
    }

    public int getIdTipoPersoneria() {
        return this.iidTipoPersoneria;
    }

    public int getIdTipoSexo() {
        return this.iidTipoSexo;
    }

    public int getIdTipoSucursal() {
        return this.iidTipoSucursal;
    }

    public int getIdTipoTalonario() {
        return this.iidTipoTalonario;
    }

    public int getIdViaCobro() {
        return this.iidViaCobro;
    }

    public int getIdDestino() {
        return this.iidDestino;
    }

    public String getRutaReporte() {
        return this.srutaReporte;
    }

    public double getDisponibilidad() {
        return this.udisponibilidad;
    }

    public int getCantidadDiaGraciaPunitorio() {
        return this.icantidadDiaGraciaPunitorio;
    }

    public double getMontoAutorizadoCajaChica() {
        return this.umontoAutorizadoCajaChica;
    }

    public double getFondoPrevision() {
        return this.ufondoPrevision;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        this.smensaje = "";
        if (this.getAporteCapital() < 0)               this.smensaje += TEXTO_APORTE_CAPITAL + "\n";
        if (this.getAporteCuotaSocial() < 0)           this.smensaje += TEXTO_APORTE_CUOTA_SOCIAL + "\n";
        if (this.getAporteSolidaridad() < 0)           this.smensaje += TEXTO_APORTE_SOLIDARIDAD + "\n";
        if (this.getCuotaIngreso() < 0)                this.smensaje += TEXTO_CUOTA_INGRESO + "\n";
        if (this.getImporteSeguroTitular() < 0)        this.smensaje += TEXTO_IMPORTE_SEGURO_TITULAR + "\n";
        if (this.getImporteSeguroAdherente() < 0)      this.smensaje += TEXTO_IMPORTE_SEGURO_ADHERENTE + "\n";
        if (this.getRetencionOrdenCredito() < 0)       this.smensaje += TEXTO_RETENCION_ORDEN_CREDITO + "\n";
        if (this.getRetencionPrestamoMutual() < 0)     this.smensaje += TEXTO_RETENCION_PRESTAMO_MUTUAL + "\n";
        if (this.getRetencionPrestamoFinanciera() < 0) this.smensaje += TEXTO_RETENCION_PRESTAMO_FINANCIERA + "\n";
        if (this.getTasaInteresMoratorio()< 0)         this.smensaje += TEXTO_TASA_INTERES_MORATORIO + "\n";
        if (this.getTasaInteresPunitorio()< 0)         this.smensaje += TEXTO_TASA_INTERES_PUNITORIO + "\n";
        if (this.getCantidadDiaGracia()< 0)            this.smensaje += TEXTO_CANTIDAD_DIA_GRACIA + "\n";
        if (this.getDisponibilidad()< 0)               this.smensaje += TEXTO_DISPONIBILIDAD + "\n";
        if (this.getCantidadDiaGraciaPunitorio()< 0)   this.smensaje += TEXTO_CANTIDAD_DIA_GRACIA_PUNITORIO + "\n";
        if (this.getMontoAutorizadoCajaChica()< 0)     this.smensaje += TEXTO_MONTO_AUTORIZADO_CAJA_CHICA + "\n";
        if (this.getFondoPrevision()< 0)               this.smensaje += TEXTO_FONDO_PREVISION + "\n";
        if (this.smensaje.isEmpty()) return true;
        this.smensaje = this.smensaje.substring(0,this.smensaje.length()-1);
        return false;
    }
    
    public String getSentencia() {
        return "SELECT defecto("+
            this.getId()+","+
            this.getIdBarrio()+","+
            this.getIdCargo()+","+
            this.getIdCiudad()+","+
            this.getIdDepartamento()+","+
            this.getIdEstadoCivil()+","+
            this.getIdGiraduria()+","+
            this.getIdMoneda()+","+
            this.getIdPais()+","+
            this.getIdParentesco()+","+
            this.getIdRamo()+","+
            this.getIdRubro()+","+
            this.getAporteCapital()+","+
            this.getAporteCuotaSocial()+","+
            this.getAporteSolidaridad()+","+
            this.getCuotaIngreso()+","+
            this.getImporteSeguroTitular()+","+
            this.getImporteSeguroAdherente()+","+
            this.getIdPromotor()+","+
            this.getIdUnidadMedida()+","+
            this.getIdRegional()+","+
            this.getIdEmisorTalonario()+","+
            this.getIdSucursal()+","+
            this.getIdTipoCredito()+","+
            this.getIdTipoValor()+","+
            this.getIdProfesion()+","+
            this.getRetencionOrdenCredito()+","+
            this.getRetencionPrestamoMutual()+","+
            this.getRetencionPrestamoFinanciera()+","+
            this.getTasaInteresMoratorio()+","+
            this.getTasaInteresPunitorio()+","+
            this.getCantidadDiaGracia()+","+
            this.getIdClaseProducto()+","+
            this.getIdEstadoAhorroProgramado()+","+
            this.getIdEstadoBeneficiario()+","+
            this.getIdEstadoEventoFormulario()+","+
            this.getIdEstadoFactura()+","+
            this.getIdEstadoFuncionario()+","+
            this.getIdEstadoIngreso()+","+
            this.getIdEstadoOperacionFija()+","+
            this.getIdEstadoOrdenCredito()+","+
            this.getIdEstadoOrdenPago()+","+
            this.getIdEstadoFondoJuridico()+","+
            this.getIdEstadoSocio()+","+
            this.getIdEstadoSolidaridad()+","+
            this.getIdEstadoTalonario()+","+
            this.getIdNivelAprobacion()+","+
            this.getIdNivelUsuario()+","+
            this.getIdTipoBeneficiario()+","+
            this.getIdTipoCasa()+","+
            this.getIdTipoCuenta()+","+
            this.getIdTipoEntidad()+","+
            this.getIdTipoFactura()+","+
            this.getIdTipoFuncionario()+","+
            this.getIdTipoMovimiento()+","+
            this.getIdTipoPermiso()+","+
            this.getIdTipoPersoneria()+","+
            this.getIdTipoSexo()+","+
            this.getIdTipoSucursal()+","+
            this.getIdTipoTalonario()+","+
            this.getIdViaCobro()+","+
            utilitario.utiCadena.getTextoGuardado(this.getRutaReporte())+","+
            this.getDisponibilidad()+","+
            this.getCantidadDiaGraciaPunitorio()+","+
            this.getIdDestino()+","+
            this.getMontoAutorizadoCajaChica()+","+
            this.getFondoPrevision()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
