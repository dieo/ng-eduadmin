/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entGenerica;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entRegional extends entGenerica{

    private int iidZona;
    private String szona;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Regional (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_ZONA = "Zona (no vacía)";
    
    public entRegional() {
        super();
        this.iidZona = 0;
        this.szona = "";
    }

    public entRegional(int iid, String sdescripcion, int iidZona, String szona, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidZona = iidZona;
        this.szona = szona;
    }

    public void setEntidad(int iid, String sdescripcion, int iidZona, String szona, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidZona = iidZona;
        this.szona = szona;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entRegional copiar(entRegional destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdZona(), this.getZona(), this.getEstadoRegistro());
        return destino;
    }
    
    public entRegional cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdZona(rs.getInt("idzona")); }
        catch(Exception e) {}
        try { this.setZona(rs.getString("zona")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdZona(int iidZona) {
        this.iidZona = iidZona;
    }
    
    public void setZona(String szona) {
        this.szona = szona;
    }
    
    public int getIdZona() {
        return this.iidZona;
    }
    
    public String getZona() {
        return this.szona;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdZona()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ZONA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT regional("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdZona()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
}
