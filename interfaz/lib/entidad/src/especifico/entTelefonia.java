/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entTelefonia extends generico.entGenerica {

    protected String sdescripcionBreve;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final int LONGITUD_DESCRIPCION_BREVE = 2;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Tipo de Crédito (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_DESCRIPCION_BREVE = "Descripción breve de Tipo de Crédito (no vacía, hasta " + LONGITUD_DESCRIPCION_BREVE + " caracteres)";

    public entTelefonia() {
        super();
        this.sdescripcionBreve = "";
    }

    public entTelefonia(int iid, String sdescripcion, String sdescripcionBreve) {
        super(iid, sdescripcion);
        this.sdescripcionBreve = sdescripcionBreve;
    }

    public entTelefonia(int iid, String sdescripcion, String sdescripcionBreve, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.sdescripcionBreve = sdescripcionBreve;
    }

    public void setEntidad(int iid, String sdescripcion, String sdescripcionBreve, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.sdescripcionBreve = sdescripcionBreve;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entTelefonia copiar(entTelefonia destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getDescripcionBreve(), this.getEstadoRegistro());
        return destino;
    }

    public entTelefonia cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setDescripcionBreve(rs.getString("descripcionbreve")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setDescripcionBreve(String sdescripcionBreve) {
        this.sdescripcionBreve = sdescripcionBreve;
    }

    public String getDescripcionBreve() {
        return this.sdescripcionBreve;
    }

    public boolean esValido() {
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) this.smensaje += TEXTO_DESCRIPCION + "\n";
        if (this.getDescripcionBreve().isEmpty()) this.smensaje += TEXTO_DESCRIPCION_BREVE + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public String getSentencia() {
        return "SELECT telefonia("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcionBreve())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
}
