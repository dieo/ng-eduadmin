/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entControlVerificador {
    
    private int iid;
    private int iidFuncionario;
    private String sfuncionario;
    private String scargo;
    private boolean bactivo;
    private boolean bcorrector;
    private short hestado;
    private String smensaje;
    
    public static final int LONGITUD_CARGO = 50;
    public static final int LONGITUD_CONTRASENA = 25;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FUNCIONARIO = "Apellido y Nombre del Funcionario (no vacío)";
    public static final String TEXTO_CARGO = "Cargo (no vacío, hasta " + LONGITUD_CARGO + " caracteres)";
    public static final String TEXTO_ACTIVO = "Activo";
    public static final String TEXTO_CORRECTOR = "Corrector";
    
    public entControlVerificador() {
        this.iid = 0;
        this.iidFuncionario = 0;
        this.sfuncionario = "";
        this.scargo = "";
        this.bactivo = false;
        this.bcorrector = false;
        this.hestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }
    
    public entControlVerificador(int iid, int iidFuncionario, String sfuncionario, String scargo, boolean bactivo, boolean bcorrector, short hestado) {
        this.iid = iid;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.scargo = scargo;
        this.bactivo = bactivo;
        this.bcorrector = bcorrector;
        this.hestado = hestado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidFuncionario, String sfuncionario, String scargo, boolean bactivo, boolean bcorrector, short hestado) {
        this.iid = iid;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.scargo = scargo;
        this.bactivo = bactivo;
        this.bcorrector = bcorrector;
        this.hestado = hestado;
        this.smensaje = "";
    }
    
    public entControlVerificador copiar(entControlVerificador destino) {
        destino.setEntidad(this.getId(), this.getIdFuncionario(), this.getFuncionario(), this.getCargo(), this.getActivo(), this.getCorrector(), this.getEstadoRegistro());
        return destino;
    }
    
    public entControlVerificador cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        try { this.setFuncionario(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setCargo(rs.getString("cargo")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setCorrector(rs.getBoolean("corrector")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }
    
    public void setFuncionario(String sfuncionario) {
        this.sfuncionario = sfuncionario;
    }
    
    public void setCargo(String scargo) {
        this.scargo = scargo;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setCorrector(boolean bcorrector) {
        this.bcorrector = bcorrector;
    }
    
    public void setEstadoRegistro(short hestado) {
        this.hestado = hestado;
    }
    
    public int getId() {
        return this.iid;
    }
    
    public int getIdFuncionario() {
        return this.iidFuncionario;
    }
    
    public String getFuncionario() {
        return this.sfuncionario;
    }
    
    public String getCargo() {
        return this.scargo;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }
    
    public boolean getCorrector() {
        return this.bcorrector;
    }
    
    public short getEstadoRegistro() {
        return this.hestado;
    }
    
    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdFuncionario() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO;
            bvalido = false;
        }
        if (this.getCargo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CARGO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT verificador("+
            this.getId()+","+
            this.getIdFuncionario()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCargo())+","+
            this.getActivo()+","+
            this.getCorrector()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Funcionario", "apellidonombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Cargo", "cargo", generico.entLista.tipo_texto));
        return lst;
    }
    
}
