/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entIngreso {
    
    private int iid;
    private int inumeroIngreso;
    private int iidPlanilla;
    private String splanilla;
    private java.util.Date dfechaOperacion;
    private int iidPersona;
    private boolean besSocio;
    private String sruc;
    private String srazonSocial;
    private String sdireccion;
    private String stelefono;
    private int iidSocio;
    private int iidEstadoSocio;
    private String sestadoSocio;
    private java.util.Date dfechaAAplicar;
    private double uimporte;
    private int iidMoneda;
    private String smoneda;
    private double usaldo;
    private int iidUsuario;
    private String susuario;
    private String sobservacion;
    private int iidEstado;
    private String sestado;
    private int iidMovimiento;
    private int inumeroSolicitud;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_OBSERVACION = 250;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMERO_INGRESO = "Número de Ingreso (no editable)";
    public static final String TEXTO_PLANILLA = "Planilla de Ingreso (no vacía)";
    public static final String TEXTO_ESTADO = "Estado (no editable)";
    public static final String TEXTO_FECHA_OPERACION = "Fecha de operación (no vacía)";
    public static final String TEXTO_SOCIO = "Es Socio (busca cuentas)";
    public static final String TEXTO_PERSONA_RUC = "RUC o Cédula de la persona (<ENT> o <ESP> calcula el Dígito Verificador)";
    public static final String TEXTO_PERSONA_RAZON_SOCIAL = "Razón Social (no vacía)";
    public static final String TEXTO_ESTADO_SOCIO = "Estado Socio (no editable)";
    public static final String TEXTO_PERSONA_DIRECCION = "Dirección de la persona (no vacía)";
    public static final String TEXTO_PERSONA_TELEFONO = "Teléfono de la persona (no vacía)";
    public static final String TEXTO_FECHA_A_APLICAR = "Fecha a aplicar (no vacía)";
    public static final String TEXTO_IMPORTE_A_PAGAR = "Importe a Pagar (valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_SALDO = "Saldo (no editable)";
    public static final String TEXTO_OBSERVACION = "Observación";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación de anulación (no vacío)";
    
    public entIngreso() {
        this.iid = 0;
        this.inumeroIngreso = 0;
        this.iidPlanilla = 0;
        this.splanilla = "";
        this.dfechaOperacion = null;
        this.iidPersona = 0;
        this.besSocio = false;
        this.sruc = "";
        this.srazonSocial = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.iidSocio = 0;
        this.iidEstadoSocio = 0;
        this.sestadoSocio = "";
        this.dfechaAAplicar = null;
        this.uimporte = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.usaldo = 0.0;
        this.iidUsuario = 0;
        this.susuario = "";
        this.sobservacion = "";
        this.iidEstado = 0;
        this.sestado = "";
        this.iidMovimiento = 0;
        this.inumeroSolicitud = 0;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entIngreso(int iid, int inumeroIngreso, int iidPlanilla, String splanilla, java.util.Date dfechaOperacion, int iidPersona, boolean besSocio, String sruc, String srazonSocial, String sdireccion, String stelefono, int iidSocio, int iidEstadoSocio, String sestadoSocio, java.util.Date dfechaAAplicar, double uimporte, int iidMoneda, String smoneda, double usaldo, int iidUsuario, String susuario, String sobservacion, int iidEstado, String sestado, int iidMovimiento, int inumeroSolicitud, java.util.Date dfechaAnulado, String sobservacionAnulado, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroIngreso = inumeroIngreso;
        this.iidPlanilla = iidPlanilla;
        this.splanilla = splanilla;
        this.dfechaOperacion = dfechaOperacion;
        this.iidPersona = iidPersona;
        this.besSocio = besSocio;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidSocio = iidSocio;
        this.iidEstadoSocio = iidEstadoSocio;
        this.sestadoSocio = sestadoSocio;
        this.dfechaAAplicar = dfechaAAplicar;
        this.uimporte = uimporte;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.usaldo = usaldo;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.sobservacion = sobservacion;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidMovimiento = iidMovimiento;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int inumeroIngreso, int iidPlanilla, String splanilla, java.util.Date dfechaOperacion, int iidPersona, boolean besSocio, String sruc, String srazonSocial, String sdireccion, String stelefono, int iidSocio, int iidEstadoSocio, String sestadoSocio, java.util.Date dfechaAAplicar, double uimporte, int iidMoneda, String smoneda, double usaldo, int iidUsuario, String susuario, String sobservacion, int iidEstado, String sestado, int iidMovimiento, int inumeroSolicitud, java.util.Date dfechaAnulado, String sobservacionAnulado, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroIngreso = inumeroIngreso;
        this.iidPlanilla = iidPlanilla;
        this.splanilla = splanilla;
        this.dfechaOperacion = dfechaOperacion;
        this.iidPersona = iidPersona;
        this.besSocio = besSocio;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidSocio = iidSocio;
        this.iidEstadoSocio = iidEstadoSocio;
        this.sestadoSocio = sestadoSocio;
        this.dfechaAAplicar = dfechaAAplicar;
        this.uimporte = uimporte;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.usaldo = usaldo;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.sobservacion = sobservacion;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidMovimiento = iidMovimiento;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entIngreso copiar(entIngreso destino) {
        destino.setEntidad(this.getId(), this.getNumeroIngreso(), this.getIdPlanilla(), this.getPlanilla(), this.getFechaOperacion(), this.getIdPersona(), this.getEsSocio(), this.getRuc(), this.getRazonSocial(), this.getDireccion(), this.getTelefono(), this.getIdSocio(), this.getIdEstadoSocio(), this.getEstadoSocio(), this.getFechaAAplicar(), this.getImporte(), this.getIdMoneda(), this.getMoneda(),  this.getSaldo(), this.getIdUsuario(), this.getUsuario(), this.getObservacion(), this.getIdEstado(), this.getEstado(), this.getIdMovimiento(), this.getNumeroSolicitud(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getEstadoRegistro());
        return destino;
    }
    
    public entIngreso cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setNumeroIngreso(rs.getInt("numerooperacion")); }
        catch(Exception e) { }
        try { this.setIdPlanilla(rs.getInt("idplanilla")); }
        catch(Exception e) { }
        try { this.setPlanilla(rs.getString("planilla")); }
        catch(Exception e) { }
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) { }
        try { this.setEsSocio(rs.getBoolean("essocio")); }
        catch(Exception e) { }
        if (this.getEsSocio()) {
            try { this.setIdSocio(rs.getInt("idsocio")); }
            catch(Exception e) { }
        } else {
            try { this.setIdPersona(rs.getInt("idpersona")); }
            catch(Exception e) { }
        }
        try { this.setIdEstadoSocio(rs.getInt("idestadosocio")); }
        catch(Exception e) { }
        try { this.setEstadoSocio(rs.getString("estadosocio")); }
        catch(Exception e) { }
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) { }
        try { this.setRazonSocial(rs.getString("razonsocial")); }
        catch(Exception e) { }
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) { }
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) { }
        try { this.setFechaAAplicar(rs.getDate("fechaaaplicar")); }
        catch(Exception e) { }
        try { this.setImporte(rs.getDouble("importe")); }
        catch(Exception e) { }
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) { }
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) { }
        try { this.setSaldo(rs.getDouble("saldo")); }
        catch(Exception e) { }
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) { }
        try { this.setUsuario(rs.getString("usuario")); }
        catch(Exception e) { }
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) { }
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) { }
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) { }
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) { }
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) { }
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) { }
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) { }
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) { }
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setNumeroIngreso(int inumeroIngreso) {
        this.inumeroIngreso = inumeroIngreso;
    }

    public void setIdPlanilla(int iidPlanilla) {
        this.iidPlanilla = iidPlanilla;
    }

    public void setPlanilla(String splanilla) {
        this.splanilla = splanilla;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setIdPersona(int iidPersona) {
        this.iidPersona = iidPersona;
    }

    public void setEsSocio(boolean besSocio) {
        this.besSocio = besSocio;
    }

    public void setRuc(String sruc) {
        this.sruc = sruc;
    }

    public void setRazonSocial(String srazonSocial) {
        this.srazonSocial = srazonSocial;
    }

    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdEstadoSocio(int iidEstadoSocio) {
        this.iidEstadoSocio = iidEstadoSocio;
    }

    public void setEstadoSocio(String sestadoSocio) {
        this.sestadoSocio = sestadoSocio;
    }

    public void setFechaAAplicar(java.util.Date dfechaAAplicar) {
        this.dfechaAAplicar = dfechaAAplicar;
    }

    public void setImporte(double uimporte) {
        this.uimporte = uimporte;
    }
    
    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setSaldo(double usaldo) {
        this.usaldo = usaldo;
    }
    
    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }
    
    public void setUsuario(String susuario) {
        this.susuario = susuario;
    }
    
    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }
    
    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getNumeroIngreso() {
        return this.inumeroIngreso;
    }

    public int getIdPlanilla() {
        return this.iidPlanilla;
    }

    public String getPlanilla() {
        return this.splanilla;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public int getIdPersona() {
        return this.iidPersona;
    }

    public boolean getEsSocio() {
        return this.besSocio;
    }

    public String getRuc() {
        return this.sruc;
    }

    public String getRazonSocial() {
        return this.srazonSocial;
    }

    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefono() {
        return this.stelefono;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public int getIdEstadoSocio() {
        return this.iidEstadoSocio;
    }

    public String getEstadoSocio() {
        return this.sestadoSocio;
    }

    public java.util.Date getFechaAAplicar() {
        return this.dfechaAAplicar;
    }

    public double getImporte() {
        return this.uimporte;
    }
    
    public int getIdMoneda() {
        return this.iidMoneda;
    }
    
    public String getMoneda() {
        return this.smoneda;
    }
    
    public double getSaldo() {
        return this.usaldo;
    }
    
    public int getIdUsuario() {
        return this.iidUsuario;
    }
    
    public String getUsuario() {
        return this.susuario;
    }
    
    public String getObservacion() {
        return this.sobservacion;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }
    
    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar(double umontoTotalValor) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNumeroIngreso()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_INGRESO;
            bvalido = false;
        }
        if (this.getFechaOperacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getIdPersona()<=0 && this.getIdSocio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Persona (no vacía)";
            bvalido = false;
        }
        if (this.getRuc().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PERSONA_RUC;
            bvalido = false;
        }
        if (this.getRazonSocial().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PERSONA_RAZON_SOCIAL;
            bvalido = false;
        }
        if (this.getDireccion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PERSONA_DIRECCION;
            bvalido = false;
        }
        if (this.getTelefono().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PERSONA_TELEFONO;
            bvalido = false;
        }
        if (this.getImporte()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPORTE_A_PAGAR;
            bvalido = false;
        }
        if (this.getImporte() > umontoTotalValor) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "El importe a Cobrar debe ser menor o igual al importe de Valores";
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getIdPlanilla()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLANILLA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaAnulado()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT ingreso("+
            this.getId()+","+
            this.getNumeroIngreso()+","+
            this.getIdPlanilla()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaOperacion())+","+
            this.getIdPersona()+","+
            this.getEsSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getRuc())+","+
            this.getIdSocio()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAAplicar())+","+
            this.getImporte()+","+
            this.getIdMoneda()+","+
            this.getSaldo()+","+
            this.getIdUsuario()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            this.getIdEstado()+","+
            this.getIdMovimiento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getRazonSocial())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getIdEstadoSocio()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número Ingreso", "numerooperacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Fecha Operación", "fechaoperacion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Razón Social", "razonsocial", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Fecha a Aplicar", "fechaaaplicar", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(5, "Sucursal", "sucursal", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Funcionario", "funcionario", generico.entLista.tipo_texto));
        return lst;
    }
    
}
