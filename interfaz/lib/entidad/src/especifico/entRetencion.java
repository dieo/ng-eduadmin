/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entRetencion {

    private int iid;
    private int iidSocio;
    private String scedula;
    private int inumeroSocio;
    private String sapellidonombre;
    private int iidOperacion;
    private String soperacion;
    private int inumeroOperacion;
    private String stabla;
    private java.util.Date dfechaoperacion;
    private java.util.Date dfechainicio;
    private boolean bactivo;
    private String smotivo;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_MOTIVO = 100;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FECHA = "Fecha (no vacía)";
    public static final String TEXTO_SOCIO = "Socio (no editable, no vacío)";
    public static final String TEXTO_OPERACION = "Operación (no editable, no vacío)";
    public static final String TEXTO_ACTIVO = "Si se encuentra o no activo (no vacía)";
    public static final String TEXTO_FECHA_INICIO = "Fecha de Inicio (no vacía)";
    public static final String TEXTO_MOTIVO = "Motivo (no vacía, hasta " + LONGITUD_MOTIVO+ " caracteres)";


    public entRetencion() {
        this.iid = 0;
        this.dfechaoperacion = null;
        this.dfechainicio = null;
        this.iidSocio = 0;
        this.scedula = "";
        this.inumeroSocio = 0;
        this.sapellidonombre = "";
        this.iidOperacion = 0;
        this.soperacion = "";
        this.inumeroOperacion = 0;
        this.stabla = "";
        this.bactivo = false;
        this.smotivo = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entRetencion(int iid, java.util.Date dfechaoperacion, java.util.Date dfechainicio, int iidSocio, String scedula, int inumeroSocio, String sapellidonombre, int iidOperacion, String soperacion, int inumeroOperacion, String stabla, boolean bactivo, String smotivo, short hestadoRegistro) {
        this.iid = iid;
        this.dfechaoperacion = dfechaoperacion;
        this.dfechainicio = dfechainicio;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.inumeroSocio = inumeroSocio;
        this.sapellidonombre = sapellidonombre;
        this.iidOperacion = iidOperacion;
        this.soperacion = soperacion;
        this.inumeroOperacion = inumeroOperacion;
        this.stabla = stabla;
        this.bactivo = bactivo;
        this.smotivo = smotivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, java.util.Date dfechaoperacion, java.util.Date dfechainicio, int iidSocio, String scedula, int inumeroSocio, String sapellidonombre, int iidOperacion, String soperacion, int inumeroOperacion, String stabla, boolean bactivo, String smotivo, short hestadoRegistro) {
        this.iid = iid;
        this.dfechaoperacion = dfechaoperacion;
        this.dfechainicio = dfechainicio;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.inumeroSocio = inumeroSocio;
        this.sapellidonombre = sapellidonombre;
        this.iidOperacion = iidOperacion;
        this.soperacion = soperacion;
        this.inumeroOperacion = inumeroOperacion;
        this.stabla = stabla;
        this.bactivo = bactivo;
        this.smotivo = smotivo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entRetencion copiar(entRetencion destino) {
        destino.setEntidad(this.getId(), this.getFechaOperacion(), this.getFechaInicio(), this.getIdSocio(), this.getCedula(), this.getNumeroSocio(), this.getApellidoNombre(), this.getIdOperacion(), this.getOperacion(), this.getNumeroOperacion(), this.getTabla(), this.getActivo(), this.getMotivo(), this.getEstadoRegistro());
        return destino;
    }

    public entRetencion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNumeroSocio(rs.getInt("numerosocio")); }
        catch(Exception e) {}
        try { this.setApellidoNombre(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setIdOperacion(rs.getInt("idoperacion")); }
        catch(Exception e) {}
        try { this.setOperacion(rs.getString("operacion")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numero")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setFechaInicio(rs.getDate("fechainicio")); }
        catch(Exception e) {}
        try { this.setMotivo(rs.getString("motivo")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setFechaOperacion(java.util.Date dfechaoperacion) {
        this.dfechaoperacion = dfechaoperacion;
    }

    public void setFechaInicio(java.util.Date dfechainicio) {
        this.dfechainicio = dfechainicio;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNumeroSocio(int inumeroSocio) {
        this.inumeroSocio = inumeroSocio;
    }

    public void setApellidoNombre(String sapellidonombre) {
        this.sapellidonombre = sapellidonombre;
    }

    public void setIdOperacion(int iidOperacion) {
        this.iidOperacion = iidOperacion;
    }

    public void setOperacion(String soperacion) {
        this.soperacion = soperacion;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setMotivo(String smotivo) {
        this.smotivo = smotivo;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaoperacion;
    }
    
    public java.util.Date getFechaInicio() {
        return this.dfechainicio;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public int getNumeroSocio() {
        return this.inumeroSocio;
    }

    public String getApellidoNombre() {
        return this.sapellidonombre;
    }

    public int getIdOperacion() {
        return this.iidOperacion;
    }

    public String getOperacion() {
        return this.soperacion;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public String getTabla() {
        return this.stabla;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public String getMotivo() {
        return this.smotivo;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdSocio() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getIdOperacion() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OPERACION;
            bvalido = false;
        }
        if (this.getFechaOperacion() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getMotivo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MOTIVO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT retencion("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdOperacion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaOperacion())+","+
            this.getActivo()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaInicio())+","+
            utilitario.utiCadena.getTextoGuardado(this.getMotivo())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cédula", "cedula", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Apellido, Nombre", "apellidonombre", new generico.entLista().tipo_texto));
        return lst;
    }

}
