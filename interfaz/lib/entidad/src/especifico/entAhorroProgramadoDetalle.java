/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entAhorroProgramadoDetalle {

    private int iid;
    private int icuota;
    private java.util.Date dfechaVencimiento;
    private double uimporte;
    private double upago;
    private double usaldo;
    private int icantidadDia;
    private double uinteres;
    
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_OBSERVACION = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CUOTA = "Número de Cuota (no editable)";
    public static final String TEXTO_FECHA_VENCIMIENTO = "Fecha de Vencimiento (no editable)";
    public static final String TEXTO_IMPORTE = "Importe mensual a pagar (no editable)";
    public static final String TEXTO_PAGO = "Importe pagado (valor positivo)";
    public static final String TEXTO_SALDO = "Importe saldo (no editable)";
    public static final String TEXTO_CANTIDAD_DIA = "Cantidad de día (no vacía)";
    public static final String TEXTO_INTERES = "Interés (no editable)";

    public entAhorroProgramadoDetalle() {
        this.iid = 0;
        this.icuota = 0;
        this.dfechaVencimiento = null;
        this.uimporte = 0.0;
        this.upago = 0.0;
        this.usaldo = 0.0;
        this.icantidadDia = 0;
        this.uinteres = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entAhorroProgramadoDetalle(int iid, int icuota, java.util.Date dfechaVencimiento, double uimporte, double upago, double usaldo, int icantidadDia, double uinteres, short hestadoRegistro) {
        this.iid = iid;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.uimporte = uimporte;
        this.upago = upago;
        this.usaldo = usaldo;
        this.icantidadDia = icantidadDia;
        this.uinteres = uinteres;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int icuota, java.util.Date dfechaVencimiento, double uimporte, double upago, double usaldo, int icantidadDia, double uinteres, short hestadoRegistro) {
        this.iid = iid;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.uimporte = uimporte;
        this.upago = upago;
        this.usaldo = usaldo;
        this.icantidadDia = icantidadDia;
        this.uinteres = uinteres;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entAhorroProgramadoDetalle copiar(entAhorroProgramadoDetalle destino) {
        destino.setEntidad(this.getId(), this.getCuota(), this.getFechaVencimiento(), this.getImporte(), this.getPago(), this.getSaldo(), this.getCantidadDia(), this.getInteres(), this.getEstadoRegistro());
        return destino;
    }
    
    public entAhorroProgramadoDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setCuota(rs.getInt("numerocuota")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setImporte(rs.getDouble("montocapital")); }
        catch(Exception e) {}
        try { this.setPago(0); }
        catch(Exception e) {}
        try { this.setSaldo(0); }
        catch(Exception e) {}
        try { this.setCantidadDia(0); }
        catch(Exception e) {}
        try { this.setInteres(0); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setImporte(double uimporte) {
        this.uimporte = uimporte;
    }

    public void setPago(double upago) {
        this.upago = upago;
    }

    public void setSaldo(double usaldo) {
        this.usaldo = usaldo;
    }

    public void setCantidadDia(int icantidadDia) {
        this.icantidadDia = icantidadDia;
    }

    public void setInteres(double uinteres) {
        this.uinteres = uinteres;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getCuota() {
        return this.icuota;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getImporte() {
        return this.uimporte;
    }

    public double getPago() {
        return this.upago;
    }

    public double getSaldo() {
        return this.usaldo;
    }

    public int getCantidadDia() {
        return this.icantidadDia;
    }

    public double getInteres() {
        return this.uinteres;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getPago()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PAGO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT ahorroprogramado("+
            this.getId()+","+
            this.getCuota()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaVencimiento())+","+
            this.getImporte()+","+
            this.getPago()+","+
            this.getSaldo()+","+
            this.getCantidadDia()+","+
            this.getInteres()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
