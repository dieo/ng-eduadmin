/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDescargaDato {
    
    private int iid;
    private int iidMovimiento;
    private int icuota;
    private java.util.Date dfechaVencimiento;
    private double umontoCapital;
    private double umontoInteres;
    private double usaldoCapital;
    private double usaldoInteres;
    private int iidSocio;
    private String scedula;
    private int iidCuenta;
    private int iidEntidad;
    private int inumeroOperacion;
    private String sprioridad;
    private String stabla;
    private int iplazo;
    private java.util.Date dfechaOperacion;
    
    private short hestadoRegistro;
    private String smensaje;

    public especifico.entArchivoGiraduria socio = new especifico.entArchivoGiraduria();
    
    public entDescargaDato() {
        this.iid = 0;
        this.iidMovimiento = 0;
        this.icuota = 0;
        this.dfechaVencimiento = null;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.usaldoCapital = 0.0;
        this.usaldoInteres = 0.0;
        this.iidSocio = 0;
        this.scedula = "";
        this.iidCuenta = 0;
        this.iidEntidad = 0;
        this.inumeroOperacion = 0;
        this.sprioridad = "";
        this.stabla = "";
        this.iplazo = 0;
        this.dfechaOperacion = null;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDescargaDato(int iid, int iidMovimiento, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double usaldoCapital, double usaldoInteres, int iidSocio, String scedula, int iidCuenta, int iidEntidad, int inumeroOperacion, String sprioridad, String stabla, int iplazo, java.util.Date dfechaOperacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.usaldoCapital = usaldoCapital;
        this.usaldoInteres = usaldoInteres;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.iidCuenta = iidCuenta;
        this.iidEntidad = iidEntidad;
        this.inumeroOperacion = inumeroOperacion;
        this.sprioridad = sprioridad;
        this.stabla = stabla;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
     public void setEntidad(int iid, int iidMovimiento, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double usaldoCapital, double usaldoInteres, int iidSocio, String scedula, int iidCuenta, int iidEntidad, int inumeroOperacion, String sprioridad, String stabla, int iplazo, java.util.Date dfechaOperacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.usaldoCapital = usaldoCapital;
        this.usaldoInteres = usaldoInteres;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.iidCuenta = iidCuenta;
        this.iidEntidad = iidEntidad;
        this.inumeroOperacion = inumeroOperacion;
        this.sprioridad = sprioridad;
        this.stabla = stabla;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDescargaDato copiar(entDescargaDato destino) {
        destino.setEntidad(this.getId(), this.getIdMovimiento(), this.getCuota(), this.getFechaVencimiento(), this.getMontoCapital(), this.getMontoInteres(), this.getSaldoCapital(), this.getSaldoInteres(), this.getIdSocio(), this.getCedula(), this.getIdCuenta(), this.getIdEntidad(), this.getNumeroOperacion(), this.getPrioridad(), this.getTabla(), this.getPlazo(), this.getFechaOperacion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entDescargaDato cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setCuota(rs.getInt("numerocuota")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setMontoCapital(rs.getDouble("montocapital")); }
        catch(Exception e) {}
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) {}
        try { this.setSaldoCapital(rs.getDouble("saldocapital")); }
        catch(Exception e) {}
        try { this.setSaldoInteres(rs.getDouble("saldointeres")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setIdEntidad(rs.getInt("identidad")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setPrioridad(rs.getString("prioridad")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        this.socio.cargar(rs);
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }
    
    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }
    
    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }
    
    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }
    
    public void setSaldoCapital(double usaldoCapital) {
        this.usaldoCapital = usaldoCapital;
    }
    
    public void setSaldoInteres(double usaldoInteres) {
        this.usaldoInteres = usaldoInteres;
    }
    
    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setIdEntidad(int iidEntidad) {
        this.iidEntidad = iidEntidad;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setPrioridad(String sprioridad) {
        this.sprioridad = sprioridad;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }
    
    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public int getCuota() {
        return this.icuota;
    }
    
    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }
    
    public double getMontoCapital() {
        return this.umontoCapital;
    }
    
    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public double getSaldoCapital() {
        return this.usaldoCapital;
    }
    
    public double getSaldoInteres() {
        return this.usaldoInteres;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public int getIdEntidad() {
        return this.iidEntidad;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public String getPrioridad() {
        return this.sprioridad;
    }
    
    public String getTabla() {
        return this.stabla;
    }
    
    public int getPlazo() {
        return this.iplazo;
    }
    
    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public String getSentencia() {
        return "SELECT archivogiraduria("+
            this.getId()+","+
            this.getCuota()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaVencimiento())+","+
            this.getSaldoCapital()+","+
            this.getSaldoInteres()+","+
            this.getIdSocio()+","+
            this.getIdCuenta()+","+
            utilitario.utiCadena.getTextoGuardado(this.getPrioridad())+","+
            this.getEstadoRegistro()+")";
    }

    /*public String getConsultaSocio(int iidGiraduria, int iidRubro, java.util.Date dperiodo, int iidSocio) {
        return "SELECT * FROM ("+
            "SELECT op.tipo, op.tabla, op.idmovimiento, op.idsocio, op.cedula, op.idcuenta, op.numerooperacion, op.plazo, op.fechaoperacion, op.identidad, cu.prioridad, dop.id, dop.numerocuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital, dop.saldointeres FROM ("+
            "SELECT 'M' AS tipo, 'of' AS tabla, of.id AS idmovimiento, of.idsocio, '' AS cedula, of.idcuenta, of.numerooperacion, of.plazo, of.fechaoperacion, 0 AS identidad "+
            "FROM operacionfija AS of WHERE of.idsocio="+iidSocio+" "+
            "UNION "+
            "SELECT 'M' AS tipo, 'mo' AS tabla, mo.id AS idmovimiento, mo.idsocio, '' AS cedula, mo.idcuenta, mo.numerooperacion, mo.plazoaprobado AS plazo, mo.fechaaprobado AS fechaoperacion, 0 AS identidad "+
            "FROM movimiento AS mo WHERE mo.idsocio="+iidSocio+" "+
            "UNION "+
            "SELECT 'M' AS tipo, 'ap' AS tabla, ap.id AS idmovimiento, ap.idsocio, '' AS cedula, ap.idcuenta, ap.numerooperacion, ap.plazo, ap.fechaoperacion, 0 AS identidad "+
            "FROM ahorroprogramado AS ap WHERE ap.idsocio="+iidSocio+" "+
            "UNION "+
            "SELECT 'M' AS tipo, 'js' AS tabla, js.id AS idmovimiento, js.idsocio, '' AS cedula, js.idcuenta, js.numerooperacion, 0 AS plazo, js.fechaoperacion, 0 AS identidad "+
            "FROM fondojuridicosepelio AS js WHERE js.idsocio="+iidSocio+" "+
            ") AS op "+
            "LEFT JOIN cuenta AS cu ON op.idcuenta=cu.id "+
            "LEFT JOIN detalleoperacion AS dop ON op.idmovimiento=dop.idmovimiento AND op.tabla=dop.tabla "+
            "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" "+
            "UNION "+
            "SELECT 'E' AS tipo, 'ce' AS tabla, oe.id AS idmovimiento, oe.idsocio, oe.cedula, oe.idcuenta, 0 AS numerooperacion, oe.plazo, oe.fecha AS fechaoperacion, 0 AS identidad, cu.prioridad, doe.id, doe.numerocuota, doe.fechavencimiento, doe.montocapital, 0 AS montointeres, doe.saldocapital, 0 AS saldointeres "+
            "FROM detalleoperacionexterno AS doe "+
            "LEFT JOIN operacionexterna AS oe ON doe.idmovimiento=oe.id "+
            "LEFT JOIN cuenta AS cu ON oe.idcuenta=cu.id "+
            "WHERE idsocio="+iidSocio+" AND doe.saldocapital>0 AND TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dperiodo)+"' "+
            ") AS detalle "+
            "ORDER BY tipo DESC, fechavencimiento DESC, prioridad, saldocapital";
    }*/
    
    /*public String getConsultaSocioConEstado(int iidGiraduria, int iidRubro, java.util.Date dperiodo, int iidSocio) {
        return 
            "SELECT * FROM (SELECT op.tipo, op.tabla, op.idmovimiento, op.idsocio, op.cedula, op.idcuenta, op.numerooperacion, op.plazo, op.fechaoperacion, op.identidad, cu.prioridad, dop.id, dop.numerocuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital, dop.saldointeres FROM ("+
                "SELECT 'M' AS tipo, 'of' AS tabla, of.id AS idmovimiento, of.idsocio, '' AS cedula, of.idcuenta, of.numerooperacion, of.plazo, of.fechaoperacion, 0 AS identidad "+
                "FROM operacionfija AS of WHERE of.idsocio="+iidSocio+" AND of.idestado=86 "+
                "UNION "+
                "SELECT 'M' AS tipo, 'mo' AS tabla, mo.id AS idmovimiento, mo.idsocio, '' AS cedula, mo.idcuenta, mo.numerooperacion, mo.plazoaprobado AS plazo, mo.fechaaprobado AS fechaoperacion, 0 AS identidad "+
                "FROM movimiento AS mo WHERE mo.idsocio="+iidSocio+" AND (mo.idestado=179 OR mo.idestado=52) "+
                "UNION "+
                "SELECT 'M' AS tipo, 'ap' AS tabla, ap.id AS idmovimiento, ap.idsocio, '' AS cedula, ap.idcuenta, ap.numerooperacion, ap.plazo, ap.fechaoperacion, 0 AS identidad "+
                "FROM ahorroprogramado AS ap WHERE ap.idsocio="+iidSocio+" AND ap.idestado=53 "+
                "UNION "+
                "SELECT 'M' AS tipo, 'js' AS tabla, js.id AS idmovimiento, js.idsocio, '' AS cedula, js.idcuenta, js.numerooperacion, 0 AS plazo, js.fechaoperacion, 0 AS identidad "+
                "FROM fondojuridicosepelio AS js WHERE js.idsocio="+iidSocio+" AND js.idestado=57 "+
            ") AS op "+
            "LEFT JOIN cuenta AS cu ON op.idcuenta=cu.id "+
            "LEFT JOIN detalleoperacion AS dop ON op.idmovimiento=dop.idmovimiento AND op.tabla=dop.tabla "+
            "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" "+
            "UNION "+
            "SELECT 'E' AS tipo, 'ce' AS tabla, oe.id AS idmovimiento, oe.idsocio, oe.cedula, oe.idcuenta, 0 AS numerooperacion, oe.plazo, oe.fecha AS fechaoperacion, 0 AS identidad, cu.prioridad, doe.id, doe.numerocuota, doe.fechavencimiento, doe.montocapital, 0 AS montointeres, doe.saldocapital, 0 AS saldointeres "+
            "FROM detalleoperacionexterno AS doe "+
            "LEFT JOIN operacionexterna AS oe ON doe.idmovimiento=oe.id "+
            "LEFT JOIN cuenta AS cu ON oe.idcuenta=cu.id "+
            "WHERE idsocio="+iidSocio+" AND doe.saldocapital>0 AND TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dperiodo)+"' "+
            ") AS detalle "+
            "ORDER BY tipo DESC, fechavencimiento DESC, prioridad, saldocapital";
    }*/
    
    public String getConsultaConEstadoxxx(java.util.Date dperiodo) {
        return // socios ACTIVO
            "SELECT * FROM ("+
            // EXCLUSIVAMENTE PARA LAS DEMAS CUENTAS, EXCEPTUANDO LOS SEGUROS SAMAP Y SANTA CLARA
            "SELECT * FROM ("+
            "SELECT op.tipo, op.tabla, op.idmovimiento, op.idsocio, op.cedula, op.idcuenta2 AS idcuenta, op.numerooperacion, op.plazo, op.fechaoperacion, op.identidad, cu.prioridad, dop.id, dop.numerocuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital, dop.saldointeres FROM ("+
                "select 'M' as tipo, " +
                //"select case " +
                //"   when (ci.idcuenta=17 or ci.idcuenta=85 or ci.idcuenta=188) and ci.tabla='of' then 'N' " +
                //"   when ci.idcuenta<>17 and ci.idcuenta<>85 and ci.idcuenta<>188 then 'M' end as tipo, " +
                "ci.tabla, ci.idmovimiento, ci.idsocio, ci.cedula, "+
                "case " +
                "when ci.idcuenta=35 then 104 " +
                "else ci.idcuenta end as idcuenta2, " +
                "ci.numerooperacion, ci.plazo, ci.fechaoperacion, ci.identidad " +
                "from cierre as ci " +
                "where ci.fechacierre="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" and ci.idgiraduria<>7 and ci.tabla<>'ce' and ci.idcuenta<>14 and ci.idcuenta<>48 and ((ci.idestadosocio=20 and (ci.idgiraduria=1 or ci.idgiraduria=2 or ci.idgiraduria=3 or ci.idgiraduria=4 or ci.idgiraduria=6)) or ci.idestadosocio=224) "+
                "group by tipo, ci.tabla, ci.idmovimiento, ci.idsocio, ci.cedula, idcuenta2, ci.numerooperacion, ci.plazo, ci.fechaoperacion, ci.identidad " +
            ") AS op "+
            "LEFT JOIN cuenta AS cu ON idcuenta2=cu.id " +
            "LEFT JOIN detalleoperacion AS dop ON op.idmovimiento=dop.idmovimiento AND op.tabla=dop.tabla "+
            "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" "+
            "UNION "+
            "SELECT op.tipo, op.tabla, op.idmovimiento, op.idsocio, op.cedula, op.idcuenta, op.numerooperacion, op.plazo, op.fechaoperacion, op.identidad, cu.prioridad, doe.id, doe.numerocuota, doe.fechavencimiento, doe.montocapital, 0 AS montointeres, doe.saldocapital, 0 AS saldointeres FROM ("+
                "select case " +
                "   when ci.tabla='ce' then 'E' end as tipo, " +
                "ci.tabla, ci.idmovimiento, ci.idsocio, ci.cedula, ci.idcuenta, ci.numerooperacion, ci.plazo, ci.fechaoperacion, ci.identidad " +
                "from cierre as ci " +
                "where ci.fechacierre="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" and ci.tabla='ce' and ci.idestadosocio=20 "+
            ") AS op "+
            "LEFT JOIN cuenta AS cu ON idcuenta=cu.id " +
            "LEFT JOIN detalleoperacionexterno AS doe ON op.idmovimiento=doe.idmovimiento "+
            "WHERE doe.saldocapital>0 AND doe.fechavencimiento<="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" "+
            ") AS detalle1 "+
            "UNION "+
            // EXCLUSIVAMENTE PARA LOS SEGUROS SAMAP Y SANTA CLARA
            "SELECT * FROM ("+
            "SELECT op.tipo, op.tabla, op.idmovimiento, op.idsocio, op.cedula, op.idcuenta2 AS idcuenta, op.numerooperacion, op.plazo, op.fechaoperacion, op.identidad, cu.prioridad, dop.id, dop.numerocuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital, dop.saldointeres FROM ("+
                "select 'M' as tipo, " +
                //"select case " +
                //"   when (ci.idcuenta=17 or ci.idcuenta=85 or ci.idcuenta=188) and ci.tabla='of' then 'N' " +
                //"   when ci.idcuenta<>17 and ci.idcuenta<>85 and ci.idcuenta<>188 then 'M' end as tipo, " +
                "ci.tabla, ci.idmovimiento, ci.idsocio, ci.cedula, "+
                "case " +
                "when ci.idcuenta=35 then 104 " +
                "else ci.idcuenta end as idcuenta2, " +
                "ci.numerooperacion, ci.plazo, ci.fechaoperacion, ci.identidad " +
                "from cierre as ci " +
                "where ci.fechacierre="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" and ci.idgiraduria<>7 and ci.tabla<>'ce' and (ci.idcuenta=14 or ci.idcuenta=48) and ((ci.idestadosocio=20 and (ci.idgiraduria=1 or ci.idgiraduria=2 or ci.idgiraduria=3 or ci.idgiraduria=4 or ci.idgiraduria=6)) or ci.idestadosocio=224) "+
                "group by tipo, ci.tabla, ci.idmovimiento, ci.idsocio, ci.cedula, idcuenta2, ci.numerooperacion, ci.plazo, ci.fechaoperacion, ci.identidad " +
            ") AS op "+
            "LEFT JOIN cuenta AS cu ON idcuenta2=cu.id " +
            "LEFT JOIN detalleoperacion AS dop ON op.idmovimiento=dop.idmovimiento AND op.tabla=dop.tabla "+
            "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.fechavencimiento="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" "+
            ") AS detalle2 "+
            ") AS detallegeneral "+
            //"LEFT JOIN ("+
            //"SELECT ag.id, ag.cedula, SUM(ag.monto) AS monto, ag.periodo, s.id AS idsocio, s.nombre nombre, s.apellido, s.direccion, s.telefonocelular AS telefono "+
            //"FROM archivogiraduria AS ag "+
            //"LEFT JOIN socio AS s ON ag.cedula=s.cedula "+
            //"WHERE ag.procesado='FALSE' AND ag.periodo="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" "+
            //"GROUP BY ag.id, ag.cedula, ag.periodo, s.id, s.nombre, s.apellido, s.direccion, s.telefonocelular, ag.procesado "+
            //") AS descuento ON descuento.cedula=detallegeneral.cedula "+
            //"WHERE detallegeneral.cedula='3787880' or detallegeneral.cedula='3697156' or detallegeneral.cedula='3434976' or detallegeneral.cedula='3764222' or detallegeneral.cedula='1829399' or detallegeneral.cedula='1723665' or detallegeneral.cedula='3524919' or detallegeneral.cedula='3640827' or detallegeneral.cedula='3756243' or detallegeneral.cedula='3730556' or detallegeneral.cedula='3503941' or detallegeneral.cedula='3735922' or detallegeneral.cedula='3771305' or detallegeneral.cedula='3642075' or detallegeneral.cedula='1805945' or detallegeneral.cedula='1828966' or detallegeneral.cedula='3728796' or detallegeneral.cedula='3797905' or detallegeneral.cedula='3426963' or detallegeneral.cedula='3746875' or detallegeneral.cedula='3650845' or detallegeneral.cedula='1817594' or detallegeneral.cedula='3683361' or detallegeneral.cedula='3750072'"+
            //"WHERE cedula='4102929' "+
            //"WHERE cedula='3861938' "+
            "WHERE (cedula = '2256283' or " +
            "cedula = '3766091' or " +
            "cedula = '4103659' or " +
            "cedula = '4105231' or " +
            "cedula = '4106427' or " +
            "cedula = '4106958' or " +
            "cedula = '4108822' or " +
            "cedula = '4111904' or " +
            "cedula = '4113266' or " +
            "cedula = '4114062' or " +
            "cedula = '4114096' or " +
            "cedula = '4114272' or " +
            "cedula = '4120963' or " +
            "cedula = '4122451' or " +
            "cedula = '4123906' or " +
            "cedula = '4125100' or " +
            "cedula = '4125138' or " +
            "cedula = '4126953' or " +
            "cedula = '4127127' or " +
            "cedula = '4127752' or " +
            "cedula = '4128321' or " +
            "cedula = '4130308' or " +
            "cedula = '4130347' or " +
            "cedula = '4131455' or " +
            "cedula = '4134165' or " +
            "cedula = '4134451' or " +
            "cedula = '4134590' or " +
            "cedula = '4135411' or " +
            "cedula = '4136656' or " +
            "cedula = '4136757' or " +
            "cedula = '4136876' or " +
            "cedula = '4137748' or " +
            "cedula = '4138872' or " +
            "cedula = '4139899' or " +
            "cedula = '4140987' or " +
            "cedula = '4141092' or " +
            "cedula = '4144608' or " +
            "cedula = '4147407' or " +
            "cedula = '4147417' or " +
            "cedula = '4149468' or " +
            "cedula = '4151452' or " +
            "cedula = '4151925' or " +
            "cedula = '4153490' or " +
            "cedula = '4154318' or " +
            "cedula = '4155039' or " +
            "cedula = '4156528' or " +
            "cedula = '4159237' or " +
            "cedula = '4159920' or " +
            "cedula = '4161056' or " +
            "cedula = '4164393' or " +
            "cedula = '4164671' or " +
            "cedula = '4168909' or " +
            "cedula = '4169771' or " +
            "cedula = '4172390' or " +
            "cedula = '4173268' or " +
            "cedula = '4173270' or " +
            "cedula = '4173448' or " +
            "cedula = '4174359' or " +
            "cedula = '4174653' or " +
            "cedula = '4174795' or " +
            "cedula = '4175106' or " +
            "cedula = '4175162' or " +
            "cedula = '4176095' or " +
            "cedula = '4176587' or " +
            "cedula = '4177871' or " +
            "cedula = '4181643' or " +
            "cedula = '4181986' or " +
            "cedula = '4182037' or " +
            "cedula = '4182178' or " +
            "cedula = '4182267' or " +
            "cedula = '4182334' or " +
            "cedula = '4182777' or " +
            "cedula = '4182787' or " +
            "cedula = '4184297' or " +
            "cedula = '4184757' or " +
            "cedula = '4184974' or " +
            "cedula = '4184978' or " +
            "cedula = '4185147' or " +
            "cedula = '4185162' or " +
            "cedula = '4186270' or " +
            "cedula = '4186436' or " +
            "cedula = '4186474' or " +
            "cedula = '4186919' or " +
            "cedula = '4187031' or " +
            "cedula = '4187586' or " +
            "cedula = '4188287' or " +
            "cedula = '4188619' or " +
            "cedula = '4189531' or " +
            "cedula = '4189712' or " +
            "cedula = '4189858' or " +
            "cedula = '4189953' or " +
            "cedula = '4190047' or " +
            "cedula = '4191343' or " +
            "cedula = '4191367' or " +
            "cedula = '4191439' or " +
            "cedula = '4191861' or " +
            "cedula = '4192391' or " +
            "cedula = '4192418' or " +
            "cedula = '4192509' or " +
            "cedula = '4192817' or " +
            "cedula = '4193451' or " +
            "cedula = '4193595' or " +
            "cedula = '4194273' or " +
            "cedula = '4195047' or " +
            "cedula = '4195768' or " +
            "cedula = '4196001' or " +
            "cedula = '4196827' or " +
            "cedula = '4197006' or " +
            "cedula = '4199252' or " +
            "cedula = '4201215' or " +
            "cedula = '4201530' or " +
            "cedula = '4203733' or " +
            "cedula = '4204498' or " +
            "cedula = '4205123' or " +
            "cedula = '4205779' or " +
            "cedula = '4205994' or " +
            "cedula = '4206299' or " +
            "cedula = '4206496' or " +
            "cedula = '4207111' or " +
            "cedula = '4207903' or " +
            "cedula = '4207986' or " +
            "cedula = '4208223' or " +
            "cedula = '4209032' or " +
            "cedula = '4209251' or " +
            "cedula = '4209727' or " +
            "cedula = '4210141' or " +
            "cedula = '4211012' or " +
            "cedula = '4211411' or " +
            "cedula = '4211421' or " +
            "cedula = '4211474' or " +
            "cedula = '4211862' or " +
            "cedula = '4212205' or " +
            "cedula = '4212895' or " +
            "cedula = '4213404' or " +
            "cedula = '4214396' or " +
            "cedula = '4214773' or " +
            "cedula = '4214949' or " +
            "cedula = '4215009' or " +
            "cedula = '4215583' or " +
            "cedula = '4216108' or " +
            "cedula = '4216561' or " +
            "cedula = '4217294' or " +
            "cedula = '4217483' or " +
            "cedula = '4217598' or " +
            "cedula = '4217876' or " +
            "cedula = '4218250' or " +
            "cedula = '4218706' or " +
            "cedula = '4219481' or " +
            "cedula = '4219817' or " +
            "cedula = '4219935' or " +
            "cedula = '4220007' or " +
            "cedula = '4220478' or " +
            "cedula = '4220836' or " +
            "cedula = '4221542' or " +
            "cedula = '4221629' or " +
            "cedula = '4222159' or " +
            "cedula = '4222702' or " +
            "cedula = '4222978' or " +
            "cedula = '4223460' or " +
            "cedula = '4223763' or " +
            "cedula = '4223869' or " +
            "cedula = '4224270' or " +
            "cedula = '4224592' or " +
            "cedula = '4224617' or " +
            "cedula = '4224848' or " +
            "cedula = '4225903' or " +
            "cedula = '4226031' or " +
            "cedula = '4226214' or " +
            "cedula = '4226327' or " +
            "cedula = '4227129' or " +
            "cedula = '4227475' or " +
            "cedula = '4228300' or " +
            "cedula = '4228661' or " +
            "cedula = '4229493' or " +
            "cedula = '4229977' or " +
            "cedula = '4230846' or " +
            "cedula = '4231581' or " +
            "cedula = '4232178' or " +
            "cedula = '4232815' or " +
            "cedula = '4232902' or " +
            "cedula = '4233182' or " +
            "cedula = '4233657' or " +
            "cedula = '4233874' or " +
            "cedula = '4235189' or " +
            "cedula = '4235659' or " +
            "cedula = '4238650' or " +
            "cedula = '4238868' or " +
            "cedula = '4239137' or " +
            "cedula = '4240367' or " +
            "cedula = '4241759' or " +
            "cedula = '4241813' or " +
            "cedula = '4241817' or " +
            "cedula = '4242395' or " +
            "cedula = '4242631' or " +
            "cedula = '4243582' or " +
            "cedula = '4243736' or " +
            "cedula = '4246445' or " +
            "cedula = '4246464' or " +
            "cedula = '4247082' or " +
            "cedula = '4247095' or " +
            "cedula = '4249616' or " +
            "cedula = '4250758' or " +
            "cedula = '4251819' or " +
            "cedula = '4252651' or " +
            "cedula = '4254176' or " +
            "cedula = '4254571' or " +
            "cedula = '4254917' or " +
            "cedula = '4256987' or " +
            "cedula = '4257360' or " +
            "cedula = '4257660' or " +
            "cedula = '4258858' or " +
            "cedula = '4259650' or " +
            "cedula = '4261345' or " +
            "cedula = '4263285' or " +
            "cedula = '4265176' or " +
            "cedula = '4266942' or " +
            "cedula = '4267131' or " +
            "cedula = '4267350' or " +
            "cedula = '4267556' or " +
            "cedula = '4267914' or " +
            "cedula = '4269182' or " +
            "cedula = '4269817' or " +
            "cedula = '4271005' or " +
            "cedula = '4273112' or " +
            "cedula = '4273492' or " +
            "cedula = '4276434' or " +
            "cedula = '4276878' or " +
            "cedula = '4278328' or " +
            "cedula = '4279187' or " +
            "cedula = '4279662' or " +
            "cedula = '4279816' or " +
            "cedula = '4279842' or " +
            "cedula = '4280802' or " +
            "cedula = '4280940' or " +
            "cedula = '4281370' or " +
            "cedula = '4281973' or " +
            "cedula = '4283125' or " +
            "cedula = '4283781' or " +
            "cedula = '4285367' or " +
            "cedula = '4287008' or " +
            "cedula = '4287044' or " +
            "cedula = '4287899' or " +
            "cedula = '4287936' or " +
            "cedula = '4289281' or " +
            "cedula = '4291289' or " +
            "cedula = '4292043' or " +
            "cedula = '4293879' or " +
            "cedula = '4294309' or " +
            "cedula = '4294618' or " +
            "cedula = '4295996' or " +
            "cedula = '4300896' or " +
            "cedula = '4300952' or " +
            "cedula = '4301575' or " +
            "cedula = '4301735' or " +
            "cedula = '4301894' or " +
            "cedula = '4303637' or " +
            "cedula = '4304610' or " +
            "cedula = '4305049' or " +
            "cedula = '4308827' or " +
            "cedula = '4309066' or " +
            "cedula = '4309831' or " +
            "cedula = '4311411' or " +
            "cedula = '4311505' or " +
            "cedula = '4311887' or " +
            "cedula = '4312174' or " +
            "cedula = '4312418' or " +
            "cedula = '4312419' or " +
            "cedula = '4312473' or " +
            "cedula = '4312532' or " +
            "cedula = '4312952' or " +
            "cedula = '4313060' or " +
            "cedula = '4313635' or " +
            "cedula = '4313731' or " +
            "cedula = '4315282' or " +
            "cedula = '4315637' or " +
            "cedula = '4315778' or " +
            "cedula = '4316110' or " +
            "cedula = '4316188' or " +
            "cedula = '4316240' or " +
            "cedula = '4316460' or " +
            "cedula = '4318018' or " +
            "cedula = '4318821' or " +
            "cedula = '4319268' or " +
            "cedula = '4321698' or " +
            "cedula = '4321733' or " +
            "cedula = '4322006' or " +
            "cedula = '4322607' or " +
            "cedula = '4323423' or " +
            "cedula = '4323951' or " +
            "cedula = '4324070' or " +
            "cedula = '4325329' or " +
            "cedula = '4325815' or " +
            "cedula = '4325816' or " +
            "cedula = '4326356' or " +
            "cedula = '4328649' or " +
            "cedula = '4328695' or " +
            "cedula = '4330535' or " +
            "cedula = '4331401' or " +
            "cedula = '4331684' or " +
            "cedula = '4332657' or " +
            "cedula = '4332781' or " +
            "cedula = '4334491' or " +
            "cedula = '4334659' or " +
            "cedula = '4335165' or " +
            "cedula = '4335236' or " +
            "cedula = '4335913' or " +
            "cedula = '4336075' or " +
            "cedula = '4336097' or " +
            "cedula = '4336190' or " +
            "cedula = '4337293' or " +
            "cedula = '4337563' or " +
            "cedula = '4337805' or " +
            "cedula = '4339025' or " +
            "cedula = '4339125' or " +
            "cedula = '4339304' or " +
            "cedula = '4339315' or " +
            "cedula = '4339626' or " +
            "cedula = '4341000' or " +
            "cedula = '4341342' or " +
            "cedula = '4341640' or " +
            "cedula = '4342344' or " +
            "cedula = '4342777' or " +
            "cedula = '4343246' or " +
            "cedula = '4343328' or " +
            "cedula = '4343501' or " +
            "cedula = '4343553' or " +
            "cedula = '4343584' or " +
            "cedula = '4344147' or " +
            "cedula = '4344526' or " +
            "cedula = '4345218' or " +
            "cedula = '4345261' or " +
            "cedula = '4345438' or " +
            "cedula = '4345820' or " +
            "cedula = '4346456' or " +
            "cedula = '4346554' or " +
            "cedula = '4347172' or " +
            "cedula = '4347235' or " +
            "cedula = '4347400' or " +
            "cedula = '4348086' or " +
            "cedula = '4348239' or " +
            "cedula = '4348354' or " +
            "cedula = '4349119' or " +
            "cedula = '4349300' or " +
            "cedula = '4350821' or " +
            "cedula = '4350849' or " +
            "cedula = '4351152' or " +
            "cedula = '4351214' or " +
            "cedula = '4351596' or " +
            "cedula = '4353499' or " +
            "cedula = '4356134' or " +
            "cedula = '4356600' or " +
            "cedula = '4356898' or " +
            "cedula = '4357592' or " +
            "cedula = '4358223' or " +
            "cedula = '4358432' or " +
            "cedula = '4359431' or " +
            "cedula = '4359442' or " +
            "cedula = '4359561' or " +
            "cedula = '4359724' or " +
            "cedula = '4360000' or " +
            "cedula = '4360216' or " +
            "cedula = '4361029' or " +
            "cedula = '4361880' or " +
            "cedula = '4361950' or " +
            "cedula = '4362079' or " +
            "cedula = '4362527' or " +
            "cedula = '4362600' or " +
            "cedula = '4362615' or " +
            "cedula = '4363159' or " +
            "cedula = '4363241' or " +
            "cedula = '4364374' or " +
            "cedula = '4365269' or " +
            "cedula = '4365270' or " +
            "cedula = '4365825' or " +
            "cedula = '4366144' or " +
            "cedula = '4366314' or " +
            "cedula = '4366327' or " +
            "cedula = '4367079' or " +
            "cedula = '4367116' or " +
            "cedula = '4367668' or " +
            "cedula = '4368072' or " +
            "cedula = '4368134' or " +
            "cedula = '4368404' or " +
            "cedula = '4368455' or " +
            "cedula = '4369016' or " +
            "cedula = '4369499' or " +
            "cedula = '4372051' or " +
            "cedula = '4372077' or " +
            "cedula = '4373227' or " +
            "cedula = '4373605' or " +
            "cedula = '4374320' or " +
            "cedula = '4374559' or " +
            "cedula = '4374719' or " +
            "cedula = '4375338' or " +
            "cedula = '4375955' or " +
            "cedula = '4376248' or " +
            "cedula = '4376454' or " +
            "cedula = '4378322' or " +
            "cedula = '4378327' or " +
            "cedula = '4378445' or " +
            "cedula = '4378473' or " +
            "cedula = '4379110' or " +
            "cedula = '4380456' or " +
            "cedula = '4380740' or " +
            "cedula = '4380912' or " +
            "cedula = '4381175' or " +
            "cedula = '4381801' or " +
            "cedula = '4382234' or " +
            "cedula = '4383043' or " +
            "cedula = '4383357' or " +
            "cedula = '4384091' or " +
            "cedula = '4384218' or " +
            "cedula = '4384322' or " +
            "cedula = '4384577' or " +
            "cedula = '4384616' or " +
            "cedula = '4384740' or " +
            "cedula = '4384904' or " +
            "cedula = '4384915' or " +
            "cedula = '4385178' or " +
            "cedula = '4385659' or " +
            "cedula = '4385706' or " +
            "cedula = '4386586' or " +
            "cedula = '4386914' or " +
            "cedula = '4387099' or " +
            "cedula = '4388848' or " +
            "cedula = '4389362' or " +
            "cedula = '4389379' or " +
            "cedula = '4389508' or " +
            "cedula = '4391173' or " +
            "cedula = '4391755' or " +
            "cedula = '4392310' or " +
            "cedula = '4392793' or " +
            "cedula = '4393069' or " +
            "cedula = '4393107' or " +
            "cedula = '4393990' or " +
            "cedula = '4395118' or " +
            "cedula = '4395283' or " +
            "cedula = '4396158' or " +
            "cedula = '4396177' or " +
            "cedula = '4396508' or " +
            "cedula = '4399871' or " +
            "cedula = '4400815' or " +
            "cedula = '4402233' or " +
            "cedula = '4402788' or " +
            "cedula = '4404556' or " +
            "cedula = '4406462' or " +
            "cedula = '4406514' or " +
            "cedula = '4407220' or " +
            "cedula = '4407426' or " +
            "cedula = '4407431' or " +
            "cedula = '4407432' or " +
            "cedula = '4407538' or " +
            "cedula = '4408137' or " +
            "cedula = '4409509' or " +
            "cedula = '4409973' or " +
            "cedula = '4411092' or " +
            "cedula = '4411804' or " +
            "cedula = '4413344' or " +
            "cedula = '4413932' or " +
            "cedula = '4419502' or " +
            "cedula = '4420080' or " +
            "cedula = '4420271' or " +
            "cedula = '4420284' or " +
            "cedula = '4420780' or " +
            "cedula = '4421316' or " +
            "cedula = '4421436' or " +
            "cedula = '4423970' or " +
            "cedula = '4424610' or " +
            "cedula = '4427097' or " +
            "cedula = '4427230' or " +
            "cedula = '4427461' or " +
            "cedula = '4428207' or " +
            "cedula = '4429111' or " +
            "cedula = '4429116' or " +
            "cedula = '4429384' or " +
            "cedula = '4430047' or " +
            "cedula = '4430133' or " +
            "cedula = '4431365' or " +
            "cedula = '4434454' or " +
            "cedula = '4435899' or " +
            "cedula = '4436520' or " +
            "cedula = '4437178' or " +
            "cedula = '4439384' or " +
            "cedula = '4439501' or " +
            "cedula = '4441892' or " +
            "cedula = '4442359' or " +
            "cedula = '4442438' or " +
            "cedula = '4442521' or " +
            "cedula = '4442579' or " +
            "cedula = '4442710' or " +
            "cedula = '4442779' or " +
            "cedula = '4444553' or " +
            "cedula = '4445444' or " +
            "cedula = '4445467' or " +
            "cedula = '4446745' or " +
            "cedula = '4447957' or " +
            "cedula = '4448093' or " +
            "cedula = '4448475' or " +
            "cedula = '4449347' or " +
            "cedula = '4450711' or " +
            "cedula = '4452351' or " +
            "cedula = '4452960' or " +
            "cedula = '4453277' or " +
            "cedula = '4455289' or " +
            "cedula = '4456692' or " +
            "cedula = '4457489' or " +
            "cedula = '4458385' or " +
            "cedula = '4459367' or " +
            "cedula = '4461678' or " +
            "cedula = '4461846' or " +
            "cedula = '4462031' or " +
            "cedula = '4462698' or " +
            "cedula = '4463701' or " +
            "cedula = '4463762' or " +
            "cedula = '4463970' or " +
            "cedula = '4464090' or " +
            "cedula = '4464555' or " +
            "cedula = '4465948' or " +
            "cedula = '4471598' or " +
            "cedula = '4471648' or " +
            "cedula = '4472245' or " +
            "cedula = '4473191' or " +
            "cedula = '4473456' or " +
            "cedula = '4473801' or " +
            "cedula = '4473837' or " +
            "cedula = '4474019' or " +
            "cedula = '4474197' or " +
            "cedula = '4474707' or " +
            "cedula = '4475007' or " +
            "cedula = '4475248' or " +
            "cedula = '4478548' or " +
            "cedula = '4479159' or " +
            "cedula = '4479438' or " +
            "cedula = '4479782' or " +
            "cedula = '4480277' or " +
            "cedula = '4482613' or " +
            "cedula = '4482867' or " +
            "cedula = '4483143' or " +
            "cedula = '4483347' or " +
            "cedula = '4483594' or " +
            "cedula = '4484172' or " +
            "cedula = '4484199' or " +
            "cedula = '4484206' or " +
            "cedula = '4484442' or " +
            "cedula = '4484457' or " +
            "cedula = '4484523' or " +
            "cedula = '4484682' or " +
            "cedula = '4484706' or " +
            "cedula = '4484933' or " +
            "cedula = '4486511' or " +
            "cedula = '4486982' or " +
            "cedula = '4488718' or " +
            "cedula = '4489245' or " +
            "cedula = '4490300' or " +
            "cedula = '4491696' or " +
            "cedula = '4492645' or " +
            "cedula = '4492686' or " +
            "cedula = '4493951' or " +
            "cedula = '4494163' or " +
            "cedula = '4496251' or " +
            "cedula = '4497643' or " +
            "cedula = '4498002' or " +
            "cedula = '4498426' or " +
            "cedula = '4498890' or " +
            "cedula = '4499007' or " +
            "cedula = '4500319' or " +
            "cedula = '4502907' or " +
            "cedula = '4502923' or " +
            "cedula = '4504490' or " +
            "cedula = '4504636' or " +
            "cedula = '4505297' or " +
            "cedula = '4505738' or " +
            "cedula = '4506200' or " +
            "cedula = '4506706' or " +
            "cedula = '4507322' or " +
            "cedula = '4507696' or " +
            "cedula = '4507919' or " +
            "cedula = '4508292' or " +
            "cedula = '4508511' or " +
            "cedula = '4511101' or " +
            "cedula = '4511188' or " +
            "cedula = '4511233' or " +
            "cedula = '4511404' or " +
            "cedula = '4512936' or " +
            "cedula = '4513253' or " +
            "cedula = '4513941' or " +
            "cedula = '4514146' or " +
            "cedula = '4514250' or " +
            "cedula = '4514274' or " +
            "cedula = '4514569' or " +
            "cedula = '4514946' or " +
            "cedula = '4515947' or " +
            "cedula = '4516027' or " +
            "cedula = '4516092' or " +
            "cedula = '4516517' or " +
            "cedula = '4517730' or " +
            "cedula = '4518935' or " +
            "cedula = '4518990' or " +
            "cedula = '4519403' or " +
            "cedula = '4519414' or " +
            "cedula = '4519512' or " +
            "cedula = '4519542' or " +
            "cedula = '4519743' or " +
            "cedula = '4520382' or " +
            "cedula = '4521156' or " +
            "cedula = '4522350' or " +
            "cedula = '4522516' or " +
            "cedula = '4524102' or " +
            "cedula = '4524225' or " +
            "cedula = '4524241' or " +
            "cedula = '4524326' or " +
            "cedula = '4524563' or " +
            "cedula = '4525390' or " +
            "cedula = '4526300' or " +
            "cedula = '4526413' or " +
            "cedula = '4527180' or " +
            "cedula = '4527717' or " +
            "cedula = '4528251' or " +
            "cedula = '4528770' or " +
            "cedula = '4528815' or " +
            "cedula = '4530106' or " +
            "cedula = '4530760' or " +
            "cedula = '4530951' or " +
            "cedula = '4531575' or " +
            "cedula = '4532542' or " +
            "cedula = '4533377' or " +
            "cedula = '4533513' or " +
            "cedula = '4534090' or " +
            "cedula = '4534319' or " +
            "cedula = '4534788' or " +
            "cedula = '4535361' or " +
            "cedula = '4535402' or " +
            "cedula = '4535490' or " +
            "cedula = '4535523' or " +
            "cedula = '4536166' or " +
            "cedula = '4537164' or " +
            "cedula = '4537206' or " +
            "cedula = '4538201' or " +
            "cedula = '4538439' or " +
            "cedula = '4538520' or " +
            "cedula = '4538590' or " +
            "cedula = '4538958' or " +
            "cedula = '4540163' or " +
            "cedula = '4540433' or " +
            "cedula = '4540596' or " +
            "cedula = '4540644' or " +
            "cedula = '4540894' or " +
            "cedula = '4541404' or " +
            "cedula = '4542139' or " +
            "cedula = '4542348' or " +
            "cedula = '4543663' or " +
            "cedula = '4544363' or " +
            "cedula = '4545350' or " +
            "cedula = '4545798' or " +
            "cedula = '4546320' or " +
            "cedula = '4546551' or " +
            "cedula = '4547322' or " +
            "cedula = '4548837' or " +
            "cedula = '4550767' or " +
            "cedula = '4550782' or " +
            "cedula = '4551067' or " +
            "cedula = '4552522' or " +
            "cedula = '4553654' or " +
            "cedula = '4553788' or " +
            "cedula = '4554359' or " +
            "cedula = '4554450' or " +
            "cedula = '4554540' or " +
            "cedula = '4555258' or " +
            "cedula = '4555563' or " +
            "cedula = '4555821' or " +
            "cedula = '4557162' or " +
            "cedula = '4557841' or " +
            "cedula = '4558109' or " +
            "cedula = '4558118' or " +
            "cedula = '4558169' or " +
            "cedula = '4559005' or " +
            "cedula = '4559376' or " +
            "cedula = '4559441' or " +
            "cedula = '4559549' or " +
            "cedula = '4559550' or " +
            "cedula = '4559633' or " +
            "cedula = '4559814' or " +
            "cedula = '4561395' or " +
            "cedula = '4561842' or " +
            "cedula = '4562476' or " +
            "cedula = '4562483' or " +
            "cedula = '4563819' or " +
            "cedula = '4565566' or " +
            "cedula = '4565814' or " +
            "cedula = '4566206' or " +
            "cedula = '4566247' or " +
            "cedula = '4566283' or " +
            "cedula = '4566289' or " +
            "cedula = '4566454' or " +
            "cedula = '4566471' or " +
            "cedula = '4566999' or " +
            "cedula = '4567988' or " +
            "cedula = '4567992' or " +
            "cedula = '4568556' or " +
            "cedula = '4568895' or " +
            "cedula = '4569017' or " +
            "cedula = '4569193' or " +
            "cedula = '4570396' or " +
            "cedula = '4573009' or " +
            "cedula = '4573889' or " +
            "cedula = '4574221' or " +
            "cedula = '4574367' or " +
            "cedula = '4575937' or " +
            "cedula = '4576122' or " +
            "cedula = '4576812' or " +
            "cedula = '4577124' or " +
            "cedula = '4578033' or " +
            "cedula = '4578889' or " +
            "cedula = '4578991' or " +
            "cedula = '4580243' or " +
            "cedula = '4580778' or " +
            "cedula = '4581297' or " +
            "cedula = '4581471' or " +
            "cedula = '4581772' or " +
            "cedula = '4582173' or " +
            "cedula = '4583203' or " +
            "cedula = '4583563' or " +
            "cedula = '4586219' or " +
            "cedula = '4588398' or " +
            "cedula = '4589315' or " +
            "cedula = '4591314' or " +
            "cedula = '4592861' or " +
            "cedula = '4595780' or " +
            "cedula = '4596518' or " +
            "cedula = '4596634' or " +
            "cedula = '4597070' or " +
            "cedula = '4597141' or " +
            "cedula = '4599773' or " +
            "cedula = '4600231' or " +
            "cedula = '4600310' or " +
            "cedula = '4601117' or " +
            "cedula = '4601969' or " +
            "cedula = '4602267' or " +
            "cedula = '4602805' or " +
            "cedula = '4602877' or " +
            "cedula = '4603217' or " +
            "cedula = '4606509' or " +
            "cedula = '4606654' or " +
            "cedula = '4608664' or " +
            "cedula = '4608899' or " +
            "cedula = '4613712' or " +
            "cedula = '4614469' or " +
            "cedula = '4615280' or " +
            "cedula = '4620098' or " +
            "cedula = '4620454' or " +
            "cedula = '4620799' or " +
            "cedula = '4620940' or " +
            "cedula = '4622729' or " +
            "cedula = '4626772' or " +
            "cedula = '4627699' or " +
            "cedula = '4627706' or " +
            "cedula = '4628392' or " +
            "cedula = '4628657' or " +
            "cedula = '4628738' or " +
            "cedula = '4628753' or " +
            "cedula = '4628999' or " +
            "cedula = '4629434' or " +
            "cedula = '4630134' or " +
            "cedula = '4630561' or " +
            "cedula = '4630706' or " +
            "cedula = '4631204' or " +
            "cedula = '4634712' or " +
            "cedula = '4634834' or " +
            "cedula = '4635134' or " +
            "cedula = '4635951' or " +
            "cedula = '4637997' or " +
            "cedula = '4638190' or " +
            "cedula = '4638663' or " +
            "cedula = '4638954' or " +
            "cedula = '4639457' or " +
            "cedula = '4641442' or " +
            "cedula = '4641623' or " +
            "cedula = '4641771' or " +
            "cedula = '4642216' or " +
            "cedula = '4643790' or " +
            "cedula = '4645249' or " +
            "cedula = '4646292' or " +
            "cedula = '4646434' or " +
            "cedula = '4647558' or " +
            "cedula = '4648169' or " +
            "cedula = '4648923' or " +
            "cedula = '4649119' or " +
            "cedula = '4649553' or " +
            "cedula = '4649705' or " +
            "cedula = '4650145' or " +
            "cedula = '4650517' or " +
            "cedula = '4651887' or " +
            "cedula = '4652650' or " +
            "cedula = '4653440' or " +
            "cedula = '4654145' or " +
            "cedula = '4654255' or " +
            "cedula = '4654551' or " +
            "cedula = '4654794' or " +
            "cedula = '4655218' or " +
            "cedula = '4655840' or " +
            "cedula = '4657961' or " +
            "cedula = '4658069' or " +
            "cedula = '4658176' or " +
            "cedula = '4659196' or " +
            "cedula = '4659519' or " +
            "cedula = '4659665' or " +
            "cedula = '4660669' or " +
            "cedula = '4660963' or " +
            "cedula = '4661161' or " +
            "cedula = '4661327' or " +
            "cedula = '4662028' or " +
            "cedula = '4662593' or " +
            "cedula = '4662633' or " +
            "cedula = '4663495' or " +
            "cedula = '4663561' or " +
            "cedula = '4663693' or " +
            "cedula = '4663739' or " +
            "cedula = '4664607' or " +
            "cedula = '4664625' or " +
            "cedula = '4665371' or " +
            "cedula = '4666513' or " +
            "cedula = '4666686' or " +
            "cedula = '4667197' or " +
            "cedula = '4669244' or " +
            "cedula = '4669273' or " +
            "cedula = '4671329' or " +
            "cedula = '4672175' or " +
            "cedula = '4672376' or " +
            "cedula = '4673356' or " +
            "cedula = '4674395' or " +
            "cedula = '4674862' or " +
            "cedula = '4674884' or " +
            "cedula = '4675592' or " +
            "cedula = '4676297' or " +
            "cedula = '4677505' or " +
            "cedula = '4677864' or " +
            "cedula = '4679386' or " +
            "cedula = '4679737' or " +
            "cedula = '4680103' or " +
            "cedula = '4680354' or " +
            "cedula = '4680617' or " +
            "cedula = '4680718' or " +
            "cedula = '4681669' or " +
            "cedula = '4684035' or " +
            "cedula = '4684111' or " +
            "cedula = '4684635' or " +
            "cedula = '4686637' or " +
            "cedula = '4688583' or " +
            "cedula = '4688666' or " +
            "cedula = '4689128' or " +
            "cedula = '4689183' or " +
            "cedula = '4689735' or " +
            "cedula = '4690782' or " +
            "cedula = '4691377' or " +
            "cedula = '4691597' or " +
            "cedula = '4692176' or " +
            "cedula = '4692463' or " +
            "cedula = '4693364' or " +
            "cedula = '4693524' or " +
            "cedula = '4693758' or " +
            "cedula = '4694182' or " +
            "cedula = '4694608' or " +
            "cedula = '4694921' or " +
            "cedula = '4695190' or " +
            "cedula = '4697193' or " +
            "cedula = '4698535' or " +
            "cedula = '4699194' or " +
            "cedula = '4699483' or " +
            "cedula = '4699653' or " +
            "cedula = '4701427' or " +
            "cedula = '4701850' or " +
            "cedula = '4702156' or " +
            "cedula = '4702385' or " +
            "cedula = '4702702' or " +
            "cedula = '4703553' or " +
            "cedula = '4704421' or " +
            "cedula = '4704422' or " +
            "cedula = '4705102' or " +
            "cedula = '4705160' or " +
            "cedula = '4705874' or " +
            "cedula = '4706423' or " +
            "cedula = '4706681' or " +
            "cedula = '4707594' or " +
            "cedula = '4710248' or " +
            "cedula = '4710553' or " +
            "cedula = '4711756' or " +
            "cedula = '4711868' or " +
            "cedula = '4712153' or " +
            "cedula = '4712819' or " +
            "cedula = '4713571' or " +
            "cedula = '4713574' or " +
            "cedula = '4714084' or " +
            "cedula = '4714812' or " +
            "cedula = '4715675' or " +
            "cedula = '4716456' or " +
            "cedula = '4717748' or " +
            "cedula = '4718329' or " +
            "cedula = '4719970' or " +
            "cedula = '4720976' or " +
            "cedula = '4722466' or " +
            "cedula = '4723802' or " +
            "cedula = '4724448' or " +
            "cedula = '4724472' or " +
            "cedula = '4724525' or " +
            "cedula = '4724632' or " +
            "cedula = '4725487' or " +
            "cedula = '4725705' or " +
            "cedula = '4727806' or " +
            "cedula = '4728251' or " +
            "cedula = '4728575' or " +
            "cedula = '4730952' or " +
            "cedula = '4733643' or " +
            "cedula = '4735683' or " +
            "cedula = '4736326' or " +
            "cedula = '4736802' or " +
            "cedula = '4738157' or " +
            "cedula = '4738687' or " +
            "cedula = '4738837' or " +
            "cedula = '4739831' or " +
            "cedula = '4741821' or " +
            "cedula = '4742195' or " +
            "cedula = '4744244' or " +
            "cedula = '4744254' or " +
            "cedula = '4746067' or " +
            "cedula = '4747547' or " +
            "cedula = '4747875' or " +
            "cedula = '4751099' or " +
            "cedula = '4751435' or " +
            "cedula = '4754165' or " +
            "cedula = '4754259' or " +
            "cedula = '4756601' or " +
            "cedula = '4756898' or " +
            "cedula = '4758198' or " +
            "cedula = '4760990' or " +
            "cedula = '4761282' or " +
            "cedula = '4763006' or " +
            "cedula = '4764174' or " +
            "cedula = '4766859' or " +
            "cedula = '4767098' or " +
            "cedula = '4768160' or " +
            "cedula = '4768376' or " +
            "cedula = '4769294' or " +
            "cedula = '4769374' or " +
            "cedula = '4769664' or " +
            "cedula = '4771039' or " +
            "cedula = '4771314' or " +
            "cedula = '4771605' or " +
            "cedula = '4772290' or " +
            "cedula = '4774325' or " +
            "cedula = '4780566' or " +
            "cedula = '4780789' or " +
            "cedula = '4782621' or " +
            "cedula = '4785789' or " +
            "cedula = '4787359' or " +
            "cedula = '4792503' or " +
            "cedula = '4792878' or " +
            "cedula = '4793609' or " +
            "cedula = '4794151' or " +
            "cedula = '4794165' or " +
            "cedula = '4795461' or " +
            "cedula = '4796317' or " +
            "cedula = '4799566' or " +
            "cedula = '4800266' or " +
            "cedula = '4801253' or " +
            "cedula = '4802017' or " +
            "cedula = '4803737' or " +
            "cedula = '4805357' or " +
            "cedula = '4806899' or " +
            "cedula = '4807290' or " +
            "cedula = '4808378' or " +
            "cedula = '4812444' or " +
            "cedula = '4813416' or " +
            "cedula = '4813685' or " +
            "cedula = '4813759' or " +
            "cedula = '4815085' or " +
            "cedula = '4816494' or " +
            "cedula = '4816686' or " +
            "cedula = '4816838' or " +
            "cedula = '4817032' or " +
            "cedula = '4818159' or " +
            "cedula = '4818340' or " +
            "cedula = '4819078' or " +
            "cedula = '4819452' or " +
            "cedula = '4819770' or " +
            "cedula = '4821515' or " +
            "cedula = '4822340' or " +
            "cedula = '4822478' or " +
            "cedula = '4824295' or " +
            "cedula = '4824306' or " +
            "cedula = '4826573' or " +
            "cedula = '4827362' or " +
            "cedula = '4830311' or " +
            "cedula = '4831697' or " +
            "cedula = '4831714' or " +
            "cedula = '4832087' or " +
            "cedula = '4833457' or " +
            "cedula = '4837273' or " +
            "cedula = '4838275' or " +
            "cedula = '4841155' or " +
            "cedula = '4841526' or " +
            "cedula = '4844540' or " +
            "cedula = '4845963' or " +
            "cedula = '4847709' or " +
            "cedula = '4849006' or " +
            "cedula = '4850359' or " +
            "cedula = '4851345' or " +
            "cedula = '4851922' or " +
            "cedula = '4854294' or " +
            "cedula = '4854745' or " +
            "cedula = '4855775' or " +
            "cedula = '4856279' or " +
            "cedula = '4857413' or " +
            "cedula = '4857555' or " +
            "cedula = '4861690' or " +
            "cedula = '4862378' or " +
            "cedula = '4866328' or " +
            "cedula = '4866483' or " +
            "cedula = '4866837' or " +
            "cedula = '4868585' or " +
            "cedula = '4869250' or " +
            "cedula = '4871773' or " +
            "cedula = '4874668' or " +
            "cedula = '4875199' or " +
            "cedula = '4875603' or " +
            "cedula = '4875669' or " +
            "cedula = '4876460' or " +
            "cedula = '4877931' or " +
            "cedula = '4878129' or " +
            "cedula = '4881183' or " +
            "cedula = '4881984' or " +
            "cedula = '4882303' or " +
            "cedula = '4882340' or " +
            "cedula = '4882575' or " +
            "cedula = '4883469' or " +
            "cedula = '4887311' or " +
            "cedula = '4888232' or " +
            "cedula = '4889624' or " +
            "cedula = '4890237' or " +
            "cedula = '4892328' or " +
            "cedula = '4894251' or " +
            "cedula = '4894679' or " +
            "cedula = '4896711' or " +
            "cedula = '4899734' or " +
            "cedula = '4902176' or " +
            "cedula = '4907682' or " +
            "cedula = '4909185' or " +
            "cedula = '4909282' or " +
            "cedula = '4909642' or " +
            "cedula = '4909725' or " +
            "cedula = '4911167' or " +
            "cedula = '4911168' or " +
            "cedula = '4912058' or " +
            "cedula = '4912639' or " +
            "cedula = '4915972' or " +
            "cedula = '4918671' or " +
            "cedula = '4920585' or " +
            "cedula = '4920998' or " +
            "cedula = '4921588' or " +
            "cedula = '4923274' or " +
            "cedula = '4927907' or " +
            "cedula = '4929203' or " +
            "cedula = '4929770' or " +
            "cedula = '4932394' or " +
            "cedula = '4934805' or " +
            "cedula = '4937538' or " +
            "cedula = '4938330' or " +
            "cedula = '4938610' or " +
            "cedula = '4940585' or " +
            "cedula = '4941516' or " +
            "cedula = '4942190' or " +
            "cedula = '4943975' or " +
            "cedula = '4944799' or " +
            "cedula = '4946736' or " +
            "cedula = '4950589' or " +
            "cedula = '4952577' or " +
            "cedula = '4952579' or " +
            "cedula = '4953492' or " +
            "cedula = '4954893' or " +
            "cedula = '4955031' or " +
            "cedula = '4955682' or " +
            "cedula = '4960049' or " +
            "cedula = '4961376' or " +
            "cedula = '4962403' or " +
            "cedula = '4963209' or " +
            "cedula = '4963928' or " +
            "cedula = '4965868' or " +
            "cedula = '4968354' or " +
            "cedula = '4969168' or " +
            "cedula = '4973788' or " +
            "cedula = '4975160' or " +
            "cedula = '4975206' or " +
            "cedula = '4976236' or " +
            "cedula = '4976400' or " +
            "cedula = '4981829' or " +
            "cedula = '4982376' or " +
            "cedula = '4982664' or " +
            "cedula = '4983246' or " +
            "cedula = '4983908' or " +
            "cedula = '4988666' or " +
            "cedula = '4990110' or " +
            "cedula = '4992916' or " +
            "cedula = '4993404' or " +
            "cedula = '4996425' or " +
            "cedula = '4997750' or " +
            "cedula = '4998785' or " +
            "cedula = '4999071' or " +
            "cedula = '4999575' or " +
            "cedula = '4999938' or " +
            "cedula = '5001359' or " +
            "cedula = '5002439' or " +
            "cedula = '5003493' or " +
            "cedula = '5003506' or " +
            "cedula = '5010962' or " +
            "cedula = '5011814' or " +
            "cedula = '5012962' or " +
            "cedula = '5013226' or " +
            "cedula = '5021705' or " +
            "cedula = '5024625' or " +
            "cedula = '5029627' or " +
            "cedula = '5031936' or " +
            "cedula = '5035281' or " +
            "cedula = '5037030' or " +
            "cedula = '5038119' or " +
            "cedula = '5039106' or " +
            "cedula = '5042553' or " +
            "cedula = '5042556' or " +
            "cedula = '5042558' or " +
            "cedula = '5043968' or " +
            "cedula = '5044441' or " +
            "cedula = '5045056' or " +
            "cedula = '5045156' or " +
            "cedula = '5046418' or " +
            "cedula = '5048340' or " +
            "cedula = '5050010' or " +
            "cedula = '5050798' or " +
            "cedula = '5051809' or " +
            "cedula = '5053624' or " +
            "cedula = '5053946' or " +
            "cedula = '5054263' or " +
            "cedula = '5057480' or " +
            "cedula = '5058265' or " +
            "cedula = '5062836' or " +
            "cedula = '5064423' or " +
            "cedula = '5066101' or " +
            "cedula = '5067023' or " +
            "cedula = '5067047' or " +
            "cedula = '5073071' or " +
            "cedula = '5073842' or " +
            "cedula = '5075690' or " +
            "cedula = '5079421' or " +
            "cedula = '5080179' or " +
            "cedula = '5081893' or " +
            "cedula = '5082975' or " +
            "cedula = '5083482' or " +
            "cedula = '5087352' or " +
            "cedula = '5091043' or " +
            "cedula = '5092344' or " +
            "cedula = '5103067' or " +
            "cedula = '5108290' or " +
            "cedula = '5109115' or " +
            "cedula = '5112928' or " +
            "cedula = '5113474' or " +
            "cedula = '5114601' or " +
            "cedula = '5115750' or " +
            "cedula = '5118792' or " +
            "cedula = '5131425' or " +
            "cedula = '5137329' or " +
            "cedula = '5137359' or " +
            "cedula = '5142963' or " +
            "cedula = '5152813' or " +
            "cedula = '5161474' or " +
            "cedula = '5161733' or " +
            "cedula = '5167740' or " +
            "cedula = '5167812' or " +
            "cedula = '5182463' or " +
            "cedula = '5183225' or " +
            "cedula = '5186192' or " +
            "cedula = '5194193' or " +
            "cedula = '5195762' or " +
            "cedula = '5197980' or " +
            "cedula = '5204441' or " +
            "cedula = '5208761' or " +
            "cedula = '5217443' or " +
            "cedula = '5219418' or " +
            "cedula = '5221544' or " +
            "cedula = '5221557' or " +
            "cedula = '5222960' or " +
            "cedula = '5225634' or " +
            "cedula = '5227277' or " +
            "cedula = '5227330' or " +
            "cedula = '5240816' or " +
            "cedula = '5240817' or " +
            "cedula = '5240954' or " +
            "cedula = '5245494' or " +
            "cedula = '5246674' or " +
            "cedula = '5250903' or " +
            "cedula = '5254222' or " +
            "cedula = '5256428' or " +
            "cedula = '5256790' or " +
            "cedula = '5258264' or " +
            "cedula = '5258733' or " +
            "cedula = '5265775' or " +
            "cedula = '5265839' or " +
            "cedula = '5265951' or " +
            "cedula = '5268956' or " +
            "cedula = '5269694' or " +
            "cedula = '5273550' or " +
            "cedula = '5278444' or " +
            "cedula = '5282854' or " +
            "cedula = '5285456' or " +
            "cedula = '5290066' or " +
            "cedula = '5292553' or " +
            "cedula = '5297774' or " +
            "cedula = '5302200' or " +
            "cedula = '5302487' or " +
            "cedula = '5313050' or " +
            "cedula = '5317286' or " +
            "cedula = '5326189' or " +
            "cedula = '5333185' or " +
            "cedula = '5349176' or " +
            "cedula = '5357380' or " +
            "cedula = '5359237' or " +
            "cedula = '5376634' or " +
            "cedula = '5377088' or " +
            "cedula = '5386076' or " +
            "cedula = '5399908' or " +
            "cedula = '5414846' or " +
            "cedula = '5432494' or " +
            "cedula = '5432593' or " +
            "cedula = '5434549' or " +
            "cedula = '5451629' or " +
            "cedula = '5463173' or " +
            "cedula = '5465802' or " +
            "cedula = '5467605' or " +
            "cedula = '5477666' or " +
            "cedula = '5481123' or " +
            "cedula = '5490836' or " +
            "cedula = '5500731' or " +
            "cedula = '5501515' or " +
            "cedula = '5504600' or " +
            "cedula = '5505580' or " +
            "cedula = '5507026' or " +
            "cedula = '5511023' or " +
            "cedula = '5516814' or " +
            "cedula = '5518000' or " +
            "cedula = '5532630' or " +
            "cedula = '5567634' or " +
            "cedula = '5567968' or " +
            "cedula = '5578136' or " +
            "cedula = '5605197' or " +
            "cedula = '5608553' or " +
            "cedula = '5614103' or " +
            "cedula = '5616414' or " +
            "cedula = '5620588' or " +
            "cedula = '5622389' or " +
            "cedula = '5633018' or " +
            "cedula = '5653379' or " +
            "cedula = '5670335' or " +
            "cedula = '5671617' or " +
            "cedula = '5695665' or " +
            "cedula = '5695862' or " +
            "cedula = '5700111' or " +
            "cedula = '5704829' or " +
            "cedula = '5710988' or " +
            "cedula = '5726409' or " +
            "cedula = '5730292' or " +
            "cedula = '5738098' or " +
            "cedula = '5738269' or " +
            "cedula = '5748365' or " +
            "cedula = '5755965' or " +
            "cedula = '5776571' or " +
            "cedula = '5785935' or " +
            "cedula = '5790820' or " +
            "cedula = '5794012' or " +
            "cedula = '5860712' or " +
            "cedula = '5871785' or " +
            "cedula = '5879645' or " +
            "cedula = '5896003' or " +
            "cedula = '5899637' or " +
            "cedula = '5905743' or " +
            "cedula = '5910139' or " +
            "cedula = '5912465' or " +
            "cedula = '5924592' or " +
            "cedula = '5937896' or " +
            "cedula = '5959222' or " +
            "cedula = '5965121' or " +
            "cedula = '5992098' or " +
            "cedula = '5992100' or " +
            "cedula = '5992153' or " +
            "cedula = '5992495' or " +
            "cedula = '5994240' or " +
            "cedula = '6005493' or " +
            "cedula = '6006169' or " +
            "cedula = '6027452' or " +
            "cedula = '6045808' or " +
            "cedula = '6051750' or " +
            "cedula = '6110033' or " +
            "cedula = '6128796' or " +
            "cedula = '6133682' or " +
            "cedula = '6153703' or " +
            "cedula = '6154717' or " +
            "cedula = '6162714' or " +
            "cedula = '6168570' or " +
            "cedula = '6169725' or " +
            "cedula = '6213850' or " +
            "cedula = '6221097' or " +
            "cedula = '6527115' or " +
            "cedula = '6536577' or " +
            "cedula = '6540134' or " +
            "cedula = '6621407' or " +
            "cedula = '6733457' or " +
            "cedula = '6748156' or " +
            "cedula = '6768817' or " +
            "cedula = '6932731' or " +
            "cedula = '7030329' or " +
            "cedula = '7360253' or " +
            "cedula = '7364472' or " +
            "cedula = '7365313' or " +
            "cedula = '7371677' or " +
            "cedula = '7456052' or " +
            "cedula = '7474913' or " +
            "cedula = '7909042' or " +
            "cedula = '4515230' or " +
            "cedula = '4327474' or " +
            "cedula = '4489334' or " +
            "cedula = '4588523' or " +
            "cedula = '4867747' or " +
            "cedula = '5095847' or " +
            "cedula = '5791118' or " +
            "cedula = '6289877' or " +
            "cedula = '4177658' or " +
            "cedula = '4758967' or " +
            "cedula = '4938975') "+
            //"WHERE cedula='596999' or cedula='4177658' or cedula='3398549' or cedula='3998529' or cedula='2074537' or cedula='573490' or cedula='617358' or cedula='658609B' or cedula='1104445A' or cedula='453014' or cedula='1335185' or cedula='1867536' or cedula='2989250' or cedula='3501239' "+
            //"WHERE cedula='249562' or cedula='298514' or cedula='313859' or cedula='333860' or cedula='335412' or cedula='362108' or cedula='362230' or cedula='378489' or cedula='397463' or cedula='399488' or cedula='402416' or cedula='411750' or cedula='414148' or cedula='421580' or cedula='426532' or cedula='438186' or cedula='447313' or cedula='460912' or cedula='485386' or cedula='533754' or cedula='546113' or cedula='563367' or cedula='573263' or cedula='573490' or cedula='590845' or cedula='596999' or cedula='611808' or cedula='617358' "+
            //"WHERE cedula='1708204' OR cedula='1104445A' or cedula='747213' or cedula='658609B' "+
            //"WHERE cedula= '1550170' or cedula= '2006289' or cedula= '2080591' or cedula= '4515230' or cedula= '2095388' "+
            //"WHERE cedula='4620098' or cedula='4113266' or cedula='3714366' or cedula='2035573' or cedula='4473837' or cedula='1367601' or cedula='854072' or cedula='1484172' or cedula='3426101' or cedula='3838493' or cedula='3369192' or cedula='810985' or cedula='1431035' or cedula='1001268' or cedula='4465309' or cedula='828501' "+
            "ORDER BY convertir_especial(detallegeneral.cedula), detallegeneral.tipo DESC, detallegeneral.fechavencimiento, detallegeneral.prioridad, detallegeneral.saldocapital";
    }
    
    public String getConsultaConEstado2(java.util.Date dperiodo) {
        return // socios ACTIVO
            "SELECT * FROM (SELECT op.tipo, op.tabla, op.idmovimiento, op.idsocio, op.cedula, op.idcuenta, op.numerooperacion, op.plazo, op.fechaoperacion, op.identidad, op.prioridad, dop.id, dop.numerocuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital, dop.saldointeres FROM ("+
                "SELECT 'N' AS tipo, 'of' AS tabla, of.id AS idmovimiento, of.idsocio, so.cedula, of.idcuenta, of.numerooperacion, of.plazo, of.fechaoperacion, 0 AS identidad, cu.prioridad "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id AND (so.idestadosocio=20 OR so.idestadosocio=224 OR so.idestadosocio=21 OR so.idestadosocio=23) "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id "+
                "WHERE of.idestado=86 AND (of.idcuenta=17 OR of.idcuenta=85) "+ // Interes Moratorio y Punitorio con tipo N (mayor prioridad)
                "UNION "+
                "SELECT 'M' AS tipo, 'of' AS tabla, of.id AS idmovimiento, of.idsocio, so.cedula, of.idcuenta, of.numerooperacion, of.plazo, of.fechaoperacion, 0 AS identidad, cu.prioridad "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id AND (so.idestadosocio=20 OR so.idestadosocio=224 OR so.idestadosocio=21 OR so.idestadosocio=23) "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id "+
                "WHERE of.idestado=86 AND of.idcuenta<>17 AND of.idcuenta<>85 "+ // demas cuentas que no sean Interes Moratorio y Punitorio con tipo M
                "UNION "+
                "SELECT 'M' AS tipo, 'mo' AS tabla, mo.id AS idmovimiento, mo.idsocio, so.cedula, mo.idcuenta, mo.numerooperacion, mo.plazoaprobado AS plazo, mo.fechaaprobado AS fechaoperacion, 0 AS identidad, cu.prioridad "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id AND (so.idestadosocio=20 OR so.idestadosocio=224 OR so.idestadosocio=21 OR so.idestadosocio=23) "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "WHERE mo.idestado=179 OR mo.idestado=52 "+
                "UNION "+
                "SELECT 'M' AS tipo, 'ap' AS tabla, ap.id AS idmovimiento, ap.idsocio, so.cedula, ap.idcuenta, ap.numerooperacion, ap.plazo, ap.fechaoperacion, 0 AS identidad, cu.prioridad "+
                "FROM ahorroprogramado AS ap "+
                "LEFT JOIN socio AS so ON ap.idsocio=so.id AND (so.idestadosocio=20 OR so.idestadosocio=224 OR so.idestadosocio=21 OR so.idestadosocio=23) "+
                "LEFT JOIN cuenta AS cu ON ap.idcuenta=cu.id "+
                "WHERE ap.idestado=53 OR (ap.idestado=55 AND ap.fecharetirado>'05/05/2017') "+
                "UNION "+
                "SELECT 'M' AS tipo, 'js' AS tabla, js.id AS idmovimiento, js.idsocio, so.cedula, js.idcuenta, js.numerooperacion, 0 AS plazo, js.fechaoperacion, 0 AS identidad, cu.prioridad "+
                "FROM fondojuridicosepelio AS js "+
                "LEFT JOIN socio AS so ON js.idsocio=so.id AND (so.idestadosocio=20 OR so.idestadosocio=224 OR so.idestadosocio=21 OR so.idestadosocio=23) "+
                "LEFT JOIN cuenta AS cu ON js.idcuenta=cu.id "+
                "WHERE js.idestado=57 "+
            ") AS op "+
            "LEFT JOIN detalleoperacion AS dop ON op.idmovimiento=dop.idmovimiento AND op.tabla=dop.tabla "+
            "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.fechavencimiento<="+utilitario.utiFecha.getFechaGuardado(dperiodo)+" "+
            "UNION "+
            "SELECT 'E' AS tipo, 'ce' AS tabla, oe.id AS idmovimiento, oe.idsocio, oe.cedula, oe.idcuenta, 0 AS numerooperacion, oe.plazo, oe.fecha AS fechaoperacion, 0 AS identidad, cu.prioridad, doe.id, doe.numerocuota, doe.fechavencimiento, doe.montocapital, 0 AS montointeres, doe.saldocapital, 0 AS saldointeres "+
            "FROM detalleoperacionexterno AS doe "+
            "LEFT JOIN operacionexterna AS oe ON doe.idmovimiento=oe.id "+
            "LEFT JOIN cuenta AS cu ON oe.idcuenta=cu.id "+
            "WHERE doe.saldocapital>0 AND TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dperiodo)+"' "+
            ") AS detalle "+
            
            //"WHERE cedula='3316050' or cedula='2335216' or cedula='4140987' or cedula='1271066' or cedula='1025848' or cedula='1824872' or cedula='998882' or cedula='3676945' or cedula='2918553' or cedula='2484700' or cedula='1102450' or cedula='1873205' or cedula='1532509' or cedula='2867123' or cedula='889015' or cedula='3206280' or cedula='1176786' or cedula='1802412' or cedula='1843020' or cedula='2013312' or cedula='604875' or cedula='1440741' or cedula='1124004' or cedula='999372' or cedula='884994' or cedula='1009391' or cedula='994104' or cedula='3404993' or cedula='531789' or cedula='1198442' or cedula='4296634' or cedula='996837' or cedula='793481' or cedula='2167800' or cedula='800102' or cedula='1251471' or cedula='849441' or cedula='1823060' or cedula='2169662' or cedula='1667959' or cedula='889849' or cedula='1066087' or cedula='2390325' or cedula='3506949' or cedula='858159' or cedula='1441672' or cedula='1171902' or cedula='2540245' or cedula='2191427' or cedula='804888' or cedula='1228627' or cedula='1428093' or cedula='669975' or cedula='1409825' or cedula='2851473' or cedula='1545443' or cedula='1254012' or cedula='4660877' or cedula='3974825' or cedula='3377550' or cedula='3329638' or cedula='3968207' or cedula='1144511' or cedula='3394438' or cedula='999960' or cedula='977293' or cedula='816373' or cedula='756283' or cedula='808897' or cedula='1023271' "+
            //"WHERE cedula='2206229' OR cedula='696631' OR cedula='931330' OR cedula='4361880' OR cedula='1008166' "+
                
            "ORDER BY cedula, tipo DESC, fechavencimiento, prioridad, saldocapital";
    }
    
    /*public String getConsultaNoSocio(int iidGiraduria, int iidRubro, java.util.Date dperiodo, String scedula) {
        return "SELECT 'E' AS tipo, 'ce' AS tabla, oe.id AS idmovimiento, oe.idsocio, oe.cedula, oe.idcuenta, 0 AS numerooperacion, oe.plazo, oe.fecha AS fechaoperacion, 0 AS identidad, cu.prioridad, doe.id, doe.numerocuota, doe.fechavencimiento, doe.montocapital, 0 AS montointeres, doe.saldocapital, 0 AS saldointeres "+
            "FROM detalleoperacionexterno AS doe "+
            "LEFT JOIN operacionexterna AS oe ON doe.idmovimiento=oe.id "+
            "LEFT JOIN cuenta AS cu ON oe.idcuenta=cu.id "+
            "WHERE oe.cedula='"+scedula.trim()+"' AND doe.saldocapital>0 AND TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dperiodo)+"' ";
    }*/
    
    /*public String getConsulta(java.util.Date dperiodo) {
        return "SELECT * FROM archivogiraduria WHERE 0=0 AND dperiodo="+utilitario.utiFecha.getFechaGuardado(dperiodo);
    }*/
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
