/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entCambioRubro {

    private int iid;
    private int iidSocio;
    private String scedula;
    private int inumeroSocio;
    private String sapellidonombre;
    private int iidOperacion;
    private String soperacion;
    private int inumeroOperacion;
    private String stabla;
    private java.util.Date dfechaoperacion;
    private java.util.Date dfechainicio;
    private int iidGiraduria;
    private String sgiraduria;
    private String sgiraduriaBreve;
    private int iidRubro;
    private String srubro;
    private String srubroBreve;
    private int iidAsignacion;
    private String sasignacion;
    
    protected boolean bactivo;
    protected short hestadoRegistro;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FECHA = "Fecha (no vacía)";
    public static final String TEXTO_SOCIO = "Socio (no editable, no vacío)";
    public static final String TEXTO_OPERACION = "Operación (no editable, no vacío)";
    public static final String TEXTO_GIRADURIA = "Giraduría (no vacía)";
    public static final String TEXTO_RUBRO = "Rubro (no vacío)";
    public static final String TEXTO_ACTIVO = "Si se encuentra o no activo (no vacía)";
    public static final String TEXTO_ASIGNACION = "Asignación (no vacía)";
    public static final String TEXTO_FECHA_INICIO = "Fecha de Inicio (no vacía)";


    public entCambioRubro() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.inumeroSocio = 0;
        this.sapellidonombre = "";
        this.dfechaoperacion = null;
        this.dfechainicio = null;
        this.iidOperacion = 0;
        this.soperacion = "";
        this.inumeroOperacion = 0;
        this.stabla = "";
        this.iidRubro = 0;
        this.srubro = "";
        this.srubroBreve = "";
        this.iidGiraduria = 0;
        this.sgiraduria = "";
        this.sgiraduriaBreve = "";
        this.iidAsignacion = 0;
        this.sasignacion = "";
        this.bactivo = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entCambioRubro(int iid, int iidSocio, String scedula, int inumeroSocio, String sapellidonombre, java.util.Date dfechaoperacion, java.util.Date dfechainicio, int iidOperacion, String soperacion, int inumeroOperacion, String stabla, int iidRubro, String srubro, String srubroBreve, int iidGiraduria, String sgiraduria, String sgiraduriaBreve, int iidAsignacion, String sasignacion, boolean bactivo,  short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.inumeroSocio = inumeroSocio;
        this.sapellidonombre = sapellidonombre;
        this.dfechaoperacion = dfechaoperacion;
        this.dfechainicio = dfechainicio;
        this.iidOperacion = iidOperacion;
        this.soperacion = soperacion;
        this.inumeroOperacion = inumeroOperacion;
        this.stabla = stabla;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.srubroBreve = srubroBreve;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.sgiraduriaBreve = sgiraduriaBreve;
        this.iidAsignacion = iidAsignacion;
        this.sasignacion = sasignacion;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidSocio, String scedula, int inumeroSocio, String sapellidonombre, java.util.Date dfechaoperacion, java.util.Date dfechainicio, int iidOperacion, String soperacion, int inumeroOperacion, String stabla, int iidRubro, String srubro, String srubroBreve, int iidGiraduria, String sgiraduria, String sgiraduriaBreve, int iidAsignacion, String sasignacion, boolean bactivo,  short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.inumeroSocio = inumeroSocio;
        this.sapellidonombre = sapellidonombre;
        this.dfechaoperacion = dfechaoperacion;
        this.dfechainicio = dfechainicio;
        this.iidOperacion = iidOperacion;
        this.soperacion = soperacion;
        this.inumeroOperacion = inumeroOperacion;
        this.stabla = stabla;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.srubroBreve = srubroBreve;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.sgiraduriaBreve = sgiraduriaBreve;
        this.iidAsignacion = iidAsignacion;
        this.sasignacion = sasignacion;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entCambioRubro copiar(entCambioRubro destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getNumeroSocio(), this.getApellidoNombre(), this.getFechaOperacion(), this.getFechaInicio(), this.getIdOperacion(), this.getOperacion(), this.getNumeroOperacion(), this.getTabla(), this.getIdRubro(), this.getRubro(), this.getRubroBreve(), this.getIdGiraduria(), this.getGiraduria(), this.getGiraduriaBreve(), this.getIdAsignacion(), this.getAsignacion(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }

    public entCambioRubro cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNumeroSocio(rs.getInt("numerosocio")); }
        catch(Exception e) {}
        try { this.setApellidoNombre(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setFechaInicio(rs.getDate("fechainicio")); }
        catch(Exception e) {}
        try { this.setIdOperacion(rs.getInt("idoperacion")); }
        catch(Exception e) {}
        try { this.setOperacion(rs.getString("operacion")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numero")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setIdGiraduria(rs.getInt("idgiraduria")); }
        catch(Exception e) {}
        try { this.setGiraduria(rs.getString("giraduria")); }
        catch(Exception e) {}
        try { this.setGiraduriaBreve(rs.getString("giraduriabreve")); }
        catch(Exception e) {}
        try { this.setIdRubro(rs.getInt("idrubro")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        try { this.setRubroBreve(rs.getString("rubrobreve")); }
        catch(Exception e) {}
        try { this.setIdAsignacion(rs.getInt("idasignacion")); }
        catch(Exception e) {}
        try { this.setAsignacion(rs.getString("asignacion")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNumeroSocio(int inumeroSocio) {
        this.inumeroSocio = inumeroSocio;
    }

    public void setApellidoNombre(String sapellidonombre) {
        this.sapellidonombre = sapellidonombre;
    }

    public void setFechaOperacion(java.util.Date dfechaoperacion) {
        this.dfechaoperacion = dfechaoperacion;
    }

    public void setFechaInicio(java.util.Date dfechainicio) {
        this.dfechainicio = dfechainicio;
    }

    public void setIdOperacion(int iidOperacion) {
        this.iidOperacion = iidOperacion;
    }

    public void setOperacion(String soperacion) {
        this.soperacion = soperacion;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setIdRubro(int iidRubro) {
        this.iidRubro = iidRubro;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setRubroBreve(String srubroBreve) {
        this.srubroBreve = srubroBreve;
    }

    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }

    public void setGiraduria(String sgiraduria) {
        this.sgiraduria = sgiraduria;
    }

    public void setGiraduriaBreve(String sgiraduriaBreve) {
        this.sgiraduriaBreve = sgiraduriaBreve;
    }

    public void setIdAsignacion(int iidAsignacion) {
        this.iidAsignacion = iidAsignacion;
    }

    public void setAsignacion(String sasignacion) {
        this.sasignacion = sasignacion;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public int getNumeroSocio() {
        return this.inumeroSocio;
    }

    public String getApellidoNombre() {
        return this.sapellidonombre;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaoperacion;
    }
    
    public java.util.Date getFechaInicio() {
        return this.dfechainicio;
    }

    public int getIdOperacion() {
        return this.iidOperacion;
    }

    public String getOperacion() {
        return this.soperacion;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public String getTabla() {
        return this.stabla;
    }

    public int getIdRubro() {
        return this.iidRubro;
    }

    public String getRubro() {
        return this.srubro;
    }

    public String getRubroBreve() {
        return this.srubroBreve;
    }

    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public String getGiraduria() {
        return this.sgiraduria;
    }

    public String getGiraduriaBreve() {
        return this.sgiraduriaBreve;
    }

    public int getIdAsignacion() {
        return this.iidAsignacion;
    }

    public String getAsignacion() {
        return this.sasignacion;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaOperacion() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getIdSocio() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getIdOperacion() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OPERACION;
            bvalido = false;
        }
        if (this.getIdGiraduria() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_GIRADURIA;
            bvalido = false;
        }
        if (this.getIdRubro() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RUBRO;
            bvalido = false;
        }
        if (this.getIdAsignacion() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ASIGNACION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT cambiorubro("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdOperacion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaOperacion())+","+
            this.getIdGiraduria()+","+
            this.getIdRubro()+","+
            this.getActivo()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaInicio())+","+
            this.getIdAsignacion()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cédula", "cedula", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Apellido, Nombre", "apellidonombre", new generico.entLista().tipo_texto));
        return lst;
    }

}
