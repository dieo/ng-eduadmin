/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entControlLegajo2 {

    private int iid;
    private int iidPlanilla;
    private String splanilla;
    private String scodigo;
    private java.util.Date dfechaOperacion;
    private int iidFuncionario;
    private String sfuncionario;
    private int inumeroOperacion;
    private java.util.Date dfecha;
    private int inumeroSolicitud;
    private String scedula;
    private String snombre;
    private String sapellido;
    private double umonto;
    private String scheque;
    private int iidBanco;
    private String sbanco;
    private int iidRegion;
    private String sregion;
    private String sdescripcion;
    private int iidTipoOperacion;
    private String stipoOperacion;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_CEDULA = 12;
    public static final int LONGITUD_NOMBRE = 100;
    public static final int LONGITUD_APELLIDO = 40;
    public static final int LONGITUD_CHEQUE = 10;
    public static final int LONGITUD_BANCO = 50;
    public static final int LONGITUD_DESCRIPCION = 200;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_PLANILLA = "Planilla de Legajo (no editable)";
    public static final String TEXTO_FECHA_OPERACION = "Fecha de registro de la operación (no vacío)";
    public static final String TEXTO_FUNCIONARIO = "Funcionario (no vacío)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (valor positivo)";
    public static final String TEXTO_NUMERO_SOLICITUD = "Número de Solicitud (valor positivo)";
    public static final String TEXTO_FECHA = "Fecha de Operación (no vacía)";
    public static final String TEXTO_CEDULA = "Cédula (no vacía, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacío, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_MONTO = "Monto (valor positivo)";
    public static final String TEXTO_CHEQUE = "Cheque (no vacío, hasta " + LONGITUD_CHEQUE + " caracteres)";
    public static final String TEXTO_BANCO = "Banco (no vacío, hasta " + LONGITUD_BANCO + " caracteres)";
    public static final String TEXTO_REGION = "Región (no vacía)";
    public static final String TEXTO_DESCRIPCION = "Descripción (no vacío, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_TIPO_OPERACION = "Tipo Operación (no vacío)";

    public entControlLegajo2() {
        this.iid = 0;
        this.iidPlanilla = 0;
        this.splanilla = "";
        this.scodigo = "";
        this.dfechaOperacion = null;
        this.iidFuncionario = 0;
        this.sfuncionario = "";
        this.inumeroOperacion = 0;
        this.dfecha = null;
        this.inumeroSolicitud = 0;
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.umonto = 0.0;
        this.scheque = "";
        this.iidBanco = 0;
        this.sbanco = "";
        this.iidRegion = 0;
        this.sregion = "";
        this.sdescripcion = "";
        this.iidTipoOperacion = 0;
        this.stipoOperacion = "";
       this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entControlLegajo2(int iid, int iidPlanilla, String splanilla, String scodigo, java.util.Date dfechaOperacion, int iidFuncionario, String sfuncionario, int inumeroOperacion, java.util.Date dfecha, int inumeroSolicitud, String scedula, String snombre, String sapellido, double umonto, String scheque, int iidBanco, String sbanco, int iidRegion, String sregion, String sdescripcion, int iidTipoOperacion, String stipoOperacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidPlanilla = iidPlanilla;
        this.splanilla = splanilla;
        this.scodigo = scodigo;
        this.dfechaOperacion = dfechaOperacion;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.inumeroOperacion = inumeroOperacion;
        this.dfecha = dfecha;
        this.inumeroSolicitud = inumeroSolicitud;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.umonto = umonto;
        this.scheque = scheque;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidRegion = iidRegion;
        this.sregion = sregion;
        this.sdescripcion = sdescripcion;
        this.iidTipoOperacion = iidTipoOperacion;
        this.stipoOperacion = stipoOperacion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidPlanilla, String splanilla, String scodigo, java.util.Date dfechaOperacion, int iidFuncionario, String sfuncionario, int inumeroOperacion, java.util.Date dfecha, int inumeroSolicitud, String scedula, String snombre, String sapellido, double umonto, String scheque, int iidBanco, String sbanco, int iidRegion, String sregion, String sdescripcion, int iidTipoOperacion, String stipoOperacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidPlanilla = iidPlanilla;
        this.splanilla = splanilla;
        this.scodigo = scodigo;
        this.dfechaOperacion = dfechaOperacion;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.inumeroOperacion = inumeroOperacion;
        this.dfecha = dfecha;
        this.inumeroSolicitud = inumeroSolicitud;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.umonto = umonto;
        this.scheque = scheque;
        this.iidBanco = iidBanco;
        this.sbanco = sbanco;
        this.iidRegion = iidRegion;
        this.sregion = sregion;
        this.sdescripcion = sdescripcion;
        this.iidTipoOperacion = iidTipoOperacion;
        this.stipoOperacion = stipoOperacion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entControlLegajo2 copiar(entControlLegajo2 destino) {
        destino.setEntidad(this.getId(), this.getIdPlanilla(), this.getPlanilla(), this.getCodigo(), this.getFechaOperacion(), this.getIdFuncionario(), this.getFuncionario(), this.getNumeroOperacion(), this.getFecha(), this.getNumeroSolicitud(), this.getCedula(), this.getNombre(), this.getApellido(), this.getMonto(), this.getCheque(), this.getIdBanco(), this.getBanco(), this.getIdRegion(), this.getRegion(), this.getDescripcion(), this.getIdTipoOperacion(), this.getTipoOperacion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entControlLegajo2 cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdPlanilla(rs.getInt("idplanilla")); }
        catch(Exception e) {}
        try { this.setPlanilla(rs.getString("planilla")); }
        catch(Exception e) {}
        try { this.setCodigo(rs.getString("codigo")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        try { this.setFuncionario(rs.getString("funcionario")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        try { this.setCheque(rs.getString("cheque")); }
        catch(Exception e) {}
        try { this.setIdBanco(rs.getInt("idbanco")); }
        catch(Exception e) {}
        try { this.setBanco(rs.getString("banco")); }
        catch(Exception e) {}
        try { this.setIdRegion(rs.getInt("idregion")); }
        catch(Exception e) {}
        try { this.setRegion(rs.getString("region")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdTipoOperacion(rs.getInt("idtipooperacion")); }
        catch(Exception e) {}
        try { this.setTipoOperacion(rs.getString("tipooperacion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdPlanilla(int iidPlanilla) {
        this.iidPlanilla = iidPlanilla;
    }

    public void setPlanilla(String splanilla) {
        this.splanilla = splanilla;
    }

    public void setCodigo(String scodigo) {
        this.scodigo = scodigo;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }

    public void setFuncionario(String sfuncionario) {
        this.sfuncionario = sfuncionario;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombre(String snombre) {
        this.snombre = snombre;
    }

    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }

    public void setMonto(double umonto) {
        this.umonto = umonto;
    }

    public void setCheque(String scheque) {
        this.scheque = scheque;
    }

    public void setIdBanco(int iidBanco) {
        this.iidBanco = iidBanco;
    }

    public void setBanco(String sbanco) {
        this.sbanco = sbanco;
    }

    public void setIdRegion(int iidRegion) {
        this.iidRegion = iidRegion;
    }

    public void setRegion(String sregion) {
        this.sregion = sregion;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setIdTipoOperacion(int iidTipoOperacion) {
        this.iidTipoOperacion = iidTipoOperacion;
    }

    public void setTipoOperacion(String stipoOperacion) {
        this.stipoOperacion = stipoOperacion;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdPlanilla() {
        return this.iidPlanilla;
    }

    public String getPlanilla() {
        return this.splanilla;
    }

    public String getCodigo() {
        return this.scodigo;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public int getIdFuncionario() {
        return this.iidFuncionario;
    }

    public String getFuncionario() {
        return this.sfuncionario;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombre() {
        return this.snombre;
    }

    public String getApellido() {
        return this.sapellido;
    }

    public double getMonto() {
        return this.umonto;
    }

    public String getCheque() {
        return this.scheque;
    }

    public int getIdBanco() {
        return this.iidBanco;
    }

    public String getBanco() {
        return this.sbanco;
    }

    public int getIdRegion() {
        return this.iidRegion;
    }

    public String getRegion() {
        return this.sregion;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public int getIdTipoOperacion() {
        return this.iidTipoOperacion;
    }

    public String getTipoOperacion() {
        return this.stipoOperacion;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNumeroOperacion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_OPERACION;
            bvalido = false;
        }
        if (this.getFechaOperacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getFecha()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getNumeroSolicitud()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_SOLICITUD;
            bvalido = false;
        }
        if (this.getCedula().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CEDULA;
            bvalido = false;
        }
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getMonto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO;
            bvalido = false;
        }
        if (this.getCheque().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CHEQUE;
            bvalido = false;
        }
        if (this.getBanco().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_BANCO;
            bvalido = false;
        }
        if (this.getIdRegion()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGION;
            bvalido = false;
        }
        if (this.getIdTipoOperacion()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_OPERACION;
            bvalido = false;
        }
        if (esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Operación ya fue registrada en la Planilla";
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT controllegajo2("+
            this.getId()+","+
            this.getNumeroOperacion()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFecha())+","+
            this.getNumeroSolicitud()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            this.getMonto()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCheque())+","+
            this.getIdBanco()+","+
            utilitario.utiCadena.getTextoGuardado(this.getBanco())+","+
            this.getIdRegion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdTipoOperacion()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Codigo", "codigo", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Fecha de registro", "fechaoperacion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Número Operación", "numerooperacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "Fecha Operación", "fecha", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(5, "Número Solicitud", "numerosolicitud", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(6, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(8, "Apellido", "apellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(9, "Cheque", "cheque", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(10, "Tipo Operación", "tipooperacion", generico.entLista.tipo_texto));
        return lst;
    }

}
