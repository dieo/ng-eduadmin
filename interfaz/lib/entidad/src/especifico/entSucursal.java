/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entSucursal extends generico.entGenerica {
    
    protected int iidTipoSucursal;
    protected String stipoSucursal;
    protected String snumeroSucursal;
    protected String sdireccion;
    protected String stelefono;
    protected int iidCiudad;
    protected String sciudad;

    public static final int LONGITUD_DESCRIPCION = 50;
    public static final int LONGITUD_DIRECCION = 50;
    public static final int LONGITUD_TELEFONO = 40;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_TIPO_SUCURSAL = "Tipo de Sucursal (no vacío)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Sucursal (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_NUMERO_SUCURSAL = "Número de Sucursal (no vacío, valor numérico)";
    public static final String TEXTO_DIRECCION = "Dirección (hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_TELEFONO = "Teléfono (hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_CIUDAD = "Ciudad (no vacía)";
    
    public entSucursal() {
        super();
        this.iidTipoSucursal = 0;
        this.stipoSucursal = "";
        this.snumeroSucursal = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.iidCiudad = 0;
        this.sciudad = "";
    }

    public entSucursal(int iid, String sdescripcion, int iidTipoSucursal, String stipoSucursal, String snumeroSucursal, String sdireccion, String stelefono, int iidCiudad, String sciudad, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidTipoSucursal = iidTipoSucursal;
        this.stipoSucursal = stipoSucursal;
        this.snumeroSucursal = snumeroSucursal;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
    }
    
     public void setEntidad(int iid, String sdescripcion, int iidTipoSucursal, String stipoSucursal, String snumeroSucursal, String sdireccion, String stelefono, int iidCiudad, String sciudad, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidTipoSucursal = iidTipoSucursal;
        this.stipoSucursal = stipoSucursal;
        this.snumeroSucursal = snumeroSucursal;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entSucursal copiar(entSucursal destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdTipoSucursal(), this.getTipoSucursal(), this.getNumeroSucursal(), this.getDireccion(), this.getTelefono(), this.getIdCiudad(), this.getCiudad(), this.getEstadoRegistro());
        return destino;
    }
    
    public entSucursal cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdTipoSucursal(rs.getInt("idtiposucursal")); }
        catch(Exception e) {}
        try { this.setTipoSucursal(rs.getString("tiposucursal")); }
        catch(Exception e) {}
        try { this.setNumeroSucursal(rs.getString("numerosucursal")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIdCiudad(rs.getInt("idciudad")); }
        catch(Exception e) {}
        try { this.setCiudad(rs.getString("ciudad")); }
        catch(Exception e) {}
        return this;
    }

    public void setIdTipoSucursal(int iidTipoSucursal) {
        this.iidTipoSucursal = iidTipoSucursal;
    }
    
    public void setTipoSucursal(String stipoSucursal) {
        this.stipoSucursal = stipoSucursal;
    }
    
    public void setNumeroSucursal(String snumeroSucursal) {
        this.snumeroSucursal = snumeroSucursal;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setIdCiudad(int iidCiudad) {
        this.iidCiudad = iidCiudad;
    }
    
    public void setCiudad(String sciudad) {
        this.sciudad = sciudad;
    }
    
    public int getIdTipoSucursal() {
        return this.iidTipoSucursal;
    }
    
    public String getTipoSucursal() {
        return this.stipoSucursal;
    }
    
    public String getNumeroSucursal() {
        return this.snumeroSucursal;
    }
    
    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefono() {
        return this.stelefono;
    }
    
    public int getIdCiudad() {
        return this.iidCiudad;
    }
    
    public String getCiudad() {
        return this.sciudad;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdTipoSucursal() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_SUCURSAL;
            bvalido = false;
        }
        if (this.getNumeroSucursal().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_SUCURSAL;
            bvalido = false;
        }
        if (this.getIdCiudad() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CIUDAD;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT sucursal("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdTipoSucursal()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getIdCiudad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumeroSucursal())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Numero Sucursal", "numerosucursal", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(3, "Tipo Sucursal", "tiposucursal", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Ciudad", "ciudad", generico.entLista.tipo_texto));
        return lst;
    }

}
