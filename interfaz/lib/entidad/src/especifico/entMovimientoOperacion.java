/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMovimientoOperacion {

    private int iid;
    private int iidMovimiento;
    private int iidOperacion;
    private String stabla;
    private short hestadoRegistro;
    private String smensaje;

    public entMovimientoOperacion() {
        this.iid = 0;
        this.iidMovimiento = 0;
        this.iidOperacion = 0;
        this.stabla = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entMovimientoOperacion(int iid, int iidMovimiento, int iidOperacion, String stabla, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidMovimiento, int iidOperacion, String stabla, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public entMovimientoOperacion copiar(entMovimientoOperacion destino) {
        destino.setEntidad(this.getId(), this.getIdMovimiento(), this.getIdOperacion(), this.getTabla(), this.getEstadoRegistro());
        return destino;
    }

    public entMovimientoOperacion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setIdOperacion(rs.getInt("idoperacion")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setIdOperacion(int iidOperacion) {
        this.iidOperacion = iidOperacion;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public int getIdOperacion() {
        return this.iidOperacion;
    }

    public String getTabla() {
        return this.stabla;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public String getSentencia() {
        return "SELECT movimientooperacion("+
            this.getId()+","+
            this.getIdMovimiento()+","+
            this.getIdOperacion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getEstadoRegistro()+")";
    }

}
