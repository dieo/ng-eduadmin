/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleAgenda {

    private int iid;
    private int iidAtencion;
    private int iidMotivoAgenda;
    private String smotivoAgenda;
    private java.util.Date dfechaAgenda;
    private String shoraAgenda;
    private String sobservacion;
    private String sregional;
    private String stelefonoSocio;
    private String stelefonoDestino;
    private int iidDependencia;
    private String sdependencia;
    private boolean bactivo;
    private int iidUsuario;
    private String susuarioNombreApellido;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_TELEFONO_DESTINO = 12;
    public static final int LONGITUD_OBSERVACION = 250;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_MOTIVO_AGENDA = "Motivo de la Agenda (no vacío)";
    public static final String TEXTO_FECHA_AGENDA = "Fecha de Agenda (no vacía)";
    public static final String TEXTO_HORA_AGENDA = "Hora de Agenda (no vacía)";
    public static final String TEXTO_OBSERVACION = "Observación (no vacía)";
    public static final String TEXTO_REGIONAL = "Regional del socio (no editable)";
    public static final String TEXTO_TELEFONO_SOCIO = "Teléfonos del socio (no editable)";
    public static final String TEXTO_TELEFONO_DESTINO = "Teléfono Destino (no vacío, hasta " + LONGITUD_TELEFONO_DESTINO + " caracteres)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia (no editable)";
    public static final String TEXTO_ACTIVO = "Estado Activo de Agenda";
    public static final String TEXTO_USUARIO = "Usuario (no editable)";

    public entDetalleAgenda() {
        this.iid = 0;
        this.iidAtencion = 0;
        this.iidMotivoAgenda = 0;
        this.smotivoAgenda = "";
        this.dfechaAgenda = null;
        this.shoraAgenda = "";
        this.sobservacion = "";
        this.sregional = "";
        this.stelefonoSocio = "";
        this.stelefonoDestino = "";
        this.iidDependencia = 0;
        this.sdependencia = "";
        this.bactivo = true;
        this.iidUsuario = 0;
        this.susuarioNombreApellido = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetalleAgenda(int iid, int iidAtencion, int iidMotivoAgenda, String smotivoAgenda, java.util.Date dfechaAgenda, String shoraAgenda, String sobservacion, String sregional, String stelefonoSocio, String stelefonoDestino, int iidDependencia, String sdependencia, boolean bactivo, int iidUsuario, String susuarioNombreApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidAtencion = iidAtencion;
        this.iidMotivoAgenda = iidMotivoAgenda;
        this.smotivoAgenda = smotivoAgenda;
        this.dfechaAgenda = dfechaAgenda;
        this.shoraAgenda = shoraAgenda;
        this.sobservacion = sobservacion;
        this.sregional = sregional;
        this.stelefonoSocio = stelefonoSocio;
        this.stelefonoDestino = stelefonoDestino;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.bactivo = bactivo;
        this.iidUsuario = iidUsuario;
        this.susuarioNombreApellido = susuarioNombreApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidAtencion, int iidMotivoAgenda, String smotivoAgenda, java.util.Date dfechaAgenda, String shoraAgenda, String sobservacion, String sregional, String stelefonoSocio, String stelefonoDestino, int iidDependencia, String sdependencia, boolean bactivo, int iidUsuario, String susuarioNombreApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidAtencion = iidAtencion;
        this.iidMotivoAgenda = iidMotivoAgenda;
        this.smotivoAgenda = smotivoAgenda;
        this.dfechaAgenda = dfechaAgenda;
        this.shoraAgenda = shoraAgenda;
        this.sobservacion = sobservacion;
        this.sregional = sregional;
        this.stelefonoSocio = stelefonoSocio;
        this.stelefonoDestino = stelefonoDestino;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.bactivo = bactivo;
        this.iidUsuario = iidUsuario;
        this.susuarioNombreApellido = susuarioNombreApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleAgenda copiar(entDetalleAgenda destino) {
        destino.setEntidad(this.getId(), this.getIdAtencion(), this.getIdMotivoAgenda(), this.getMotivoAgenda(), this.getFechaAgenda(), this.getHoraAgenda(), this.getObservacion(), this.getRegional(), this.getTelefonoSocio(), this.getTelefonoDestino(), this.getIdDependencia(), this.getDependencia(), this.getActivo(), this.getIdUsuario(), this.getUsuarioNombreApellido(), this.getEstadoRegistro());
        return destino;
    }
    
    public entDetalleAgenda cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdAtencion(rs.getInt("idatencion")); }
        catch(Exception e) {}
        try { this.setIdMotivoAgenda(rs.getInt("idmotivoagenda")); }
        catch(Exception e) {}
        try { this.setMotivoAgenda(rs.getString("motivoagenda")); }
        catch(Exception e) {}
        try { this.setFechaAgenda(rs.getDate("fechaagenda")); }
        catch(Exception e) {}
        try { this.setHoraAgenda(rs.getString("horaagenda")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setTelefonoSocio(rs.getString("telefonosocio")); }
        catch(Exception e) {}
        try { this.setTelefonoDestino(rs.getString("telefonodestino")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuarioNombreApellido(rs.getString("usuarionombre")+" "+rs.getString("usuarioapellido")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdAtencion(int iidAtencion) {
        this.iidAtencion = iidAtencion;
    }

    public void setIdMotivoAgenda(int iidMotivoAgenda) {
        this.iidMotivoAgenda = iidMotivoAgenda;
    }

    public void setMotivoAgenda(String smotivoAgenda) {
        this.smotivoAgenda = smotivoAgenda;
    }

    public void setFechaAgenda(java.util.Date dfechaAgenda) {
        this.dfechaAgenda = dfechaAgenda;
    }

    public void setHoraAgenda(String shoraAgenda) {
        this.shoraAgenda = shoraAgenda;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setTelefonoSocio(String stelefonoSocio) {
        this.stelefonoSocio = stelefonoSocio;
    }

    public void setTelefonoDestino(String stelefonoDestino) {
        this.stelefonoDestino = stelefonoDestino;
    }

    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }

    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }

    public void setUsuarioNombreApellido(String susuarioNombreApellido) {
        this.susuarioNombreApellido = susuarioNombreApellido;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdAtencion() {
        return this.iidAtencion;
    }

    public int getIdMotivoAgenda() {
        return this.iidMotivoAgenda;
    }

    public String getMotivoAgenda() {
        return this.smotivoAgenda;
    }

    public java.util.Date getFechaAgenda() {
        return this.dfechaAgenda;
    }

    public String getHoraAgenda() {
        return this.shoraAgenda;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public String getRegional() {
        return this.sregional;
    }

    public String getTelefonoSocio() {
        return this.stelefonoSocio;
    }

    public String getTelefonoDestino() {
        return this.stelefonoDestino;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }

    public String getDependencia() {
        return this.sdependencia;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }

    public String getUsuarioNombreApellido() {
        return this.susuarioNombreApellido;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdUsuario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_USUARIO;
            bvalido = false;
        }
        if (this.getIdDependencia()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DEPENDENCIA;
            bvalido = false;
        }
        if (this.getTelefonoDestino().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO_DESTINO;
            bvalido = false;
        }
        if (this.getIdMotivoAgenda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MOTIVO_AGENDA;
            bvalido = false;
        }
        if (this.getFechaAgenda()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_AGENDA;
            bvalido = false;
        }
        if (this.getHoraAgenda().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_AGENDA;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT detalleagenda("+
            this.getId()+","+
            this.getIdAtencion()+","+
            this.getIdMotivoAgenda()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAgenda())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraAgenda())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoDestino())+","+
            this.getIdDependencia()+","+
            this.getIdUsuario()+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
