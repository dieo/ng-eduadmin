/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entGenericaPersona;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entListaSocio extends entGenericaPersona {
    
    protected int inumeroSocio;
    protected int iidEstado;
    protected String sestado;
    protected String stelefonoCelularPrincipal;
    protected String stelefono;
    protected double uatraso;
    protected boolean bacepta;
    
    public static final int LONGITUD_DESCRIPCION = 50;

    public static final String TEXTO_DESCRIPCION = "Texto a buscar";

    public static final String TEXTO_CEDULA = "Cédula";
    public static final String TEXTO_NOMBRE = "Nombre";
    public static final String TEXTO_APELLIDO = "Apellido";
    public static final String TEXTO_NUMERO = "Número Socio";
    public static final String TEXTO_ESTADO = "Estado";
    public static final String TEXTO_ATRASO = "Atraso";
    public static final String TEXTO_ACEPTA = "Acepta";

    public entListaSocio() {
        super();
        this.inumeroSocio = 0;
        this.iidEstado = 0;
        this.sestado = "";
        this.stelefonoCelularPrincipal = "";
        this.stelefono = "";
        this.uatraso = 0.0;
        this.bacepta = false;
    }

    public entListaSocio(int iid, String snombre, String sapellido, String scedula, int inumeroSocio, int iidEstado, String sestado, String stelefonoCelularPrincipal, String stelefono, double uatraso, boolean bacepta) {
        super(iid, snombre, sapellido, scedula, (short)0);
        this.inumeroSocio = inumeroSocio;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.stelefonoCelularPrincipal = stelefonoCelularPrincipal;
        this.stelefono = stelefono;
        this.uatraso = uatraso;
        this.bacepta = bacepta;
    }

    public void setEntidad(int iid, String snombre, String sapellido, String scedula, int inumeroSocio, int iidEstado, String sestado, String stelefonoCelularPrincipal, String stelefono, double uatraso, boolean bacepta) {
        this.iid = iid;
        this.snombre = snombre.trim().toUpperCase();
        this.sapellido = sapellido.trim().toUpperCase();
        this.scedula = scedula;
        this.inumeroSocio = inumeroSocio;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.stelefonoCelularPrincipal = stelefonoCelularPrincipal;
        this.stelefono = stelefono;
        this.uatraso = uatraso;
        this.bacepta = bacepta;
        this.hestadoRegistro = (short)0;
    }

    public entListaSocio copiar(entListaSocio destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getNumeroSocio(), this.getIdEstado(), this.getEstado(), this.getTelefonoCelularPrincipal(), this.getTelefono(), this.getAtraso(), this.getAcepta());
        return destino;
    }
    
    public entListaSocio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) { }
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) { }
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) { }
        try { this.setNumeroSocio(rs.getInt("numerosocio")); }
        catch(Exception e) { }
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) { }
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) { }
        try { this.setTelefonoCelularPrincipal(rs.getString("telefonocelularprincipal")); }
        catch(Exception e) { }
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) { }
        try { this.setAtraso(rs.getDouble("atrasomax")); }
        catch(Exception e) { }
        try { this.setAcepta(false); }
        catch(Exception e) { }
        return this;
    }
    
    public void setNumeroSocio(int inumeroSocio) {
        this.inumeroSocio = inumeroSocio;
    }
    
    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }
    
    public void setEstado(String sestado) {
        this.sestado = sestado;
    }
    
    public void setTelefonoCelularPrincipal(String stelefonoCelularPrincipal) {
        this.stelefonoCelularPrincipal = stelefonoCelularPrincipal;
    }
    
    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }
    
    public void setAtraso(double uatraso) {
        this.uatraso = uatraso;
    }
    
    public void setAcepta(boolean bacepta) {
        this.bacepta = bacepta;
    }
    
    public int getNumeroSocio() {
        return this.inumeroSocio;
    }
    
    public int getIdEstado() {
        return this.iidEstado;
    }
    
    public String getEstado() {
        return this.sestado;
    }
    
    public String getTelefonoCelularPrincipal() {
        return this.stelefonoCelularPrincipal;
    }
    
    public String getTelefono() {
        return this.stelefono;
    }
    
    public double getAtraso() {
        return this.uatraso;
    }
    
    public boolean getAcepta() {
        return this.bacepta;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Apellido", "apellido", generico.entLista.tipo_texto));
        return lst;
    }
    
}
