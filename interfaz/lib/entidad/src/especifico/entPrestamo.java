/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPrestamo {

    private int iid;
    private int iidSocio;
    private String scedula;
    private int iidMoneda;
    private int inumeroSolicitud;
    private java.util.Date dfechaSolicitud;
    private double dmontoSolicitud;
    private int iplazoSolicitud;
    private int iidPromotor;
    private String spromotor;
    private String sdestino;
    private int iidEntidad;
    private short iidTipoCredito;
    private short iidEstadoSolicitud;
    private String sestadoSolicitud;
    private double dtasaRetencion;
    private String sobservacionSolicitud;
    private int iplazoMaximo;

    private int inumeroActa;
    private java.util.Date dfechaActa;
    private java.util.Date dfechaAprobado;
    private double dmontoAprobado;
    private int iplazoAprobado;
    private int iidAutorizador;
    private String sautorizador;
    private String sobservacionAprobado;
    private int iidRegional;
    private String sregional;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    private java.util.Date dfechaRechazado;
    private String sobservacionRechazado;
    
    private java.util.Date dfechaReconsiderado;
    private String sobservacionReconsiderado;
    
    private double dtasaInteres;
    private int inumeroBoleta;
    private int iidGiraduria;
    private String sgiraduria;
    private java.util.Date dfechaEntrega;
    private String sobservacionEntrega;
    private java.util.Date dfechaPrimerVencimiento;
    private java.util.Date dfechaVencimiento;
    private java.util.Date dfechaImpresion;
    private int inumeroOperacion;
    
    private int iidCuentaFija;
    private String scuentaFija;
    private short hestadoRegistro;
    private String smensaje;

    public static final int INICIO_PLAZO = 1;
    public static final int FIN_PLAZO = 99;
    public static final int INICIO_DIA_VENCIMIENTO = 0;
    public static final int FIN_DIA_VENCIMIENTO = 31;
    public static final int LONGITUD_DESTINO = 100;
    public static final int LONGITUD_OBSERVACION = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ESTADO = "Estado de Solicitud (no editable)";
    public static final String TEXTO_NUMERO_SOLICITUD = "Número de Solicitud (no editable)";
    public static final String TEXTO_FECHA_IMPRESION = "Fecha de Impresión de Boleta";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_FECHA_SOLICITUD = "Fecha de Solicitud (no vacía)";
    public static final String TEXTO_ENTIDAD = "Entidad (no vacía)";
    public static final String TEXTO_CIUDAD_DEPARTAMENTO = "Ciudad - Departamento (no editable)";
    public static final String TEXTO_PLAZO_MAXIMO = "Plazo máximo (no editable)";
    public static final String TEXTO_MONTO_SOLICITUD = "Monto Solicitado (valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_PLAZO_SOLICITUD = "Plazo Solicitado (no vacío, desde " + INICIO_PLAZO + " hasta " + FIN_PLAZO +")";
    public static final String TEXTO_PROMOTOR = "Promotor (no vacío)";
    public static final String TEXTO_DESTINO = "Destino (hasta " + LONGITUD_DESTINO + " caracteres)";
    public static final String TEXTO_OBSERVACION_SOLICITUD = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public static final String TEXTO_NUMERO_ACTA = "Número de Acta (valor positivo)";
    public static final String TEXTO_FECHA_ACTA = "Fecha de Acta";
    public static final String TEXTO_FECHA_APROBADO = "Fecha de Aprobación (no editable)";
    public static final String TEXTO_MONTO_APROBADO = "Monto Aprobado (valor positivo)";
    public static final String TEXTO_PLAZO_APROBADO = "Plazo Aprobado (no vacío, desde " + INICIO_PLAZO + " hasta " + FIN_PLAZO +")";
    public static final String TEXTO_AUTORIZADOR = "Autorizador (no vacía)";
    public static final String TEXTO_OBSERVACION_APROBADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_REGIONAL = "Regional (no vacía)";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_RECHAZADO = "Fecha de Rechazo (no editable)";
    public static final String TEXTO_OBSERVACION_RECHAZADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_RECONSIDERADO = "Fecha de Reconsideración (no editable)";
    public static final String TEXTO_OBSERVACION_RECONSIDERADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public static final String TEXTO_FECHA_PRIMER_VENCIMIENTO = "Fecha del Primer Vencimiento";
    public static final String TEXTO_FECHA_VENCIMIENTO = "Fecha de Vencimiento de la Operación (no editable)";
    public static final String TEXTO_NUMERO_BOLETA = "Número de Boleta (valor positivo, único)";
    public static final String TEXTO_GIRADURIA = "Giraduría (no vacío)";
    public static final String TEXTO_FECHA_ENTREGADO = "Fecha de Entrega (no editable)";
    public static final String TEXTO_OBSERVACION_ENTREGADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (no editable)";
    
    public entPrestamo() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.iidMoneda = 0;
        this.inumeroSolicitud = 0;
        this.dfechaSolicitud = null;
        this.dmontoSolicitud = 0.0;
        this.iplazoSolicitud = 0;
        this.sdestino = "";
        this.iidEntidad = 0;
        this.iidTipoCredito = 0;
        this.inumeroBoleta = 0;
        this.inumeroActa = 0;
        this.dfechaActa = null;
        this.dmontoAprobado = 0.0;
        this.iplazoAprobado = 0;
        this.dtasaInteres = 0.0;
        this.dfechaAprobado = null;
        this.dfechaPrimerVencimiento = null;
        this.dfechaVencimiento = null;
        this.iidEstadoSolicitud = (short)0;
        this.sestadoSolicitud = "";
        this.iidAutorizador = 0;
        this.sautorizador = "";
        this.dtasaRetencion = 0.0;
        this.iidPromotor = 0;
        this.spromotor = "";
        this.iidGiraduria = 0;
        this.sgiraduria = "";
        this.iidCuentaFija = 0;
        this.scuentaFija = "";
        this.sobservacionSolicitud = "";
        this.sobservacionAprobado = "";
        this.dfechaImpresion = null;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.dfechaRechazado = null;
        this.sobservacionRechazado = "";
        this.dfechaReconsiderado = null;
        this.sobservacionReconsiderado = "";
        this.dfechaEntrega = null;
        this.sobservacionEntrega = "";
        this.iplazoMaximo = 0;
        this.iidRegional = 0;
        this.sregional = "";
        this.inumeroOperacion = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entPrestamo(int iid, int iidSocio, String scedula, int iidMoneda, int inumeroSolicitud, java.util.Date dfechaSolicitud, double dmontoSolicitud, int iplazoSolicitud, String sdestino, int iidEntidad, short iidTipoCredito, int inumeroBoleta, int inumeroActa, java.util.Date dfechaActa, double dmontoAprobado, int iplazoAprobado, double dtasaInteres, java.util.Date dfechaAprobado, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, short iidEstadoSolicitud, String sestadoSolicitud, int iidAutorizador, String sautorizador, double dtasaRetencion, int iidPromotor, String spromotor, int iidGiraduria, String sgiraduria, int iidCuentaFija, String scuentaFija, String sobservacionSolicitud, String sobservacionAprobado, java.util.Date dfechaImpresion, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRechazado, String sobservacionRechazado, java.util.Date dfechaReconsiderado, String sobservacionReconsiderado, java.util.Date dfechaEntrega, String sobservacionEntrega, int iidRegional, String sregional, int inumeroOperacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.iidMoneda = iidMoneda;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaSolicitud = dfechaSolicitud;
        this.dmontoSolicitud = dmontoSolicitud;
        this.iplazoSolicitud = iplazoSolicitud;
        this.sdestino = sdestino;
        this.iidEntidad = iidEntidad;
        this.iidTipoCredito = iidTipoCredito;
        this.inumeroBoleta = inumeroBoleta;
        this.inumeroActa = inumeroActa;
        this.dfechaActa = dfechaActa;
        this.dmontoAprobado = dmontoAprobado;
        this.iplazoAprobado = iplazoAprobado;
        this.dtasaInteres = dtasaInteres;
        this.dfechaAprobado = dfechaAprobado;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.iidEstadoSolicitud = iidEstadoSolicitud;
        this.sestadoSolicitud = sestadoSolicitud;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.dtasaRetencion = dtasaRetencion;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.iidCuentaFija = iidCuentaFija;
        this.sobservacionSolicitud = sobservacionSolicitud;
        this.sobservacionAprobado = sobservacionAprobado;
        this.dfechaImpresion = dfechaImpresion;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRechazado = dfechaRechazado;
        this.sobservacionRechazado = sobservacionRechazado;
        this.dfechaReconsiderado = dfechaReconsiderado;
        this.sobservacionReconsiderado = sobservacionReconsiderado;
        this.dfechaEntrega = dfechaEntrega;
        this.sobservacionEntrega = sobservacionEntrega;
        this.iplazoMaximo = 0;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.inumeroOperacion = inumeroOperacion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, int iidMoneda, int inumeroSolicitud, java.util.Date dfechaSolicitud, double dmontoSolicitud, int iplazoSolicitud, String sdestino, int iidEntidad, short iidTipoCredito, int inumeroBoleta, int inumeroActa, java.util.Date dfechaActa, double dmontoAprobado, int iplazoAprobado, double dtasaInteres, java.util.Date dfechaAprobado, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, short iidEstadoSolicitud, String sestadoSolicitud, int iidAutorizador, String sautorizador, double dtasaRetencion, int iidPromotor, String spromotor, int iidGiraduria, String sgiraduria, int iidCuentaFija, String scuentaFija, String sobservacionSolicitud, String sobservacionAprobado, java.util.Date dfechaImpresion, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRechazado, String sobservacionRechazado, java.util.Date dfechaReconsiderado, String sobservacionReconsiderado, java.util.Date dfechaEntrega, String sobservacionEntrega, int iidRegional, String sregional, int inumeroOperacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.iidMoneda = iidMoneda;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaSolicitud = dfechaSolicitud;
        this.dmontoSolicitud = dmontoSolicitud;
        this.iplazoSolicitud = iplazoSolicitud;
        this.sdestino = sdestino;
        this.iidEntidad = iidEntidad;
        this.iidTipoCredito = iidTipoCredito;
        this.inumeroBoleta = inumeroBoleta;
        this.inumeroActa = inumeroActa;
        this.dfechaActa = dfechaActa;
        this.dmontoAprobado = dmontoAprobado;
        this.iplazoAprobado = iplazoAprobado;
        this.dtasaInteres = dtasaInteres;
        this.dfechaAprobado = dfechaAprobado;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.iidEstadoSolicitud = iidEstadoSolicitud;
        this.sestadoSolicitud = sestadoSolicitud;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.dtasaRetencion = dtasaRetencion;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.iidCuentaFija = iidCuentaFija;
        this.scuentaFija = scuentaFija;
        this.sobservacionSolicitud = sobservacionSolicitud;
        this.sobservacionAprobado = sobservacionAprobado;
        this.dfechaImpresion = dfechaImpresion;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRechazado = dfechaRechazado;
        this.sobservacionRechazado = sobservacionRechazado;
        this.dfechaReconsiderado = dfechaReconsiderado;
        this.sobservacionReconsiderado = sobservacionReconsiderado;
        this.dfechaEntrega = dfechaEntrega;
        this.sobservacionEntrega = sobservacionEntrega;
        this.iplazoMaximo = 0;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.inumeroOperacion = inumeroOperacion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entPrestamo copiar(entPrestamo destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getIdMoneda(), this.getNumeroSolicitud(), this.getFechaSolicitud(), this.getMontoSolicitud(), this.getPlazoSolicitud(), this.getDestino(), this.getIdEntidad(), this.getIdTipoCredito(), this.getNumeroBoleta(), this.getNumeroActa(), this.getFechaActa(), this.getMontoAprobado(), this.getPlazoAprobado(), this.getTasaInteres(), this.getFechaAprobado(), this.getFechaPrimerVencimiento(), this.getFechaVencimiento(), this.getIdEstadoSolicitud(), this.getEstadoSolicitud(), this.getIdAutorizador(), this.getAutorizador(), this.getTasaRetencion(), this.getIdPromotor(), this.getPromotor(), this.getIdGiraduria(), this.getGiraduria(), this.getIdCuentaFija(), this.getCuentaFija(), this.getObservacionSolicitud(), this.getObservacionAprobado(), this.getFechaImpresion(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getFechaRechazado(), this.getObservacionRechazado(), this.getFechaReconsiderado(), this.getObservacionReconsiderado(), this.getFechaEntrega(), this.getObservacionEntrega(), this.getIdRegional(), this.getRegional(), this.getNumeroOperacion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entPrestamo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setFechaSolicitud(rs.getDate("fechasolicitud")); }
        catch(Exception e) {}
        try { this.setMontoSolicitud(rs.getDouble("montosolicitud")); }
        catch(Exception e) {}
        try { this.setPlazoSolicitud(rs.getInt("plazosolicitud")); }
        catch(Exception e) {}
        try { this.setDestino(rs.getString("destino")); }
        catch(Exception e) {}
        try { this.setObservacionSolicitud(rs.getString("observacionsolicitud")); }
        catch(Exception e) {}
        try { this.setIdEntidad(rs.getInt("identidad")); }
        catch(Exception e) {}
        try { this.setIdTipoCredito(rs.getShort("idtipocredito")); }
        catch(Exception e) {}
        try { this.setNumeroBoleta(rs.getInt("numeroboleta")); }
        catch(Exception e) {}
        try { this.setNumeroActa(rs.getInt("numeroacta")); }
        catch(Exception e) {}
        try { this.setFechaActa(rs.getDate("fechaacta")); }
        catch(Exception e) {}
        try { this.setMontoAprobado(rs.getInt("montoaprobado")); }
        catch(Exception e) {}
        try { this.setPlazoAprobado(rs.getInt("plazoaprobado")); }
        catch(Exception e) {}
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(Exception e) {}
        try { this.setFechaAprobado(rs.getDate("fechaaprobado")); }
        catch(Exception e) {}
        try { this.setObservacionAprobado(rs.getString("observacionaprobado")); }
        catch(Exception e) {}
        try { this.setFechaPrimerVencimiento(rs.getDate("fechaprimervencimiento")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setIdEstadoSolicitud(rs.getShort("idestado")); }
        catch(Exception e) {}
        try { this.setEstadoSolicitud(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setIdAutorizador(rs.getShort("idautorizador")); }
        catch(Exception e) {}
        try { this.setTasaRetencion(rs.getDouble("tasaretencion")); }
        catch(Exception e) {}
        try { this.setIdPromotor(rs.getInt("idpromotor")); }
        catch(Exception e) {}
        try { this.setIdGiraduria(rs.getInt("idgiraduria")); }
        catch(Exception e) {}
        try { this.setIdCuentaFija(rs.getInt("idcuentafija")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setFechaRechazado(rs.getDate("fecharechazado")); }
        catch(Exception e) {}
        try { this.setObservacionRechazado(rs.getString("observacionrechazado")); }
        catch(Exception e) {}
        try { this.setFechaReconsiderado(rs.getDate("fechareconsiderado")); }
        catch(Exception e) {}
        try { this.setObservacionReconsiderado(rs.getString("observacionreconsiderado")); }
        catch(Exception e) {}
        try { this.setFechaEntrega(rs.getDate("fechaentrega")); }
        catch(Exception e) {}
        try { this.setObservacionEntrega(rs.getString("observacionentrega")); }
        catch(Exception e) {}
        try { this.setFechaImpresion(rs.getDate("fechaimpresion")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setFechaSolicitud(java.util.Date dfechaSolicitud) {
        this.dfechaSolicitud = dfechaSolicitud;
    }

    public void setMontoSolicitud(double dmontoSolicitud) {
        this.dmontoSolicitud = dmontoSolicitud;
    }

    public void setPlazoSolicitud(int iplazoSolicitud) {
        this.iplazoSolicitud = iplazoSolicitud;
    }

    public void setDestino(String sdestino) {
        this.sdestino = sdestino;
    }

    public void setIdEntidad(int iidEntidad) {
        this.iidEntidad = iidEntidad;
    }

    public void setIdTipoCredito(short iidTipoCredito) {
        this.iidTipoCredito = iidTipoCredito;
    }

    public void setNumeroBoleta(int inumeroBoleta) {
        this.inumeroBoleta = inumeroBoleta;
    }

    public void setNumeroActa(int inumeroActa) {
        this.inumeroActa = inumeroActa;
    }

    public void setFechaActa(java.util.Date dfechaActa) {
        this.dfechaActa = dfechaActa;
    }

    public void setMontoAprobado(double dmontoAprobado) {
        this.dmontoAprobado = dmontoAprobado;
    }

    public void setPlazoAprobado(int iplazoAprobado) {
        this.iplazoAprobado = iplazoAprobado;
    }

    public void setTasaInteres(double dtasaInteres) {
        this.dtasaInteres = dtasaInteres;
    }

    public void setFechaAprobado(java.util.Date dfechaAprobado) {
        this.dfechaAprobado = dfechaAprobado;
    }

    public void setFechaPrimerVencimiento(java.util.Date dfechaPrimerVencimiento) {
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setIdEstadoSolicitud(short iidEstadoSolicitud) {
        this.iidEstadoSolicitud = iidEstadoSolicitud;
    }

    public void setEstadoSolicitud(String sestadoSolicitud) {
        this.sestadoSolicitud = sestadoSolicitud;
    }

    public void setIdAutorizador(int iidAutorizador) {
        this.iidAutorizador = iidAutorizador;
    }

    public void setAutorizador(String sautorizador) {
        this.sautorizador = sautorizador;
    }

    public void setTasaRetencion(double dtasaRetencion) {
        this.dtasaRetencion = dtasaRetencion;
    }

    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }

    public void setPromotor(String spromotor) {
        this.spromotor = spromotor;
    }

    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }

    public void setGiraduria(String sgiraduria) {
        this.sgiraduria = sgiraduria;
    }

    public void setIdCuentaFija(int iidCuentaFija) {
        this.iidCuentaFija = iidCuentaFija;
    }

    public void setCuentaFija(String scuentaFija) {
        this.scuentaFija = scuentaFija;
    }

    public void setObservacionSolicitud(String sobservacionSolicitud) {
        this.sobservacionSolicitud = sobservacionSolicitud;
    }

    public void setObservacionAprobado(String sobservacionAprobado) {
        this.sobservacionAprobado = sobservacionAprobado;
    }

    public void setFechaImpresion(java.util.Date dfechaImpresion) {
        this.dfechaImpresion = dfechaImpresion;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setFechaRechazado(java.util.Date dfechaRechazado) {
        this.dfechaRechazado = dfechaRechazado;
    }

    public void setObservacionRechazado(String sobservacionRechazado) {
        this.sobservacionRechazado = sobservacionRechazado;
    }

    public void setFechaReconsiderado(java.util.Date dfechaReconsiderado) {
        this.dfechaReconsiderado = dfechaReconsiderado;
    }

    public void setObservacionReconsiderado(String sobservacionReconsiderado) {
        this.sobservacionReconsiderado = sobservacionReconsiderado;
    }

    public void setFechaEntrega(java.util.Date dfechaEntrega) {
        this.dfechaEntrega = dfechaEntrega;
    }

    public void setObservacionEntrega(String sobservacionEntrega) {
        this.sobservacionEntrega = sobservacionEntrega;
    }

    public void setPlazoMaximo(int iplazoMaximo) {
        this.iplazoMaximo = iplazoMaximo;
    }
    
    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }
    
    public void setRegional(String sregional) {
        this.sregional = sregional;
    }
    
    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public java.util.Date getFechaSolicitud() {
        return this.dfechaSolicitud;
    }

    public double getMontoSolicitud() {
        return this.dmontoSolicitud;
    }

    public int getPlazoSolicitud() {
        return this.iplazoSolicitud;
    }

    public String getDestino() {
        return this.sdestino;
    }

    public int getIdEntidad() {
        return this.iidEntidad;
    }

    public short getIdTipoCredito() {
        return this.iidTipoCredito;
    }

    public int getNumeroBoleta() {
        return this.inumeroBoleta;
    }

    public int getNumeroActa() {
        return this.inumeroActa;
    }

    public java.util.Date getFechaActa() {
        return this.dfechaActa;
    }

    public double getMontoAprobado() {
        return this.dmontoAprobado;
    }

    public int getPlazoAprobado() {
        return this.iplazoAprobado;
    }

    public double getTasaInteres() {
        return this.dtasaInteres;
    }

    public java.util.Date getFechaAprobado() {
        return this.dfechaAprobado;
    }

    public java.util.Date getFechaPrimerVencimiento() {
        return this.dfechaPrimerVencimiento;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public short getIdEstadoSolicitud() {
        return this.iidEstadoSolicitud;
    }

    public String getEstadoSolicitud() {
        return this.sestadoSolicitud;
    }

    public int getIdAutorizador() {
        return this.iidAutorizador;
    }

    public String getAutorizador() {
        return this.sautorizador;
    }

    public double getTasaRetencion() {
        return this.dtasaRetencion;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public String getPromotor() {
        return this.spromotor;
    }

    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public String getGiraduria() {
        return this.sgiraduria;
    }

    public int getIdCuentaFija() {
        return this.iidCuentaFija;
    }

    public String getCuentaFija() {
        return this.scuentaFija;
    }

    public String getObservacionSolicitud() {
        return this.sobservacionSolicitud;
    }

    public String getObservacionAprobado() {
        return this.sobservacionAprobado;
    }

    public java.util.Date getFechaImpresion() {
        return this.dfechaImpresion;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public java.util.Date getFechaRechazado() {
        return this.dfechaRechazado;
    }

    public String getObservacionRechazado() {
        return this.sobservacionRechazado;
    }

    public java.util.Date getFechaReconsiderado() {
        return this.dfechaReconsiderado;
    }

    public String getObservacionReconsiderado() {
        return this.sobservacionReconsiderado;
    }

    public java.util.Date getFechaEntrega() {
        return this.dfechaEntrega;
    }

    public String getObservacionEntrega() {
        return this.sobservacionEntrega;
    }

    public int getPlazoMaximo() {
        return this.iplazoMaximo;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoSolicitar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdSocio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getFechaSolicitud()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_SOLICITUD;
            bvalido = false;
        }
        if (this.getIdEntidad()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ENTIDAD;
            bvalido = false;
        }
        if (this.getMontoSolicitud()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_SOLICITUD;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getPlazoSolicitud()<INICIO_PLAZO || this.getPlazoSolicitud()>FIN_PLAZO) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO_SOLICITUD;
            bvalido = false;
        }
        if (this.getPlazoSolicitud()>this.getPlazoMaximo()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Plazo solicitado ("+this.getPlazoSolicitud()+") sobrepasa el Plazo máximo establecido ("+this.getPlazoMaximo()+")";
            bvalido = false;
        }
        if (this.getIdPromotor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PROMOTOR;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAprobar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getMontoAprobado()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_APROBADO;
            bvalido = false;
        }
        if (this.getMontoAprobado()>this.getMontoSolicitud()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Monto aprobado ("+this.getMontoAprobado()+") no puede exceder al Monto solicitado ("+this.getMontoSolicitud()+")";
            bvalido = false;
        }
        if (this.getPlazoAprobado()<INICIO_PLAZO || this.getPlazoAprobado()>FIN_PLAZO) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO_APROBADO;
            bvalido = false;
        }
        if (this.getPlazoAprobado()>this.getPlazoSolicitud()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Plazo aprobado ("+this.getPlazoAprobado()+") no puede exceder al Plazo solicitado ("+this.getPlazoSolicitud()+")";
            bvalido = false;
        }
        if (this.getIdAutorizador()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_AUTORIZADOR;
            bvalido = false;
        }
        if (this.getObservacionAprobado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_APROBADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoImprimir() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNumeroBoleta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_BOLETA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoRechazar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionRechazado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_RECHAZADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoEntregar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdGiraduria()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_GIRADURIA;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        if (this.getObservacionEntrega().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ENTREGADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        if (this.getEstadoRegistro() == generico.entConstante.estadoregistro_modificado) return this.getCampoModificacion();
        if (this.getEstadoRegistro() == generico.entConstante.estadoregistro_insertado) return this.getCampoInsercion();
        if (this.getEstadoRegistro() == generico.entConstante.estadoregistro_eliminado) return this.getCampoEliminacion();
        return "";
    }

    // métodos para la conexión
    private String getCampoInsercion() {
        return "SELECT insertarmovimiento("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdMoneda()+","+
            this.getNumeroSolicitud()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaSolicitud())+","+
            this.getMontoSolicitud()+","+
            this.getPlazoSolicitud()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDestino())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionSolicitud())+","+
            this.getIdEntidad()+","+
            this.getIdTipoCredito()+","+
            this.getNumeroBoleta()+","+
            this.getNumeroActa()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaActa())+","+
            this.getMontoAprobado()+","+
            this.getPlazoAprobado()+","+
            this.getTasaInteres()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAprobado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAprobado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaPrimerVencimiento())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaVencimiento())+","+
            this.getIdEstadoSolicitud()+","+
            this.getIdAutorizador()+","+
            this.getTasaRetencion()+","+
            this.getIdPromotor()+","+
            this.getIdGiraduria()+","+
            this.getIdCuentaFija()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaRechazado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionRechazado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaReconsiderado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionReconsiderado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaEntrega())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionEntrega())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaImpresion())+","+
            this.getNumeroOperacion()+","+
            this.getIdRegional()+")";
    }

    private String getCampoEliminacion() {
        return "SELECT eliminar("+
            this.getId()+","+
            "'movimiento'"+")";
    }
    
    private String getCampoModificacion() {
        return "SELECT modificarmovimiento("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdMoneda()+","+
            this.getNumeroSolicitud()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaSolicitud())+","+
            this.getMontoSolicitud()+","+
            this.getPlazoSolicitud()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDestino())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionSolicitud())+","+
            this.getIdEntidad()+","+
            this.getIdTipoCredito()+","+
            this.getNumeroBoleta()+","+
            this.getNumeroActa()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaActa())+","+
            this.getMontoAprobado()+","+
            this.getPlazoAprobado()+","+
            this.getTasaInteres()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAprobado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAprobado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaPrimerVencimiento())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaVencimiento())+","+
            this.getIdEstadoSolicitud()+","+
            this.getIdAutorizador()+","+
            this.getTasaRetencion()+","+
            this.getIdPromotor()+","+
            this.getIdGiraduria()+","+
            this.getIdCuentaFija()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaRechazado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionRechazado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaReconsiderado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionReconsiderado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaEntrega())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionEntrega())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaImpresion())+","+
            this.getNumeroOperacion()+","+
            this.getIdRegional()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha Solicitud", "fechasolicitud", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(2, "Fecha Aprobación", "fechaaprobado", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Número Solicitud", "numerosolicitud", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Estado", "estado", generico.entLista.tipo_texto));
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha Solicitud", "fechasolicitud", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(2, "Fecha Aprobación", "fechaaprobado", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Número Solicitud", "numerosolicitud", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Estado", "estado", generico.entLista.tipo_texto));
        return lst;
    }

}
