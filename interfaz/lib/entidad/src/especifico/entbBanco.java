/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbBanco extends generico.entGenerica{
    
    protected String sruc;
    protected String scontacto;
    protected String sweb;
    protected String semail;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final int LONGITUD_RUC = 12;
    public static final int LONGITUD_CONTACTO = 50;
    public static final int LONGITUD_WEB = 40;
    public static final int LONGITUD_EMAIL = 40;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Banco (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_RUC = "RUC de Entidad (única, hasta " + LONGITUD_RUC + " caracteres)";
    public static final String TEXTO_CONTACTO = "Contacto con Banco (hasta " + LONGITUD_CONTACTO + " caracteres)";
    public static final String TEXTO_WEB = "Página web del Banco (hasta " + LONGITUD_WEB + " caracteres)";
    public static final String TEXTO_EMAIL = "e-mail de Contacto (hasta " + LONGITUD_EMAIL + " caracteres)";

    public entbBanco() {
        super();
        this.sruc = "";
        this.scontacto = "";
        this.sweb = "";
        this.semail = "";
    }

    public entbBanco(int iid, String sdescripcion, String sruc, String scontacto, String sweb, String semail, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.sruc = sruc;
        this.scontacto = scontacto;
        this.sweb = sweb;
        this.semail = semail;
    }

    public void setEntidad(int iid, String sdescripcion, String sruc, String scontacto, String sweb, String semail, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.sruc = sruc;
        this.scontacto = scontacto;
        this.sweb = sweb;
        this.semail = semail;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entbBanco copiar(entbBanco destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getRuc(), this.getContacto(), this.getWeb(), this.getEmail(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbBanco cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) {}
        try { this.setContacto(rs.getString("contacto")); }
        catch(Exception e) {}
        try { this.setWeb(rs.getString("web")); }
        catch(Exception e) {}
        try { this.setEmail(rs.getString("email")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setRuc(String sruc) {
        this.sruc = sruc;
    }
    
    public void setContacto(String scontacto) {
        this.scontacto = scontacto;
    }
    
    public void setWeb(String sweb) {
        this.sweb = sweb;
    }
    
    public void setEmail(String semail) {
        this.semail = semail;
    }
    
    public String getRuc() {
        return this.sruc;
    }
    
    public String getContacto() {
        return this.scontacto;
    }
    
    public String getWeb() {
        return this.sweb;
    }
    
    public String getEmail() {
        return this.semail;
    }

    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (!this.getRuc().isEmpty() && esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RUC;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT banco.banco("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getRuc())+","+
            utilitario.utiCadena.getTextoGuardado(this.getContacto())+","+
            utilitario.utiCadena.getTextoGuardado(this.getWeb())+","+
            utilitario.utiCadena.getTextoGuardado(this.getEmail())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Ruc", "ruc", new generico.entLista().tipo_texto));
        return lst;
    }
    
}