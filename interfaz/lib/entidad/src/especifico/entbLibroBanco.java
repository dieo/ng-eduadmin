/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbLibroBanco {
    
    private int iidBancoCuenta;
    protected String sbancoCuenta;
    private java.util.Date dfechaoperacion;
    protected String stipo;
    private int icomprobante;
    protected String sconcepto;
    private double ddebe;
    private double dhaber;
    private double dsaldo;
    private short hestadoRegistro;
    private String smensaje;
    
    //public static final int LONGITUD_DESCRIPCION = 50;
    //public static final int LONGITUD_RUC = 12;
    //public static final int LONGITUD_CONTACTO = 50;
    //public static final int LONGITUD_WEB = 40;
    //public static final int LONGITUD_EMAIL = 40;
    
    public static final String TEXTO_CUENTA_BANCO = "Cuenta Banco (no editable)";
    public static final String TEXTO_FECHA = "Fecha de Operación";
    public static final String TEXTO_TIPO = "Tipo de operarión realizada sobre la cuenta";
    public static final String TEXTO_COMPROBANTE = "Número de Comprobante";
    public static final String TEXTO_CONCEPTO = "Descripción de Operación";
    public static final String TEXTO_DEBE = "Monto de la operación (débito)";
    public static final String TEXTO_HABER = "Monto de la operación (crédito)";
    public static final String TEXTO_SALDO = "Saldo de cuenta";
    
    public entbLibroBanco() {
        this.iidBancoCuenta = 0;
        this.sbancoCuenta = "";
        this.dfechaoperacion = null;
        this.stipo = "";
        this.icomprobante = 0;
        this.sconcepto = "";
        this.ddebe = 0.0;
        this.dhaber = 0.0;
        this.dsaldo = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbLibroBanco(int iidBancoCuenta, String sbancoCuenta, java.util.Date dfechaoperacion, String stipo, int icomprobante, String sconcepto, double ddebe, double dhaber, double dsaldo, short hestadoRegistro) {
        this.iidBancoCuenta = iidBancoCuenta;
        this.sbancoCuenta = sbancoCuenta;
        this.dfechaoperacion = dfechaoperacion;
        this.stipo = stipo;
        this.icomprobante = icomprobante;
        this.sconcepto = sconcepto;
        this.ddebe = ddebe;
        this.dhaber = dhaber;
        this.dsaldo = dsaldo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iidBancoCuenta, String sbancoCuenta, java.util.Date dfechaoperacion, String stipo, int icomprobante, String sconcepto, double ddebe, double dhaber, double dsaldo, short hestadoRegistro) {
        this.iidBancoCuenta = iidBancoCuenta;
        this.sbancoCuenta = sbancoCuenta;
        this.dfechaoperacion = dfechaoperacion;
        this.stipo = stipo;
        this.icomprobante = icomprobante;
        this.sconcepto = sconcepto;
        this.ddebe = ddebe;
        this.dhaber = dhaber;
        this.dsaldo = dsaldo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entbLibroBanco copiar(entbLibroBanco destino) {
        destino.setEntidad(this.getIdBancoCuenta(), this.getBancoCuenta(), this.getFechaOperacion(), this.getTipo(), this.getComprobante(), this.getConcepto(), this.getDebe(), this.getHaber(),this.getSaldo(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbLibroBanco cargar(java.sql.ResultSet rs) {
        try { this.setIdBancoCuenta(rs.getInt("idbancocuenta")); }
        catch(Exception e) {}
        try { this.setBancoCuenta(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setTipo(rs.getString("tipo")); }
        catch(Exception e) {}
        try { this.setComprobante(rs.getInt("comprobante")); }
        catch(Exception e) {}
        try { this.setConcepto(rs.getString("concepto")); }
        catch(Exception e) {}
        try { this.setDebe(rs.getDouble("debe")); }
        catch(Exception e) {}
        try { this.setHaber(rs.getDouble("haber")); }
        catch(Exception e) {}
        try { this.setSaldo(rs.getDouble("saldo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdBancoCuenta(int iidBancoCuenta) {
        this.iidBancoCuenta = iidBancoCuenta;
    }

    public void setBancoCuenta(String sbancoCuenta) {
        this.sbancoCuenta = sbancoCuenta;
    }
    
    public void setFechaOperacion(java.util.Date dfechaoperacion) {
        this.dfechaoperacion = dfechaoperacion;
    }
    
    public void setTipo(String stipo) {
        this.stipo = stipo;
    }
    
    public void setComprobante(int icomprobante) {
        this.icomprobante = icomprobante;
    }
    
    public void setConcepto(String sconcepto) {
        this.sconcepto = sconcepto;
    }
    
    public void setDebe(double ddebe) {
        this.ddebe = ddebe;
    }
    
    public void setHaber(double dhaber) {
        this.dhaber = dhaber;
    }
    
    public void setSaldo(double dsaldo) {
        this.dsaldo = dsaldo;
    } 
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public int getIdBancoCuenta() {
        return this.iidBancoCuenta;
    }

    public String getBancoCuenta() {
        return this.sbancoCuenta;
    }
    
    public java.util.Date getFechaOperacion() {
        return this.dfechaoperacion;
    }
    
    public String getTipo() {
        return this.stipo;
    }
    
    public int getComprobante() {
        return this.icomprobante;
    }
     
     public String getConcepto() {
        return this.sconcepto;
    }
    
    public double getDebe() {
        return this.ddebe;
    }
    
    public double getHaber() {
        return this.dhaber;
    }
    
    public double getSaldo() {
        return this.dsaldo;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        
        return bvalido;
    }

    /*public String getSentencia() {
        "SELECT banco.banco("+
            this.getId()+","+
            utilitario.utiGeneral.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiGeneral.getTextoGuardado(this.getRuc())+","+
            utilitario.utiGeneral.getTextoGuardado(this.getContacto())+","+
            utilitario.utiGeneral.getTextoGuardado(this.getWeb())+","+
            utilitario.utiGeneral.getTextoGuardado(this.getEmail())+","+
            this.getEstadoRegistro()+")";
    }*/

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cuenta Banco", "descripcion", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Concepto", "concepto", new generico.entLista().tipo_texto));
        return lst;
    }
    
}