/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleDeposito {

    private int iid;
    private int iidDeposito;
    private int iidConcepto;
    private String sconcepto;
    private int inumeroOperacion;
    private double umonto;
    private int iestadoRegistro;
    private String smensaje;

    public static final String TEXTO_DEPOSITO = "Depósito (no vacío)";
    public static final String TEXTO_CONCEPTO = "Concepto de depósito (no vacía)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (valor positivo)";
    public static final String TEXTO_MONTO = "Monto (valor positivo)";
    
    public entDetalleDeposito() {
        this.iid = 0;
        this.iidDeposito = 0;
        this.iidConcepto = 0;
        this.sconcepto = "";
        this.inumeroOperacion = 0;
        this.umonto = 0.0;
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleDeposito(int iid, int iidDeposito, int iidConcepto, String sconcepto, int inumeroOperacion, double umonto, int iestadoRegistro) {
        this.iid = iid;
        this.iidDeposito = iidDeposito;
        this.iidConcepto = iidConcepto;
        this.sconcepto = sconcepto;
        this.inumeroOperacion = inumeroOperacion;
        this.umonto = umonto;
        this.iestadoRegistro = iestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidDeposito, int iidConcepto, String sconcepto, int inumeroOperacion, double umonto, int iestadoRegistro) {
        this.iid = iid;
        this.iidDeposito = iidDeposito;
        this.iidConcepto = iidConcepto;
        this.sconcepto = sconcepto;
        this.inumeroOperacion = inumeroOperacion;
        this.umonto = umonto;
        this.iestadoRegistro = iestadoRegistro;
        this.smensaje = "";
    }

    public entDetalleDeposito copiar(entDetalleDeposito destino) {
        destino.setEntidad(this.getId(), this.getIdDeposito(), this.getIdConcepto(), this.getConcepto(), this.getNumeroOperacion(), this.getMonto(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleDeposito cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdDeposito(rs.getInt("iddeposito")); }
        catch(Exception e) {}
        try { this.setIdConcepto(rs.getInt("idconcepto")); }
        catch(Exception e) {}
        try { this.setConcepto(rs.getString("concepto")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdDeposito(int iidDeposito) {
        this.iidDeposito = iidDeposito;
    }

    public void setIdConcepto(int iidConcepto) {
        this.iidConcepto = iidConcepto;
    }
    
    public void setConcepto(String sconcepto) {
        this.sconcepto = sconcepto;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setMonto(double umonto) {
        this.umonto = umonto;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdDeposito() {
        return this.iidDeposito;
    }

    public int getIdConcepto() {
        return this.iidConcepto;
    }
    
    public String getConcepto() {
        return this.sconcepto;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public double getMonto() {
        return this.umonto;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        this.smensaje = "";
        if (this.getIdDeposito()<=0) this.smensaje += TEXTO_DEPOSITO + "\n";
        if (this.getIdConcepto()<=0) this.smensaje += TEXTO_CONCEPTO + "\n";
        if (this.getNumeroOperacion()<0) this.smensaje += TEXTO_NUMERO_OPERACION + "\n";
        if (this.getMonto()<0) this.smensaje += TEXTO_MONTO + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }

    
    public String getSentencia() {
        return "SELECT detalledeposito("+
            this.getId()+","+
            this.getIdDeposito()+","+
            this.getIdConcepto()+","+
            this.getNumeroOperacion()+","+
            this.getMonto()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
