/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMotivoAtencion extends generico.entGenerica{
    
    protected int iidDependencia;
    protected String sdependencia;

    public static final int LONGITUD_DESCRIPCION = 50;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Motivo de Atención (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia (no vacía)";

    public entMotivoAtencion() {
        super();
    }

    public entMotivoAtencion(int iid, String sdescripcion, int iidDependencia, String sdependencia, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
    }
    
    public void setEntidad(int iid, String sdescripcion, int iidDependencia, String sdependencia, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entMotivoAtencion copiar(entMotivoAtencion destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdDependencia(), this.getDependencia(), this.getEstadoRegistro());
        return destino;
    }

    public entMotivoAtencion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }
    
    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }
    
    public int getIdDependencia() {
        return this.iidDependencia;
    }
    
    public String getDependencia() {
        return this.sdependencia;
    }

    public boolean esValido() {
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) this.smensaje += TEXTO_DESCRIPCION + "\n";
        if (this.getIdDependencia()<0) this.smensaje += TEXTO_DEPENDENCIA + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public String getSentencia() {
        return "SELECT motivoatencion("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdDependencia()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Dependencia", "dependencia", generico.entLista.tipo_texto));
        return lst;
    }
    
}
