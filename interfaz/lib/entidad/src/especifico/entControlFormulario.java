/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entControlFormulario extends generico.entGenerica{
    
    private String sserie;
    private String sversion;
    private boolean bactivo;

    public static final int LONGITUD_DESCRIPCION = 80;
    public static final int LONGITUD_SERIE = 1;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Formulario (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_SERIE = "Serie de Formulario (hasta " + LONGITUD_SERIE + " caracteres)";
    public static final String TEXTO_VERSION = "Versión de Formulario";
    public static final String TEXTO_ACTIVO = "Activo";

    public entControlFormulario() {
        super();
        this.sserie = "";
        this.sversion = "";
        this.bactivo = false;
    }

    public entControlFormulario(int iid, String sdescripcion, String sserie, String sversion, boolean bactivo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.sserie = sserie;
        this.sversion = sversion;
        this.bactivo = bactivo;
    }
    
    public void setEntidad(int iid, String sdescripcion, String sserie, String sversion, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.sserie = sserie;
        this.sversion = sversion;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entControlFormulario copiar(entControlFormulario destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getSerie(), this.getVersion(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }

    public entControlFormulario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setSerie(rs.getString("serie")); }
        catch(Exception e) {}
        try { this.setVersion(rs.getString("version")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setSerie(String sserie) {
        this.sserie = sserie;
    }

    public void setVersion(String sversion) {
        this.sversion = sversion;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public String getSerie() {
        return this.sserie;
    }

    public String getVersion() {
        return this.sversion;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT formulario("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getSerie())+","+
            utilitario.utiCadena.getTextoGuardado(this.getVersion())+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Serie", "serie", generico.entLista.tipo_texto));
        return lst;
    }
}
