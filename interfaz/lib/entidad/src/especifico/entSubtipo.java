/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entSubtipo extends generico.entGenerica {

    protected int iidTipo;
    protected String stipo;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Subtipo (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_TIPO = "Tipo (no vacío)";

    public entSubtipo() {
        super();
        this.iidTipo = 0;
        this.stipo = "";
    }

    public entSubtipo(int iid, String sdescripcion, int iidTipo, String stipo) {
        super(iid, sdescripcion);
        this.iidTipo = iidTipo;
        this.stipo = stipo;
    }

    public entSubtipo(int iid, String sdescripcion, int iidTipo, String stipo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidTipo = iidTipo;
        this.stipo = stipo;
    }

    public void setEntidad(int iid, String sdescripcion, int iidTipo, String stipo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidTipo = iidTipo;
        this.stipo = stipo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entSubtipo copiar(entSubtipo destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdTipo(), this.getTipo(), this.getEstadoRegistro());
        return destino;
    }

    public entSubtipo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdTipo(rs.getInt("idtipo")); }
        catch(Exception e) {}
        try { this.setTipo(rs.getString("tipo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdTipo(int iidTipo) {
        this.iidTipo = iidTipo;
    }

    public void setTipo(String stipo) {
        this.stipo = stipo;
    }

    public int getIdTipo() {
        return this.iidTipo;
    }

    public String getTipo() {
        return this.stipo;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdTipo()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT subtipo("+
            this.getId()+","+
            this.getIdTipo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Tipo", "tipo", generico.entLista.tipo_texto));
        return lst;
    }
    
}
