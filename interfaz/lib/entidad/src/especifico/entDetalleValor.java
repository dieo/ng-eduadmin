/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleValor {

    private int iid;
    private int iidIngreso;
    private int iidTipoValor;
    private String stipoValor;
    private String snumeroDocumento;
    private String sdenominacion;
    private double umonto;
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_NUMERO_DOCUMENTO = 12;
    public static final int LONGITUD_DENOMINACION = 50;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_TIPO_VALOR = "Tipo de valor (no vacío)";
    public static final String TEXTO_NUMERO_DOCUMENTO = "Artículo (hasta " + LONGITUD_NUMERO_DOCUMENTO + " caracteres)";
    public static final String TEXTO_DENOMINACION = "Denominación (hasta " + LONGITUD_DENOMINACION + " caracteres)";
    public static final String TEXTO_MONTO = "Monto (valor positivo)";

    public entDetalleValor() {
        this.iid = 0;
        this.iidIngreso = 0;
        this.iidTipoValor = 0;
        this.stipoValor = "";
        this.snumeroDocumento = "";
        this.sdenominacion = "";
        this.umonto = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleValor(int iid, int iidIngreso, int iidTipoValor, String stipoValor, String snumeroDocumento, String sdenominacion, double umonto, short hestadoRegistro) {
        this.iid = iid;
        this.iidIngreso = iidIngreso;
        this.iidTipoValor = iidTipoValor;
        this.stipoValor = stipoValor;
        this.snumeroDocumento = snumeroDocumento;
        this.sdenominacion = sdenominacion;
        this.umonto = umonto;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidIngreso, int iidTipoValor, String stipoValor, String snumeroDocumento, String sdenominacion, double umonto, short hestadoRegistro) {
        this.iid = iid;
        this.iidIngreso = iidIngreso;
        this.iidTipoValor = iidTipoValor;
        this.stipoValor = stipoValor;
        this.snumeroDocumento = snumeroDocumento;
        this.sdenominacion = sdenominacion;
        this.umonto = umonto;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public entDetalleValor copiar(entDetalleValor destino) {
        destino.setEntidad(this.getId(), this.getIdIngreso(), this.getIdTipoValor(), this.getTipoValor(), this.getNumeroDocumento(), this.getDenominacion(), this.getMonto(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleValor cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdIngreso(rs.getInt("idingreso")); }
        catch(Exception e) {}
        try { this.setIdTipoValor(rs.getInt("idtipovalor")); }
        catch(Exception e) {}
        try { this.setTipoValor(rs.getString("tipovalor")); }
        catch(Exception e) {}
        try { this.setNumeroDocumento(rs.getString("numerodocumento")); }
        catch(Exception e) {}
        try { this.setDenominacion(rs.getString("denominacion")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdIngreso(int iidIngreso) {
        this.iidIngreso = iidIngreso;
    }

    public void setIdTipoValor(int iidTipoValor) {
        this.iidTipoValor = iidTipoValor;
    }

    public void setTipoValor(String stipoValor) {
        this.stipoValor = stipoValor;
    }

    public void setNumeroDocumento(String snumeroDocumento) {
        this.snumeroDocumento = snumeroDocumento;
    }

    public void setDenominacion(String sdenominacion) {
        this.sdenominacion = sdenominacion;
    }

    public void setMonto(double umonto) {
        this.umonto = umonto;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdIngreso() {
        return this.iidIngreso;
    }

    public int getIdTipoValor() {
        return this.iidTipoValor;
    }

    public String getTipoValor() {
        return this.stipoValor;
    }

    public String getNumeroDocumento() {
        return this.snumeroDocumento;
    }

    public String getDenominacion() {
        return this.sdenominacion;
    }

    public double getMonto() {
        return this.umonto;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdTipoValor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_VALOR;
            bvalido = false;
        }
        if (this.getMonto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detallevalor("+
            this.getId()+","+
            this.getIdIngreso()+","+
            this.getIdTipoValor()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumeroDocumento())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDenominacion())+","+
            this.getMonto()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Tipo Valor", "tipovalor", generico.entLista.tipo_texto));
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Tipo Valor", "tipovalor", generico.entLista.tipo_texto));
        return lst;
    }

}
