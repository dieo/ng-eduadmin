/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entdRequisitoOperacion {
    
    protected int iid;
    protected int iidTipoOperacion;
    protected String stipoOperacion;
    protected int iidRequisito;
    protected String srequisito;
    protected boolean bactivo;
    protected String sdescripcion;
    protected boolean bimputable;
    protected short cestado;
    protected String smensaje;
    
    //public static final int LONGITUD_DESCRIPCION = 100;
        
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_TIPO_OPERACION = "Tipo de Operación (no vacía)";
    public static final String TEXTO_REQUISITO = "Requisito a agregar (no vacía)";
    //public static final String TEXTO_IMPUTABLE = "Sí o No";
    public static final String TEXTO_ACTIVO = "Sí o No";
    //public static final String TEXTO_NIVEL = "Nivel de cuenta (no vacía)";
    //public static final String TEXTO_PERIODO = "Año al que corresponde el plan de cuenta";
    
    public entdRequisitoOperacion() {
        this.iid = 0;
        this.iidTipoOperacion = 0;
        this.stipoOperacion = "";
        this.iidRequisito = 0;
        this.srequisito = "";
        this.bactivo = false;
        this.sdescripcion = "";
        this.bimputable = false;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entdRequisitoOperacion(int iid, String sdescripcion, String stipoOperacion, String srequisito, boolean bimputable, boolean bactivo, int iidTipoOperacion, int iidRequisito, short cestado) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidTipoOperacion = iidTipoOperacion;
        this.stipoOperacion = stipoOperacion;
        this.iidRequisito = iidRequisito;
        this.srequisito = srequisito;
        this.bactivo = bactivo;
        this.bimputable = bimputable;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, String sdescripcion, String stipoOperacion, String srequisito, boolean bimputable, boolean bactivo, int iidTipoOperacion, int iidRequisito, short cestado) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidTipoOperacion = iidTipoOperacion;
        this.stipoOperacion = stipoOperacion;
        this.iidRequisito = iidRequisito;
        this.srequisito = srequisito;
        this.bactivo = bactivo;
        this.bimputable = bimputable;
        this.cestado = cestado;
    }

    public entdRequisitoOperacion copiar(entdRequisitoOperacion destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getTipoOperacion(), this.getRequisito(), this.getImputable(), this.getActivo(), this.getIdTipoOperacion(), this.getIdRequisito(), this.getEstadoRegistro());
        return destino;
    }
    
    public entdRequisitoOperacion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("idrequisitooperacion")); }
        catch(Exception e) { }
        //try { this.setDescripcion(rs.getString("descripcion")); }
        //catch(Exception e) { }
        try { this.setIdTipoOperacion(rs.getInt("idtipooperacion")); }
        catch(Exception e) { }
        try { this.setTipoOperacion(rs.getString("tipooperacion")); }
        catch(Exception e) { }
        try { this.setIdRequisito(rs.getInt("idrequisito")); }
        catch(Exception e) { }
        try { this.setRequisito(rs.getString("requisito")); }
        catch(Exception e) { }
        //try { this.setImputable(rs.getBoolean("imputable")); }
        //catch(Exception e) { }
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setIdTipoOperacion(int iidTipoOperacion) {
        this.iidTipoOperacion = iidTipoOperacion;
    }

    public void setTipoOperacion(String stipoOperacion) {
        this.stipoOperacion = stipoOperacion;
    }
    
    public void setIdRequisito(int iidRequisito) {
        this.iidRequisito = iidRequisito;
    }

    public void setRequisito(String srequisito) {
        this.srequisito = srequisito;
    }
    
    public void setImputable(boolean bimputable) {
        this.bimputable = bimputable;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public int getIdTipoOperacion() {
        return this.iidTipoOperacion;
    }
    
    public String getTipoOperacion() {
        return this.stipoOperacion;
    }
    
    public int getIdRequisito() {
        return this.iidRequisito;
    }
    
    public String getRequisito() {
        return this.srequisito;
    }
    
    public boolean getImputable() {
        return this.bimputable;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }
    
    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        /*if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }*/
        if (this.getIdTipoOperacion()==0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_TIPO_OPERACION;
            bvalido = false;
        }
        if (this.getIdRequisito()==0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_REQUISITO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT control.requisitooperacion("+
            this.getId()+","+
            this.getIdTipoOperacion()+","+
            this.getIdRequisito()+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Tipo Operación", "tipooperacion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Requisito", "requisito", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Activo", "activo2", new generico.entLista().tipo_texto));
        //lst.add(new generico.entLista(2, "Descripción", "descripcion", generico.entLista.tipo_texto));
        //lst.add(new generico.entLista(4, "Imputable", "imputable2", new generico.entLista().tipo_texto));
        return lst;
    }
}
