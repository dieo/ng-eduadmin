/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMovimientoPerfil {

    private int iid;
    private int iidUsuario;
    private int iidPerfil;
    private String sperfil;
    private short hestadoRegistro;
    private String smensaje;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_PERFIL = "Perfil (no vacío)";

    public entMovimientoPerfil() {
        this.iid = 0;
        this.iidUsuario = 0;
        this.iidPerfil = 0;
        this.sperfil = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entMovimientoPerfil(int iid, int iidUsuario, int iidPerfil, String sperfil, short hestadoRegistro) {
        this.iid = iid;
        this.iidUsuario = iidUsuario;
        this.iidPerfil = iidPerfil;
        this.sperfil = sperfil;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidUsuario, int iidPerfil, String sperfil, short hestadoRegistro) {
        this.iid = iid;
        this.iidUsuario = iidUsuario;
        this.iidPerfil = iidPerfil;
        this.sperfil = sperfil;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public entMovimientoPerfil copiar(entMovimientoPerfil destino) {
        destino.setEntidad(this.getId(), this.getIdUsuario(), this.getIdPerfil(), this.getPerfil(), this.getEstadoRegistro());
        return destino;
    }

    public entMovimientoPerfil cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setIdPerfil(rs.getInt("idperfil")); }
        catch(Exception e) {}
        try { this.setPerfil(rs.getString("perfil")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }

    public void setIdPerfil(int iidPerfil) {
        this.iidPerfil = iidPerfil;
    }

    public void setPerfil(String sperfil) {
        this.sperfil = sperfil;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }

    public int getIdPerfil() {
        return this.iidPerfil;
    }

    public String getPerfil() {
        return this.sperfil;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdPerfil()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PERFIL;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT movimientoperfil("+
            this.getId()+","+
            this.getIdUsuario()+","+
            this.getIdPerfil()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Perfil", "perfil", generico.entLista.tipo_texto));
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Perfil", "perfil", generico.entLista.tipo_texto));
        return lst;
    }

}
