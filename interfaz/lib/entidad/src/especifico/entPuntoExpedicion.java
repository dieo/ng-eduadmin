/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPuntoExpedicion extends generico.entGenerica{
    
    protected String snumeroExpedicion;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Punto de Expedición (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_NUMERO_EXPEDICION = "Número de Punto de Expedición (no vacío, valor numérico)";

    public entPuntoExpedicion() {
        super();
        this.snumeroExpedicion = "";
    }

    public entPuntoExpedicion(int iid, String sdescripcion, String snumeroExpedicion, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.snumeroExpedicion = snumeroExpedicion;
    }
    
    public void setEntidad(int iid, String sdescripcion, String snumeroExpedicion, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.snumeroExpedicion = snumeroExpedicion;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entPuntoExpedicion copiar(entPuntoExpedicion destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getNumeroExpedicion(), this.getEstadoRegistro());
        return destino;
    }

    public entPuntoExpedicion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setNumeroExpedicion(rs.getString("numeroexpedicion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setNumeroExpedicion(String snumeroExpedicion) {
        this.snumeroExpedicion = snumeroExpedicion;
    }

    public String getNumeroExpedicion() {
        return this.snumeroExpedicion;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getNumeroExpedicion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_EXPEDICION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT puntoexpedicion("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumeroExpedicion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
}
