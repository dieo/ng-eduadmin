/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entUnidadMedida extends generico.entGenerica{
    
    private String sdescripcionBreve;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final int LONGITUD_DESCRIPCION_BREVE = 5;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Unidad de Medida (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_DESCRIPCION_BREVE = "Descripción breve de Unidad de Medida (no vacía, hasta " + LONGITUD_DESCRIPCION_BREVE + " caracteres)";

    public entUnidadMedida() {
        super();
        this.sdescripcionBreve = "";
    }

    public entUnidadMedida(int iid, String sdescripcion, String sdescripcionBreve, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.sdescripcionBreve = sdescripcionBreve;
    }
    
    public void setEntidad(int iid, String sdescripcion, String sdescripcionBreve, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.sdescripcionBreve = sdescripcionBreve;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entUnidadMedida copiar(entUnidadMedida destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getDescripcionBreve(), this.getEstadoRegistro());
        return destino;
    }

    public entUnidadMedida cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setDescripcionBreve(rs.getString("descripcionbreve")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setDescripcionBreve(String sdescripcionBreve) {
        this.sdescripcionBreve = sdescripcionBreve;
    }

    public String getDescripcionBreve() {
        return this.sdescripcionBreve;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getDescripcionBreve().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION_BREVE;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT unidadmedida("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcionBreve())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Descripción breve", "descripcionbreve", generico.entLista.tipo_texto));
        return lst;
    }

}
