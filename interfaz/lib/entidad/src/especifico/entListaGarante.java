/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entGenericaPersona;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entListaGarante extends entGenericaPersona {
    
    protected boolean bacepta;
    
    public static final int LONGITUD_DESCRIPCION = 50;

    public static final String TEXTO_DESCRIPCION = "Texto a buscar";

    public static final String TEXTO_CEDULA = "Cédula";
    public static final String TEXTO_NOMBRE = "Nombre";
    public static final String TEXTO_APELLIDO = "Apellido";
    public static final String TEXTO_ACEPTA = "Acepta";

    public entListaGarante() {
        super();
        this.bacepta = false;
    }

    public entListaGarante(int iid, String snombre, String sapellido, String scedula, boolean bacepta) {
        super(iid, snombre, sapellido, scedula, (short)0);
        this.bacepta = bacepta;
    }

    public void setEntidad(int iid, String snombre, String sapellido, String scedula, boolean bacepta) {
        this.iid = iid;
        this.snombre = snombre.trim().toUpperCase();
        this.sapellido = sapellido.trim().toUpperCase();
        this.scedula = scedula;
        this.bacepta = bacepta;
        this.hestadoRegistro = (short)0;
    }

    public entListaGarante copiar(entListaGarante destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getAcepta());
        return destino;
    }
    
    public entListaGarante cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) { }
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) { }
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) { }
        try { this.setAcepta(false); }
        catch(Exception e) { }
        return this;
    }
    
    public void setAcepta(boolean bacepta) {
        this.bacepta = bacepta;
    }
    
    public boolean getAcepta() {
        return this.bacepta;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Apellido", "apellido", generico.entLista.tipo_texto));
        return lst;
    }
    
}
