/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbMovimientoCuenta {

    private int iid;
    private int inumeroMovimiento;
    private int iidBancoCuenta;
    private String snumerocuenta;
    private String sbancocuenta;
    private String snroPlanBancoCuenta;
    private java.util.Date dfecha;
    private java.util.Date dfechaingreso;
    private int iidEstado;
    private String sestado;
    private int inumeroComprobante;
    private int iidTipoMovimiento;
    private String stipomovimiento;
    private double dmonto;
    private double dsaldo;
    private int iidCentroCosto;
    private String scentroCosto;
    private String snota;
    private String sserie;
    private boolean basiento;
    private int iidFilial;
    private String sfilial;
    private short hestadoRegistro;
    private String smensaje;
    
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    public static final int LONGITUD_NOTA = 100;
    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMERO_MOVIMIENTO = "Número de movimiento (no editable)";
    public static final String TEXTO_BANCO_CUENTA = "Cuenta Bancaria sobre la que se opera (no vacía)";
    public static final String TEXTO_FECHA = "Fecha de Movimiento (no vacía)";
    public static final String TEXTO_FECHA_INGRESO = "Fecha de Carga del movimiento (no editable)";
    public static final String TEXTO_ESTADO = "Estado de Movimiento (no editable)";
    public static final String TEXTO_NUMERO_COMPROBANTE = "Número de Comprobante";
    public static final String TEXTO_TIPOMOVIMIENTO = "Tipo de movimiento a realizar sobre la cuenta (no vacío)";
    public static final String TEXTO_MONTO = "Monto de la operación (valor positivo)";
    public static final String TEXTO_SALDO = "Disponible en la cuenta";
    public static final String TEXTO_CENTRO_COSTO = "Centro de Costo (no vacío)";
    public static final String TEXTO_ASIENTO = "Si fue asentado o no el movimiento";
    public static final String TEXTO_NOTA = "Descripción de Movimiento";
    public static final String TEXTO_SERIE = "Serie en caso de cheques";
    public static final String TEXTO_FILIAL = "Filial (no vacía)";
    
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";          
    
    public entbMovimientoCuenta() {
        this.iid = 0;
        this.inumeroMovimiento = 0;
        this.iidBancoCuenta = 0;
        this.snumerocuenta = "";
        this.sbancocuenta = "";
        this.snroPlanBancoCuenta = "";
        this.dfecha = null;
        this.dfechaingreso = null;
        this.iidEstado = 0;
        this.sestado = "";
        this.inumeroComprobante = 0;
        this.iidTipoMovimiento = 0;
        this.stipomovimiento = "";
        this.dmonto = 0.0;
        this.dsaldo = 0.0;
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.snota = "";
        this.sserie = "";
        this.basiento = false;
        this.iidFilial = 0;
        this.sfilial = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbMovimientoCuenta(int iid, int inumeroMovimiento, int iidBancoCuenta, String snumerocuenta, String sbancocuenta, String snroPlanBancoCuenta, java.util.Date dfecha, java.util.Date dfechaingreso, int iidEstado, String sestado, int inumeroComprobante, int iidTipoMovimiento, String stipomovimiento, double dmonto, double dsaldo, int iidCentroCosto,  String scentroCosto, java.util.Date dfechaAnulado, String sobservacionAnulado, String snota, String sserie, boolean basiento, int iidFilial, String sfilial, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroMovimiento = inumeroMovimiento;
        this.iidBancoCuenta = iidBancoCuenta;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.snroPlanBancoCuenta = snroPlanBancoCuenta;
        this.dfecha = dfecha;
        this.dfechaingreso = dfechaingreso;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.inumeroComprobante = inumeroComprobante;
        this.iidTipoMovimiento = iidTipoMovimiento;
        this.stipomovimiento = stipomovimiento;
        this.dmonto = dmonto;
        this.dsaldo = dsaldo;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.snota = snota;
        this.sserie = sserie;
        this.basiento = basiento;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroMovimiento, int iidBancoCuenta, String snumerocuenta, String sbancocuenta, String snroPlanBancoCuenta, java.util.Date dfecha, java.util.Date dfechaingreso, int iidEstado, String sestado, int inumeroComprobante, int iidTipoMovimiento, String stipomovimiento, double dmonto, double dsaldo, int iidCentroCosto,  String scentroCosto, java.util.Date dfechaAnulado, String sobservacionAnulado, String snota, String sserie, boolean basiento, int iidFilial, String sfilial, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroMovimiento = inumeroMovimiento;
        this.iidBancoCuenta = iidBancoCuenta;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.snroPlanBancoCuenta = snroPlanBancoCuenta;
        this.dfecha = dfecha;
        this.dfechaingreso = dfechaingreso;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.inumeroComprobante = inumeroComprobante;
        this.iidTipoMovimiento = iidTipoMovimiento;
        this.stipomovimiento = stipomovimiento;
        this.dmonto = dmonto;
        this.dsaldo = dsaldo;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.snota = snota;
        this.sserie = sserie;
        this.basiento = basiento;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entbMovimientoCuenta copiar(entbMovimientoCuenta destino) {
        destino.setEntidad(this.getId(), this.getNumeroMovimiento(), this.getIdBancoCuenta(), this.getNumeroCuenta(), this.getBancoCuenta(), this.getNroPlanBancoCuenta(), this.getFecha(), this.getFechaIngreso(),  this.getIdEstado(), this.getEstado(), this.getComprobante(), this.getIdTipoMovimiento(), this.getTipoMovimiento(), this.getMonto(), this.getSaldo(), this.getIdCentroCosto(), this.getCentroCosto(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getNota(), this.getSerie(), this.getAsiento(), this.getIdFilial(), this.getFilial(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbMovimientoCuenta cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroMovimiento(rs.getInt("nromovimiento")); }
        catch(Exception e) {}
        try { this.setIdBancoCuenta(rs.getInt("idbancocuenta")); }
        catch(Exception e) {}
        try { this.setNumeroCuenta(rs.getString("numerocuenta")); }
        catch(Exception e) {}
        try { this.setBancoCuenta(rs.getString("cuentabanco")); }
        catch(Exception e) {}
        try { this.setNroPlanBancoCuenta(rs.getString("nroplancuentabanco")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaIngreso(rs.getDate("fechaingreso")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setComprobante(rs.getInt("comprobante")); }
        catch(Exception e) {}
        try { this.setIdTipoMovimiento(rs.getInt("idtipomovimiento")); }
        catch(Exception e) {}
        try { this.setTipoMovimiento(rs.getString("movimiento")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        try { this.setIdCentroCosto(rs.getInt("idcentrocosto")); }
        catch(Exception e) {}
        try { this.setCentroCosto(rs.getString("centrocosto")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setNota(rs.getString("nota")); }
        catch(Exception e) {}
        try { this.setSerie(rs.getString("serie")); }
        catch(Exception e) {}
        try { this.setAsiento(rs.getBoolean("asiento")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroMovimiento(int inumeroMovimiento) {
        this.inumeroMovimiento = inumeroMovimiento;
    }

    public void setIdBancoCuenta(int iidBancoCuenta) {
        this.iidBancoCuenta = iidBancoCuenta;
    }

    public void setNumeroCuenta(String snumerocuenta) {
        this.snumerocuenta = snumerocuenta;
    }

    public void setBancoCuenta(String sbancocuenta) {
        this.sbancocuenta = sbancocuenta;
    }

    public void setNroPlanBancoCuenta(String snroPlanBancoCuenta) {
        this.snroPlanBancoCuenta = snroPlanBancoCuenta;
    }
    
    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }
    
    public void setFechaIngreso(java.util.Date dfechaingreso) {
        this.dfechaingreso = dfechaingreso;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setComprobante(int inumeroComprobante) {
        this.inumeroComprobante = inumeroComprobante;
    }

    public void setIdTipoMovimiento(int iidTipoMovimiento) {
        this.iidTipoMovimiento = iidTipoMovimiento;
    }

    public void setTipoMovimiento(String stipomovimiento) {
        this.stipomovimiento = stipomovimiento;
    }

    public void setMonto(double dmonto) {
        this.dmonto = dmonto;
    }
    
    public void setSaldo(double dsaldo) {
        this.dsaldo = dsaldo;
    }        
    
    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }

    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }
   
    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setNota(String snota) {
        this.snota = snota;
    }

    public void setSerie(String sserie) {
        this.sserie = sserie;
    }

    public void setAsiento(boolean basiento) {
        this.basiento = basiento;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }

    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getNumeroMovimiento() {
        return this.inumeroMovimiento;
    }

    public int getIdBancoCuenta() {
        return this.iidBancoCuenta;
    }

    public String getNumeroCuenta() {
        return this.snumerocuenta;
    }

    public String getBancoCuenta() {
        return this.sbancocuenta;
    }
    
    public String getNroPlanBancoCuenta() {
        return this.snroPlanBancoCuenta;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public java.util.Date getFechaIngreso() {
        return this.dfechaingreso;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }
 
    public int getComprobante() {
        return this.inumeroComprobante;
    }

    public int getIdTipoMovimiento() {
        return this.iidTipoMovimiento;
    }

    public String getTipoMovimiento() {
        return this.stipomovimiento;
    }

    public double getMonto() {
        return this.dmonto;
    }
    
    public double getSaldo() {
        return this.dsaldo;
    }
    
    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }

    public String getCentroCosto() {
        return this.scentroCosto;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public String getNota() {
        return this.snota;
    }

    public String getSerie() {
        return this.sserie;
    }

    public boolean getAsiento() {
        return this.basiento;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }

    public String getFilial() {
        return this.sfilial;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoSolicitar(boolean esCheque, java.util.Date dfechacheque) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdCentroCosto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CENTRO_COSTO;
            bvalido = false;
        }
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA;
            bvalido = false;
        }
        if(esCheque) {
            if (utilitario.utiFecha.obtenerDiferenciaDia(dfechacheque, this.getFecha())<0) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "La fecha de movimiento es menor a la fecha de emisión de cheque. Verifique";
                bvalido = false;
            }
        }
        if (this.getIdBancoCuenta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_BANCO_CUENTA;
            bvalido = false;
        }
        if (this.getMonto()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO;
            bvalido = false;
        }        
        if (this.getComprobante()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_NUMERO_COMPROBANTE;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.movimientocuenta("+
            this.getId()+","+
            this.getIdBancoCuenta()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFecha())+","+
            this.getComprobante()+","+
            this.getIdTipoMovimiento()+","+
            this.getMonto()+","+
            this.getIdCentroCosto()+","+
            this.getIdEstado()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            this.getNumeroMovimiento()+","+
            this.getAsiento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNota())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaIngreso())+","+
            utilitario.utiCadena.getTextoGuardado(this.getSerie())+","+
            this.getIdFilial()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha", "fecha", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(2, "Fecha Ingreso", "fechaingreso", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Número Movimiento", "nromovimiento", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "Número Comprobante", "comprobante", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(5, "Monto", "monto", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(6, "Número de cuenta", "numerocuenta", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Descripción de cuenta", "cuentabanco", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(8, "Tipo Movimiento", "movimiento", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(9, "Nota", "nota", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(10, "Estado", "estado", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(11, "Centro de Costo", "centrocosto", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(12, "Filial", "filial", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
