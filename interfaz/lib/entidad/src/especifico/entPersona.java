/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPersona {
    
    private int iid;
    private String srazonSocial;
    private String sruc;
    private String scedula;
    private String snombre;
    private String sapellido;
    private String sdireccion;
    private String stelefonoLineaBaja;
    private String stelefonoCelular;
    private int iidPersoneria;
    private String spersoneria;
    private int iidCiudad;
    private String sciudad;
    private String sweb;
    private String semail;
    private boolean bactivo;
    private int iidSocio;
    private int iidPlanCuenta;
    private String snroPlanCuenta;
    private String splanCuenta;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_RAZON_SOCIAL = 80;
    public static final int LONGITUD_CEDULA = 12;
    public static final int LONGITUD_RUC = 12;
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final int LONGITUD_DIRECCION = 100;
    public static final int LONGITUD_TELEFONO = 40;
    public static final int LONGITUD_WEB = 30;
    public static final int LONGITUD_EMAIL = 50;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_RAZON_SOCIAL = "Razón Social (no vacía, hasta " + LONGITUD_RAZON_SOCIAL + " caracteres)";
    public static final String TEXTO_RUC = "RUC de Entidad (no vacía, hasta " + LONGITUD_RUC + " caracteres)";
    public static final String TEXTO_CEDULA = "Cédula (no vacía para Personería Física, vacía para Personería Jurídica, única, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío para Personería Física, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacío para Personería Física, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_DIRECCION = "Dirección (hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_TELEFONO_LINEA_BAJA = "Teléfono Línea Baja (hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_TELEFONO_CELULAR = "Teléfono Celular (hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_PERSONERIA = "Personería (no vacío)";
    public static final String TEXTO_CIUDAD = "Ciudad (no vacía)";
    public static final String TEXTO_WEB = "Página web de Entidad (hasta " + LONGITUD_WEB + " caracteres)";
    public static final String TEXTO_EMAIL = "E-mail de Entidad (hasta " + LONGITUD_EMAIL + " caracteres)";
    public static final String TEXTO_ACTIVO = "Activo";
    
    public entPersona() {
        this.iid = 0;
        this.srazonSocial = "";
        this.sruc = "";
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.sdireccion = "";
        this.stelefonoLineaBaja = "";
        this.stelefonoCelular = "";
        this.iidCiudad = 0;
        this.sciudad = "";
        this.iidPersoneria = 0;
        this.spersoneria = "";
        this.sweb = "";
        this.semail = "";
        this.bactivo = false;
        this.iidSocio = 0;
        this.iidPlanCuenta = 0;
        this.snroPlanCuenta = "";
        this.splanCuenta = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entPersona(int iid, String srazonSocial, String sruc, String scedula, String snombre, String sapellido, String sdireccion, String stelefonoLineaBaja, String stelefonoCelular, int iidCiudad, String sciudad, int iidPersoneria, String spersoneria, String sweb, String semail, boolean bactivo, int iidSocio, int iidPlanCuenta, String snroPlanCuenta, String splanCuenta, short hestadoRegistro) {
        this.iid = iid;
        this.srazonSocial = srazonSocial.trim();
        this.sruc = sruc;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.sdireccion = sdireccion;
        this.stelefonoLineaBaja = stelefonoLineaBaja;
        this.stelefonoCelular = stelefonoCelular;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.iidPersoneria = iidPersoneria;
        this.spersoneria = spersoneria;
        this.sweb = sweb;
        this.semail = semail;
        this.bactivo = bactivo;
        this.iidSocio = iidSocio;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.splanCuenta = splanCuenta;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
     public void setEntidad(int iid, String srazonSocial, String sruc, String scedula, String snombre, String sapellido, String sdireccion, String stelefonoLineaBaja, String stelefonoCelular, int iidCiudad, String sciudad, int iidPersoneria, String spersoneria, String sweb, String semail, boolean bactivo, int iidSocio, int iidPlanCuenta, String snroPlanCuenta, String splanCuenta, short hestadoRegistro) {
        this.iid = iid;
        this.srazonSocial = srazonSocial.trim();
        this.sruc = sruc;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.sdireccion = sdireccion;
        this.stelefonoLineaBaja = stelefonoLineaBaja;
        this.stelefonoCelular = stelefonoCelular;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.iidPersoneria = iidPersoneria;
        this.spersoneria = spersoneria;
        this.sweb = sweb;
        this.semail = semail;
        this.bactivo = bactivo;
        this.iidSocio = iidSocio;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.splanCuenta = splanCuenta;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entPersona copiar(entPersona destino) {
        destino.setEntidad(this.getId(), this.getRazonSocial(), this.getRuc(), this.getCedula(), this.getNombre(), this.getApellido(), this.getDireccion(), this.getTelefonoLineaBaja(), this.getTelefonoCelular(), this.getIdCiudad(), this.getCiudad(), this.getIdPersoneria(), this.getPersoneria(), this.getWeb(), this.getEmail(), this.getActivo(), this.getIdSocio(), this.getIdPlanCuenta(), this.getNroPlanCuenta(), this.getPlanCuenta(), this.getEstadoRegistro());
        return destino;
    }
    
    public entPersona cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setRazonSocial(rs.getString("razonsocial")); }
        catch(Exception e) {}
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefonoLineaBaja(rs.getString("telefonolineabaja")); }
        catch(Exception e) {}
        try { this.setTelefonoCelular(rs.getString("telefonocelular")); }
        catch(Exception e) {}
        try { this.setIdCiudad(rs.getInt("idciudad")); }
        catch(Exception e) {}
        try { this.setCiudad(rs.getString("ciudad")); }
        catch(Exception e) {}
        try { this.setIdPersoneria(rs.getInt("idpersoneria")); }
        catch(Exception e) {}
        try { this.setPersoneria(rs.getString("personeria")); }
        catch(Exception e) {}
        try { this.setWeb(rs.getString("web")); }
        catch(Exception e) {}
        try { this.setEmail(rs.getString("email")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) { }
        try { this.setNroPlanCuenta(rs.getString("nroplancuenta")); }
        catch(Exception e) { }
        try { this.setPlanCuenta(rs.getString("plancuenta")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setRazonSocial(String srazonSocial) {
        this.srazonSocial = srazonSocial.trim();
    }

    public void setRuc(String sruc) {
        this.sruc = sruc;
    }
    
    public void setCedula(String scedula) {
        this.scedula = scedula;
    }
    
    public void setNombre(String snombre) {
        this.snombre = snombre;
    }
    
    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefonoLineaBaja(String stelefonoLineaBaja) {
        this.stelefonoLineaBaja = stelefonoLineaBaja;
    }

    public void setTelefonoCelular(String stelefonoCelular) {
        this.stelefonoCelular = stelefonoCelular;
    }

    public void setIdCiudad(int iidCiudad) {
        this.iidCiudad = iidCiudad;
    }
    
    public void setCiudad(String sciudad) {
        this.sciudad = sciudad;
    }
    
    public void setIdPersoneria(int iidPersoneria) {
        this.iidPersoneria = iidPersoneria;
    }

    public void setPersoneria(String spersoneria) {
        this.spersoneria = spersoneria;
    }
    
    public void setWeb(String sweb) {
        this.sweb = sweb;
    }
    
    public void setEmail(String semail) {
        this.semail = semail;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }
    
    public void setNroPlanCuenta(String snroPlanCuenta) {
        this.snroPlanCuenta = snroPlanCuenta;
    }
    
    public void setPlanCuenta(String splanCuenta) {
        this.splanCuenta = splanCuenta;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public String getRazonSocial() {
        return this.srazonSocial;
    }

    public String getRuc() {
        return this.sruc;
    }
    
    public String getCedula() {
        return this.scedula;
    }
    
    public String getNombre() {
        return this.snombre;
    }
    
    public String getApellido() {
        return this.sapellido;
    }
    
    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefonoLineaBaja() {
        return this.stelefonoLineaBaja;
    }
    
    public String getTelefonoCelular() {
        return this.stelefonoCelular;
    }
    
    public int getIdCiudad() {
        return this.iidCiudad;
    }
    
    public String getCiudad() {
        return this.sciudad;
    }
    
    public int getIdPersoneria() {
        return this.iidPersoneria;
    }

    public String getPersoneria() {
        return this.spersoneria;
    }
    
    public String getWeb() {
        return this.sweb;
    }
    
    public String getEmail() {
        return this.semail;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }
    
    public int getIdSocio() {
        return this.iidSocio;
    }
    
    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }
    
    public String getNroPlanCuenta() {
        return this.snroPlanCuenta;
    }
    
    public String getPlanCuenta() {
        return this.splanCuenta;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido(boolean esFisica, boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (esFisica) { // verifica si es persona física
            if (this.getCedula().isEmpty()) { // verifica si está vacía para personas físicas
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += TEXTO_CEDULA;
                bvalido = false;
            }
            if (this.getNombre().isEmpty()) { // verifica si está vacía para personas físicas
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += TEXTO_NOMBRE;
                bvalido = false;
            }
            if (this.getApellido().isEmpty()) { // verifica si está vacía para personas físicas
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += TEXTO_APELLIDO;
                bvalido = false;
            }
        } else { // verifica si es persona jurídica
            if (this.getRuc().isEmpty()) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += TEXTO_RUC;
                bvalido = false;
            }
            if (this.getRazonSocial().isEmpty()) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += TEXTO_RAZON_SOCIAL;
                bvalido = false;
            }
        }
        if (esDuplicado) { // verifica si está duplicado la cédula
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Cédula duplicada.";
            bvalido = false;
        }
        if (this.getIdCiudad() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CIUDAD;
            bvalido = false;
        }
        if (this.getIdPersoneria()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PERSONERIA;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT persona("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getRazonSocial())+","+
            utilitario.utiCadena.getTextoGuardado(this.getRuc())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoLineaBaja())+","+
            this.getIdPersoneria()+","+
            this.getIdCiudad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getWeb())+","+
            utilitario.utiCadena.getTextoGuardado(this.getEmail())+","+
            this.getActivo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            this.getIdSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoCelular())+","+
            this.getIdPlanCuenta()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Razón Social", "razonsocial", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Apellido", "apellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Ruc", "ruc", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Personería", "personeria", generico.entLista.tipo_texto));
        return lst;
    }

}
