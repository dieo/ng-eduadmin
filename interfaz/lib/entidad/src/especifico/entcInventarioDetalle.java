/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entcInventarioDetalle {
    
    protected int iid;
    protected int iidInventario;
    protected int iidCentroCosto;
    protected String sobservacion;
    protected int iidArticulo;
    protected String sdescripcion;
    protected double ustockActual;
    protected double uexistencia;
    protected double udiferencia;
    protected short cestado;
    protected String smensaje;
    
    public static final int LONGITUD_OBSERVACION = 200;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_OBSERVACION = "Observación del producto (hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_DIFERENCIA = "Diferencia entre stock actual y existencia física (no editable)";
    public static final String TEXTO_ARTICULO = "Producto inventariado (no vacía)";
    public static final String TEXTO_STOCK_ACTUAL = "Cantidad actual del Producto en sistema (no editable)";
    public static final String TEXTO_EXISTENCIA = "Cantidad en existencia física del Producto (no vacío, valor positivo)";
    
    public entcInventarioDetalle() {
        this.iid = 0;
        this.iidInventario = 0;
        this.iidCentroCosto = 0;
        this.sobservacion = "";
        this.udiferencia = 0.0;
        this.iidArticulo = 0;
        this.sdescripcion = "";
        this.ustockActual = 0.0;
        this.uexistencia = 0.0;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entcInventarioDetalle(int iid, int iidOrdenCompra, int iidCentroCosto, String sobservacion, double udiferencia, int iidArticulo, String sdescripcion, double ustockActual, double uexistencia, short cestado) {
        this.iid = iid;
        this.iidInventario = iidOrdenCompra;
        this.iidCentroCosto = iidCentroCosto;
        this.sobservacion = sobservacion;
        this.udiferencia = udiferencia;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.ustockActual = ustockActual;
        this.uexistencia = uexistencia;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidOrdenCompra, int iidCentroCosto, String sobservacion, double udiferencia, int iidArticulo, String sdescripcion, double ustockActual, double uexistencia, short cestado) {
        this.iid = iid;
        this.iidInventario = iidOrdenCompra;
        this.iidCentroCosto = iidCentroCosto;
        this.sobservacion = sobservacion;
        this.udiferencia = udiferencia;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.ustockActual = ustockActual;
        this.uexistencia = uexistencia;
        this.cestado = cestado;
    }

    public entcInventarioDetalle copiar(entcInventarioDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdInventario(), this.getIdCentroCosto(), this.getObservacion(), this.getDiferencia(), this.getIdArticulo(), this.getDescripcion(), this.getStockActual(), this.getExistencia(),  this.getEstadoRegistro());
        return destino;
    }
    
    public entcInventarioDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(SQLException e) { }
        try { this.setIdInventario(rs.getInt("idinventario")); }
        catch(SQLException e) { }
        try { this.setObservacion(rs.getString("observacion")); }
        catch(SQLException e) { }
        try { this.setIdArticulo(rs.getInt("idarticulo")); }
        catch(SQLException e) { }
        try { this.setDescripcion(rs.getString("articulo")); }
        catch(SQLException e) { }
        try { this.setStockActual(rs.getDouble("stockactual")); }
        catch(SQLException e) { }
        try { this.setExistencia(rs.getDouble("existenciafisica")); }
        catch(SQLException e) { }
        try { this.setDiferencia(rs.getDouble("diferencia")); }
        catch(SQLException e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdInventario(int iidInventario) {
        this.iidInventario = iidInventario;
    }

    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }
    
    public void setStockActual(double ustockActual) {
        this.ustockActual = ustockActual;
    }

    public void setExistencia(double uexistencia) {
        this.uexistencia = uexistencia;
    }

    public void setDiferencia(double udiferencia) {
        this.udiferencia = udiferencia;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdInventario() {
        return this.iidInventario;
    }
        
    public int getIdArticulo() {
        return this.iidArticulo;
    }
   
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }
   
    public String getObservacion() {
        return this.sobservacion;
    }
    
    public double getStockActual() {
        return this.ustockActual;
    }

    public double getExistencia() {
        return this.uexistencia;
    }

    public double getDiferencia() {
        return this.udiferencia;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdArticulo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_ARTICULO;
            bvalido = false;
        }
        if ((this.getExistencia()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_EXISTENCIA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.inventariodetalle("+
            this.getId()+","+
            this.getIdInventario()+","+
            this.getIdArticulo()+","+
            this.getStockActual()+","+
            this.getExistencia()+","+
            this.getDiferencia()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getEstadoRegistro()+")";
    }

}
