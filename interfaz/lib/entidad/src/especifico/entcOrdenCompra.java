/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entcOrdenCompra {

    private int iid;
    private int iidPresupuesto;
    private int iidUsuario;
    private String susuario;
    private int iidAutorizador;
    private String sautorizador;
    private int iidProveedor;
    private String sproveedor;
    private int iidFilial;
    private String sfilial;
    private int iidDependencia;
    private String sdependencia;
    private int iidTipoPedido;
    private String stipoPedido;
    private java.util.Date dfecha;
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    private String scondicion;
    private int iidMoneda;
    private String smoneda;
    private int iidEstadoOrdenCompra;
    private String sestadoOrdenCompra;
    private double utotalOrdenCompra;
    private double ucambio;
    private double utotalGs;
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_IDPRESUPUESTO = "Nº de Presupuesto en que se basa la OC (0 si no esta relacionado con Presupuesto)";
    public static final String TEXTO_USUARIO = "Usuario que solicita la Compra (no vacía)";
    public static final String TEXTO_AUTORIZADOR = "Persona que autoriza la Compra";
    public static final String TEXTO_FILIAL = "Filial para la que se realiza la Compra (no vacía)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia para la que se realiza la Compra (no vacía)";
    public static final String TEXTO_PROVEEDOR = "Proveedor del artículo (no vacía)";
    public static final String TEXTO_FECHA = "Fecha del presupuesto (no vacía)";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación de la OC (no editable)";
    public static final String TEXTO_TOTAL_ORDEN_COMPRA = "Monto total de la OC (no editable)";
    public static final String TEXTO_CAMBIO = "Monto del cambio del día de la moneda (no vacía, 1 si en Gs)";
    public static final String TEXTO_TOTAL_GS = "Monto total de la OC en Gs (no editable)";
    public static final String TEXTO_MONEDA = "Moneda en que se cotiza la OC (no vacía)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Motivo de la Anulación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_CONDICION = "Condiciones de pago (no vacía, hasta 200 caracteres)";
    public static final String TEXTO_TIPO_PEDIDO = "Tipo de Pedido de la OC (no vacía)";
    public static final String TEXTO_ESTADO = "Estado de la OC (no editable)";
    
    
    public entcOrdenCompra() {
        this.iid = 0;
        this.iidPresupuesto = 0;
        this.iidUsuario = 0;
        this.susuario = "";
        this.iidAutorizador = 0;
        this.sautorizador = "";
        this.iidProveedor = 0;
        this.sproveedor = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.iidDependencia = 0;
        this.sdependencia = "";
        this.iidTipoPedido = 0;
        this.stipoPedido = "";
        this.dfecha = null;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.scondicion = "";
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iidEstadoOrdenCompra = 0;
        this.sestadoOrdenCompra = "";
        this.utotalOrdenCompra = 0.0;
        this.ucambio = 0.0;
        this.utotalGs = 0.0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entcOrdenCompra(int iid, int iidPresupuesto, int iidUsuario, String susuario, int iidAutorizador, String sautorizador, int iidProveedor, String sproveedor, int iidFilial, String sfilial, int iidDependencia, String sdependencia, int iidTipoPedido, String stipoPedido, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidMoneda,  String smoneda, int iidEstadoOrdenCompra, String sestadoOrdenCompra, double utotalOrdenCompra, double ucambio, double utotalGs, String scondicion, short hestadoRegistro) {
        this.iid = iid;
        this.iidPresupuesto = iidPresupuesto;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidTipoPedido = iidTipoPedido;
        this.stipoPedido = stipoPedido;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.scondicion = scondicion;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidEstadoOrdenCompra = iidEstadoOrdenCompra;
        this.sestadoOrdenCompra = sestadoOrdenCompra;
        this.utotalOrdenCompra = utotalOrdenCompra;
        this.ucambio = ucambio;
        this.utotalGs = utotalGs;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidPresupuesto, int iidUsuario, String susuario, int iidAutorizador, String sautorizador, int iidProveedor, String sproveedor, int iidFilial, String sfilial, int iidDependencia, String sdependencia, int iidTipoPedido, String stipoPedido, java.util.Date dfecha, java.util.Date dfechaAnulado, String sobservacionAnulado, int iidMoneda,  String smoneda, int iidEstadoOrdenCompra, String sestadoOrdenCompra, double utotalOrdenCompra, double ucambio, double utotalGs, String scondicion, short hestadoRegistro) {
        this.iid = iid;
        this.iidPresupuesto = iidPresupuesto;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidTipoPedido = iidTipoPedido;
        this.stipoPedido = stipoPedido;
        this.dfecha = dfecha;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.scondicion = scondicion;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidEstadoOrdenCompra = iidEstadoOrdenCompra;
        this.sestadoOrdenCompra = sestadoOrdenCompra;
        this.utotalOrdenCompra = utotalOrdenCompra;
        this.ucambio = ucambio;
        this.utotalGs = utotalGs;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entcOrdenCompra copiar(entcOrdenCompra destino) {
        destino.setEntidad(this.getId(), this.getIdPresupuesto(), this.getIdUsuario(), this.getUsuario(), this.getIdAutorizador(), this.getAutorizador(), this.getIdProveedor(), this.getProveedor(), this.getIdFilial(), this.getFilial(), this.getIdDependencia(), this.getDependencia(), this.getIdTipoPedido(), this.getTipoPedido(), this.getFecha(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getIdMoneda(), this.getMoneda(), this.getIdEstadoOrdenCompra(), this.getEstadoOrdenCompra(), this.getTotalOrdenCompra(), this.getCambio(), this.getTotalGs(), this.getCondicion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcOrdenCompra cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdPresupuesto(rs.getInt("idpresupuesto")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuario(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setIdAutorizador(rs.getInt("idautorizador")); }
        catch(Exception e) {}
        try { this.setAutorizador(rs.getString("autorizador")); }
        catch(Exception e) {}
        try { this.setIdProveedor(rs.getInt("idproveedor")); }
        catch(Exception e) {}
        try { this.setProveedor(rs.getString("proveedor")); }
        catch(Exception e) {}
        try { this.setIdTipoPedido(rs.getInt("idtipopedido")); }
        catch(Exception e) {}
        try { this.setTipoPedido(rs.getString("tipopedido")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setCondicion(rs.getString("condicion")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setIdEstadoOrdenCompra(rs.getInt("idestadoordencompra")); }
        catch(Exception e) {}
        try { this.setEstadoOrdenCompra(rs.getString("estadoordencompra")); }
        catch(Exception e) {}
        try { this.setTotalOrdenCompra(rs.getDouble("totalordencompra")); }
        catch(Exception e) {}
        try { this.setCambio(rs.getDouble("cambio")); }
        catch(Exception e) {}
        try { this.setTotalGs(rs.getDouble("totalgs")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdPresupuesto(int iidPresupuesto) {
        this.iidPresupuesto = iidPresupuesto;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }
    
    public void setUsuario(String susuario) {
        this.susuario = susuario;
    }

    public void setIdAutorizador(int iidAutorizador) {
        this.iidAutorizador = iidAutorizador;
    }
    
    public void setAutorizador(String sautorizador) {
        this.sautorizador = sautorizador;
    }

    public void setIdProveedor(int iidProveedor) {
        this.iidProveedor = iidProveedor;
    }
    
    public void setProveedor(String sproveedor) {
        this.sproveedor = sproveedor;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }
    
    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public void setIdTipoPedido(int iidTipoPedido) {
        this.iidTipoPedido = iidTipoPedido;
    }
    
    public void setTipoPedido(String stipoPedido) {
        this.stipoPedido = stipoPedido;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }
    
    public void setCondicion(String scondicion) {
        this.scondicion = scondicion;
    }
    
    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setIdEstadoOrdenCompra(int iidEstadoOrdenCompra) {
        this.iidEstadoOrdenCompra = iidEstadoOrdenCompra;
    }

    public void setEstadoOrdenCompra(String sestadoOrdenCompra) {
        this.sestadoOrdenCompra = sestadoOrdenCompra;
    }
    
    public void setTotalOrdenCompra(double utotalOrdenCompra) {
        this.utotalOrdenCompra = utotalOrdenCompra;
    }
        
    public void setCambio(double ucambio) {
        this.ucambio = ucambio;
    }
        
    public void setTotalGs(double utotalGs) {
        this.utotalGs = utotalGs;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdPresupuesto() {
        return this.iidPresupuesto;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }
    
    public String getUsuario() {
        return this.susuario;
    }

    public int getIdAutorizador() {
        return this.iidAutorizador;
    }
    
    public String getAutorizador() {
        return this.sautorizador;
    }

    public int getIdProveedor() {
        return this.iidProveedor;
    }
    
    public String getProveedor() {
        return this.sproveedor;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }
    
    public String getFilial() {
        return this.sfilial;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }
    
    public String getDependencia() {
        return this.sdependencia;
    }

    public int getIdTipoPedido() {
        return this.iidTipoPedido;
    }
    
    public String getTipoPedido() {
        return this.stipoPedido;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public String getCondicion() {
        return this.scondicion;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public int getIdEstadoOrdenCompra() {
        return this.iidEstadoOrdenCompra;
    }

    public String getEstadoOrdenCompra() {
        return this.sestadoOrdenCompra;
    }

    public double getTotalOrdenCompra() {
        return this.utotalOrdenCompra;
    }

    public double getCambio() {
        return this.ucambio;
    }

    public double getTotalGs() {
        return this.utotalGs;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getCondicion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CONDICION;
            bvalido = false;
        }
        if (this.getIdProveedor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_PROVEEDOR;
            bvalido = false;
        }
        if (this.getIdTipoPedido()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_TIPO_PEDIDO;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.ordencompra("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFecha())+","+
            this.getIdTipoPedido()+","+
            this.getIdUsuario()+","+
            this.getIdAutorizador()+","+
            this.getIdProveedor()+","+
            this.getTotalOrdenCompra()+","+
            this.getIdMoneda()+","+
            this.getIdPresupuesto()+","+
            this.getIdEstadoOrdenCompra()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCondicion())+","+
            this.getIdFilial()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha", "fecha", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Nº Orden Compra", "id", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(3, "Proveedor", "proveedor", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(4, "Estado", "estadoordencompra", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(5, "Tipo de Pedido", "tipopedido", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(6, "Filial", "filial", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
