/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleEstadoSocio {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String snombre;
    private String sapellido;
    private int iidEstadoAnterior;
    private String sestadoAnterior;
    private int iidEstadoActual;
    private String sestadoActual;
    private boolean bactivo;
    private java.util.Date dfecha;
    private int iidDocumento;
    private String sdocumento;
    private int inumeroDocumento;
    private java.util.Date dfechaDocumento;
    private String sobservacion;
    
    private short hidEstadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_OBSERVACION = 200;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no editable)";
    public static final String TEXTO_ESTADO_ANTERIOR = "Estado anterior del Socio (no vacío)";
    public static final String TEXTO_ESTADO_ACTUAL = "Estado al cual pasa el Socio (no vacío)";
    public static final String TEXTO_ACTIVO = "Activo";
    public static final String TEXTO_FECHA = "Fecha de la operación (no vacío)";
    public static final String TEXTO_DOCUMENTO = "Documento por el cual pasa al Estado actual (no vacío)";
    public static final String TEXTO_NUMERO_DOCUMENTO = "Número del Documento por el cual pasa al Estado actual (no vacío, valor positivo)";
    public static final String TEXTO_FECHA_DOCUMENTO = "Fecha del Documento por el cual pasa al Estado actual (no vacío)";
    public static final String TEXTO_OBSERVACION = "Observación (no vacío " + LONGITUD_OBSERVACION + " caracteres)";

    public entDetalleEstadoSocio() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.iidEstadoAnterior = 0;
        this.sestadoAnterior = "";
        this.iidEstadoActual = 0;
        this.sestadoActual = "";
        this.bactivo = false;
        this.dfecha = null;
        this.iidDocumento = 0;
        this.sdocumento = "";
        this.inumeroDocumento = 0;
        this.dfechaDocumento = null;
        this.sobservacion = "";
        this.hidEstadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleEstadoSocio(int iid, int iidSocio, String scedula, String snombre, String sapellido, int iidEstadoAnterior, String sestadoAnterior, int iidEstadoActual, String sestadoActual, boolean bactivo, java.util.Date dfecha, int iidDocumento, String sdocumento, int inumeroDocumento, java.util.Date dfechaDocumento, String sobservacion, short hidEstadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.iidEstadoAnterior = iidEstadoAnterior;
        this.sestadoAnterior = sestadoAnterior;
        this.iidEstadoActual = iidEstadoActual;
        this.sestadoActual = sestadoActual;
        this.bactivo = bactivo;
        this.dfecha = dfecha;
        this.iidDocumento = iidDocumento;
        this.sdocumento = sdocumento;
        this.inumeroDocumento = inumeroDocumento;
        this.dfechaDocumento = dfechaDocumento;
        this.sobservacion = sobservacion;
        this.hidEstadoRegistro = hidEstadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidSocio, String scedula, String snombre, String sapellido, int iidEstadoAnterior, String sestadoAnterior, int iidEstadoActual, String sestadoActual, boolean bactivo, java.util.Date dfecha, int iidDocumento, String sdocumento, int inumeroDocumento, java.util.Date dfechaDocumento, String sobservacion, short hidEstadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.iidEstadoAnterior = iidEstadoAnterior;
        this.sestadoAnterior = sestadoAnterior;
        this.iidEstadoActual = iidEstadoActual;
        this.sestadoActual = sestadoActual;
        this.bactivo = bactivo;
        this.dfecha = dfecha;
        this.iidDocumento = iidDocumento;
        this.sdocumento = sdocumento;
        this.inumeroDocumento = inumeroDocumento;
        this.dfechaDocumento = dfechaDocumento;
        this.sobservacion = sobservacion;
        this.hidEstadoRegistro = hidEstadoRegistro;
    }

    public entDetalleEstadoSocio copiar(entDetalleEstadoSocio destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getNombre(), this.getApellido(), this.getIdEstadoAnterior(), this.getEstadoAnterior(), this.getIdEstadoActual(), this.getEstadoActual(), this.getActivo(), this.getFecha(), this.getIdDocumento(), this.getDocumento(), this.getNumeroDocumento(), this.getFechaDocumento(), this.getObservacion(), this.getEstadoRegistro());
        return destino;
    }
    
    public entDetalleEstadoSocio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setIdEstadoAnterior(rs.getInt("idestadoanterior")); }
        catch(Exception e) {}
        try { this.setEstadoAnterior(rs.getString("estadoanterior")); }
        catch(Exception e) {}
        try { this.setIdEstadoActual(rs.getInt("idestadoactual")); }
        catch(Exception e) {}
        try { this.setEstadoActual(rs.getString("estadoactual")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setIdDocumento(rs.getInt("iddocumento")); }
        catch(Exception e) {}
        try { this.setDocumento(rs.getString("documento")); }
        catch(Exception e) {}
        try { this.setNumeroDocumento(rs.getInt("numerodocumento")); }
        catch(Exception e) {}
        try { this.setFechaDocumento(rs.getDate("fechadocumento")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombre(String snombre) {
        this.snombre = snombre;
    }

    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }

    public void setIdEstadoAnterior(int iidEstadoAnterior) {
        this.iidEstadoAnterior = iidEstadoAnterior;
    }

    public void setEstadoAnterior(String sestadoAnterior) {
        this.sestadoAnterior = sestadoAnterior;
    }

    public void setIdEstadoActual(int iidEstadoActual) {
        this.iidEstadoActual = iidEstadoActual;
    }

    public void setEstadoActual(String sestadoActual) {
        this.sestadoActual = sestadoActual;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setIdDocumento(int iidDocumento) {
        this.iidDocumento = iidDocumento;
    }

    public void setDocumento(String sdocumento) {
        this.sdocumento = sdocumento;
    }

    public void setNumeroDocumento(int inumeroDocumento) {
        this.inumeroDocumento = inumeroDocumento;
    }

    public void setFechaDocumento(java.util.Date dfechaDocumento) {
        this.dfechaDocumento = dfechaDocumento;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setEstadoRegistro(short hidEstadoRegistro) {
        this.hidEstadoRegistro = hidEstadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombre() {
        return this.snombre;
    }

    public String getApellido() {
        return this.sapellido;
    }

    public int getIdEstadoAnterior() {
        return this.iidEstadoAnterior;
    }

    public String getEstadoAnterior() {
        return this.sestadoAnterior;
    }

    public int getIdEstadoActual() {
        return this.iidEstadoActual;
    }

    public String getEstadoActual() {
        return this.sestadoActual;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public int getIdDocumento() {
        return this.iidDocumento;
    }

    public String getDocumento() {
        return this.sdocumento;
    }

    public int getNumeroDocumento() {
        return this.inumeroDocumento;
    }

    public java.util.Date getFechaDocumento() {
        return this.dfechaDocumento;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public short getEstadoRegistro() {
        return this.hidEstadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdSocio()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getIdEstadoAnterior()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ESTADO_ANTERIOR;
            bvalido = false;
        }
        if (this.getIdEstadoActual()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ESTADO_ACTUAL;
            bvalido = false;
        }
        if (this.getIdEstadoAnterior()==this.getIdEstadoActual()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "El Estado acutal debe ser distinto al Estado anterior";
            bvalido = false;
        }
        if (this.getFecha()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getIdDocumento()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DOCUMENTO;
            bvalido = false;
        }
        if (this.getNumeroDocumento()< 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_DOCUMENTO;
            bvalido = false;
        }
        if (this.getFechaDocumento()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_DOCUMENTO;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detalleestadosocio("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdEstadoAnterior()+","+
            this.getIdEstadoActual()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFecha())+","+
            this.getIdDocumento()+","+
            this.getNumeroDocumento()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaDocumento())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

}
