/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPrueba {
    
    private int iid;
    private String snombre;
    private String scomentario;
    private double usueldo;
    private java.util.Date dfecha;
    private String scontrasena;
    private boolean bactivo;
    private int iidProfesion;
    private String sprofesion;
    private int iidGiraduria;
    private String sgiraduria;
    private int iidTipoCasa;
    private String stipoCasa;
    private String sedad;
    private int iestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_NOMBRE = 50;
    public static final int LONGITUD_COMENTARIO = 100;
    public static final int LONGITUD_CONTRASENA = 10;
    public static final int LONGITUD_EDAD = 2;
    
    public entPrueba() {
        this.iid = 0;
        this.snombre = "";
        this.scomentario = "";
        this.usueldo = 0.0;
        this.dfecha = null;
        this.scontrasena = "";
        this.bactivo = false;
        this.iidProfesion = 0;
        this.sprofesion = "";
        this.iidGiraduria = 0;
        this.sgiraduria = "";
        this.iidTipoCasa = 0;
        this.stipoCasa = "";
        this.sedad = "";
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entPrueba(int iid, String snombre, String scomentario, double usueldo, java.util.Date dfecha, String scontrasena, boolean bactivo, int iidProfesion, String sprofesion, int iidGiraduria, String sgiraduria, int iidTipoCasa, String stipoCasa, String sedad, int iestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre;
        this.scomentario = scomentario;
        this.usueldo = usueldo;
        this.dfecha = dfecha;
        this.scontrasena = scontrasena;
        this.bactivo = bactivo;
        this.iidProfesion = iidProfesion;
        this.sprofesion = sprofesion;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.iidTipoCasa = iidTipoCasa;
        this.stipoCasa = stipoCasa;
        this.sedad = sedad;
        this.iestadoRegistro = iestadoRegistro;
    }
    
     public void setEntidad(int iid, String snombre, String scomentario, double usueldo, java.util.Date dfecha, String scontrasena, boolean bactivo, int iidProfesion, String sprofesion, int iidGiraduria, String sgiraduria, int iidTipoCasa, String stipoCasa, String sedad, int iestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre;
        this.scomentario = scomentario;
        this.usueldo = usueldo;
        this.dfecha = dfecha;
        this.scontrasena = scontrasena;
        this.bactivo = bactivo;
        this.iidProfesion = iidProfesion;
        this.sprofesion = sprofesion;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.iidTipoCasa = iidTipoCasa;
        this.stipoCasa = stipoCasa;
        this.sedad = sedad;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entPrueba copiar(entPrueba destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getComentario(), this.getSueldo(), this.getFecha(), this.getContrasena(), this.getActivo(), this.getIdProfesion(), this.getProfesion(), this.getIdGiraduria(), this.getGiraduria(), this.getIdTipoCasa(), this.getTipoCasa(), this.getEdad(), this.getEstadoRegistro());
        return destino;
    }
    
    public entPrueba cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setComentario(rs.getString("comentario")); }
        catch(Exception e) {}
        try { this.setSueldo(rs.getDouble("sueldo")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setContrasena(rs.getString("contrasena")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setIdProfesion(rs.getInt("idprofesion")); }
        catch(Exception e) {}
        try { this.setProfesion(rs.getString("profesion")); }
        catch(Exception e) {}
        try { this.setIdGiraduria(rs.getInt("idgiraduria")); }
        catch(Exception e) {}
        try { this.setGiraduria(rs.getString("giraduria")); }
        catch(Exception e) {}
        try { this.setIdTipoCasa(rs.getInt("idtipocasa")); }
        catch(Exception e) {}
        try { this.setTipoCasa(rs.getString("tipocasa")); }
        catch(Exception e) {}
        try { this.setEdad(rs.getString("edad")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setNombre(String snombre) {
        this.snombre = snombre;
    }

    public void setComentario(String scomentario) {
        this.scomentario = scomentario;
    }

    public void setSueldo(double usueldo) {
        this.usueldo = usueldo;
    }
    
    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }
    
    public void setContrasena(String scontrasena) {
        this.scontrasena = scontrasena;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setIdProfesion(int iidProfesion) {
        this.iidProfesion = iidProfesion;
    }

    public void setProfesion(String sprofesion) {
        this.sprofesion = sprofesion;
    }
    
    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }

    public void setGiraduria(String sgiraduria) {
        this.sgiraduria = sgiraduria;
    }
    
    public void setIdTipoCasa(int iidTipoCasa) {
        this.iidTipoCasa = iidTipoCasa;
    }

    public void setTipoCasa(String stipoCasa) {
        this.stipoCasa = stipoCasa;
    }
    
    public void setEdad(String sedad) {
        this.sedad = sedad;
    }
    
    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public String getNombre() {
        return this.snombre;
    }

    public String getComentario() {
        return this.scomentario;
    }
    
    public double getSueldo() {
        return this.usueldo;
    }
    
    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public String getContrasena() {
        return this.scontrasena;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }

    public int getIdProfesion() {
        return this.iidProfesion;
    }

    public String getProfesion() {
        return this.sprofesion;
    }

    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public String getGiraduria() {
        return this.sgiraduria;
    }

    public int getIdTipoCasa() {
        return this.iidTipoCasa;
    }

    public String getTipoCasa() {
        return this.stipoCasa;
    }
    
    public String getEdad() {
        return this.sedad;
    }
    
    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    public boolean esValido() {
        this.smensaje = "";
        if (this.getNombre().isEmpty()) this.smensaje += "nombre\n";
        if (this.getComentario().isEmpty()) this.smensaje += "comentario\n";
        if (this.getSueldo()<0) this.smensaje += "sueldo\n";
        if (this.getFecha()==null) this.smensaje += "fecha\n";
        if (this.getContrasena().isEmpty()) this.smensaje += "contrasena\n";
        if (this.getIdProfesion()<=0) this.smensaje += "profesion\n";
        if (this.getIdGiraduria()<=0) this.smensaje += "giraduria\n";
        if (this.getIdTipoCasa()<=0) this.smensaje += "tipocasa\n";
        if (this.getEdad().isEmpty()) this.smensaje += "edad\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }

    public String getSentencia() {
        return "SELECT prueba("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getComentario())+","+
            this.getSueldo()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFecha())+","+
            utilitario.utiCadena.getTextoGuardado(this.getContrasena())+","+
            this.getActivo()+","+
            this.getIdProfesion()+","+
            this.getIdGiraduria()+","+
            this.getIdTipoCasa()+","+
            utilitario.utiCadena.getTextoGuardado(this.getEdad())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Ruc", "ruc", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Tipo Entidad", "tipoentidad", generico.entLista.tipo_texto));
        return lst;
    }
    
}
