/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entEmpresa extends generico.entGenerica {
    
    protected String sdireccion;
    protected String sruc;
    protected String stelefono;
    protected int iidCiudad;
    protected String sciudad;
    protected int iidRegional;
    protected String sregional;
    protected String scontacto;
    protected String sweb;
    protected String semail;
    protected boolean bactivo;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final int LONGITUD_RUC = 12;
    public static final int LONGITUD_DIRECCION = 40;
    public static final int LONGITUD_TELEFONO = 20;
    public static final int LONGITUD_CONTACTO = 40;
    public static final int LONGITUD_WEB = 30;
    public static final int LONGITUD_EMAIL = 50;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_RUC = "RUC de Empresa (hasta " + LONGITUD_RUC + " caracteres)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Empresa (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres, sin la secuencia '_/_')";
    public static final String TEXTO_DIRECCION = "Dirección (hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_TELEFONO = "Teléfono (hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_CIUDAD = "Ciudad (no vacía)";
    public static final String TEXTO_REGIONAL = "Regional (no vacío)";
    public static final String TEXTO_CONTACTO = "Contacto con Entidad (hasta " + LONGITUD_CONTACTO + " caracteres)";
    public static final String TEXTO_WEB = "Página web de Entidad (hasta " + LONGITUD_WEB + " caracteres)";
    public static final String TEXTO_EMAIL = "E-mail de Entidad (hasta " + LONGITUD_EMAIL + " caracteres)";
    public static final String TEXTO_TIPO_ENTIDAD = "Tipo de Entidad (no vacío)";
    public static final String TEXTO_ACTIVO = "Activo";
    
    public entEmpresa() {
        super();
        this.sruc = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.iidCiudad = 0;
        this.sciudad = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.scontacto = "";
        this.sweb = "";
        this.semail = "";
        this.bactivo = false;
    }

    public entEmpresa(int iid, String sdescripcion, String sdireccion, String stelefono, String sruc, int iidCiudad, String sciudad, int iidRegional, String sregional, String scontacto, String sweb, String semail, boolean bactivo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.sruc = sruc;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.scontacto = scontacto;
        this.sweb = sweb;
        this.semail = semail;
        this.bactivo = bactivo;
    }
    
     public void setEntidad(int iid, String sdescripcion, String sdireccion, String stelefono, String sruc, int iidCiudad, String sciudad, int iidRegional, String sregional, String scontacto, String sweb, String semail, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.sruc = sruc;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.scontacto = scontacto;
        this.sweb = sweb;
        this.semail = semail;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entEmpresa copiar(entEmpresa destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getDireccion(), this.getTelefono(), this.getRuc(), this.getIdCiudad(), this.getCiudad(), this.getIdRegional(), this.getRegional(), this.getContacto(), this.getWeb(), this.getEmail(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }
    
    public entEmpresa cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIdCiudad(rs.getInt("idciudad")); }
        catch(Exception e) {}
        try { this.setCiudad(rs.getString("ciudad")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setContacto(rs.getString("contacto")); }
        catch(Exception e) {}
        try { this.setWeb(rs.getString("web")); }
        catch(Exception e) {}
        try { this.setEmail(rs.getString("email")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setRuc(String sruc) {
        this.sruc = sruc;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setIdCiudad(int iidCiudad) {
        this.iidCiudad = iidCiudad;
    }
    
    public void setCiudad(String sciudad) {
        this.sciudad = sciudad;
    }
    
    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }
    
    public void setRegional(String sregional) {
        this.sregional = sregional;
    }
    
    public void setContacto(String scontacto) {
        this.scontacto = scontacto;
    }
    
    public void setWeb(String sweb) {
        this.sweb = sweb;
    }
    
    public void setEmail(String semail) {
        this.semail = semail;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public String getRuc() {
        return this.sruc;
    }
    
    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefono() {
        return this.stelefono;
    }
    
    public int getIdCiudad() {
        return this.iidCiudad;
    }
    
    public String getCiudad() {
        return this.sciudad;
    }
    
    public int getIdRegional() {
        return this.iidRegional;
    }
    
    public String getRegional() {
        return this.sregional;
    }
    
    public String getContacto() {
        return this.scontacto;
    }
    
    public String getWeb() {
        return this.sweb;
    }
    
    public String getEmail() {
        return this.semail;
    }

    public boolean getActivo() {
        return this.bactivo;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty() || this.getDescripcion().contains(" / ")) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdCiudad() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CIUDAD;
            bvalido = false;
        }
        if (this.getIdRegional() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT empresa("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            utilitario.utiCadena.getTextoGuardado(this.getRuc())+","+
            this.getIdCiudad()+","+
            this.getIdRegional()+","+
            utilitario.utiCadena.getTextoGuardado(this.getContacto())+","+
            utilitario.utiCadena.getTextoGuardado(this.getWeb())+","+
            utilitario.utiCadena.getTextoGuardado(this.getEmail())+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Ruc", "ruc", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Ciudad", "ciudad", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Regional", "regional", generico.entLista.tipo_texto));
        return lst;
    }
    
}
