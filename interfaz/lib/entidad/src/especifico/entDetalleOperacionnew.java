/*0
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleOperacionnew {
    
    // cabecera
    private int iidMovimiento;
    private int iidMovimientoCancela;
    private int iidCuenta;
    private String scuenta;
    private String stabla;
    private java.util.Date dfechaOperacion;
    private double utasaInteresMoratorio;
    private double utasaInteresPunitorio;
    private int iplazo;
    private int inumeroOperacion;
    private int iidImpuesto;
    private double utasaImpuesto;
    private double utotalCapital;
    private double utotalInteres;
    private double utotalAtraso;
    private double utotalSaldo;
    private double utotalExoneracion;
    private double utotalMes;
    private double utotalCobro;
    private double utotalAplicado;
    private double utotalCancelar;
    private String sprioridad;
    private double utasaExoneracion;
    private boolean bcancela;
    private String sresumenCuota;
    public int idesde;
    public int ihasta;
    
    private int iid;
    private int iidDetalleOperacion;
    private int icuota;
    private java.util.Date dfechaVencimiento;
    private double umontoCapital;
    private double umontoInteres;
    private double umontoSaldo;
    private double umontoExoneracion;
    private double umontoCobro;
    private double umontoAplicado;
    private double umontoInteresMoratorio;
    private double umontoInteresPunitorio;
    private String sestado; // A=atraso - C=cobro - S=saldo - E=enviado
    public int iorden;

    private short hestadoRegistro;
    private String smensaje;

    public static final String estado_atraso = "A";
    public static final String estado_cobro = "C";
    public static final String estado_enviado = "E";
    public static final String estado_saldo = "S";
    
    public entDetalleOperacionnew() {
        this.iidMovimiento = 0;
        this.iidMovimientoCancela = 0;
        this.iidCuenta = 0;
        this.scuenta = "";
        this.stabla = "";
        this.dfechaOperacion = null;
        this.utasaInteresMoratorio = 0.0;
        this.utasaInteresPunitorio = 0.0;
        this.iplazo = 0;
        this.inumeroOperacion = 0;
        this.iidImpuesto = 0;
        this.utasaImpuesto = 0.0;
        this.utotalCapital = 0.0;
        this.utotalInteres = 0.0;
        this.utotalAtraso = 0.0;
        this.utotalSaldo = 0.0;
        this.utotalExoneracion = 0.0;
        this.utotalMes = 0.0;
        this.utotalCobro = 0.0;
        this.utotalAplicado = 0.0;
        this.utotalCancelar = 0.0;
        this.sprioridad = "";
        this.utasaExoneracion = 0.0;
        this.sresumenCuota = "";
        this.bcancela = false;
        
        this.iid = 0;
        this.iidDetalleOperacion = 0;
        this.icuota = 0;
        this.dfechaVencimiento = null;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.umontoSaldo = 0.0;
        this.umontoExoneracion = 0.0;
        this.umontoCobro = 0.0;
        this.umontoAplicado = 0.0;
        this.umontoInteresMoratorio = 0.0;
        this.umontoInteresPunitorio = 0.0;
        this.sestado = "";
        this.iorden = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetalleOperacionnew(int iidMovimiento, int iidMovimientoCancela, int iidCuenta, String scuenta, String stabla, java.util.Date dfechaOperacion, double utasaInteresMoratorio, double utasaInteresPunitorio, int iplazo, int inumeroOperacion, int iidImpuesto, double utasaImpuesto, double utotalCapital, double utotalInteres, double utotalAtraso, double utotalSaldo, double utotalExoneracion, double utotalMes, double utotalCobro, double utotalAplicado, double utotalCancelar, String sprioridad, double utasaExoneracion, String sresumenCuota, boolean bcancela, int iid, int iidDetalleOperacion, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double umontoSaldo, double umontoExoneracion, double umontoCobro, double umontoAplicado, double umontoInteresMoratorio, double umontoInteresPunitorio, String sestado, int iorden, short hestadoRegistro) {
        this.iidMovimiento = iidMovimiento;
        this.iidMovimientoCancela = iidMovimientoCancela;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.stabla = stabla;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.iplazo = iplazo;
        this.inumeroOperacion = inumeroOperacion;
        this.iidImpuesto = iidImpuesto;
        this.utasaImpuesto = utasaImpuesto;
        this.utotalCapital = utotalCapital;
        this.utotalInteres = utotalInteres;
        this.utotalAtraso = utotalAtraso;
        this.utotalSaldo = utotalSaldo;
        this.utotalExoneracion = utotalExoneracion;
        this.utotalMes = utotalMes;
        this.utotalCobro = utotalCobro;
        this.utotalAplicado = utotalAplicado;
        this.utotalCancelar = utotalCancelar;
        this.sprioridad = sprioridad;
        this.utasaExoneracion = utasaExoneracion;
        this.sresumenCuota = sresumenCuota;
        this.bcancela = bcancela;
        
        this.iid = iid;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoSaldo = umontoSaldo;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoCobro = umontoCobro;
        this.umontoAplicado = utotalAplicado;
        this.umontoInteresMoratorio = umontoInteresMoratorio;
        this.umontoInteresPunitorio = umontoInteresPunitorio;
        this.sestado = sestado;
        this.iorden = iorden;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iidMovimiento, int iidMovimientoCancela, int iidCuenta, String scuenta, String stabla, java.util.Date dfechaOperacion, double utasaInteresMoratorio, double utasaInteresPunitorio, int iplazo, int inumeroOperacion, int iidImpuesto, double utasaImpuesto, double utotalCapital, double utotalInteres, double utotalAtraso, double utotalSaldo, double utotalExoneracion, double utotalMes, double utotalCobro, double utotalAplicado, double utotalCancelar, String sprioridad, double utasaExoneracion, String sresumenCuota, boolean bcancela, int iid, int iidDetalleOperacion, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double umontoSaldo, double umontoExoneracion, double umontoCobro, double umontoAplicado, double umontoInteresMoratorio, double umontoInteresPunitorio, String sestado, int iorden, short hestadoRegistro) {
        this.iidMovimiento = iidMovimiento;
        this.iidMovimientoCancela = iidMovimientoCancela;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.stabla = stabla;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.iplazo = iplazo;
        this.inumeroOperacion = inumeroOperacion;
        this.iidImpuesto = iidImpuesto;
        this.utasaImpuesto = utasaImpuesto;
        this.utotalCapital = utotalCapital;
        this.utotalInteres = utotalInteres;
        this.utotalAtraso = utotalAtraso;
        this.utotalSaldo = utotalSaldo;
        this.utotalExoneracion = utotalExoneracion;
        this.utotalMes = utotalMes;
        this.utotalCobro = utotalCobro;
        this.utotalAplicado = utotalAplicado;
        this.utotalCancelar = utotalCancelar;
        this.sprioridad = sprioridad;
        this.utasaExoneracion = utasaExoneracion;
        this.sresumenCuota = sresumenCuota;
        this.bcancela = bcancela;
        
        this.iid = iid;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoSaldo = umontoSaldo;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoCobro = umontoCobro;
        this.umontoAplicado = umontoAplicado;
        this.umontoInteresMoratorio = umontoInteresMoratorio;
        this.umontoInteresPunitorio = umontoInteresPunitorio;
        this.sestado = sestado;
        this.iorden = iorden;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleOperacionnew copiar(entDetalleOperacionnew destino) {
        destino.setEntidad(this.getcIdMovimiento(), this.getcIdMovimientoCancela(), this.getcIdCuenta(), this.getcCuenta(), this.getcTabla(), this.getcFechaOperacion(), this.getcTasaInteresMoratorio(), this.getcTasaInteresPunitorio(), this.getcPlazo(), this.getcNumeroOperacion(), this.getcIdImpuesto(), this.getcTasaImpuesto(), this.getcTotalCapital(), this.getcTotalInteres(), this.getcTotalAtraso(), this.getcTotalSaldo(), this.getcTotalExoneracion(), this.getcTotalMes(), this.getcTotalCobro(), this.getcTotalAplicado(), this.getcTotalCancelar(), this.getcPrioridad(), this.getcTasaExoneracion(), this.getcResumenCuota(), this.getcCancela(), this.getdId(), this.getdIdDetalleOperacion(), this.getdCuota(), this.getdFechaVencimiento(), this.getdMontoCapital(), this.getdMontoInteres(), this.getdMontoSaldo(), this.getdMontoExoneracion(), this.getdMontoCobro(), this.getdMontoAplicado(), this.getdMontoInteresMoratorio(), this.getdMontoInteresPunitorio(), this.getdEstado(), this.getdOrden(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleOperacionnew cargar(java.sql.ResultSet rs) {
        try { this.setcIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) { }
        try { this.setcIdMovimientoCancela(rs.getInt("idmovimientocancela")); }
        catch(Exception e) { }
        try { this.setcIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) { }
        try { this.setcCuenta(rs.getString("cuenta")); }
        catch(Exception e) { }
        try { this.setcTabla(rs.getString("tabla")); }
        catch(Exception e) { }
        try { this.setcFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) { }
        try { this.setcTasaInteresMoratorio(rs.getDouble("tasainteresmoratorio")); }
        catch(Exception e) { }
        try { this.setcTasaInteresPunitorio(rs.getDouble("tasainteresmoratorio")); }
        catch(Exception e) { }
        try { this.setcPlazo(rs.getInt("plazo")); }
        catch(Exception e) { }
        try { this.setcNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) { }
        try { this.setcIdImpuesto(rs.getInt("idimpuesto")); }
        catch(Exception e) { }
        try { this.setcTasaImpuesto(rs.getDouble("tasaimpuesto")); }
        catch(Exception e) { }
        try { this.setcTotalCapital(rs.getDouble("totalcapital")); }
        catch(Exception e) { }
        try { this.setcTotalInteres(rs.getDouble("totalinteres")); }
        catch(Exception e) { }
        try { this.setcTotalAtraso(rs.getDouble("totalatraso")); }
        catch(Exception e) { }
        try { this.setcTotalSaldo(rs.getDouble("totalsaldo")); }
        catch(Exception e) { }
        try { this.setcTotalExoneracion(rs.getDouble("totalexoneracion")); }
        catch(Exception e) { }
        try { this.setcTotalMes(rs.getDouble("totalmes")); }
        catch(Exception e) { }
        try { this.setcTotalCobro(rs.getDouble("totalcobro")); }
        catch(Exception e) { }
        try { this.setcTotalAplicado(rs.getDouble("totalaplicado")); }
        catch(Exception e) { }
        try { this.setcTotalCancelar(rs.getDouble("totalcancelar")); }
        catch(Exception e) { }
        try { this.setcPrioridad(rs.getString("prioridad")); }
        catch(Exception e) { }
        try { this.setcTasaExoneracion(rs.getDouble("tasaexoneracion")); }
        catch(Exception e) { }
        try { this.setcResumenCuota(rs.getString("resumencuota")); }
        catch(Exception e) { }
        try { this.setcCancela(rs.getBoolean("cancela")); }
        catch(Exception e) { }

        try { this.setdId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setdIdDetalleOperacion(rs.getInt("iddetalleoperacion")); }
        catch(Exception e) { }
        try { this.setdCuota(rs.getInt("cuota")); }
        catch(Exception e) { }
        try { this.setdFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) { }
        try { this.setdMontoCapital(rs.getDouble("montocapital")); }
        catch(Exception e) { }
        try { this.setdMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) { }
        try { this.setdMontoSaldo(rs.getDouble("montosaldo")); }
        catch(Exception e) { }
        try { this.setdMontoExoneracion(rs.getDouble("montoexoneracion")); }
        catch(Exception e) { }
        try { this.setdMontoCobro(rs.getDouble("montocobro")); }
        catch(Exception e) { }
        try { this.setdMontoAplicado(rs.getDouble("montoaplicado")); }
        catch(Exception e) { }
        try { this.setdMontoInteresMoratorio(rs.getDouble("montointeresmoratorio")); }
        catch(Exception e) { }
        try { this.setdMontoInteresPunitorio(rs.getDouble("montointerespunitorio")); }
        catch(Exception e) { }
        try { this.setdEstado(rs.getString("estado")); }
        catch(Exception e) { }
        try { this.setdOrden(rs.getInt("orden")); }
        catch(Exception e) { }
        return this;
    }

    public void setcIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setcIdMovimientoCancela(int iidMovimientoCancela) {
        this.iidMovimientoCancela = iidMovimientoCancela;
    }

    public void setcIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setcCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setcTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setcFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setcTasaInteresMoratorio(double utasaInteresMoratorio) {
        this.utasaInteresMoratorio = utasaInteresMoratorio;
    }

    public void setcTasaInteresPunitorio(double utasaInteresPunitorio) {
        this.utasaInteresPunitorio = utasaInteresPunitorio;
    }

    public void setcPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setcNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setcIdImpuesto(int iidImpuesto) {
        this.iidImpuesto = iidImpuesto;
    }

    public void setcTasaImpuesto(double utasaImpuesto) {
        this.utasaImpuesto = utasaImpuesto;
    }

    public void setcTotalCapital(double utotalCapital) {
        this.utotalCapital = utotalCapital;
    }

    public void setcTotalInteres(double utotalInteres) {
        this.utotalInteres = utotalInteres;
    }

    public void setcTotalAtraso(double utotalAtraso) {
        this.utotalAtraso = utotalAtraso;
    }

    public void setcTotalSaldo(double utotalSaldo) {
        this.utotalSaldo = utotalSaldo;
    }

    public void setcTotalExoneracion(double utotalExoneracion) {
        this.utotalExoneracion = utotalExoneracion;
    }

    public void setcTotalMes(double utotalMes) {
        this.utotalMes = utotalMes;
    }

    public void setcTotalCobro(double utotalCobro) {
        this.utotalCobro = utotalCobro;
    }

    public void setcTotalAplicado(double utotalAplicado) {
        this.utotalAplicado = utotalAplicado;
    }

    public void setcTotalCancelar(double utotalCancelar) {
        this.utotalCancelar = utotalCancelar;
    }

    public void setcPrioridad(String sprioridad) {
        this.sprioridad = sprioridad;
    }

    public void setcTasaExoneracion(double utasaExoneracion) {
        this.utasaExoneracion = utasaExoneracion;
    }

    public void setcResumenCuota(String sresumenCuota) {
        this.sresumenCuota = sresumenCuota;
    }

    public void setcCancela(boolean bcancela) {
        this.bcancela = bcancela;
    }
    //-------------
    public void setdId(int iid) {
        this.iid = iid;
    }
    
    public void setdIdDetalleOperacion(int iidDetalleOperacion) {
        this.iidDetalleOperacion = iidDetalleOperacion;
    }

    public void setdCuota(int icuota) {
        this.icuota = icuota;
    }

    public void setdFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setdMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setdMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setdMontoSaldo(double umontoSaldo) {
        this.umontoSaldo = umontoSaldo;
    }

    public void setdMontoExoneracion(double umontoExoneracion) {
        this.umontoExoneracion = umontoExoneracion;
    }

    public void setdMontoCobro(double umontoCobro) {
        this.umontoCobro = umontoCobro;
    }

    public void setdMontoAplicado(double umontoAplicado) {
        this.umontoAplicado = umontoAplicado;
    }

    public void setdMontoInteresMoratorio(double umontoInteresMoratorio) {
        this.umontoInteresMoratorio = umontoInteresMoratorio;
    }

    public void setdMontoInteresPunitorio(double umontoInteresPunitorio) {
        this.umontoInteresPunitorio = umontoInteresPunitorio;
    }

    public void setdEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setdOrden(int iorden) {
        this.iorden = iorden;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getcIdMovimiento() {
        return this.iidMovimiento;
    }

    public int getcIdMovimientoCancela() {
        return this.iidMovimientoCancela;
    }

    public int getcIdCuenta() {
        return this.iidCuenta;
    }

    public String getcCuenta() {
        return this.scuenta;
    }

    public String getcTabla() {
        return this.stabla;
    }

    public java.util.Date getcFechaOperacion() {
        return this.dfechaOperacion;
    }

    public double getcTasaInteresMoratorio() {
        return this.utasaInteresMoratorio;
    }

    public double getcTasaInteresPunitorio() {
        return this.utasaInteresPunitorio;
    }

    public int getcPlazo() {
        return this.iplazo;
    }

    public int getcNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getcIdImpuesto() {
        return this.iidImpuesto;
    }

    public double getcTasaImpuesto() {
        return this.utasaImpuesto;
    }

    public double getcTotalCapital() {
        return this.utotalCapital;
    }

    public double getcTotalInteres() {
        return this.utotalInteres;
    }

    public double getcTotalAtraso() {
        return this.utotalAtraso;
    }

    public double getcTotalSaldo() {
        return this.utotalSaldo;
    }

    public double getcTotalExoneracion() {
        return this.utotalExoneracion;
    }

    public double getcTotalMes() {
        return this.utotalMes;
    }

    public double getcTotalCobro() {
        return this.utotalCobro;
    }

    public double getcTotalAplicado() {
        return this.utotalAplicado;
    }

    public double getcTotalCancelar() {
        return this.utotalCancelar;
    }

    public String getcPrioridad() {
        return this.sprioridad;
    }

    public double getcTasaExoneracion() {
        return this.utasaExoneracion;
    }

    public String getcResumenCuota() {
        return this.sresumenCuota;
    }

    public boolean getcCancela() {
        return this.bcancela;
    }
    //---------------
    public int getdId() {
        return this.iid;
    }
    
    public int getdIdDetalleOperacion() {
        return this.iidDetalleOperacion;
    }

    public int getdCuota() {
        return this.icuota;
    }

    public java.util.Date getdFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getdMontoCapital() {
        return this.umontoCapital;
    }

    public double getdMontoInteres() {
        return this.umontoInteres;
    }

    public double getdMontoSaldo() {
        return this.umontoSaldo;
    }

    public double getdMontoExoneracion() {
        return this.umontoExoneracion;
    }

    public double getdMontoCobro() {
        return this.umontoCobro;
    }

    public double getdMontoAplicado() {
        return this.umontoAplicado;
    }

    public double getdMontoInteresMoratorio() {
        return this.umontoInteresMoratorio;
    }

    public double getdMontoInteresPunitorio() {
        return this.umontoInteresPunitorio;
    }

    public String getdEstado() {
        return this.sestado;
    }

    public int getdOrden() {
        return this.iorden;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getdMontoCapital()< 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            //this.smensaje += TEXTO_MONTO_CAPITAL;
            bvalido = false;
        }
        if (this.getdMontoInteres()< 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            //this.smensaje += TEXTO_MONTO_INTERES;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detallecancelacion("+
            this.getdId()+","+
            this.getcIdMovimiento()+","+
            this.getcIdMovimientoCancela()+","+
            this.getdIdDetalleOperacion()+","+
            this.getdCuota()+","+
            utilitario.utiFecha.getFechaGuardado(this.getdFechaVencimiento())+","+
            this.getdMontoCapital()+","+
            this.getdMontoInteres()+","+
            this.getdMontoCobro()+","+
            this.getdMontoExoneracion()+","+
            utilitario.utiCadena.getTextoGuardado("")+","+
            utilitario.utiCadena.getTextoGuardado(this.getdEstado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getcTabla())+","+
            this.getcNumeroOperacion()+","+
            this.getcPlazo()+","+
            utilitario.utiFecha.getFechaGuardado(this.getcFechaOperacion())+","+
            this.getcTasaInteresMoratorio()+","+
            this.getcIdCuenta()+","+
            this.getcCancela()+","+
            this.getdMontoInteresMoratorio()+","+
            utilitario.utiCadena.getTextoGuardado("")+","+
            this.getdMontoInteresPunitorio()+","+
            this.getEstadoRegistro()+")";
    }
    
    public String getConsulta(java.util.Date dfecha, int iidSocio, int iidMutual, double utasaInteresPunitorio, int iidInteres, String sinteres, String sprioridad, int iidEstadoOrdenCredito, int iidEstadoPrestamo, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 0 AS id, mo.id AS idmovimiento, 0 AS idmovimientocancela, mo.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+utasaInteresPunitorio+" AS tasainterespunitorio, mo.tasainteres AS tasainteresmoratorio, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.idimpuesto, i.tasa AS tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoOrdenCredito+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardado(utilitario.utiFecha.getUltimoDia(dfecha))+") AS ci ON dop.id=ci.iddetalleoperacion "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 0 AS id, mo.id AS idmovimiento, 0 AS idmovimientocancela, mo.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+utasaInteresPunitorio+" AS tasainterespunitorio, mo.tasainteres AS tasainteresmoratorio, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.idimpuesto, i.tasa AS tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoPrestamo+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardado(utilitario.utiFecha.getUltimoDia(dfecha))+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta=mo.idcuenta "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 0 AS id, mo.id AS idmovimiento, 0 AS idmovimientocancela, "+iidInteres+" AS idcuenta, '"+sinteres+"' AS cuenta, '"+sprioridad+"' AS prioridad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+utasaInteresPunitorio+" AS tasainterespunitorio, mo.tasainteres AS tasainteresmoratorio, mo.plazoaprobado AS plazo, mo.numerooperacion, mo.idimpuesto, i.tasa AS tasaimpuesto, mo.montoaprobado AS totalcapital, mo.montointeres AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, ci.saldo AS montocobro, tc.exoneracion AS tasaexoneracion "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoPrestamo+") AS mo LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito=tc.id "+
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardado(utilitario.utiFecha.getUltimoDia(dfecha))+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta="+iidInteres+" "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 0 AS id, ap.id AS idmovimiento, 0 AS idmovimientocancela, ap.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, ap.fechaoperacion, 0 AS tasainterespunitorio, 0 AS tasainteresmoratorio, ap.plazo, ap.numerooperacion, 0 AS idimpuesto, 0 AS tasaimpuesto, ap.importe*ap.plazo AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM ahorroprogramado WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoAhorro+") AS ap LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=ap.id "+
                       "LEFT JOIN cuenta AS c ON ap.idcuenta=c.id "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardado(utilitario.utiFecha.getUltimoDia(dfecha))+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta=ap.idcuenta "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='ap' "+
                   "UNION " +
                   "SELECT 0 AS id, of.id AS idmovimiento, 0 AS idmovimientocancela, of.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, of.fechaoperacion, 0 AS tasainterespunitorio, 0 AS tasainteresmoratorio, of.plazo, of.numerooperacion, 0 AS idimpuesto, 0 AS tasaimpuesto, of.importe AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM operacionfija WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoOperacion+") AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardado(utilitario.utiFecha.getUltimoDia(dfecha))+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta=of.idcuenta "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='of' "+
                   "UNION " +
                   "SELECT 0 AS id, js.id AS idmovimiento, 0 AS idmovimientocancela, js.idcuenta, c.descripcion AS cuenta, c.prioridad, dop.tabla, js.fechaoperacion, 0 AS tasainterespunitorio, 0 AS tasainteresmoratorio, 0 AS plazo, js.numerooperacion, 0 AS idimpuesto, 0 AS tasaimpuesto, js.importetitular+js.importeadherente*js.cantidadadherente AS totalcapital, 0 AS totalinteres, dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, ci.saldo AS montocobro, 0 AS tasaexoneracion "+
                       "FROM (SELECT * FROM fondojuridicosepelio WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoFondo+") AS js LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=js.id "+
                       "LEFT JOIN cuenta AS c ON js.idcuenta=c.id "+
                       "LEFT JOIN (SELECT * FROM cierre WHERE fechacierre="+utilitario.utiFecha.getFechaGuardado(utilitario.utiFecha.getUltimoDia(dfecha))+") AS ci ON dop.id=ci.iddetalleoperacion AND ci.idcuenta=js.idcuenta "+
                       "WHERE dop.saldocapital+dop.saldointeres>0 AND dop.tabla='js' "+
               ") AS detalle "+sorden;


            /*       "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, mo.idsocio, mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion, mo.montoaprobado AS monto,mo.plazoaprobado AS plazo, mo.tasainteres, mo.idestado, mo.idcuenta, c.descripcion AS cuenta, mo.idimpuesto, i.tasa AS tasaImpuesto "+
                       "FROM detalleoperacion as dop LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id LEFT JOIN cuenta AS c ON mo.idcuenta=c.id LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoOrdenCredito+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, 0 AS saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, mo.idsocio, mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion, mo.montoaprobado AS monto,mo.plazoaprobado AS plazo, mo.tasainteres, mo.idestado, mo.idcuenta, c.descripcion AS cuenta, mo.idimpuesto, i.tasa AS tasaImpuesto "+
                       "FROM detalleoperacion as dop LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id LEFT JOIN cuenta AS c ON mo.idcuenta=c.id LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldointeres AS montosaldo, dop.saldointeres, 0 AS saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, mo.idsocio, mo.idmoneda, mo.numerooperacion, mo.fechaaprobado AS fechaoperacion, mo.montoaprobado AS monto,mo.plazoaprobado AS plazo, mo.tasainteres, mo.idestado, "+iidInteres+" AS idcuenta, '"+sinteres+"' AS cuenta, mo.idimpuesto, i.tasa AS tasaImpuesto "+
                       "FROM detalleoperacion as dop LEFT JOIN movimiento AS mo ON dop.idmovimiento=mo.id LEFT JOIN cuenta AS c ON mo.idcuenta=c.id LEFT JOIN impuesto AS i ON mo.idimpuesto=i.id WHERE dop.tabla='mo' AND mo.idestado="+iidEstadoPrestamo+" AND mo.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, ap.idsocio, ap.idmoneda, ap.numerooperacion, ap.fechaoperacion, ap.importe AS monto, ap.plazo,ap.tasainteres, ap.idestado, ap.idcuenta, c.descripcion AS cuenta, 0 AS idimpuesto, 0 AS tasaImpuesto "+
                       "FROM detalleoperacion AS dop LEFT JOIN ahorroprogramado AS ap ON dop.idmovimiento=ap.id LEFT JOIN cuenta AS c ON ap.idcuenta=c.id WHERE dop.tabla='ap' AND ap.idestado="+iidEstadoAhorro+" AND ap.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, of.idsocio, of.idmoneda, of.numerooperacion, of.fechaoperacion, of.importe AS monto, of.plazo,of.tasainteres, of.idestado, of.idcuenta, c.descripcion AS cuenta, 0 AS idimpuesto, 0 AS tasaImpuesto "+ 
                       "FROM detalleoperacion AS dop LEFT JOIN operacionfija AS of ON dop.idmovimiento=of.id LEFT JOIN cuenta AS c ON of.idcuenta=c.id WHERE dop.tabla='of' AND of.idestado="+iidEstadoOperacion+" AND of.idsocio="+iidSocio+" "+
                   "UNION " +
                   "SELECT dop.id AS iddetalleoperacion, dop.numerocuota AS cuota, dop.fechavencimiento, dop.montocapital, dop.montointeres, dop.saldocapital AS montosaldo, dop.saldocapital, dop.saldointeres, dop.giraduria AS enviado, dop.idmovimiento, dop.tabla, js.idsocio, js.idmoneda, js.numerooperacion, js.fechaoperacion, js.importetitular+js.importeadherente*js.cantidadadherente AS monto, 0 AS plazo, 0.0 AS tasainteres, js.idestado, js.idcuenta, c.descripcion AS cuenta, 0 AS idimpuesto, 0 AS tasaImpuesto "+
                       "FROM detalleoperacion AS dop LEFT JOIN fondojuridicosepelio AS js ON dop.idmovimiento=js.id LEFT JOIN cuenta AS c ON js.idcuenta=c.id WHERE dop.tabla='js' AND js.idestado="+iidEstadoFondo+" AND js.idsocio="+iidSocio+" "+
               ") AS detalle ORDER BY tabla, numerooperacion, idcuenta DESC, cuota";*/
    }
    
}
