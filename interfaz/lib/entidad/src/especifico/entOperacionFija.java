/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entOperacionFija {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String sapellidoNombre;
    private int inumeroOperacion;
    private int iidCuenta;
    private String scuenta;
    private int iidEstadoOperacionFija;
    private String sestadoOperacionFija;
    private double dimporte;
    private double dimporteTotal;
    private double dtasaInteres;
    private int iidMoneda;
    private String smoneda;
    private int iplazo;
    private java.util.Date dfechaOperacion;
    private int iidPromotor;
    private String spromotor;
    private java.util.Date dfechaPrimerVencimiento;
    private java.util.Date dfechaVencimiento;
    private String sobservacion;
    private int iidRegional;
    private String sregional;
    private int inumeroCuota;
    private int ireferencia;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    private java.util.Date dfechaDesactivado;
    private String sobservacionDesactivado;
    
    private java.util.Date dfechaCumplido;
    private String sobservacionCumplido;
    
    private short hestadoRegistro;
    private String smensaje;

    public static final int INICIO_DIA_VENCIMIENTO = 0;
    public static final int FIN_DIA_VENCIMIENTO = 31;
    public static final int LONGITUD_OBSERVACION = 100;
    public static final int LONGITUD_NUMERO_DECLARACION = 12;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ESTADO = "Estado de Operación Fija (no editable)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación Fija (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_CUENTA = "Cuenta (no vacía)";
    public static final String TEXTO_IMPORTE = "Importe (valor positivo)";
    public static final String TEXTO_IMPORTE_TOTAL = "Importe Total (valor positivo)";
    public static final String TEXTO_INTERES = "Interés (valor positivo)";
    public static final String TEXTO_FECHA_OPERACION = "Fecha de Operación (no vacía)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_PLAZO = "Plazo (valor positivo; '0' indica plazo indefinido)";
    public static final String TEXTO_FECHA_PRIMER_VENCIMIENTO = "Fecha del Primer Vencimiento (fecha de cierre)";
    public static final String TEXTO_FECHA_VENCIMIENTO = "Fecha de Vencimiento de la Operación (no editable)";
    public static final String TEXTO_OBSERVACION = "Observación (no vacía)";
    public static final String TEXTO_PROMOTOR = "Promotor (no vacío)";
    public static final String TEXTO_REGIONAL = "Regional (no vacío)";
    public static final String TEXTO_REFERENCIA = "Referencia (no editable)";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_DESACTIVADO = "Fecha de Desactivado (no editable)";
    public static final String TEXTO_OBSERVACION_DESACTIVADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_CUMPLIDO = "Fecha de Cumplimiento (no editable)";
    public static final String TEXTO_OBSERVACION_CUMPLIDO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    
    public entOperacionFija() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.sapellidoNombre = "";
        this.inumeroOperacion = 0;
        this.iidCuenta = 0;
        this.scuenta = "";
        this.iidEstadoOperacionFija = 0;
        this.sestadoOperacionFija = "";
        this.dimporte = 0.0;
        this.dimporteTotal = 0.0;
        this.dtasaInteres = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iplazo = 0;
        this.dfechaOperacion = null;
        this.iidPromotor = 0;
        this.spromotor = "";
        this.dfechaPrimerVencimiento = null;
        this.dfechaVencimiento = null;
        this.sobservacion = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.ireferencia = 0;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.dfechaDesactivado = null;
        this.sobservacionDesactivado = "";
        this.dfechaCumplido = null;
        this.sobservacionCumplido = "";
        this.inumeroCuota = 1; // predefinido en 1
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entOperacionFija(int iid, int iidSocio, String scedula, String sapellidoNombre, int inumeroOperacion, int iidCuenta, String scuenta, int iidEstadoOperacionFija, String sestadoOperacionFija, double dimporte, double dimporteTotal, double dtasaInteres, int iidMoneda, String smoneda, int iplazo, java.util.Date dfechaOperacion, int iidPromotor, String spromotor, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, String sobservacion, int iidRegional, String sregional, int ireferencia, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaDesactivado, String sobservacionDesactivado, java.util.Date dfechaCumplido, String sobservacionCumplido, int inumeroCuota, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.inumeroOperacion = inumeroOperacion;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.iidEstadoOperacionFija = iidEstadoOperacionFija;
        this.sestadoOperacionFija = sestadoOperacionFija;
        this.dimporte = dimporte;
        this.dimporteTotal = dimporteTotal;
        this.dtasaInteres = dtasaInteres;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.sobservacion = sobservacion;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.ireferencia = ireferencia;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaDesactivado = dfechaDesactivado;
        this.sobservacionDesactivado = sobservacionDesactivado;
        this.dfechaCumplido = dfechaCumplido;
        this.sobservacionCumplido = sobservacionCumplido;
        this.inumeroCuota = inumeroCuota;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, String sapellidoNombre, int inumeroOperacion, int iidCuenta, String scuenta, int iidEstadoOperacionFija, String sestadoOperacionFija, double dimporte, double dimporteTotal, double dtasaInteres, int iidMoneda, String smoneda, int iplazo, java.util.Date dfechaOperacion, int iidPromotor, String spromotor, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, String sobservacion, int iidRegional, String sregional, int ireferencia, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaDesactivado, String sobservacionDesactivado, java.util.Date dfechaCumplido, String sobservacionCumplido, int inumeroCuota, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.inumeroOperacion = inumeroOperacion;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.iidEstadoOperacionFija = iidEstadoOperacionFija;
        this.sestadoOperacionFija = sestadoOperacionFija;
        this.dimporte = dimporte;
        this.dimporteTotal = dimporteTotal;
        this.dtasaInteres = dtasaInteres;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.sobservacion = sobservacion;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.ireferencia = ireferencia;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaDesactivado = dfechaDesactivado;
        this.sobservacionDesactivado = sobservacionDesactivado;
        this.dfechaCumplido = dfechaCumplido;
        this.sobservacionCumplido = sobservacionCumplido;
        this.inumeroCuota = inumeroCuota;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entOperacionFija copiar(entOperacionFija destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getApellidoNombre(), this.getNumeroOperacion(), this.getIdCuenta(), this.getCuenta(), this.getIdEstadoOperacionFija(), this.getEstadoOperacionFija(), this.getImporte(), this.getImporteTotal(), this.getTasaInteres(), this.getIdMoneda(), this.getMoneda(), this.getPlazo(), this.getFechaOperacion(), this.getIdPromotor(), this.getPromotor(), this.getFechaPrimerVencimiento(), this.getFechaVencimiento(), this.getObservacion(), this.getIdRegional(), this.getRegional(), this.getReferencia(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getFechaDesactivado(), this.getObservacionDesactivado(), this.getFechaCumplido(), this.getObservacionCumplido(), this.getNumeroCuota(), this.getEstadoRegistro());
        return destino;
    }
    
    public entOperacionFija cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setApellidoNombre(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setIdEstadoOperacionFija(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstadoOperacionFija(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setImporte(rs.getDouble("importe")); }
        catch(Exception e) {}
        try { this.setImporteTotal(rs.getDouble("importetotal")); }
        catch(Exception e) {}
        try { this.setTasaInteres(rs.getDouble("interes")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setIdPromotor(rs.getInt("idpromotor")); }
        catch(Exception e) {}
        try { this.setPromotor(rs.getString("promotor")); }
        catch(Exception e) {}
        try { this.setFechaPrimerVencimiento(rs.getDate("fechaprimervencimiento")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setReferencia(rs.getInt("referencia")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setFechaDesactivado(rs.getDate("fechadesactivado")); }
        catch(Exception e) {}
        try { this.setObservacionDesactivado(rs.getString("observaciondesactivado")); }
        catch(Exception e) {}
        try { this.setFechaCumplido(rs.getDate("fechacumplido")); }
        catch(Exception e) {}
        try { this.setObservacionCumplido(rs.getString("observacioncumplido")); }
        catch(Exception e) {}
        try { this.setNumeroCuota(rs.getInt("numerocuota")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setApellidoNombre(String sapellidoNombre) {
        this.sapellidoNombre = sapellidoNombre;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setIdEstadoOperacionFija(int iidEstadoOperacionFija) {
        this.iidEstadoOperacionFija = iidEstadoOperacionFija;
    }

    public void setEstadoOperacionFija(String sestadoOperacionFija) {
        this.sestadoOperacionFija = sestadoOperacionFija;
    }

    public void setImporte(double dimporte) {
        this.dimporte = dimporte;
    }

    public void setImporteTotal(double dimporteTotal) {
        this.dimporteTotal = dimporteTotal;
    }

    public void setTasaInteres(double dtasaInteres) {
        this.dtasaInteres = dtasaInteres;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }

    public void setPromotor(String spromotor) {
        this.spromotor = spromotor;
    }

    public void setFechaPrimerVencimiento(java.util.Date dfechaPrimerVencimiento) {
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setFechaDesactivado(java.util.Date dfechaDesactivado) {
        this.dfechaDesactivado = dfechaDesactivado;
    }

    public void setObservacionDesactivado(String sobservacionDesactivado) {
        this.sobservacionDesactivado = sobservacionDesactivado;
    }

    public void setFechaCumplido(java.util.Date dfechaCumplido) {
        this.dfechaCumplido = dfechaCumplido;
    }

    public void setObservacionCumplido(String sobservacionCumplido) {
        this.sobservacionCumplido = sobservacionCumplido;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setReferencia(int ireferencia) {
        this.ireferencia = ireferencia;
    }

    public void setNumeroCuota(int inumeroCuota) {
        this.inumeroCuota = inumeroCuota;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getApellidoNombre() {
        return this.sapellidoNombre;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public int getIdEstadoOperacionFija() {
        return this.iidEstadoOperacionFija;
    }

    public String getEstadoOperacionFija() {
        return this.sestadoOperacionFija;
    }

    public double getImporte() {
        return this.dimporte;
    }

    public double getImporteTotal() {
        return this.dimporteTotal;
    }

    public double getTasaInteres() {
        return this.dtasaInteres;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public String getPromotor() {
        return this.spromotor;
    }

    public java.util.Date getFechaPrimerVencimiento() {
        return this.dfechaPrimerVencimiento;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public java.util.Date getFechaDesactivado() {
        return this.dfechaDesactivado;
    }

    public String getObservacionDesactivado() {
        return this.sobservacionDesactivado;
    }

    public java.util.Date getFechaCumplido() {
        return this.dfechaCumplido;
    }

    public String getObservacionCumplido() {
        return this.sobservacionCumplido;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public int getReferencia() {
        return this.ireferencia;
    }

    public int getNumeroCuota() {
        return this.inumeroCuota;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdCuenta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CUENTA;
            bvalido = false;
        }
        if (this.getImporteTotal()>0) { // importe total, el plazo debe ser mayor a 0
            if (this.getPlazo()==0) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += TEXTO_PLAZO;
                bvalido = false;
            }
        }
        if (this.getImporte()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPORTE;
            bvalido = false;
        }
        if ((this.getImporteTotal()<0 && this.getImporte()<0) || (this.getImporteTotal()>0 && this.getImporte()>0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Importe Total o Importe Mensual deben contener valores positivos";
            bvalido = false;
        }
        if (this.getPlazo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getFechaOperacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getIdPromotor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PROMOTOR;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoGenerar(int icantidadSocio) {
        boolean bvalido = true;
        this.smensaje = "";
        if (icantidadSocio<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getIdCuenta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CUENTA;
            bvalido = false;
        }
        if (this.getImporteTotal()>0) { // importe total, el plazo debe ser mayor a 0
            if (this.getPlazo()==0) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += TEXTO_PLAZO;
                bvalido = false;
            }
        }
        if (this.getImporte()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPORTE;
            bvalido = false;
        }
        if (this.getImporteTotal()<0 && this.getImporte()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Importe Total o Importe Mensual deben contener valores positivos";
            bvalido = false;
        }
        if (this.getPlazo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getFechaOperacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getIdPromotor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PROMOTOR;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoDesactivar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionDesactivado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_DESACTIVADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT operacionfija("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdCuenta()+","+
            this.getIdEstadoOperacionFija()+","+
            this.getImporte()+","+
            this.getTasaInteres()+","+
            this.getIdMoneda()+","+
            this.getPlazo()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaOperacion())+","+
            this.getIdPromotor()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaPrimerVencimiento())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaVencimiento())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaDesactivado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionDesactivado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaCumplido())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionCumplido())+","+
            this.getIdRegional()+","+
            this.getNumeroOperacion()+","+
            this.getImporteTotal()+","+
            this.getReferencia()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Apellido y Nombre", "apellidonombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Cuenta", "cuenta", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Número Operación", "numerooperacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(5, "Fecha Operación", "fechaoperacion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(6, "Fecha Primer Vencimiento", "fechaprimervencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(7, "Fecha Vencimiento", "fechavencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(8, "Estado", "estado", generico.entLista.tipo_texto));
        return lst;
    }

}
