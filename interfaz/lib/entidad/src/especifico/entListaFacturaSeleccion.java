/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entListaFacturaSeleccion {
    
    private int iid;
    private String sdescripcion;
    private String snumero;
    private int ioperacion;
    private java.util.Date dfecha;
    private Double uimporte;
    private Double uexenta;
    private Double ugravada10;
    private Double ugravada5;
    private String stabla;
    private boolean bacepta;
    
    public static final int LONGITUD_DESCRIPCION = 50;

    public static final String TEXTO_TEXTO = "Texto a buscar";

    public static final String TEXTO_DESCRIPCION = "Tipo";
    public static final String TEXTO_NUMERO = "Nro. Factura";
    public static final String TEXTO_OPERACION = "Nro. Operación";
    public static final String TEXTO_FECHA = "Fecha";
    public static final String TEXTO_IMPORTE = "Importe";
    public static final String TEXTO_EXENTA = "Exenta";
    public static final String TEXTO_GRAVADA10 = "Gravada 10";
    public static final String TEXTO_GRAVADA5 = "Gravada 5";
    public static final String TEXTO_ACEPTA = "Acepta";

    public entListaFacturaSeleccion() {
        this.iid = 0;
        this.sdescripcion = "";
        this.snumero = "";
        this.ioperacion = 0;
        this.dfecha = null;
        this.uimporte = 0.0;
        this.uexenta = 0.0;
        this.ugravada10 = 0.0;
        this.ugravada5 = 0.0;
        this.stabla = "";
        this.bacepta = false;
    }

    public entListaFacturaSeleccion(int iid, String sdescripcion, String snumero, int ioperacion, java.util.Date dfecha, Double uimporte, Double uexenta, Double ugravada10, Double ugravada5, String stabla, boolean bacepta) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.snumero = snumero;
        this.ioperacion = ioperacion;
        this.dfecha = dfecha;
        this.uimporte = uimporte;
        this.uexenta = uexenta;
        this.ugravada10 = ugravada10;
        this.ugravada5 = ugravada5;
        this.stabla = stabla;
        this.bacepta = bacepta;
    }

    public void setEntidad(int iid, String sdescripcion, String snumero, int ioperacion, java.util.Date dfecha, Double uimporte,  Double uexenta, Double ugravada10, Double ugravada5, String stabla, boolean bacepta) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.snumero = snumero;
        this.ioperacion = ioperacion;
        this.dfecha = dfecha;
        this.uimporte = uimporte;
        this.uexenta = uexenta;
        this.ugravada10 = ugravada10;
        this.ugravada5 = ugravada5;
        this.stabla = stabla;
        this.bacepta = bacepta;
    }

    public entListaFacturaSeleccion copiar(entListaFacturaSeleccion destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getNumero(), this.getOperacion(), this.getFecha(), this.getImporte(), this.getExenta(), this.getGravada10(), this.getGravada5(), this.getTabla(), this.getAcepta());
        return destino;
    }
    
    public entListaFacturaSeleccion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setNumero(rs.getString("numerofactura")); }
        catch(Exception e) {}
        try { this.setOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setImporte(rs.getDouble("importe")); }
        catch(Exception e) {}
        try { this.setExenta(rs.getDouble("exenta")); }
        catch(Exception e) {}
        try { this.setGravada10(rs.getDouble("gravada10")); }
        catch(Exception e) {}
        try { this.setGravada5(rs.getDouble("gravada5")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setAcepta(false); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setNumero(String snumero) {
        this.snumero = snumero;
    }
    
    public void setOperacion(int ioperacion) {
        this.ioperacion = ioperacion;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }
    
    public void setImporte(Double uimporte) {
        this.uimporte = uimporte;
    }
    
    public void setExenta(Double uexenta) {
        this.uexenta = uexenta;
    }
    
    public void setGravada10(Double ugravada10) {
        this.ugravada10 = ugravada10;
    }
    
    public void setGravada5(Double ugravada5) {
        this.ugravada5 = ugravada5;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setAcepta(boolean bacepta) {
        this.bacepta = bacepta;
    }
    
    public int getId() {
        return this.iid;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public String getNumero() {
        return this.snumero;
    }
    
    public int getOperacion() {
        return this.ioperacion;
    }
    
    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public Double getImporte() {
        return this.uimporte;
    }
    
    public Double getExenta() {
        return this.uexenta;
    }
    
    public Double getGravada10() {
        return this.ugravada10;
    }
    
    public Double getGravada5() {
        return this.ugravada5;
    }
    
    public String getTabla() {
        return this.stabla;
    }
    
    public boolean getAcepta() {
        return this.bacepta;
    }
    
    public String getConsulta(boolean esSocio, int iidPersona, String stexto) {
        return "SELECT f.id, f.fechafactura as fecha, f.local||'-'||f.puntoexpedicion||'-'||SUBSTR((f.numerofactura+100000000)::text,2,8) as numerofactura, " +
            " 'FACTURA '||st.descripcion||' - IMPORTE: '||f.total::numeric(18) AS descripcion, f.idpersona, f.idoperacion as numerooperacion, f.total::numeric(18) as importe, 'mo' as tabla, det.exenta::numeric(18), det.gravada10::numeric(18), det.gravada5::numeric(18) " +
            " FROM facturaingreso f" +
            " LEFT JOIN (SELECT idfacturaingreso, SUM(exenta) AS exenta, SUM(gravada10) AS gravada10, SUM(gravada5) AS gravada5 " +
            "	FROM detallefacturaingreso " +
            "	GROUP BY idfacturaingreso " +
            "	) det ON det.idfacturaingreso = f.id" +    
            " LEFT JOIN subtipo st ON st.id = f.idtipofactura" +
            " WHERE f.idestado = 146" +
            " AND f.numerofactura = "+stexto+" AND f.socio = "+esSocio+" AND f.idpersona = "+iidPersona +
            " ORDER BY f.local||'-'||f.puntoexpedicion||'-'||f.numerofactura";
    }    
}
