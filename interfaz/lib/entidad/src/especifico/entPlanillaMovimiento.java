/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPlanillaMovimiento extends generico.entGenerica {

    protected int inumeroPlanilla;
    protected int iidTipoMovimiento;
    protected String stipoMovimiento;
    protected String sdestinatario;
    protected String sremitente;
    protected int iidRegional;
    protected String sregional;
    protected String sobservacion;
    protected java.util.Date dfechaApertura;
    protected String shoraApertura;
    protected java.util.Date dfechaCierre;
    protected String shoraCierre;
    protected boolean bactivo;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final int LONGITUD_DESTINATARIO = 80;
    public static final int LONGITUD_REMITENTE = 80;
    public static final int LONGITUD_OBSERVACION = 200;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Planilla de Movimiento (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_NUMERO_PLANILLA = "Número de Planilla (no editable)";
    public static final String TEXTO_TIPO_MOVIMIENTO = "Tipo de Movimiento de Planilla (no vacía)";
    public static final String TEXTO_DESTINATARIO = "Destinatario de Planilla (no vacío, hasta " + LONGITUD_DESTINATARIO + " caracteres)";
    public static final String TEXTO_REMITENTE = "Remitente de Planilla (no vacío, hasta " + LONGITUD_REMITENTE + " caracteres)";
    public static final String TEXTO_REGIONAL = "Regional de Planilla (no vacía)";
    public static final String TEXTO_OBSERVACION = "Observación de Planilla (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_APERTURA = "Fecha de apertura de Planilla (no vacía)";
    public static final String TEXTO_HORA_APERTURA = "Hora de apertura de Planilla (no vacía)";
    public static final String TEXTO_FECHA_CIERRE = "Fecha de cierre de Planilla (no editable)";
    public static final String TEXTO_HORA_CIERRE = "Hora de cierre de Planilla (no editable)";
    public static final String TEXTO_ACTIVO = "Activo (no editable)";

    public entPlanillaMovimiento() {
        super();
        this.inumeroPlanilla = 0;
        this.iidTipoMovimiento = 0;
        this.stipoMovimiento = "";
        this.sdestinatario = "";
        this.sremitente = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.sobservacion = "";
        this.dfechaApertura = null;
        this.shoraApertura = "";
        this.dfechaCierre = null;
        this.shoraCierre = "";
        this.bactivo = false;
    }

    public entPlanillaMovimiento(int iid, String sdescripcion, int inumeroPlanilla, int iidTipoMovimiento, String stipoMovimiento, String sdestinatario, String sremitente, int iidRegional, String sregional, String sobservacion, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, boolean bactivo) {
        super(iid, sdescripcion);
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidTipoMovimiento = iidTipoMovimiento;
        this.stipoMovimiento = stipoMovimiento;
        this.sdestinatario = sdestinatario;
        this.sremitente = sremitente;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.sobservacion = sobservacion;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.bactivo = bactivo;
    }

    public entPlanillaMovimiento(int iid, String sdescripcion, int inumeroPlanilla, int iidTipoMovimiento, String stipoMovimiento, String sdestinatario, String sremitente, int iidRegional, String sregional, String sobservacion, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, boolean bactivo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidTipoMovimiento = iidTipoMovimiento;
        this.stipoMovimiento = stipoMovimiento;
        this.sdestinatario = sdestinatario;
        this.sremitente = sremitente;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.sobservacion = sobservacion;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.bactivo = bactivo;
    }

    public void setEntidad(int iid, String sdescripcion, int inumeroPlanilla, int iidTipoMovimiento, String stipoMovimiento, String sdestinatario, String sremitente, int iidRegional, String sregional, String sobservacion, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidTipoMovimiento = iidTipoMovimiento;
        this.stipoMovimiento = stipoMovimiento;
        this.sdestinatario = sdestinatario;
        this.sremitente = sremitente;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.sobservacion = sobservacion;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entPlanillaMovimiento copiar(entPlanillaMovimiento destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getNumeroPlanilla(), this.getIdTipoMovimiento(), this.getTipoMovimiento(), this.getDestinatario(), this.getRemitente(), this.getIdRegional(), this.getRegional(), this.getObservacion(), this.getFechaApertura(), this.getHoraApertura(), this.getFechaCierre(), this.getHoraCierre(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }

    public entPlanillaMovimiento cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setNumeroPlanilla(rs.getInt("numeroplanilla")); }
        catch(Exception e) {}
        try { this.setIdTipoMovimiento(rs.getInt("idtipomovimiento")); }
        catch(Exception e) {}
        try { this.setTipoMovimiento(rs.getString("tipomovimiento")); }
        catch(Exception e) {}
        try { this.setDestinatario(rs.getString("destinatario")); }
        catch(Exception e) {}
        try { this.setRemitente(rs.getString("remitente")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setFechaApertura(rs.getDate("fechaapertura")); }
        catch(Exception e) {}
        try { this.setHoraApertura(rs.getString("horaapertura")); }
        catch(Exception e) {}
        try { this.setFechaCierre(rs.getDate("fechacierre")); }
        catch(Exception e) {}
        try { this.setHoraCierre(rs.getString("horacierre")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setNumeroPlanilla(int inumeroPlanilla) {
        this.inumeroPlanilla = inumeroPlanilla;
    }

    public void setIdTipoMovimiento(int iidTipoMovimiento) {
        this.iidTipoMovimiento = iidTipoMovimiento;
    }

    public void setTipoMovimiento(String stipoMovimiento) {
        this.stipoMovimiento = stipoMovimiento;
    }

    public void setDestinatario(String sdestinatario) {
        this.sdestinatario = sdestinatario;
    }

    public void setRemitente(String sremitente) {
        this.sremitente = sremitente;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setFechaApertura(java.util.Date dfechaApertura) {
        this.dfechaApertura = dfechaApertura;
    }

    public void setHoraApertura(String shoraApertura) {
        this.shoraApertura = shoraApertura;
    }

    public void setFechaCierre(java.util.Date dfechaCierre) {
        this.dfechaCierre = dfechaCierre;
    }

    public void setHoraCierre(String shoraCierre) {
        this.shoraCierre = shoraCierre;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public int getNumeroPlanilla() {
        return this.inumeroPlanilla;
    }

    public int getIdTipoMovimiento() {
        return this.iidTipoMovimiento;
    }

    public String getTipoMovimiento() {
        return this.stipoMovimiento;
    }

    public String getDestinatario() {
        return this.sdestinatario;
    }

    public String getRemitente() {
        return this.sremitente;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public java.util.Date getFechaApertura() {
        return this.dfechaApertura;
    }

    public String getHoraApertura() {
        return this.shoraApertura;
    }

    public java.util.Date getFechaCierre() {
        return this.dfechaCierre;
    }

    public String getHoraCierre() {
        return this.shoraCierre;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getNumeroPlanilla()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_PLANILLA;
            bvalido = false;
        }
        if (this.getIdTipoMovimiento()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_MOVIMIENTO;
            bvalido = false;
        }
        if (this.getDestinatario().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESTINATARIO;
            bvalido = false;
        }
        if (this.getRemitente().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REMITENTE;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        if (this.getFechaApertura()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_APERTURA;
            bvalido = false;
        }
        if (this.getHoraApertura().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_APERTURA;
            bvalido = false;
        } else {
            if (!utilitario.utiFecha.esValidoHoraLarga(this.getHoraApertura())) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Hora inválida";
                bvalido = false;
            }
        }
        return bvalido;
    }
    
    public boolean esValidoCerrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaCierre()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_CIERRE;
            bvalido = false;
        }
        if (this.getHoraCierre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_CIERRE;
            bvalido = false;
        } else {
            if (!utilitario.utiFecha.esValidoHoraLarga(this.getHoraCierre())) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Hora inválida";
                bvalido = false;
            }
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT planillamovimiento("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getNumeroPlanilla()+","+
            this.getIdTipoMovimiento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDestinatario())+","+
            utilitario.utiCadena.getTextoGuardado(this.getRemitente())+","+
            this.getIdRegional()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaApertura())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraApertura())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaCierre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraCierre())+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número Planilla", "numeroplanilla", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Tipo Movimiento", "tipomovimiento", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Fecha apertura", "fechaapertura", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(5, "Fecha cierre", "fechacierre", generico.entLista.tipo_fecha));
        return lst;
    }
    
}
