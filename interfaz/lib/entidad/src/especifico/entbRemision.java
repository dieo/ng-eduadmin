/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbRemision {

    private int iid;
    private java.util.Date dfechadesde;
    private java.util.Date dfechahasta;
    private java.util.Date dfechaenvio;
    private int iidDestino;
    private String sdestino;
    private int iidEstado;
    private String sestado;
    private int iidCourier;
    private String scourier;
    private String snroCourier;
    private int iidBancoCuenta;
    private String scodigo;
    private String sbancocuenta;
    private int iidPromotor;
    private String spromotor;
    private int iidDependencia;
    private String sdependencia;
    private double dmontoEfectivo;
    private double dmontoChequeOtro;
    private double dmontoChequePropio;
    private int inumeroBoleta;
    private int iidBancoSucursal;
    private String sbancoSucursal;
    private boolean brecibido;
    private boolean basiento;
    private int iidFilial;
    private String sfilial;
    private short hestadoRegistro;
    private String smensaje;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    public static final int LONGITUD_NOTA = 100;
    public static final int LONGITUD_RECIBIDO = 50;
    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FECHA_DESDE = "Fecha inicial para periodo seleccionado (no vacía)";
    public static final String TEXTO_FECHA_HASTA = "Rango para periodo seleccionado (no vacía)";
    public static final String TEXTO_FECHA_ENVIO = "Fecha del Envío";
    public static final String TEXTO_CODIGO = "Código de Envío";
    public static final String TEXTO_DESTINO = "Localidad y Referente Regional a que se remite";
    public static final String TEXTO_ESTADO = "Estado de Remisión (no editable)";
    public static final String TEXTO_COURIER = "Nº de Courier";
    public static final String TEXTO_BANCO_CUENTA = "Cuenta Bancaria sobre la que se opera (no vacía)";
    public static final String TEXTO_PROMOTOR = "Promotor a quien se remite.";
    public static final String TEXTO_DEPENDENCIA = "Dependencia donde se creó la remisión";
    public static final String TEXTO_MONTO_EFECTIVO = "Monto en efectivo de la operación (valor positivo)";
    public static final String TEXTO_MONTO_CHEQUE_OTRO = "Monto total de los cheques de otros bancos depositados (valor positivo)";
    public static final String TEXTO_MONTO_CHEQUE_PROPIO = "Monto total de los cheques del mismo banco depositados (valor positivo)";
    public static final String TEXTO_NOTA = "Descripción de depósito";
    public static final String TEXTO_NUMERO_BOLETA = "Número de la Boleta de depósito (no vacía)";
    public static final String TEXTO_RECIBIDO = "Confimación de Recepción.";
    public static final String TEXTO_ASIENTO = "Si fue asentado o no el depósito";
    public static final String TEXTO_FILIAL = "Filial (no vacía)";
    
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    
    
    public entbRemision() {
        this.iid = 0;
        this.dfechadesde = null;
        this.dfechahasta = null;
        this.dfechaenvio = null;
        this.iidDestino = 0;
        this.sdestino = "";
        this.iidEstado = 0;
        this.sestado = "";
        this.iidCourier = 0;
        this.scourier = "";
        this.snroCourier = "";
        this.iidBancoCuenta = 0;
        this.scodigo = "";
        this.sbancocuenta = "";
        this.iidPromotor = 0;
        this.spromotor = "";
        this.iidDependencia = 0;
        this.sdependencia = "";
        this.dmontoEfectivo = 0.0;
        this.dmontoChequeOtro = 0.0;
        this.dmontoChequePropio = 0.0;
        this.inumeroBoleta = 0;
        this.iidBancoSucursal = 0;
        this.sbancoSucursal = "";
        this.brecibido = false;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.basiento = false;
        this.iidFilial = 0;
        this.sfilial = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbRemision(int iid, java.util.Date dfechadesde, java.util.Date dfechahasta, java.util.Date dfechaenvio, int iidDestino, String sdestino, int iidEstado, String sestado, int iidCourier, int iidBancoCuenta, String scodigo, String sbancocuenta, String snroCourier, int iidPromotor,  String spromotor, int iidDependencia, String sdependencia, double dmontoEfectivo, double dmontoChequeOtro, double dmontoChequePropio, String scourier, int inumeroBoleta, int iidBancoSucursal, String sbancoSucursal, boolean brecibido, java.util.Date dfechaAnulado, String sobservacionAnulado, boolean basiento, int iidFilial, String sfilial, short hestadoRegistro) {
        this.iid = iid;
        this.dfechadesde = dfechadesde;
        this.dfechahasta = dfechahasta;
        this.dfechaenvio = dfechaenvio;
        this.iidDestino = iidDestino;
        this.sdestino = sdestino;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidCourier = iidCourier;
        this.scourier = scourier;
        this.snroCourier = snroCourier;
        this.iidBancoCuenta = iidBancoCuenta;
        this.scodigo = scodigo;
        this.sbancocuenta = sbancocuenta;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.dmontoEfectivo = dmontoEfectivo;
        this.dmontoChequeOtro = dmontoChequeOtro;
        this.dmontoChequePropio = dmontoChequePropio;
        this.inumeroBoleta = inumeroBoleta;
        this.iidBancoSucursal = iidBancoSucursal;
        this.sbancoSucursal = sbancoSucursal;
        this.brecibido = brecibido;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.basiento = basiento;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, java.util.Date dfechadesde, java.util.Date dfechahasta, java.util.Date dfechaenvio, int iidDestino, String sdestino, int iidEstado, String sestado, int iidCourier, int iidBancoCuenta, String scodigo, String sbancocuenta, String snroCourier, int iidPromotor,  String spromotor, int iidDependencia, String sdependencia, double dmontoEfectivo, double dmontoChequeOtro, double dmontoChequePropio, String scourier, int inumeroBoleta, int iidBancoSucursal, String sbancoSucursal, boolean brecibido, java.util.Date dfechaAnulado, String sobservacionAnulado, boolean basiento, int iidFilial, String sfilial, short hestadoRegistro) {
        this.iid = iid;
        this.dfechadesde = dfechadesde;
        this.dfechahasta = dfechahasta;
        this.dfechaenvio = dfechaenvio;
        this.iidDestino = iidDestino;
        this.sdestino = sdestino;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidCourier = iidCourier;
        this.scourier = scourier;
        this.snroCourier = snroCourier;
        this.iidBancoCuenta = iidBancoCuenta;
        this.scodigo = scodigo;
        this.sbancocuenta = sbancocuenta;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.dmontoEfectivo = dmontoEfectivo;
        this.dmontoChequeOtro = dmontoChequeOtro;
        this.dmontoChequePropio = dmontoChequePropio;
        this.inumeroBoleta = inumeroBoleta;
        this.iidBancoSucursal = iidBancoSucursal;
        this.sbancoSucursal = sbancoSucursal;
        this.brecibido = brecibido;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.basiento = basiento;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entbRemision copiar(entbRemision destino) {
        destino.setEntidad(this.getId(), this.getFechaDesde(), this.getFechaHasta(), this.getFechaEnvio(), this.getIdDestino(), this.getDestino(), this.getIdEstado(), this.getEstado(), this.getIdCourier(), this.getIdBancoCuenta(), this.getCodigo(), this.getBancoCuenta(), this.getNroCourier(), this.getIdPromotor(), this.getPromotor(), this.getIdDependencia(), this.getDependencia(), this.getMontoEfectivo(), this.getMontoChequeOtro(), this.getMontoChequePropio(),  this.getCourier(), this.getNumeroBoleta(), this.getIdBancoSucursal(), this.getBancoSucursal(), this.getRecibido(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getAsiento(), this.getIdFilial(), this.getFilial(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbRemision cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("idremision")); }
        catch(Exception e) {}
        try { this.setFechaDesde(rs.getDate("fechadesde")); }
        catch(Exception e) {}
        try { this.setFechaHasta(rs.getDate("fechahasta")); }
        catch(Exception e) {}
        try { this.setFechaEnvio(rs.getDate("fechaenvio")); }
        catch(Exception e) {}
        try { this.setIdDestino(rs.getInt("iddestino")); }
        catch(Exception e) {}
        try { this.setDestino(rs.getString("destino")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestadoremision")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estadoremision")); }
        catch(Exception e) {}
        //try { this.setIdCourier(rs.getInt("courrier")); }
        //catch(Exception e) {}
        try { this.setIdCourier(rs.getInt("idcourier")); }
        catch(Exception e) {}
        try { this.setCourier(rs.getString("courier")); }
        catch(Exception e) {}
        try { this.setNroCourier(rs.getString("numerocourier")); }
        catch(Exception e) {}
        //try { this.setIdBancoCuenta(rs.getInt("idbancocuenta")); }
        //catch(Exception e) {}
        try { this.setCodigo(rs.getString("codigo")); }
        catch(Exception e) {}
        //try { this.setBancoCuenta(rs.getString("cuentabanco")); }
        //catch(Exception e) {}
        //try { this.setNroCourier(rs.getString("nrocourier")); }
        //catch(Exception e) {}
        try { this.setIdPromotor(rs.getInt("idpromotor")); }
        catch(Exception e) {}
        try { this.setPromotor(rs.getString("promotor")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        //try { this.setMontoEfectivo(rs.getDouble("montoefectivo")); }
        //catch(Exception e) {}
        //try { this.setMontoChequeOtro(rs.getDouble("montochequeotro")); }
        //catch(Exception e) {}
        //try { this.setMontoChequePropio(rs.getDouble("montochequepropio")); }
        //catch(Exception e) {}
        //try { this.setCourier(rs.getString("courier")); }
        //catch(Exception e) {}
        //try { this.setNumeroBoleta(rs.getInt("numeroboleta")); }
        //catch(Exception e) {}
        //try { this.setIdBancoSucursal(rs.getInt("idbancosucursal")); }
        //catch(Exception e) {}
        //try { this.setBancoSucursal(rs.getString("bancosucursal")); }
        //catch(Exception e) {}
        try { this.setRecibido(rs.getBoolean("recibido")); }
        catch(Exception e) {}
        //try { this.setAsiento(rs.getBoolean("asiento")); }
        //catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setFechaDesde(java.util.Date dfechadesde) {
        this.dfechadesde = dfechadesde;
    }

    public void setFechaHasta(java.util.Date dfechahasta) {
        this.dfechahasta = dfechahasta;
    }

    public void setFechaEnvio(java.util.Date dfechaenvio) {
        this.dfechaenvio = dfechaenvio;
    }

    public void setIdDestino(int iidDestino) {
        this.iidDestino = iidDestino;
    }

    public void setDestino(String sdestino) {
        this.sdestino = sdestino;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setIdCourier(int iidCourier) {
        this.iidCourier = iidCourier;
    }

    public void setIdBancoCuenta(int iidBancoCuenta) {
        this.iidBancoCuenta = iidBancoCuenta;
    }
    
    public void setCodigo(String scodigo) {
        this.scodigo = scodigo;
    }

    public void setBancoCuenta(String sbancocuenta) {
        this.sbancocuenta = sbancocuenta;
    }
    
    public void setNroCourier(String snroCourier) {
        this.snroCourier = snroCourier;
    }
    
    public void setAsiento(boolean basiento) {
        this.basiento = basiento;
    }

    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }

    public void setPromotor(String spromotor) {
        this.spromotor = spromotor;
    }
    
    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }
    
    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public void setMontoEfectivo(double dmontoEfectivo) {
        this.dmontoEfectivo = dmontoEfectivo;
    }
    
    public void setMontoChequeOtro(double dmontoChequeOtro) {
        this.dmontoChequeOtro = dmontoChequeOtro;
    }
    
    public void setMontoChequePropio(double dmontoChequePropio) {
        this.dmontoChequePropio = dmontoChequePropio;
    }
    
    public void setCourier(String scourier) {
        this.scourier = scourier;
    }
    
    public void setNumeroBoleta(int inumeroBoleta) {
        this.inumeroBoleta = inumeroBoleta;
    }
    
    public void setIdBancoSucursal(int iidBancoSucursal) {
        this.iidBancoSucursal = iidBancoSucursal;
    }
    
    public void setBancoSucursal(String sbancoSucursal) {
        this.sbancoSucursal = sbancoSucursal;
    }
     
    public void setRecibido(boolean brecibido) {
        this.brecibido = brecibido;
    }
     
    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }

    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public java.util.Date getFechaDesde() {
        return this.dfechadesde;
    }
    
    public java.util.Date getFechaHasta() {
        return this.dfechahasta;
    }

    public java.util.Date getFechaEnvio() {
        return this.dfechaenvio;
    }

    public int getIdDestino() {
        return this.iidDestino;
    }

    public String getDestino() {
        return this.sdestino;
    }
    
    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }
 
    public int getIdCourier() {
        return this.iidCourier;
    }

    public int getIdBancoCuenta() {
        return this.iidBancoCuenta;
    }
    
    public String getCodigo() {
        return this.scodigo;
    }

    public String getBancoCuenta() {
        return this.sbancocuenta;
    }
    
    public String getNroCourier() {
        return this.snroCourier;
    }

    public boolean getAsiento() {
        return this.basiento;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public String getPromotor() {
        return this.spromotor;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }

    public String getDependencia() {
        return this.sdependencia;
    }

    public double getMontoEfectivo() {
        return this.dmontoEfectivo;
    }
    
    public double getMontoChequeOtro() {
        return this.dmontoChequeOtro;
    }
    
    public double getMontoChequePropio() {
        return this.dmontoChequePropio;
    }
    
    public String getCourier() {
        return this.scourier;
    }

    public int getNumeroBoleta() {
        return this.inumeroBoleta;
    }

    public int getIdBancoSucursal() {
        return this.iidBancoSucursal;
    }

    public String getBancoSucursal() {
        return this.sbancoSucursal;
    }
    
    public boolean getRecibido() {
        return this.brecibido;
    }
    
    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }

    public String getFilial() {
        return this.sfilial;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaDesde() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA_DESDE;
            bvalido = false;
        }
        if (this.getFechaHasta() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA_HASTA;
            bvalido = false;
        }
        /*if (this.getMontoEfectivo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO_EFECTIVO;
            bvalido = false;
        }        
        if (this.getMontoEfectivo()== 0 && this.getMontoChequeOtro()== 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO_EFECTIVO;
            bvalido = false;
        }*/        
        return bvalido;
    }

    public boolean esValidoEnviar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getCodigo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CODIGO;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.remision("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCodigo())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaDesde())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaHasta())+","+
            this.getIdDestino()+","+
            this.getIdPromotor()+","+
            this.getIdEstado()+","+
            this.getIdDependencia()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaEnvio())+","+
            this.getIdCourier()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNroCourier())+","+
            this.getRecibido()+","+
            this.getIdFilial()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Desde", "fechadesde", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Hasta", "fechahasta", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(3, "Número Remisión", "idremision", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(4, "Destino", "destino", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(5, "Estado", "estadoremision", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(6, "Código Envío", "codigo", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(7, "Fecha Envío", "fechaenvio", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(8, "Dependencia", "dependencia", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(9, "Nº Courier", "numerocourier", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(10, "Promotor", "promotor", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(11, "Recibido", "recibido2", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(12, "Filial", "filial", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
