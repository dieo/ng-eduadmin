/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPermisoPerfil {
    
    private int iid;
    private int iidPrograma;
    private String sprograma;
    private String scodigo;
    private String smenu;
    private int iidPerfil;
    private String sperfil;
    private boolean bitem1; //inserta
    private boolean bitem2; //modifica
    private boolean bitem3; //elimina
    private boolean bitem4; //visualiza
    private boolean bitem5; //imprime
    private boolean bitem6; //aprueba
    private boolean bitem7; //genera
    private boolean bitem8; //anula
    private boolean bitem9; //rechaza
    private short hestadoRegistro;
    private String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_PROGRAMA = "Programa (no vacío)";
    public static final String TEXTO_PERFIL = "Perfil (no vacío)";
    public static final String TEXTO_ITEM1 = "Inserta / No Inserta";
    public static final String TEXTO_ITEM2 = "Modifica / No Modifica";
    public static final String TEXTO_ITEM3 = "Elimina / No Elimina";
    public static final String TEXTO_ITEM4 = "Visualiza / No Visualiza";
    public static final String TEXTO_ITEM5 = "Imprimie / No Imprime";
    public static final String TEXTO_ITEM6 = "Aprueba / No Aprueba";
    public static final String TEXTO_ITEM7 = "Genera / No Genera";
    public static final String TEXTO_ITEM8 = "Anula / No Anula";
    public static final String TEXTO_ITEM9 = "Rechaza / No Rechaza";

    public entPermisoPerfil() {
        this.iid = 0;
        this.iidPrograma = 0;
        this.sprograma = "";
        this.scodigo = "";
        this.smenu = "";
        this.iidPerfil = 0;
        this.sperfil = "";
        this.bitem1 = false;
        this.bitem2 = false;
        this.bitem3 = false;
        this.bitem4 = false;
        this.bitem5 = false;
        this.bitem6 = false;
        this.bitem7 = false;
        this.bitem8 = false;
        this.bitem9 = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entPermisoPerfil(int iid, int iidPrograma, String sprograma, String scodigo, String smenu, int iidPerfil, String sperfil, boolean bitem1, boolean bitem2, boolean bitem3, boolean bitem4, boolean bitem5, boolean bitem6, boolean bitem7, boolean bitem8, boolean bitem9, short hestadoRegistro) {
        this.iid = iid;
        this.iidPrograma = iidPrograma;
        this.sprograma = sprograma;
        this.scodigo = scodigo;
        this.smenu = smenu;
        this.iidPerfil = iidPerfil;
        this.sperfil = sperfil;
        this.bitem1 = bitem1;
        this.bitem2 = bitem2;
        this.bitem3 = bitem3;
        this.bitem4 = bitem4;
        this.bitem5 = bitem5;
        this.bitem6 = bitem6;
        this.bitem7 = bitem7;
        this.bitem8 = bitem8;
        this.bitem9 = bitem9;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public void setEntidad(int iid, int iidPrograma, String sprograma, String scodigo, String smenu, int iidPerfil, String sperfil, boolean bitem1, boolean bitem2, boolean bitem3, boolean bitem4, boolean bitem5, boolean bitem6, boolean bitem7, boolean bitem8, boolean bitem9, short hestadoRegistro) {
        this.iid = iid;
        this.iidPrograma = iidPrograma;
        this.sprograma = sprograma;
        this.scodigo = scodigo;
        this.smenu = smenu;
        this.iidPerfil = iidPerfil;
        this.sperfil = sperfil;
        this.bitem1 = bitem1;
        this.bitem2 = bitem2;
        this.bitem3 = bitem3;
        this.bitem4 = bitem4;
        this.bitem5 = bitem5;
        this.bitem6 = bitem6;
        this.bitem7 = bitem7;
        this.bitem8 = bitem8;
        this.bitem9 = bitem9;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entPermisoPerfil copiar(entPermisoPerfil destino) {
        destino.setEntidad(this.getId(), this.getIdPrograma(), this.getPrograma(), this.getCodigo(), this.getMenu(), this.getIdPerfil(), this.getPerfil(), this.getItem1(), this.getItem2(), this.getItem3(), this.getItem4(), this.getItem5(), this.getItem6(), this.getItem7(), this.getItem8(), this.getItem9(), this.getEstadoRegistro());
        return destino;
    }

    public entPermisoPerfil cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdPrograma(rs.getInt("idprograma")); }
        catch(Exception e) {}
        try { this.setPrograma(rs.getString("programa")); }
        catch(Exception e) {}
        try { this.setCodigo(rs.getString("codigo")); }
        catch(Exception e) {}
        try { this.setMenu(rs.getString("menu")); }
        catch(Exception e) {}
        try { this.setIdPerfil(rs.getInt("idperfil")); }
        catch(Exception e) {}
        try { this.setPerfil(rs.getString("perfil")); }
        catch(Exception e) {}
        try { this.setItem1(rs.getBoolean("item1")); }
        catch(Exception e) {}
        try { this.setItem2(rs.getBoolean("item2")); }
        catch(Exception e) {}
        try { this.setItem3(rs.getBoolean("item3")); }
        catch(Exception e) {}
        try { this.setItem4(rs.getBoolean("item4")); }
        catch(Exception e) {}
        try { this.setItem5(rs.getBoolean("item5")); }
        catch(Exception e) {}
        try { this.setItem6(rs.getBoolean("item6")); }
        catch(Exception e) {}
        try { this.setItem7(rs.getBoolean("item7")); }
        catch(Exception e) {}
        try { this.setItem8(rs.getBoolean("item8")); }
        catch(Exception e) {}
        try { this.setItem9(rs.getBoolean("item9")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdPrograma(int iidPrograma) {
        this.iidPrograma = iidPrograma;
    }
    
    public void setPrograma(String sprograma) {
        this.sprograma = sprograma;
    }
    
    public void setCodigo(String scodigo) {
        this.scodigo = scodigo;
    }
    
    public void setMenu(String smenu) {
        this.smenu = smenu;
    }
    
    public void setIdPerfil(int iidPerfil) {
        this.iidPerfil = iidPerfil;
    }
    
    public void setPerfil(String sperfil) {
        this.sperfil = sperfil;
    }
    
    public void setItem1(boolean bitem1) {
        this.bitem1 = bitem1;
    }
    
    public void setItem2(boolean bitem2) {
        this.bitem2 = bitem2;
    }
    
    public void setItem3(boolean bitem3) {
        this.bitem3 = bitem3;
    }
    
    public void setItem4(boolean bitem4) {
        this.bitem4 = bitem4;
    }
    
    public void setItem5(boolean bitem5) {
        this.bitem5 = bitem5;
    }
    
    public void setItem6(boolean bitem6) {
        this.bitem6 = bitem6;
    }
    
    public void setItem7(boolean bitem7) {
        this.bitem7 = bitem7;
    }
    
    public void setItem8(boolean bitem8) {
        this.bitem8 = bitem8;
    }
    
    public void setItem9(boolean bitem9) {
        this.bitem9 = bitem9;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getIdPrograma() {
        return this.iidPrograma;
    }
    
    public String getPrograma() {
        return this.sprograma;
    }
    
    public String getCodigo() {
        return this.scodigo;
    }
    
    public String getMenu() {
        return this.smenu;
    }
    
    public int getIdPerfil() {
        return this.iidPerfil;
    }
    
    public String getPerfil() {
        return this.sperfil;
    }
    
    public boolean getItem1() {
        return this.bitem1;
    }
    
    public boolean getItem2() {
        return this.bitem2;
    }
    
    public boolean getItem3() {
        return this.bitem3;
    }
    
    public boolean getItem4() {
        return this.bitem4;
    }
    
    public boolean getItem5() {
        return this.bitem5;
    }
    
    public boolean getItem6() {
        return this.bitem6;
    }
    
    public boolean getItem7() {
        return this.bitem7;
    }
    
    public boolean getItem8() {
        return this.bitem8;
    }
    
    public boolean getItem9() {
        return this.bitem9;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdPrograma()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PROGRAMA;
            bvalido = false;
        }
        if (this.getIdPerfil()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PERFIL;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT permisoperfil("+
            this.getId()+","+
            this.getIdPerfil()+","+
            this.getIdPrograma()+","+
            this.getItem1()+","+
            this.getItem2()+","+
            this.getItem3()+","+
            this.getItem4()+","+
            this.getItem5()+","+
            this.getItem6()+","+
            this.getItem7()+","+
            this.getItem8()+","+
            this.getItem9()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
}
