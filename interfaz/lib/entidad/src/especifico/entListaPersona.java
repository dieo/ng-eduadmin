/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entListaPersona {
    
    protected int iid;
    protected String scedula;
    protected String sruc;
    protected String srazonSocial;
    private int iidPlanCuenta;
    private String snroPlanCuenta;
    private String splanCuenta;
    protected String sestado;
    protected boolean bacepta;
    
    public static final int LONGITUD_DESCRIPCION = 50;

    public static final String TEXTO_DESCRIPCION = "Texto a buscar";
    public static final String TEXTO_CEDULA = "Cédula";
    public static final String TEXTO_RUC = "RUC";
    public static final String TEXTO_RAZON_SOCIAL = "Razón Social";
    public static final String TEXTO_ACEPTA = "Acepta";

    public entListaPersona() {
        this.iid = 0;
        this.scedula = "";
        this.sruc = "";
        this.srazonSocial = "";
        this.iidPlanCuenta = 0;
        this.snroPlanCuenta = "";
        this.splanCuenta = "";
        this.sestado = "";
        this.bacepta = false;
    }

    public entListaPersona(int iid, String scedula, String sruc, String srazonSocial, int iidPlanCuenta, String snroPlanCuenta, String splanCuenta, String sestado, boolean bacepta) {
        this.iid = iid;
        this.scedula = scedula;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.splanCuenta = splanCuenta;
        this.sestado = sestado;
        this.bacepta = bacepta;
    }

    public void setEntidad(int iid, String scedula, String sruc, String srazonSocial, int iidPlanCuenta, String snroPlanCuenta, String splanCuenta, String sestado, boolean bacepta) {
        this.iid = iid;
        this.scedula = scedula;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.splanCuenta = splanCuenta;
        this.sestado = sestado;
        this.bacepta = bacepta;
    }

    public entListaPersona copiar(entListaPersona destino) {
        destino.setEntidad(this.getId(), this.getCedula(), this.getRuc(), this.getRazonSocial(), this.getIdPlanCuenta(), this.getNroPlanCuenta(), this.getPlanCuenta(), this.getEstado(), this.getAcepta());
        return destino;
    }
    
    public entListaPersona cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) {}
        try { this.setRazonSocial(rs.getString("razonsocial")); }
        catch(Exception e) {}
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) { }
        try { this.setNroPlanCuenta(rs.getString("nroplancuenta")); }
        catch(Exception e) { }
        try { this.setPlanCuenta(rs.getString("plancuenta")); }
        catch(Exception e) { }
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setAcepta(false); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setRuc(String sruc) {
        this.sruc = sruc;
    }

    public void setRazonSocial(String srazonSocial) {
        this.srazonSocial = srazonSocial.trim();
    }

    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }
    
    public void setNroPlanCuenta(String snroPlanCuenta) {
        this.snroPlanCuenta = snroPlanCuenta;
    }
    
    public void setPlanCuenta(String splanCuenta) {
        this.splanCuenta = splanCuenta;
    }
    
    public void setEstado(String sestado) {
        this.sestado = sestado.trim();
    }

    public void setAcepta(boolean bacepta) {
        this.bacepta = bacepta;
    }
    
    public int getId() {
        return this.iid;
    }

    public String getRuc() {
        return this.sruc;
    }
    
    public String getCedula() {
        return this.scedula;
    }
    
    public String getRazonSocial() {
        return this.srazonSocial;
    }

    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }
    
    public String getNroPlanCuenta() {
        return this.snroPlanCuenta;
    }
    
    public String getPlanCuenta() {
        return this.splanCuenta;
    }
    
    public String getEstado() {
        return this.sestado;
    }

    public boolean getAcepta() {
        return this.bacepta;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "RUC", "ruc", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Razón Social", "razonsocial", generico.entLista.tipo_texto));
        return lst;
    }
    
}
