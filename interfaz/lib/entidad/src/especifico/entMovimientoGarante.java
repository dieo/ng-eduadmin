/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMovimientoGarante {

    protected int iid;
    protected int iidMovimiento;
    protected int iidGarante;
    protected String scedula;
    protected String snombre;
    protected String sapellido;
    protected String sapellidoNombre;
    protected String sdireccion;
    protected int iidAnalisis;
    protected short hestadoRegistro;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_GARANTE = "Garante (no vacío)";
    
    public entMovimientoGarante() {
        this.iid = 0;
        this.iidMovimiento = 0;
        this.iidGarante = 0;
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.sapellidoNombre = "";
        this.sdireccion = "";
        this.iidAnalisis = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entMovimientoGarante(int iid, int iidMovimiento, int iidGarante, String scedula, String snombre, String sapellido, String sapellidoNombre, String sdireccion, int iidAnalisis, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidGarante = iidGarante;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.sapellidoNombre = sapellidoNombre;
        this.sdireccion = sdireccion;
        this.iidAnalisis = iidAnalisis;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidMovimiento, int iidGarante, String scedula, String snombre, String sapellido, String sapellidoNombre, String sdireccion, int iidAnalisis, short hestadoRegistro) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.iidGarante = iidGarante;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.sapellidoNombre = sapellidoNombre;
        this.sdireccion = sdireccion;
        this.iidAnalisis = iidAnalisis;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entMovimientoGarante copiar(entMovimientoGarante destino) {
        destino.setEntidad(this.getId(), this.getIdMovimiento(), this.getIdGarante(), this.getCedula(), this.getNombre(), this.getApellido(), this.getApellidoNombre(), this.getDireccion(), this.getIdAnalisis(), this.getEstadoRegistro());
        return destino;
    }

    public entMovimientoGarante cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setIdGarante(rs.getInt("idgarante")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setApellidoNombre(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setIdAnalisis(rs.getInt("idanalisis")); }
        catch(Exception e) {}
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setIdGarante(int iidGarante) {
        this.iidGarante = iidGarante;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombre(String snombre) {
        this.snombre = snombre;
    }

    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }

    public void setApellidoNombre(String sapellidoNombre) {
        this.sapellidoNombre = sapellidoNombre;
    }

    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setIdAnalisis(int iidAnalisis) {
        this.iidAnalisis = iidAnalisis;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public int getIdGarante() {
        return this.iidGarante;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombre() {
        return this.snombre;
    }

    public String getApellido() {
        return this.sapellido;
    }

    public String getApellidoNombre() {
        return this.sapellidoNombre;
    }

    public String getDireccion() {
        return this.sdireccion;
    }

    public int getIdAnalisis() {
        return this.iidAnalisis;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdGarante() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_GARANTE;
            bvalido = false;
        }
        if (esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Verifique el Garante o la Cédula sean únicos";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT movimientogarante("+
            this.getId()+","+
            this.getIdMovimiento()+","+
            this.getIdGarante()+","+
            this.getIdAnalisis()+","+
            this.getEstadoRegistro()+")";
    }

}
