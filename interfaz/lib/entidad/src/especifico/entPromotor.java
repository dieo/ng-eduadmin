/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPromotor {
    
    private int iid;
    private int iidFuncionario;
    private String sfuncionario;
    protected short hestado;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FUNCIONARIO = "Nombre y Apellido del Funcionario (no vacío)";
    
    public entPromotor() {
        this.iid = 0;
        this.iidFuncionario = 0;
        this.sfuncionario = "";
        this.hestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }
    
    public entPromotor(int iid, int iidFuncionario, String sfuncionario, short hestado) {
        this.iid = iid;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.hestado = hestado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidFuncionario, String sfuncionario, short hestado) {
        this.iid = iid;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.hestado = hestado;
        this.smensaje = "";
    }
    
    public entPromotor copiar(entPromotor destino) {
        destino.setEntidad(this.getId(), this.getIdFuncionario(), this.getFuncionario(), this.getEstadoRegistro());
        return destino;
    }
    
    public entPromotor cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        try { this.setFuncionario(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }
    
    public void setFuncionario(String sfuncionario) {
        this.sfuncionario = sfuncionario;
    }
    
    public void setEstadoRegistro(short hestado) {
        this.hestado = hestado;
    }
    
    public int getId() {
        return this.iid;
    }
    
    public int getIdFuncionario() {
        return this.iidFuncionario;
    }
    
    public String getFuncionario() {
        return this.sfuncionario;
    }
    
    public short getEstadoRegistro() {
        return this.hestado;
    }
    
    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdFuncionario() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT promotor("+
            this.getId()+","+
            this.getIdFuncionario()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Funcionario", "apellidonombre", generico.entLista.tipo_texto));
        return lst;
    }
    
}
