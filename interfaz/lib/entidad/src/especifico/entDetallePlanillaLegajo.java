/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetallePlanillaLegajo {

    private int iid;
    private int iidPlanilla;
    private String splanilla;
    private java.util.Date dfecha;
    private int iidLegajo;
    private String slegajo;
    private int iidFuncionario;
    private String sfuncionario;
    protected short hestadoRegistro;

    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_PLANILLA = "Planilla (no vacía)";
    public static final String TEXTO_FECHA = "Fecha (no vacía)";
    public static final String TEXTO_NUMERO = "Número de Orden";
    public static final String TEXTO_LEGAJO = "Legajo (no vacía)";
    public static final String TEXTO_FUNCIONARIO = "Funcionario (no vacía)";
    
    public entDetallePlanillaLegajo() {
        this.iid = 0;
        this.iidPlanilla = 0;
        this.splanilla = "";
        this.dfecha = null;
        this.iidLegajo = 0;
        this.slegajo = "";
        this.iidFuncionario = 0;
        this.sfuncionario = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetallePlanillaLegajo(int iid, int iidPlanilla, String splanilla, java.util.Date dfecha, int iidLegajo, String slegajo, int iidFuncionario, String sfuncionario, short hestadoRegistro) {
        this.iid = iid;
        this.iidPlanilla = iidPlanilla;
        this.splanilla = splanilla;
        this.dfecha = dfecha;
        this.iidLegajo = iidLegajo;
        this.slegajo = slegajo;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidPlanilla, String splanilla, java.util.Date dfecha, int iidLegajo, String slegajo, int iidFuncionario, String sfuncionario, short hestadoRegistro) {
        this.iid = iid;
        this.iidPlanilla = iidPlanilla;
        this.splanilla = splanilla;
        this.dfecha = dfecha;
        this.iidLegajo = iidLegajo;
        this.slegajo = slegajo;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entDetallePlanillaLegajo copiar(entDetallePlanillaLegajo destino) {
        destino.setEntidad(this.getId(), this.getIdPlanilla(), this.getPlanilla(), this.getFecha(), this.getIdLegajo(), this.getLegajo(), this.getIdFuncionario(), this.getFuncionario(), this.getEstadoRegistro());
        return destino;
    }

    public entDetallePlanillaLegajo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdPlanilla(rs.getInt("idplanilla")); }
        catch(Exception e) {}
        try { this.setPlanilla(rs.getString("planilla")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setIdLegajo(rs.getInt("idlegajo")); }
        catch(Exception e) {}
        try { this.setLegajo(rs.getString("legajo")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        try { this.setFuncionario(rs.getString("funcionario")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdPlanilla(int iidPlanilla) {
        this.iidPlanilla = iidPlanilla;
    }

    public void setPlanilla(String splanilla) {
        this.splanilla = splanilla;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setIdLegajo(int iidLegajo) {
        this.iidLegajo = iidLegajo;
    }

    public void setLegajo(String slegajo) {
        this.slegajo = slegajo;
    }

    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }

    public void setFuncionario(String sfuncionario) {
        this.sfuncionario = sfuncionario;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdPlanilla() {
        return this.iidPlanilla;
    }

    public String getPlanilla() {
        return this.splanilla;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public int getIdLegajo() {
        return this.iidLegajo;
    }

    public String getLegajo() {
        return this.slegajo;
    }

    public int getIdFuncionario() {
        return this.iidFuncionario;
    }

    public String getFuncionario() {
        return this.sfuncionario;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdPlanilla() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLANILLA;
            bvalido = false;
        }
        if (this.getIdLegajo()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_LEGAJO;
            bvalido = false;
        }
        if (this.getIdFuncionario()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO;
            bvalido = false;
        }
        if (esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje = "El Legajo ya existe en esta u otra Planilla";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detalleplanillalegajo("+
            this.getId()+","+
            this.getIdPlanilla()+","+
            this.getIdLegajo()+","+
            this.getIdFuncionario()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
}
