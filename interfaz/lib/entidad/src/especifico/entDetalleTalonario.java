/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleTalonario {

    protected int iid;
    protected int iidTalonario;
    protected int isecuencia;
    protected java.util.Date dfecha;
    protected String sobservacion;
    protected short hestadoRegistro;

    protected String smensaje;
    
    public static final int LONGITUD_OBSERVACION = 100;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_SECUENCIA = "Secuencia (no vacía, comprendido entre Número Inicial y Número Final)";
    public static final String TEXTO_FECHA = "Fecha (no vacía)";
    public static final String TEXTO_OBSERVACION = "Observación (hasta " + LONGITUD_OBSERVACION + " caracteres)";
    
    public entDetalleTalonario() {
        this.iid = 0;
        this.iidTalonario = 0;
        this.isecuencia = 0;
        this.dfecha = null;
        this.sobservacion = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleTalonario(int iid, int iidTalonario, int isecuencia, java.util.Date dfecha, String sobservacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidTalonario = iidTalonario;
        this.isecuencia = isecuencia;
        this.dfecha = dfecha;
        this.sobservacion = sobservacion;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidTalonario, int isecuencia, java.util.Date dfecha, String sobservacion, short hestadoRegistro) {
        this.iid = iid;
        this.iidTalonario = iidTalonario;
        this.isecuencia = isecuencia;
        this.dfecha = dfecha;
        this.sobservacion = sobservacion;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entDetalleTalonario copiar(entDetalleTalonario destino) {
        destino.setEntidad(this.getId(), this.getIdTalonario(), this.getSecuencia(), this.getFecha(), this.getObservacion(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleTalonario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdTalonario(rs.getInt("idtalonario")); }
        catch(Exception e) {}
        try { this.setSecuencia(rs.getInt("secuencia")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdTalonario(int iidTalonario) {
        this.iidTalonario = iidTalonario;
    }

    public void setSecuencia(int isecuencia) {
        this.isecuencia = isecuencia;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdTalonario() {
        return this.iidTalonario;
    }

    public int getSecuencia() {
        return this.isecuencia;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido(boolean esDuplicado, int inumeroInicial, int inumeroFinal) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getSecuencia() < inumeroInicial || this.getSecuencia() > inumeroFinal) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SECUENCIA;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        if (esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje = "El número de secuencia debe ser único";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detalletalonario("+
            this.getId()+","+
            this.getIdTalonario()+","+
            this.getSecuencia()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFecha())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
}
