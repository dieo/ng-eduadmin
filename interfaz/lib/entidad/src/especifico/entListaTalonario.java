/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entListaTalonario {
    
    protected int iid;
    protected int inumeroInicial;
    protected int inumeroFinal;
    protected java.util.Date dfechaVencimiento;
    protected int isecuencia;
    protected boolean bacepta;
    
    public static final int LONGITUD_DESCRIPCION = 50;

    public static final String TEXTO_DESCRIPCION = "Texto a buscar";

    public static final String TEXTO_CEDULA = "Cédula";
    public static final String TEXTO_NOMBRE = "Nombre";
    public static final String TEXTO_APELLIDO = "Apellido";
    public static final String TEXTO_ACEPTA = "Acepta";

    public entListaTalonario() {
        this.iid = 0;
        this.inumeroInicial = 0;
        this.inumeroFinal = 0;
        this.dfechaVencimiento = null;
        this.isecuencia = 0;
        this.bacepta = false;
    }

    public entListaTalonario(int iid, int inumeroInicial, int inumeroFinal, java.util.Date dfechaVencimiento, int isecuencia, boolean bacepta) {
        this.iid = iid;
        this.inumeroInicial = inumeroInicial;
        this.inumeroFinal = inumeroFinal;
        this.dfechaVencimiento = dfechaVencimiento;
        this.isecuencia = isecuencia;
        this.bacepta = bacepta;
    }

    public void setEntidad(int iid, int inumeroInicial, int inumeroFinal, java.util.Date dfechaVencimiento, int isecuencia, boolean bacepta) {
        this.iid = iid;
        this.inumeroInicial = inumeroInicial;
        this.inumeroFinal = inumeroFinal;
        this.dfechaVencimiento = dfechaVencimiento;
        this.isecuencia = isecuencia;
        this.bacepta = bacepta;
    }

    public entListaTalonario copiar(entListaTalonario destino) {
        destino.setEntidad(this.getId(), this.getNumeroInicial(), this.getNumeroFinal(), this.getFechaVencimiento(), this.getSecuencia(), this.getAcepta());
        return destino;
    }
    
    public entListaTalonario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setNumeroInicial(rs.getInt("numeroinicial")); }
        catch(Exception e) { }
        try { this.setNumeroFinal(rs.getInt("numerofinal")); }
        catch(Exception e) { }
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) { }
        try { this.setSecuencia(rs.getInt("secuencia")); }
        catch(Exception e) { }
        try { this.setAcepta(false); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setNumeroInicial(int inumeroInicial) {
        this.inumeroInicial = inumeroInicial;
    }
    
    public void setNumeroFinal(int inumeroFinal) {
        this.inumeroFinal = inumeroFinal;
    }
    
    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }
    
    public void setSecuencia(int isecuencia) {
        this.isecuencia = isecuencia;
    }
    
    public void setAcepta(boolean bacepta) {
        this.bacepta = bacepta;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getNumeroInicial() {
        return this.inumeroInicial;
    }
    
    public int getNumeroFinal() {
        return this.inumeroFinal;
    }
    
    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }
    
    public int getSecuencia() {
        return this.isecuencia;
    }
    
    public boolean getAcepta() {
        return this.bacepta;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
}
