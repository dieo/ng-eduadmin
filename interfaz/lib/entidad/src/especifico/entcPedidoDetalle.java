/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;
import java.util.Date;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entcPedidoDetalle {
    
    protected int iid;
    protected int iidPedido;
    protected int iidArticulo;
    protected String sdescripcion;
    protected int iidUnidadMedida;
    protected String sunidadMedida;
    protected double ucantidad;
    protected int icuotas;
    protected int iidFrecuenciaDescuento;
    protected String sfrecuenciaDescuento;
    protected java.util.Date dfechaVenc;
    protected boolean bentregado;
    protected int iidDescuento;
    protected short cestado;
    protected String smensaje;
    
    public static final int LONGITUD_DESCRIPCION = 200;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción del producto (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de Medida";
    public static final String TEXTO_ENTREGADO = "Si se entregò el pedido";
    public static final String TEXTO_ARTICULO = "Producto solicitado (no vacía)";
    public static final String TEXTO_CANTIDAD = "Cantidad solicitada del Producto (no vacía, valor positivo)";
    public static final String TEXTO_CUOTAS = "Cantidad de cuotas para abonar por el artículo (valor positivo, 0 en caso de pago al contado)";
    public static final String TEXTO_FRECUENCIA = "Frecuencia para el pago del artículo";
    public static final String TEXTO_FECHAVENC = "Fecha para el primer vencimiento del descuento";
    
    public entcPedidoDetalle() {
        this.iid = 0;
        this.iidPedido = 0;
        this.iidUnidadMedida = 0;
        this.sunidadMedida = "";
        this.icuotas = 0;
        this.iidArticulo = 0;
        this.sdescripcion = "";
        this.iidFrecuenciaDescuento = 0;
        this.iidDescuento = 0;
        this.sfrecuenciaDescuento = "";
        this.dfechaVenc= null;
        this.ucantidad = 0.0;
        this.bentregado = false;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entcPedidoDetalle(int iid, int iidPedido, int iidUnidadMedida, String sunidadMedida, int iidArticulo, String sdescripcion, double ucantidad, int icuotas, int iidFrecuenciaDescuento, String sfrecuenciaDescuento, java.util.Date dfechaVenc, boolean bentregado, int iidDescuento, short cestado) {
        this.iid = iid;
        this.iidPedido = iidPedido;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.ucantidad = ucantidad;
        this.icuotas = icuotas;
        this.iidDescuento = iidDescuento;
        this.iidFrecuenciaDescuento = iidFrecuenciaDescuento;
        this.sfrecuenciaDescuento = sfrecuenciaDescuento;
        this.dfechaVenc=dfechaVenc;
        this.bentregado = bentregado;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidPedido, int iidUnidadMedida, String sunidadMedida, int iidArticulo, String sdescripcion, double ucantidad, int icuotas, int iidFrecuenciaDescuento, String sfrecuenciaDescuento, java.util.Date dfechaVenc, boolean bentregado, int iidDescuento, short cestado) {
        this.iid = iid;
        this.iidPedido = iidPedido;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.ucantidad = ucantidad;
        this.icuotas = icuotas;
        this.iidDescuento = iidDescuento;
        this.iidFrecuenciaDescuento = iidFrecuenciaDescuento;
        this.sfrecuenciaDescuento = sfrecuenciaDescuento;
        this.dfechaVenc=dfechaVenc;
        this.bentregado = bentregado;
        this.cestado = cestado;
    }

    public entcPedidoDetalle copiar(entcPedidoDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdPedido(), this.getIdUnidadMedida(), this.getUnidadMedida(), this.getIdArticulo(), this.getDescripcion(), this.getCantidad(), this.getCuotas(), this.getIdFrecuenciaDescuento(), this.getFrecuenciaDescuento(), this.getFechaVenc(), this.getEntregado(),  this.getIdDescuento(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcPedidoDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(SQLException e) { }
        try { this.setIdPedido(rs.getInt("idpedido")); }
        catch(SQLException e) { }
        try { this.setIdUnidadMedida(rs.getInt("idunidadmedida")); }
        catch(SQLException e) { }
        try { this.setUnidadMedida(rs.getString("unidadmedida")); }
        catch(SQLException e) { }
        try { this.setIdArticulo(rs.getInt("idarticulo")); }
        catch(SQLException e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(SQLException e) { }
        try { this.setCuotas(rs.getInt("cuotas")); }
        catch(SQLException e) { }
        try { this.setIdDescuento(rs.getInt("iddescuento")); }
        catch(SQLException e) { }
        try { this.setIdFrecuenciaDescuento(rs.getInt("idfrecuenciadescuento")); }
        catch(SQLException e) { }
        try { this.setFrecuenciaDescuento(rs.getString("frecuenciadescuento")); }
        catch(SQLException e) { }
        try { this.setFechaVenc(rs.getDate("fechavenc")); }
        catch(SQLException e) { }
        try { this.setCantidad(rs.getDouble("cantidad")); }
        catch(SQLException e) { }
        try { this.setEntregado(rs.getBoolean("entregado")); }
        catch(SQLException e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdPedido(int iidPedido) {
        this.iidPedido = iidPedido;
    }

    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }

    public void setUnidadMedida(String sunidadMedida) {
        this.sunidadMedida = sunidadMedida;
    }
    
    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setEntregado(boolean bentregado) {
        this.bentregado = bentregado;
    }

    public void setCuotas(int icuotas) {
        this.icuotas = icuotas;
    }

    public void setIdFrecuenciaDescuento(int iidFrecuenciaDescuento) {
        this.iidFrecuenciaDescuento = iidFrecuenciaDescuento;
    }

    public void setFrecuenciaDescuento(String sfrecuenciaDescuento) {
        this.sfrecuenciaDescuento = sfrecuenciaDescuento;
    }

    public void setFechaVenc(Date dfechaVenc) {
        this.dfechaVenc = dfechaVenc;
    }

    public void setIdDescuento(int iidDescuento) {
        this.iidDescuento = iidDescuento;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdPedido() {
        return this.iidPedido;
    }
        
    public int getIdArticulo() {
        return this.iidArticulo;
    }
   
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }
   
    public String getUnidadMedida() {
        return this.sunidadMedida;
    }
    
    public double getCantidad() {
        return this.ucantidad;
    }

    public boolean getEntregado() {
        return this.bentregado;
    }

    public int getCuotas() {
        return icuotas;
    }

    public int getIdFrecuenciaDescuento() {
        return iidFrecuenciaDescuento;
    }

    public String getFrecuenciaDescuento() {
        return sfrecuenciaDescuento;
    }

    public Date getFechaVenc() {
        return dfechaVenc;
    }

    public int getIdDescuento() {
        return iidDescuento;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdArticulo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_ARTICULO;
            bvalido = false;
        }
        if ((this.getCantidad()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CANTIDAD;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.pedidodetalle("+
            this.getId()+","+
            this.getIdPedido()+","+
            this.getIdArticulo()+","+
            this.getCantidad()+","+
            this.getEntregado()+","+
            this.getCuotas()+","+
            this.getIdFrecuenciaDescuento()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaVenc())+","+
            this.getIdDescuento()+","+
            this.getEstadoRegistro()+")";
    }

}
