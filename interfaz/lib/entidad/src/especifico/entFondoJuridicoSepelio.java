/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entFondoJuridicoSepelio {

    private int iid;
    private int inumeroOperacion;
    private int iidSocio;
    private String scedula;
    private String sapellidoNombre;
    private int iidEstadoFondoJuridicoSepelio;
    private String sestadoFondoJuridicoSepelio;
    private String snumeroDeclaracion;
    private double dimporteTitular;
    private double dimporteAdherente;
    private int icantidadAdherente;
    private int iidMoneda;
    private String smoneda;
    private java.util.Date dfechaOperacion;
    private java.util.Date dfechaPrimerVencimiento;
    private int iidPromotor;
    private String spromotor;
    private int inumeroCuota;
    private int iidRegional;
    private String sregional;
    private int iidCuenta;
    private String scuenta;
    private String sfolio;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    private java.util.Date dfechaRenunciado;
    private String sobservacionRenunciado;
    
    private java.util.Date dfechaInactivo;
    private String sobservacionInactivo;
    
    private short hestadoRegistro;
    private String smensaje;

    public static final int INICIO_DIA_VENCIMIENTO = 0;
    public static final int FIN_DIA_VENCIMIENTO = 31;
    public static final int LONGITUD_OBSERVACION = 100;
    public static final int LONGITUD_NUMERO_DECLARACION = 12;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (no editable)";
    public static final String TEXTO_ESTADO = "Estado de Fondo Jurídico y Sepelio (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_NUMERO_DECLARACION = "Número de Declaración (no vacía)";
    public static final String TEXTO_FECHA_OPERACION = "Fecha de Operación (no editable)";
    public static final String TEXTO_IMPORTE_TITULAR = "Importe Titular (no editable)";
    public static final String TEXTO_CANTIDAD_TITULAR = "Cantidad Titular (no editable)";
    public static final String TEXTO_IMPORTE_ADHERENTE = "Importe Adherente (no editable)";
    public static final String TEXTO_CANTIDAD_ADHERENTE = "Cantidad Adherente (no editable)";
    public static final String TEXTO_IMPORTE_TOTAL_ADHERENTE = "Importe Total Adherente (no editable)";
    public static final String TEXTO_IMPORTE_TOTAL_GENERAL = "Importe Total General (no editable)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_FECHA_PRIMER_VENCIMIENTO = "Fecha del Primer Vencimiento (no nulo, menor o igual a la próxima Fecha de Cierre)";
    public static final String TEXTO_PROMOTOR = "Promotor (no vacío)";
    public static final String TEXTO_REGIONAL = "Regional (no vacía)";
    public static final String TEXTO_FOLIO = "Folio";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_RENUNCIADO = "Fecha de Renuncia (no editable)";
    public static final String TEXTO_OBSERVACION_RENUNCIADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_INACTIVO = "Fecha de Inactivación (no editable)";
    public static final String TEXTO_OBSERVACION_INACTIVO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    
    public entFondoJuridicoSepelio() {
        this.iid = 0;
        this.inumeroOperacion = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.sapellidoNombre = "";
        this.iidEstadoFondoJuridicoSepelio = 0;
        this.sestadoFondoJuridicoSepelio = "";
        this.snumeroDeclaracion = "";
        this.dimporteTitular = 0.0;
        this.dimporteAdherente = 0.0;
        this.icantidadAdherente = 0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.dfechaOperacion = null;
        this.dfechaPrimerVencimiento = null;
        this.iidPromotor = 0;
        this.spromotor = "";
        this.inumeroCuota = 0;
        this.iidRegional = 0;
        this.sregional = "";
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.dfechaRenunciado = null;
        this.sobservacionRenunciado = "";
        this.dfechaInactivo = null;
        this.sobservacionInactivo = "";
        this.iidCuenta = 0;
        this.scuenta = "";
        this.sfolio = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entFondoJuridicoSepelio(int iid, int inumeroOperacion, int iidSocio, String scedula, String sapellidoNombre, int iidEstadoFondoJuridicoSepelio, String sestadoFondoJuridicoSepelio, String snumeroDeclaracion, double dimporteTitular, double dimporteAdherente, int icantidadAdherente, int iidMoneda, String smoneda, java.util.Date dfechaOperacion, int iidGiraduria, String sgiraduria, java.util.Date dfechaPrimerVencimiento, int iidPromotor, String spromotor, int inumeroCuota, int iidRegional, String sregional, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRenunciado, String sobservacionRenunciado, java.util.Date dfechaInactivo, String sobservacionInactivo, int iidCuenta, String scuenta, String sfolio, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOperacion = inumeroOperacion;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.iidEstadoFondoJuridicoSepelio = iidEstadoFondoJuridicoSepelio;
        this.sestadoFondoJuridicoSepelio = sestadoFondoJuridicoSepelio;
        this.snumeroDeclaracion = snumeroDeclaracion;
        this.dimporteTitular = dimporteTitular;
        this.dimporteAdherente = dimporteAdherente;
        this.icantidadAdherente = icantidadAdherente;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.dfechaOperacion = dfechaOperacion;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.inumeroCuota = inumeroCuota;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRenunciado = dfechaRenunciado;
        this.sobservacionRenunciado = sobservacionRenunciado;
        this.dfechaInactivo = dfechaInactivo;
        this.sobservacionInactivo = sobservacionInactivo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sfolio = sfolio;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroOperacion, int iidSocio, String scedula, String sapellidoNombre, int iidEstadoFondoJuridicoSepelio, String sestadoFondoJuridicoSepelio, String snumeroDeclaracion, double dimporteTitular, double dimporteAdherente, int icantidadAdherente, int iidMoneda, String smoneda, java.util.Date dfechaOperacion, java.util.Date dfechaPrimerVencimiento, int iidPromotor, String spromotor, int inumeroCuota, int iidRegional, String sregional, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRenunciado, String sobservacionRenunciado, java.util.Date dfechaInactivo, String sobservacionInactivo, int iidCuenta, String scuenta, String sfolio, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOperacion = inumeroOperacion;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.sapellidoNombre = sapellidoNombre;
        this.iidEstadoFondoJuridicoSepelio = iidEstadoFondoJuridicoSepelio;
        this.sestadoFondoJuridicoSepelio = sestadoFondoJuridicoSepelio;
        this.snumeroDeclaracion = snumeroDeclaracion;
        this.dimporteTitular = dimporteTitular;
        this.dimporteAdherente = dimporteAdherente;
        this.icantidadAdherente = icantidadAdherente;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.dfechaOperacion = dfechaOperacion;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.inumeroCuota = inumeroCuota;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRenunciado = dfechaRenunciado;
        this.sobservacionRenunciado = sobservacionRenunciado;
        this.dfechaInactivo = dfechaInactivo;
        this.sobservacionInactivo = sobservacionInactivo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sfolio = sfolio;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entFondoJuridicoSepelio copiar(entFondoJuridicoSepelio destino) {
        destino.setEntidad(this.getId(), this.getNumeroOperacion(), this.getIdSocio(), this.getCedula(), this.getApellidoNombre(), this.getIdEstadoFondoJuridicoSepelio(), this.getEstadoFondoJuridicoSepelio(), this.getNumeroDeclaracion(), this.getImporteTitular(), this.getImporteAdherente(), this.getCantidadAdherente(), this.getIdMoneda(), this.getMoneda(), this.getFechaOperacion(), this.getFechaPrimerVencimiento(), this.getIdPromotor(), this.getPromotor(), this.getNumeroCuota(), this.getIdRegional(), this.getRegional(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getFechaRenunciado(), this.getObservacionRenunciado(), this.getFechaInactivo(), this.getObservacionInactivo(), this.getIdCuenta(), this.getCuenta(), this.getFolio(), this.getEstadoRegistro());
        return destino;
    }
    
    public entFondoJuridicoSepelio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setApellidoNombre(rs.getString("apellidonombre")); }
        catch(Exception e) {}
        try { this.setIdEstadoFondoJuridicoSepelio(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstadoFondoJuridicoSepelio(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setNumeroDeclaracion(rs.getString("numerodeclaracion")); }
        catch(Exception e) {}
        try { this.setImporteTitular(rs.getDouble("importetitular")); }
        catch(Exception e) {}
        try { this.setImporteAdherente(rs.getDouble("importeadherente")); }
        catch(Exception e) {}
        try { this.setCantidadAdherente(rs.getInt("cantidadadherente")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setFechaPrimerVencimiento(rs.getDate("fechaprimervencimiento")); }
        catch(Exception e) {}
        try { this.setIdPromotor(rs.getInt("idpromotor")); }
        catch(Exception e) {}
        try { this.setPromotor(rs.getString("promotor")); }
        catch(Exception e) {}
        try { this.setNumeroCuota(rs.getInt("numerocuota")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setFechaRenunciado(rs.getDate("fecharenuncia")); }
        catch(Exception e) {}
        try { this.setObservacionRenunciado(rs.getString("observacionrenuncia")); }
        catch(Exception e) {}
        try { this.setFechaInactivo(rs.getDate("fechainactivo")); }
        catch(Exception e) {}
        try { this.setObservacionInactivo(rs.getString("observacioninactivo")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setFolio(rs.getString("folio")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setApellidoNombre(String sapellidoNombre) {
        this.sapellidoNombre = sapellidoNombre;
    }

    public void setIdEstadoFondoJuridicoSepelio(int iidEstadoFondoJuridicoSepelio) {
        this.iidEstadoFondoJuridicoSepelio = iidEstadoFondoJuridicoSepelio;
    }

    public void setEstadoFondoJuridicoSepelio(String sestadoFondoJuridicoSepelio) {
        this.sestadoFondoJuridicoSepelio = sestadoFondoJuridicoSepelio;
    }

    public void setNumeroDeclaracion(String snumeroDeclaracion) {
        this.snumeroDeclaracion = snumeroDeclaracion;
    }

    public void setImporteTitular(double dimporteTitular) {
        this.dimporteTitular = dimporteTitular;
    }

    public void setImporteAdherente(double dimporteAdherente) {
        this.dimporteAdherente = dimporteAdherente;
    }

    public void setCantidadAdherente(int icantidadAdherente) {
        this.icantidadAdherente = icantidadAdherente;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setFechaPrimerVencimiento(java.util.Date dfechaPrimerVencimiento) {
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
    }

    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }

    public void setPromotor(String spromotor) {
        this.spromotor = spromotor;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setNumeroCuota(int inumeroCuota) {
        this.inumeroCuota = inumeroCuota;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setFechaRenunciado(java.util.Date dfechaRenunciado) {
        this.dfechaRenunciado = dfechaRenunciado;
    }

    public void setObservacionRenunciado(String sobservacionRenunciado) {
        this.sobservacionRenunciado = sobservacionRenunciado;
    }

    public void setFechaInactivo(java.util.Date dfechaInactivo) {
        this.dfechaInactivo = dfechaInactivo;
    }

    public void setObservacionInactivo(String sobservacionInactivo) {
        this.sobservacionInactivo = sobservacionInactivo;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setFolio(String sfolio) {
        this.sfolio = sfolio;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getApellidoNombre() {
        return this.sapellidoNombre;
    }

    public int getIdEstadoFondoJuridicoSepelio() {
        return this.iidEstadoFondoJuridicoSepelio;
    }

    public String getEstadoFondoJuridicoSepelio() {
        return this.sestadoFondoJuridicoSepelio;
    }

    public String getNumeroDeclaracion() {
        return this.snumeroDeclaracion;
    }

    public double getImporteTitular() {
        return this.dimporteTitular;
    }

    public double getImporteAdherente() {
        return this.dimporteAdherente;
    }

    public int getCantidadAdherente() {
        return this.icantidadAdherente;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public java.util.Date getFechaPrimerVencimiento() {
        return this.dfechaPrimerVencimiento;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public String getPromotor() {
        return this.spromotor;
    }

    public int getNumeroCuota() {
        return this.inumeroCuota;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public java.util.Date getFechaRenunciado() {
        return this.dfechaRenunciado;
    }

    public String getObservacionRenunciado() {
        return this.sobservacionRenunciado;
    }

    public java.util.Date getFechaInactivo() {
        return this.dfechaInactivo;
    }

    public String getObservacionInactivo() {
        return this.sobservacionInactivo;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getFolio() {
        return this.sfolio;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdSocio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getNumeroDeclaracion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_DECLARACION;
            bvalido = false;
        }
        if (this.getImporteTitular()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPORTE_TITULAR;
            bvalido = false;
        }
        if (this.getImporteAdherente()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPORTE_ADHERENTE;
            bvalido = false;
        }
        if (this.getCantidadAdherente()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CANTIDAD_ADHERENTE;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getFechaOperacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_OPERACION;
            bvalido = false;
        }
        if (this.getIdPromotor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PROMOTOR;
            bvalido = false;
        }
        if (this.getFechaPrimerVencimiento()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_PRIMER_VENCIMIENTO;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoRenunciar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionRenunciado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_RENUNCIADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoInactivar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionInactivo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_INACTIVO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT fondojuridicosepelio("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdEstadoFondoJuridicoSepelio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumeroDeclaracion())+","+
            this.getImporteTitular()+","+
            this.getImporteAdherente()+","+
            this.getCantidadAdherente()+","+
            this.getIdMoneda()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaOperacion())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaPrimerVencimiento())+","+
            this.getIdPromotor()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaRenunciado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionRenunciado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaInactivo())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionInactivo())+","+
            this.getIdRegional()+","+
            this.getNumeroOperacion()+","+
            this.getIdCuenta()+","+
            utilitario.utiCadena.getTextoGuardado(this.getFolio())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Número de Operación", "numerooperacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(3, "Apellido y Nombre", "apellidonombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Fecha Operación", "fechaoperacion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(5, "Fecha Primer Vencimiento", "fechaprimervencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(6, "Fecha Vencimiento", "fechavencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(7, "Folio", "folio", generico.entLista.tipo_fecha));
        return lst;
    }

}
