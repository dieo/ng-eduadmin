/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entLlamadaRegional {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String snombreApellido;
    private int iidTipoLlamada;
    private String stipoLlamada;
    private int iidIntermediario;
    private String snombreApellidoIntermediario;
    private int iidCargo;
    private String scargo;
    private int iidRegional;
    private String sregional;
    private int iidMotivoLlamada;
    private String smotivoLlamada;
    private String stelefono;
    private java.util.Date dfechaLlamada;
    private String shoraLlamada;
    private String sobservacion;
    private int iduracion;
    private int iidUsuario;
    private String susuarioNombre;
    private String susuarioApellido;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_TELEFONO = 40;
    public static final int LONGITUD_OBSERVACION = 250;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_INTERMEDIARIO = "Intermediario (no vacío)";
    public static final String TEXTO_CARGO = "Cargo (no editable)";
    public static final String TEXTO_REGIONAL = "Regional (no editable)";
    public static final String TEXTO_TIPO_LLAMADA = "Tipo de Llamada (no vacío)";
    public static final String TEXTO_MOTIVO_LLAMADA = "Motivo de Llamada (no vacío)";
    public static final String TEXTO_TELEFONO = "Teléfono (no vacío, hasta " + LONGITUD_TELEFONO + " caracteres)";
    public static final String TEXTO_FECHA_LLAMADA = "Fecha de la Llamada (no vacía)";
    public static final String TEXTO_HORA_LLAMADA = "Hora de la Llamada (no vacía)";
    public static final String TEXTO_OBSERVACION = "Observación (no vacía)";
    public static final String TEXTO_DURACION = "Duración de la Llamada en minutos (no vacía, valor positivo hasta 60)";
    public static final String TEXTO_USUARIO = "Usuario (no editable)";

    public entLlamadaRegional() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.snombreApellido = "";
        this.iidTipoLlamada = 0;
        this.stipoLlamada = "";
        this.iidIntermediario = 0;
        this.snombreApellidoIntermediario = "";
        this.iidCargo = 0;
        this.scargo = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.iidMotivoLlamada = 0;
        this.smotivoLlamada = "";
        this.stelefono = "";
        this.dfechaLlamada = null;
        this.shoraLlamada = "";
        this.sobservacion = "";
        this.iduracion = 0;
        this.iidUsuario = 0;
        this.susuarioNombre = "";
        this.susuarioApellido = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entLlamadaRegional(int iid, int iidSocio, String scedula, String snombreApellido, int iidTipoLlamada, String stipoLlamada, int iidIntermediario, String snombreApellidoIntermediario, int iidCargo, String scargo, int iidRegional, String sregional, int iidMotivoLlamada, String smotivoLlamada, String stelefono, java.util.Date dfechaLlamada, String shoraLlamada, String sobservacion, int iduracion, int iidUsuario, String susuarioNombre, String susuarioApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidTipoLlamada = iidTipoLlamada;
        this.stipoLlamada = stipoLlamada;
        this.iidIntermediario = iidIntermediario;
        this.snombreApellidoIntermediario = snombreApellidoIntermediario;
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidMotivoLlamada = iidMotivoLlamada;
        this.smotivoLlamada = smotivoLlamada;
        this.stelefono = stelefono;
        this.dfechaLlamada = dfechaLlamada;
        this.shoraLlamada = shoraLlamada;
        this.sobservacion = sobservacion;
        this.iduracion = iduracion;
        this.iidUsuario = iidUsuario;
        this.susuarioNombre = susuarioNombre;
        this.susuarioApellido = susuarioApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, String snombreApellido, int iidTipoLlamada, String stipoLlamada, int iidIntermediario, String snombreApellidoIntermediario, int iidCargo, String scargo, int iidRegional, String sregional, int iidMotivoLlamada, String smotivoLlamada, String stelefono, java.util.Date dfechaLlamada, String shoraLlamada, String sobservacion, int iduracion, int iidUsuario, String susuarioNombre, String susuarioApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidTipoLlamada = iidTipoLlamada;
        this.stipoLlamada = stipoLlamada;
        this.iidIntermediario = iidIntermediario;
        this.snombreApellidoIntermediario = snombreApellidoIntermediario;
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidMotivoLlamada = iidMotivoLlamada;
        this.smotivoLlamada = smotivoLlamada;
        this.stelefono = stelefono;
        this.dfechaLlamada = dfechaLlamada;
        this.shoraLlamada = shoraLlamada;
        this.sobservacion = sobservacion;
        this.iduracion = iduracion;
        this.iidUsuario = iidUsuario;
        this.susuarioNombre = susuarioNombre;
        this.susuarioApellido = susuarioApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entLlamadaRegional copiar(entLlamadaRegional destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getNombreApellido(), this.getIdTipoLlamada(), this.getTipoLlamada(), this.getIdIntermediario(), this.getNombreApellidoIntermediario(), this.getIdCargo(), this.getCargo(), this.getIdRegional(), this.getRegional(), this.getIdMotivoLlamada(), this.getMotivoLlamada(), this.getTelefono(), this.getFechaLlamada(), this.getHoraLlamada(), this.getObservacion(), this.getDuracion(), this.getIdUsuario(), this.getUsuarioNombre(), this.getUsuarioApellido(), this.getEstadoRegistro());
        return destino;
    }
    
    public entLlamadaRegional cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombreApellido(rs.getString("nombreapellido")); }
        catch(Exception e) {}
        try { this.setIdTipoLlamada(rs.getInt("idtipollamada")); }
        catch(Exception e) {}
        try { this.setTipoLlamada(rs.getString("tipollamada")); }
        catch(Exception e) {}
        try { this.setIdIntermediario(rs.getInt("idintermediario")); }
        catch(Exception e) {}
        try { this.setNombreApellidoIntermediario(rs.getString("nombreapellidointermediario")); }
        catch(Exception e) {}
        try { this.setIdCargo(rs.getInt("idcargo")); }
        catch(Exception e) {}
        try { this.setCargo(rs.getString("cargo")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setIdMotivoLlamada(rs.getInt("idmotivollamada")); }
        catch(Exception e) {}
        try { this.setMotivoLlamada(rs.getString("motivollamada")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setFechaLlamada(rs.getDate("fechallamada")); }
        catch(Exception e) {}
        try { this.setHoraLlamada(rs.getString("horallamada")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setDuracion(rs.getInt("duracion")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuarioNombre(rs.getString("usuarionombre")); }
        catch(Exception e) {}
        try { this.setUsuarioApellido(rs.getString("usuarioapellido")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombreApellido(String snombreApellido) {
        this.snombreApellido = snombreApellido;
    }

    public void setIdTipoLlamada(int iidTipoLlamada) {
        this.iidTipoLlamada = iidTipoLlamada;
    }

    public void setTipoLlamada(String stipoLlamada) {
        this.stipoLlamada = stipoLlamada;
    }

    public void setIdIntermediario(int iidIntermediario) {
        this.iidIntermediario = iidIntermediario;
    }

    public void setNombreApellidoIntermediario(String snombreApellidoIntermediario) {
        this.snombreApellidoIntermediario = snombreApellidoIntermediario;
    }

    public void setIdCargo(int iidCargo) {
        this.iidCargo = iidCargo;
    }

    public void setCargo(String scargo) {
        this.scargo = scargo;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setIdMotivoLlamada(int iidMotivoLlamada) {
        this.iidMotivoLlamada = iidMotivoLlamada;
    }

    public void setMotivoLlamada(String smotivoLlamada) {
        this.smotivoLlamada = smotivoLlamada;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setFechaLlamada(java.util.Date dfechaLlamada) {
        this.dfechaLlamada = dfechaLlamada;
    }

    public void setHoraLlamada(String shoraLlamada) {
        this.shoraLlamada = shoraLlamada;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setDuracion(int iduracion) {
        this.iduracion = iduracion;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }

    public void setUsuarioNombre(String susuarioNombre) {
        this.susuarioNombre = susuarioNombre;
    }

    public void setUsuarioApellido(String susuarioApellido) {
        this.susuarioApellido = susuarioApellido;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombreApellido() {
        return this.snombreApellido;
    }

    public int getIdTipoLlamada() {
        return this.iidTipoLlamada;
    }

    public String getTipoLlamada() {
        return this.stipoLlamada;
    }

    public int getIdIntermediario() {
        return this.iidIntermediario;
    }

    public String getNombreApellidoIntermediario() {
        return this.snombreApellidoIntermediario;
    }

    public int getIdCargo() {
        return this.iidCargo;
    }

    public String getCargo() {
        return this.scargo;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public int getIdMotivoLlamada() {
        return this.iidMotivoLlamada;
    }

    public String getMotivoLlamada() {
        return this.smotivoLlamada;
    }

    public String getTelefono() {
        return this.stelefono;
    }

    public java.util.Date getFechaLlamada() {
        return this.dfechaLlamada;
    }

    public String getHoraLlamada() {
        return this.shoraLlamada;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public int getDuracion() {
        return this.iduracion;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }

    public String getUsuarioNombre() {
        return this.susuarioNombre;
    }

    public String getUsuarioApellido() {
        return this.susuarioApellido;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdSocio()<=0 && this.getCedula().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getIdTipoLlamada()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_LLAMADA;
            bvalido = false;
        }
        if (this.getIdIntermediario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_INTERMEDIARIO;
            bvalido = false;
        }
        if (this.getIdMotivoLlamada()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MOTIVO_LLAMADA;
            bvalido = false;
        }
        if (this.getTelefono().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO;
            bvalido = false;
        }
        if (this.getFechaLlamada()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_LLAMADA;
            bvalido = false;
        }
        if (this.getHoraLlamada().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_LLAMADA;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        if (this.getDuracion()<0 || this.getDuracion()>60) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DURACION;
            bvalido = false;
        }
        if (this.getIdUsuario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_USUARIO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT llamadaregional("+
            this.getId()+","+
            this.getIdTipoLlamada()+","+
            this.getIdIntermediario()+","+
            this.getIdSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getIdMotivoLlamada()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaLlamada())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraLlamada())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getDuracion()+","+
            this.getIdUsuario()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombreApellido())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Tipo Llamada", "tipollamada", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Nombre y Apellido", "nombreapellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Teléfono", "telefono", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Motivo Llamada", "motivollamada", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Fecha Llamada", "fechallamada", generico.entLista.tipo_fecha));
        return lst;
    }

}
