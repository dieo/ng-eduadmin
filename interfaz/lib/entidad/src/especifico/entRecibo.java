/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entRecibo {
    
    private int iid;
    private java.util.Date dfechaRecibo;
    private int iidPersona;
    private String sruc;
    private String srazonSocial;
    private String sdireccion;
    private String stelefono;
    private int iidFactura;
    private int iidTipoPrecio;
    private int iidUsuario;
    private String susuario;
    private int iidTalonario;
    private String stalonario;
    private String slocal;
    private String spuntoExpedicion;
    private int inumeroRecibo;
    private int iidEstadoRecibo;
    private String sestadoRecibo;
    private double utotal;
    private int iidMoneda;
    private String smoneda;
    private int iidOperacion;
    private boolean bsocio;
    private double uexoneracion;
    
    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;

    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_RUC = 12;
    public static final int LONGITUD_RAZON_SOCIAL = 80;
    public static final int LONGITUD_DIRECCION = 100;
    public static final int LONGITUD_TELEFONO = 40;
    public static final int LONGITUD_LOCAL = 3;
    public static final int LONGITUD_PUNTO_EXPEDICION = 3;
    public static final int LONGITUD_OBSERVACION_ANULADO = 100;
    public static final int CANTIDAD_LINEA_RECIBO = 12;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FECHA_RECIBO = "Fecha de Recibo (no vacía)";
    public static final String TEXTO_RUC = "RUC (no editable)";
    public static final String TEXTO_RAZON_SOCIAL = "Razón Social (no editable)";
    public static final String TEXTO_DIRECCION = "Dirección (no editable)";
    public static final String TEXTO_TELEFONO = "Teléfono (no editable)";
    public static final String TEXTO_TIPO_PRECIO = "Tipo de Precio (no vacío)";
    public static final String TEXTO_USUARIO = "Usuario (no editable)";
    public static final String TEXTO_TALONARIO = "Talonario (no vacío)";
    public static final String TEXTO_LOCAL = "Local (no vacío, hasta "+LONGITUD_LOCAL+" caracteres)";
    public static final String TEXTO_PUNTO_EXPEDICION = "Punto de Expedición (no vacío, hasta "+LONGITUD_PUNTO_EXPEDICION+" caracteres)";
    public static final String TEXTO_NUMERO_RECIBO = "Número de Recibo (no vacío, no editable)";
    public static final String TEXTO_ESTADO = "Estado de Recibo (no editable)";
    public static final String TEXTO_TOTAL = "Importe Total (no editable, valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_SOCIO = "Socio";
    public static final String TEXTO_ATRASO = "Atraso";
    public static final String TEXTO_ACTUAL = "Actual";
    public static final String TEXTO_SALDO = "Saldo";
    public static final String TEXTO_EXONERACION = "Exoneración";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación de anulación (no vacía, hasta " + LONGITUD_OBSERVACION_ANULADO + " caracteres)";
    
    public entRecibo() {
        this.iid = 0;
        this.dfechaRecibo = null;
        this.iidPersona = 0;
        this.sruc = "";
        this.srazonSocial = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.iidFactura = 0;
        this.iidTipoPrecio = 0;
        this.iidUsuario = 0;
        this.susuario = "";
        this.iidTalonario = 0;
        this.stalonario = "";
        this.slocal = "";
        this.spuntoExpedicion = "";
        this.inumeroRecibo = 0;
        this.iidEstadoRecibo = 0;
        this.sestadoRecibo = "";
        this.utotal = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iidOperacion =0;
        this.bsocio = false;
        this.uexoneracion = 0.0;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entRecibo(int iid, java.util.Date dfechaRecibo, int iidPersona, String sruc, String srazonSocial, String sdireccion, String stelefono, int iidFactura, int iidTipoPrecio, int iidUsuario, String susuario, int iidTalonario, String stalonario, String slocal, String spuntoExpedicion, int inumeroRecibo, int iidEstadoRecibo, String sestadoRecibo, double utotal, int iidMoneda, String smoneda, int iidOperacion, boolean bsocio, double uexoneracion, java.util.Date dfechaAnulado, String sobservacionAnulado, short hestadoRegistro) {
        this.iid = iid;
        this.dfechaRecibo = dfechaRecibo;
        this.iidPersona = iidPersona;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidFactura = iidFactura;
        this.iidTipoPrecio = iidTipoPrecio;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidTalonario = iidTalonario;
        this.stalonario = stalonario;
        this.slocal = slocal;
        this.spuntoExpedicion = spuntoExpedicion;
        this.inumeroRecibo = inumeroRecibo;
        this.iidEstadoRecibo = iidEstadoRecibo;
        this.sestadoRecibo = sestadoRecibo;
        this.utotal = utotal;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidOperacion =iidOperacion;
        this.bsocio = bsocio;
        this.uexoneracion = uexoneracion;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setEntidad(int iid, java.util.Date dfechaRecibo, int iidPersona, String sruc, String srazonSocial, String sdireccion, String stelefono, int iidFactura, int iidTipoPrecio, int iidUsuario, String susuario, int iidTalonario, String stalonario, String slocal, String spuntoExpedicion, int inumeroRecibo, int iidEstadoRecibo, String sestadoRecibo, double utotal, int iidMoneda, String smoneda, int iidOperacion, boolean bsocio, double uexoneracion, java.util.Date dfechaAnulado, String sobservacionAnulado, short hestadoRegistro) {
        this.iid = iid;
        this.dfechaRecibo = dfechaRecibo;
        this.iidPersona = iidPersona;
        this.sruc = sruc;
        this.srazonSocial = srazonSocial;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidFactura = iidFactura;
        this.iidTipoPrecio = iidTipoPrecio;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidTalonario = iidTalonario;
        this.stalonario = stalonario;
        this.slocal = slocal;
        this.spuntoExpedicion = spuntoExpedicion;
        this.inumeroRecibo = inumeroRecibo;
        this.iidEstadoRecibo = iidEstadoRecibo;
        this.sestadoRecibo = sestadoRecibo;
        this.utotal = utotal;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iidOperacion =iidOperacion;
        this.bsocio = bsocio;
        this.uexoneracion = uexoneracion;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entRecibo copiar(entRecibo destino) {
        destino.setEntidad(this.getId(), this.getFechaRecibo(), this.getIdPersona(), this.getRuc(), this.getRazonSocial(), this.getDireccion(), this.getTelefono(), this.getIdFactura(), this.getIdTipoPrecio(), this.getIdUsuario(), this.getUsuario(), this.getIdTalonario(), this.getTalonario(), this.getLocal(), this.getPuntoExpedicion(), this.getNumeroRecibo(), this.getIdEstadoRecibo(), this.getEstadoRecibo(), this.getTotal(), this.getIdMoneda(), this.getMoneda(), this.getIdOperacion(), this.getSocio(), this.getExoneracion(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getEstadoRegistro());
        return destino;
    }
    
    public entRecibo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setFechaRecibo(rs.getDate("fecharecibo")); }
        catch(Exception e) { }
        try { this.setSocio(rs.getBoolean("socio")); }
        catch(Exception e) { }
        try { this.setIdPersona(rs.getInt("idpersona")); }
        catch(Exception e) { }
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) { }
        try { this.setRazonSocial(rs.getString("razonsocial")); }
        catch(Exception e) { }
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) { }
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) { }
        try { this.setIdFactura(rs.getInt("idfactura")); }
        catch(Exception e) { }
        try { this.setIdTipoPrecio(rs.getInt("idtipoprecio")); }
        catch(Exception e) { }
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) { }
        try { this.setUsuario(rs.getString("usuario")); }
        catch(Exception e) { }
        try { this.setIdTalonario(rs.getInt("idtalonario")); }
        catch(Exception e) { }
        try { this.setTalonario(rs.getString("talonario")); }
        catch(Exception e) { }
        try { this.setLocal(rs.getString("local")); }
        catch(Exception e) { }
        try { this.setPuntoExpedicion(rs.getString("puntoexpedicion")); }
        catch(Exception e) { }
        try { this.setNumeroRecibo(rs.getInt("numerorecibo")); }
        catch(Exception e) { }
        try { this.setIdEstadoRecibo(rs.getInt("idestado")); }
        catch(Exception e) { }
        try { this.setEstadoRecibo(rs.getString("estadorecibo")); }
        catch(Exception e) { }
        try { this.setTotal(rs.getDouble("total")); }
        catch(Exception e) { }
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) { }
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) { }
        try { this.setIdOperacion(rs.getInt("idoperacion")); }
        catch(Exception e) { }
        try { this.setSocio(rs.getBoolean("socio")); }
        catch(Exception e) { }
        try { this.setExoneracion(rs.getDouble("exoneracion")); }
        catch(Exception e) { }
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) { }
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setFechaRecibo(java.util.Date dfechaRecibo) {
        this.dfechaRecibo = dfechaRecibo;
    }

    public void setIdPersona(int iidPersona) {
        this.iidPersona = iidPersona;
    }
    
    public void setRuc(String sruc) {
        this.sruc = sruc;
    }
    
    public void setRazonSocial(String srazonSocial) {
        this.srazonSocial = srazonSocial;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }
    
    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }
    
    public void setIdFactura(int iidFactura) {
        this.iidFactura = iidFactura;
    }
    
    public void setIdTipoPrecio(int iidTipoPrecio) {
        this.iidTipoPrecio = iidTipoPrecio;
    }
    
    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }
    
    public void setUsuario(String susuario) {
        this.susuario = susuario;
    }
    
    public void setIdTalonario(int iidTalonario) {
        this.iidTalonario = iidTalonario;
    }
    
    public void setTalonario(String stalonario) {
        this.stalonario = stalonario;
    }
    
    public void setLocal(String slocal) {
        this.slocal = slocal;
    }
    
    public void setPuntoExpedicion(String spuntoExpedicion) {
        this.spuntoExpedicion = spuntoExpedicion;
    }
    
    public void setNumeroRecibo(int inumeroRecibo) {
        this.inumeroRecibo = inumeroRecibo;
    }

    public void setIdEstadoRecibo(int iidEstadoRecibo) {
        this.iidEstadoRecibo = iidEstadoRecibo;
    }
    
    public void setEstadoRecibo(String sestadoRecibo) {
        this.sestadoRecibo = sestadoRecibo;
    }
    
    public void setTotal(double utotal) {
        this.utotal = utotal;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setIdOperacion(int iidOperacion) {
        this.iidOperacion = iidOperacion;
    }
    
    public void setSocio(boolean bsocio) {
        this.bsocio = bsocio;
    }
    
    public void setExoneracion(double uexoneracion) {
        this.uexoneracion = uexoneracion;
    }
    
    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public java.util.Date getFechaRecibo() {
        return this.dfechaRecibo;
    }

    public int getIdPersona() {
        return this.iidPersona;
    }
    
    public String getRuc() {
        return this.sruc;
    }
    
    public String getRazonSocial() {
        return this.srazonSocial;
    }
    
    public String getDireccion() {
        return this.sdireccion;
    }
    
    public String getTelefono() {
        return this.stelefono;
    }
    
    public int getIdFactura() {
        return this.iidFactura;
    }
    
    public int getIdTipoPrecio() {
        return this.iidTipoPrecio;
    }
    
    public int getIdUsuario() {
        return this.iidUsuario;
    }
    
    public String getUsuario() {
        return this.susuario;
    }
    
    public int getIdTalonario() {
        return this.iidTalonario;
    }
    
    public String getTalonario() {
        return this.stalonario;
    }
    
    public String getLocal() {
        return this.slocal;
    }
    
    public String getPuntoExpedicion() {
        return this.spuntoExpedicion;
    }
    
    public int getNumeroRecibo() {
        return this.inumeroRecibo;
    }

    public int getIdEstadoRecibo() {
        return this.iidEstadoRecibo;
    }

    public String getEstadoRecibo() {
        return this.sestadoRecibo;
    }
    
    public double getTotal() {
        return this.utotal;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }
    
    public String getMoneda() {
        return this.smoneda;
    }

    public int getIdOperacion() {
        return this.iidOperacion;
    }
    
    public boolean getSocio() {
        return this.bsocio;
    }

    public double getExoneracion() {
        return this.uexoneracion;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaRecibo()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_RECIBO;
            bvalido = false;
        }
        if (this.getRuc().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RUC;
            bvalido = false;
        }
        if (this.getRazonSocial().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RAZON_SOCIAL;
            bvalido = false;
        }
        if (this.getDireccion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DIRECCION;
            bvalido = false;
        }
        if (this.getTelefono().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO;
            bvalido = false;
        }
        if (this.getIdTipoPrecio()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_PRECIO;
            bvalido = false;
        }
        if (this.getIdUsuario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_USUARIO;
            bvalido = false;
        }
        if (this.getIdTalonario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TALONARIO;
            bvalido = false;
        }
        if (this.getLocal().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_LOCAL;
            bvalido = false;
        }
        if (this.getPuntoExpedicion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PUNTO_EXPEDICION;
            bvalido = false;
        }
        if (this.getNumeroRecibo()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_RECIBO;
            bvalido = false;
        }
        if (this.getIdEstadoRecibo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ESTADO;
            bvalido = false;
        }
        if (this.getTotal()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TOTAL;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaAnulado()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_ANULADO;
            bvalido = false;
        }
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT recibo("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaRecibo())+","+
            this.getIdPersona()+","+
            this.getIdFactura()+","+
            this.getIdTipoPrecio()+","+
            this.getIdUsuario()+","+
            this.getIdTalonario()+","+
            utilitario.utiCadena.getTextoGuardado(this.getLocal())+","+
            utilitario.utiCadena.getTextoGuardado(this.getPuntoExpedicion())+","+
            this.getNumeroRecibo()+","+
            this.getIdEstadoRecibo()+","+
            this.getTotal()+","+
            this.getIdMoneda()+","+
            this.getIdOperacion()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            this.getSocio()+","+
            this.getExoneracion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getRuc())+","+
            utilitario.utiCadena.getTextoGuardado(this.getRazonSocial())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha Recibo", "fecharecibo", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(2, "Razón Social", "razonsocial", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Funcionario", "funcionario", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Número Recibo", "numerorecibo", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(5, "Estado Recibo", "estadorecibo", generico.entLista.tipo_texto));
        return lst;
    }
    
}
