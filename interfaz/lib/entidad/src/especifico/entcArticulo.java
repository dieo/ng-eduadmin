/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entcArticulo extends generico.entGenerica{
    
    protected String scodigo;
    protected int iidFilial;
    protected String sfilial;
    protected int iidCategoria;
    protected String scategoria;
    protected int iidUnidadMedida;
    protected String sunidadMedida;
    protected String sunidadCompra;
    protected double uconversion;
    protected int iidPlanCuenta;
    protected String snroPlanCuenta;
    protected String snroPlanCuenta1;
    protected String splanCuenta;
    protected double ucosto;
    protected double ustock;
    protected double uminimo;
    
    public static final int LONGITUD_DESCRIPCION = 100;
    public static final int LONGITUD_UNIDAD_COMPRA = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción del producto (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_CODIGO = "Código de barras";
    public static final String TEXTO_FILIAL = "Filial a que pertenece (no vacía)";
    public static final String TEXTO_CATEGORIA = "Categoría de produto o servicio (no vacía)";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de medida del producto (uso interno)";
    public static final String TEXTO_UNIDAD_COMPRA = "Unidad de medida en la compra del producto (según proveedor, hasta "+ LONGITUD_UNIDAD_COMPRA + " caracteres)";
    public static final String TEXTO_CONVERSION = "Factor de conversión entre compra y distribución interna (no vacia, valor positivo)";
    public static final String TEXTO_STOCK = "Existencia del producto (no editable)";
    public static final String TEXTO_PRECIO = "Precio unitario (no vacío, valor positivo)";
    public static final String TEXTO_MINIMO = "Stock Mínimo del producto para solicitar reposición (no vacío, valor positivo)";

    public entcArticulo() {
        super();
        this.scodigo = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.iidCategoria = 0;
        this.scategoria = "";
        this.iidUnidadMedida = 0;
        this.sunidadMedida = "";
        this.sunidadCompra = "";
        this.uconversion = 0.0;
        this.iidPlanCuenta = 0;
        this.snroPlanCuenta = "";
        this.snroPlanCuenta1 = "";
        this.splanCuenta = "";
        this.ustock = 0.0;
        this.uminimo = 0.0;
    }

    public entcArticulo(int iid, String sdescripcion, String scodigo, int iidFilial, String sfilial, int iidCategoria, String scategoria, int iidUnidadMedida, String sunidadMedida, String sunidadCompra, double uconversion, double ustock, double uminimo, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta1, String splanCuenta, double ucosto, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.scodigo = scodigo;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidCategoria = iidCategoria;
        this.scategoria = scategoria;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.sunidadCompra = sunidadCompra;
        this.uconversion = uconversion;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta1 = snroPlanCuenta1;
        this.splanCuenta = splanCuenta;
        this.ustock = ustock;
        this.uminimo = uminimo;
        this.ucosto = ucosto;
    }

    public void setEntidad(int iid, String sdescripcion, String scodigo, int iidFilial, String sfilial, int iidCategoria, String scategoria, int iidUnidadMedida, String sunidadMedida, String sunidadCompra, double uconversion, double ustock, double uminimo, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta1, String splanCuenta, double ucosto, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.scodigo = scodigo;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.iidCategoria = iidCategoria;
        this.scategoria = scategoria;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.sunidadCompra = sunidadCompra;
        this.uconversion = uconversion;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta1 = snroPlanCuenta1;
        this.splanCuenta = splanCuenta;
        this.ustock = ustock;
        this.uminimo = uminimo;
        this.ucosto = ucosto;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entcArticulo copiar(entcArticulo destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getCodigo(), this.getIdFilial(), this.getFilial(), this.getIdCategoria(), this.getCategoria(), this.getIdUnidadMedida(), this.getUnidadMedida(), this.getUnidadCompra(), this.getConversion(), this.getStock(), this.getMinimo(), this.getIdPlanCuenta(), this.getNroPlanCuenta(), this.getNroPlanCuenta1(), this.getPlanCuenta(), this.getCosto(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcArticulo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setCodigo(rs.getString("codigo")); }
        catch(Exception e) {}
        try { this.setIdCategoria(rs.getInt("idcategoria")); }
        catch(Exception e) {}
        try { this.setCategoria(rs.getString("categoria")); }
        catch(Exception e) {}
        try { this.setIdUnidadMedida(rs.getInt("idunidadmedida")); }
        catch(Exception e) {}
        try { this.setUnidadMedida(rs.getString("unidadmedida")); }
        catch(Exception e) {}
        try { this.setUnidadCompra(rs.getString("unidadcompra")); }
        catch(Exception e) {}
        try { this.setConversion(rs.getDouble("conversion")); }
        catch(Exception e) {}
        try { this.setCosto(rs.getDouble("costo")); }
        catch(Exception e) {}
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) { }
        try { this.setNroPlanCuenta(rs.getString("nroplancuenta1")); }
        catch(Exception e) { }
        try { this.setNroPlanCuenta1(rs.getString("nroplancuenta")); }
        catch(Exception e) { }
        try { this.setPlanCuenta(rs.getString("plancuenta")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setCodigo(String scodigo) {
        this.scodigo = scodigo;
    }
    
    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }
    
    public void setIdCategoria(int iidCategoria) {
        this.iidCategoria = iidCategoria;
    }
    
    public void setCategoria(String scategoria) {
        this.scategoria = scategoria;
    }
    
    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }
    
    public void setUnidadMedida(String sunidadMedida) {
        this.sunidadMedida = sunidadMedida;
    }
    
    public void setUnidadCompra(String sunidadCompra) {
        this.sunidadCompra = sunidadCompra;
    }
    
    public void setConversion(double uconversion) {
        this.uconversion = uconversion;
    }
    
    public void setStock(double ustock) {
        this.ustock = ustock;
    }
    
    public void setMinimo(double uminimo) {
        this.uminimo = uminimo;
    }
    
    public void setCosto(double ucosto) {
        this.ucosto = ucosto;
    }

    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }
    
    public void setNroPlanCuenta(String snroPlanCuenta) {
        this.snroPlanCuenta = snroPlanCuenta;
    }
    
    public void setNroPlanCuenta1(String snroPlanCuenta1) {
        this.snroPlanCuenta1 = snroPlanCuenta1;
    }
    
    public void setPlanCuenta(String splanCuenta) {
        this.splanCuenta = splanCuenta;
    }
    
    public String getCodigo() {
        return this.scodigo;
    }
    
    public int getIdFilial() {
        return this.iidFilial;
    }
    
    public String getFilial() {
        return this.sfilial;
    }
    
    public int getIdCategoria() {
        return this.iidCategoria;
    }
    
    public String getCategoria() {
        return this.scategoria;
    }
    
    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }

    public String getUnidadMedida() {
        return this.sunidadMedida;
    }

    public String getUnidadCompra() {
        return this.sunidadCompra;
    }

    public double getConversion() {
        return this.uconversion;
    }

    public double getStock() {
        return this.ustock;
    }

    public double getMinimo() {
        return this.uminimo;
    }

    public double getCosto() {
        return this.ucosto;
    }

    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }
    
    public String getNroPlanCuenta() {
        return this.snroPlanCuenta;
    }
    
    public String getNroPlanCuenta1() {
        return this.snroPlanCuenta1;
    }
    
    public String getPlanCuenta() {
        return this.splanCuenta;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getCodigo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CODIGO;
            bvalido = false;
        }
        if (this.getConversion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CONVERSION;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT compras.articulo("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCodigo())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdCategoria()+","+
            this.getIdUnidadMedida()+","+
            utilitario.utiCadena.getTextoGuardado(this.getUnidadCompra())+","+
            this.getConversion()+","+
            this.getIdPlanCuenta()+","+
            this.getCosto()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Categoría", "categoria", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(3, "Unidad", "unidadmedida", new generico.entLista().tipo_texto));
        return lst;
    }
    
}