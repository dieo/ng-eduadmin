/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleCasoJuridico {

    protected int iid;
    protected int iidCasoJuridico;
    protected int iidAsesor;
    protected String sasesor;
    protected java.util.Date dfechaEvento;
    protected String sevento;
    
    protected short hestadoRegistro;
    protected String smensaje;
    
    public static final int LONGITUD_EVENTO = 250;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ASESOR = "Asesor (no vacío)";
    public static final String TEXTO_FECHA_EVENTO = "Fecha de Evento (no vacía)";
    public static final String TEXTO_EVENTO = "Evento (no vacío, hasta " + LONGITUD_EVENTO + " caracteres)";

    public entDetalleCasoJuridico() {
        this.iid = 0;
        this.iidCasoJuridico = 0;
        this.iidAsesor = 0;
        this.sasesor = "";
        this.dfechaEvento = null;
        this.sevento = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleCasoJuridico(int iid, int iidCasoJuridico, int iidAsesor, String sasesor, java.util.Date dfechaEvento, String sevento, short hestadoRegistro) {
        this.iid = iid;
        this.iidCasoJuridico = iidCasoJuridico;
        this.iidAsesor = iidAsesor;
        this.sasesor = sasesor;
        this.dfechaEvento = dfechaEvento;
        this.sevento = sevento;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidCasoJuridico, int iidAsesor, String sasesor, java.util.Date dfechaEvento, String sevento, short hestadoRegistro) {
        this.iid = iid;
        this.iidCasoJuridico = iidCasoJuridico;
        this.iidAsesor = iidAsesor;
        this.sasesor = sasesor;
        this.dfechaEvento = dfechaEvento;
        this.sevento = sevento;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleCasoJuridico copiar(entDetalleCasoJuridico destino) {
        destino.setEntidad(this.getId(), this.getIdCasoJuridico(), this.getIdAsesor(), this.getAsesor(), this.getFechaEvento(), this.getEvento(), this.getEstadoRegistro());
        return destino;
    }
    
    public entDetalleCasoJuridico cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdCasoJuridico(rs.getInt("idcasojuridico")); }
        catch(Exception e) {}
        try { this.setIdAsesor(rs.getInt("idasesor")); }
        catch(Exception e) {}
        try { this.setAsesor(rs.getString("asesor")); }
        catch(Exception e) {}
        try { this.setFechaEvento(rs.getDate("fechaevento")); }
        catch(Exception e) {}
        try { this.setEvento(rs.getString("evento")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdCasoJuridico(int iidCasoJuridico) {
        this.iidCasoJuridico = iidCasoJuridico;
    }

    public void setIdAsesor(int iidAsesor) {
        this.iidAsesor = iidAsesor;
    }

    public void setAsesor(String sasesor) {
        this.sasesor = sasesor;
    }

    public void setFechaEvento(java.util.Date dfechaEvento) {
        this.dfechaEvento = dfechaEvento;
    }

    public void setEvento(String sevento) {
        this.sevento = sevento;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdCasoJuridico() {
        return this.iidCasoJuridico;
    }

    public int getIdAsesor() {
        return this.iidAsesor;
    }

    public String getAsesor() {
        return this.sasesor;
    }

    public java.util.Date getFechaEvento() {
        return this.dfechaEvento;
    }

    public String getEvento() {
        return this.sevento;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdAsesor()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ASESOR;
            bvalido = false;
        }
        if (this.getFechaEvento()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_EVENTO;
            bvalido = false;
        }
        if (this.getEvento().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_EVENTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detallecasojuridico("+
            this.getId()+","+
            this.getIdCasoJuridico()+","+
            this.getIdAsesor()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaEvento())+","+
            utilitario.utiCadena.getTextoGuardado(this.getEvento())+","+
            this.getEstadoRegistro()+")";
    }

}
