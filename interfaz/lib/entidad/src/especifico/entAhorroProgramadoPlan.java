/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entAhorroProgramadoPlan extends generico.entGenerica {

    private double uimporteBase;
    private int iplazo;
    private double utasaInteres;
    private int iidMoneda;
    private String smoneda;
    private boolean bactivo;
    private int idiaExtraccion;

    public static final int LONGITUD_DESCRIPCION = 20;
    public static final int INICIO_IMPORTE_BASE = 1;
    public static final int FIN_IMPORTE_BASE = 9999999;
    public static final int INICIO_PLAZO = 1;
    public static final int FIN_PLAZO = 120;
    public static final int INICIO_TASA_INTERES = 1;
    public static final int FIN_TASA_INTERES = 120;
    public static final int INICIO_DIA_EXTRACCION = 1;
    public static final int FIN_DIA_EXTRACCION = 31;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción (no vacía)";
    public static final String TEXTO_IMPORTE_BASE = "Importe base (desde " + INICIO_IMPORTE_BASE + " hasta " + FIN_IMPORTE_BASE +")";
    public static final String TEXTO_PLAZO = "Plazo (desde " + INICIO_PLAZO + " hasta " + FIN_PLAZO +")";
    public static final String TEXTO_TASA_INTERES = "Tasa de Interes Anual (desde " + INICIO_TASA_INTERES + " hasta " + FIN_TASA_INTERES +")";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_ACTIVO = "Activo";
    public static final String TEXTO_DIA_EXTRACCION = "Día de extracción del Capital (desde " + INICIO_DIA_EXTRACCION + " hasta " + FIN_DIA_EXTRACCION +")";

    public entAhorroProgramadoPlan() {
        super();
    }

    public entAhorroProgramadoPlan(int iid, String sdescripcion, double uimporteBase, int iplazo, double utasaInteres, int iidMoneda, String smoneda, boolean bactivo, int idiaExtraccion) {
        super(iid, sdescripcion);
        this.uimporteBase = uimporteBase;
        this.iplazo = iplazo;
        this.utasaInteres = utasaInteres;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.bactivo = bactivo;
        this.idiaExtraccion = idiaExtraccion;
    }

    public entAhorroProgramadoPlan(int iid, String sdescripcion, double uimporteBase, int iplazo, double utasaInteres, int iidMoneda, String smoneda, boolean bactivo, int idiaExtraccion, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.uimporteBase = uimporteBase;
        this.iplazo = iplazo;
        this.utasaInteres = utasaInteres;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.bactivo = bactivo;
        this.idiaExtraccion = idiaExtraccion;
    }

    public void setEntidad(int iid, String sdescripcion, double uimporteBase, int iplazo, double utasaInteres, int iidMoneda, String smoneda, boolean bactivo, int idiaExtraccion, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.uimporteBase = uimporteBase;
        this.iplazo = iplazo;
        this.utasaInteres = utasaInteres;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.bactivo = bactivo;
        this.idiaExtraccion = idiaExtraccion;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public entAhorroProgramadoPlan copiar(entAhorroProgramadoPlan destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getImporteBase(), this.getPlazo(), this.getTasaInteres(), this.getIdMoneda(), this.getMoneda(), this.getActivo(), this.getDiaExtraccion(), this.getEstadoRegistro());
        return destino;
    }

    public entAhorroProgramadoPlan cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setImporteBase(rs.getDouble("importebase")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setDiaExtraccion(rs.getInt("diaextraccion")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setImporteBase(double uimporteBase) {
        this.uimporteBase = uimporteBase;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setTasaInteres(double utasaInteres) {
        this.utasaInteres = utasaInteres;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public void setDiaExtraccion(int idiaExtraccion) {
        this.idiaExtraccion = idiaExtraccion;
    }

    public double getImporteBase() {
        return this.uimporteBase;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public double getTasaInteres() {
        return this.utasaInteres;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public int getDiaExtraccion() {
        return this.idiaExtraccion;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getImporteBase()<INICIO_IMPORTE_BASE || this.getImporteBase()>FIN_IMPORTE_BASE) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_IMPORTE_BASE;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getPlazo()<INICIO_PLAZO || this.getPlazo()>FIN_PLAZO) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO;
            bvalido = false;
        }
        if (this.getTasaInteres()<INICIO_TASA_INTERES || this.getTasaInteres()>FIN_TASA_INTERES) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TASA_INTERES;
            bvalido = false;
        }
        if (this.getDiaExtraccion()<INICIO_DIA_EXTRACCION || this.getDiaExtraccion()>FIN_DIA_EXTRACCION) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DIA_EXTRACCION;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT planahorroprogramado("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getImporteBase()+","+
            this.getPlazo()+","+
            this.getTasaInteres()+","+
            this.getIdMoneda()+","+
            this.getActivo()+","+
            this.getDiaExtraccion()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripcion", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Importe Base", "importebase", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(3, "Moneda", "moneda", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Plazo", "plazo", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(5, "Tasa de Interes", "tasainteres", generico.entLista.tipo_numero));
        return lst;
    }

}
