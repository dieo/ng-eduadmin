/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entcTransferencias {

    private int iid;
    private java.util.Date dfecha;
    private int iidUsuario;
    private String susuario;
    private int iidArticulo;
    private String sdescripcion;
    private int iidFilialOrigen;
    private String sfilialOrigen;
    private int iidFilialDestino;
    private String sfilialDestino;
    private double ucantidad;
    private String sobservacion;
    private int iidEstado;
    private String sestado;
    
    private java.util.Date dfechaanulado;
    private String sobservacionanulado;
    
    private short hidEstado;
    private String smensaje;
    
    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_FECHA = "Fecha de la transferencia (no editable)";
    public static final String TEXTO_USUARIO = "Usuario que realiza la transferencia (no vacío)";
    public static final String TEXTO_PRODUCTO = "Producto a transferir (no vacío)";
    public static final String TEXTO_FILIAL_ORIGEN = "Filial desde donde se transfiere (no vacío)";
    public static final String TEXTO_FILIAL_DESTINO = "Filial de destino de la transferencia (no vacío)";
    public static final String TEXTO_CANTIDAD = "Cantidad (no vacío, valor positivo)";
    public static final String TEXTO_OBSERVACION = "Motivo de la Transferencia (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_ESTADO = "Estado de la transferencia (no editable)";
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación de la transferencia (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Motivo de la Anulación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    

    public entcTransferencias() {
        this.iid = 0;
        this.dfecha = null;
        this.iidUsuario = 0;
        this.susuario = "";
        this.iidArticulo = 0;
        this.sdescripcion = "";
        this.iidFilialOrigen = 0;
        this.sfilialOrigen = "";
        this.iidFilialDestino = 0;
        this.sfilialDestino = "";
        this.ucantidad = 0.0;
        this.sobservacion = "";
        this.iidEstado = 0;
        this.sestado = "";
        this.dfechaanulado = null;
        this.sobservacionanulado = "";
        this.hidEstado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entcTransferencias(int iid, java.util.Date dfecha, int iidUsuario, String susuario, int iidArticulo, String sdescripcion, int iidFilialOrigen, String sfilialOrigen, int iidFilialDestino, String sfilialDestino, double ucantidad, String sobservacion, int iidEstado, String sestado, java.util.Date dfechaanulado, String sobservacionanulado, short hidEstado) {
        this.iid = iid;
        this.dfecha = dfecha;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.iidFilialOrigen = iidFilialOrigen;
        this.sfilialOrigen = sfilialOrigen;
        this.iidFilialDestino = iidFilialDestino;
        this.sfilialDestino = sfilialDestino;
        this.ucantidad = ucantidad;
        this.sobservacion = sobservacion;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.dfechaanulado = dfechaanulado;
        this.sobservacionanulado = sobservacionanulado;
        this.hidEstado = hidEstado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, java.util.Date dfecha, int iidUsuario, String susuario, int iidArticulo, String sdescripcion, int iidFilialOrigen, String sfilialOrigen, int iidFilialDestino, String sfilialDestino, double ucantidad, String sobservacion, int iidEstado, String sestado, java.util.Date dfechaanulado, String sobservacionanulado, short hidEstado) {
        this.iid = iid;
        this.dfecha = dfecha;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.iidFilialOrigen = iidFilialOrigen;
        this.sfilialOrigen = sfilialOrigen;
        this.iidFilialDestino = iidFilialDestino;
        this.sfilialDestino = sfilialDestino;
        this.ucantidad = ucantidad;
        this.sobservacion = sobservacion;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.dfechaanulado = dfechaanulado;
        this.sobservacionanulado = sobservacionanulado;
        this.hidEstado = hidEstado;
    }

    public entcTransferencias copiar(entcTransferencias destino) {
        destino.setEntidad(this.getId(), this.getFecha(), this.getIdUsuario(), this.getUsuario(), this.getIdArticulo(), this.getDescripcion(), this.getIdFilialOrigen(), this.getFilialOrigen(), this.getIdFilialDestino(), this.getFilialDestino(), this.getCantidad(), this.getObservacion(), this.getIdEstado(), this.getEstado(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcTransferencias cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuario(rs.getString("usuario")); }
        catch(Exception e) { }
        try { this.setIdArticulo(rs.getInt("idarticulo")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("articulo")); }
        catch(Exception e) {}
        try { this.setIdFilialOrigen(rs.getInt("idfilialorigen")); }
        catch(Exception e) { }
        try { this.setFilialOrigen(rs.getString("filialorigen")); }
        catch(Exception e) { }
        try { this.setIdFilialDestino(rs.getInt("idfilialdestino")); }
        catch(Exception e) {}
        try { this.setFilialDestino(rs.getString("filialdestino")); }
        catch(Exception e) {}
        try { this.setCantidad(rs.getDouble("cantidad")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestadotransferencia")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estadotransferencia")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }

    public void setUsuario(String susuario) {
        this.susuario = susuario;
    }
    
    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setIdFilialOrigen(int iidFilialOrigen) {
        this.iidFilialOrigen = iidFilialOrigen;
    }
    
    public void setFilialOrigen(String sfilialOrigen) {
        this.sfilialOrigen = sfilialOrigen;
    }
    
    public void setIdFilialDestino(int iidFilialDestino) {
        this.iidFilialDestino = iidFilialDestino;
    }

    public void setFilialDestino(String sfilialDestino) {
        this.sfilialDestino = sfilialDestino;
    }

    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setFechaAnulado(java.util.Date dfechaanulado) {
        this.dfechaanulado = dfechaanulado;
    }

    public void setObservacionAnulado(String sobservacionanulado) {
        this.sobservacionanulado = sobservacionanulado;
    }

    public void setEstadoRegistro(short hidEstado) {
        this.hidEstado = hidEstado;
    }

    public int getId() {
        return this.iid;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }

    public String getUsuario() {
        return this.susuario;
    }
    
    public int getIdArticulo() {
        return this.iidArticulo;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public int getIdFilialOrigen() {
        return this.iidFilialOrigen;
    }
    
    public String getFilialOrigen() {
        return this.sfilialOrigen;
    }
    
    public int getIdFilialDestino() {
        return this.iidFilialDestino;
    }

    public String getFilialDestino() {
        return this.sfilialDestino;
    }

    public double getCantidad() {
        return this.ucantidad;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaanulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionanulado;
    }

    public short getEstadoRegistro() {
        return this.hidEstado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdFilialOrigen()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FILIAL_ORIGEN;
            bvalido = false;
        }
        if (this.getIdFilialDestino()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FILIAL_DESTINO;
            bvalido = false;
        }
        if (this.getIdFilialOrigen()==this.getIdFilialDestino()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La filial de origen y destino deben ser distintas.";
            bvalido = false;
        }
        if (this.getCantidad()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CANTIDAD;
            bvalido = false;
        }
        return bvalido;
    }
    
     public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.transferencia("+
            this.getId()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFecha())+","+
            this.getIdUsuario()+","+
            this.getIdArticulo()+","+
            this.getIdFilialOrigen()+","+
            this.getIdFilialDestino()+","+
            this.getCantidad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getIdEstado()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha", "fecha", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Nº de Transferencia", "id", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(3, "Artículo", "articulo", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(4, "Origen", "filialorigen", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(5, "Destino", "filialdestino", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(6, "Estado", "estadotransferencia", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
