/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbChequeEmitido {

    private int iid;
    private int inumeroSolicitud;
    private int iidBancoCuenta;
    private String snumerocuenta;
    private String sbancocuenta;
    private String sserie;
    private int inumerocheque;
    private java.util.Date dfechaEmisionCheque;
    private java.util.Date dfechaCobro;
    private int inumeroorden;
    private double dmonto;
    private int iidEstado;
    private String sestado;
    private int iidReceptorCheque;
    protected String scedulaRuc;
    protected String sreceptorCheque;
    protected String sdescripcion;
    private int iidTalonario;
    private int iidCentroCosto;
    private String scentroCosto;
    private String shora;
    private int iidFilial;
    private String sfilial;
    
    private short hestadoRegistro;
    private String smensaje;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    public static final int LONGITUD_DESCRIPCION = 100;
    public static final int LONGITUD_OBSERVACION = 100;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ESTADO = "Estado del Cheque (no editable)";
    public static final String TEXTO_SERIE = "Serie del talonario actual (no editable)";
    public static final String TEXTO_NUMERO_CHEQUE = "Número del Cheque (no editable)";
    public static final String TEXTO_NUMERO_ORDEN = "Número de la Orden de Pago (no editable)";
    public static final String TEXTO_CUENTA_BANCO = "Cuenta Banco (no editable)";
    public static final String TEXTO_MONTO = "Monto del Cheque (no editable)";
    public static final String TEXTO_FECHA_EMISION = "Fecha de emisión del Cheque (no editable)";
    public static final String TEXTO_HORA = "Hora de emisión del Cheque (no editable)";
    public static final String TEXTO_FECHA_COBRO = "Fecha de cobro del Cheque (no editable)";
    public static final String TEXTO_RECEPTOR = "Receptor del Cheque (no editable)";
    public static final String TEXTO_CENTRO_COSTO = "Centro de Costo (no editable)";
    public static final String TEXTO_FILIAL = "Filial (no vacía)";
    
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción del concepto (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación del porqué se anuló (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    
    
    public entbChequeEmitido() {
        this.iid = 0;
        this.inumeroSolicitud = 0;
        this.iidBancoCuenta = 0;
        this.snumerocuenta = "";
        this.sbancocuenta = "";
        this.sserie = "";
        this.inumerocheque = 0;
        this.dfechaEmisionCheque = null;
        this.dfechaCobro = null;
        this.inumeroorden = 0;
        this.dmonto = 0.0;
        this.iidEstado = 0;
        this.sestado = "";
        this.iidReceptorCheque = 0;
        this.scedulaRuc = "";
        this.sreceptorCheque = "";
        this.iidTalonario = 0;
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.sdescripcion = "";
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.shora = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbChequeEmitido(int iid, int inumeroSolicitud, int iidBancoCuenta, String snumerocuenta, String sbancocuenta, String sserie, int inumerocheque, java.util.Date dfechaEmisionCheque, java.util.Date dfechaCobro, int inumeroorden, double dmonto, int iidEstado, String sestado, int iidReceptorCheque, String scedulaRuc, String sreceptorCheque, int iidTalonario, java.util.Date dfechaAnulado, String sobservacionAnulado, String sdescripcion, int iidCentroCosto, String scentroCosto, String shora, int iidFilial, String sfilial, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroSolicitud = inumeroSolicitud;
        this.iidBancoCuenta = iidBancoCuenta;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.sserie = sserie;
        this.inumerocheque = inumerocheque;
        this.dfechaEmisionCheque = dfechaEmisionCheque;
        this.dfechaCobro = dfechaCobro;
        this.inumeroorden = inumeroorden;
        this.dmonto = dmonto;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidReceptorCheque = iidReceptorCheque;
        this.scedulaRuc = scedulaRuc;
        this.sreceptorCheque = sreceptorCheque;
        this.iidTalonario = iidTalonario;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.sdescripcion = sdescripcion;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.shora = shora;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroSolicitud, int iidBancoCuenta, String snumerocuenta, String sbancocuenta, String sserie, int inumerocheque, java.util.Date dfechaEmisionCheque, java.util.Date dfechaCobro, int inumeroorden, double dmonto, int iidEstado, String sestado, int iidReceptorCheque, String scedulaRuc, String sreceptorCheque, int iidTalonario, java.util.Date dfechaAnulado, String sobservacionAnulado, String sdescripcion, int iidCentroCosto, String scentroCosto, String shora, int iidFilial, String sfilial, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroSolicitud = inumeroSolicitud;
        this.iidBancoCuenta = iidBancoCuenta;
        this.snumerocuenta = snumerocuenta;
        this.sbancocuenta = sbancocuenta;
        this.sserie = sserie;
        this.inumerocheque = inumerocheque;
        this.dfechaEmisionCheque = dfechaEmisionCheque;
        this.dfechaCobro = dfechaCobro;
        this.inumeroorden = inumeroorden;
        this.dmonto = dmonto;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidReceptorCheque = iidReceptorCheque;
        this.scedulaRuc = scedulaRuc;
        this.sreceptorCheque = sreceptorCheque;
        this.iidTalonario = iidTalonario;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.sdescripcion = sdescripcion;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.shora = shora;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entbChequeEmitido copiar(entbChequeEmitido destino) {
        destino.setEntidad(this.getId(), this.getNumeroSolicitud(), this.getIdBancoCuenta(), this.getNumeroCuenta(), this.getBancoCuenta(), this.getSerie(), this.getNumeroCheque(), this.getFechaEmisionCheque(), this.getFechaCobro(), this.getNumeroOrden(), this.getMonto(), this.getIdEstado(), this.getEstado(), this.getIdReceptorCheque(), this.getCedulaRUC(), this.getReceptorCheque(), this.getIdTalonario(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getDescripcion(), this.getIdCentroCosto(), this.getCentroCosto(), this.getHora(), this.getIdFilial(), this.getFilial(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbChequeEmitido cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setIdBancoCuenta(rs.getInt("idbancocuenta")); }
        catch(Exception e) {}
        try { this.setNumeroCuenta(rs.getString("numerocuenta")); }
        catch(Exception e) {}
        try { this.setBancoCuenta(rs.getString("cuentabanco")); }
        catch(Exception e) {}
        try { this.setSerie(rs.getString("serie")); }
        catch(Exception e) {}
        try { this.setNumeroCheque(rs.getInt("numerocheque")); }
        catch(Exception e) {}
        try { this.setFechaEmisionCheque(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaCobro(rs.getDate("fechacobro")); }
        catch(Exception e) {}
        try { this.setNumeroOrden(rs.getInt("numeroorden")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        try { this.setIdEstado(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setIdReceptorCheque(rs.getInt("idreceptor")); }
        catch(Exception e) {}
        try { this.setCedulaRUC(rs.getString("cedularuc")); }
        catch(Exception e) {}
        try { this.setReceptorCheque(rs.getString("receptor")); }
        catch(Exception e) {}
        try { this.setIdCentroCosto(rs.getInt("idcentrocosto")); }
        catch(Exception e) {}
        try { this.setCentroCosto(rs.getString("centrocosto")); }
        catch(Exception e) {}
        try { this.setIdTalonario(rs.getInt("idtalonario")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setHora(rs.getString("hora")); }
        catch(Exception e) {}
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setIdBancoCuenta(int iidBancoCuenta) {
        this.iidBancoCuenta = iidBancoCuenta;
    }

    public void setNumeroCuenta(String snumerocuenta) {
        this.snumerocuenta = snumerocuenta;
    }

    public void setBancoCuenta(String sbancocuenta) {
        this.sbancocuenta = sbancocuenta;
    }

    public void setSerie(String sserie) {
        this.sserie = sserie;
    }

    public void setNumeroCheque(int inumerocheque) {
        this.inumerocheque = inumerocheque;
    }

    public void setFechaEmisionCheque(java.util.Date dfechaEmisionCheque) {
        this.dfechaEmisionCheque = dfechaEmisionCheque;
    }
    
    public void setFechaCobro(java.util.Date dfechaCobro) {
        this.dfechaCobro = dfechaCobro;
    }
    
    public void setNumeroOrden(int inumeroorden) {
        this.inumeroorden = inumeroorden;
    }

    public void setMonto(double dmonto) {
        this.dmonto = dmonto;
    }
    
    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }
    
    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }

    public void setIdReceptorCheque(int iidReceptorCheque) {
        this.iidReceptorCheque = iidReceptorCheque;
    }

    public void setCedulaRUC(String scedulaRuc) {
        this.scedulaRuc = scedulaRuc;
    }

    public void setReceptorCheque(String sreceptorCheque) {
        this.sreceptorCheque = sreceptorCheque;
    }
    
    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }
    
    public void setIdTalonario(int iidTalonario) {
        this.iidTalonario = iidTalonario;
    }
    
    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setHora(String shora) {
        this.shora = shora;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }

    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public int getIdBancoCuenta() {
        return this.iidBancoCuenta;
    }

    public String getNumeroCuenta() {
        return this.snumerocuenta;
    }

    public String getBancoCuenta() {
        return this.sbancocuenta;
    }

    public String getSerie() {
        return this.sserie;
    }
    
    public int getNumeroCheque() {
        return this.inumerocheque;
    }

    public java.util.Date getFechaEmisionCheque() {
        return this.dfechaEmisionCheque;
    }

    public java.util.Date getFechaCobro() {
        return this.dfechaCobro;
    }

    public int getNumeroOrden() {
        return this.inumeroorden;
    }

    public double getMonto() {
        return this.dmonto;
    }
    
    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }
 
    public String getCentroCosto() {
        return this.scentroCosto;
    }
 
    public int getIdReceptorCheque() {
        return this.iidReceptorCheque;
    }

    public String getCedulaRUC() {
        return this.scedulaRuc;
    } 

    public String getReceptorCheque() {
        return this.sreceptorCheque;
    }
 
    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }

    public int getIdTalonario() {
        return this.iidTalonario;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public String getHora() {
        return this.shora;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public int getIdFilial() {
        return this.iidFilial;
    }

    public String getFilial() {
        return this.sfilial;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        return bvalido;
    }

    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        /*return "SELECT banco.emitircheque("+
            this.getNumeroOrden()+","+
            this.getIdBancoCuenta()+","+
            this.getNumeroCheque()+")";*/
        return "SELECT banco.chequeemitido("+
            this.getId()+","+
            this.getIdBancoCuenta()+","+
            this.getNumeroCheque()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaEmisionCheque())+","+
            this.getMonto()+","+
            this.getIdReceptorCheque()+","+
            this.getIdTalonario()+","+
            this.getIdEstado()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaCobro())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdCentroCosto()+","+
            utilitario.utiCadena.getTextoGuardado(this.getHora())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha", "fecha", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(2, "Número Cheque", "numerocheque", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(3, "Cuenta Banco", "cuentabanco", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Estado", "estado", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Cedula", "cedularuc", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Receptor", "receptor", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Centro de Costo", "centrocosto", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(8, "Nº Orden de Pago", "nroordenpago", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(9, "Concepto ", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(10, "Fecha anulación", "fechaanulado", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(11, "Filial", "filial", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(12, "Nº Solicitud", "nrosolicitud", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(13, "Nº Operación", "numerooperacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(14, "Tipo Servicio", "tiposervicio", generico.entLista.tipo_texto));
        return lst;
    }
    
}
