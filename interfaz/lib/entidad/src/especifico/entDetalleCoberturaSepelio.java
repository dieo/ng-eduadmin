/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleCoberturaSepelio {

    protected int iid;
    protected int iidCoberturaSepelio;
    protected int iidDocumento;
    protected String sdocumento;
    protected short cestado;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DOCUMENTO = "Documento (no vacío)";
    
    public entDetalleCoberturaSepelio() {
        this.iid = 0;
        this.iidCoberturaSepelio = 0;
        this.iidDocumento = 0;
        this.sdocumento = "";
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleCoberturaSepelio(int iid, int iidCoberturaSepelio, int iidDocumento, String sdocumento, short cestado) {
        this.iid = iid;
        this.iidCoberturaSepelio = iidCoberturaSepelio;
        this.iidDocumento = iidDocumento;
        this.sdocumento = sdocumento;
        this.cestado = cestado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidCoberturaSepelio, int iidDocumento, String sdocumento, short cestado) {
        this.iid = iid;
        this.iidCoberturaSepelio = iidCoberturaSepelio;
        this.iidDocumento = iidDocumento;
        this.sdocumento = sdocumento;
        this.cestado = cestado;
    }
    
    public entDetalleCoberturaSepelio copiar(entDetalleCoberturaSepelio destino) {
        destino.setEntidad(this.getId(), this.getIdCoberturaSepelio(), this.getIdDocumento(), this.getDocumento(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleCoberturaSepelio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdCoberturaSepelio(rs.getInt("idcoberturasepelio")); }
        catch(Exception e) {}
        try { this.setIdDocumento(rs.getInt("iddocumento")); }
        catch(Exception e) {}
        try { this.setDocumento(rs.getString("documento")); }
        catch(Exception e) {}
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdCoberturaSepelio(int iidCoberturaSepelio) {
        this.iidCoberturaSepelio = iidCoberturaSepelio;
    }

    public void setIdDocumento(int iidDocumento) {
        this.iidDocumento = iidDocumento;
    }

    public void setDocumento(String sdocumento) {
        this.sdocumento = sdocumento;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdCoberturaSepelio() {
        return this.iidCoberturaSepelio;
    }

    public int getIdDocumento() {
        return this.iidDocumento;
    }

    public String getDocumento() {
        return this.sdocumento;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdDocumento() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DOCUMENTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detallecoberturasepelio("+
            this.getId()+","+
            this.getIdCoberturaSepelio()+","+
            this.getIdDocumento()+","+
            this.getEstadoRegistro()+")";
    }

}
