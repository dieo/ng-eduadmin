/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entCuenta extends generico.entGenerica {

    protected int iidTipoCuenta;
    protected String stipoCuenta;
    protected String sprioridad;
    protected double uinteresMoratorio;
    protected int iidTipoDocumento;
    protected String stipoDocumento;
    protected int iidImpuesto;
    protected String simpuesto;
    protected boolean bactivo;
    protected double utasa;
    protected String scodigo;

    public static final int LONGITUD_DESCRIPCION = 50;
    public static final int LONGITUD_PRIORIDAD = 11;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_TIPO_CUENTA = "Tipo Cuenta (no vacía)";
    public static final String TEXTO_PRIORIDAD = "Prioridad (no vacía, de " + LONGITUD_PRIORIDAD + " caracteres)";
    public static final String TEXTO_INTERES_MORATORIO = "Interés Moratorio (valor positivo)";
    public static final String TEXTO_TIPO_DOCUMENTO = "Tipo Documento (no vacío)";
    public static final String TEXTO_IMPUESTO = "Impuesto (no vacío)";
    public static final String TEXTO_ACTIVO = "Activo";
    public static final String TEXTO_CODIGO = "Código (no vacío)";

    public entCuenta() {
        super();
        this.iidTipoCuenta = 0;
        this.stipoCuenta = "";
        this.sprioridad = "";
        this.uinteresMoratorio = 0.0;
        this.iidTipoDocumento = 0;
        this.stipoDocumento = "";
        this.iidImpuesto = 0;
        this.simpuesto = "";
        this.bactivo = false;
        this.utasa = 0.0;
        this.scodigo = "";
    }

    public entCuenta(int iid, String sdescripcion, int iidTipoCuenta, String stipoCuenta, String sprioridad, double uinteresMoratorio, int iidTipoDocumento, String stipoDocumento, int iidImpuesto, String simpuesto, boolean bactivo, double utasa, String scodigo, String sestado) {
        super(iid, sdescripcion);
        this.iidTipoCuenta = iidTipoCuenta;
        this.stipoCuenta = stipoCuenta;
        this.sprioridad = sprioridad;
        this.uinteresMoratorio = uinteresMoratorio;
        this.iidTipoDocumento = iidTipoDocumento;
        this.stipoDocumento = stipoDocumento;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.bactivo = bactivo;
        this.utasa = utasa;
        this.scodigo = scodigo;
    }

    public entCuenta(int iid, String sdescripcion, int iidTipoCuenta, String stipoCuenta, String sprioridad, double uinteresMoratorio, int iidTipoDocumento, String stipoDocumento, int iidImpuesto, String simpuesto, boolean bactivo, double utasa, String scodigo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidTipoCuenta = iidTipoCuenta;
        this.stipoCuenta = stipoCuenta;
        this.sprioridad = sprioridad;
        this.uinteresMoratorio = uinteresMoratorio;
        this.iidTipoDocumento = iidTipoDocumento;
        this.stipoDocumento = stipoDocumento;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.bactivo = bactivo;
        this.bactivo = bactivo;
        this.scodigo = scodigo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, String sdescripcion, int iidTipoCuenta, String stipoCuenta, String sprioridad, double uinteresMoratorio, int iidTipoDocumento, String stipoDocumento, int iidImpuesto, String simpuesto, boolean bactivo, double utasa, String scodigo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidTipoCuenta = iidTipoCuenta;
        this.stipoCuenta = stipoCuenta;
        this.sprioridad = sprioridad;
        this.uinteresMoratorio = uinteresMoratorio;
        this.iidTipoDocumento = iidTipoDocumento;
        this.stipoDocumento = stipoDocumento;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.bactivo = bactivo;
        this.bactivo = bactivo;
        this.scodigo = scodigo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entCuenta copiar(entCuenta destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdTipoCuenta(), this.getTipoCuenta(), this.getPrioridad(), this.getInteresMoratorio(), this.getIdTipoDocumento(), this.getTipoDocumento(), this.getIdImpuesto(), this.getImpuesto(), this.getActivo(), this.getTasa(), this.getCodigo(), this.getEstadoRegistro());
        return destino;
    }

    public entCuenta cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdTipoCuenta(rs.getInt("idtipocuenta")); }
        catch(Exception e) {}
        try { this.setTipoCuenta(rs.getString("tipocuenta")); }
        catch(Exception e) {}
        try { this.setPrioridad(rs.getString("prioridad")); }
        catch(Exception e) {}
        try { this.setInteresMoratorio(rs.getDouble("interesmoratorio")); }
        catch(Exception e) {}
        try { this.setIdTipoDocumento(rs.getInt("idtipodocumento")); }
        catch(Exception e) {}
        try { this.setTipoDocumento(rs.getString("tipodocumento")); }
        catch(Exception e) {}
        try { this.setIdImpuesto(rs.getInt("idimpuesto")); }
        catch(Exception e) {}
        try { this.setImpuesto(rs.getString("impuesto")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setTasa(rs.getDouble("tasa")); }
        catch(Exception e) {}
        try { this.setCodigo(rs.getString("codigo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdTipoCuenta(int iidTipoCuenta) {
        this.iidTipoCuenta = iidTipoCuenta;
    }

    public void setTipoCuenta(String stipoCuenta) {
        this.stipoCuenta = stipoCuenta;
    }

    public void setPrioridad(String sprioridad) {
        this.sprioridad = sprioridad;
    }

    public void setInteresMoratorio(double uinteresMoratorio) {
        this.uinteresMoratorio = uinteresMoratorio;
    }

    public void setIdTipoDocumento(int iidTipoDocumento) {
        this.iidTipoDocumento = iidTipoDocumento;
    }

    public void setTipoDocumento(String stipoDocumento) {
        this.stipoDocumento = stipoDocumento;
    }

    public void setIdImpuesto(int iidImpuesto) {
        this.iidImpuesto = iidImpuesto;
    }

    public void setImpuesto(String simpuesto) {
        this.simpuesto = simpuesto;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setTasa(double utasa) {
        this.utasa = utasa;
    }
    
    public void setCodigo(String scodigo) {
        this.scodigo = scodigo;
    }
    
    public int getIdTipoCuenta() {
        return this.iidTipoCuenta;
    }

    public String getTipoCuenta() {
        return this.stipoCuenta;
    }

    public String getPrioridad() {
        return this.sprioridad;
    }

    public double getInteresMoratorio() {
        return this.uinteresMoratorio;
    }

    public int getIdTipoDocumento() {
        return this.iidTipoDocumento;
    }

    public String getTipoDocumento() {
        return this.stipoDocumento;
    }

    public int getIdImpuesto() {
        return this.iidImpuesto;
    }

    public String getImpuesto() {
        return this.simpuesto;
    }

    public boolean getActivo() {
        return this.bactivo;
    }
    
    public double getTasa() {
        return this.utasa;
    }
    
    public String getCodigo() {
        return this.scodigo;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdTipoCuenta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_CUENTA;
            bvalido = false;
        }
        if (this.getPrioridad().trim().isEmpty() || this.getPrioridad().trim().length()!=11) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PRIORIDAD;
            bvalido = false;
        }
        if (this.getCodigo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CODIGO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT cuenta("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdTipoCuenta()+","+
            utilitario.utiCadena.getTextoGuardado(this.getPrioridad())+","+
            this.getInteresMoratorio()+","+
            this.getActivo()+","+
            this.getIdTipoDocumento()+","+
            this.getIdImpuesto()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCodigo())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Tipo Cuenta", "tipocuenta", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Prioridad", "prioridad", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Tipo Documento", "tipodocumento", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Impuesto", "impuesto", generico.entLista.tipo_texto));
        return lst;
    }
    
    public java.util.ArrayList getListaCodigo() {
        java.util.ArrayList lst = new java.util.ArrayList();
        lst.add("---");
        lst.add("ACA");
        lst.add("ACS");
        lst.add("CIN");
        lst.add("GAD");
        lst.add("IMO");
        lst.add("IPU");
        lst.add("MSE");
        lst.add("OCR");
        lst.add("PFI");
        lst.add("PMU");
        lst.add("ROC");
        lst.add("RPR");
        lst.add("SOL");
        return lst;
    }
    
}
