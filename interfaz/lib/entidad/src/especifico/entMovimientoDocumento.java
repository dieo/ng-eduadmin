/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMovimientoDocumento {

    protected int iid;
    protected int iidCobertura;
    protected int iidDocumento;
    protected String sdocumento;
    protected short cestado;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DOCUMENTO = "Documento (no vacío)";
    
    public entMovimientoDocumento() {
        this.iid = 0;
        this.iidCobertura = 0;
        this.iidDocumento = 0;
        this.sdocumento = "";
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entMovimientoDocumento(int iid, int iidCobertura, int iidDocumento, String sdocumento, short cestado) {
        this.iid = iid;
        this.iidCobertura = iidCobertura;
        this.iidDocumento = iidDocumento;
        this.sdocumento = sdocumento;
        this.cestado = cestado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidCobertura, int iidDocumento, String sdocumento, short cestado) {
        this.iid = iid;
        this.iidCobertura = iidCobertura;
        this.iidDocumento = iidDocumento;
        this.sdocumento = sdocumento;
        this.cestado = cestado;
    }
    
    public entMovimientoDocumento copiar(entMovimientoDocumento destino) {
        destino.setEntidad(this.getId(), this.getIdCobertura(), this.getIdDocumento(), this.getDocumento(), this.getEstadoRegistro());
        return destino;
    }

    public entMovimientoDocumento cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdCobertura(rs.getInt("idcobertura")); }
        catch(Exception e) {}
        try { this.setIdDocumento(rs.getInt("iddocumento")); }
        catch(Exception e) {}
        try { this.setDocumento(rs.getString("documento")); }
        catch(Exception e) {}
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdCobertura(int iidCobertura) {
        this.iidCobertura = iidCobertura;
    }

    public void setIdDocumento(int iidDocumento) {
        this.iidDocumento = iidDocumento;
    }

    public void setDocumento(String sdocumento) {
        this.sdocumento = sdocumento;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdCobertura() {
        return this.iidCobertura;
    }

    public int getIdDocumento() {
        return this.iidDocumento;
    }

    public String getDocumento() {
        return this.sdocumento;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdDocumento() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DOCUMENTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detallecobertura("+
            this.getId()+","+
            this.getIdCobertura()+","+
            this.getIdDocumento()+","+
            this.getEstadoRegistro()+")";
    }

}
