/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetallePlanillaEnvio {

    private int iid;
    private int iidPlanillaEnvio;
    private String splanillaEnvio;
    private int iidPlanillaMovimiento;
    private String splanillaMovimiento;
    protected short hestadoRegistro;

    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_PLANILLA_ENVIO = "Planilla de Envío (no vacía)";
    public static final String TEXTO_PLANILLA_MOVIMIENTO = "Planilla de Movimiento (no vacía)";
    
    public entDetallePlanillaEnvio() {
        this.iid = 0;
        this.iidPlanillaEnvio = 0;
        this.splanillaEnvio = "";
        this.iidPlanillaMovimiento = 0;
        this.splanillaMovimiento = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetallePlanillaEnvio(int iid, int iidPlanillaEnvio, String splanillaEnvio, int iidPlanillaMovimiento, String splanillaMovimiento, short hestadoRegistro) {
        this.iid = iid;
        this.iidPlanillaEnvio = iidPlanillaEnvio;
        this.splanillaEnvio = splanillaEnvio;
        this.iidPlanillaMovimiento = iidPlanillaMovimiento;
        this.splanillaMovimiento = splanillaMovimiento;
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidPlanillaEnvio, String splanillaEnvio, int iidPlanillaMovimiento, String splanillaMovimiento, short hestadoRegistro) {
        this.iid = iid;
        this.iidPlanillaEnvio = iidPlanillaEnvio;
        this.splanillaEnvio = splanillaEnvio;
        this.iidPlanillaMovimiento = iidPlanillaMovimiento;
        this.splanillaMovimiento = splanillaMovimiento;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entDetallePlanillaEnvio copiar(entDetallePlanillaEnvio destino) {
        destino.setEntidad(this.getId(), this.getIdPlanillaEnvio(), this.getPlanillaEnvio(), this.getIdPlanillaMovimiento(), this.getPlanillaMovimiento(), this.getEstadoRegistro());
        return destino;
    }

    public entDetallePlanillaEnvio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdPlanillaEnvio(rs.getInt("idplanillaenvio")); }
        catch(Exception e) {}
        try { this.setPlanillaEnvio(rs.getString("planillaenvio")); }
        catch(Exception e) {}
        try { this.setIdPlanillaMovimiento(rs.getInt("idplanillamovimiento")); }
        catch(Exception e) {}
        try { this.setPlanillaMovimiento(rs.getString("planillamovimiento")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdPlanillaEnvio(int iidPlanillaEnvio) {
        this.iidPlanillaEnvio = iidPlanillaEnvio;
    }

    public void setPlanillaEnvio(String splanillaEnvio) {
        this.splanillaEnvio = splanillaEnvio;
    }

    public void setIdPlanillaMovimiento(int iidPlanillaMovimiento) {
        this.iidPlanillaMovimiento = iidPlanillaMovimiento;
    }

    public void setPlanillaMovimiento(String splanillaMovimiento) {
        this.splanillaMovimiento = splanillaMovimiento;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdPlanillaEnvio() {
        return this.iidPlanillaEnvio;
    }

    public String getPlanillaEnvio() {
        return this.splanillaEnvio;
    }

    public int getIdPlanillaMovimiento() {
        return this.iidPlanillaMovimiento;
    }

    public String getPlanillaMovimiento() {
        return this.splanillaMovimiento;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdPlanillaEnvio() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLANILLA_ENVIO;
            bvalido = false;
        }
        if (this.getIdPlanillaMovimiento()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLANILLA_MOVIMIENTO;
            bvalido = false;
        }
        if (esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje = "La Planilla de Movimiento ya existe en esta u otra Planilla de Envío";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detalleplanillaenvio("+
            this.getId()+","+
            this.getIdPlanillaEnvio()+","+
            this.getIdPlanillaMovimiento()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
}
