/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbTipoMovimiento {

    protected int iid;
    protected String sdescripcion;
    protected int iidPlanCuenta;
    protected int iidDebitoCredito;
    protected String sdebitocredito;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción del Tipo de Movimiento a definir (no vacío)";
    public static final String TEXTO_DEBITO_CREDITO = "Tipo de Movimiento (no vacío, Débito/Crédito)";
    
    public entbTipoMovimiento() {
        this.iid = 0;
        this.sdescripcion = "";
        this.iidDebitoCredito = 0;
        this.sdebitocredito = "";
        this.iidPlanCuenta = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbTipoMovimiento(int iid, String sdescripcion, int iidPlanCuenta, int iidDebitoCredito, String sdebitocredito, short hestadoRegistro) {        
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidDebitoCredito = iidDebitoCredito;
        this.sdebitocredito = sdebitocredito;
        this.iidPlanCuenta = iidPlanCuenta;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, String sdescripcion, int iidPlanCuenta, int iidDebitoCredito, String sdebitocredito, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidDebitoCredito = iidDebitoCredito;
        this.sdebitocredito = sdebitocredito;
        this.iidPlanCuenta = iidPlanCuenta;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entbTipoMovimiento copiar(entbTipoMovimiento destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdPlanCuenta(), this.getIdDebitoCredito(), this.getDebitoCredito(), this.getEstadoRegistro());
        return destino;
    }

    public entbTipoMovimiento cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdDebitoCredito(rs.getInt("iddebitocredito")); }
        catch(Exception e) {}
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) {}
        try { this.setDebitoCredito(rs.getString("debitocredito")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setIdDebitoCredito(int iidDebitoCredito) {
        this.iidDebitoCredito = iidDebitoCredito;
    }
    
    public void setDebitoCredito(String sdebitocredito) {
        this.sdebitocredito = sdebitocredito;
    }
    
    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public String getDescripcion() {
        return this.sdescripcion;
    }

    public int getIdDebitoCredito() {
        return this.iidDebitoCredito;
    }
    
    public String getDebitoCredito() {
        return this.sdebitocredito;
    }
    
    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    } 
     
    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdDebitoCredito()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DEBITO_CREDITO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT banco.tipomovimiento("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdPlanCuenta()+","+
            this.getIdDebitoCredito()+","+
            this.getEstadoRegistro()+")";
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Tipo de Movimiento", "debitocredito", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
