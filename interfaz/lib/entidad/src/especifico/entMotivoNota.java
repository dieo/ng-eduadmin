/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMotivoNota extends generico.entGenerica{
    
    private int iidCuenta;
    private String scuenta;
    private int iidMotivo;
    private String smotivo;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Motivo de Nota de Crédito (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_CUENTA = "Cuenta (no vacía)";
    public static final String TEXTO_MOTIVO = "Motivo (no vacía)";

    public entMotivoNota() {
        super();
        this.iidCuenta = 0;
        this.scuenta = "";
        this.iidMotivo = 0;
        this.smotivo = "";
    }

    public entMotivoNota(int iid, String sdescripcion, int iidCuenta, String scuenta, int iidMotivo, String smotivo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidCuenta = 0;
        this.scuenta = "";
        this.iidMotivo = 0;
        this.smotivo = "";
    }
    
    public void setEntidad(int iid, String sdescripcion, int iidCuenta, String scuenta, int iidMotivo, String smotivo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.iidMotivo = iidMotivo;
        this.smotivo = smotivo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entMotivoNota copiar(entMotivoNota destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdCuenta(), this.getCuenta(), this.getIdMotivo(), this.getMotivo(), this.getEstadoRegistro());
        return destino;
    }

    public entMotivoNota cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setIdMotivo(rs.getInt("idmotivo")); }
        catch(Exception e) {}
        try { this.setMotivo(rs.getString("motivo")); }
        catch(Exception e) {}
        return this;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }
    
    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }
    
    public void setIdMotivo(int iidMotivo) {
        this.iidMotivo = iidMotivo;
    }
    
    public void setMotivo(String smotivo) {
        this.smotivo = smotivo;
    }
    
    public int getIdCuenta() {
        return this.iidCuenta;
    }
    
    public String getCuenta() {
        return this.scuenta;
    }
    
    public int getIdMotivo() {
        return this.iidMotivo;
    }
    
    public String getMotivo() {
        return this.smotivo;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdCuenta()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CUENTA;
            bvalido = false;
        }
        if (this.getIdMotivo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MOTIVO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT motivonota("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdCuenta()+","+
            this.getIdMotivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Cuenta", "cuenta", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Motivo", "motivo", generico.entLista.tipo_texto));
        return lst;
    }
    
}
