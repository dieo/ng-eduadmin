/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entBeneficiarioSepelio extends generico.entGenericaPersona {

    protected java.util.Date dfechaNacimiento;
    protected int iidParentesco;
    protected String sparentesco;
    protected boolean bdiscapacidad;
    protected boolean bdependencia;
    protected int iidTipo;
    protected String stipo;
    protected int iidSepelio;
    protected double umontoAprobado;
    protected int iidMoneda;
    protected String smoneda;
    
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final int LONGITUD_CEDULA = 12;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacío, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_CEDULA = "Apellido (no vacío, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_FECHA_NACIMIENTO = "Fecha de Nacimiento (no vacía)";
    public static final String TEXTO_PARENTESCO = "Parentesco (no vacío)";
    public static final String TEXTO_DISCAPACIDAD = "Discapacidad";
    public static final String TEXTO_DEPENDENCIA = "Dependencia económica";
    public static final String TEXTO_TIPO = "Tipo de Beneficiario";
    public static final String TEXTO_MONTO_APROBADO = "Monto Aprobado (valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacío)";

    public entBeneficiarioSepelio() {
        super();
        this.dfechaNacimiento = null;
        this.iidParentesco = 0;
        this.bdiscapacidad = false;
        this.bdependencia = false;
        this.iidTipo = 0;
        this.stipo = "";
        this.iidSepelio = 0;
        this.umontoAprobado = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
    }

    public entBeneficiarioSepelio(int iid, String snombre, String sapellido, String scedula, java.util.Date dfechaNacimiento, int iidParentesco, String sparentesco, boolean bdiscapacidad, boolean bdependencia, int iidTipo, String stipo, int iidSepelio, double umontoAprobado, int iidMoneda, String smoneda, short hestadoRegistro) {
        super(iid, snombre, sapellido, scedula, hestadoRegistro);
        this.dfechaNacimiento = dfechaNacimiento;
        this.iidParentesco = iidParentesco;
        this.sparentesco = sparentesco;
        this.bdiscapacidad = bdiscapacidad;
        this.bdependencia = bdependencia;
        this.iidTipo = iidTipo;
        this.stipo = stipo;
        this.iidSepelio = iidSepelio;
        this.umontoAprobado = umontoAprobado;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
    }
    
    public void setEntidad(int iid, String snombre, String sapellido, String scedula, java.util.Date dfechaNacimiento, int iidParentesco, String sparentesco, boolean bdiscapacidad, boolean bdependencia, int iidTipo, String stipo, int iidSepelio, double umontoAprobado, int iidMoneda, String smoneda, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre.trim().toUpperCase();
        this.sapellido = sapellido.trim().toUpperCase();
        this.scedula = scedula;
        this.dfechaNacimiento = dfechaNacimiento;
        this.iidParentesco = iidParentesco;
        this.sparentesco = sparentesco;
        this.bdiscapacidad = bdiscapacidad;
        this.bdependencia = bdependencia;
        this.iidTipo = iidTipo;
        this.stipo = stipo;
        this.iidSepelio = iidSepelio;
        this.umontoAprobado = umontoAprobado;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entBeneficiarioSepelio copiar(entBeneficiarioSepelio destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getFechaNacimiento(), this.getIdParentesco(), this.getParentesco(), this.getDiscapacidad(), this.getDependencia(), this.getIdTipo(), this.getTipo(), this.getIdSepelio(), this.getMontoAprobado(), this.getIdMoneda(), this.getMoneda(), this.getEstadoRegistro());
        return destino;
    }
    
    public entBeneficiarioSepelio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setFechaNacimiento(rs.getDate("fechanacimiento")); }
        catch(Exception e) {}
        try { this.setIdParentesco(rs.getInt("idparentesco")); }
        catch(Exception e) {}
        try { this.setParentesco(rs.getString("parentesco")); }
        catch(Exception e) {}
        try { this.setDiscapacidad(rs.getBoolean("discapacidad")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getBoolean("dependencia")); }
        catch(Exception e) {}
        try { this.setIdTipo(rs.getInt("idtipo")); }
        catch(Exception e) {}
        try { this.setTipo(rs.getString("tipo")); }
        catch(Exception e) {}
        try { this.setIdSepelio(rs.getInt("idsepelio")); }
        catch(Exception e) {}
        try { this.setMontoAprobado(rs.getDouble("montoaprobado")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setFechaNacimiento(java.util.Date dfechaNacimiento) {
        this.dfechaNacimiento = dfechaNacimiento;
    }

    public void setIdParentesco(int iidParentesco) {
        this.iidParentesco = iidParentesco;
    }

    public void setParentesco(String sparentesco) {
        this.sparentesco = sparentesco;
    }

    public void setDiscapacidad(Boolean bdiscapacidad) {
        this.bdiscapacidad = bdiscapacidad;
    }

    public void setDependencia(Boolean bdependencia) {
        this.bdependencia = bdependencia;
    }

    public void setIdTipo(int iidTipo) {
        this.iidTipo = iidTipo;
    }

    public void setTipo(String stipo) {
        this.stipo = stipo;
    }

    public void setIdSepelio(int iidSepelio) {
        this.iidSepelio = iidSepelio;
    }

    public void setMontoAprobado(double umontoAprobado) {
        this.umontoAprobado = umontoAprobado;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }

    public java.util.Date getFechaNacimiento() {
        return this.dfechaNacimiento;
    }

    public int getIdParentesco() {
        return this.iidParentesco;
    }

    public String getParentesco() {
        return this.sparentesco;
    }

    public Boolean getDiscapacidad() {
        return this.bdiscapacidad;
    }

    public Boolean getDependencia() {
        return this.bdependencia;
    }

    public int getIdTipo() {
        return this.iidTipo;
    }

    public String getTipo() {
        return this.stipo;
    }

    public int getIdSepelio() {
        return this.iidSepelio;
    }

    public double getMontoAprobado() {
        return this.umontoAprobado;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_APELLIDO;
            bvalido = false;
        }
        if (this.getIdParentesco() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PARENTESCO;
            bvalido = false;
        }
        if (this.getFechaNacimiento() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_NACIMIENTO;
            bvalido = false;
        }
        if (this.getIdTipo() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO;
            bvalido = false;
        }
        if (this.getMontoAprobado() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_APROBADO;
            bvalido = false;
        }
        if (this.getIdMoneda() < 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT beneficiariosepelio("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaNacimiento())+","+
            this.getIdParentesco()+","+
            this.getDiscapacidad()+","+
            this.getDependencia()+","+
            this.getIdTipo()+","+
            this.getIdSepelio()+","+
            this.getMontoAprobado()+","+
            this.getIdMoneda()+","+
            this.getEstadoRegistro()+")";
    }

}