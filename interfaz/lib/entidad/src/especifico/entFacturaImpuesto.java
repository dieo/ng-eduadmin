/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entFacturaImpuesto extends generico.entGenerica{
    
    protected double utasa;
    protected boolean bactivo;

    public static final int LONGITUD_DESCRIPCION = 30;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Impuesto (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_TASA = "Porcentaje de Tasa del Impuesto (valor positivo)";
    public static final String TEXTO_ACTIVO = "Activo / Inactivo";
    
    public entFacturaImpuesto() {
        super();
        this.utasa = 0.0;
        this.bactivo = false;
    }

    public entFacturaImpuesto(int iid, String sdescripcion, double utasa, boolean bactivo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.utasa = utasa;
        this.bactivo = bactivo;
    }

    public void setEntidad(int iid, String sdescripcion, double utasa, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.utasa = utasa;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entFacturaImpuesto copiar(entFacturaImpuesto destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getTasa(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }
    
    public entFacturaImpuesto cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setTasa(rs.getDouble("tasa")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setTasa(double utasa) {
        this.utasa = utasa;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public double getTasa() {
        return this.utasa;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getTasa()<0 || this.getTasa()>100) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TASA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT impuesto("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getTasa()+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Tasa", "tasa", generico.entLista.tipo_numero));
        return lst;
    }
    
}
