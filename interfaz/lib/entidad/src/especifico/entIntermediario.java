/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entIntermediario extends generico.entGenericaPersona {

    protected int iidCargo;
    protected String scargo;
    protected int iidRegional;
    protected String sregional;
    
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacía, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacía, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_CARGO = "Cargo (no vacío)";
    public static final String TEXTO_REGIONAL = "Regional (no vacío)";

    public entIntermediario() {
        super();
        this.iidCargo = 0;
        this.scargo = "";
        this.iidRegional = 0;
        this.sregional = "";
    }

    public entIntermediario(int iid, String snombre, String sapellido, String scedula, int iidCargo, String scargo, int iidRegional, String sregional) {
        super(iid, snombre, sapellido, scedula, (short)0);
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
    }

    public entIntermediario(int iid, String snombre, String sapellido, String scedula, int iidCargo, String scargo, int iidRegional, String sregional, short hestadoRegistro) {
        super(iid, snombre, sapellido, scedula, hestadoRegistro);
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
    }

    public void setEntidad(int iid, String snombre, String sapellido, String scedula, int iidCargo, String scargo, int iidRegional, String sregional, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.scedula = scedula;
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entIntermediario copiar(entIntermediario destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getIdCargo(), this.getCargo(), this.getIdRegional(), this.getRegional(), this.getEstadoRegistro());
        return destino;
    }

    public entIntermediario cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setIdCargo(rs.getInt("idcargo")); }
        catch(Exception e) {}
        try { this.setCargo(rs.getString("cargo")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdCargo(int iidCargo) {
        this.iidCargo = iidCargo;
    }

    public void setCargo(String scargo) {
        this.scargo = scargo;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public int getIdCargo() {
        return this.iidCargo;
    }

    public String getCargo() {
        return this.scargo;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_APELLIDO;
            bvalido = false;
        }
        if (this.getIdCargo()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CARGO;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT intermediario("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            this.getIdCargo()+","+
            this.getIdRegional()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Apellido", "apellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Cargo", "cargo", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Regional", "regional", generico.entLista.tipo_texto));
        return lst;
    }
    
}
