/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entSocio extends generico.entGenericaPersona {
    
    protected int inumeroSocio;
    protected String sdireccion;
    protected int inumeroCasa;
    protected int iidBarrio;
    protected String sbarrio;
    protected int iidCiudadResidencia;
    protected String sciudadResidencia;
    protected String stelefonoCelularPrincipal;
    protected String stelefonoCelular;
    protected String stelefonoLineaBaja;
    protected java.util.Date dfechaNacimiento;
    protected int iidSexo;
    protected String ssexo;
    protected int iidEstado;
    protected String sestado;
    protected int iidEstadoCivil;
    protected String sestadoCivil;
    protected int iidLugarNacimiento;
    protected String slugarNacimiento;
    protected int iidNacionalidad;
    protected String snacionalidad;
    protected int iidProfesion;
    protected String sprofesion;
    protected String semail;
    protected int iidTipoCasa;
    protected String stipoCasa;
    protected int iidViaCobro;
    protected String sviaCobro;
    protected int idiaVencimiento;
    protected java.util.Date dfechaIngreso;
    protected int iidPromotor;
    protected String sruc;
    protected String spin;
    protected double udisponibilidad;
    
    public static final int LONGITUD_CEDULA = 12;
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final int LONGITUD_DIRECCION = 100;
    public static final int LONGITUD_TELEFONO_CELULAR_PRINCIPAL = 10;
    public static final int LONGITUD_TELEFONO_CELULAR = 100;
    public static final int LONGITUD_TELEFONO_LINEA_BAJA = 100;
    public static final int LONGITUD_EMAIL = 50;
    public static final int LONGITUD_RUC = 12;
    public static final int LONGITUD_PIN = 20;
    
    public static final int INICIO_NUMERO_CASA = 0;
    public static final int FIN_NUMERO_CASA = 99999;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CEDULA = "Cédula (no vacía, única, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacío, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_NUMERO_SOCIO = "Número de Socio (no editable)";
    public static final String TEXTO_DIRECCION = "Dirección (no vacía, hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_NUMERO_CASA = "Número de Casa (desde " + INICIO_NUMERO_CASA + " hasta " + FIN_NUMERO_CASA +")";
    public static final String TEXTO_BARRIO = "Barrio (no vacío)";
    public static final String TEXTO_CIUDAD_RESIDENCIA = "Ciudad de Residencia (no vacía)";
    public static final String TEXTO_SEXO = "Sexo (no vacío)";
    public static final String TEXTO_TELEFONO_CELULAR_PRINCIPAL = "Teléfono Celular Principal (hasta " + LONGITUD_TELEFONO_CELULAR_PRINCIPAL + " caracteres)";
    public static final String TEXTO_TELEFONO_CELULAR = "Teléfono Celular (no vacío, hasta " + LONGITUD_TELEFONO_CELULAR + " caracteres)";
    public static final String TEXTO_TELEFONO_LINEA_BAJA = "Teléfono Línea Baja (no vacío, hasta " + LONGITUD_TELEFONO_LINEA_BAJA + " caracteres)";
    public static final String TEXTO_FECHA_NACIMIENTO = "Fecha de Nacimiento (no vacío)";
    public static final String TEXTO_EDAD = "Edad en año, mes y día (no editable)";
    public static final String TEXTO_ESTADO_CIVIL = "Estado Civil (no vacío)";
    public static final String TEXTO_LUGAR_NACIMIENTO = "Lugar de Nacimiento (no vacío)";
    public static final String TEXTO_NACIONALIDAD = "Nacionalidad (no vacía)";
    public static final String TEXTO_PROFESION = "Profesión (no vacía)";
    public static final String TEXTO_EMAIL = "Email (no vacío, hasta " + LONGITUD_EMAIL + " caracteres)";
    public static final String TEXTO_TIPO_CASA = "Tipo de Casa (no vacío)";
    public static final String TEXTO_FECHA_SALIDA = "Fecha de Salida";
    public static final String TEXTO_ESTADO_SOCIO = "Estado de Socio (no vacío)";
    public static final String TEXTO_REGIONAL = "Regional (no vacía)";
    public static final String TEXTO_VIA_COBRO = "Vía de Cobro (no vacía)";
    public static final String TEXTO_DIA_VENCIMIENTO = "Día de Vencimiento (debe contener valores 5,10,15,20,25,31)";
    public static final String TEXTO_PIN = "Pin (valor positivo)";
    public static final String TEXTO_DISPONIBILIDAD = "Porcentaje de disponibilidad (valor positivo)";
    public static final String TEXTO_FECHA_INGRESO = "Fecha de Ingreso (no vacío)";
    public static final String TEXTO_PROMOTOR = "Promotor (no vacío)";
    public static final String TEXTO_PERSONA_MAYOR = "Cantidad de Persona Mayor (no editable)";
    public static final String TEXTO_PERSONA_MENOR = "Cantidad de Persona Menor (no editable)";
    public static final String TEXTO_RUC = "RUC (no vacía, única, hasta " + LONGITUD_RUC + " caracteres)";

    public entSocio() {
        super();
        this.inumeroSocio = 0;
        this.sdireccion = "";
        this.inumeroCasa = 0;
        this.iidBarrio = 0;
        this.sbarrio = "";
        this.iidCiudadResidencia = 0;
        this.sciudadResidencia = "";
        this.stelefonoCelularPrincipal = "";
        this.stelefonoCelular = "";
        this.stelefonoLineaBaja = "";
        this.dfechaNacimiento = null;
        this.iidSexo = 0;
        this.ssexo = "";
        this.iidEstado = 0;
        this.sestado = "";
        this.iidEstadoCivil = 0;
        this.sestadoCivil = "";
        this.iidLugarNacimiento = 0;
        this.slugarNacimiento = "";
        this.iidNacionalidad = 0;
        this.snacionalidad = "";
        this.iidProfesion = 0;
        this.sprofesion = "";
        this.semail = "";
        this.iidTipoCasa = 0;
        this.stipoCasa = "";
        this.iidTipoCasa = 0;
        this.stipoCasa = "";
        this.iidViaCobro = 0;
        this.sviaCobro = "";
        this.idiaVencimiento = 0;
        this.iidPromotor = 0;
        this.sruc = "";
        this.spin = "";
        this.udisponibilidad = 0.0;
        this.dfechaIngreso = null;
    }

    public entSocio(int iid, String snombre, String sapellido, String scedula, int inumeroSocio, String sdireccion, int inumeroCasa, int iidBarrio, String sbarrio, int iidCiudadResidencia, String sciudadResidencia, String stelefonoCelularPrincipal, String stelefonoCelular, String stelefonoLineaBaja, java.util.Date dfechaNacimiento, int iidSexo, String ssexo, int iidEstado, String sestado, int iidEstadoCivil, String sestadoCivil, int iidLugarNacimiento, String slugarNacimiento, int iidNacionalidad, String snacionalidad, int iidProfesion, String sprofesion, String semail, int iidTipoCasa, String stipoCasa, int iidViaCobro, String sviaCobro, int idiaVencimiento, java.util.Date dfechaIngreso, int iidPromotor, String sruc, String spin, double udisponibilidad, short hestadoRegistro) {
        super(iid, snombre, sapellido, scedula, hestadoRegistro);
        this.inumeroSocio = inumeroSocio;
        this.sdireccion = sdireccion.trim().toUpperCase();
        this.inumeroCasa = inumeroCasa;
        this.iidBarrio = iidBarrio;
        this.sbarrio = sbarrio;
        this.iidCiudadResidencia = iidCiudadResidencia;
        this.sciudadResidencia = sciudadResidencia;
        this.stelefonoCelularPrincipal = stelefonoCelularPrincipal.trim();
        this.stelefonoCelular = stelefonoCelular.trim();
        this.stelefonoLineaBaja = stelefonoLineaBaja.trim();
        this.dfechaNacimiento = dfechaNacimiento;
        this.iidSexo = iidSexo;
        this.ssexo = ssexo;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidEstadoCivil = iidEstadoCivil;
        this.sestadoCivil = sestadoCivil;
        this.iidLugarNacimiento = iidLugarNacimiento;
        this.slugarNacimiento = slugarNacimiento;
        this.iidNacionalidad = iidNacionalidad;
        this.snacionalidad = snacionalidad;
        this.iidProfesion = iidProfesion;
        this.sprofesion = sprofesion;
        this.semail = semail.trim().toLowerCase();
        this.iidTipoCasa = iidTipoCasa;
        this.stipoCasa = stipoCasa;
        this.iidViaCobro = iidViaCobro;
        this.sviaCobro = sviaCobro;
        this.idiaVencimiento = idiaVencimiento;
        this.dfechaIngreso = dfechaIngreso;
        this.iidPromotor = iidPromotor;
        this.sruc = sruc;
        this.spin = spin;
        this.udisponibilidad = udisponibilidad;
    }

    public void setEntidad(int iid, String snombre, String sapellido, String scedula, int inumeroSocio, String sdireccion, int inumeroCasa, int iidBarrio, String sbarrio, int iidCiudadResidencia, String sciudadResidencia, String stelefonoCelularPrincipal, String stelefonoCelular, String stelefonoLineaBaja, java.util.Date dfechaNacimiento, int iidSexo, String ssexo, int iidEstado, String sestado, int iidEstadoCivil, String sestadoCivil, int iidLugarNacimiento, String slugarNacimiento, int iidNacionalidad, String snacionalidad, int iidProfesion, String sprofesion, String semail, int iidTipoCasa, String stipoCasa, int iidViaCobro, String sviaCobro, int idiaVencimiento, java.util.Date dfechaIngreso, int iidPromotor, String sruc, String spin, double udisponibilidad, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre.trim().toUpperCase();
        this.sapellido = sapellido.trim().toUpperCase();
        this.scedula = scedula;
        this.inumeroSocio = inumeroSocio;
        this.sdireccion = sdireccion.trim().toUpperCase();
        this.inumeroCasa = inumeroCasa;
        this.iidBarrio = iidBarrio;
        this.sbarrio = sbarrio;
        this.iidCiudadResidencia = iidCiudadResidencia;
        this.sciudadResidencia = sciudadResidencia;
        this.stelefonoCelularPrincipal = stelefonoCelularPrincipal.trim();
        this.stelefonoCelular = stelefonoCelular.trim();
        this.stelefonoLineaBaja = stelefonoLineaBaja.trim();
        this.dfechaNacimiento = dfechaNacimiento;
        this.iidSexo = iidSexo;
        this.ssexo = ssexo;
        this.iidEstado = iidEstado;
        this.sestado = sestado;
        this.iidEstadoCivil = iidEstadoCivil;
        this.sestadoCivil = sestadoCivil;
        this.iidLugarNacimiento = iidLugarNacimiento;
        this.slugarNacimiento = slugarNacimiento;
        this.iidNacionalidad = iidNacionalidad;
        this.snacionalidad = snacionalidad;
        this.iidProfesion = iidProfesion;
        this.sprofesion = sprofesion;
        this.semail = semail.trim().toLowerCase();
        this.iidTipoCasa = iidTipoCasa;
        this.stipoCasa = stipoCasa;
        this.iidViaCobro = iidViaCobro;
        this.sviaCobro = sviaCobro;
        this.idiaVencimiento = idiaVencimiento;
        this.dfechaIngreso = dfechaIngreso;
        this.iidPromotor = iidPromotor;
        this.sruc = sruc;
        this.spin = spin;
        this.udisponibilidad = udisponibilidad;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entSocio copiar(entSocio destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getNumeroSocio(), this.getDireccion(), this.getNumeroCasa(), this.getIdBarrio(), this.getBarrio(), this.getIdCiudadResidencia(), this.getCiudadResidencia(), this.getTelefonoCelularPrincipal(), this.getTelefonoCelular(), this.getTelefonoLineaBaja(), this.getFechaNacimiento(), this.getIdSexo(), this.getSexo(), this.getIdEstado(), this.getEstado(), this.getIdEstadoCivil(), this.getEstadoCivil(), this.getIdLugarNacimiento(), this.getLugarNacimiento(), this.getIdNacionalidad(), this.getNacionalidad(), this.getIdProfesion(), this.getProfesion(), this.getEmail(), this.getIdTipoCasa(), this.getTipoCasa(), this.getIdViaCobro(), this.getViaCobro(), this.getDiaVencimiento(), this.getFechaIngreso(), this.getIdPromotor(), this.getRuc(), this.getPin(), this.getDisponibilidad(), this.getEstadoRegistro());
        return destino;
    }
    
    public entSocio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) { }
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) { }
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) { }
        try { this.setNumeroSocio(rs.getInt("numerosocio")); }
        catch(Exception e) { }
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) { }
        try { this.setNumeroCasa(rs.getInt("numerocasa")); }
        catch(Exception e) { }
        try { this.setIdBarrio(rs.getInt("idbarrio")); }
        catch(Exception e) { }
        try { this.setBarrio(rs.getString("barrio")); }
        catch(Exception e) { }
        try { this.setIdCiudadResidencia(rs.getInt("idciudadresidencia")); }
        catch(Exception e) { }
        try { this.setCiudadResidencia(rs.getString("ciudadresidencia")); }
        catch(Exception e) { }
        try { this.setTelefonoCelularPrincipal(rs.getString("telefonocelularprincipal")); }
        catch(Exception e) { }
        try { this.setTelefonoCelular(rs.getString("telefonocelular")); }
        catch(Exception e) { }
        try { this.setTelefonoLineaBaja(rs.getString("telefonolineabaja")); }
        catch(Exception e) { }
        try { this.setFechaNacimiento(rs.getDate("fechanacimiento")); }
        catch(Exception e) { }
        try { this.setIdSexo(rs.getInt("idsexo")); }
        catch(Exception e) { }
        try { this.setSexo(rs.getString("sexo")); }
        catch(Exception e) { }
        try { this.setIdEstado(rs.getInt("idestadosocio")); }
        catch(Exception e) { }
        try { this.setEstado(rs.getString("estadosocio")); }
        catch(Exception e) { }
        try { this.setIdEstadoCivil(rs.getInt("idestadocivil")); }
        catch(Exception e) { }
        try { this.setEstadoCivil(rs.getString("estadocivil")); }
        catch(Exception e) { }
        try { this.setIdLugarNacimiento(rs.getInt("idlugarnacimiento")); }
        catch(Exception e) { }
        try { this.setLugarNacimiento(rs.getString("lugarnacimiento")); }
        catch(Exception e) { }
        try { this.setIdNacionalidad(rs.getInt("idnacionalidad")); }
        catch(Exception e) { }
        try { this.setNacionalidad(rs.getString("nacionalidad")); }
        catch(Exception e) { }
        try { this.setIdProfesion(rs.getInt("idprofesion")); }
        catch(Exception e) { }
        try { this.setProfesion(rs.getString("profesion")); }
        catch(Exception e) { }
        try { this.setEmail(rs.getString("email")); }
        catch(Exception e) { }
        try { this.setIdTipoCasa(rs.getInt("idtipocasa")); }
        catch(Exception e) { }
        try { this.setTipoCasa(rs.getString("tipocasa")); }
        catch(Exception e) { }
        try { this.setIdViaCobro(rs.getInt("idviacobro")); }
        catch(Exception e) { }
        try { this.setViaCobro(rs.getString("viacobro")); }
        catch(Exception e) { }
        try { this.setDiaVencimiento(rs.getInt("diavencimiento")); }
        catch(Exception e) { }
        try { this.setFechaIngreso(rs.getDate("fechaingreso")); }
        catch(Exception e) { }
        try { this.setIdPromotor(rs.getInt("idpromotor")); }
        catch(Exception e) { }
        try { this.setNumeroSocio(rs.getInt("numerosocio")); }
        catch(Exception e) { }
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) { }
        try { this.setPin(rs.getString("pin")); }
        catch(Exception e) { }
        try { this.setDisponibilidad(rs.getDouble("disponibilidad")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setNumeroSocio(int inumeroSocio) {
        this.inumeroSocio = inumeroSocio;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion.trim().toUpperCase();
    }
    
    public void setNumeroCasa(int inumeroCasa) {
        this.inumeroCasa = inumeroCasa;
    }

    public void setIdBarrio(int iidBarrio) {
        this.iidBarrio = iidBarrio;
    }

    public void setBarrio(String sbarrio) {
        this.sbarrio = sbarrio.trim().toUpperCase();
    }
    
    public void setIdCiudadResidencia(int iidCiudadResidencia) {
        this.iidCiudadResidencia = iidCiudadResidencia;
    }

    public void setCiudadResidencia(String sciudadResidencia) {
        this.sciudadResidencia = sciudadResidencia.trim().toUpperCase();
    }
        
    public void setTelefonoCelularPrincipal(String stelefonoCelularPrincipal) {
        this.stelefonoCelularPrincipal = stelefonoCelularPrincipal.trim();
    }
    
    public void setTelefonoCelular(String stelefonoCelular) {
        this.stelefonoCelular = stelefonoCelular.trim();
    }
    
    public void setTelefonoLineaBaja(String stelefonoLineaBaja) {
        this.stelefonoLineaBaja = stelefonoLineaBaja.trim();
    }

    public void setFechaNacimiento(java.util.Date dfechaNacimiento) {
        this.dfechaNacimiento = dfechaNacimiento;
    }

    public void setIdSexo(int iidSexo) {
        this.iidSexo = iidSexo;
    }

    public void setSexo(String ssexo) {
        this.ssexo = ssexo;
    }

    public void setIdEstado(int iidEstado) {
        this.iidEstado = iidEstado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setIdEstadoCivil(int iidEstadoCivil) {
        this.iidEstadoCivil = iidEstadoCivil;
    }

    public void setEstadoCivil(String sestadoCivil) {
        this.sestadoCivil = sestadoCivil;
    }

    public void setIdLugarNacimiento(int iidLugarNacimiento) {
        this.iidLugarNacimiento = iidLugarNacimiento;
    }

    public void setLugarNacimiento(String slugarNacimiento) {
        this.slugarNacimiento = slugarNacimiento;
    }

    public void setIdNacionalidad(int iidNacionalidad) {
        this.iidNacionalidad = iidNacionalidad;
    }

    public void setNacionalidad(String snacionalidad) {
        this.snacionalidad = snacionalidad;
    }
    
    public void setIdProfesion(int iidProfesion) {
        this.iidProfesion = iidProfesion;
    }

    public void setProfesion(String sprofesion) {
        this.sprofesion = sprofesion;
    }
    
    public void setEmail(String semail) {
        this.semail = semail.trim().toLowerCase();
    }

    public void setIdTipoCasa(int iidTipoCasa) {
        this.iidTipoCasa = iidTipoCasa;
    }

    public void setTipoCasa(String stipoCasa) {
        this.stipoCasa = stipoCasa;
    }

    public void setIdViaCobro(int iidViaCobro) {
        this.iidViaCobro = iidViaCobro;
    }

    public void setViaCobro(String sviaCobro) {
        this.sviaCobro = sviaCobro;
    }

    public void setDiaVencimiento(int idiaVencimiento) {
        this.idiaVencimiento = idiaVencimiento;
    }

    public void setFechaIngreso(java.util.Date dfechaIngreso) {
        this.dfechaIngreso = dfechaIngreso;
    }

    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }

    public void setRuc(String sruc) {
        this.sruc = sruc;
    }

    public void setPin(String spin) {
        this.spin = spin;
    }

    public void setDisponibilidad(double udisponibilidad) {
        this.udisponibilidad = udisponibilidad;
    }

    public int getNumeroSocio() {
        return this.inumeroSocio;
    }
    
    public String getDireccion() {
        return this.sdireccion;
    }
    
    public int getNumeroCasa() {
        return this.inumeroCasa;
    }

    public int getIdBarrio() {
        return this.iidBarrio;
    }

    public String getBarrio() {
        return this.sbarrio;
    }
    
    public int getIdCiudadResidencia() {
        return this.iidCiudadResidencia;
    }

    public String getCiudadResidencia() {
        return this.sciudadResidencia;
    }
        
    public String getTelefonoCelularPrincipal() {
        return this.stelefonoCelularPrincipal.trim();
    }
    
    public String getTelefonoCelular() {
        return this.stelefonoCelular;
    }
    
    public String getTelefonoLineaBaja() {
        return this.stelefonoLineaBaja;
    }

    public java.util.Date getFechaNacimiento() {
        return this.dfechaNacimiento;
    }

    public int getIdSexo() {
        return this.iidSexo;
    }

    public String getSexo() {
        return this.ssexo;
    }

    public int getIdEstado() {
        return this.iidEstado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public int getIdEstadoCivil() {
        return this.iidEstadoCivil;
    }

    public String getEstadoCivil() {
        return this.sestadoCivil;
    }

    public int getIdLugarNacimiento() {
        return this.iidLugarNacimiento;
    }

    public String getLugarNacimiento() {
        return this.slugarNacimiento;
    }

    public int getIdNacionalidad() {
        return this.iidNacionalidad;
    }

    public String getNacionalidad() {
        return this.snacionalidad;
    }
    
    public int getIdProfesion() {
        return this.iidProfesion;
    }

    public String getProfesion() {
        return this.sprofesion;
    }
    
    public String getEmail() {
        return this.semail;
    }

    public int getIdTipoCasa() {
        return this.iidTipoCasa;
    }

    public String getTipoCasa() {
        return this.stipoCasa;
    }

    public int getIdViaCobro() {
        return this.iidViaCobro;
    }

    public String getViaCobro() {
        return this.sviaCobro;
    }

    public int getDiaVencimiento() {
        return this.idiaVencimiento;
    }

    public java.util.Date getFechaIngreso() {
        return this.dfechaIngreso;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public String getRuc() {
        return this.sruc;
    }

    public String getPin() {
        return this.spin;
    }

    public double getDisponibilidad() {
        return this.udisponibilidad;
    }

    public boolean esValido(boolean esDuplicado, int cantidadActivoLabor, int cantidadLabor) {
        this.smensaje = "";
        if (this.getCedula().isEmpty()) this.smensaje += TEXTO_CEDULA + "\n";
        //if (esDuplicado) this.smensaje += TEXTO_CEDULA + "\n";
        if (this.getNombre().isEmpty()) this.smensaje += TEXTO_NOMBRE + "\n";
        if (this.getApellido().isEmpty()) this.smensaje += TEXTO_APELLIDO + "\n";
        if (this.getTelefonoCelular().isEmpty()) this.smensaje += TEXTO_TELEFONO_CELULAR + "\n";
        if (this.getTelefonoLineaBaja().isEmpty()) this.smensaje += TEXTO_TELEFONO_LINEA_BAJA + "\n";
        if (!(this.getNumeroCasa()>=INICIO_NUMERO_CASA && this.getNumeroCasa()<=FIN_NUMERO_CASA)) this.smensaje += TEXTO_NUMERO_CASA + "\n";
        if (this.getIdBarrio()<0) this.smensaje += TEXTO_BARRIO + "\n";
        if (this.getIdCiudadResidencia()<0) this.smensaje += TEXTO_CIUDAD_RESIDENCIA + "\n";
        if (this.getIdSexo()<0) this.smensaje += TEXTO_SEXO + "\n";
        if (this.getIdTipoCasa()<0) this.smensaje += TEXTO_TIPO_CASA + "\n";
        if (this.getIdEstadoCivil()<0) this.smensaje += TEXTO_ESTADO_CIVIL + "\n";
        if (this.getIdEstado()<0) this.smensaje += TEXTO_ESTADO_SOCIO + "\n";
        if (this.getIdLugarNacimiento()<0) this.smensaje += TEXTO_LUGAR_NACIMIENTO + "\n";
        if (this.getIdNacionalidad()<0) this.smensaje += TEXTO_NACIONALIDAD + "\n";
        if (this.getIdProfesion()<0) this.smensaje += TEXTO_PROFESION + "\n";
        if (this.getIdViaCobro()<0) this.smensaje += TEXTO_VIA_COBRO + "\n";
        if (this.getDiaVencimiento()==0) this.smensaje += TEXTO_DIA_VENCIMIENTO + "\n";
        if (this.getPin().isEmpty()) this.smensaje += TEXTO_PIN + "\n";
        if (this.getDisponibilidad()<0) this.smensaje += TEXTO_DISPONIBILIDAD + "\n";
        if (this.getFechaIngreso()==null) this.smensaje += TEXTO_FECHA_INGRESO + "\n";
        if (this.getIdPromotor()<0) this.smensaje += TEXTO_PROMOTOR + "\n";
        if ((this.getRuc().trim()).isEmpty()) this.setRuc(this.getCedula());
        if (cantidadLabor<=0) this.smensaje += "Debe tener por lo menos un Lugar Laboral" + "\n";
        if (cantidadActivoLabor>1) this.smensaje += "Debe estar asociado a un único Lugar Laboral" + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public boolean esValidoLugarLaboral(int cantidadActivoLabor, int cantidadLabor) {
        this.smensaje = "";
        if (cantidadLabor<=0) this.smensaje += "Debe tener por lo menos un Lugar Laboral" + "\n";
        if (cantidadLabor>0 && cantidadActivoLabor!=1) this.smensaje += "Debe estar asociado a un único Lugar Laboral" + "\n";
        if (!this.smensaje.isEmpty()) this.smensaje = this.smensaje.substring(0, this.smensaje.length()-1);
        if (this.smensaje.isEmpty()) return true;
        else return false;
    }
    
    public String getSentencia() {
        return "SELECT socio("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            this.getNumeroCasa()+","+
            this.getIdBarrio()+","+
            this.getIdCiudadResidencia()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoCelular())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoLineaBaja())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaNacimiento())+","+
            this.getIdSexo()+","+
            this.getIdEstado()+","+
            this.getIdEstadoCivil()+","+
            this.getIdLugarNacimiento()+","+
            this.getIdNacionalidad()+","+
            this.getIdProfesion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getEmail().toLowerCase())+","+
            this.getIdTipoCasa()+","+
            this.getIdViaCobro()+","+
            this.getDiaVencimiento()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaIngreso())+","+
            this.getIdPromotor()+","+
            this.getNumeroSocio()+","+
            utilitario.utiCadena.getTextoGuardado(this.getRuc())+","+
            utilitario.utiCadena.getTextoGuardado(this.getPin())+","+
            this.getDisponibilidad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoCelularPrincipal())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Apellido", "apellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Fecha Ingreso", "fechaingreso", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(5, "Numero", "numerosocio", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(6, "Estado Socio", "estadosocio", generico.entLista.tipo_texto));
        return lst;
    }
    
}
