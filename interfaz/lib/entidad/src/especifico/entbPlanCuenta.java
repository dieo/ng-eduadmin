/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entbPlanCuenta {
    
    protected int iid;
    protected String snroPlanCuenta;
    protected String snumero;
    protected String sdescripcion;
    protected boolean bimputable;
    protected boolean bactivo;
    protected int inivel;
    protected int iperiodo;
    protected short cestado;
    protected String smensaje;
    
    public static final int LONGITUD_DESCRIPCION = 100;
        
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_NUMERO_PLANCUENTA = "Nº de Plan de cuenta, sin puntos (no vacía)";
    public static final String TEXTO_IMPUTABLE = "Sí o No";
    public static final String TEXTO_ACTIVO = "Sí o No";
    public static final String TEXTO_NIVEL = "Nivel de cuenta (no vacía)";
    public static final String TEXTO_PERIODO = "Año al que corresponde el plan de cuenta";
    
    public entbPlanCuenta() {
        this.iid = 0;
        this.snroPlanCuenta = "";
        this.snumero = "";
        this.sdescripcion = "";
        this.bimputable = false;
        this.bactivo = false;
        this.inivel = 0;
        this.iperiodo = 0;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entbPlanCuenta(int iid, String sdescripcion, String snroPlanCuenta, String snumero, boolean bimputable, boolean bactivo, int inivel, int iperiodo, short cestado) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snumero = snumero;
        this.bimputable = bimputable;
        this.bactivo = bactivo;
        this.inivel = inivel;
        this.iperiodo = iperiodo;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, String sdescripcion, String snroPlanCuenta, String snumero, boolean bimputable, boolean bactivo, int inivel, int iperiodo, short cestado) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snumero = snumero;
        this.bimputable = bimputable;
        this.bactivo = bactivo;
        this.inivel = inivel;
        this.iperiodo = iperiodo;
        this.cestado = cestado;
    }

    public entbPlanCuenta copiar(entbPlanCuenta destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getNroPlanCuenta(), this.getNumero(), this.getImputable(), this.getActivo(), this.getNivel(), this.getPeriodo(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbPlanCuenta cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) { }
        try { this.setNroPlanCuenta(rs.getString("nroplancuenta")); }
        catch(Exception e) { }
        try { this.setNumero(rs.getString("nroplancuenta2")); }
        catch(Exception e) { }
        try { this.setImputable(rs.getBoolean("imputable")); }
        catch(Exception e) { }
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) { }
        try { this.setNivel(rs.getInt("nivel")); }
        catch(Exception e) { }
        try { this.setPeriodo(rs.getInt("periodo")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setNroPlanCuenta(String snroPlanCuenta) {
        this.snroPlanCuenta = snroPlanCuenta;
    }
    
    public void setNumero(String snumero) {
        this.snumero = snumero;
    }
    
    public void setImputable(boolean bimputable) {
        this.bimputable = bimputable;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setNivel(int inivel) {
        this.inivel = inivel;
    }

    public void setPeriodo(int iperiodo) {
        this.iperiodo = iperiodo;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public String getNroPlanCuenta() {
        return this.snroPlanCuenta;
    }
    
    public String getNumero() {
        return this.snumero;
    }
    
    public boolean getImputable() {
        return this.bimputable;
    }
    
    public boolean getActivo() {
        return this.bactivo;
    }
    
    public int getNivel() {
        return this.inivel;
    }
    
    public int getPeriodo() {
        return this.iperiodo;
    }
    
    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    //public String getCampoConcatenado() {
    //    return this.getId()+" "+this.getIdOrdenPago()+" "+this.getDescripcion()+" "+this.getIdPlanCuenta()+" "+this.getNroPlanCuenta()+" "+this.getPlanCuenta()+" "+this.getMonto();
    //}
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getNumero().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_NUMERO_PLANCUENTA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.plancuenta("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNumero())+","+
            this.getNivel()+","+
            this.getImputable()+","+
            this.getActivo()+","+
            this.getPeriodo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Nº Plan de Cuenta", "nroplancuenta", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Nivel", "nivel", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "Imputable", "imputable2", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(5, "Activa", "activo2", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(6, "Periodo", "periodo", generico.entLista.tipo_numero));
        return lst;
    }
}
