/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbReceptorCheque {

    protected int iid;
    protected String scedulaRuc;
    protected String sdescripcion;
    protected int iidPlanCuenta;
    protected int iidCentroCosto;
    protected String scentroCosto;
    protected int iidPersoneria;
    protected String spersoneria;
    private short hestadoRegistro;
    private String smensaje;
    
    public static final int LONGITUD_CEDULA_RUC = 12;
    public static final int LONGITUD_DESCRIPCION = 100;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CEDULA_RUC = "Número de Cédula o RUC (no vacía, hasta " + LONGITUD_CEDULA_RUC + " caracteres)";
    public static final String TEXTO_DESCRIPCION = "Descripción de la persona física o jurídica (no vacío, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_CENTRO_COSTO = "Centro de Costo a que pertenece (no vacío)";
    public static final String TEXTO_PERSONERIA = "Personería (no vacía, Física/Jurídica)";
    
    public entbReceptorCheque() {
        this.iid = 0;
        this.scedulaRuc = "";
        this.sdescripcion = "";
        this.iidPersoneria = 0;
        this.spersoneria = "";
        this.iidPlanCuenta = 0;
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entbReceptorCheque(int iid, String scedulaRuc, String sdescripcion, int iidPlanCuenta, int iidCentroCosto, String scentroCosto, int iidPersoneria, String spersoneria, short hestadoRegistro) {        
        this.iid = iid;
        this.scedulaRuc = scedulaRuc;
        this.sdescripcion = sdescripcion;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidPersoneria = iidPersoneria;
        this.spersoneria = spersoneria;
        this.iidPlanCuenta = iidPlanCuenta;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, String scedulaRuc, String sdescripcion, int iidPlanCuenta, int iidCentroCosto, String scentroCosto, int iidPersoneria, String spersoneria, short hestadoRegistro) {
        this.iid = iid;
        this.scedulaRuc = scedulaRuc;
        this.sdescripcion = sdescripcion;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.iidPersoneria = iidPersoneria;
        this.spersoneria = spersoneria;
        this.iidPlanCuenta = iidPlanCuenta;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entbReceptorCheque copiar(entbReceptorCheque destino) {
        destino.setEntidad(this.getId(), this.getCedulaRuc(), this.getDescripcion(), this.getIdPlanCuenta(), this.getIdCentroCosto(), this.getCentroCosto(), this.getIdPersoneria(), this.getPersoneria(), this.getEstadoRegistro());
        return destino;
    }

    public entbReceptorCheque cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setCedulaRuc(rs.getString("cedularuc")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(Exception e) {}
        try { this.setIdCentroCosto(rs.getInt("idcentrocosto")); }
        catch(Exception e) {}
        try { this.setCentroCosto(rs.getString("centrocosto")); }
        catch(Exception e) {}
        try { this.setIdPersoneria(rs.getShort("idpersoneria")); }
        catch(Exception e) {}
        try { this.setPersoneria(rs.getString("personeria")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setCedulaRuc(String scedulaRuc) {
        this.scedulaRuc = scedulaRuc;
    }
    
    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }
    
    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }
    
    public void setIdPersoneria(int iidPersoneria) {
        this.iidPersoneria = iidPersoneria;
    }
    
    public void setPersoneria(String spersoneria) {
        this.spersoneria = spersoneria;
    }
    
    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }
    
    public String getCedulaRuc() {
        return this.scedulaRuc;
    }
    
    public String getDescripcion() {
        return this.sdescripcion;
    }

    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }
    
    public String getCentroCosto() {
        return this.scentroCosto;
    }
    
    public int getIdPersoneria() {
        return this.iidPersoneria;
    }
    
    public String getPersoneria() {
        return this.spersoneria;
    }
    
    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    } 
     
     public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getCedulaRuc().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CEDULA_RUC;
            bvalido = false;
        }
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdPersoneria()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_PERSONERIA;
            bvalido = false;
        }
        if (this.getIdCentroCosto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CENTRO_COSTO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT banco.receptorcheque("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedulaRuc())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdPlanCuenta()+","+
            this.getIdCentroCosto()+","+
            this.getIdPersoneria()+","+
            this.getEstadoRegistro()+")";
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número de Cédula/RUC", "cedularuc", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(2, "Razón Social", "descripcion", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(3, "Centro de Costo", "centrocosto", new generico.entLista().tipo_texto));
        lst.add(new generico.entLista(4, "Tipo de Personería", "personeria", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
