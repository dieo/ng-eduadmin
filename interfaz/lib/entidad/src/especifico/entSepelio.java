/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entSepelio {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String snombre;
    private String sapellido;
    private int inumeroSolicitud;
    private java.util.Date dfechaSolicitud;
    private int inumeroOperacion;
    private int iidCoberturaSepelio;
    private String scoberturaSepelio;
    private double dmontoCobertura;
    private String sobservacionSolicitud;
    private int iidEstadoSolicitud;
    private String sestadoSolicitud;
    private int iantiguedadMinima;
    private int iantiguedad;
    private int iidRegional;
    private String sregional;
    private String scedulaAfectado;
    private String snombreAfectado;
    private int iidMoneda;
    private String smoneda;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    private java.util.Date dfechaRechazado;
    private String sobservacionRechazado;

    private java.util.Date dfechaAprobado;
    private String sobservacionAprobado;
    private double dmontoAprobado;
    private int inumeroDictamen;
    private java.util.Date dfechaDictamen;
    
    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_DESTINO = 100;
    public static final int LONGITUD_OBSERVACION = 100;
    public static final int LONGITUD_CEDULA_AFECTADO = 12;
    public static final int LONGITUD_NOMBRE_AFECTADO = 40;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ESTADO = "Estado de Solicitud (no editable)";
    public static final String TEXTO_NUMERO_SOLICITUD = "Número de Solicitud (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_FECHA_SOLICITUD = "Fecha de Solicitud (no vacía)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (no editable)";
    public static final String TEXTO_COBERTURA = "Cobertura de Sepelio (no vacía)";
    public static final String TEXTO_MONTO_COBERTURA = "Monto de Cobertura (valor positivo)";
    public static final String TEXTO_OBSERVACION_SOLICITUD = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_ANTIGUEDAD_MINIMA = "Antigüedad mínima (no editable)";
    public static final String TEXTO_ANTIGUEDAD = "Antigüedad (no editable)";
    public static final String TEXTO_REGIONAL = "Regional (no vacía)";
    public static final String TEXTO_CEDULA_AFECTADO = "Cédula del afectado (hasta " + LONGITUD_CEDULA_AFECTADO + " caracteres)";
    public static final String TEXTO_NOMBRE_AFECTADO = "Nombre y Apellido del afectado (hasta " + LONGITUD_NOMBRE_AFECTADO + " caracteres)";
    public static final String TEXTO_MONEDA = "Moneda (no editable)";
    
    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_RECHAZADO = "Fecha de Rechazo (no editable)";
    public static final String TEXTO_OBSERVACION_RECHAZADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public static final String TEXTO_FECHA_APROBADO = "Fecha de Aprobación (no editable)";
    public static final String TEXTO_OBSERVACION_APROBADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_MONTO_APROBADO = "Monto Aprobado (valor positivo)";
    public static final String TEXTO_NUMERO_DICTAMEN = "Número de Dictamen";
    public static final String TEXTO_FECHA_DICTAMEN = "Fecha del Dictamen";

    public entSepelio() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.inumeroSolicitud = 0;
        this.dfechaSolicitud = null;
        this.inumeroOperacion = 0;
        this.iidCoberturaSepelio = 0;
        this.scoberturaSepelio = "";
        this.dmontoCobertura = 0.0;
        this.sobservacionSolicitud = "";
        this.iidEstadoSolicitud = 0;
        this.sestadoSolicitud = "";
        this.iantiguedadMinima = 0;
        this.iantiguedad = 0;
        this.iidRegional = 0;
        this.sregional = "";
        this.scedulaAfectado = "";
        this.snombreAfectado = "";
        this.iidMoneda = 0;
        this.smoneda = "";
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.dfechaRechazado = null;
        this.sobservacionRechazado = "";
        this.dfechaAprobado = null;
        this.sobservacionAprobado = "";
        this.dmontoAprobado = 0.0;
        this.inumeroDictamen = 0;
        this.dfechaDictamen = null;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entSepelio(int iid, int iidSocio, String scedula, String snombre, String sapellido, int inumeroSolicitud, java.util.Date dfechaSolicitud, int inumeroOperacion, int iidCoberturaSepelio, String scoberturaSepelio, double dmontoCobertura, String sobservacionSolicitud, int iidEstadoSolicitud, String sestadoSolicitud, int iantiguedadMinima, int iantiguedad, int iidRegional, String sregional, String scedulaAfectado, String snombreAfectado, int iidMoneda, String smoneda, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRechazado, String sobservacionRechazado, java.util.Date dfechaAprobado, String sobservacionAprobado, double dmontoAprobado, int inumeroDictamen, java.util.Date dfechaDictamen, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaSolicitud = dfechaSolicitud;
        this.inumeroOperacion = inumeroOperacion;
        this.iidCoberturaSepelio = iidCoberturaSepelio;
        this.scoberturaSepelio = scoberturaSepelio;
        this.dmontoCobertura = dmontoCobertura;
        this.sobservacionSolicitud = sobservacionSolicitud;
        this.iidEstadoSolicitud = iidEstadoSolicitud;
        this.sestadoSolicitud = sestadoSolicitud;
        this.iantiguedadMinima = iantiguedadMinima;
        this.iantiguedad = iantiguedad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.scedulaAfectado = scedulaAfectado;
        this.snombreAfectado = snombreAfectado;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRechazado = dfechaRechazado;
        this.sobservacionRechazado = sobservacionRechazado;
        this.dfechaAprobado = dfechaAprobado;
        this.sobservacionAprobado = sobservacionAprobado;
        this.dmontoAprobado = dmontoAprobado;
        this.inumeroDictamen = inumeroDictamen;
        this.dfechaDictamen = dfechaDictamen;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, String snombre, String sapellido, int inumeroSolicitud, java.util.Date dfechaSolicitud, int inumeroOperacion, int iidCoberturaSepelio, String scoberturaSepelio, double dmontoCobertura, String sobservacionSolicitud, int iidEstadoSolicitud, String sestadoSolicitud, int iantiguedadMinima, int iantiguedad, int iidRegional, String sregional, String scedulaAfectado, String snombreAfectado, int iidMoneda, String smoneda, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRechazado, String sobservacionRechazado, java.util.Date dfechaAprobado, String sobservacionAprobado, double dmontoAprobado, int inumeroDictamen, java.util.Date dfechaDictamen, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaSolicitud = dfechaSolicitud;
        this.inumeroOperacion = inumeroOperacion;
        this.iidCoberturaSepelio = iidCoberturaSepelio;
        this.scoberturaSepelio = scoberturaSepelio;
        this.dmontoCobertura = dmontoCobertura;
        this.sobservacionSolicitud = sobservacionSolicitud;
        this.iidEstadoSolicitud = iidEstadoSolicitud;
        this.sestadoSolicitud = sestadoSolicitud;
        this.iantiguedadMinima = iantiguedadMinima;
        this.iantiguedad = iantiguedad;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.scedulaAfectado = scedulaAfectado;
        this.snombreAfectado = snombreAfectado;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRechazado = dfechaRechazado;
        this.sobservacionRechazado = sobservacionRechazado;
        this.dfechaAprobado = dfechaAprobado;
        this.sobservacionAprobado = sobservacionAprobado;
        this.dmontoAprobado = dmontoAprobado;
        this.inumeroDictamen = inumeroDictamen;
        this.dfechaDictamen = dfechaDictamen;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entSepelio copiar(entSepelio destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getNombre(), this.getApellido(), this.getNumeroSolicitud(), this.getFechaSolicitud(), this.getNumeroOperacion(), this.getIdCoberturaSepelio(), this.getCoberturaSepelio(), this.getMontoCobertura(), this.getObservacionSolicitud(), this.getIdEstadoSolicitud(), this.getEstadoSolicitud(), this.getAntiguedadMinima(), this.getAntiguedad(), this.getIdRegional(), this.getRegional(), this.getCedulaAfectado(), this.getNombreAfectado(), this.getIdMoneda(), this.getMoneda(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getFechaRechazado(), this.getObservacionRechazado(), this.getFechaAprobado(), this.getObservacionAprobado(), this.getMontoAprobado(), this.getNumeroDictamen(), this.getFechaDictamen(), this.getEstadoRegistro());
        return destino;
    }
    
    public entSepelio cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setFechaSolicitud(rs.getDate("fechasolicitud")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setIdCoberturaSepelio(rs.getInt("idcoberturasepelio")); }
        catch(Exception e) {}
        try { this.setCoberturaSepelio(rs.getString("coberturasepelio")); }
        catch(Exception e) {}
        try { this.setMontoCobertura(rs.getDouble("montocobertura")); }
        catch(Exception e) {}
        try { this.setObservacionSolicitud(rs.getString("observacionsolicitud")); }
        catch(Exception e) {}
        try { this.setIdEstadoSolicitud(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstadoSolicitud(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setAntiguedadMinima(rs.getInt("antiguedadminima")); }
        catch(Exception e) {}
        try { this.setAntiguedad(rs.getInt("antiguedad")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setCedulaAfectado(rs.getString("cedulaafectado")); }
        catch(Exception e) {}
        try { this.setNombreAfectado(rs.getString("nombreafectado")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setFechaRechazado(rs.getDate("fecharechazado")); }
        catch(Exception e) {}
        try { this.setObservacionRechazado(rs.getString("observacionrechazado")); }
        catch(Exception e) {}
        try { this.setFechaAprobado(rs.getDate("fechaaprobado")); }
        catch(Exception e) {}
        try { this.setObservacionAprobado(rs.getString("observacionaprobado")); }
        catch(Exception e) {}
        try { this.setMontoAprobado(rs.getInt("montoaprobado")); }
        catch(Exception e) {}
        try { this.setNumeroDictamen(rs.getInt("numerodictamen")); }
        catch(Exception e) {}
        try { this.setFechaDictamen(rs.getDate("fechadictamen")); }
        catch(Exception e) {}
        return this;
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombre(String snombre) {
        this.snombre = snombre;
    }

    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setFechaSolicitud(java.util.Date dfechaSolicitud) {
        this.dfechaSolicitud = dfechaSolicitud;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setIdCoberturaSepelio(int iidCoberturaSepelio) {
        this.iidCoberturaSepelio = iidCoberturaSepelio;
    }

    public void setCoberturaSepelio(String scoberturaSepelio) {
        this.scoberturaSepelio = scoberturaSepelio;
    }

    public void setMontoCobertura(double dmontoCobertura) {
        this.dmontoCobertura = dmontoCobertura;
    }

    public void setObservacionSolicitud(String sobservacionSolicitud) {
        this.sobservacionSolicitud = sobservacionSolicitud;
    }

    public void setIdEstadoSolicitud(int iidEstadoSolicitud) {
        this.iidEstadoSolicitud = iidEstadoSolicitud;
    }

    public void setEstadoSolicitud(String sestadoSolicitud) {
        this.sestadoSolicitud = sestadoSolicitud;
    }

    public void setAntiguedadMinima(int iantiguedadMinima) {
        this.iantiguedadMinima = iantiguedadMinima;
    }

    public void setAntiguedad(int iantiguedad) {
        this.iantiguedad = iantiguedad;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }
    
    public void setRegional(String sregional) {
        this.sregional = sregional;
    }
    
    public void setCedulaAfectado(String scedulaAfectado) {
        this.scedulaAfectado = scedulaAfectado;
    }
    
    public void setNombreAfectado(String snombreAfectado) {
        this.snombreAfectado = snombreAfectado;
    }
    
    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }
    
    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setFechaRechazado(java.util.Date dfechaRechazado) {
        this.dfechaRechazado = dfechaRechazado;
    }

    public void setObservacionRechazado(String sobservacionRechazado) {
        this.sobservacionRechazado = sobservacionRechazado;
    }

    public void setFechaAprobado(java.util.Date dfechaAprobado) {
        this.dfechaAprobado = dfechaAprobado;
    }

    public void setObservacionAprobado(String sobservacionAprobado) {
        this.sobservacionAprobado = sobservacionAprobado;
    }

    public void setMontoAprobado(double dmontoAprobado) {
        this.dmontoAprobado = dmontoAprobado;
    }

    public void setNumeroDictamen(int inumeroDictamen) {
        this.inumeroDictamen = inumeroDictamen;
    }

    public void setFechaDictamen(java.util.Date dfechaDictamen) {
        this.dfechaDictamen = dfechaDictamen;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombre() {
        return this.snombre;
    }

    public String getApellido() {
        return this.sapellido;
    }

    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public java.util.Date getFechaSolicitud() {
        return this.dfechaSolicitud;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getIdCoberturaSepelio() {
        return this.iidCoberturaSepelio;
    }

    public String getCoberturaSepelio() {
        return this.scoberturaSepelio;
    }

    public double getMontoCobertura() {
        return this.dmontoCobertura;
    }

    public String getObservacionSolicitud() {
        return this.sobservacionSolicitud;
    }

    public int getIdEstadoSolicitud() {
        return this.iidEstadoSolicitud;
    }

    public String getEstadoSolicitud() {
        return this.sestadoSolicitud;
    }

    public int getAntiguedadMinima() {
        return this.iantiguedadMinima;
    }

    public int getAntiguedad() {
        return this.iantiguedad;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }
    
    public String getRegional() {
        return this.sregional;
    }
    
    public String getCedulaAfectado() {
        return this.scedulaAfectado;
    }
    
    public String getNombreAfectado() {
        return this.snombreAfectado;
    }
    
    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public java.util.Date getFechaRechazado() {
        return this.dfechaRechazado;
    }

    public String getObservacionRechazado() {
        return this.sobservacionRechazado;
    }

    public java.util.Date getFechaAprobado() {
        return this.dfechaAprobado;
    }

    public String getObservacionAprobado() {
        return this.sobservacionAprobado;
    }

    public double getMontoAprobado() {
        return this.dmontoAprobado;
    }

    public int getNumeroDictamen() {
        return this.inumeroDictamen;
    }

    public java.util.Date getFechaDictamen() {
        return this.dfechaDictamen;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoSolicitar(double umontoBeneficiario) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdSocio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getFechaSolicitud()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_SOLICITUD;
            bvalido = false;
        }
        if (this.getIdCoberturaSepelio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_COBERTURA;
            bvalido = false;
        }
        if (this.getAntiguedad()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ANTIGUEDAD;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        if (umontoBeneficiario>this.getMontoCobertura()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "El importe total de los Beneficiarios es mayor al Monto de Cobertura";
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAprobar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getMontoAprobado()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_APROBADO;
            bvalido = false;
        }
        if (this.getMontoAprobado()>this.getMontoCobertura()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Monto aprobado ("+this.getMontoAprobado()+") no puede exceder al Monto de la cobertura ("+this.getMontoCobertura()+")";
            bvalido = false;
        }
        if (this.getObservacionAprobado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_APROBADO;
            bvalido = false;
        }
        if (this.getNumeroDictamen()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_DICTAMEN;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoAnular() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoRechazar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionRechazado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_RECHAZADO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT sepelio ("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getNumeroSolicitud()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaSolicitud())+","+
            this.getNumeroOperacion()+","+
            this.getIdCoberturaSepelio()+","+
            this.getMontoCobertura()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionSolicitud())+","+
            this.getIdEstadoSolicitud()+","+
            this.getAntiguedadMinima()+","+
            this.getAntiguedad()+","+
            this.getIdRegional()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaRechazado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionRechazado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAprobado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAprobado())+","+
            this.getMontoAprobado()+","+
            this.getNumeroDictamen()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaDictamen())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedulaAfectado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombreAfectado())+","+
            this.getIdMoneda()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha Solicitud", "fechasolicitud", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(2, "Fecha Aprobación", "fechaaprobado", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Número Solicitud", "numerosolicitud", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Estado", "estado", generico.entLista.tipo_texto));
        return lst;
    }

}
