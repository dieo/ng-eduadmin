/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entbSucursalBanco {

    protected int iid;
    protected int iidBanco;
    protected String sdescripcion;
    protected String sdireccion;
    protected String stelefono;
    protected int iidCiudad;
    protected String sciudad;
    protected short cestado;
    protected String smensaje;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final int LONGITUD_DIRECCION = 50;
    public static final int LONGITUD_TELEFONO = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de sucursal de banco (no vacía)";
    public static final String TEXTO_CIUDAD = "Ciudad (no vacía)";
    public static final String TEXTO_DIRECCION = "Dirección (hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_TELEFONO = "Teléfono (hasta " + LONGITUD_TELEFONO + " caracteres)";
    
    public entbSucursalBanco() {
        this.iid = 0;
        this.iidBanco = 0;
        this.sdescripcion = "";
        this.sdireccion = "";
        this.stelefono = "";
        this.iidCiudad = 0;
        this.sciudad = "";
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entbSucursalBanco(int iid, int iidBanco, String sdescripcion, String sdireccion, String stelefono, int iidCiudad, String sciudad, short cestado) {
        this.iid = iid;
        this.iidBanco = iidBanco;
        this.sdescripcion = sdescripcion;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.cestado = cestado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iidBanco, String sdescripcion, String sdireccion, String stelefono, int iidCiudad, String sciudad, short cestado) {
        this.iid = iid;
        this.iidBanco = iidBanco;
        this.sdescripcion = sdescripcion;
        this.sdireccion = sdireccion;
        this.stelefono = stelefono;
        this.iidCiudad = iidCiudad;
        this.sciudad = sciudad;
        this.cestado = cestado;
    }
    
    public entbSucursalBanco copiar(entbSucursalBanco destino) {
        destino.setEntidad(this.getId(), this.getIdBanco(), this.getDescripcion(), this.getDireccion(), this.getTelefono(), this.getIdCiudad(), this.getCiudad(), this.getEstadoRegistro());
        return destino;
    }

    public entbSucursalBanco cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdBanco(rs.getInt("idbanco")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) {}
        try { this.setTelefono(rs.getString("telefono")); }
        catch(Exception e) {}
        try { this.setIdCiudad(rs.getInt("idciudad")); }
        catch(Exception e) {}
        try { this.setCiudad(rs.getString("ciudad")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdBanco(int iidBanco) {
        this.iidBanco = iidBanco;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion;
    }

    public void setTelefono(String stelefono) {
        this.stelefono = stelefono;
    }

    public void setIdCiudad(int iidCiudad) {
        this.iidCiudad = iidCiudad;
    }

    public void setCiudad(String sciudad) {
        this.sciudad = sciudad;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdBanco() {
        return this.iidBanco;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public String getDireccion() {
        return this.sdireccion;
    }

    public String getTelefono() {
        return this.stelefono;
    }

    public int getIdCiudad() {
        return this.iidCiudad;
    }

    public String getCiudad() {
        return this.sciudad;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdCiudad() <= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CIUDAD;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.bancosucursal("+
            this.getId()+","+
            this.getIdBanco()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdCiudad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefono())+","+
            this.getEstadoRegistro()+")";
    }

}
