/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;

/**
 *
 * @author Lic. Didier Barreto
 */
public class entCDADetalle {
    
    private int iid;
    private int iidcda;
    private int inumeroCuota;
    private java.util.Date dfechaInicio;
    private java.util.Date dfechaFin;
    private int icantidadDias;
    private double ucapital;
    private double ucapitalactual;
    private double uinteres;
    private double uretiroInteres;
    private double uretiroCapital;
    private java.util.Date dfechaRetiro;
    
    protected short hestado;
    protected String smensaje;
    
    //public static final int LONGITUD_DESCRIPCION = 100;
        
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMERO_CUOTA = "Numero cuota (no editable)";
    public static final String TEXTO_DIAS = "Plazo en días del CDA";
    public static final String TEXTO_CAPITAL = "Capital inicial del CDA";
    public static final String TEXTO_FECHA_INICIO = "Fecha de inicio (no vacía)";
    public static final String TEXTO_FECHA_FIN = "Fecha de inicio (no vacía)";
    public static final String TEXTO_FECHA_RETIRO = "Fecha de retiro";
    //public static final String TEXTO_DESCRIPCION = "Descripción (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_PLANCUENTA = "Plan de cuenta (no vacía)";
    public static final String TEXTO_RETIRO_INTERES = "Cobro normal de interés. No puede ser mayor a interés generado";
    public static final String TEXTO_INTERES = "Cálculo de interés correspondiente";
    public static final String TEXTO_RETIRO_CAPITAL = "Retiro de capital";
    
    public entCDADetalle() {
        this.iid = 0;
        this.iidcda = 0;
        this.inumeroCuota = 0;
        this.dfechaInicio = null;
        this.dfechaFin = null;
        this.icantidadDias = 0;
        this.ucapital = 0.0;
        this.ucapitalactual = 0.0;
        this.uinteres = 0.0;
        this.uretiroInteres = 0.0;
        this.uretiroCapital = 0.0;
        this.dfechaRetiro = null;
        this.hestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entCDADetalle(int iid, int iidcda, int inumeroCuota, java.util.Date dfechaInicio, java.util.Date dfechaFin, int icantidadDias, double ucapital, double ucapitalactual, double uinteres, double uretiroInteres, double uretiroCapital, java.util.Date dfechaRetiro, short hestado) {
        this.iid = iid;
        this.iidcda = iidcda;
        this.inumeroCuota = inumeroCuota;
        this.dfechaInicio = dfechaInicio;
        this.dfechaFin = dfechaFin;
        this.icantidadDias = icantidadDias;
        this.ucapital = ucapital;
        this.ucapitalactual = ucapitalactual;
        this.uinteres = uinteres;
        this.uretiroInteres = uretiroInteres;
        this.uretiroCapital = uretiroCapital;
        this.dfechaRetiro = dfechaRetiro;
        this.hestado = hestado;
    }
    
     public void setEntidad(int iid, int iidcda, int inumeroCuota, java.util.Date dfechaInicio, java.util.Date dfechaFin, int icantidadDias, double ucapital, double ucapitalactual, double uinteres, double uretiroInteres, double uretiroCapital, java.util.Date dfechaRetiro, short hestado) {
        this.iid = iid;
        this.iidcda = iidcda;
        this.inumeroCuota = inumeroCuota;
        this.dfechaInicio = dfechaInicio;
        this.dfechaFin = dfechaFin;
        this.icantidadDias = icantidadDias;
        this.ucapital = ucapital;
        this.ucapitalactual = ucapitalactual;
        this.uinteres = uinteres;
        this.uretiroInteres = uretiroInteres;
        this.uretiroCapital = uretiroCapital;
        this.dfechaRetiro = dfechaRetiro;
        this.hestado = hestado;
    }

    public entCDADetalle copiar(entCDADetalle destino) {
        destino.setEntidad(this.getId(), this.getIdCDA(), this.getNumeroCuota(), this.getFechaInicio(), this.getFechaFin(), this.getCantidadDias(), this.getCapital(), this.getCapitalActual(), this.getInteres(), this.getRetiroInteres(), this.getRetiroCapital(), this.getFechaRetiro(), this.getEstadoRegistro());
        return destino;
    }
    
    public entCDADetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(SQLException e) {}
        try { this.setIdCDA(rs.getInt("idcda")); }
        catch(SQLException e) {}
        try { this.setNumeroCuota(rs.getShort("numerocuota")); }
        catch(SQLException e) {}
        try { this.setFechaInicio(rs.getDate("fechainicio")); }
        catch(SQLException e) {}
        try { this.setFechaFin(rs.getDate("fechafin")); }
        catch(SQLException e) {}
        try { this.setCantidadDias(rs.getInt("dias")); }
        catch(SQLException e) {}
        try { this.setCapital(rs.getDouble("capital")); }
        catch(SQLException e) {}
        try { this.setCapitalActual(rs.getDouble("capitalactual")); }
        catch(SQLException e) {}
        try { this.setInteres(rs.getDouble("interes")); }
        catch(SQLException e) {}
        try { this.setRetiroInteres(rs.getDouble("retirointeres")); }
        catch(SQLException e) {}
        try { this.setRetiroCapital(rs.getDouble("retirocapital")); }
        catch(SQLException e) {}
        try { this.setFechaRetiro(rs.getDate("fecharetiro")); }
        catch(SQLException e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdCDA(int iidcda) {
        this.iidcda = iidcda;
    }

    public void setNumeroCuota(int inumeroCuota) {
        this.inumeroCuota = inumeroCuota;
    }
    
    public void setFechaInicio(java.util.Date dfechaInicio) {
        this.dfechaInicio = dfechaInicio;
    }
    
    public void setFechaFin(java.util.Date dfechaFin) {
        this.dfechaFin = dfechaFin;
    }
    
    public void setCantidadDias(int icantidadDias) {
        this.icantidadDias = icantidadDias;
    }
    
    public void setCapital(double ucapital) {
        this.ucapital = ucapital;
    }
    
    public void setCapitalActual(double ucapitalactual) {
        this.ucapitalactual = ucapitalactual;
    }
    
    public void setInteres(double uinteres) {
        this.uinteres = uinteres;
    }
    
    public void setRetiroInteres(double uretiroInteres) {
        this.uretiroInteres = uretiroInteres;
    }
    
    public void setRetiroCapital(double uretiroCapital) {
        this.uretiroCapital = uretiroCapital;
    }
    
    public void setFechaRetiro(java.util.Date dfechaRetiro) {
        this.dfechaRetiro = dfechaRetiro;
    }
    
    public void setEstadoRegistro(short hestado) {
        this.hestado = hestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdCDA() {
        return this.iidcda;
    }
    
    public int getNumeroCuota() {
        return this.inumeroCuota;
    }
    
    public java.util.Date getFechaInicio() {
        return this.dfechaInicio;
    }

    public java.util.Date getFechaFin() {
        return this.dfechaFin;
    }

    public int getCantidadDias() {
        return this.icantidadDias;
    }
    
    public double getCapital() {
        return this.ucapital;
    }
    
    public double getCapitalActual() {
        return this.ucapitalactual;
    }
    
    public double getInteres() {
        return this.uinteres;
    }
    
    public double getRetiroInteres() {
        return this.uretiroInteres;
    }
    
    public double getRetiroCapital() {
        return this.uretiroCapital;
    }
    
    public java.util.Date getFechaRetiro() {
        return this.dfechaRetiro;
    }

    public short getEstadoRegistro() {
        return this.hestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getRetiroInteres()>this.getInteres()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += entCDADetalle.TEXTO_RETIRO_INTERES;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT cdadetalle("+
            this.getId()+","+
            this.getIdCDA()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaInicio())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaFin())+","+
            this.getCantidadDias()+","+
            utilitario.utiNumero.redondear(this.getInteres(), 0)+","+
            utilitario.utiNumero.redondear(this.getRetiroCapital(), 0)+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaRetiro())+","+
            utilitario.utiNumero.redondear(this.getCapital(), 0)+","+
            this.getNumeroCuota()+","+
            utilitario.utiNumero.redondear(this.getRetiroInteres(), 0)+","+
            utilitario.utiNumero.redondear(this.getCapitalActual(), 0)+","+
            this.getEstadoRegistro()+")";
    }

}
