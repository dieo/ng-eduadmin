/*0
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleCancelacionCabecera {
    
    private int iidMovimiento;
    private int iidIngreso;
    private int iidMovimientoCancela;
    private String stabla;
    private int iidDetalleOperacion;
    private double umontoCapital;
    private double umontoInteres;
    private double umontoExoneracion;
    private String senviado;
    private String sestado; // A=atraso - S=saldo - E=enviado
    private double umontoCobro;
    private String sprioridad;

    private int inumeroOperacion;
    private int iplazo;
    private java.util.Date dfechaOperacion;
    private double utasaInteres;
    private boolean batraso;
    private boolean bcancela;
    private int iidCuenta;
    private String scuenta;
    private String sdescripcion;
    private double umontoAtraso;
    private double umontoSaldo;
    private String sreferencia;
    private int idesde;
    private int ihasta;
    
    public java.util.ArrayList<especifico.entDetalleIngresoCancelacion> detalle = new java.util.ArrayList<especifico.entDetalleIngresoCancelacion>();
    
    private short hestadoRegistro;
    private String smensaje;

    public static final String estado_atraso = "A";
    public static final String estado_enviado = "E";
    public static final String estado_saldo = "S";
    
    public entDetalleCancelacionCabecera() {
        this.iidMovimiento = 0;
        this.iidIngreso = 0;
        this.iidMovimientoCancela = 0;
        this.stabla = "";
        this.iidDetalleOperacion = 0;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.umontoExoneracion = 0.0;
        this.senviado = "";
        this.sestado = "";
        this.umontoCobro = 0.0;
        this.sprioridad = "";

        this.inumeroOperacion = 0;
        this.iplazo = 0;
        this.dfechaOperacion = null;
        this.utasaInteres = 0.0;
        this.batraso = false;
        this.bcancela = false;
        this.iidCuenta = 0;
        this.scuenta = "";
        this.sdescripcion = "";
        this.umontoAtraso = 0.0;
        this.umontoSaldo = 0.0;
        this.sreferencia = "";
        this.idesde = 0;
        this.ihasta = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetalleCancelacionCabecera(int iidMovimiento, int iidIngreso, int iidMovimientoCancela, String stabla, int iidDetalleOperacion, double umontoCapital, double umontoInteres, double umontoExoneracion, String senviado, String sestado, double umontoCobro, String sprioridad, int inumeroOperacion, int iplazo, java.util.Date dfechaOperacion, double utasaInteres, boolean batraso, boolean bcancela, int iidCuenta, String scuenta, String sdescripcion, double umontoAtraso, double umontoSaldo, String sreferencia, int idesde, int ihasta, short hestadoRegistro) {
        this.iidMovimiento = iidMovimiento;
        this.iidIngreso = iidIngreso;
        this.iidMovimientoCancela = iidMovimientoCancela;
        this.stabla = stabla;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoExoneracion = umontoExoneracion;
        this.senviado = senviado;
        this.sestado = sestado;
        this.umontoCobro = umontoCobro;
        this.sprioridad = sprioridad;

        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteres = utasaInteres;
        this.batraso = batraso;
        this.bcancela = bcancela;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdescripcion = sdescripcion;
        this.umontoAtraso = umontoAtraso;
        this.umontoSaldo = umontoSaldo;
        this.sreferencia = sreferencia;
        this.idesde = idesde;
        this.ihasta = ihasta;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iidMovimiento, int iidIngreso, int iidMovimientoCancela, String stabla, int iidDetalleOperacion, double umontoCapital, double umontoInteres, double umontoExoneracion, String senviado, String sestado, double umontoCobro,String sprioridad, int inumeroOperacion, int iplazo, java.util.Date dfechaOperacion, double utasaInteres, boolean batraso, boolean bcancela, int iidCuenta, String scuenta, String sdescripcion, double umontoAtraso, double umontoSaldo, String sreferencia, int idesde, int ihasta, short hestadoRegistro) {
        this.iidMovimiento = iidMovimiento;
        this.iidIngreso = iidIngreso;
        this.iidMovimientoCancela = iidMovimientoCancela;
        this.stabla = stabla;
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoExoneracion = umontoExoneracion;
        this.senviado = senviado;
        this.sestado = sestado;
        this.umontoCobro = umontoCobro;
        this.sprioridad = sprioridad;

        this.inumeroOperacion = inumeroOperacion;
        this.iplazo = iplazo;
        this.dfechaOperacion = dfechaOperacion;
        this.utasaInteres = utasaInteres;
        this.batraso = batraso;
        this.bcancela = bcancela;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdescripcion = sdescripcion;
        this.umontoAtraso = umontoAtraso;
        this.umontoSaldo = umontoSaldo;
        this.sreferencia = sreferencia;
        this.idesde = idesde;
        this.ihasta = ihasta;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleCancelacionCabecera copiar(entDetalleCancelacionCabecera destino) {
        destino.setEntidad(this.getIdMovimiento(), this.getIdIngreso(), this.getIdMovimientoCancela(), this.getTabla(), this.getIdDetalleOperacion(), this.getMontoCapital(), this.getMontoInteres(), this.getMontoExoneracion(), this.getEnviado(), this.getEstado(), this.getMontoCobro(), this.getPrioridad(), this.getNumeroOperacion(), this.getPlazo(), this.getFechaOperacion(), this.getTasaInteres(), this.getAtraso(), this.getCancela(), this.getIdCuenta(), this.getCuenta(), this.getDescripcion(), this.getMontoAtraso(), this.getMontoSaldo(), this.getReferencia(), this.getDesde(), this.getHasta(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleCancelacionCabecera cargar(java.sql.ResultSet rs) {
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) { }
        try { this.setIdIngreso(rs.getInt("idingreso")); }
        catch(Exception e) { }
        try { this.setIdMovimientoCancela(rs.getInt("idmovimientocancela")); }
        catch(Exception e) { }
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) { }
        try { this.setIdDetalleOperacion(rs.getInt("iddetalleoperacion")); }
        catch(Exception e) { }
        try { this.setMontoCapital(rs.getDouble("montocapital")); }
        catch(Exception e) { }
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) { }
        try { this.setMontoExoneracion(rs.getDouble("montoexoneracion")); }
        catch(Exception e) { }
        try { this.setEnviado(rs.getString("enviado")); }
        catch(Exception e) { }
        try { this.setEstado(rs.getString("estado")); }
        catch(Exception e) { }
        try { this.setMontoCobro(rs.getDouble("montocobro")); }
        catch(Exception e) { }
        try { this.setPrioridad(rs.getString("prioridad")); }
        catch(Exception e) { }

        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) { }
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) { }
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) { }
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(Exception e) { }
        try { this.setAtraso(rs.getBoolean("atraso")); }
        catch(Exception e) { }
        try { this.setCancela(rs.getBoolean("cancela")); }
        catch(Exception e) { }
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) { }
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) { }
        try { this.setReferencia(rs.getString("referencia")); }
        catch(Exception e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) { }
        try { this.setMontoAtraso(rs.getDouble("montoatraso")); }
        catch(Exception e) { }
        try { this.setMontoSaldo(rs.getDouble("montosaldo")); }
        catch(Exception e) { }
        return this;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setIdIngreso(int iidIngreso) {
        this.iidIngreso = iidIngreso;
    }

    public void setIdMovimientoCancela(int iidMovimientoCancela) {
        this.iidMovimientoCancela = iidMovimientoCancela;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setIdDetalleOperacion(int iidDetalleOperacion) {
        this.iidDetalleOperacion = iidDetalleOperacion;
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setMontoExoneracion(double umontoExoneracion) {
        this.umontoExoneracion = umontoExoneracion;
    }

    public void setEnviado(String senviado) {
        this.senviado = senviado;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setMontoCobro(double umontoCobro) {
        this.umontoCobro = umontoCobro;
    }

    public void setPrioridad(String sprioridad) {
        this.sprioridad = sprioridad;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = dfechaOperacion;
    }

    public void setTasaInteres(double utasaInteres) {
        this.utasaInteres = utasaInteres;
    }

    public void setAtraso(boolean batraso) {
        this.batraso = batraso;
    }

    public void setCancela(boolean bcancela) {
        this.bcancela = bcancela;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }

    public void setMontoAtraso(double umontoAtraso) {
        this.umontoAtraso = umontoAtraso;
    }

    public void setMontoSaldo(double umontoSaldo) {
        this.umontoSaldo = umontoSaldo;
    }

    public void setReferencia(String sreferencia) {
        this.sreferencia = sreferencia;
    }

    public void setDesde(int idesde) {
        this.idesde = idesde;
    }

    public void setHasta(int ihasta) {
        this.ihasta = ihasta;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public int getIdIngreso() {
        return this.iidIngreso;
    }

    public int getIdMovimientoCancela() {
        return this.iidMovimientoCancela;
    }

    public String getTabla() {
        return this.stabla;
    }

    public int getIdDetalleOperacion() {
        return this.iidDetalleOperacion;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public double getMontoExoneracion() {
        return this.umontoExoneracion;
    }

    public String getEnviado() {
        return this.senviado;
    }

    public String getEstado() {
        return this.sestado;
    }

    public double getMontoCobro() {
        return this.umontoCobro;
    }

    public String getPrioridad() {
        return this.sprioridad;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public double getTasaInteres() {
        return this.utasaInteres;
    }

    public boolean getAtraso() {
        return this.batraso;
    }

    public boolean getCancela() {
        return this.bcancela;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public double getMontoAtraso() {
        return this.umontoAtraso;
    }

    public double getMontoSaldo() {
        return this.umontoSaldo;
    }

    public String getReferencia() {
        return this.sreferencia;
    }

    public int getDesde() {
        return this.idesde;
    }

    public int getHasta() {
        return this.ihasta;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

}
