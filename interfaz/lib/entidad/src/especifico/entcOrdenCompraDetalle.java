/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entcOrdenCompraDetalle {
    
    protected int iid;
    protected int iidOrdenCompra;
    protected int iidCentroCosto;
    protected String scentroCosto;
    protected int iidArticulo;
    protected String sdescripcion;
    protected int iidFilial;
    protected String sfilial;
    protected double ucantidad;
    protected double ucosto;
    protected double utotal;
    protected short cestado;
    protected String smensaje;
    
    public static final int LONGITUD_DESCRIPCION = 200;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción del producto (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_FILIAL = "Filial para la que se realiza la Compra (no vacía)";
    public static final String TEXTO_CENTRO_COSTO = "Centro de Costo";
    public static final String TEXTO_TOTAL = "Monto Total (no editable)";
    public static final String TEXTO_ARTICULO = "Producto solicitado (no vacía)";
    public static final String TEXTO_CANTIDAD = "Cantidad olicitada del Producto (no vacía, valor positivo)";
    public static final String TEXTO_COSTO = "Costo unitario del Producto solicitado (no vacío, valor positivo)";
    
    public entcOrdenCompraDetalle() {
        this.iid = 0;
        this.iidOrdenCompra = 0;
        this.iidCentroCosto = 0;
        this.scentroCosto = "";
        this.utotal = 0.0;
        this.iidArticulo = 0;
        this.sdescripcion = "";
        this.iidFilial = 0;
        this.sfilial = "";
        this.ucantidad = 0.0;
        this.ucosto = 0.0;
        this.cestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entcOrdenCompraDetalle(int iid, int iidOrdenCompra, int iidCentroCosto, String scentroCosto, double utotal, int iidArticulo, String sdescripcion, int iidFilial, String sfilial, double ucantidad, double ucosto, short cestado) {
        this.iid = iid;
        this.iidOrdenCompra = iidOrdenCompra;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.utotal = utotal;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.ucantidad = ucantidad;
        this.ucosto = ucosto;
        this.cestado = cestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidOrdenCompra, int iidCentroCosto, String scentroCosto, double utotal, int iidArticulo, String sdescripcion, int iidFilial, String sfilial, double ucantidad, double ucosto, short cestado) {
        this.iid = iid;
        this.iidOrdenCompra = iidOrdenCompra;
        this.iidCentroCosto = iidCentroCosto;
        this.scentroCosto = scentroCosto;
        this.utotal = utotal;
        this.iidArticulo = iidArticulo;
        this.sdescripcion = sdescripcion;
        this.iidFilial = iidFilial;
        this.sfilial = sfilial;
        this.ucantidad = ucantidad;
        this.ucosto = ucosto;
        this.cestado = cestado;
    }

    public entcOrdenCompraDetalle copiar(entcOrdenCompraDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdOrdenCompra(), this.getIdCentroCosto(), this.getCentroCosto(), this.getTotal(), this.getIdArticulo(), this.getDescripcion(), this.getIdFilial(), this.getFilial(), this.getCantidad(), this.getCosto(),  this.getEstadoRegistro());
        return destino;
    }
    
    public entcOrdenCompraDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(SQLException e) { }
        try { this.setIdOrdenCompra(rs.getInt("idordencompra")); }
        catch(SQLException e) { }
        try { this.setIdCentroCosto(rs.getInt("idcentrocosto")); }
        catch(SQLException e) { }
        try { this.setCentroCosto(rs.getString("centrocosto")); }
        catch(SQLException e) { }
        try { this.setIdArticulo(rs.getInt("idarticulo")); }
        catch(SQLException e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(SQLException e) { }
        try { this.setIdFilial(rs.getInt("idfilial")); }
        catch(Exception e) {}
        try { this.setFilial(rs.getString("filial")); }
        catch(Exception e) {}
        try { this.setCantidad(rs.getDouble("cantidad")); }
        catch(SQLException e) { }
        try { this.setCosto(rs.getDouble("costo")); }
        catch(SQLException e) { }
        try { this.setTotal(rs.getDouble("total")); }
        catch(SQLException e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdOrdenCompra(int iidOrdenCompra) {
        this.iidOrdenCompra = iidOrdenCompra;
    }

    public void setIdArticulo(int iidArticulo) {
        this.iidArticulo = iidArticulo;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setIdFilial(int iidFilial) {
        this.iidFilial = iidFilial;
    }
    
    public void setFilial(String sfilial) {
        this.sfilial = sfilial;
    }

    public void setIdCentroCosto(int iidCentroCosto) {
        this.iidCentroCosto = iidCentroCosto;
    }

    public void setCentroCosto(String scentroCosto) {
        this.scentroCosto = scentroCosto;
    }
    
    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setCosto(double ucosto) {
        this.ucosto = ucosto;
    }

    public void setTotal(double utotal) {
        this.utotal = utotal;
    }

    public void setEstadoRegistro(short cestado) {
        this.cestado = cestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdOrdenCompra() {
        return this.iidOrdenCompra;
    }
        
    public int getIdArticulo() {
        return this.iidArticulo;
    }
   
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public int getIdFilial() {
        return this.iidFilial;
    }
    
    public String getFilial() {
        return this.sfilial;
    }

    public int getIdCentroCosto() {
        return this.iidCentroCosto;
    }
   
    public String getCentroCosto() {
        return this.scentroCosto;
    }
    
    public double getCantidad() {
        return this.ucantidad;
    }

    public double getCosto() {
        return this.ucosto;
    }

    public double getTotal() {
        return this.utotal;
    }

    public short getEstadoRegistro() {
        return this.cestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdArticulo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_ARTICULO;
            bvalido = false;
        }
        if (this.getIdFilial()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FILIAL;
            bvalido = false;
        }
        if ("".equals(this.getDescripcion())) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if ((this.getCantidad()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_CANTIDAD;
            bvalido = false;
        }
        if ((this.getCosto()<0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_COSTO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT compras.ordencompradetalle("+
            this.getId()+","+
            this.getIdOrdenCompra()+","+
            this.getIdArticulo()+","+
            this.getIdFilial()+","+
            this.getCantidad()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getCosto()+","+
            this.getTotal()+","+
            this.getEstadoRegistro()+")";
    }

}
