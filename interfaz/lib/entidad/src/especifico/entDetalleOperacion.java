/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleOperacion {

    private int iid;
    private int icuota;
    private java.util.Date dfechaVencimiento;
    private double umontoCapital;
    private double umontoInteres;
    private double upagoCapital;
    private double upagoInteres;
    private double usaldoCapital;
    private double usaldoInteres;
    private String sgiraduria;
    private double uinteres; // monto interes del ahorro programado
    private int icantidadDia;
    private double upago;
    private double usaldo;
    
    private int iidMovimiento;
    private String stabla;
    private int iplazo;
    private double utasaInteres;
    private java.util.Date dfechaPrimerVencimiento;
    private double ucapital;
    
    private String smensaje;
    private int iestadoRegistro;
    
    public static final String tabla_ahorroprogramado = "ap";
    public static final String tabla_operacionfija = "of";
    public static final String tabla_comercioexterno = "ce";
    public static final String tabla_movimiento = "mo";
    public static final String tabla_fondojuridicosepelio = "js";
    public static final String tabla_ahorrocda = "cd";
    public static final String tabla_ingreso = "in";
    public static final String tabla_movimientogarante = "mg";
    public static final String tabla_ordenpago = "op";
    public static final String tabla_deposito = "de";
    public static final String tabla_facturaegreso = "fe";
    
    public entDetalleOperacion() {
        this.iid = 0;
        this.icuota = 0;
        this.dfechaVencimiento = null;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.upagoCapital = 0.0;
        this.upagoInteres = 0.0;
        this.usaldoCapital = 0.0;
        this.usaldoInteres = 0.0;
        this.sgiraduria = "";
        this.uinteres = 0.0;
        this.icantidadDia = 0;
        this.upago = 0.0;
        this.usaldo = 0.0;

        this.iidMovimiento = 0;
        this.stabla = "";
        this.iplazo = 0;
        this.utasaInteres = 0.0;
        this.dfechaPrimerVencimiento = null;
        this.ucapital = 0.0;
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetalleOperacion(int iid, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double upagoCapital, double upagoInteres, double usaldoCapital, double usaldoInteres, String sgiraduria, double uinteres, int icantidadDia, double upago, double usaldo, int iidMovimiento, String stabla, int iplazo, double utasaInteres, java.util.Date dfechaPrimerVencimiento, double ucapital, int iestadoRegistro) {
        this.iid = iid;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.upagoCapital = upagoCapital;
        this.upagoInteres = upagoInteres;
        this.usaldoCapital = usaldoCapital;
        this.usaldoInteres = usaldoInteres;
        this.sgiraduria = sgiraduria;
        this.uinteres = uinteres;
        this.icantidadDia = icantidadDia;
        this.upago = upago;
        this.usaldo = usaldo;

        this.iidMovimiento = iidMovimiento;
        this.stabla = stabla;
        this.iplazo = iplazo;
        this.utasaInteres = utasaInteres;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.ucapital = ucapital;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iid, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double upagoCapital, double upagoInteres, double usaldoCapital, double usaldoInteres, String sgiraduria, double uinteres, int icantidadDia, double upago, double usaldo, int iidMovimiento, String stabla, int iplazo, double utasaInteres, java.util.Date dfechaPrimerVencimiento, double ucapital, int iestadoRegistro) {
        this.iid = iid;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.upagoCapital = upagoCapital;
        this.upagoInteres = upagoInteres;
        this.usaldoCapital = usaldoCapital;
        this.usaldoInteres = usaldoInteres;
        this.sgiraduria = sgiraduria;
        this.uinteres = uinteres;
        this.icantidadDia = icantidadDia;
        this.upago = upago;
        this.usaldo = usaldo;

        this.iidMovimiento = iidMovimiento;
        this.stabla = stabla;
        this.iplazo = iplazo;
        this.utasaInteres = utasaInteres;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.ucapital = ucapital;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entDetalleOperacion copiar(entDetalleOperacion destino) {
        destino.setEntidad(this.getId(), this.getCuota(), this.getFechaVencimiento(), this.getMontoCapital(), this.getMontoInteres(), this.getPagoCapital(), this.getPagoInteres(), this.getSaldoCapital(), this.getSaldoInteres(), this.getGiraduria(), this.getInteres(), this.getCantidadDia(), this.getPago(), this.getSaldo(), this.getIdMovimiento(), this.getTabla(), this.getPlazo(), this.getTasaInteres(), this.getFechaPrimerVencimiento(), this.getCapital(), this.getEstadoRegistro());
        return destino;
    }

    public entDetalleOperacion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setCuota(rs.getInt("numerocuota")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setMontoCapital(rs.getDouble("montocapital")); }
        catch(Exception e) {}
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) {}
        try { this.setSaldoCapital(rs.getDouble("saldocapital")); }
        catch(Exception e) {}
        try { this.setSaldoInteres(rs.getDouble("saldointeres")); }
        catch(Exception e) {}
        try { this.setPagoCapital(this.getMontoCapital()-this.getSaldoCapital()); }
        catch(Exception e) {}
        try { this.setPagoInteres(this.getMontoInteres()-this.getSaldoInteres()); }
        catch(Exception e) {}
        try { this.setGiraduria(rs.getString("giraduria")); }
        catch(Exception e) {}
        try { this.setInteres(rs.getDouble("interes")); }
        catch(Exception e) {}
        try { this.setCantidadDia(rs.getInt("cantidaddia")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = (java.util.Date)dfechaVencimiento.clone();
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setPagoCapital(double upagoCapital) {
        this.upagoCapital = upagoCapital;
    }

    public void setPagoInteres(double upagoInteres) {
        this.upagoInteres = upagoInteres;
    }

    public void setSaldoCapital(double usaldoCapital) {
        this.usaldoCapital = usaldoCapital;
    }

    public void setSaldoInteres(double usaldoInteres) {
        this.usaldoInteres = usaldoInteres;
    }

    public void setGiraduria(String sgiraduria) {
        this.sgiraduria = sgiraduria;
    }

    public void setInteres(double uinteres) { // creado para ahorro programado, los intereses a pagar
        this.uinteres = uinteres;
    }

    public void setCantidadDia(int icantidadDia) { // creado para ahorro programado, los dias devengados
        this.icantidadDia = icantidadDia;
    }
    
    public void setPago(double upago) { // creado para ahorro programado, los pagos
        this.upago = upago;
    }

    public void setSaldo(double usaldo) { // creado para ahorro programado, los saldos
        this.usaldo = usaldo;
    }

    // ========================================
    
    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setTasaInteres(double utasaInteres) {
        this.utasaInteres = utasaInteres;
    }

    public void setFechaPrimerVencimiento(java.util.Date dfechaPrimerVencimiento) {
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
    }

    public void setCapital(double ucapital) {
        this.ucapital = ucapital;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getCuota() {
        return this.icuota;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public double getPagoCapital() {
        return this.upagoCapital;
    }

    public double getPagoInteres() {
        return this.upagoInteres;
    }

    public double getSaldoCapital() {
        return this.usaldoCapital;
    }

    public double getSaldoInteres() {
        return this.usaldoInteres;
    }

    public String getGiraduria() {
        return this.sgiraduria;
    }

    public double getInteres() {
        return this.uinteres;
    }

    public int getCantidadDia() {
        return this.icantidadDia;
    }
    
    public double getPago() {
        return this.upago;
    }

    public double getSaldo() {
        return this.usaldo;
    }

    // ========================================

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public String getTabla() {
        return this.stabla;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public double getTasaInteres() {
        return this.utasaInteres;
    }

    public java.util.Date getFechaPrimerVencimiento() {
        return this.dfechaPrimerVencimiento;
    }

    public double getCapital() {
        return this.ucapital;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    // métodos para la conexión
    public String getSentencia() {
        return "SELECT detalleoperacion("+
            this.getId()+","+
            this.getCuota()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaVencimiento())+","+
            this.getMontoCapital()+","+
            this.getMontoInteres()+","+
            this.getSaldoCapital()+","+
            this.getSaldoInteres()+","+
            utilitario.utiCadena.getTextoGuardado(this.getGiraduria())+","+
            this.getInteres()+","+
            this.getCantidadDia()+","+
            this.getIdMovimiento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getEstadoRegistro()+")";
    }

    public String getSentenciaGeneracionDetalleLote() {
        return "SELECT detalleoperacion("+
            this.getIdMovimiento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getPlazo()+","+
            this.getTasaInteres()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaPrimerVencimiento())+","+
            this.getCapital()+")";
    }

    public String getSentenciaGeneracionDetalleLotePorInteres() {
        return "SELECT detalleoperacion("+
            this.getIdMovimiento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getPlazo()+","+
            this.getTasaInteres()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaPrimerVencimiento())+","+
            this.getCapital()+","+
            this.getInteres()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampoFiltro() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }
    
    public java.util.ArrayList<generico.entLista> getCampoOrden() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
