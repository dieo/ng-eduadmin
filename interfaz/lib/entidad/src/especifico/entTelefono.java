/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entTelefono extends generico.entGenerica {

    protected int iidDependencia;
    protected String sdependencia;
    protected int iidUsuario;
    protected String susuario;
    
    public static final int LONGITUD_DESCRIPCION = 20;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Número de Teléfono (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_DEPENDENCIA = "Dependencia a que pertenece el Teléfono (no vacío)";
    public static final String TEXTO_USUARIO = "Usuario a que pertenece el Teléfono (no vacío)";

    public entTelefono() {
        super();
        this.iidDependencia = 0;
        this.sdependencia = "";
        this.iidUsuario = 0;
        this.susuario = "";
    }

    public entTelefono(int iid, String sdescripcion, int iidDependencia, String sdependencia, int iidUsuario, String susuario) {
        super(iid, sdescripcion);
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
    }

    public entTelefono(int iid, String sdescripcion, int iidDependencia, String sdependencia, int iidUsuario, String susuario, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
    }

    public void setEntidad(int iid, String sdescripcion, int iidDependencia, String sdependencia, int iidUsuario, String susuario, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.iidDependencia = iidDependencia;
        this.sdependencia = sdependencia;
        this.iidUsuario = iidUsuario;
        this.susuario = susuario;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entTelefono copiar(entTelefono destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getIdDependencia(), this.getDependencia(), this.getIdUsuario(), this.getUsuario(), this.getEstadoRegistro());
        return destino;
    }

    public entTelefono cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("numero")); }
        catch(Exception e) {}
        try { this.setIdDependencia(rs.getInt("iddependencia")); }
        catch(Exception e) {}
        try { this.setDependencia(rs.getString("dependencia")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuario(rs.getString("usuario")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdDependencia(int iidDependencia) {
        this.iidDependencia = iidDependencia;
    }

    public void setDependencia(String sdependencia) {
        this.sdependencia = sdependencia;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }

    public void setUsuario(String susuario) {
        this.susuario = susuario;
    }

    public int getIdDependencia() {
        return this.iidDependencia;
    }

    public String getDependencia() {
        return this.sdependencia;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }

    public String getUsuario() {
        return this.susuario;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdDependencia()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DEPENDENCIA;
            bvalido = false;
        }
        if (this.getIdUsuario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_USUARIO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT telefono("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdDependencia()+","+
            this.getIdUsuario()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Dependencia", "dependencia", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Usuario", "usuario", generico.entLista.tipo_texto));
        return lst;
    }
    
}
