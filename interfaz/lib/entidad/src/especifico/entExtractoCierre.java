/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entExtractoCierre {

    private int iidSocio;
    private int iidMovimiento;
    private String stipo;
    private java.util.Date dfecha;
    private int iidCuenta;
    private String scuenta;
    private String sdenominacion;
    private int inumeroOperacion;
    private java.util.Date dfechaOperacion;
    private int iplazo;
    private int iidTipoCredito;
    private String stipoCredito;
    private int iidEntidad;
    private String sentidad;
    private double umontoCierre;
    private double umontoCobro;
    private double umontoDiferencia;
    private String stabla;
    private String srubro;
    private int iestadoRegistro;
    
    public entExtractoCierre() {
        this.iidSocio = 0;
        this.iidMovimiento = 0;
        this.stipo = "";
        this.dfecha = null;
        this.iidCuenta = 0;
        this.scuenta = "";
        this.sdenominacion = "";
        this.inumeroOperacion = 0;
        this.dfechaOperacion = null;
        this.iplazo = 0;
        this.iidTipoCredito = 0;
        this.stipoCredito = "";
        this.iidEntidad = 0;
        this.sentidad = "";
        this.umontoCierre = 0.0;
        this.umontoCobro = 0.0;
        this.umontoDiferencia = 0.0;
        this.stabla = "";
        this.srubro = "";
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entExtractoCierre(int iidSocio, int iidMovimiento, String stipo, java.util.Date dfecha, int iidCuenta, String scuenta, String sdenominacion, int inumeroOperacion, java.util.Date dfechaOperacion, int iplazo, int iidTipoCredito, String stipoCredito, int iidEntidad, String sentidad, double umontoCierre, double umontoCobro, double umontoDiferencia, String stabla, String srubro, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.stipo = stipo;
        this.dfecha = dfecha;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdenominacion = sdenominacion;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaOperacion = dfechaOperacion;
        this.iplazo = iplazo;
        this.iidTipoCredito = iidTipoCredito;
        this.stipoCredito = stipoCredito;
        this.iidEntidad = iidEntidad;
        this.sentidad = sentidad;
        this.umontoCierre = umontoCierre;
        this.umontoCobro = umontoCobro;
        this.umontoDiferencia = umontoDiferencia;
        this.stabla = stabla;
        this.srubro = srubro;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iidSocio, int iidMovimiento, String stipo, java.util.Date dfecha, int iidCuenta, String scuenta, String sdenominacion, int inumeroOperacion, java.util.Date dfechaOperacion, int iplazo, int iidTipoCredito, String stipoCredito, int iidEntidad, String sentidad, double umontoCierre, double umontoCobro, double umontoDiferencia, String stabla, String srubro, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.iidMovimiento = iidMovimiento;
        this.stipo = stipo;
        this.dfecha = dfecha;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdenominacion = sdenominacion;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaOperacion = dfechaOperacion;
        this.iplazo = iplazo;
        this.iidTipoCredito = iidTipoCredito;
        this.stipoCredito = stipoCredito;
        this.iidEntidad = iidEntidad;
        this.sentidad = sentidad;
        this.umontoCierre = umontoCierre;
        this.umontoCobro = umontoCobro;
        this.umontoDiferencia = umontoDiferencia;
        this.stabla = stabla;
        this.srubro = srubro;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entExtractoCierre copiar(entExtractoCierre destino) {
        destino.setEntidad(this.getIdSocio(), this.getIdMovimiento(), this.getTipo(), this.getFecha(), this.getIdCuenta(), this.getCuenta(), this.getDenominacion(), this.getNumeroOperacion(), this.getFechaOperacion(), this.getPlazo(), this.getIdTipoCredito(), this.getTipoCredito(), this.getIdEntidad(), this.getEntidad(), this.getMontoCierre(), this.getMontoCobro(), this.getMontoDiferencia(), this.getTabla(), this.getRubro(), this.getEstadoRegistro());
        return destino;
    }

    public entExtractoCierre cargar(java.sql.ResultSet rs) {
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setTipo(rs.getString("tipo")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setDenominacion(rs.getString("denominacion")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setIdTipoCredito(rs.getInt("idtipocredito")); }
        catch(Exception e) {}
        try { this.setTipoCredito(rs.getString("tipocredito")); }
        catch(Exception e) {}
        try { this.setIdEntidad(rs.getInt("identidad")); }
        catch(Exception e) {}
        try { this.setEntidad(rs.getString("entidad")); }
        catch(Exception e) {}
        try { this.setMontoCierre(rs.getDouble("montocierre")); }
        catch(Exception e) {}
        try { this.setMontoCobro(rs.getDouble("montocobro")); }
        catch(Exception e) {}
        try { this.setMontoDiferencia(rs.getDouble("montodiferencia")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setTipo(String stipo) {
        this.stipo = stipo;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setDenominacion(String sdenominacion) {
        this.sdenominacion = sdenominacion;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = (java.util.Date)dfechaOperacion.clone();
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setIdTipoCredito(int iidTipoCredito) {
        this.iidTipoCredito = iidTipoCredito;
    }

    public void setTipoCredito(String stipoCredito) {
        this.stipoCredito = stipoCredito;
    }

    public void setIdEntidad(int iidEntidad) {
        this.iidEntidad = iidEntidad;
    }

    public void setEntidad(String sentidad) {
        this.sentidad = sentidad;
    }

    public void setMontoCierre(double umontoCierre) {
        this.umontoCierre = umontoCierre;
    }

    public void setMontoCobro(double umontoCobro) {
        this.umontoCobro = umontoCobro;
    }

    public void setMontoDiferencia(double umontoDiferencia) {
        this.umontoDiferencia = umontoDiferencia;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public String getTipo() {
        return this.stipo;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getDenominacion() {
        return this.sdenominacion;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public int getIdTipoCredito() {
        return this.iidTipoCredito;
    }

    public String getTipoCredito() {
        return this.stipoCredito;
    }

    public int getIdEntidad() {
        return this.iidEntidad;
    }

    public String getEntidad() {
        return this.sentidad;
    }

    public double getMontoCierre() {
        return this.umontoCierre;
    }

    public double getMontoCobro() {
        return this.umontoCobro;
    }

    public double getMontoDiferencia() {
        return this.umontoDiferencia;
    }

    public String getTabla() {
        return this.stabla;
    }

    public String getRubro() {
        return this.srubro;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    public String getConsulta(java.util.Date dfecha, int iidSocio, int iidMutual, int iidAporteCapital, int iidSolidaridad, int iidCuotaSocial, int iidFondoPrevision, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, int iidInteresMoratorio, int iidInteresPunitorio, double utasaInteresPunitorio, int iidInteresPrestamo, int iidEstadoOrdenCredito, int iidEstadoPrestamo, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 'P' AS tipo, mo.id AS idmovimiento, c.descripcion||' ('||e.descripcion||')' AS denominacion, mo.idcuenta, c.descripcion AS cuenta, mo.identidad, e.descripcion AS entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres "+
                       "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                       "     ELSE mo.tasainteres END AS tasainteres, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres*"+utasaInteresPunitorio+"/100 "+
                       "     WHEN mo.tasainteres>5 THEN (mo.tasainteres/mo.plazoaprobado)*"+utasaInteresPunitorio+"/100 "+
                       "     ELSE mo.tasainteres*"+utasaInteresPunitorio+"/100 END AS tasainterespunitorio, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.montoaprobado AS montototal, mo.idtipocredito, tc.descripcionbreve AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoOrdenCredito+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito = tc.id "+ // ORDEN DE CREDITO
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'P' AS tipo, mo.id AS idmovimiento, c.descripcion||' '||tc.descripcionbreve AS denominacion, mo.idcuenta, c.descripcion AS cuenta, mo.identidad, e.descripcion AS entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres "+
                       "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                       "     ELSE mo.tasainteres END AS tasainteres, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres*"+utasaInteresPunitorio+"/100 "+
                       "     WHEN mo.tasainteres>5 THEN (mo.tasainteres/mo.plazoaprobado)*"+utasaInteresPunitorio+"/100 "+
                       "     ELSE mo.tasainteres*"+utasaInteresPunitorio+"/100 END AS tasainterespunitorio, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.montoaprobado AS montototal, mo.idtipocredito, tc.descripcionbreve AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoPrestamo+" AND identidad="+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito = tc.id "+ // PRESTAMO MUTUAL
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'P' AS tipo, mo.id AS idmovimiento, c.descripcion||' '||tc.descripcionbreve AS denominacion, "+iidInteresPrestamo+" AS idcuenta, c.descripcion AS cuenta, mo.identidad, e.descripcion AS entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres "+
                       "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                       "     ELSE mo.tasainteres END AS tasainteres, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres*"+utasaInteresPunitorio+"/100 "+
                       "     WHEN mo.tasainteres>5 THEN (mo.tasainteres/mo.plazoaprobado)*"+utasaInteresPunitorio+"/100 "+
                       "     ELSE mo.tasainteres*"+utasaInteresPunitorio+"/100 END AS tasainterespunitorio, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, 0 AS montototal, mo.idtipocredito, tc.descripcionbreve AS tipocredito, dop.fechavencimiento, dop.montointeres AS cuota, dop.saldointeres AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoPrestamo+" AND identidad="+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito=tc.id "+ // INTERES PRESTAMO
                       "LEFT JOIN cuenta AS c ON "+iidInteresPrestamo+"=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'P' AS tipo, mo.id AS idmovimiento, c.descripcion||' '||tc.descripcionbreve||' ('||e.descripcion||')' AS denominacion, mo.idcuenta, c.descripcion AS cuenta, mo.identidad, e.descripcion AS entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres "+
                       "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                       "     ELSE mo.tasainteres END AS tasainteres, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres*"+utasaInteresPunitorio+"/100 "+
                       "     WHEN mo.tasainteres>5 THEN (mo.tasainteres/mo.plazoaprobado)*"+utasaInteresPunitorio+"/100 "+
                       "     ELSE mo.tasainteres*"+utasaInteresPunitorio+"/100 END AS tasainterespunitorio, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.montoaprobado AS montototal, mo.idtipocredito, tc.descripcionbreve AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoPrestamo+" AND identidad<>"+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito = tc.id "+ // PRESTAMO FINANCIERA
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS denominacion, "+iidInteresMoratorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS cuenta, 0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, 0 AS montototal, 0 AS idtipocredito, '' AS tipocredito, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo, '' AS rubro, 0 AS numerocuota "+
                   "UNION " +
                   "SELECT 'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS denominacion, "+iidInteresPunitorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS cuenta, 0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, 0 AS montototal, 0 AS idtipocredito, '' AS tipocredito, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo, '' AS rubro, 0 AS numerocuota "+
                   "UNION " +
                   "SELECT 'O' AS tipo, ap.id AS idmovimiento, c.descripcion||' ('||pa.descripcion||')' AS denominacion, ap.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, dop.tabla, ap.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, ap.plazo, FALSE AS cancelacion, ap.numerooperacion, 0 AS numerosolicitud, ap.importe*ap.plazo AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM ahorroprogramado WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoAhorro+") AS ap LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=ap.id "+
                       "LEFT JOIN cuenta AS c ON ap.idcuenta=c.id "+
                       "LEFT JOIN planahorroprogramado AS pa ON ap.idplan=pa.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=ap.id AND cr.tabla='ap' "+
                       "WHERE dop.tabla='ap' "+
                   "UNION " +
                   "SELECT 'O' AS tipo, of.id AS idmovimiento, c.descripcion AS denominacion, of.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, dop.tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, of.importe AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM operacionfija WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoOperacion+" AND idcuenta<>"+iidAporteCapital+" AND idcuenta<>"+iidSolidaridad+" AND idcuenta<>"+iidCuotaSocial+" AND idcuenta<>"+iidFondoPrevision+") AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                       "WHERE dop.tabla='of' "+
                   "UNION " +
                   "SELECT 'O' AS tipo, js.id AS idmovimiento, c.descripcion AS denominacion, js.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, dop.tabla, js.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 0 AS plazo, FALSE AS cancelacion, js.numerooperacion, 0 AS numerosolicitud, js.importetitular+js.importeadherente*js.cantidadadherente AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM fondojuridicosepelio WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoFondo+") AS js LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=js.id "+
                       "LEFT JOIN cuenta AS c ON js.idcuenta=c.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=js.id AND cr.tabla='js' "+
                       "WHERE dop.tabla='js' "+
                   "UNION " +
                   "SELECT 'A' AS tipo, of.id AS idmovimiento, c.descripcion AS denominacion, of.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, dop.tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, of.importe AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM operacionfija WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoOperacion+" AND (idcuenta="+iidAporteCapital+" OR idcuenta="+iidSolidaridad+" OR idcuenta="+iidCuotaSocial+" OR idcuenta="+iidFondoPrevision+")) AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                       "WHERE dop.tabla='of' "+
               ") AS detalle "+sorden;
    }
    
    public String getConsultaEstado(String sfiltro, String sorden) {
        return "SELECT * FROM detalleoperacion WHERE "+sfiltro+sorden;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
