/*0
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleIngresoCancelacion {
    
    private int iidDetalleOperacion;
    private int icuota;
    private java.util.Date dfechaVencimiento;
    private double umontoCapital;
    private double umontoInteres;
    private double umontoSaldo;
    private double umontoExoneracion;
    private double umontoCobro;
    private double utasaInteresMoratorio;
    private double umontoInteresMoratorio;
    private double utasaInteresPunitorio;
    private double umontoInteresPunitorio;
    private String sestado; // A=atraso - S=saldo - E=enviado
    private boolean bcancela;
    private boolean batraso;
    private int iorden;

    private short hestadoRegistro;

    public entDetalleIngresoCancelacion() {
        this.iidDetalleOperacion = 0;
        this.icuota = 0;
        this.dfechaVencimiento = null;
        this.umontoCapital = 0.0;
        this.umontoInteres = 0.0;
        this.umontoSaldo = 0.0;
        this.umontoExoneracion = 0.0;
        this.umontoCobro = 0.0;
        this.utasaInteresMoratorio = 0.0;
        this.umontoInteresMoratorio = 0.0;
        this.utasaInteresPunitorio = 0.0;
        this.umontoInteresPunitorio = 0.0;
        this.sestado = "";
        this.bcancela = false;
        this.batraso = false;
        this.iorden = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entDetalleIngresoCancelacion(int iidDetalleOperacion, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double umontoSaldo, double umontoExoneracion, double umontoCobro, double utasaInteresMoratorio, double umontoInteresMoratorio, double utasaInteresPunitorio, double umontoInteresPunitorio, String sestado, boolean bcancela, boolean batraso, int iorden, short hestadoRegistro) {
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoSaldo = umontoSaldo;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoCobro = umontoCobro;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.umontoInteresMoratorio = umontoInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.umontoInteresPunitorio = umontoInteresPunitorio;
        this.sestado = sestado;
        this.bcancela = bcancela;
        this.batraso = batraso;
        this.iorden = iorden;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iidDetalleOperacion, int icuota, java.util.Date dfechaVencimiento, double umontoCapital, double umontoInteres, double umontoSaldo, double umontoExoneracion, double umontoCobro, double utasaInteresMoratorio, double umontoInteresMoratorio, double utasaInteresPunitorio, double umontoInteresPunitorio, String sestado, boolean bcancela, boolean batraso, int iorden, short hestadoRegistro) {
        this.iidDetalleOperacion = iidDetalleOperacion;
        this.icuota = icuota;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoCapital = umontoCapital;
        this.umontoInteres = umontoInteres;
        this.umontoSaldo = umontoSaldo;
        this.umontoExoneracion = umontoExoneracion;
        this.umontoCobro = umontoCobro;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.umontoInteresMoratorio = umontoInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.umontoInteresPunitorio = umontoInteresPunitorio;
        this.sestado = sestado;
        this.bcancela = bcancela;
        this.batraso = batraso;
        this.iorden = iorden;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDetalleIngresoCancelacion copiar(entDetalleIngresoCancelacion destino) {
        destino.setEntidad(this.getIdDetalleOperacion(), this.getCuota(), this.getFechaVencimiento(), this.getMontoCapital(), this.getMontoInteres(), this.getMontoSaldo(), this.getMontoExoneracion(), this.getMontoCobro(), this.getTasaInteresMoratorio(), this.getMontoInteresMoratorio(), this.getTasaInteresPunitorio(), this.getMontoInteresPunitorio(), this.getEstado(), this.getCancela(), this.getAtraso(), this.getOrden(), this.getEstadoRegistro());
        return destino;
    }

    public void setIdDetalleOperacion(int iidDetalleOperacion) {
        this.iidDetalleOperacion = iidDetalleOperacion;
    }

    public void setCuota(int icuota) {
        this.icuota = icuota;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setMontoCapital(double umontoCapital) {
        this.umontoCapital = umontoCapital;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setMontoSaldo(double umontoSaldo) {
        this.umontoSaldo = umontoSaldo;
    }

    public void setMontoExoneracion(double umontoExoneracion) {
        this.umontoExoneracion = umontoExoneracion;
    }

    public void setMontoCobro(double umontoCobro) {
        this.umontoCobro = umontoCobro;
    }

    public void setTasaInteresMoratorio(double utasaInteresMoratorio) {
        this.utasaInteresMoratorio = utasaInteresMoratorio;
    }

    public void setMontoInteresMoratorio(double umontoInteresMoratorio) {
        this.umontoInteresMoratorio = umontoInteresMoratorio;
    }

    public void setTasaInteresPunitorio(double utasaInteresPunitorio) {
        this.utasaInteresPunitorio = utasaInteresPunitorio;
    }

    public void setMontoInteresPunitorio(double umontoInteresPunitorio) {
        this.umontoInteresPunitorio = umontoInteresPunitorio;
    }

    public void setEstado(String sestado) {
        this.sestado = sestado;
    }

    public void setCancela(boolean bcancela) {
        this.bcancela = bcancela;
    }

    public void setAtraso(boolean batraso) {
        this.batraso = batraso;
    }

    public void setOrden(int iorden) {
        this.iorden = iorden;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getIdDetalleOperacion() {
        return this.iidDetalleOperacion;
    }

    public int getCuota() {
        return this.icuota;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getMontoCapital() {
        return this.umontoCapital;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public double getMontoSaldo() {
        return this.umontoSaldo;
    }

    public double getMontoExoneracion() {
        return this.umontoExoneracion;
    }

    public double getMontoCobro() {
        return this.umontoCobro;
    }

    public double getTasaInteresMoratorio() {
        return this.utasaInteresMoratorio;
    }

    public double getMontoInteresMoratorio() {
        return this.umontoInteresMoratorio;
    }

    public double getTasaInteresPunitorio() {
        return this.utasaInteresPunitorio;
    }

    public double getMontoInteresPunitorio() {
        return this.umontoInteresPunitorio;
    }

    public String getEstado() {
        return this.sestado;
    }

    public boolean getCancela() {
        return this.bcancela;
    }

    public boolean getAtraso() {
        return this.batraso;
    }

    public int getOrden() {
        return this.iorden;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

}
