/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPlanillaCajaChica extends generico.entGenerica {

    protected int inumeroPlanilla;
    protected int iidFuncionario;
    protected String sfuncionario;
    protected java.util.Date dfechaApertura;
    protected String shoraApertura;
    protected java.util.Date dfechaCierre;
    protected String shoraCierre;
    protected boolean bactivo;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de planilla (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_NUMERO_PLANILLA = "Número de Planilla (no editable)";
    public static final String TEXTO_FUNCIONARIO = "Funcionario (no vacío)";
    public static final String TEXTO_FECHA_APERTURA = "Fecha de apertura de planilla (no vacía)";
    public static final String TEXTO_HORA_APERTURA = "Hora de apertura de planilla (no vacía)";
    public static final String TEXTO_FECHA_CIERRE = "Fecha de cierre de planilla (no editable)";
    public static final String TEXTO_HORA_CIERRE = "Hora de cierre de planilla (no editable)";
    public static final String TEXTO_ACTIVO = "Activo (no editable)";

    public entPlanillaCajaChica() {
        super();
        this.inumeroPlanilla = 0;
        this.iidFuncionario = 0;
        this.sfuncionario = "";
        this.dfechaApertura = null;
        this.shoraApertura = "";
        this.dfechaCierre = null;
        this.shoraCierre = "";
        this.bactivo = false;
    }

    public entPlanillaCajaChica(int iid, String sdescripcion, int inumeroPlanilla, int iidFuncionario, String sfuncionario, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, boolean bactivo) {
        super(iid, sdescripcion);
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.bactivo = bactivo;
    }

    public entPlanillaCajaChica(int iid, String sdescripcion, int inumeroPlanilla, int iidFuncionario, String sfuncionario, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, boolean bactivo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.bactivo = bactivo;
    }

    public void setEntidad(int iid, String sdescripcion, int inumeroPlanilla, int iidFuncionario, String sfuncionario, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidFuncionario = iidFuncionario;
        this.sfuncionario = sfuncionario;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entPlanillaCajaChica copiar(entPlanillaCajaChica destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getNumeroPlanilla(), this.getIdFuncionario(), this.getFuncionario(), this.getFechaApertura(), this.getHoraApertura(), this.getFechaCierre(), this.getHoraCierre(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }

    public entPlanillaCajaChica cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setNumeroPlanilla(rs.getInt("numeroplanilla")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        try { this.setFuncionario(rs.getString("funcionario")); }
        catch(Exception e) {}
        try { this.setFechaApertura(rs.getDate("fechaapertura")); }
        catch(Exception e) {}
        try { this.setHoraApertura(rs.getString("horaapertura")); }
        catch(Exception e) {}
        try { this.setFechaCierre(rs.getDate("fechacierre")); }
        catch(Exception e) {}
        try { this.setHoraCierre(rs.getString("horacierre")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setNumeroPlanilla(int inumeroPlanilla) {
        this.inumeroPlanilla = inumeroPlanilla;
    }

    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }

    public void setFuncionario(String sfuncionario) {
        this.sfuncionario = sfuncionario;
    }

    public void setFechaApertura(java.util.Date dfechaApertura) {
        this.dfechaApertura = dfechaApertura;
    }

    public void setHoraApertura(String shoraApertura) {
        this.shoraApertura = shoraApertura;
    }

    public void setFechaCierre(java.util.Date dfechaCierre) {
        this.dfechaCierre = dfechaCierre;
    }

    public void setHoraCierre(String shoraCierre) {
        this.shoraCierre = shoraCierre;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public int getNumeroPlanilla() {
        return this.inumeroPlanilla;
    }

    public int getIdFuncionario() {
        return this.iidFuncionario;
    }

    public String getFuncionario() {
        return this.sfuncionario;
    }

    public java.util.Date getFechaApertura() {
        return this.dfechaApertura;
    }

    public String getHoraApertura() {
        return this.shoraApertura;
    }

    public java.util.Date getFechaCierre() {
        return this.dfechaCierre;
    }

    public String getHoraCierre() {
        return this.shoraCierre;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdFuncionario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO;
            bvalido = false;
        }
        if (this.getFechaApertura()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_APERTURA;
            bvalido = false;
        }
        if (this.getHoraApertura().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_APERTURA;
            bvalido = false;
        } else {
            if (!utilitario.utiFecha.esValidoHoraLarga(this.getHoraApertura())) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Hora inválida";
                bvalido = false;
            }
        }
        return bvalido;
    }
    
    public boolean esValidoCerrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaCierre()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_CIERRE;
            bvalido = false;
        }
        if (this.getHoraCierre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_CIERRE;
            bvalido = false;
        } else {
            if (!utilitario.utiFecha.esValidoHoraLarga(this.getHoraCierre())) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Hora inválida";
                bvalido = false;
            }
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT planillacajachica("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getNumeroPlanilla()+","+
            this.getIdFuncionario()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaApertura())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraApertura())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaCierre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraCierre())+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número Planilla", "numeroplanilla", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Funcionario", "funcionario", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Fecha apertura", "fechaapertura", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(5, "Fecha cierre", "fechacierre", generico.entLista.tipo_fecha));
        return lst;
    }
    
}
