/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entGenericaPersona;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entGarante extends entGenericaPersona {
    
    protected String sdireccion;
    protected int inumeroCasa;
    protected int iidBarrio;
    protected String sbarrio;
    protected int iidCiudadResidencia;
    protected String sciudadResidencia;
    protected String stelefonoCelular;
    protected String stelefonoLineaBaja;
    protected java.util.Date dfechaNacimiento;
    protected int iidSexo;
    protected String ssexo;
    protected String slugarLaboral;
    protected double usueldo;
    protected int iidMoneda;
    protected String smoneda;
    protected String sdireccionLaboral;
    protected String stelefonoLaboral;
    protected String sreferenciaPersonal;
    protected String stelefonoReferencia;
    protected int iidTipoCasa;
    protected String stipoCasa;
    protected java.util.Date dfechaIngreso;
    protected int iidRubro;
    protected String srubro;
    protected int iidRegional;
    protected String sregional;
    protected int iidGiraduria;
    protected String sgiraduria;
    protected int iidCargo;
    protected String scargo;
    
    public static final int LONGITUD_CEDULA = 12;
    public static final int LONGITUD_NOMBRE = 40;
    public static final int LONGITUD_APELLIDO = 40;
    public static final int LONGITUD_DIRECCION = 100;
    public static final int LONGITUD_TELEFONO_CELULAR = 40;
    public static final int LONGITUD_TELEFONO_LINEA_BAJA = 40;
    public static final int LONGITUD_LUGAR_LABORAL = 50;
    public static final int LONGITUD_DIRECCION_LABORAL = 100;
    public static final int LONGITUD_TELEFONO_LABORAL = 40;
    public static final int LONGITUD_REFERENCIA_PERSONAL = 50;
    public static final int LONGITUD_TELEFONO_REFERENCIA = 40;
    
    public static final int INICIO_NUMERO_CASA = 0;
    public static final int FIN_NUMERO_CASA = 99999;
    public static final int INICIO_SUELDO = 0;
    public static final int FIN_SUELDO = 999999999;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_CEDULA = "Cédula (no vacía, única, hasta " + LONGITUD_CEDULA + " caracteres)";
    public static final String TEXTO_NOMBRE = "Nombre (no vacío, hasta " + LONGITUD_NOMBRE + " caracteres)";
    public static final String TEXTO_APELLIDO = "Apellido (no vacío, hasta " + LONGITUD_APELLIDO + " caracteres)";
    public static final String TEXTO_DIRECCION = "Dirección (no vacía, hasta " + LONGITUD_DIRECCION + " caracteres)";
    public static final String TEXTO_NUMERO_CASA = "Número de Casa (desde " + INICIO_NUMERO_CASA + " hasta " + FIN_NUMERO_CASA +")";
    public static final String TEXTO_BARRIO = "Barrio (no vacío)";
    public static final String TEXTO_CIUDAD_RESIDENCIA = "Ciudad de Residencia (no vacía)";
    public static final String TEXTO_SEXO = "Sexo (no vacío)";
    public static final String TEXTO_TELEFONO_CELULAR = "Teléfono Celular (no vacío, hasta " + LONGITUD_TELEFONO_CELULAR + " caracteres)";
    public static final String TEXTO_TELEFONO_LINEA_BAJA = "Teléfono Línea Baja (no vacío, hasta " + LONGITUD_TELEFONO_LINEA_BAJA + " caracteres)";
    public static final String TEXTO_FECHA_NACIMIENTO = "Fecha de Nacimiento (no vacío)";
    public static final String TEXTO_EDAD = "Edad en año, mes y día (no editable)";
    public static final String TEXTO_LUGAR_LABORAL = "Lugar Laboral (no vacía, hasta " + LONGITUD_LUGAR_LABORAL +" caracteres)";
    public static final String TEXTO_SUELDO = "Sueldo (desde " + INICIO_SUELDO + " hasta " + FIN_SUELDO +")";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_DIRECCION_LABORAL = "Dirección del Lugar Laboral (hasta " + LONGITUD_DIRECCION_LABORAL +" caracteres)";
    public static final String TEXTO_TELEFONO_LABORAL = "Teléfono del Lugar Laboral (hasta " + LONGITUD_TELEFONO_LABORAL +" caracteres)";
    public static final String TEXTO_REFERENCIA_PERSONAL = "Referencia Personal (hasta " + LONGITUD_REFERENCIA_PERSONAL +" caracteres)";
    public static final String TEXTO_TELEFONO_REFERENCIA = "Teléfono de la Referencia Personal (hasta " + LONGITUD_TELEFONO_REFERENCIA +" caracteres)";
    public static final String TEXTO_TIPO_CASA = "Tipo de Casa (no vacía)";
    public static final String TEXTO_FECHA_INGRESO = "Fecha de Ingreso";
    public static final String TEXTO_RUBRO = "Rubro (no vacío)";
    public static final String TEXTO_REGIONAL = "Regional (no vacía)";
    public static final String TEXTO_GIRADURIA = "Giraduría (no vacía)";
    public static final String TEXTO_CARGO = "Cargo (no vacío)";
    
    public entGarante() {
        super();
        this.sdireccion = "";
        this.inumeroCasa = 0;
        this.iidBarrio = 0;
        this.sbarrio = "";
        this.iidCiudadResidencia = 0;
        this.sciudadResidencia = "";
        this.stelefonoCelular = "";
        this.stelefonoLineaBaja = "";
        this.dfechaNacimiento = null;
        this.iidSexo = 0;
        this.ssexo = "";
        this.slugarLaboral = "";
        this.usueldo = 0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.sdireccionLaboral = "";
        this.stelefonoLaboral = "";
        this.sreferenciaPersonal = "";
        this.stelefonoReferencia = "";
        this.iidTipoCasa = 0;
        this.stipoCasa = "";
        this.dfechaIngreso = null;
        this.iidRubro = 0;
        this.srubro = "";
        this.iidRegional = 0;
        this.sregional = "";
        this.iidGiraduria = 0;
        this.sgiraduria = "";
        this.iidCargo = 0;
        this.scargo = "";
    }

    public entGarante(int iid, String snombre, String sapellido, String scedula, String sdireccion, int inumeroCasa, int iidBarrio, String sbarrio, int iidCiudadResidencia, String sciudadResidencia, String stelefonoCelular, String stelefonoLineaBaja, java.util.Date dfechaNacimiento, int iidSexo, String ssexo, String slugarLaboral, double usueldo, int iidMoneda, String smoneda, String sdireccionLaboral, String stelefonoLaboral, String sreferenciaPersonal, String stelefonoReferencia, int iidTipoCasa, String stipoCasa, java.util.Date dfechaIngreso, int iidRubro, String srubro, int iidRegional, String sregional, int iidGiraduria, String sgiraduria, int iidCargo, String scargo, short hestadoRegistro) {
        super(iid, snombre, sapellido, scedula, hestadoRegistro);
        this.sdireccion = sdireccion.trim().toUpperCase();
        this.inumeroCasa = inumeroCasa;
        this.iidBarrio = iidBarrio;
        this.sbarrio = sbarrio;
        this.iidCiudadResidencia = iidCiudadResidencia;
        this.sciudadResidencia = sciudadResidencia;
        this.stelefonoCelular = stelefonoCelular.trim();
        this.stelefonoLineaBaja = stelefonoLineaBaja.trim();
        this.dfechaNacimiento = dfechaNacimiento;
        this.iidSexo = iidSexo;
        this.ssexo = ssexo;
        this.slugarLaboral = slugarLaboral;
        this.usueldo = usueldo;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.sdireccionLaboral = sdireccionLaboral;
        this.stelefonoLaboral = stelefonoLaboral;
        this.sreferenciaPersonal = sreferenciaPersonal;
        this.stelefonoReferencia = stelefonoReferencia;
        this.iidTipoCasa = iidTipoCasa;
        this.stipoCasa = stipoCasa;
        this.dfechaIngreso = dfechaIngreso;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.iidCargo = iidCargo;
        this.scargo = scargo;
    }

    public void setEntidad(int iid, String snombre, String sapellido, String scedula, String sdireccion, int inumeroCasa, int iidBarrio, String sbarrio, int iidCiudadResidencia, String sciudadResidencia, String stelefonoCelular, String stelefonoLineaBaja, java.util.Date dfechaNacimiento, int iidSexo, String ssexo, String slugarLaboral, double usueldo, int iidMoneda, String smoneda, String sdireccionLaboral, String stelefonoLaboral, String sreferenciaPersonal, String stelefonoReferencia, int iidTipoCasa, String stipoCasa, java.util.Date dfechaIngreso, int iidRubro, String srubro, int iidRegional, String sregional, int iidGiraduria, String sgiraduria, int iidCargo, String scargo, short hestadoRegistro) {
        this.iid = iid;
        this.snombre = snombre.trim().toUpperCase();
        this.sapellido = sapellido.trim().toUpperCase();
        this.scedula = scedula;
        this.sdireccion = sdireccion.trim().toUpperCase();
        this.inumeroCasa = inumeroCasa;
        this.iidBarrio = iidBarrio;
        this.sbarrio = sbarrio;
        this.iidCiudadResidencia = iidCiudadResidencia;
        this.sciudadResidencia = sciudadResidencia;
        this.stelefonoCelular = stelefonoCelular.trim();
        this.stelefonoLineaBaja = stelefonoLineaBaja.trim();
        this.dfechaNacimiento = dfechaNacimiento;
        this.iidSexo = iidSexo;
        this.ssexo = ssexo;
        this.slugarLaboral = slugarLaboral;
        this.usueldo = usueldo;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.sdireccionLaboral = sdireccionLaboral;
        this.stelefonoLaboral = stelefonoLaboral;
        this.sreferenciaPersonal = sreferenciaPersonal;
        this.stelefonoReferencia = stelefonoReferencia;
        this.iidTipoCasa = iidTipoCasa;
        this.stipoCasa = stipoCasa;
        this.dfechaIngreso = dfechaIngreso;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.iidCargo = iidCargo;
        this.scargo = scargo;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entGarante copiar(entGarante destino) {
        destino.setEntidad(this.getId(), this.getNombre(), this.getApellido(), this.getCedula(), this.getDireccion(), this.getNumeroCasa(), this.getIdBarrio(), this.getBarrio(), this.getIdCiudadResidencia(), this.getCiudadResidencia(), this.getTelefonoCelular(), this.getTelefonoLineaBaja(), this.getFechaNacimiento(), this.getIdSexo(), this.getSexo(), this.getLugarLaboral(), this.getSueldo(), this.getIdMoneda(), this.getMoneda(), this.getDireccionLaboral(), this.getTelefonoLaboral(), this.getReferenciaPersonal(), this.getTelefonoReferencia(), this.getIdTipoCasa(), this.getTipoCasa(), this.getFechaIngreso(), this.getIdRubro(), this.getRubro(), this.getIdRegional(), this.getRegional(), this.getIdGiraduria(), this.getGiraduria(), this.getIdCargo(), this.getCargo(), this.getEstadoRegistro());
        return destino;
    }
    
    public entGarante cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) { }
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) { }
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) { }
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) { }
        try { this.setDireccion(rs.getString("direccion")); }
        catch(Exception e) { }
        try { this.setNumeroCasa(rs.getInt("numerocasa")); }
        catch(Exception e) { }
        try { this.setIdBarrio(rs.getInt("idbarrio")); }
        catch(Exception e) { }
        try { this.setBarrio(rs.getString("barrio")); }
        catch(Exception e) { }
        try { this.setIdCiudadResidencia(rs.getInt("idciudadresidencia")); }
        catch(Exception e) { }
        try { this.setCiudadResidencia(rs.getString("ciudadresidencia")); }
        catch(Exception e) { }
        try { this.setTelefonoCelular(rs.getString("telefonocelular")); }
        catch(Exception e) { }
        try { this.setTelefonoLineaBaja(rs.getString("telefonolineabaja")); }
        catch(Exception e) { }
        try { this.setFechaNacimiento(rs.getDate("fechanacimiento")); }
        catch(Exception e) { }
        try { this.setIdSexo(rs.getInt("idsexo")); }
        catch(Exception e) { }
        try { this.setSexo(rs.getString("sexo")); }
        catch(Exception e) { }
        try { this.setLugarLaboral(rs.getString("lugarlaboral")); }
        catch(Exception e) { }
        try { this.setSueldo(rs.getDouble("sueldo")); }
        catch(Exception e) { }
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) { }
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) { }
        try { this.setDireccionLaboral(rs.getString("direccionlaboral")); }
        catch(Exception e) { }
        try { this.setTelefonoLaboral(rs.getString("telefonolaboral")); }
        catch(Exception e) { }
        try { this.setReferenciaPersonal(rs.getString("referenciapersonal")); }
        catch(Exception e) { }
        try { this.setTelefonoReferencia(rs.getString("telefonoreferencia")); }
        catch(Exception e) { }
        try { this.setIdTipoCasa(rs.getInt("idtipocasa")); }
        catch(Exception e) { }
        try { this.setTipoCasa(rs.getString("tipocasa")); }
        catch(Exception e) { }
        try { this.setFechaIngreso(rs.getDate("fechaingreso")); }
        catch(Exception e) { }
        try { this.setIdRubro(rs.getInt("idrubro")); }
        catch(Exception e) { }
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) { }
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) { }
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) { }
        try { this.setIdGiraduria(rs.getInt("idgiraduria")); }
        catch(Exception e) { }
        try { this.setGiraduria(rs.getString("giraduria")); }
        catch(Exception e) { }
        try { this.setIdCargo(rs.getInt("idcargo")); }
        catch(Exception e) { }
        try { this.setCargo(rs.getString("cargo")); }
        catch(Exception e) { }
        return this;
    }
    
    public void setDireccion(String sdireccion) {
        this.sdireccion = sdireccion.trim().toUpperCase();
    }
    
    public void setNumeroCasa(int inumeroCasa) {
        this.inumeroCasa = inumeroCasa;
    }

    public void setIdBarrio(int iidBarrio) {
        this.iidBarrio = iidBarrio;
    }

    public void setBarrio(String sbarrio) {
        this.sbarrio = sbarrio.trim().toUpperCase();
    }
    
    public void setIdCiudadResidencia(int iidCiudadResidencia) {
        this.iidCiudadResidencia = iidCiudadResidencia;
    }

    public void setCiudadResidencia(String sciudadResidencia) {
        this.sciudadResidencia = sciudadResidencia.trim().toUpperCase();
    }
        
    public void setTelefonoCelular(String stelefonoCelular) {
        this.stelefonoCelular = stelefonoCelular.trim();
    }
    
    public void setTelefonoLineaBaja(String stelefonoLineaBaja) {
        this.stelefonoLineaBaja = stelefonoLineaBaja.trim();
    }

    public void setFechaNacimiento(java.util.Date dfechaNacimiento) {
        this.dfechaNacimiento = dfechaNacimiento;
    }

    public void setIdSexo(int iidSexo) {
        this.iidSexo = iidSexo;
    }

    public void setSexo(String ssexo) {
        this.ssexo = ssexo;
    }

    public void setLugarLaboral(String slugarLaboral) {
        this.slugarLaboral = slugarLaboral;
    }

    public void setSueldo(double usueldo) {
        this.usueldo = usueldo;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }

    public void setDireccionLaboral(String sdireccionLaboral) {
        this.sdireccionLaboral = sdireccionLaboral;
    }

    public void setTelefonoLaboral(String stelefonoLaboral) {
        this.stelefonoLaboral = stelefonoLaboral;
    }

    public void setReferenciaPersonal(String sreferenciaPersonal) {
        this.sreferenciaPersonal = sreferenciaPersonal;
    }

    public void setTelefonoReferencia(String stelefonoReferencia) {
        this.stelefonoReferencia = stelefonoReferencia;
    }

    public void setIdTipoCasa(int iidTipoCasa) {
        this.iidTipoCasa = iidTipoCasa;
    }

    public void setTipoCasa(String stipoCasa) {
        this.stipoCasa = stipoCasa;
    }

    public void setFechaIngreso(java.util.Date dfechaIngreso) {
        this.dfechaIngreso = dfechaIngreso;
    }

    public void setIdRubro(int iidRubro) {
        this.iidRubro = iidRubro;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }

    public void setRegional(String sregional) {
        this.sregional = sregional;
    }

    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }

    public void setGiraduria(String sgiraduria) {
        this.sgiraduria = sgiraduria;
    }

    public void setIdCargo(int iidCargo) {
        this.iidCargo = iidCargo;
    }

    public void setCargo(String scargo) {
        this.scargo = scargo;
    }

    public String getDireccion() {
        return this.sdireccion;
    }
    
    public int getNumeroCasa() {
        return this.inumeroCasa;
    }

    public int getIdBarrio() {
        return this.iidBarrio;
    }

    public String getBarrio() {
        return this.sbarrio;
    }
    
    public int getIdCiudadResidencia() {
        return this.iidCiudadResidencia;
    }

    public String getCiudadResidencia() {
        return this.sciudadResidencia;
    }
        
    public String getTelefonoCelular() {
        return this.stelefonoCelular;
    }
    
    public String getTelefonoLineaBaja() {
        return this.stelefonoLineaBaja;
    }

    public java.util.Date getFechaNacimiento() {
        return this.dfechaNacimiento;
    }

    public int getIdSexo() {
        return this.iidSexo;
    }

    public String getSexo() {
        return this.ssexo;
    }

    public String getLugarLaboral() {
        return this.slugarLaboral;
    }

    public double getSueldo() {
        return this.usueldo;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }
    
    public String getDireccionLaboral() {
        return this.sdireccionLaboral;
    }

    public String getTelefonoLaboral() {
        return this.stelefonoLaboral;
    }

    public String getReferenciaPersonal() {
        return this.sreferenciaPersonal;
    }

    public String getTelefonoReferencia() {
        return this.stelefonoReferencia;
    }

    public int getIdTipoCasa() {
        return this.iidTipoCasa;
    }

    public String getTipoCasa() {
        return this.stipoCasa;
    }
    
    public java.util.Date getFechaIngreso() {
        return this.dfechaIngreso;
    }
    
    public int getIdRubro() {
        return this.iidRubro;
    }

    public String getRubro() {
        return this.srubro;
    }
    
    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }
    
    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public String getGiraduria() {
        return this.sgiraduria;
    }
    
    public int getIdCargo() {
        return this.iidCargo;
    }

    public String getCargo() {
        return this.scargo;
    }
    
    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getCedula().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CEDULA;
            bvalido = false;
        }
        if (esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CEDULA;
            bvalido = false;
        }
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_APELLIDO;
            bvalido = false;
        }
        if (!(this.getNumeroCasa()>=INICIO_NUMERO_CASA && this.getNumeroCasa()<=FIN_NUMERO_CASA)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_CASA;
            bvalido = false;
        }
        if (this.getIdBarrio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_BARRIO;
            bvalido = false;
        }
        if (this.getIdCiudadResidencia()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CIUDAD_RESIDENCIA;
            bvalido = false;
        }
        if (this.getIdSexo()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SEXO;
            bvalido = false;
        }
        if (!(this.getSueldo()>=INICIO_SUELDO && this.getSueldo()<=FIN_SUELDO)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SUELDO;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getIdTipoCasa()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_CASA;
            bvalido = false;
        }
        if (this.getIdRubro()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RUBRO;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        if (this.getIdGiraduria()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_GIRADURIA;
            bvalido = false;
        }
        if (this.getIdCargo()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CARGO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT garante("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccion())+","+
            this.getNumeroCasa()+","+
            this.getIdBarrio()+","+
            this.getIdCiudadResidencia()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoCelular())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoLineaBaja())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaNacimiento())+","+
            this.getIdSexo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getLugarLaboral())+","+
            this.getSueldo()+","+
            this.getIdMoneda()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDireccionLaboral())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoLaboral())+","+
            utilitario.utiCadena.getTextoGuardado(this.getReferenciaPersonal())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoReferencia())+","+
            this.getIdTipoCasa()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaIngreso())+","+
            this.getIdRubro()+","+
            this.getIdRegional()+","+
            this.getIdGiraduria()+","+
            this.getIdCargo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Nombre", "nombre", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Apellido", "apellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Dirección", "direccion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Barrio", "barrio", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Ciudad Residencia", "ciudadresidencia", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Teléfono Celular", "telefonocelular", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(8, "Teléfono Línea Baja", "telefonolineabaja", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(9, "Fecha Nacimiento", "fechanacimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(10, "Sexo", "sexo", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(11, "Lugar Laboral", "lugarlaboral", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(12, "Sueldo", "sueldo", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(13, "Dirección Laboral", "direccionlaboral", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(14, "Telefono Laboral", "telefonolaboral", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(15, "Referencia Personal", "referencialaboral", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(16, "Telefono Referencia", "telefonoreferencia", generico.entLista.tipo_texto));
        return lst;
    }
    
}
