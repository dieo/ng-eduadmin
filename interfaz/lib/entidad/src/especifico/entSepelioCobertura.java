/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entSepelioCobertura extends generico.entGenerica {

    protected double dmonto;
    protected int iidMoneda;
    protected String smoneda;
    protected int iantiguedadMinima;
    protected boolean bactivo;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Cobertura (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_MONTO = "Monto (no vacío, valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacío)";
    public static final String TEXTO_ANTIGUEDAD_MINIMA = "Antigüedad mínima (no vacío, valor positivo)";
    public static final String TEXTO_ACTIVO = "Activo";

    public entSepelioCobertura() {
        super();
        this.dmonto = 0.0;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.iantiguedadMinima = 0;
        this.bactivo = false;
    }

    public entSepelioCobertura(int iid, String sdescripcion, double dmonto, int iidMoneda, String smoneda, int iantiguedadMinima, boolean bactivo) {
        super(iid, sdescripcion);
        this.dmonto = dmonto;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iantiguedadMinima = iantiguedadMinima;
        this.bactivo = bactivo;
    }

    public entSepelioCobertura(int iid, String sdescripcion, double dmonto, int iidMoneda, String smoneda, int iantiguedadMinima, boolean bactivo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.dmonto = dmonto;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iantiguedadMinima = iantiguedadMinima;
        this.bactivo = bactivo;
    }

    public void setEntidad(int iid, String sdescripcion, double dmonto, int iidMoneda, String smoneda, int iantiguedadMinima, boolean bactivo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.dmonto = dmonto;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.iantiguedadMinima = iantiguedadMinima;
        this.bactivo = bactivo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entSepelioCobertura copiar(entSepelioCobertura destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getMonto(), this.getIdMoneda(), this.getMoneda(), this.getAntiguedadMinima(), this.getActivo(), this.getEstadoRegistro());
        return destino;
    }

    public entSepelioCobertura cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setAntiguedadMinima(rs.getInt("antiguedadminima")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setMonto(double dmonto) {
        this.dmonto = dmonto;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }

    public void setAntiguedadMinima(int iantiguedadMinima) {
        this.iantiguedadMinima = iantiguedadMinima;
    }

    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }

    public double getMonto() {
        return this.dmonto;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public int getAntiguedadMinima() {
        return this.iantiguedadMinima;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getMonto()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT coberturasepelio("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getMonto()+","+
            this.getIdMoneda()+","+
            this.getAntiguedadMinima()+","+
            this.getActivo()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        return lst;
    }
    
}
