/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entMovimiento {

    private int iid;
    private int iidSocio;
    private int iidFuncionario;
    private String scedula;
    private String snombreApellido;
    private int iidMoneda;
    private String smoneda;
    private int inumeroSolicitud;
    private java.util.Date dfechaSolicitud;
    private String shoraSolicitud;
    private double umontoSolicitud;
    private int iplazoSolicitud;
    private int iidPromotor;
    private String spromotor;
    private int iidDestino;
    private String sdestino;
    private int iidEntidad;
    private String sentidad;
    private int iidTipoCredito;
    private String stipoCredito;
    private int iidEstadoSolicitud;
    private String sestadoSolicitud;
    private double utasaRetencion;
    private String sobservacionSolicitud;
    private int iplazoMaximo;
    private boolean bcancelacion;
    private double umontoCancelacion;
    private double umontoSaldo;
    private double umontoImpuesto;
    private double utasaImpuesto;
    private java.util.Date dfechaIngreso;
    private int iidRubro;
    private String srubro;
    private String szona;
    private boolean bcancelado;
    private int iidOficial;
    private String soficial;
    private int iidAnalista;
    private String sanalista;

    private int inumeroActa;
    private java.util.Date dfechaActa;
    private java.util.Date dfechaAprobado;
    private String shoraAprobado;
    private double umontoAprobado;
    private int iplazoAprobado;
    private int iidAutorizador;
    private String sautorizador;
    private String sobservacionAprobado;
    private int iidRegional;
    private String sregional;
    private int iidRemision;
    private String sremision;
    private int iidGiraduria;
    private String sgiraduria;
    private int iidAsignacion;
    private String sasignacion;
    private int iidCambioRubro;

    private java.util.Date dfechaAnulado;
    private String sobservacionAnulado;
    
    private java.util.Date dfechaRechazado;
    private String sobservacionRechazado;
    
    private java.util.Date dfechaReconsiderado;
    private String sobservacionReconsiderado;
    
    private double utasaInteres;
    private double umontoInteres;
    private int inumeroBoleta;
    private java.util.Date dfechaGeneracion;
    private String sobservacionGeneracion;
    private java.util.Date dfechaPrimerVencimiento;
    private java.util.Date dfechaVencimiento;
    private int inumeroOperacion;
    private double uretencion;
    
    private int iidCuenta;
    private String scuenta;
    private java.util.ArrayList<String> validacion = new java.util.ArrayList<String>();
    private int iidLiquidador;
    private String sliquidador;
    private boolean bcompromiso;
    private short hestadoRegistro;
    private String smensaje;

    public static final int INICIO_PLAZO = 1;
    public static final int FIN_PLAZO = 999;
    public static final int INICIO_DIA_VENCIMIENTO = 0;
    public static final int FIN_DIA_VENCIMIENTO = 31;
    public static final int LONGITUD_OBSERVACION = 100;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ESTADO = "Estado de Solicitud (no editable)";
    public static final String TEXTO_NUMERO_SOLICITUD = "Número de Solicitud (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_CEDULA = "Cédula de Socio (no vacío)";
    public static final String TEXTO_FECHA_SOLICITUD = "Fecha de Solicitud (no vacía)";
    public static final String TEXTO_HORA_SOLICITUD = "Hora de Solicitud (no editable)";
    public static final String TEXTO_ENTIDAD = "Entidad (no vacía)";
    public static final String TEXTO_CIUDAD_DEPARTAMENTO = "Ciudad - Departamento (no editable)";
    public static final String TEXTO_PLAZO_MAXIMO = "Plazo máximo (no editable)";
    public static final String TEXTO_MONTO_SOLICITUD = "Monto Solicitado (valor positivo)";
    public static final String TEXTO_MONEDA = "Moneda (no vacía)";
    public static final String TEXTO_PLAZO_SOLICITUD = "Plazo Solicitado (no vacío, desde " + INICIO_PLAZO + " hasta " + FIN_PLAZO +")";
    public static final String TEXTO_PROMOTOR = "Promotor (no vacío)";
    public static final String TEXTO_DESTINO = "Destino (no vacío)";
    public static final String TEXTO_TIPO_CREDITO = "Tipo de Crédito (no vacío)";
    public static final String TEXTO_OBSERVACION_SOLICITUD = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_CANCELACION = "Cancela operaciones pendientes (puede reescribir las operaciones cargadas inicialmente)";
    public static final String TEXTO_MONTO_CANCELACION = "Monto a cancelar (no editable)";
    public static final String TEXTO_MONTO_SALDO = "Monto del saldo (no editable, valor positivo)";
    public static final String TEXTO_MONTO_IMPUESTO = "Monto del impuesto (no editable, valor positivo)";
    public static final String TEXTO_IMPUESTO = "Tasa de Impuesto (no editable, valor positivo)";

    public static final String TEXTO_NUMERO_ACTA = "Número de Acta (valor positivo)";
    public static final String TEXTO_FECHA_ACTA = "Fecha de Acta";
    public static final String TEXTO_FECHA_APROBADO = "Fecha de Aprobación (no editable)";
    public static final String TEXTO_HORA_APROBADO = "Hora de Aprobación (no editable)";
    public static final String TEXTO_MONTO_APROBADO = "Monto Aprobado (valor positivo)";
    public static final String TEXTO_PLAZO_APROBADO = "Plazo Aprobado (no vacío, desde " + INICIO_PLAZO + " hasta " + FIN_PLAZO +")";
    public static final String TEXTO_FUENTE_FINANCIERA = "Fuente Financiera (no vacío)";
    public static final String TEXTO_AUTORIZADOR = "Autorizador (no vacía)";
    public static final String TEXTO_OBSERVACION_APROBADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_REGIONAL = "Regional (no vacía)";
    public static final String TEXTO_REMISION = "Remisión (no vacío)";
    public static final String TEXTO_GIRADURIA = "Giraduría (no vacío)";
    public static final String TEXTO_RUBRO = "Rubro (no vacío)";
    public static final String TEXTO_ASIGNACION = "Asignación (no vacío)";
    public static final String TEXTO_OFICIAL = "Oficial de Crédito (no editable)";
    public static final String TEXTO_ANALISTA = "Analista de Crédito (no editable)";
    public static final String TEXTO_LIQUIDADOR = "Quien realiza la Liquidación del Crédito (no editable)";

    public static final String TEXTO_FECHA_ANULADO = "Fecha de Anulación (no editable)";
    public static final String TEXTO_OBSERVACION_ANULADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_RECHAZADO = "Fecha de Rechazo (no editable)";
    public static final String TEXTO_OBSERVACION_RECHAZADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_FECHA_RECONSIDERADO = "Fecha de Reconsideración (no editable)";
    public static final String TEXTO_OBSERVACION_RECONSIDERADO = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";

    public static final String TEXTO_FECHA_PRIMER_VENCIMIENTO = "Fecha del Primer Vencimiento (no editable)";
    public static final String TEXTO_FECHA_VENCIMIENTO = "Fecha de Vencimiento de la Operación (no editable)";
    public static final String TEXTO_NUMERO_BOLETA = "Número de Boleta (valor positivo, único)";
    public static final String TEXTO_MONTO_INTERES = "Monto de Interés (no editable)";
    //public static final String TEXTO_VIA_COBRO = "Vía Cobro (no vacío)";
    public static final String TEXTO_FECHA_GENERACION = "Fecha de Generación (no editable)";
    public static final String TEXTO_OBSERVACION_GENERACION = "Observación (no vacía, hasta " + LONGITUD_OBSERVACION + " caracteres)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (no editable)";
    public static final String TEXTO_TASA_RETENCION = "Tasa de Retención (no editable)";
    public static final String TEXTO_RETENCION = "Retención (no editable)";
    public static final String TEXTO_COMPROMISO = "Si es que existe compromiso de pago firmado.";
    
    public entMovimiento() {
        this.iid = 0;
        this.iidSocio = 0;
        this.iidFuncionario = 0;
        this.scedula = "";
        this.snombreApellido = "";
        this.iidMoneda = 0;
        this.smoneda = "";
        this.inumeroSolicitud = 0;
        this.dfechaSolicitud = null;
        this.shoraSolicitud = "";
        this.umontoSolicitud = 0.0;
        this.iplazoSolicitud = 0;
        this.iidDestino = 0;
        this.sdestino = "";
        this.iidEntidad = 0;
        this.sentidad = "";
        this.iidTipoCredito = 0;
        this.stipoCredito = "";
        this.inumeroBoleta = 0;
        this.inumeroActa = 0;
        this.dfechaActa = null;
        this.umontoAprobado = 0.0;
        this.iplazoAprobado = 0;
        this.utasaInteres = 0.0;
        this.umontoInteres = 0.0;
        this.dfechaAprobado = null;
        this.shoraAprobado = "";
        this.dfechaPrimerVencimiento = null;
        this.dfechaVencimiento = null;
        this.iidEstadoSolicitud = 0;
        this.sestadoSolicitud = "";
        this.iidAutorizador = 0;
        this.sautorizador = "";
        this.utasaRetencion = 0.0;
        this.iidPromotor = 0;
        this.spromotor = "";
        this.iidCuenta = 0;
        this.scuenta = "";
        this.sobservacionSolicitud = "";
        this.sobservacionAprobado = "";
        this.dfechaAnulado = null;
        this.sobservacionAnulado = "";
        this.dfechaRechazado = null;
        this.sobservacionRechazado = "";
        this.dfechaReconsiderado = null;
        this.sobservacionReconsiderado = "";
        this.dfechaGeneracion = null;
        this.sobservacionGeneracion = "";
        this.iplazoMaximo = 0;
        this.bcancelacion = false;
        this.umontoCancelacion = 0.0;
        this.umontoSaldo = 0.0;
        this.umontoImpuesto = 0.0;
        this.utasaImpuesto = 0.0;
        this.iidRegional = 0;
        this.sregional = "";
        this.iidRemision = 0;
        this.sremision = "";
        this.iidGiraduria = 0;
        this.sgiraduria = "";
        this.iidRubro = 0;
        this.srubro = "";
        this.iidAsignacion = 0;
        this.sasignacion = "";
        this.inumeroOperacion = 0;
        this.uretencion = 0.0;
        this.dfechaIngreso = null;
        this.szona = "";
        this.bcancelado = false;
        this.iidOficial = 0;
        this.soficial = "";
        this.iidAnalista = 0;
        this.sanalista = "";
        this.iidCambioRubro = 0;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.iidLiquidador = 0;
        this.sliquidador = "";
        this.bcompromiso = false;
        this.setValidacion();
    }

    public entMovimiento(int iid, int iidSocio, int iidFuncionario, String scedula, String snombreApellido, int iidMoneda, String smoneda, int inumeroSolicitud, java.util.Date dfechaSolicitud, String shoraSolicitud, double umontoSolicitud, int iplazoSolicitud, int iidDestino, String sdestino, int iidEntidad, String sentidad, int iidTipoCredito, String stipoCredito, int inumeroBoleta, int inumeroActa, java.util.Date dfechaActa, double umontoAprobado, int iplazoAprobado, double utasaInteres, double umontoInteres, java.util.Date dfechaAprobado, String shoraAprobado, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, int iidEstadoSolicitud, String sestadoSolicitud, int iidAutorizador, String sautorizador, double utasaRetencion, int iidPromotor, String spromotor, int iidCuenta, String scuenta, String sobservacionSolicitud, String sobservacionAprobado, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRechazado, String sobservacionRechazado, java.util.Date dfechaReconsiderado, String sobservacionReconsiderado, java.util.Date dfechaGeneracion, String sobservacionGeneracion, boolean bcancelacion, double umontoCancelacion, double umontoSaldo, double umontoImpuesto, double utasaImpuesto, int iidRegional, String sregional, int iidRemision, String sremision, int iidGiraduria, String sgiraduria, int iidRubro, String srubro, int iidAsignacion, String sasignacion, int inumeroOperacion, double uretencion, java.util.Date dfechaIngreso, String szona, boolean bcancelado, int iidOficial, String soficial, int iidAnalista, String sanalista, int iidCambioRubro, int iidLiquidador, String sliquidador, boolean bcompromiso, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.iidFuncionario = iidFuncionario;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaSolicitud = dfechaSolicitud;
        this.shoraSolicitud = shoraSolicitud;
        this.umontoSolicitud = umontoSolicitud;
        this.iplazoSolicitud = iplazoSolicitud;
        this.iidDestino = iidDestino;
        this.sdestino = sdestino;
        this.iidEntidad = iidEntidad;
        this.sentidad = sentidad;
        this.iidTipoCredito = iidTipoCredito;
        this.stipoCredito = stipoCredito;
        this.inumeroBoleta = inumeroBoleta;
        this.inumeroActa = inumeroActa;
        this.dfechaActa = dfechaActa;
        this.umontoAprobado = umontoAprobado;
        this.iplazoAprobado = iplazoAprobado;
        this.utasaInteres = utasaInteres;
        this.umontoInteres = umontoInteres;
        this.dfechaAprobado = dfechaAprobado;
        this.shoraAprobado = shoraAprobado;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.iidEstadoSolicitud = iidEstadoSolicitud;
        this.sestadoSolicitud = sestadoSolicitud;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.utasaRetencion = utasaRetencion;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sobservacionSolicitud = sobservacionSolicitud;
        this.sobservacionAprobado = sobservacionAprobado;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRechazado = dfechaRechazado;
        this.sobservacionRechazado = sobservacionRechazado;
        this.dfechaReconsiderado = dfechaReconsiderado;
        this.sobservacionReconsiderado = sobservacionReconsiderado;
        this.dfechaGeneracion = dfechaGeneracion;
        this.sobservacionGeneracion = sobservacionGeneracion;
        this.iplazoMaximo = 0;
        this.bcancelacion = bcancelacion;
        this.umontoCancelacion = umontoCancelacion;
        this.umontoSaldo = umontoSaldo;
        this.umontoImpuesto = umontoImpuesto;
        this.utasaImpuesto = utasaImpuesto;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidRemision = iidRemision;
        this.sremision = sremision;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.iidAsignacion = iidAsignacion;
        this.sasignacion = sasignacion;
        this.inumeroOperacion = inumeroOperacion;
        this.uretencion = uretencion;
        this.dfechaIngreso = dfechaIngreso;
        this.szona = szona;
        this.bcancelado = bcancelado;
        this.iidOficial = iidOficial;
        this.soficial = soficial;
        this.iidAnalista = iidAnalista;
        this.sanalista = sanalista;
        this.iidCambioRubro = iidCambioRubro;
        this.hestadoRegistro = hestadoRegistro;
        this.iidLiquidador = iidLiquidador;
        this.sliquidador = sliquidador;
        this.bcompromiso = bcompromiso;
        this.setValidacion();
    }

    public void setEntidad(int iid, int iidSocio, int iidFuncionario, String scedula, String snombreApellido, int iidMoneda, String smoneda, int inumeroSolicitud, java.util.Date dfechaSolicitud, String shoraSolicitud, double umontoSolicitud, int iplazoSolicitud, int iidDestino, String sdestino, int iidEntidad, String sentidad, int iidTipoCredito, String stipoCredito, int inumeroBoleta, int inumeroActa, java.util.Date dfechaActa, double umontoAprobado, int iplazoAprobado, double utasaInteres, double umontoInteres, java.util.Date dfechaAprobado, String shoraAprobado, java.util.Date dfechaPrimerVencimiento, java.util.Date dfechaVencimiento, int iidEstadoSolicitud, String sestadoSolicitud, int iidAutorizador, String sautorizador, double utasaRetencion, int iidPromotor, String spromotor, int iidCuenta, String scuenta, String sobservacionSolicitud, String sobservacionAprobado, java.util.Date dfechaAnulado, String sobservacionAnulado, java.util.Date dfechaRechazado, String sobservacionRechazado, java.util.Date dfechaReconsiderado, String sobservacionReconsiderado, java.util.Date dfechaGeneracion, String sobservacionGeneracion, boolean bcancelacion, double umontoCancelacion, double umontoSaldo, double umontoImpuesto, double utasaImpuesto, int iidRegional, String sregional, int iidRemision, String sremision, int iidGiraduria, String sgiraduria, int iidRubro, String srubro, int iidAsignacion, String sasignacion, int inumeroOperacion, double uretencion, java.util.Date dfechaIngreso, String szona, boolean bcancelado, int iidOficial, String soficial, int iidAnalista, String sanalista, int iidCambioRubro, int iidLiquidador, String sliquidador, boolean bcompromiso, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.iidFuncionario = iidFuncionario;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaSolicitud = dfechaSolicitud;
        this.shoraSolicitud = shoraSolicitud;
        this.umontoSolicitud = umontoSolicitud;
        this.iplazoSolicitud = iplazoSolicitud;
        this.iidDestino = iidDestino;
        this.sdestino = sdestino;
        this.iidEntidad = iidEntidad;
        this.sentidad = sentidad;
        this.iidTipoCredito = iidTipoCredito;
        this.stipoCredito = stipoCredito;
        this.inumeroBoleta = inumeroBoleta;
        this.inumeroActa = inumeroActa;
        this.dfechaActa = dfechaActa;
        this.umontoAprobado = umontoAprobado;
        this.iplazoAprobado = iplazoAprobado;
        this.utasaInteres = utasaInteres;
        this.umontoInteres = umontoInteres;
        this.dfechaAprobado = dfechaAprobado;
        this.shoraAprobado = shoraAprobado;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.dfechaVencimiento = dfechaVencimiento;
        this.iidEstadoSolicitud = iidEstadoSolicitud;
        this.sestadoSolicitud = sestadoSolicitud;
        this.iidAutorizador = iidAutorizador;
        this.sautorizador = sautorizador;
        this.utasaRetencion = utasaRetencion;
        this.iidPromotor = iidPromotor;
        this.spromotor = spromotor;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sobservacionSolicitud = sobservacionSolicitud;
        this.sobservacionAprobado = sobservacionAprobado;
        this.dfechaAnulado = dfechaAnulado;
        this.sobservacionAnulado = sobservacionAnulado;
        this.dfechaRechazado = dfechaRechazado;
        this.sobservacionRechazado = sobservacionRechazado;
        this.dfechaReconsiderado = dfechaReconsiderado;
        this.sobservacionReconsiderado = sobservacionReconsiderado;
        this.dfechaGeneracion = dfechaGeneracion;
        this.sobservacionGeneracion = sobservacionGeneracion;
        this.iplazoMaximo = 0;
        this.bcancelacion = bcancelacion;
        this.umontoCancelacion = umontoCancelacion;
        this.umontoSaldo = umontoSaldo;
        this.umontoImpuesto = umontoImpuesto;
        this.utasaImpuesto = utasaImpuesto;
        this.iidRegional = iidRegional;
        this.sregional = sregional;
        this.iidRemision = iidRemision;
        this.sremision = sremision;
        this.iidGiraduria = iidGiraduria;
        this.sgiraduria = sgiraduria;
        this.iidRubro = iidRubro;
        this.srubro = srubro;
        this.iidAsignacion = iidAsignacion;
        this.sasignacion = sasignacion;
        this.inumeroOperacion = inumeroOperacion;
        this.uretencion = uretencion;
        this.dfechaIngreso = dfechaIngreso;
        this.szona = szona;
        this.bcancelado = bcancelado;
        this.iidOficial = iidOficial;
        this.soficial = soficial;
        this.iidAnalista = iidAnalista;
        this.sanalista = sanalista;
        this.iidCambioRubro = iidCambioRubro;
        this.iidLiquidador = iidLiquidador;
        this.sliquidador = sliquidador;
        this.bcompromiso = bcompromiso;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entMovimiento copiar(entMovimiento destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getIdFuncionario(), this.getCedula(), this.getNombreApellido(), this.getIdMoneda(), this.getMoneda(), this.getNumeroSolicitud(), this.getFechaSolicitud(), this.getHoraSolicitud(), this.getMontoSolicitud(), this.getPlazoSolicitud(), this.getIdDestino(), this.getDestino(), this.getIdEntidad(), this.getEntidad(), this.getIdTipoCredito(), this.getTipoCredito(), this.getNumeroBoleta(), this.getNumeroActa(), this.getFechaActa(), this.getMontoAprobado(), this.getPlazoAprobado(), this.getTasaInteres(), this.getMontoInteres(), this.getFechaAprobado(), this.getHoraAprobado(), this.getFechaPrimerVencimiento(), this.getFechaVencimiento(), this.getIdEstadoSolicitud(), this.getEstadoSolicitud(), this.getIdAutorizador(), this.getAutorizador(), this.getTasaRetencion(), this.getIdPromotor(), this.getPromotor(), this.getIdCuenta(), this.getCuenta(), this.getObservacionSolicitud(), this.getObservacionAprobado(), this.getFechaAnulado(), this.getObservacionAnulado(), this.getFechaRechazado(), this.getObservacionRechazado(), this.getFechaReconsiderado(), this.getObservacionReconsiderado(), this.getFechaGeneracion(), this.getObservacionGeneracion(), this.getCancelacion(), this.getMontoCancelacion(), this.getMontoSaldo(), this.getMontoImpuesto(), this.getTasaImpuesto(), this.getIdRegional(), this.getRegional(), this.getIdRemision(), this.getRemision(), this.getIdGiraduria(), this.getGiraduria(), this.getIdRubro(), this.getRubro(), this.getIdAsignacion(), this.getAsignacion(), this.getNumeroOperacion(), this.getRetencion(), this.getFechaIngreso(), this.getZona(), this.getCancelado(), this.getIdOficial(), this.getOficial(), this.getIdAnalista(), this.getAnalista(), this.getIdCambioRubro(), this.getIdLiquidador(), this.getLiquidador(), this.getCompromiso(), this.getEstadoRegistro());
        return destino;
    }
    
    public entMovimiento cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setIdFuncionario(rs.getInt("idfuncionario")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombreApellido(rs.getString("nombreapellido")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setFechaSolicitud(rs.getDate("fechasolicitud")); }
        catch(Exception e) {}
        try { this.setHoraSolicitud(rs.getString("horasolicitud")); }
        catch(Exception e) {}
        try { this.setMontoSolicitud(rs.getDouble("montosolicitud")); }
        catch(Exception e) {}
        try { this.setPlazoSolicitud(rs.getInt("plazosolicitud")); }
        catch(Exception e) {}
        try { this.setIdDestino(rs.getInt("iddestino")); }
        catch(Exception e) {}
        try { this.setDestino(rs.getString("destino")); }
        catch(Exception e) {}
        try { this.setObservacionSolicitud(rs.getString("observacionsolicitud")); }
        catch(Exception e) {}
        try { this.setIdEntidad(rs.getInt("identidad")); }
        catch(Exception e) {}
        try { this.setEntidad(rs.getString("entidad")); }
        catch(Exception e) {}
        try { this.setIdTipoCredito(rs.getInt("idtipocredito")); }
        catch(Exception e) {}
        try { this.setTipoCredito(rs.getString("tipocredito")); }
        catch(Exception e) {}
        try { this.setNumeroBoleta(rs.getInt("numeroboleta")); }
        catch(Exception e) {}
        try { this.setNumeroActa(rs.getInt("numeroacta")); }
        catch(Exception e) {}
        try { this.setFechaActa(rs.getDate("fechaacta")); }
        catch(Exception e) {}
        try { this.setMontoAprobado(rs.getInt("montoaprobado")); }
        catch(Exception e) {}
        try { this.setPlazoAprobado(rs.getInt("plazoaprobado")); }
        catch(Exception e) {}
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(Exception e) {}
        try { this.setMontoInteres(rs.getDouble("montointeres")); }
        catch(Exception e) {}
        try { this.setFechaAprobado(rs.getDate("fechaaprobado")); }
        catch(Exception e) {}
        try { this.setHoraAprobado(rs.getString("horaaprobado")); }
        catch(Exception e) {}
        try { this.setObservacionAprobado(rs.getString("observacionaprobado")); }
        catch(Exception e) {}
        try { this.setFechaPrimerVencimiento(rs.getDate("fechaprimervencimiento")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setIdEstadoSolicitud(rs.getInt("idestado")); }
        catch(Exception e) {}
        try { this.setEstadoSolicitud(rs.getString("estado")); }
        catch(Exception e) {}
        try { this.setIdAutorizador(rs.getInt("idautorizador")); }
        catch(Exception e) {}
        try { this.setAutorizador(rs.getString("autorizador")); }
        catch(Exception e) {}
        try { this.setTasaRetencion(rs.getDouble("tasaretencion")); }
        catch(Exception e) {}
        try { this.setIdPromotor(rs.getInt("idpromotor")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setFechaAnulado(rs.getDate("fechaanulado")); }
        catch(Exception e) {}
        try { this.setObservacionAnulado(rs.getString("observacionanulado")); }
        catch(Exception e) {}
        try { this.setFechaRechazado(rs.getDate("fecharechazado")); }
        catch(Exception e) {}
        try { this.setObservacionRechazado(rs.getString("observacionrechazado")); }
        catch(Exception e) {}
        try { this.setFechaReconsiderado(rs.getDate("fechareconsiderado")); }
        catch(Exception e) {}
        try { this.setObservacionReconsiderado(rs.getString("observacionreconsiderado")); }
        catch(Exception e) {}
        try { this.setFechaGeneracion(rs.getDate("fechageneracion")); }
        catch(Exception e) {}
        try { this.setObservacionGeneracion(rs.getString("observaciongeneracion")); }
        catch(Exception e) {}
        try { this.setCancelacion(rs.getBoolean("cancelacion")); }
        catch(Exception e) {}
        try { this.setMontoCancelacion(rs.getDouble("montocancelacion")); }
        catch(Exception e) {}
        try { this.setMontoSaldo(rs.getDouble("montosaldo")); }
        catch(Exception e) {}
        try { this.setMontoImpuesto(rs.getDouble("montoimpuesto")); }
        catch(Exception e) {}
        try { this.setTasaImpuesto(rs.getDouble("tasaimpuesto")); }
        catch(Exception e) {}
        try { this.setIdRegional(rs.getInt("idregional")); }
        catch(Exception e) {}
        try { this.setRegional(rs.getString("regional")); }
        catch(Exception e) {}
        try { this.setIdRemision(rs.getInt("idremision")); }
        catch(Exception e) {}
        try { this.setRemision(rs.getString("remision")); }
        catch(Exception e) {}
        try { this.setIdGiraduria(rs.getInt("idgiraduria")); }
        catch(Exception e) {}
        try { this.setGiraduria(rs.getString("giraduria")); }
        catch(Exception e) {}
        try { this.setIdRubro(rs.getInt("idrubro")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        try { this.setIdAsignacion(rs.getInt("idasignacion")); }
        catch(Exception e) {}
        try { this.setAsignacion(rs.getString("asignacion")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setRetencion(rs.getDouble("retencion")); }
        catch(Exception e) {}
        try { this.setFechaIngreso(rs.getDate("fechaingreso")); }
        catch(Exception e) {}
        try { this.setZona(rs.getString("zona")); }
        catch(Exception e) {}
        try { this.setCancelado(rs.getBoolean("cancelado")); }
        catch(Exception e) {}
        try { this.setIdOficial(rs.getInt("idoficial")); }
        catch(Exception e) {}
        try { this.setOficial(rs.getString("oficial")); }
        catch(Exception e) {}
        try { this.setIdAnalista(rs.getInt("idanalista")); }
        catch(Exception e) {}
        try { this.setAnalista(rs.getString("analista")); }
        catch(Exception e) {}
        try { this.setIdLiquidador(rs.getInt("idliquidador")); }
        catch(Exception e) {}
        try { this.setLiquidador(rs.getString("liquidador")); }
        catch(Exception e) {}
        try { this.setCompromiso(rs.getBoolean("compromisopago")); }
        catch(Exception e) {}
        try { this.setIdCambioRubro(rs.getInt("idcambiorubro")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setIdFuncionario(int iidFuncionario) {
        this.iidFuncionario = iidFuncionario;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombreApellido(String snombreApellido) {
        this.snombreApellido = snombreApellido;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setFechaSolicitud(java.util.Date dfechaSolicitud) {
        this.dfechaSolicitud = dfechaSolicitud;
    }

    public void setHoraSolicitud(String shoraSolicitud) {
        this.shoraSolicitud = shoraSolicitud;
    }

    public void setMontoSolicitud(double umontoSolicitud) {
        this.umontoSolicitud = umontoSolicitud;
    }

    public void setPlazoSolicitud(int iplazoSolicitud) {
        this.iplazoSolicitud = iplazoSolicitud;
    }

    public void setIdDestino(int iidDestino) {
        this.iidDestino = iidDestino;
    }

    public void setDestino(String sdestino) {
        this.sdestino = sdestino;
    }

    public void setIdEntidad(int iidEntidad) {
        this.iidEntidad = iidEntidad;
    }

    public void setEntidad(String sentidad) {
        this.sentidad = sentidad;
    }

    public void setIdTipoCredito(int iidTipoCredito) {
        this.iidTipoCredito = iidTipoCredito;
    }

    public void setTipoCredito(String stipoCredito) {
        this.stipoCredito = stipoCredito;
    }

    public void setNumeroBoleta(int inumeroBoleta) {
        this.inumeroBoleta = inumeroBoleta;
    }

    public void setNumeroActa(int inumeroActa) {
        this.inumeroActa = inumeroActa;
    }

    public void setFechaActa(java.util.Date dfechaActa) {
        this.dfechaActa = dfechaActa;
    }

    public void setMontoAprobado(double umontoAprobado) {
        this.umontoAprobado = umontoAprobado;
    }

    public void setPlazoAprobado(int iplazoAprobado) {
        this.iplazoAprobado = iplazoAprobado;
    }

    public void setTasaInteres(double utasaInteres) {
        this.utasaInteres = utasaInteres;
    }

    public void setMontoInteres(double umontoInteres) {
        this.umontoInteres = umontoInteres;
    }

    public void setFechaAprobado(java.util.Date dfechaAprobado) {
        this.dfechaAprobado = dfechaAprobado;
    }

    public void setHoraAprobado(String shoraAprobado) {
        this.shoraAprobado = shoraAprobado;
    }

    public void setFechaPrimerVencimiento(java.util.Date dfechaPrimerVencimiento) {
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setIdEstadoSolicitud(int iidEstadoSolicitud) {
        this.iidEstadoSolicitud = iidEstadoSolicitud;
    }

    public void setEstadoSolicitud(String sestadoSolicitud) {
        this.sestadoSolicitud = sestadoSolicitud;
    }

    public void setIdAutorizador(int iidAutorizador) {
        this.iidAutorizador = iidAutorizador;
    }

    public void setAutorizador(String sautorizador) {
        this.sautorizador = sautorizador;
    }

    public void setTasaRetencion(double utasaRetencion) {
        this.utasaRetencion = utasaRetencion;
    }

    public void setIdPromotor(int iidPromotor) {
        this.iidPromotor = iidPromotor;
    }

    public void setPromotor(String spromotor) {
        this.spromotor = spromotor;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setObservacionSolicitud(String sobservacionSolicitud) {
        this.sobservacionSolicitud = sobservacionSolicitud;
    }

    public void setObservacionAprobado(String sobservacionAprobado) {
        this.sobservacionAprobado = sobservacionAprobado;
    }

    public void setFechaAnulado(java.util.Date dfechaAnulado) {
        this.dfechaAnulado = dfechaAnulado;
    }

    public void setObservacionAnulado(String sobservacionAnulado) {
        this.sobservacionAnulado = sobservacionAnulado;
    }

    public void setFechaRechazado(java.util.Date dfechaRechazado) {
        this.dfechaRechazado = dfechaRechazado;
    }

    public void setObservacionRechazado(String sobservacionRechazado) {
        this.sobservacionRechazado = sobservacionRechazado;
    }

    public void setFechaReconsiderado(java.util.Date dfechaReconsiderado) {
        this.dfechaReconsiderado = dfechaReconsiderado;
    }

    public void setObservacionReconsiderado(String sobservacionReconsiderado) {
        this.sobservacionReconsiderado = sobservacionReconsiderado;
    }

    public void setFechaGeneracion(java.util.Date dfechaGeneracion) {
        this.dfechaGeneracion = dfechaGeneracion;
    }

    public void setObservacionGeneracion(String sobservacionGeneracion) {
        this.sobservacionGeneracion = sobservacionGeneracion;
    }

    public void setPlazoMaximo(int iplazoMaximo) {
        this.iplazoMaximo = iplazoMaximo;
    }
    
    public void setCancelacion(boolean bcancelacion) {
        this.bcancelacion = bcancelacion;
    }
    
    public void setMontoCancelacion(double umontoCancelacion) {
        this.umontoCancelacion = umontoCancelacion;
    }
    
    public void setMontoSaldo(double umontoSaldo) {
        this.umontoSaldo = umontoSaldo;
    }
    
    public void setMontoImpuesto(double umontoImpuesto) {
        this.umontoImpuesto = umontoImpuesto;
    }
    
    public void setTasaImpuesto(double utasaImpuesto) {
        this.utasaImpuesto = utasaImpuesto;
    }
    
    public void setIdRegional(int iidRegional) {
        this.iidRegional = iidRegional;
    }
    
    public void setRegional(String sregional) {
        this.sregional = sregional;
    }
    
    public void setIdRemision(int iidRemision) {
        this.iidRemision = iidRemision;
    }
    
    public void setRemision(String sremision) {
        this.sremision = sremision;
    }
    
    public void setIdGiraduria(int iidGiraduria) {
        this.iidGiraduria = iidGiraduria;
    }
    
    public void setGiraduria(String sgiraduria) {
        this.sgiraduria = sgiraduria;
    }
    
    public void setIdRubro(int iidRubro) {
        this.iidRubro = iidRubro;
    }
    
    public void setRubro(String srubro) {
        this.srubro = srubro;
    }
    
    public void setIdAsignacion(int iidAsignacion) {
        this.iidAsignacion = iidAsignacion;
    }
    
    public void setAsignacion(String sasignacion) {
        this.sasignacion = sasignacion;
    }
    
    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }
    
    public void setRetencion(double uretencion) {
        this.uretencion = uretencion;
    }
    
    public void setFechaIngreso(java.util.Date dfechaIngreso) {
        this.dfechaIngreso = dfechaIngreso;
    }
    
    public void setZona(String szona) {
        this.szona = szona;
    }
    
    public void setCancelado(boolean bcancelado) {
        this.bcancelado = bcancelado;
    }
    
    public void setIdOficial(int iidOficial) {
        this.iidOficial = iidOficial;
    }
    
    public void setOficial(String soficial) {
        this.soficial = soficial;
    }
    
    public void setIdAnalista(int iidAnalista) {
        this.iidAnalista = iidAnalista;
    }
    
    public void setAnalista(String sanalista) {
        this.sanalista = sanalista;
    }
      
    public void setIdLiquidador(int iidLiquidador) {
        this.iidLiquidador = iidLiquidador;
    }

    public void setLiquidador(String sliquidador) {
        this.sliquidador = sliquidador;
    }

    public void setCompromiso(boolean bcompromiso) {
        this.bcompromiso = bcompromiso;
    }

    public void setIdCambioRubro(int iidCambioRubro) {
        this.iidCambioRubro = iidCambioRubro;
    }
    
    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public int getIdFuncionario() {
        return this.iidFuncionario;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombreApellido() {
        return this.snombreApellido;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public java.util.Date getFechaSolicitud() {
        return this.dfechaSolicitud;
    }

    public String getHoraSolicitud() {
        return this.shoraSolicitud;
    }

    public double getMontoSolicitud() {
        return this.umontoSolicitud;
    }

    public int getPlazoSolicitud() {
        return this.iplazoSolicitud;
    }

    public int getIdDestino() {
        return this.iidDestino;
    }

    public String getDestino() {
        return this.sdestino;
    }

    public int getIdEntidad() {
        return this.iidEntidad;
    }

    public String getEntidad() {
        return this.sentidad;
    }

    public int getIdTipoCredito() {
        return this.iidTipoCredito;
    }

    public String getTipoCredito() {
        return this.stipoCredito;
    }

    public int getNumeroBoleta() {
        return this.inumeroBoleta;
    }

    public int getNumeroActa() {
        return this.inumeroActa;
    }

    public java.util.Date getFechaActa() {
        return this.dfechaActa;
    }

    public double getMontoAprobado() {
        return this.umontoAprobado;
    }

    public int getPlazoAprobado() {
        return this.iplazoAprobado;
    }

    public double getTasaInteres() {
        return this.utasaInteres;
    }

    public double getMontoInteres() {
        return this.umontoInteres;
    }

    public java.util.Date getFechaAprobado() {
        return this.dfechaAprobado;
    }

    public String getHoraAprobado() {
        return this.shoraAprobado;
    }

    public java.util.Date getFechaPrimerVencimiento() {
        return this.dfechaPrimerVencimiento;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public int getIdEstadoSolicitud() {
        return this.iidEstadoSolicitud;
    }

    public String getEstadoSolicitud() {
        return this.sestadoSolicitud;
    }

    public int getIdAutorizador() {
        return this.iidAutorizador;
    }

    public String getAutorizador() {
        return this.sautorizador;
    }

    public double getTasaRetencion() {
        return this.utasaRetencion;
    }

    public int getIdPromotor() {
        return this.iidPromotor;
    }

    public String getPromotor() {
        return this.spromotor;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getObservacionSolicitud() {
        return this.sobservacionSolicitud;
    }

    public String getObservacionAprobado() {
        return this.sobservacionAprobado;
    }

    public java.util.Date getFechaAnulado() {
        return this.dfechaAnulado;
    }

    public String getObservacionAnulado() {
        return this.sobservacionAnulado;
    }

    public java.util.Date getFechaRechazado() {
        return this.dfechaRechazado;
    }

    public String getObservacionRechazado() {
        return this.sobservacionRechazado;
    }

    public java.util.Date getFechaReconsiderado() {
        return this.dfechaReconsiderado;
    }

    public String getObservacionReconsiderado() {
        return this.sobservacionReconsiderado;
    }

    public java.util.Date getFechaGeneracion() {
        return this.dfechaGeneracion;
    }

    public String getObservacionGeneracion() {
        return this.sobservacionGeneracion;
    }

    public int getPlazoMaximo() {
        return this.iplazoMaximo;
    }

    public boolean getCancelacion() {
        return this.bcancelacion;
    }

    public double getMontoCancelacion() {
        return this.umontoCancelacion;
    }

    public double getMontoSaldo() {
        return this.umontoSaldo;
    }

    public double getMontoImpuesto() {
        return this.umontoImpuesto;
    }

    public double getTasaImpuesto() {
        return this.utasaImpuesto;
    }

    public int getIdRegional() {
        return this.iidRegional;
    }

    public String getRegional() {
        return this.sregional;
    }

    public int getIdRemision() {
        return this.iidRemision;
    }

    public String getRemision() {
        return this.sremision;
    }

    public int getIdGiraduria() {
        return this.iidGiraduria;
    }

    public String getGiraduria() {
        return this.sgiraduria;
    }

    public int getIdRubro() {
        return this.iidRubro;
    }

    public String getRubro() {
        return this.srubro;
    }

    public int getIdAsignacion() {
        return this.iidAsignacion;
    }

    public String getAsignacion() {
        return this.sasignacion;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public double getRetencion() {
        return this.uretencion;
    }

    public java.util.Date getFechaIngreso() {
        return this.dfechaIngreso;
    }

    public String getZona() {
        return this.szona;
    }

    public boolean getCancelado() {
        return this.bcancelado;
    }

    public int getIdOficial() {
        return this.iidOficial;
    }

    public String getOficial() {
        return this.soficial;
    }

    public int getIdAnalista() {
        return this.iidAnalista;
    }

    public String getAnalista() {
        return this.sanalista;
    }

    public int getIdLiquidador() {
        return this.iidLiquidador;
    }

    public String getLiquidador() {
        return this.sliquidador;
    }

    public boolean getCompromiso() {
        return this.bcompromiso;
    }

    public int getIdCambioRubro() {
        return this.iidCambioRubro;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoSolicitarOrdenCredito(boolean primeraValidacion, String sestado) {
        boolean bvalido = true;
        this.smensaje = "";
        /*if (this.getIdSocio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }*/
        if (this.getCedula().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CEDULA;
            bvalido = false;
        }
        if (this.getFechaSolicitud()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_SOLICITUD;
            bvalido = false;
        }
        if (this.getIdEntidad()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ENTIDAD;
            bvalido = false;
        }
        if (this.getMontoSolicitud()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_SOLICITUD;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getPlazoSolicitud()<INICIO_PLAZO || this.getPlazoSolicitud()>FIN_PLAZO) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO_SOLICITUD;
            bvalido = false;
        }
        if (this.getIdEntidad()>0 && !primeraValidacion) { // para Orden de Credito
            if (this.getPlazoSolicitud()>this.getPlazoMaximo()) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Plazo solicitado ("+this.getPlazoSolicitud()+") sobrepasa el Plazo máximo establecido ("+this.getPlazoMaximo()+")";
                bvalido = false;
            }
        }
        if (this.getIdDestino()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESTINO;
            bvalido = false;
        }
        if (this.getIdPromotor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PROMOTOR;
            bvalido = false;
        }
        if (!sestado.isEmpty()) sestado = sestado.substring(0,3);
        if (!esValidoEstado(sestado+"-"+this.getEstadoSolicitud().substring(0,3))) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La solicitud se encuentra en otro estado";
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoSolicitarPrestamo(String sestado) {
        boolean bvalido = true;
        this.smensaje = "";
        /*if (this.getIdSocio()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }*/
        if (this.getCedula().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CEDULA;
            bvalido = false;
        }        
        if (this.getFechaSolicitud()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_SOLICITUD;
            bvalido = false;
        }
        if (this.getIdTipoCredito()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_CREDITO;
            bvalido = false;
        }
        if (this.getMontoSolicitud()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_SOLICITUD;
            bvalido = false;
        }
        if (this.getIdMoneda()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONEDA;
            bvalido = false;
        }
        if (this.getPlazoSolicitud()<INICIO_PLAZO || this.getPlazoSolicitud()>FIN_PLAZO) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO_SOLICITUD;
            bvalido = false;
        }
        if (this.getIdDestino()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESTINO;
            bvalido = false;
        }
        if (this.getIdPromotor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PROMOTOR;
            bvalido = false;
        }
        if (this.getIdRemision()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REMISION;
            bvalido = false;
        }
        if (this.getMontoSolicitud()<this.getMontoCancelacion()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "El monto solicitado debe ser mayor al total a cancelar";
            bvalido = false;
        }
        if (!sestado.isEmpty()) sestado = sestado.substring(0,3);
        if (!esValidoEstado(sestado+"-"+this.getEstadoSolicitud().substring(0,3))) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La solicitud se encuentra en otro estado";
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAprobarOrdenCredito(boolean bfuenteFinanciera, String sestado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getMontoAprobado()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_APROBADO;
            bvalido = false;
        }
        if (this.getMontoAprobado()>this.getMontoSolicitud()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Monto aprobado ("+this.getMontoAprobado()+") no puede exceder al Monto solicitado ("+this.getMontoSolicitud()+")";
            bvalido = false;
        }
        if (this.getPlazoAprobado()<INICIO_PLAZO || this.getPlazoAprobado()>FIN_PLAZO) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO_APROBADO;
            bvalido = false;
        }
        if (this.getPlazoAprobado()>this.getPlazoSolicitud()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Plazo aprobado ("+this.getPlazoAprobado()+") no puede exceder al Plazo solicitado ("+this.getPlazoSolicitud()+")";
            bvalido = false;
        }
        if (this.getIdAutorizador()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_AUTORIZADOR;
            bvalido = false;
        }
        if (this.getObservacionAprobado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_APROBADO;
            bvalido = false;
        }
        if (!sestado.isEmpty()) sestado = sestado.substring(0,3);
        if (!esValidoEstado(sestado+"-"+this.getEstadoSolicitud().substring(0,3))) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La solicitud se encuentra en otro estado";
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoAprobarPrestamo(int iidPendiente, String sestado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getMontoAprobado()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO_APROBADO;
            bvalido = false;
        }
        if (this.getMontoAprobado()>this.getMontoSolicitud()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Monto aprobado ("+this.getMontoAprobado()+") no puede exceder al Monto solicitado ("+this.getMontoSolicitud()+")";
            bvalido = false;
        }
        if (this.getPlazoAprobado()<INICIO_PLAZO || this.getPlazoAprobado()>FIN_PLAZO) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO_APROBADO;
            bvalido = false;
        }
        if (this.getPlazoAprobado()>this.getPlazoSolicitud()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Plazo aprobado ("+this.getPlazoAprobado()+") no puede exceder al Plazo solicitado ("+this.getPlazoSolicitud()+")";
            bvalido = false;
        }
        if (this.getIdEntidad()<=0) { // fuente financiera vacía
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUENTE_FINANCIERA;
            bvalido = false;
        }
        if (this.getIdAutorizador()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_AUTORIZADOR;
            bvalido = false;
        }
        if (this.getObservacionAprobado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_APROBADO;
            bvalido = false;
        }
        if (this.getMontoAprobado()<this.getMontoCancelacion()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "El monto aprobado debe ser mayor al total a cancelar";
            bvalido = false;
        }
        if (!sestado.isEmpty()) sestado = sestado.substring(0,3);
        if (!esValidoEstado(sestado+"-"+this.getEstadoSolicitud().substring(0,3))) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La solicitud se encuentra en otro estado";
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoAnular(String sestado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionAnulado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ANULADO;
            bvalido = false;
        }
        if (!sestado.isEmpty()) sestado = sestado.substring(0,3);
        if (!esValidoEstado(sestado+"-"+this.getEstadoSolicitud().substring(0,3))) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La solicitud se encuentra en otro estado";
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoRechazar(String sestado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getObservacionRechazado().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_RECHAZADO;
            bvalido = false;
        }
        if (!sestado.isEmpty()) sestado = sestado.substring(0,3);
        if (!esValidoEstado(sestado+"-"+this.getEstadoSolicitud().substring(0,3))) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La solicitud se encuentra en otro estado";
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoGenerar(String sestado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaGeneracion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_GENERACION;
            bvalido = false;
        }
        if (this.getFechaPrimerVencimiento()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_PRIMER_VENCIMIENTO;
            bvalido = false;
        }
        if (this.getFechaVencimiento()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_VENCIMIENTO;
            bvalido = false;
        }
        if (this.getIdRegional()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGIONAL;
            bvalido = false;
        }
        if (this.getIdGiraduria()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_GIRADURIA;
            bvalido = false;
        }
        if (this.getIdRubro()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_RUBRO;
            bvalido = false;
        }
        if (this.getIdAsignacion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_ASIGNACION;
            bvalido = false;
        }
        if (!sestado.isEmpty()) sestado = sestado.substring(0,3);
        if (!esValidoEstado(sestado+"-"+this.getEstadoSolicitud().substring(0,3))) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "La solicitud se encuentra en otro estado";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT movimiento("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdMoneda()+","+
            this.getNumeroSolicitud()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaSolicitud())+","+
            this.getMontoSolicitud()+","+
            this.getPlazoSolicitud()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionSolicitud())+","+
            this.getIdEntidad()+","+
            this.getIdTipoCredito()+","+
            this.getNumeroBoleta()+","+
            this.getNumeroActa()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaActa())+","+
            this.getMontoAprobado()+","+
            this.getPlazoAprobado()+","+
            this.getTasaInteres()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAprobado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAprobado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaPrimerVencimiento())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaVencimiento())+","+
            this.getIdEstadoSolicitud()+","+
            this.getIdAutorizador()+","+
            this.getTasaRetencion()+","+
            this.getIdPromotor()+","+
            this.getIdCuenta()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAnulado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAnulado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaRechazado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionRechazado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaReconsiderado())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionReconsiderado())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaGeneracion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionGeneracion())+","+
            this.getNumeroOperacion()+","+
            this.getIdRegional()+","+
            this.getMontoInteres()+","+
            this.getRetencion()+","+
            this.getCancelacion()+","+
            this.getMontoCancelacion()+","+
            this.getMontoSaldo()+","+
            this.getMontoImpuesto()+","+
            this.getTasaImpuesto()+","+
            this.getIdDestino()+","+
            this.getCancelado()+","+
            this.getIdOficial()+","+
            this.getIdAnalista()+","+
            this.getIdRemision()+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraSolicitud())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraAprobado())+","+
            this.getIdCambioRubro()+","+
            this.getIdLiquidador()+","+
            this.getCompromiso()+","+
            this.getIdFuncionario()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha Solicitud", "fechasolicitud", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(2, "Fecha Aprobación", "fechaaprobado", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Número Solicitud", "numerosolicitud", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(4, "Número Operación", "numerooperacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(5, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Nombre y Apellido", "nombreapellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Estado", "estado", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Oficial de Créditos", "oficial", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Analista de Créditos", "analista", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Liquidador ", "liquidador", generico.entLista.tipo_texto));
        return lst;
    }
    
    public void setValidacion() {
        this.validacion.add("-PEN");
        this.validacion.add("PEN-PEN");
        this.validacion.add("PEN-APR");
        this.validacion.add("PEN-ANU");
        this.validacion.add("PEN-REC");
        this.validacion.add("APR-ANU");
        this.validacion.add("APR-GEN");
        this.validacion.add("GEN-ANU");
    }

    private boolean esValidoEstado(String sestado) {
        boolean bvalido = false;
        for (int i=0; i<this.validacion.size(); i++) {
            if (this.validacion.get(i).contains(sestado)) bvalido=true;
        }
        return bvalido;
    }

}
