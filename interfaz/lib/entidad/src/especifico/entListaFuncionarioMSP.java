/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entListaFuncionarioMSP {
    
    protected int iid;
    protected String scedula;
    protected String sruc;
    protected String snombreApellido;
    protected String sgiraduria;
    protected String srubro;
    protected boolean bacepta;
    
    public static final int LONGITUD_DESCRIPCION = 50;

    public static final String TEXTO_DESCRIPCION = "Texto a buscar";

    public static final String TEXTO_CEDULA = "Cédula";
    public static final String TEXTO_RUC = "RUC";
    public static final String TEXTO_NOMBRE_APELLIDO = "Nombre y Apellido";
    public static final String TEXTO_ACEPTA = "Acepta";

    public entListaFuncionarioMSP() {
        this.iid = 0;
        this.scedula = "";
        this.sruc = "";
        this.snombreApellido = "";
        this.sgiraduria = "";
        this.srubro = "";
        this.bacepta = false;
    }

    public entListaFuncionarioMSP(int iid, String scedula, String sruc, String snombreApellido, String sgiraduria, String srubro, boolean bacepta) {
        this.iid = iid;
        this.scedula = scedula;
        this.sruc = sruc;
        this.snombreApellido = snombreApellido;
        this.sgiraduria = sgiraduria;
        this.srubro = srubro;
        this.bacepta = bacepta;
    }

    public void setEntidad(int iid, String scedula, String sruc, String snombreApellido, String sgiraduria, String srubro, boolean bacepta) {
        this.iid = iid;
        this.scedula = scedula;
        this.sruc = sruc;
        this.snombreApellido = snombreApellido;
        this.sgiraduria = sgiraduria;
        this.srubro = srubro;
        this.bacepta = bacepta;
    }

    public entListaFuncionarioMSP copiar(entListaFuncionarioMSP destino) {
        destino.setEntidad(this.getId(), this.getCedula(), this.getRuc(), this.getNombreApellido(), this.getGiraduria(), this.getRubro(), this.getAcepta());
        return destino;
    }
    
    public entListaFuncionarioMSP cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setRuc(rs.getString("ruc")); }
        catch(Exception e) {}
        try { this.setNombreApellido(rs.getString("nombreapellido")); }
        catch(Exception e) {}
        try { this.setGiraduria(rs.getString("giraduria")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        try { this.setAcepta(false); }
        catch(Exception e) { }
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setRuc(String sruc) {
        this.sruc = sruc;
    }

    public void setNombreApellido(String snombreApellido) {
        this.snombreApellido = snombreApellido.trim();
    }

    public void setGiraduria(String sgiraduria) {
        this.sgiraduria = sgiraduria;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setAcepta(boolean bacepta) {
        this.bacepta = bacepta;
    }
    
    public int getId() {
        return this.iid;
    }

    public String getRuc() {
        return this.sruc;
    }
    
    public String getCedula() {
        return this.scedula;
    }
    
    public String getNombreApellido() {
        return this.snombreApellido;
    }

    public String getGiraduria() {
        return this.sgiraduria;
    }

    public String getRubro() {
        return this.srubro;
    }

    public boolean getAcepta() {
        return this.bacepta;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "RUC", "ruc", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Nombre y Apellido", "nombreapellido", generico.entLista.tipo_texto));
        return lst;
    }
    
}
