/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import generico.entGenerica;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entPais extends entGenerica{
    
    protected String sgentilicio;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final int LONGITUD_GENTILICIO = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de País (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_GENTILICIO = "Gentilicio (no vacío, hasta " + LONGITUD_GENTILICIO + " caracteres)";

    public entPais() {
        super();
        this.sgentilicio = "";
    }

    public entPais(int iid, String sdescripcion, String sgentilicio, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.sgentilicio = sgentilicio.trim().toUpperCase();
    }

    public void setEntidad(int iid, String sdescripcion, String sgentilicio, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.sgentilicio = sgentilicio.trim().toUpperCase();
        this.hestadoRegistro = hestadoRegistro;
    }

    public entPais copiar(entPais destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getGentilicio(), this.getEstadoRegistro());
        return destino;
    }
    
    public entPais cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setGentilicio(rs.getString("gentilicio")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setGentilicio(String sgentilicio) {
        this.sgentilicio = sgentilicio.trim().toUpperCase();
    }

    public String getGentilicio() {
        return this.sgentilicio;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getGentilicio().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_GENTILICIO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT pais("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getGentilicio())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Gentilicio", "gentilicio", generico.entLista.tipo_texto));
        return lst;
    }
    
}
