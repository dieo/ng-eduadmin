/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entOperacion {

    private int iid;
    private int iidMovimiento;
    private String stabla;
    private int iplazo;
    private double utasa;
    private java.util.Date dfechaPrimerVencimiento;
    private double ucapital;
    private double uinteres;
    private int icantidadDia;

    public entOperacion() {
        this.iid = 0;
        this.iidMovimiento = 0;
        this.stabla = "";
        this.iplazo = 0;
        this.utasa = 0.0;
        this.dfechaPrimerVencimiento = null;
        this.ucapital = 0.0;
        this.uinteres = 0.0;
        this.icantidadDia = 0;
    }

    public entOperacion(int iid, int iidMovimiento, String stabla, int iplazo, double utasa, java.util.Date dfechaPrimerVencimiento, double ucapital, double uinteres, int icantidadDia) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.stabla = stabla;
        this.iplazo = iplazo;
        this.utasa = utasa;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.ucapital = ucapital;
        this.uinteres = uinteres;
        this.icantidadDia = icantidadDia;
    }

    public void setEntidad(int iid, int iidMovimiento, String stabla, int iplazo, double utasa, java.util.Date dfechaPrimerVencimiento, double ucapital, double uinteres, int icantidadDia) {
        this.iid = iid;
        this.iidMovimiento = iidMovimiento;
        this.stabla = stabla;
        this.iplazo = iplazo;
        this.utasa = utasa;
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
        this.ucapital = ucapital;
        this.uinteres = uinteres;
        this.icantidadDia = icantidadDia;
    }

    public entOperacion copiar(entOperacion destino) {
        destino.setEntidad(this.getId(), this.getIdMovimiento(), this.getTabla(), this.getPlazo(), this.getTasa(), this.getFechaPrimerVencimiento(), this.getCapital(), this.getInteres(), this.getCantidadDia());
        return destino;
    }

    public entOperacion cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idoperacion")); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setTasa(double utasa) {
        this.utasa = utasa;
    }

    public void setFechaPrimerVencimiento(java.util.Date dfechaPrimerVencimiento) {
        this.dfechaPrimerVencimiento = dfechaPrimerVencimiento;
    }

    public void setCapital(double ucapital) {
        this.ucapital = ucapital;
    }

    public void setInteres(double uinteres) { // creado para ahorro programado, los intereses a pagar
        this.uinteres = uinteres;
    }

    public void setCantidadDia(int icantidadDia) { // creado para ahorro programado, los dias devengados
        this.icantidadDia = icantidadDia;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public String getTabla() {
        return this.stabla;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public double getTasa() {
        return this.utasa;
    }

    public java.util.Date getFechaPrimerVencimiento() {
        return this.dfechaPrimerVencimiento;
    }

    public double getCapital() {
        return this.ucapital;
    }

    public double getInteres() {
        return this.uinteres;
    }

    public int getCantidadDia() {
        return this.icantidadDia;
    }

    public String getSentenciaOperacionDetallexx() {
        return "SELECT operacion("+
            this.getId()+","+
            this.getIdMovimiento()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTabla())+","+
            this.getPlazo()+","+
            this.getTasa()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaPrimerVencimiento())+","+
            this.getCapital()+")";
    }

}
