/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDependencia extends generico.entGenerica{
    
    private String snivel;
    private int iidDependenciaPadre;
    private String sdependenciaPadre;
    
    public static final int LONGITUD_DESCRIPCION = 50;
    public static final int LONGITUD_NIVEL = 14;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de la Dependencia (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_NIVEL = "Nivel (no vacía, hasta " + LONGITUD_NIVEL + " caracteres)";
    public static final String TEXTO_DEPENDENCIA_PADRE = "Dependencia Padre";

    public entDependencia() {
        super();
        this.snivel = "";
        this.iidDependenciaPadre = 0;
        this.sdependenciaPadre = "";
    }

    public entDependencia(int iid, String sdescripcion, String snivel, int iidDependenciaPadre, String sdependenciaPadre, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.snivel = snivel;
        this.iidDependenciaPadre = iidDependenciaPadre;
        this.sdependenciaPadre = sdependenciaPadre;
    }
    
    public void setEntidad(int iid, String sdescripcion, String snivel, int iidDependenciaPadre, String sdependenciaPadre, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.snivel = snivel;
        this.iidDependenciaPadre = iidDependenciaPadre;
        this.sdependenciaPadre = sdependenciaPadre;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entDependencia copiar(entDependencia destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getNivel(), this.getIdDependenciaPadre(), this.getDependenciaPadre(), this.getEstadoRegistro());
        return destino;
    }

    public entDependencia cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setNivel(rs.getString("nivel")); }
        catch(Exception e) { }
        try { this.setIdDependenciaPadre(rs.getInt("iddependenciapadre")); }
        catch(Exception e) {}
        try { this.setDependenciaPadre(rs.getString("dependenciapadre")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setNivel(String snivel) {
        this.snivel = snivel;
    }

    public void setIdDependenciaPadre(int iidDependenciaPadre) {
        this.iidDependenciaPadre = iidDependenciaPadre;
    }

    public void setDependenciaPadre(String sdependenciaPadre) {
        this.sdependenciaPadre = sdependenciaPadre;
    }

    public String getNivel() {
        return this.snivel;
    }

    public int getIdDependenciaPadre() {
        return this.iidDependenciaPadre;
    }

    public String getDependenciaPadre() {
        return this.sdependenciaPadre;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getNivel().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NIVEL;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT dependencia("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNivel())+","+
            this.getIdDependenciaPadre()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(1, "Dependencia Padre", "dependenciapadre", generico.entLista.tipo_texto));
        return lst;
    }
    
}
