/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entLlamada {

    private int iid;
    private int iidSocio;
    private String scedula;
    private String snombreApellido;
    private int iidTelefonoOrigen;
    private String stelefonoOrigen;
    private String stelefonoDestino;
    private int iidObjetoLlamada;
    private String sobjetoLlamada;
    private int iidSolucionLlamada;
    private String ssolucionLlamada;
    private java.util.Date dfechaLlamada;
    private String shoraLlamada;
    private String sobservacion;
    private int iduracion;
    private int iidUsuario;
    private String susuarioNombre;
    private String susuarioApellido;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_TELEFONO_DESTINO = 12;
    public static final int LONGITUD_OBSERVACION = 250;
    public static final int LONGITUD_NOMBRE_APELLIDO = 80;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_SOCIO = "Socio (no vacío)";
    public static final String TEXTO_TELEFONO_ORIGEN = "Teléfono Origen (no vacío)";
    public static final String TEXTO_TELEFONO_DESTINO = "Teléfono Destino (no vacío, hasta " + LONGITUD_TELEFONO_DESTINO + " caracteres)";
    public static final String TEXTO_OBJETO_LLAMADA = "Objeto de la Llamada (no vacío)";
    public static final String TEXTO_SOLUCION_LLAMADA = "Solución de la Llamada (no vacío)";
    public static final String TEXTO_FECHA_LLAMADA = "Fecha de la Llamada (no vacía)";
    public static final String TEXTO_HORA_LLAMADA = "Hora de la Llamada (no vacía)";
    public static final String TEXTO_OBSERVACION = "Observación (no vacía)";
    public static final String TEXTO_DURACION = "Duración de la Llamada en minutos (no vacía, valor positivo hasta 60)";
    public static final String TEXTO_USUARIO = "Usuario (no editable)";

    public entLlamada() {
        this.iid = 0;
        this.iidSocio = 0;
        this.scedula = "";
        this.snombreApellido = "";
        this.iidTelefonoOrigen = 0;
        this.stelefonoOrigen = "";
        this.stelefonoDestino = "";
        this.iidObjetoLlamada = 0;
        this.sobjetoLlamada = "";
        this.iidSolucionLlamada = 0;
        this.ssolucionLlamada = "";
        this.dfechaLlamada = null;
        this.shoraLlamada = "";
        this.sobservacion = "";
        this.iduracion = 0;
        this.iidUsuario = 0;
        this.susuarioNombre = "";
        this.susuarioApellido = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entLlamada(int iid, int iidSocio, String scedula, String snombreApellido, int iidTelefonoOrigen, String stelefonoOrigen, String stelefonoDestino, int iidObjetoLlamada, String sobjetoLlamada, int iidSolucionLlamada, String ssolucionLlamada, java.util.Date dfechaLlamada, String shoraLlamada, String sobservacion, int iduracion, int iidUsuario, String susuarioNombre, String susuarioApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidTelefonoOrigen = iidTelefonoOrigen;
        this.stelefonoOrigen = stelefonoOrigen;
        this.stelefonoDestino = stelefonoDestino;
        this.iidObjetoLlamada = iidObjetoLlamada;
        this.sobjetoLlamada = sobjetoLlamada;
        this.iidSolucionLlamada = iidSolucionLlamada;
        this.ssolucionLlamada = ssolucionLlamada;
        this.dfechaLlamada = dfechaLlamada;
        this.shoraLlamada = shoraLlamada;
        this.sobservacion = sobservacion;
        this.iduracion = iduracion;
        this.iidUsuario = iidUsuario;
        this.susuarioNombre = susuarioNombre;
        this.susuarioApellido = susuarioApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidSocio, String scedula, String snombreApellido, int iidTelefonoOrigen, String stelefonoOrigen, String stelefonoDestino, int iidObjetoLlamada, String sobjetoLlamada, int iidSolucionLlamada, String ssolucionLlamada, java.util.Date dfechaLlamada, String shoraLlamada, String sobservacion, int iduracion, int iidUsuario, String susuarioNombre, String susuarioApellido, short hestadoRegistro) {
        this.iid = iid;
        this.iidSocio = iidSocio;
        this.scedula = scedula;
        this.snombreApellido = snombreApellido;
        this.iidTelefonoOrigen = iidTelefonoOrigen;
        this.stelefonoOrigen = stelefonoOrigen;
        this.stelefonoDestino = stelefonoDestino;
        this.iidObjetoLlamada = iidObjetoLlamada;
        this.sobjetoLlamada = sobjetoLlamada;
        this.iidSolucionLlamada = iidSolucionLlamada;
        this.ssolucionLlamada = ssolucionLlamada;
        this.dfechaLlamada = dfechaLlamada;
        this.shoraLlamada = shoraLlamada;
        this.sobservacion = sobservacion;
        this.iduracion = iduracion;
        this.iidUsuario = iidUsuario;
        this.susuarioNombre = susuarioNombre;
        this.susuarioApellido = susuarioApellido;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entLlamada copiar(entLlamada destino) {
        destino.setEntidad(this.getId(), this.getIdSocio(), this.getCedula(), this.getNombreApellido(), this.getIdTelefonoOrigen(), this.getTelefonoOrigen(), this.getTelefonoDestino(), this.getIdObjetoLlamada(), this.getObjetoLlamada(), this.getIdSolucionLlamada(), this.getSolucionLlamada(), this.getFechaLlamada(), this.getHoraLlamada(), this.getObservacion(), this.getDuracion(), this.getIdUsuario(), this.getUsuarioNombre(), this.getUsuarioApellido(), this.getEstadoRegistro());
        return destino;
    }
    
    public entLlamada cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombreApellido(rs.getString("nombreapellido")); }
        catch(Exception e) {}
        try { this.setIdTelefonoOrigen(rs.getInt("idtelefonoorigen")); }
        catch(Exception e) {}
        try { this.setTelefonoOrigen(rs.getString("telefonoorigen")); }
        catch(Exception e) {}
        try { this.setTelefonoDestino(rs.getString("telefonodestino")); }
        catch(Exception e) {}
        try { this.setIdObjetoLlamada(rs.getInt("idobjetollamada")); }
        catch(Exception e) {}
        try { this.setObjetoLlamada(rs.getString("objetollamada")); }
        catch(Exception e) {}
        try { this.setIdSolucionLlamada(rs.getInt("idsolucionllamada")); }
        catch(Exception e) {}
        try { this.setSolucionLlamada(rs.getString("solucionllamada")); }
        catch(Exception e) {}
        try { this.setFechaLlamada(rs.getDate("fechallamada")); }
        catch(Exception e) {}
        try { this.setHoraLlamada(rs.getString("horallamada")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setDuracion(rs.getInt("duracion")); }
        catch(Exception e) {}
        try { this.setIdUsuario(rs.getInt("idusuario")); }
        catch(Exception e) {}
        try { this.setUsuarioNombre(rs.getString("usuarionombre")); }
        catch(Exception e) {}
        try { this.setUsuarioApellido(rs.getString("usuarioapellido")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombreApellido(String snombreApellido) {
        this.snombreApellido = snombreApellido;
    }

    public void setIdTelefonoOrigen(int iidTelefonoOrigen) {
        this.iidTelefonoOrigen = iidTelefonoOrigen;
    }

    public void setTelefonoOrigen(String stelefonoOrigen) {
        this.stelefonoOrigen = stelefonoOrigen;
    }

    public void setTelefonoDestino(String stelefonoDestino) {
        this.stelefonoDestino = stelefonoDestino;
    }

    public void setIdObjetoLlamada(int iidObjetoLlamada) {
        this.iidObjetoLlamada = iidObjetoLlamada;
    }

    public void setObjetoLlamada(String sobjetoLlamada) {
        this.sobjetoLlamada = sobjetoLlamada;
    }

    public void setIdSolucionLlamada(int iidSolucionLlamada) {
        this.iidSolucionLlamada = iidSolucionLlamada;
    }

    public void setSolucionLlamada(String ssolucionLlamada) {
        this.ssolucionLlamada = ssolucionLlamada;
    }

    public void setFechaLlamada(java.util.Date dfechaLlamada) {
        this.dfechaLlamada = dfechaLlamada;
    }

    public void setHoraLlamada(String shoraLlamada) {
        this.shoraLlamada = shoraLlamada;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setDuracion(int iduracion) {
        this.iduracion = iduracion;
    }

    public void setIdUsuario(int iidUsuario) {
        this.iidUsuario = iidUsuario;
    }

    public void setUsuarioNombre(String susuarioNombre) {
        this.susuarioNombre = susuarioNombre;
    }

    public void setUsuarioApellido(String susuarioApellido) {
        this.susuarioApellido = susuarioApellido;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombreApellido() {
        return this.snombreApellido;
    }

    public int getIdTelefonoOrigen() {
        return this.iidTelefonoOrigen;
    }

    public String getTelefonoOrigen() {
        return this.stelefonoOrigen;
    }

    public String getTelefonoDestino() {
        return this.stelefonoDestino;
    }

    public int getIdObjetoLlamada() {
        return this.iidObjetoLlamada;
    }

    public String getObjetoLlamada() {
        return this.sobjetoLlamada;
    }

    public int getIdSolucionLlamada() {
        return this.iidSolucionLlamada;
    }

    public String getSolucionLlamada() {
        return this.ssolucionLlamada;
    }

    public java.util.Date getFechaLlamada() {
        return this.dfechaLlamada;
    }

    public String getHoraLlamada() {
        return this.shoraLlamada;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public int getDuracion() {
        return this.iduracion;
    }

    public int getIdUsuario() {
        return this.iidUsuario;
    }

    public String getUsuarioNombre() {
        return this.susuarioNombre;
    }

    public String getUsuarioApellido() {
        return this.susuarioApellido;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getCedula().isEmpty() || this.getNombreApellido().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_SOCIO;
            bvalido = false;
        }
        if (this.getIdTelefonoOrigen()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO_ORIGEN;
            bvalido = false;
        }
        if (this.getTelefonoDestino().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TELEFONO_DESTINO;
            bvalido = false;
        }
        if (this.getIdObjetoLlamada()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBJETO_LLAMADA;
            bvalido = false;
        }
        if (this.getFechaLlamada()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_LLAMADA;
            bvalido = false;
        }
        if (this.getHoraLlamada().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_LLAMADA;
            bvalido = false;
        }
        if (this.getIdSolucionLlamada()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBJETO_LLAMADA;
            bvalido = false;
        }
        if (this.getObservacion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION;
            bvalido = false;
        }
        if (this.getDuracion()<0 || this.getDuracion()>60) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DURACION;
            bvalido = false;
        }
        if (this.getIdUsuario()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_USUARIO;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT llamada("+
            this.getId()+","+
            this.getIdSocio()+","+
            this.getIdTelefonoOrigen()+","+
            utilitario.utiCadena.getTextoGuardado(this.getTelefonoDestino())+","+
            this.getIdObjetoLlamada()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaLlamada())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraLlamada())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getDuracion()+","+
            this.getIdUsuario()+","+
            this.getIdSolucionLlamada()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombreApellido())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Cedula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Nombre y Apellido", "nombreapellido", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Teléfono Origen", "telefonoorigen", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Teléfono Destino", "telefonodestino", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Objeto Llamada", "objetollamada", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Fecha Llamada", "fechallamada", generico.entLista.tipo_fecha));
        return lst;
    }

}
