/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entDetalleFacturaEgreso {

    protected int iid;
    protected int iitem;
    protected int iidFactura;
    protected int iidProducto;
    protected String sproducto;
    protected int iidUnidadMedida;
    protected String sunidadMedida;
    protected int iidImpuesto;
    protected String simpuesto;
    protected double utasa;
    protected double ucantidad;
    protected double uprecio;
    protected double usubtotal;
    
    protected short hidEstado;
    protected String smensaje;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_ITEM = "Item (no editable)";
    public static final String TEXTO_CANTIDAD = "Cantidad (no vacío, valor positivo)";
    public static final String TEXTO_PRODUCTO = "Producto (no vacío)";
    public static final String TEXTO_UNIDAD_MEDIDA = "Unidad de Medida (no vacía)";
    public static final String TEXTO_IMPUESTO = "Impuesto (no vacío)";
    public static final String TEXTO_PRECIO = "Precio unitario (no vacío, valor positivo)";

    public entDetalleFacturaEgreso() {
        this.iid = 0;
        this.iitem = 0;
        this.iidFactura = 0;
        this.iidProducto = 0;
        this.sproducto = "";
        this.iidUnidadMedida = 0;
        this.sunidadMedida = "";
        this.iidImpuesto = 0;
        this.simpuesto = "";
        this.utasa = 0.0;
        this.ucantidad = 0.0;
        this.uprecio = 0.0;
        this.usubtotal = 0.0;
        this.hidEstado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entDetalleFacturaEgreso(int iid, int iitem, int iidFactura, int iidProducto, String sproducto, int iidUnidadMedida, String sunidadMedida, int iidImpuesto, String simpuesto, double utasa, double ucantidad, double uprecio, double usubtotal, short hidEstado) {
        this.iid = iid;
        this.iitem = iitem;
        this.iidFactura = iidFactura;
        this.iidProducto = iidProducto;
        this.sproducto = sproducto;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.utasa = utasa;
        this.ucantidad = ucantidad;
        this.uprecio = uprecio;
        this.usubtotal = usubtotal;
        this.hidEstado = hidEstado;
        this.smensaje = "";
    }
    
    public void setEntidad(int iid, int iitem, int iidFactura, int iidProducto, String sproducto, int iidUnidadMedida, String sunidadMedida, int iidImpuesto, String simpuesto, double utasa, double ucantidad, double uprecio, double usubtotal, short hidEstado) {
        this.iid = iid;
        this.iitem = iitem;
        this.iidFactura = iidFactura;
        this.iidProducto = iidProducto;
        this.sproducto = sproducto;
        this.iidUnidadMedida = iidUnidadMedida;
        this.sunidadMedida = sunidadMedida;
        this.iidImpuesto = iidImpuesto;
        this.simpuesto = simpuesto;
        this.utasa = utasa;
        this.ucantidad = ucantidad;
        this.uprecio = uprecio;
        this.usubtotal = usubtotal;
        this.hidEstado = hidEstado;
    }

    public entDetalleFacturaEgreso copiar(entDetalleFacturaEgreso destino) {
        destino.setEntidad(this.getId(), this.getItem(), this.getIdFactura(), this.getIdProducto(), this.getProducto(), this.getIdUnidadMedida(), this.getUnidadMedida(), this.getIdImpuesto(), this.getImpuesto(), this.getTasa(), this.getCantidad(), this.getPrecio(), this.getSubtotal(), this.getEstadoRegistro());
        return destino;
    }
    
    public entDetalleFacturaEgreso cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setItem(0); }
        catch(Exception e) {}
        try { this.setIdFactura(rs.getInt("idfacturaegreso")); }
        catch(Exception e) {}
        try { this.setIdProducto(rs.getInt("idproducto")); }
        catch(Exception e) {}
        try { this.setProducto(rs.getString("producto")); }
        catch(Exception e) {}
        try { this.setIdUnidadMedida(rs.getInt("idunidadmedida")); }
        catch(Exception e) {}
        try { this.setUnidadMedida(rs.getString("unidadmedida")); }
        catch(Exception e) {}
        try { this.setIdImpuesto(rs.getInt("idimpuesto")); }
        catch(Exception e) {}
        try { this.setImpuesto(rs.getString("impuesto")); }
        catch(Exception e) {}
        try { this.setTasa(rs.getDouble("tasa")); }
        catch(Exception e) {}
        try { this.setCantidad(rs.getDouble("cantidad")); }
        catch(Exception e) {}
        try { this.setPrecio(rs.getDouble("precio")); }
        catch(Exception e) {}
        try { this.setSubtotal(0.0); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setItem(int iitem) {
        this.iitem = iitem;
    }

    public void setIdFactura(int iidFactura) {
        this.iidFactura = iidFactura;
    }

    public void setIdProducto(int iidProducto) {
        this.iidProducto = iidProducto;
    }

    public void setProducto(String sproducto) {
        this.sproducto = sproducto;
    }

    public void setIdUnidadMedida(int iidUnidadMedida) {
        this.iidUnidadMedida = iidUnidadMedida;
    }

    public void setUnidadMedida(String sunidadMedida) {
        this.sunidadMedida = sunidadMedida;
    }

    public void setIdImpuesto(int iidImpuesto) {
        this.iidImpuesto = iidImpuesto;
    }

    public void setImpuesto(String simpuesto) {
        this.simpuesto = simpuesto;
    }

    public void setTasa(double utasa) {
        this.utasa = utasa;
    }

    public void setCantidad(double ucantidad) {
        this.ucantidad = ucantidad;
    }

    public void setPrecio(double uprecio) {
        this.uprecio = uprecio;
    }

    public void setSubtotal(double usubtotal) {
        this.usubtotal = usubtotal;
    }

    public void setEstadoRegistro(short hidEstado) {
        this.hidEstado = hidEstado;
    }

    public int getId() {
        return this.iid;
    }

    public int getItem() {
        return this.iitem;
    }

    public int getIdFactura() {
        return this.iidFactura;
    }

    public int getIdProducto() {
        return this.iidProducto;
    }

    public String getProducto() {
        return this.sproducto;
    }

    public int getIdUnidadMedida() {
        return this.iidUnidadMedida;
    }

    public String getUnidadMedida() {
        return this.sunidadMedida;
    }

    public int getIdImpuesto() {
        return this.iidImpuesto;
    }

    public String getImpuesto() {
        return this.simpuesto;
    }

    public double getTasa() {
        return this.utasa;
    }

    public double getCantidad() {
        return this.ucantidad;
    }

    public double getPrecio() {
        return this.uprecio;
    }

    public double getSubtotal() {
        return this.usubtotal;
    }

    public short getEstadoRegistro() {
        return this.hidEstado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getIdProducto()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PRODUCTO;
            bvalido = false;
        }
        if (this.getCantidad()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CANTIDAD;
            bvalido = false;
        }
        if (this.getPrecio()<= 0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PRECIO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT detallefacturaegreso("+
            this.getId()+","+
            this.getIdFactura()+","+
            this.getIdProducto()+","+
            this.getIdUnidadMedida()+","+
            this.getIdImpuesto()+","+
            this.getCantidad()+","+
            this.getPrecio()+","+
            this.getEstadoRegistro()+")";
    }

}
