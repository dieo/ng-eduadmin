/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entControlArchivo {

    private int iid;
    private int inumeroOperacion;
    private java.util.Date dfechaProceso;
    private java.util.Date dfechaSolicitud;
    private java.util.Date dfechaAprobacion;
    private String scedula;
    private String snombre;
    private String sapellido;
    private String stipoPrestamo;
    private String scomercio;
    private double umonto;
    private int iplazo;
    private String scheque;
    private String sobservacion;
    private int iidRegion;
    private String sregion;
    private java.util.Date dfechaVencimiento;
    private java.util.Date dfechaAlta;
    private int iidFuncionarioAlta;
    private String sfuncionarioAlta;
    private int iidTipoDocumento;
    private String stipoDocumento;
    private int iidTipoOperacion;
    private String stipoOperacion;
    private String subicacion;
    private String sobservacionAlta;
    private java.util.Date dfechaCancela;
    private int iidFuncionarioCancela;
    private String sfuncionarioCancela;
    private String sobservacionCancela;
    private java.util.Date dfechaBaja;
    private int iidFuncionarioBaja;
    private String sfuncionarioBaja;
    private String sobservacionBaja;
    private int iidTipoBaja;
    private String stipoBaja;

    private short hestadoRegistro;
    private String smensaje;

    public static final int LONGITUD_UBICACION = 3;
    public static final int LONGITUD_OBSERVACION_ALTA = 200;
    public static final int LONGITUD_OBSERVACION_CANCELA = 200;
    public static final int LONGITUD_OBSERVACION_BAJA = 200;
    
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_NUMERO_OPERACION = "Número de Operación (valor positivo)";
    public static final String TEXTO_FECHA_PROCESO = "Fecha de proceso de la operación (no editable)";
    public static final String TEXTO_FECHA_SOLICITUD = "Fecha de solicitud de la operación (no editable)";
    public static final String TEXTO_FECHA_APROBACION = "Fecha de aprobación de la operación (no editable)";
    public static final String TEXTO_CEDULA = "Cédula (no editable)";
    public static final String TEXTO_NOMBRE = "Nombre (no editable)";
    public static final String TEXTO_APELLIDO = "Apellido (no editable)";
    public static final String TEXTO_TIPO_PRESTAMO = "Tipo de Préstamo (no editable)";
    public static final String TEXTO_COMERCIO = "Comercio (no editable)";
    public static final String TEXTO_MONTO = "Monto (valor positivo)";
    public static final String TEXTO_PLAZO = "Plazo (no editable)";
    public static final String TEXTO_CHEQUE = "Cheque (no editable)";
    public static final String TEXTO_OBSERVACION = "Observación (no editable)";
    public static final String TEXTO_REGION = "Región (no vacío)";
    public static final String TEXTO_FECHA_VENCIMIENTO = "Fecha de vencimiento de la operación (no vacía)";

    public static final String TEXTO_FECHA_ALTA = "Fecha de alta del documento (no vacía)";
    public static final String TEXTO_FUNCIONARIO_ALTA = "Funcionario que da alta al documento (no vacío)";
    public static final String TEXTO_TIPO_DOCUMENTO = "Tipo de documento (no vacío)";
    public static final String TEXTO_TIPO_OPERACION = "Tipo de operación (no vacío)";
    public static final String TEXTO_UBICACION = "Código de ubicación física (no vacía, compuesto de 6 caracteres: 3 alfabéticos y 3 numéricos)";
    public static final String TEXTO_OBSERVACION_ALTA = "Observación de alta del documento (no vacía, hasta " + LONGITUD_OBSERVACION_ALTA + " caracteres)";
    
    public static final String TEXTO_FECHA_CANCELA = "Fecha de cancelación del documento (no vacía)";
    public static final String TEXTO_FUNCIONARIO_CANCELA = "Funcionario que cancela el documento (no vacío)";
    public static final String TEXTO_OBSERVACION_CANCELA = "Observación de cancelación del documento (no vacía, hasta " + LONGITUD_OBSERVACION_CANCELA + " caracteres)";
    
    public static final String TEXTO_FECHA_BAJA = "Fecha de baja del documento (no vacía)";
    public static final String TEXTO_FUNCIONARIO_BAJA = "Funcionario que baja el documento (no vacío)";
    public static final String TEXTO_OBSERVACION_BAJA = "Observación de baja del documento (no vacía, hasta " + LONGITUD_OBSERVACION_BAJA + " caracteres)";
    public static final String TEXTO_TIPO_BAJA = "Tipo de baja del documento";
    
    public entControlArchivo() {
        this.iid = 0;
        this.inumeroOperacion = 0;
        this.dfechaProceso = null;
        this.dfechaSolicitud = null;
        this.dfechaAprobacion = null;
        this.scedula = "";
        this.snombre = "";
        this.sapellido = "";
        this.stipoPrestamo = "";
        this.scomercio = "";
        this.umonto = 0.0;
        this.iplazo = 0;
        this.scheque = "";
        this.sobservacion = "";
        this.iidRegion = 0;
        this.sregion = "";
        this.dfechaVencimiento = null;
        this.dfechaAlta = null;
        this.iidFuncionarioAlta = 0;
        this.sfuncionarioAlta = "";
        this.iidTipoDocumento = 0;
        this.stipoDocumento = "";
        this.iidTipoOperacion = 0;
        this.stipoOperacion = "";
        this.subicacion = "";
        this.sobservacionAlta = "";
        this.dfechaCancela = null;
        this.iidFuncionarioCancela = 0;
        this.sfuncionarioCancela = "";
        this.sobservacionCancela = "";
        this.dfechaBaja = null;
        this.iidFuncionarioBaja = 0;
        this.sfuncionarioBaja = "";
        this.sobservacionBaja = "";
        this.iidTipoBaja = 0;
        this.stipoBaja = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entControlArchivo(int iid, int inumeroOperacion, java.util.Date dfechaProceso, java.util.Date dfechaSolicitud, java.util.Date dfechaAprobacion, String scedula, String snombre, String sapellido, String stipoPrestamo, String scomercio, double umonto, int iplazo, String scheque, String sobservacion, int iidRegion, String sregion, java.util.Date dfechaVencimiento, java.util.Date dfechaAlta, int iidFuncionarioAlta, String sfuncionarioAlta, int iidTipoDocumento, String stipoDocumento, int iidTipoOperacion, String stipoOperacion, String subicacion, String sobservacionAlta, java.util.Date dfechaCancela, int iidFuncionarioCancela, String sfuncionarioCancela, String sobservacionCancela, java.util.Date dfechaBaja, int iidFuncionarioBaja, String sfuncionarioBaja, String sobservacionBaja, int iidTipoBaja, String stipoBaja, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaProceso = dfechaProceso;
        this.dfechaSolicitud = dfechaSolicitud;
        this.dfechaAprobacion = dfechaAprobacion;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.stipoPrestamo = stipoPrestamo;
        this.scomercio = scomercio;
        this.umonto = umonto;
        this.iplazo = iplazo;
        this.scheque = scheque;
        this.sobservacion = sobservacion;
        this.iidRegion = iidRegion;
        this.sregion = sregion;
        this.dfechaVencimiento = dfechaVencimiento;
        this.dfechaAlta = dfechaAlta;
        this.iidFuncionarioAlta = iidFuncionarioAlta;
        this.sfuncionarioAlta = sfuncionarioAlta;
        this.iidTipoDocumento = iidTipoDocumento;
        this.stipoDocumento = stipoDocumento;
        this.iidTipoOperacion = iidTipoOperacion;
        this.stipoOperacion = stipoOperacion;
        this.subicacion = subicacion;
        this.sobservacionAlta = sobservacionAlta;
        this.dfechaCancela = dfechaCancela;
        this.iidFuncionarioCancela = iidFuncionarioCancela;
        this.sfuncionarioCancela = sfuncionarioCancela;
        this.sobservacionCancela = sobservacionCancela;
        this.dfechaBaja = dfechaBaja;
        this.iidFuncionarioBaja = iidFuncionarioBaja;
        this.sfuncionarioBaja = sfuncionarioBaja;
        this.sobservacionBaja = sobservacionBaja;
        this.iidTipoBaja = iidTipoBaja;
        this.stipoBaja = stipoBaja;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int inumeroOperacion, java.util.Date dfechaProceso, java.util.Date dfechaSolicitud, java.util.Date dfechaAprobacion, String scedula, String snombre, String sapellido, String stipoPrestamo, String scomercio, double umonto, int iplazo, String scheque, String sobservacion, int iidRegion, String sregion, java.util.Date dfechaVencimiento, java.util.Date dfechaAlta, int iidFuncionarioAlta, String sfuncionarioAlta, int iidTipoDocumento, String stipoDocumento, int iidTipoOperacion, String stipoOperacion, String subicacion, String sobservacionAlta, java.util.Date dfechaCancela, int iidFuncionarioCancela, String sfuncionarioCancela, String sobservacionCancela, java.util.Date dfechaBaja, int iidFuncionarioBaja, String sfuncionarioBaja, String sobservacionBaja, int iidTipoBaja, String stipoBaja, short hestadoRegistro) {
        this.iid = iid;
        this.inumeroOperacion = inumeroOperacion;
        this.dfechaProceso = dfechaProceso;
        this.dfechaSolicitud = dfechaSolicitud;
        this.dfechaAprobacion = dfechaAprobacion;
        this.scedula = scedula;
        this.snombre = snombre;
        this.sapellido = sapellido;
        this.stipoPrestamo = stipoPrestamo;
        this.scomercio = scomercio;
        this.umonto = umonto;
        this.iplazo = iplazo;
        this.scheque = scheque;
        this.sobservacion = sobservacion;
        this.iidRegion = iidRegion;
        this.sregion = sregion;
        this.dfechaVencimiento = dfechaVencimiento;
        this.dfechaAlta = dfechaAlta;
        this.iidFuncionarioAlta = iidFuncionarioAlta;
        this.sfuncionarioAlta = sfuncionarioAlta;
        this.iidTipoDocumento = iidTipoDocumento;
        this.stipoDocumento = stipoDocumento;
        this.iidTipoOperacion = iidTipoOperacion;
        this.stipoOperacion = stipoOperacion;
        this.subicacion = subicacion;
        this.sobservacionAlta = sobservacionAlta;
        this.dfechaCancela = dfechaCancela;
        this.iidFuncionarioCancela = iidFuncionarioCancela;
        this.sfuncionarioCancela = sfuncionarioCancela;
        this.sobservacionCancela = sobservacionCancela;
        this.dfechaBaja = dfechaBaja;
        this.iidFuncionarioBaja = iidFuncionarioBaja;
        this.sfuncionarioBaja = sfuncionarioBaja;
        this.sobservacionBaja = sobservacionBaja;
        this.iidTipoBaja = iidTipoBaja;
        this.stipoBaja = stipoBaja;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entControlArchivo copiar(entControlArchivo destino) {
        destino.setEntidad(this.getId(), this.getNumeroOperacion(), this.getFechaProceso(), this.getFechaSolicitud(), this.getFechaAprobacion(), this.getCedula(), this.getNombre(), this.getApellido(), this.getTipoPrestamo(), this.getComercio(), this.getMonto(), this.getPlazo(), this.getCheque(), this.getObservacion(), this.getIdRegion(), this.getRegion(), this.getFechaVencimiento(), this.getFechaAlta(), this.getIdFuncionarioAlta(), this.getFuncionarioAlta(), this.getIdTipoDocumento(), this.getTipoDocumento(), this.getIdTipoOperacion(), this.getTipoOperacion(), this.getUbicacion(), this.getObservacionAlta(), this.getFechaCancela(), this.getIdFuncionarioCancela(), this.getFuncionarioCancela(), this.getObservacionCancela(), this.getFechaBaja(), this.getIdFuncionarioBaja(), this.getFuncionarioBaja(), this.getObservacionBaja(), this.getIdTipoBaja(), this.getTipoBaja(), this.getEstadoRegistro());
        return destino;
    }
    
    public entControlArchivo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setFechaProceso(rs.getDate("fechaproceso")); }
        catch(Exception e) {}
        try { this.setFechaSolicitud(rs.getDate("fechasolicitud")); }
        catch(Exception e) {}
        try { this.setFechaAprobacion(rs.getDate("fechaaprobacion")); }
        catch(Exception e) {}
        try { this.setCedula(rs.getString("cedula")); }
        catch(Exception e) {}
        try { this.setNombre(rs.getString("nombre")); }
        catch(Exception e) {}
        try { this.setApellido(rs.getString("apellido")); }
        catch(Exception e) {}
        try { this.setTipoPrestamo(rs.getString("tipoprestamo")); }
        catch(Exception e) {}
        try { this.setComercio(rs.getString("comercio")); }
        catch(Exception e) {}
        try { this.setMonto(rs.getDouble("monto")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setCheque(rs.getString("cheque")); }
        catch(Exception e) {}
        try { this.setObservacion(rs.getString("observacion")); }
        catch(Exception e) {}
        try { this.setIdRegion(rs.getInt("idregion")); }
        catch(Exception e) {}
        try { this.setRegion(rs.getString("region")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setFechaAlta(rs.getDate("fechaalta")); }
        catch(Exception e) {}
        try { this.setIdFuncionarioAlta(rs.getInt("idfuncionarioalta")); }
        catch(Exception e) {}
        try { this.setFuncionarioAlta(rs.getString("funcionarioalta")); }
        catch(Exception e) {}
        try { this.setIdTipoDocumento(rs.getInt("idtipodocumento")); }
        catch(Exception e) {}
        try { this.setTipoDocumento(rs.getString("tipodocumento")); }
        catch(Exception e) {}
        try { this.setIdTipoOperacion(rs.getInt("idtipooperacion")); }
        catch(Exception e) {}
        try { this.setTipoOperacion(rs.getString("tipooperacion")); }
        catch(Exception e) {}
        try { this.setUbicacion(rs.getString("ubicacion")); }
        catch(Exception e) {}
        try { this.setObservacionAlta(rs.getString("observacionalta")); }
        catch(Exception e) {}
        try { this.setFechaCancela(rs.getDate("fechacancela")); }
        catch(Exception e) {}
        try { this.setIdFuncionarioCancela(rs.getInt("idfuncionariocancela")); }
        catch(Exception e) {}
        try { this.setFuncionarioCancela(rs.getString("funcionariocancela")); }
        catch(Exception e) {}
        try { this.setObservacionCancela(rs.getString("observacioncancela")); }
        catch(Exception e) {}
        try { this.setFechaBaja(rs.getDate("fechabaja")); }
        catch(Exception e) {}
        try { this.setIdFuncionarioBaja(rs.getInt("idfuncionariobaja")); }
        catch(Exception e) {}
        try { this.setFuncionarioBaja(rs.getString("funcionariobaja")); }
        catch(Exception e) {}
        try { this.setObservacionBaja(rs.getString("observacionbaja")); }
        catch(Exception e) {}
        try { this.setIdTipoBaja(rs.getInt("idtipobaja")); }
        catch(Exception e) {}
        try { this.setTipoBaja(rs.getString("tipobaja")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setFechaProceso(java.util.Date dfechaProceso) {
        this.dfechaProceso = dfechaProceso;
    }

    public void setFechaSolicitud(java.util.Date dfechaSolicitud) {
        this.dfechaSolicitud = dfechaSolicitud;
    }

    public void setFechaAprobacion(java.util.Date dfechaAprobacion) {
        this.dfechaAprobacion = dfechaAprobacion;
    }

    public void setCedula(String scedula) {
        this.scedula = scedula;
    }

    public void setNombre(String snombre) {
        this.snombre = snombre;
    }

    public void setApellido(String sapellido) {
        this.sapellido = sapellido;
    }

    public void setTipoPrestamo(String stipoPrestamo) {
        this.stipoPrestamo = stipoPrestamo;
    }

    public void setComercio(String scomercio) {
        this.scomercio = scomercio;
    }

    public void setMonto(double umonto) {
        this.umonto = umonto;
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setCheque(String scheque) {
        this.scheque = scheque;
    }

    public void setObservacion(String sobservacion) {
        this.sobservacion = sobservacion;
    }

    public void setIdRegion(int iidRegion) {
        this.iidRegion = iidRegion;
    }

    public void setRegion(String sregion) {
        this.sregion = sregion;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setFechaAlta(java.util.Date dfechaAlta) {
        this.dfechaAlta = dfechaAlta;
    }

    public void setIdFuncionarioAlta(int iidFuncionarioAlta) {
        this.iidFuncionarioAlta = iidFuncionarioAlta;
    }

    public void setFuncionarioAlta(String sfuncionarioAlta) {
        this.sfuncionarioAlta = sfuncionarioAlta;
    }

    public void setIdTipoDocumento(int iidTipoDocumento) {
        this.iidTipoDocumento = iidTipoDocumento;
    }

    public void setTipoDocumento(String stipoDocumento) {
        this.stipoDocumento = stipoDocumento;
    }

    public void setIdTipoOperacion(int iidTipoOperacion) {
        this.iidTipoOperacion = iidTipoOperacion;
    }

    public void setTipoOperacion(String stipoOperacion) {
        this.stipoOperacion = stipoOperacion;
    }

    public void setUbicacion(String subicacion) {
        this.subicacion = subicacion;
    }

    public void setObservacionAlta(String sobservacionAlta) {
        this.sobservacionAlta = sobservacionAlta;
    }

    public void setFechaCancela(java.util.Date dfechaCancela) {
        this.dfechaCancela = dfechaCancela;
    }

    public void setIdFuncionarioCancela(int iidFuncionarioCancela) {
        this.iidFuncionarioCancela = iidFuncionarioCancela;
    }

    public void setFuncionarioCancela(String sfuncionarioCancela) {
        this.sfuncionarioCancela = sfuncionarioCancela;
    }

    public void setObservacionCancela(String sobservacionCancela) {
        this.sobservacionCancela = sobservacionCancela;
    }

    public void setFechaBaja(java.util.Date dfechaBaja) {
        this.dfechaBaja = dfechaBaja;
    }

    public void setIdFuncionarioBaja(int iidFuncionarioBaja) {
        this.iidFuncionarioBaja = iidFuncionarioBaja;
    }

    public void setFuncionarioBaja(String sfuncionarioBaja) {
        this.sfuncionarioBaja = sfuncionarioBaja;
    }

    public void setObservacionBaja(String sobservacionBaja) {
        this.sobservacionBaja = sobservacionBaja;
    }

    public void setIdTipoBaja(int iidTipoBaja) {
        this.iidTipoBaja = iidTipoBaja;
    }

    public void setTipoBaja(String stipoBaja) {
        this.stipoBaja = stipoBaja;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public java.util.Date getFechaProceso() {
        return this.dfechaProceso;
    }

    public java.util.Date getFechaSolicitud() {
        return this.dfechaSolicitud;
    }

    public java.util.Date getFechaAprobacion() {
        return this.dfechaAprobacion;
    }

    public String getCedula() {
        return this.scedula;
    }

    public String getNombre() {
        return this.snombre;
    }

    public String getApellido() {
        return this.sapellido;
    }

    public String getTipoPrestamo() {
        return this.stipoPrestamo;
    }

    public String getComercio() {
        return this.scomercio;
    }

    public double getMonto() {
        return this.umonto;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public String getCheque() {
        return this.scheque;
    }

    public String getObservacion() {
        return this.sobservacion;
    }

    public int getIdRegion() {
        return this.iidRegion;
    }

    public String getRegion() {
        return this.sregion;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public java.util.Date getFechaAlta() {
        return this.dfechaAlta;
    }

    public int getIdFuncionarioAlta() {
        return this.iidFuncionarioAlta;
    }

    public String getFuncionarioAlta() {
        return this.sfuncionarioAlta;
    }

    public int getIdTipoDocumento() {
        return this.iidTipoDocumento;
    }

    public String getTipoDocumento() {
        return this.stipoDocumento;
    }

    public int getIdTipoOperacion() {
        return this.iidTipoOperacion;
    }

    public String getTipoOperacion() {
        return this.stipoOperacion;
    }

    public String getUbicacion() {
        return this.subicacion;
    }

    public String getObservacionAlta() {
        return this.sobservacionAlta;
    }

    public java.util.Date getFechaCancela() {
        return this.dfechaCancela;
    }

    public int getIdFuncionarioCancela() {
        return this.iidFuncionarioCancela;
    }

    public String getFuncionarioCancela() {
        return this.sfuncionarioCancela;
    }

    public String getObservacionCancela() {
        return this.sobservacionCancela;
    }

    public java.util.Date getFechaBaja() {
        return this.dfechaBaja;
    }

    public int getIdFuncionarioBaja() {
        return this.iidFuncionarioBaja;
    }

    public String getFuncionarioBaja() {
        return this.sfuncionarioBaja;
    }

    public String getObservacionBaja() {
        return this.sobservacionBaja;
    }

    public int getIdTipoBaja() {
        return this.iidTipoBaja;
    }

    public String getTipoBaja() {
        return this.stipoBaja;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getNumeroOperacion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_OPERACION;
            bvalido = false;
        }
        if (this.getFechaProceso()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_PROCESO;
            bvalido = false;
        }
        if (this.getFechaSolicitud()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_SOLICITUD;
            bvalido = false;
        }
        if (this.getFechaAprobacion()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_APROBACION;
            bvalido = false;
        }
        if (this.getFechaVencimiento()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_VENCIMIENTO;
            bvalido = false;
        }
        if (this.getCedula().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CEDULA;
            bvalido = false;
        }
        if (this.getNombre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NOMBRE;
            bvalido = false;
        }
        if (this.getTipoPrestamo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_PRESTAMO;
            bvalido = false;
        }
        if (this.getComercio().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_COMERCIO;
            bvalido = false;
        }
        if (this.getMonto()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_MONTO;
            bvalido = false;
        }
        if (this.getPlazo()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_PLAZO;
            bvalido = false;
        }
        if (this.getCheque().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CHEQUE;
            bvalido = false;
        }
        if (this.getIdRegion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_REGION;
            bvalido = false;
        }
        if (esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Operación ya fue registrada";
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoAlta() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaAlta()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_ALTA;
            bvalido = false;
        }
        if (this.getIdFuncionarioAlta()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO_ALTA;
            bvalido = false;
        }
        if (this.getIdTipoDocumento()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_DOCUMENTO;
            bvalido = false;
        }
        if (this.getIdTipoOperacion()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_OPERACION;
            bvalido = false;
        }
        if (this.getUbicacion().trim().length()!=LONGITUD_UBICACION || this.getUbicacion().trim().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_UBICACION;
            bvalido = false;
        }
        if (this.getUbicacion().trim().length()==3 && !this.getUbicacion().trim().isEmpty()) {
            int caracter1 = (int)this.getUbicacion().charAt(0);
            int caracter2 = (int)this.getUbicacion().charAt(1);
            int caracter3 = (int)this.getUbicacion().charAt(2);
            if (caracter1<65 || caracter1>90 || caracter2<65 || caracter2>90 || caracter3<65 || caracter3>90) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += TEXTO_UBICACION;
                bvalido = false;
            }
        }
        if (this.getObservacionAlta().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_ALTA;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoCancela() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaCancela()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_CANCELA;
            bvalido = false;
        }
        if (this.getIdFuncionarioCancela()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO_CANCELA;
            bvalido = false;
        }
        if (this.getObservacionCancela().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_CANCELA;
            bvalido = false;
        }
        return bvalido;
    }

    public boolean esValidoBaja() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaBaja()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_BAJA;
            bvalido = false;
        }
        if (this.getIdFuncionarioBaja()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FUNCIONARIO_BAJA;
            bvalido = false;
        }
        if (this.getIdTipoBaja()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_BAJA;
            bvalido = false;
        }
        if (this.getObservacionBaja().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_OBSERVACION_BAJA;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT controlarchivo("+
            this.getId()+","+
            this.getNumeroOperacion()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaProceso())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaSolicitud())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAprobacion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCedula())+","+
            utilitario.utiCadena.getTextoGuardado(this.getNombre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getApellido())+","+
            utilitario.utiCadena.getTextoGuardado(this.getTipoPrestamo())+","+
            utilitario.utiCadena.getTextoGuardado(this.getComercio())+","+
            this.getMonto()+","+
            this.getPlazo()+","+
            utilitario.utiCadena.getTextoGuardado(this.getCheque())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacion())+","+
            this.getIdRegion()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaVencimiento())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaAlta())+","+
            this.getIdFuncionarioAlta()+","+
            this.getIdTipoDocumento()+","+
            this.getIdTipoOperacion()+","+
            utilitario.utiCadena.getTextoGuardado(this.getUbicacion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionAlta())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaCancela())+","+
            this.getIdFuncionarioCancela()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionCancela())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaBaja())+","+
            this.getIdFuncionarioBaja()+","+
            utilitario.utiCadena.getTextoGuardado(this.getObservacionBaja())+","+
            this.getIdTipoBaja()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número Operación", "numerooperacion", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Fecha Proceso", "fechaproceso", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Fecha Solicitud", "fechasolicitud", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(4, "Fecha Aprobación", "fechaaprobacion", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(5, "Cédula", "cedula", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Tipo Préstamo", "tipoprestamo", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(7, "Comercio", "comercio", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(8, "Cheque", "cheque", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(9, "Región", "region", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(10, "Fecha Vencimiento", "fechavencimiento", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(11, "Fecha Alta", "fechaalta", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(12, "Funcionario Alta", "funcionarioalta", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(13, "Tipo Documento", "tipodocumento", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(14, "Tipo Operación", "tipooperacion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(15, "Ubicación", "ubicacion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(16, "Fecha Cancela", "fechacancela", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(17, "Funcionario Cancela", "funcionariocancela", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(18, "Fecha Baja", "fechabaja", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(19, "Funcionario Baja", "funcionariobaja", generico.entLista.tipo_texto));
        return lst;
    }

}
