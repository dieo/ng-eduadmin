/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entTipoPlanillaLegajo extends generico.entGenerica {

    protected String scodigo;
    
    public static final int LONGITUD_DESCRIPCION = 30;
    public static final int LONGITUD_CODIGO = 1;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Tipo Planilla de Legajo (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_CODIGO = "Código (no vacía, hasta " + LONGITUD_CODIGO + " caracteres)";

    public entTipoPlanillaLegajo() {
        super();
        this.scodigo = "";
    }

    public entTipoPlanillaLegajo(int iid, String sdescripcion, String scodigo) {
        super(iid, sdescripcion);
        this.scodigo = scodigo;
    }

    public entTipoPlanillaLegajo(int iid, String sdescripcion, String scodigo, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.scodigo = scodigo;
    }

    public void setEntidad(int iid, String sdescripcion, String scodigo, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.scodigo = scodigo;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entTipoPlanillaLegajo copiar(entTipoPlanillaLegajo destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getCodigo(), this.getEstadoRegistro());
        return destino;
    }

    public entTipoPlanillaLegajo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setCodigo(rs.getString("codigo")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setCodigo(String scodigo) {
        this.scodigo = scodigo;
    }

    public String getCodigo() {
        return this.scodigo;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getCodigo().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_CODIGO;
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT tipoplanillalegajo("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            utilitario.utiCadena.getTextoGuardado(this.getCodigo())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Código", "codigo", generico.entLista.tipo_texto));
        return lst;
    }
    
}
