/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;


/**
 *
 * @author Lic. Didier Barreto
 */
public class entbMovimientoCuentaDetalle {
    
    protected int iid;
    protected int iidMovimientoCuenta;
    protected String sdescripcion;
    protected int iidPlanCuenta;
    protected String snroPlanCuenta;
    protected String snroPlanCuenta2;
    protected String splancuenta;
    protected double dmonto;
    protected short hestado;
    protected String smensaje;
    
    public static final int LONGITUD_DESCRIPCION = 100;
        
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_PLANCUENTA = "Plan de cuenta (no vacía)";
    public static final String TEXTO_MONTO = "Monto (no vacío)";
    
    public entbMovimientoCuentaDetalle() {
        this.iid = 0;
        this.iidMovimientoCuenta = 0;
        this.sdescripcion = "";
        this.iidPlanCuenta = 0;
        this.snroPlanCuenta = "";
        this.snroPlanCuenta2 = "";
        this.splancuenta = "";
        this.dmonto = 0;
        this.hestado = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    public entbMovimientoCuentaDetalle(int iid, int iidMovimientoCuenta, String sdescripcion, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta2, String splancuenta, double dmonto, short hestado) {
        this.iid = iid;
        this.iidMovimientoCuenta = iidMovimientoCuenta;
        this.sdescripcion = sdescripcion;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta2 = snroPlanCuenta2;
        this.splancuenta = splancuenta;
        this.dmonto = dmonto;
        this.hestado = hestado;
        this.smensaje = "";
    }

    public void setEntidad(int iid, int iidMovimientoCuenta, String sdescripcion, int iidPlanCuenta, String snroPlanCuenta, String snroPlanCuenta2, String splancuenta, double dmonto, short hestado) {
        this.iid = iid;
        this.iidMovimientoCuenta = iidMovimientoCuenta;
        this.sdescripcion = sdescripcion;
        this.iidPlanCuenta = iidPlanCuenta;
        this.snroPlanCuenta = snroPlanCuenta;
        this.snroPlanCuenta2 = snroPlanCuenta2;
        this.splancuenta = splancuenta;
        this.dmonto = dmonto;
        this.hestado = hestado;
    }

    public entbMovimientoCuentaDetalle copiar(entbMovimientoCuentaDetalle destino) {
        destino.setEntidad(this.getId(), this.getIdMovimientoCuenta(), this.getDescripcion(), this.getIdPlanCuenta(), this.getNroPlanCuenta(), this.getNroPlanCuenta2(), this.getPlanCuenta(), this.getMonto(), this.getEstadoRegistro());
        return destino;
    }
    
    public entbMovimientoCuentaDetalle cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(SQLException e) { }
        try { this.setIdMovimientoCuenta(rs.getInt("idmovimientocuenta")); }
        catch(SQLException e) { }
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(SQLException e) { }
        try { this.setMonto(rs.getDouble("monto")); }
        catch(SQLException e) { }
        try { this.setIdPlanCuenta(rs.getInt("idplancuenta")); }
        catch(SQLException e) { }
        try { this.setNroPlanCuenta(rs.getString("nroplancuenta")); }
        catch(SQLException e) { }
        try { this.setNroPlanCuenta2(rs.getString("nroplancuenta2")); }
        catch(SQLException e) { }
        try { this.setPlanCuenta(rs.getString("plancuenta")); }
        catch(SQLException e) { }
        //this.setNroPlanCuenta(""); 
        //this.setPlanCuenta(""); 
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }
    
    public void setIdMovimientoCuenta(int iidMovimientoCuenta) {
        this.iidMovimientoCuenta = iidMovimientoCuenta;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion;
    }
    
    public void setIdPlanCuenta(int iidPlanCuenta) {
        this.iidPlanCuenta = iidPlanCuenta;
    }

    public void setNroPlanCuenta(String snroPlanCuenta) {
        this.snroPlanCuenta = snroPlanCuenta;
    }

    public void setNroPlanCuenta2(String snroPlanCuenta2) {
        this.snroPlanCuenta2 = snroPlanCuenta2;
    }
    
    public void setPlanCuenta(String splancuenta) {
        this.splancuenta = splancuenta;
    }
    
    public void setMonto(double dmonto) {
        this.dmonto = dmonto;
    }

    public void setEstadoRegistro(short hestado) {
        this.hestado = hestado;
    }
    
    public int getId() {
        return this.iid;
    }

    public int getIdMovimientoCuenta() {
        return this.iidMovimientoCuenta;
    }
    
    public String getDescripcion() {
        return this.sdescripcion;
    }
    
    public int getIdPlanCuenta() {
        return this.iidPlanCuenta;
    }

    public String getNroPlanCuenta() {
        return this.snroPlanCuenta;
    }

    public String getNroPlanCuenta2() {
        return this.snroPlanCuenta2;
    }
    
    public String getPlanCuenta() {
        return this.splancuenta;
    }
    
    public double getMonto() {
        return this.dmonto;
    }

    public short getEstadoRegistro() {
        return this.hestado;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getIdPlanCuenta()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_PLANCUENTA;
            bvalido = false;
        }
         /*if ((this.getMonto()<=0)) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_MONTO;
            bvalido = false;
        }*/
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT banco.detallemovimientocuenta("+
            this.getId()+","+
            this.getIdMovimientoCuenta()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getIdPlanCuenta()+","+
            this.getMonto()+","+
            this.getEstadoRegistro()+")";
    }

}
