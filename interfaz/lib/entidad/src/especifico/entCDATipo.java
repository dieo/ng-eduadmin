/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier Barreto
 */
public class entCDATipo extends generico.entGenerica {

    protected boolean bactivo;
    private java.util.Date dfechaDesde;
    private java.util.Date dfechaHasta;
    
    public static final int LONGITUD_DESCRIPCION = 40;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Tipo de CDA (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_ACTIVO = "Activo";
    public static final String TEXTO_VIGENCIA = "Vigencia de los intereses fijados para CDA";

    public entCDATipo() {
        super();
        this.bactivo = false;
        this.dfechaDesde=null;
        this.dfechaHasta=null;
    }

    public entCDATipo(int iid, String sdescripcion, boolean bactivo, java.util.Date  dfechaDesde, java.util.Date dfechaHasta) {
        super(iid, sdescripcion);
        this.bactivo = bactivo;
        this.dfechaDesde = dfechaDesde;
        this.dfechaHasta = dfechaHasta;
    }

    public entCDATipo(int iid, String sdescripcion, boolean bactivo, java.util.Date  dfechaDesde, java.util.Date dfechaHasta, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.bactivo = bactivo;
        this.dfechaDesde = dfechaDesde;
        this.dfechaHasta = dfechaHasta;
    }

    public void setEntidad(int iid, String sdescripcion, boolean bactivo, java.util.Date  dfechaDesde, java.util.Date dfechaHasta, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.bactivo = bactivo;
        this.dfechaDesde = dfechaDesde;
        this.dfechaHasta = dfechaHasta;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entCDATipo copiar(entCDATipo destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getActivo(), this.getFechaDesde(), this.getFechaHasta(), this.getEstadoRegistro());
        return destino;
    }

    public entCDATipo cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setActivo(rs.getBoolean("activo")); }
        catch(Exception e) {}
        try { this.setFechaDesde(rs.getDate("fechadesde")); }
        catch(Exception e) {}
        try { this.setFechaHasta(rs.getDate("fechahasta")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setActivo(boolean bactivo) {
        this.bactivo = bactivo;
    }
    
    public void setFechaDesde(java.util.Date dfechaDesde) {
        this.dfechaDesde = dfechaDesde;
    }

    public void setFechaHasta(java.util.Date dfechaHasta) {
        this.dfechaHasta = dfechaHasta;
    }

    public boolean getActivo() {
        return this.bactivo;
    }

    public java.util.Date getFechaDesde() {
        return this.dfechaDesde;
    }

    public java.util.Date getFechaHasta() {
        return this.dfechaHasta;
    }

    public boolean esValido() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (utilitario.utiFecha.getAMD(this.getFechaDesde())>utilitario.utiFecha.getAMD(this.getFechaHasta()) && this.getFechaHasta()!=null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_VIGENCIA+"\nFecha desde no debe ser mayor a Fecha Hasta.";
            bvalido = false;
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT cdatipo("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getActivo()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaDesde())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaHasta())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(2, "Vigencia Desde", "fechadesde", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(3, "Vigencia Hasta", "fechahasta", generico.entLista.tipo_fecha));
        return lst;
    }
    
}
