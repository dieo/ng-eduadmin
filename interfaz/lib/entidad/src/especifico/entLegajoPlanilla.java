/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entLegajoPlanilla extends generico.entGenerica {

    protected int inumeroPlanilla;
    protected int iidTipoPlanilla;
    protected String stipoPlanilla;
    protected int iidDependenciaOrigen;
    protected String sdependenciaOrigen;
    protected int iidDependenciaDestino;
    protected String sdependenciaDestino;
    protected java.util.Date dfechaApertura;
    protected String shoraApertura;
    protected java.util.Date dfechaCierre;
    protected String shoraCierre;
    
    public static final int LONGITUD_DESCRIPCION = 80;
    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_DESCRIPCION = "Descripción de Planilla de Legajo (no vacía, hasta " + LONGITUD_DESCRIPCION + " caracteres)";
    public static final String TEXTO_NUMERO_PLANILLA = "Número de Planilla (no editable)";
    public static final String TEXTO_TIPO_PLANILLA = "Tipo Planilla (no vacío)";
    public static final String TEXTO_DEPENDENCIA_ORIGEN = "Dependencia origen (no vacía)";
    public static final String TEXTO_DEPENDENCIA_DESTINO = "Dependencia destino (no vacía)";
    public static final String TEXTO_FECHA_APERTURA = "Fecha de apertura (no vacía)";
    public static final String TEXTO_HORA_APERTURA = "Hora de apertura (no vacía)";
    public static final String TEXTO_FECHA_CIERRE = "Fecha de cierre (no editable)";
    public static final String TEXTO_HORA_CIERRE = "Hora de cierre (no editable)";

    public entLegajoPlanilla() {
        super();
        this.inumeroPlanilla = 0;
        this.iidTipoPlanilla = 0;
        this.stipoPlanilla = "";
        this.iidDependenciaOrigen = 0;
        this.sdependenciaOrigen = "";
        this.iidDependenciaDestino = 0;
        this.sdependenciaDestino = "";
        this.dfechaApertura = null;
        this.shoraApertura = "";
        this.dfechaCierre = null;
        this.shoraCierre = "";
    }

    public entLegajoPlanilla(int iid, String sdescripcion, int inumeroPlanilla, int iidTipoPlanilla, String stipoPlanilla, int iidDependenciaOrigen, String sdependenciaOrigen, int iidDependenciaDestino, String sdependenciaDestino, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre) {
        super(iid, sdescripcion);
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidTipoPlanilla = iidTipoPlanilla;
        this.stipoPlanilla = stipoPlanilla;
        this.iidDependenciaOrigen = iidDependenciaOrigen;
        this.sdependenciaOrigen = sdependenciaOrigen;
        this.iidDependenciaDestino = iidDependenciaDestino;
        this.sdependenciaDestino = sdependenciaDestino;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
    }

    public entLegajoPlanilla(int iid, String sdescripcion, int inumeroPlanilla, int iidTipoPlanilla, String stipoPlanilla, int iidDependenciaOrigen, String sdependenciaOrigen, int iidDependenciaDestino, String sdependenciaDestino, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, short hestadoRegistro) {
        super(iid, sdescripcion, hestadoRegistro);
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidTipoPlanilla = iidTipoPlanilla;
        this.stipoPlanilla = stipoPlanilla;
        this.iidDependenciaOrigen = iidDependenciaOrigen;
        this.sdependenciaOrigen = sdependenciaOrigen;
        this.iidDependenciaDestino = iidDependenciaDestino;
        this.sdependenciaDestino = sdependenciaDestino;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
    }

    public void setEntidad(int iid, String sdescripcion, int inumeroPlanilla, int iidTipoPlanilla, String stipoPlanilla, int iidDependenciaOrigen, String sdependenciaOrigen, int iidDependenciaDestino, String sdependenciaDestino, java.util.Date dfechaApertura, String shoraApertura, java.util.Date dfechaCierre, String shoraCierre, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion;
        this.inumeroPlanilla = inumeroPlanilla;
        this.iidTipoPlanilla = iidTipoPlanilla;
        this.stipoPlanilla = stipoPlanilla;
        this.iidDependenciaOrigen = iidDependenciaOrigen;
        this.sdependenciaOrigen = sdependenciaOrigen;
        this.iidDependenciaDestino = iidDependenciaDestino;
        this.sdependenciaDestino = sdependenciaDestino;
        this.dfechaApertura = dfechaApertura;
        this.shoraApertura = shoraApertura;
        this.dfechaCierre = dfechaCierre;
        this.shoraCierre = shoraCierre;
        this.hestadoRegistro = hestadoRegistro;
    }
    
    public entLegajoPlanilla copiar(entLegajoPlanilla destino) {
        destino.setEntidad(this.getId(), this.getDescripcion(), this.getNumeroPlanilla(), this.getIdTipoPlanilla(), this.getTipoPlanilla(), this.getIdDependenciaOrigen(), this.getDependenciaOrigen(), this.getIdDependenciaDestino(), this.getDependenciaDestino(), this.getFechaApertura(), this.getHoraApertura(), this.getFechaCierre(), this.getHoraCierre(), this.getEstadoRegistro());
        return destino;
    }

    public entLegajoPlanilla cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setDescripcion(rs.getString("descripcion")); }
        catch(Exception e) {}
        try { this.setNumeroPlanilla(rs.getInt("numeroplanilla")); }
        catch(Exception e) {}
        try { this.setIdTipoPlanilla(rs.getInt("idtipoplanilla")); }
        catch(Exception e) {}
        try { this.setTipoPlanilla(rs.getString("tipoplanilla")); }
        catch(Exception e) {}
        try { this.setIdDependenciaOrigen(rs.getInt("iddependenciaorigen")); }
        catch(Exception e) {}
        try { this.setDependenciaOrigen(rs.getString("dependenciaorigen")); }
        catch(Exception e) {}
        try { this.setIdDependenciaDestino(rs.getInt("iddependenciadestino")); }
        catch(Exception e) {}
        try { this.setDependenciaDestino(rs.getString("dependenciadestino")); }
        catch(Exception e) {}
        try { this.setFechaApertura(rs.getDate("fechaapertura")); }
        catch(Exception e) {}
        try { this.setHoraApertura(rs.getString("horaapertura")); }
        catch(Exception e) {}
        try { this.setFechaCierre(rs.getDate("fechacierre")); }
        catch(Exception e) {}
        try { this.setHoraCierre(rs.getString("horacierre")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setNumeroPlanilla(int inumeroPlanilla) {
        this.inumeroPlanilla = inumeroPlanilla;
    }

    public void setIdTipoPlanilla(int iidTipoPlanilla) {
        this.iidTipoPlanilla = iidTipoPlanilla;
    }

    public void setTipoPlanilla(String stipoPlanilla) {
        this.stipoPlanilla = stipoPlanilla;
    }

    public void setIdDependenciaOrigen(int iidDependenciaOrigen) {
        this.iidDependenciaOrigen = iidDependenciaOrigen;
    }

    public void setDependenciaOrigen(String sdependenciaOrigen) {
        this.sdependenciaOrigen = sdependenciaOrigen;
    }

    public void setIdDependenciaDestino(int iidDependenciaDestino) {
        this.iidDependenciaDestino = iidDependenciaDestino;
    }

    public void setDependenciaDestino(String sdependenciaDestino) {
        this.sdependenciaDestino = sdependenciaDestino;
    }

    public void setFechaApertura(java.util.Date dfechaApertura) {
        this.dfechaApertura = dfechaApertura;
    }

    public void setHoraApertura(String shoraApertura) {
        this.shoraApertura = shoraApertura;
    }

    public void setFechaCierre(java.util.Date dfechaCierre) {
        this.dfechaCierre = dfechaCierre;
    }

    public void setHoraCierre(String shoraCierre) {
        this.shoraCierre = shoraCierre;
    }

    public int getNumeroPlanilla() {
        return this.inumeroPlanilla;
    }

    public int getIdTipoPlanilla() {
        return this.iidTipoPlanilla;
    }

    public String getTipoPlanilla() {
        return this.stipoPlanilla;
    }

    public int getIdDependenciaOrigen() {
        return this.iidDependenciaOrigen;
    }

    public String getDependenciaOrigen() {
        return this.sdependenciaOrigen;
    }

    public int getIdDependenciaDestino() {
        return this.iidDependenciaDestino;
    }

    public String getDependenciaDestino() {
        return this.sdependenciaDestino;
    }

    public java.util.Date getFechaApertura() {
        return this.dfechaApertura;
    }

    public String getHoraApertura() {
        return this.shoraApertura;
    }

    public java.util.Date getFechaCierre() {
        return this.dfechaCierre;
    }

    public String getHoraCierre() {
        return this.shoraCierre;
    }

    public boolean esValido(boolean esDuplicado) {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getDescripcion().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_DESCRIPCION;
            bvalido = false;
        }
        if (this.getNumeroPlanilla()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_NUMERO_PLANILLA;
            bvalido = false;
        }
        if (this.getIdTipoPlanilla()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_TIPO_PLANILLA;
            bvalido = false;
        }
        if (this.getFechaApertura()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_APERTURA;
            bvalido = false;
        }
        if (this.getHoraApertura().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_APERTURA;
            bvalido = false;
        } else {
            if (!utilitario.utiFecha.esValidoHoraLarga(this.getHoraApertura())) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Hora inválida";
                bvalido = false;
            }
        }
        if (esDuplicado) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += "Planilla debe tener único Código";
            bvalido = false;
        }
        return bvalido;
    }
    
    public boolean esValidoCerrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFechaCierre()==null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_FECHA_CIERRE;
            bvalido = false;
        }
        if (this.getHoraCierre().isEmpty()) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += TEXTO_HORA_CIERRE;
            bvalido = false;
        } else {
            if (!utilitario.utiFecha.esValidoHoraLarga(this.getHoraCierre())) {
                if (!this.smensaje.isEmpty()) this.smensaje += "\n";
                this.smensaje += "Hora inválida";
                bvalido = false;
            }
        }
        return bvalido;
    }
    
    public String getSentencia() {
        return "SELECT legajoplanilla("+
            this.getId()+","+
            utilitario.utiCadena.getTextoGuardado(this.getDescripcion())+","+
            this.getNumeroPlanilla()+","+
            this.getIdTipoPlanilla()+","+
            this.getIdDependenciaOrigen()+","+
            this.getIdDependenciaDestino()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaApertura())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraApertura())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaCierre())+","+
            utilitario.utiCadena.getTextoGuardado(this.getHoraCierre())+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Número Planilla", "numeroplanilla", generico.entLista.tipo_numero));
        lst.add(new generico.entLista(2, "Descripción", "descripcion", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(3, "Tipo Planilla", "tipoplanilla", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(4, "Dependencia origen", "dependenciaorigen", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(5, "Dependencia destino", "dependenciadestino", generico.entLista.tipo_texto));
        lst.add(new generico.entLista(6, "Fecha apertura", "fechaapertura", generico.entLista.tipo_fecha));
        lst.add(new generico.entLista(7, "Fecha cierre", "fechacierre", generico.entLista.tipo_fecha));
        return lst;
    }
    
}
