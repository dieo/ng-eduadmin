/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entExtracto {

    private int iidSocio;
    private java.util.Date dfecha;
    private int iidMovimiento;
    private String stipoInforme;
    private String stipo;
    private int iidCuenta;
    private String scuenta;
    private String sdenominacion;
    private int inumeroOperacion;
    private int inumeroSolicitud;
    private java.util.Date dfechaOperacion;
    private int iplazo;
    private int inumeroCuota;
    private double utasaInteres;
    private double utasaInteresMoratorio;
    private double utasaInteresPunitorio;
    private boolean bcancelacion;
    private int iidTipoCredito;
    private String stipoCredito;
    private int iidEntidad;
    private String sentidad;
    private java.util.Date dfechaVencimiento;
    private double umontoTotal;
    private double ucobro;
    private double uatraso;
    private double uactual;
    private double usaldo;
    private double ucuota;
    private double uexoneracion;
    private double uinteresMoratorio;
    private double uinteresPunitorio;
    private String stabla;
    private String srubro;
    private int idiaAtraso;
    private int iestadoRegistro;
    
    public entExtracto() {
        this.iidSocio = 0;
        this.dfecha = null;
        this.iidMovimiento = 0;
        this.stipoInforme = "";
        this.stipo = "";
        this.iidCuenta = 0;
        this.scuenta = "";
        this.sdenominacion = "";
        this.inumeroOperacion = 0;
        this.inumeroSolicitud = 0;
        this.dfechaOperacion = null;
        this.iplazo = 0;
        this.inumeroCuota = 0;
        this.utasaInteres = 0.0;
        this.utasaInteresMoratorio = 0.0;
        this.utasaInteresPunitorio = 0.0;
        this.bcancelacion = false;
        this.iidTipoCredito = 0;
        this.stipoCredito = "";
        this.iidEntidad = 0;
        this.sentidad = "";
        this.dfechaVencimiento = null;
        this.umontoTotal = 0.0;
        this.ucobro = 0.0;
        this.uatraso = 0.0;
        this.uactual = 0.0;
        this.usaldo = 0.0;
        this.ucuota = 0.0;
        this.uexoneracion = 0.0;
        this.uinteresMoratorio = 0.0;
        this.uinteresPunitorio = 0.0;
        this.stabla = "";
        this.srubro = "";
        this.idiaAtraso = 0;
        this.iestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entExtracto(int iidSocio, java.util.Date dfecha, int iidMovimiento, String stipoInforme, String stipo, int iidCuenta, String scuenta, String sdenominacion, int inumeroOperacion, int inumeroSolicitud, java.util.Date dfechaOperacion, int iplazo, int inumeroCuota, double utasaInteres, double utasaInteresMoratorio, double utasaInteresPunitorio, boolean bcancelacion, int iidTipoCredito, String stipoCredito, int iidEntidad, String sentidad, java.util.Date dfechaVencimiento, double umontoTotal, double ucobro, double uatraso, double uactual, double usaldo, double ucuota, double uexoneracion, double uinteresMoratorio, double uinteresPunitorio, String stabla, String srubro, int idiaAtraso, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.dfecha = dfecha;
        this.iidMovimiento = iidMovimiento;
        this.stipoInforme = stipoInforme;
        this.stipo = stipo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdenominacion = sdenominacion;
        this.inumeroOperacion = inumeroOperacion;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaOperacion = dfechaOperacion;
        this.iplazo = iplazo;
        this.inumeroCuota = inumeroCuota;
        this.utasaInteres = utasaInteres;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.bcancelacion = bcancelacion;
        this.iidTipoCredito = iidTipoCredito;
        this.stipoCredito = stipoCredito;
        this.iidEntidad = iidEntidad;
        this.sentidad = sentidad;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoTotal = umontoTotal;
        this.ucobro = ucobro;
        this.uatraso = uatraso;
        this.uactual = uactual;
        this.usaldo = usaldo;
        this.ucuota = ucuota;
        this.uexoneracion = uexoneracion;
        this.uinteresMoratorio = uinteresMoratorio;
        this.uinteresPunitorio = uinteresPunitorio;
        this.stabla = stabla;
        this.srubro = srubro;
        this.idiaAtraso = idiaAtraso;
        this.iestadoRegistro = iestadoRegistro;
    }

    public void setEntidad(int iidSocio, java.util.Date dfecha, int iidMovimiento, String stipoInforme, String stipo, int iidCuenta, String scuenta, String sdenominacion, int inumeroOperacion, int inumeroSolicitud, java.util.Date dfechaOperacion, int iplazo, int inumeroCuota, double utasaInteres, double utasaInteresMoratorio, double utasaInteresPunitorio, boolean bcancelacion, int iidTipoCredito, String stipoCredito, int iidEntidad, String sentidad, java.util.Date dfechaVencimiento, double umontoTotal, double ucobro, double uatraso, double uactual, double usaldo, double ucuota, double uexoneracion, double uinteresMoratorio, double uinteresPunitorio, String stabla, String srubro, int idiaAtraso, int iestadoRegistro) {
        this.iidSocio = iidSocio;
        this.dfecha = dfecha;
        this.iidMovimiento = iidMovimiento;
        this.stipoInforme = stipoInforme;
        this.stipo = stipo;
        this.iidCuenta = iidCuenta;
        this.scuenta = scuenta;
        this.sdenominacion = sdenominacion;
        this.inumeroOperacion = inumeroOperacion;
        this.inumeroSolicitud = inumeroSolicitud;
        this.dfechaOperacion = dfechaOperacion;
        this.iplazo = iplazo;
        this.inumeroCuota = inumeroCuota;
        this.utasaInteres = utasaInteres;
        this.utasaInteresMoratorio = utasaInteresMoratorio;
        this.utasaInteresPunitorio = utasaInteresPunitorio;
        this.bcancelacion = bcancelacion;
        this.iidTipoCredito = iidTipoCredito;
        this.stipoCredito = stipoCredito;
        this.iidEntidad = iidEntidad;
        this.sentidad = sentidad;
        this.dfechaVencimiento = dfechaVencimiento;
        this.umontoTotal = umontoTotal;
        this.ucobro = ucobro;
        this.uatraso = uatraso;
        this.uactual = uactual;
        this.usaldo = usaldo;
        this.ucuota = ucuota;
        this.uexoneracion = uexoneracion;
        this.uinteresMoratorio = uinteresMoratorio;
        this.uinteresPunitorio = uinteresPunitorio;
        this.stabla = stabla;
        this.srubro = srubro;
        this.idiaAtraso = idiaAtraso;
        this.iestadoRegistro = iestadoRegistro;
    }

    public entExtracto copiar(entExtracto destino) {
        destino.setEntidad(this.getIdSocio(), this.getFecha(), this.getIdMovimiento(), this.getTipoInforme(), this.getTipo(), this.getIdCuenta(), this.getCuenta(), this.getDenominacion(), this.getNumeroOperacion(), this.getNumeroSolicitud(), this.getFechaOperacion(), this.getPlazo(), this.getNumeroCuota(), this.getTasaInteres(), this.getTasaInteresMoratorio(), this.getTasaInteresPunitorio(), this.getCancelacion(), this.getIdTipoCredito(), this.getTipoCredito(), this.getIdEntidad(), this.getEntidad(), this.getFechaVencimiento(), this.getMontoTotal(), this.getCobro(), this.getAtraso(), this.getActual(), this.getSaldo(), this.getCuota(), this.getExoneracion(), this.getInteresMoratorio(), this.getInteresPunitorio(), this.getTabla(), this.getRubro(), this.getDiaAtraso(), this.getEstadoRegistro());
        return destino;
    }

    public entExtracto cargar(java.sql.ResultSet rs) {
        try { this.setIdSocio(rs.getInt("idsocio")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setIdMovimiento(rs.getInt("idmovimiento")); }
        catch(Exception e) {}
        try { this.setTipoInforme(rs.getString("tipoinforme")); }
        catch(Exception e) {}
        try { this.setTipo(rs.getString("tipo")); }
        catch(Exception e) {}
        try { this.setIdCuenta(rs.getInt("idcuenta")); }
        catch(Exception e) {}
        try { this.setCuenta(rs.getString("cuenta")); }
        catch(Exception e) {}
        try { this.setDenominacion(rs.getString("denominacion")); }
        catch(Exception e) {}
        try { this.setNumeroOperacion(rs.getInt("numerooperacion")); }
        catch(Exception e) {}
        try { this.setNumeroSolicitud(rs.getInt("numerosolicitud")); }
        catch(Exception e) {}
        try { this.setFechaOperacion(rs.getDate("fechaoperacion")); }
        catch(Exception e) {}
        try { this.setPlazo(rs.getInt("plazo")); }
        catch(Exception e) {}
        try { this.setNumeroCuota(rs.getInt("numerocuota")); }
        catch(Exception e) {}
        try { this.setTasaInteres(rs.getDouble("tasainteres")); }
        catch(Exception e) {}
        try { this.setTasaInteresMoratorio(rs.getDouble("tasainteresmoratorio")); }
        catch(Exception e) {}
        try { this.setTasaInteresPunitorio(rs.getDouble("tasainterespunitorio")); }
        catch(Exception e) {}
        try { this.setCancelacion(rs.getBoolean("cancelacion")); }
        catch(Exception e) {}
        try { this.setIdTipoCredito(rs.getInt("idtipocredito")); }
        catch(Exception e) {}
        try { this.setTipoCredito(rs.getString("tipocredito")); }
        catch(Exception e) {}
        try { this.setIdEntidad(rs.getInt("identidad")); }
        catch(Exception e) {}
        try { this.setEntidad(rs.getString("entidad")); }
        catch(Exception e) {}
        try { this.setFechaVencimiento(rs.getDate("fechavencimiento")); }
        catch(Exception e) {}
        try { this.setMontoTotal(rs.getDouble("montototal")); }
        catch(Exception e) {}
        try { this.setCobro(rs.getDouble("cobro")); }
        catch(Exception e) {}
        try { this.setAtraso(rs.getDouble("atraso")); }
        catch(Exception e) {}
        try { this.setActual(rs.getDouble("actual")); }
        catch(Exception e) {}
        try { this.setSaldo(rs.getDouble("saldo")); }
        catch(Exception e) {}
        try { this.setCuota(rs.getDouble("cuota")); }
        catch(Exception e) {}
        try { this.setExoneracion(rs.getDouble("exoneracion")); }
        catch(Exception e) {}
        try { this.setInteresMoratorio(0.0); }
        catch(Exception e) {}
        try { this.setInteresPunitorio(0.0); }
        catch(Exception e) {}
        try { this.setTabla(rs.getString("tabla")); }
        catch(Exception e) {}
        try { this.setRubro(rs.getString("rubro")); }
        catch(Exception e) {}
        try { this.setDiaAtraso(rs.getInt("diaatraso")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setIdSocio(int iidSocio) {
        this.iidSocio = iidSocio;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setIdMovimiento(int iidMovimiento) {
        this.iidMovimiento = iidMovimiento;
    }

    public void setTipoInforme(String stipoInforme) {
        this.stipoInforme = stipoInforme;
    }

    public void setTipo(String stipo) {
        this.stipo = stipo;
    }

    public void setIdCuenta(int iidCuenta) {
        this.iidCuenta = iidCuenta;
    }

    public void setCuenta(String scuenta) {
        this.scuenta = scuenta;
    }

    public void setDenominacion(String sdenominacion) {
        this.sdenominacion = sdenominacion;
    }

    public void setNumeroOperacion(int inumeroOperacion) {
        this.inumeroOperacion = inumeroOperacion;
    }

    public void setNumeroSolicitud(int inumeroSolicitud) {
        this.inumeroSolicitud = inumeroSolicitud;
    }

    public void setFechaOperacion(java.util.Date dfechaOperacion) {
        this.dfechaOperacion = (java.util.Date)dfechaOperacion.clone();
    }

    public void setPlazo(int iplazo) {
        this.iplazo = iplazo;
    }

    public void setNumeroCuota(int inumeroCuota) {
        this.inumeroCuota = inumeroCuota;
    }

    public void setTasaInteres(double utasaInteres) {
        this.utasaInteres = utasaInteres;
    }

    public void setTasaInteresMoratorio(double utasaInteresMoratorio) {
        this.utasaInteresMoratorio = utasaInteresMoratorio;
    }

    public void setTasaInteresPunitorio(double utasaInteresPunitorio) {
        this.utasaInteresPunitorio = utasaInteresPunitorio;
    }

    public void setCancelacion(boolean bcancelacion) {
        this.bcancelacion = bcancelacion;
    }

    public void setIdTipoCredito(int iidTipoCredito) {
        this.iidTipoCredito = iidTipoCredito;
    }

    public void setTipoCredito(String stipoCredito) {
        this.stipoCredito = stipoCredito;
    }

    public void setIdEntidad(int iidEntidad) {
        this.iidEntidad = iidEntidad;
    }

    public void setEntidad(String sentidad) {
        this.sentidad = sentidad;
    }

    public void setFechaVencimiento(java.util.Date dfechaVencimiento) {
        this.dfechaVencimiento = dfechaVencimiento;
    }

    public void setMontoTotal(double umontoTotal) {
        this.umontoTotal = umontoTotal;
    }

    public void setCobro(double ucobro) {
        this.ucobro = ucobro;
    }

    public void setAtraso(double uatraso) {
        this.uatraso = uatraso;
    }

    public void setActual(double uactual) {
        this.uactual = uactual;
    }

    public void setSaldo(double usaldo) {
        this.usaldo = usaldo;
    }

    public void setCuota(double ucuota) {
        this.ucuota = ucuota;
    }

    public void setExoneracion(double uexoneracion) {
        this.uexoneracion = uexoneracion;
    }

    public void setInteresMoratorio(double uinteresMoratorio) {
        this.uinteresMoratorio = uinteresMoratorio;
    }

    public void setInteresPunitorio(double uinteresPunitorio) {
        this.uinteresPunitorio = uinteresPunitorio;
    }

    public void setTabla(String stabla) {
        this.stabla = stabla;
    }

    public void setRubro(String srubro) {
        this.srubro = srubro;
    }

    public void setDiaAtraso(int idiaAtraso) {
        this.idiaAtraso = idiaAtraso;
    }

    public void setEstadoRegistro(int iestadoRegistro) {
        this.iestadoRegistro = iestadoRegistro;
    }

    public int getIdSocio() {
        return this.iidSocio;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }

    public int getIdMovimiento() {
        return this.iidMovimiento;
    }

    public String getTipo() {
        return this.stipo;
    }

    public String getTipoInforme() {
        return this.stipoInforme;
    }

    public int getIdCuenta() {
        return this.iidCuenta;
    }

    public String getCuenta() {
        return this.scuenta;
    }

    public String getDenominacion() {
        return this.sdenominacion;
    }

    public int getNumeroOperacion() {
        return this.inumeroOperacion;
    }

    public int getNumeroSolicitud() {
        return this.inumeroSolicitud;
    }

    public java.util.Date getFechaOperacion() {
        return this.dfechaOperacion;
    }

    public int getPlazo() {
        return this.iplazo;
    }

    public int getNumeroCuota() {
        return this.inumeroCuota;
    }

    public Double getTasaInteres() {
        return this.utasaInteres;
    }

    public Double getTasaInteresMoratorio() {
        return this.utasaInteresMoratorio;
    }

    public Double getTasaInteresPunitorio() {
        return this.utasaInteresPunitorio;
    }

    public boolean getCancelacion() {
        return this.bcancelacion;
    }

    public int getIdTipoCredito() {
        return this.iidTipoCredito;
    }

    public String getTipoCredito() {
        return this.stipoCredito;
    }

    public int getIdEntidad() {
        return this.iidEntidad;
    }

    public String getEntidad() {
        return this.sentidad;
    }

    public java.util.Date getFechaVencimiento() {
        return this.dfechaVencimiento;
    }

    public double getMontoTotal() {
        return this.umontoTotal;
    }

    public double getCobro() {
        return this.ucobro;
    }

    public double getAtraso() {
        return this.uatraso;
    }

    public double getActual() {
        return this.uactual;
    }

    public double getSaldo() {
        return this.usaldo;
    }

    public double getCuota() {
        return this.ucuota;
    }

    public double getExoneracion() {
        return this.uexoneracion;
    }

    public double getInteresMoratorio() {
        return this.uinteresMoratorio;
    }

    public double getInteresPunitorio() {
        return this.uinteresPunitorio;
    }

    public String getTabla() {
        return this.stabla;
    }

    public String getRubro() {
        return this.srubro;
    }

    public int getDiaAtraso() {
        return this.idiaAtraso;
    }

    public int getEstadoRegistro() {
        return this.iestadoRegistro;
    }

    /*public String getConsulta(java.util.Date dfecha, int iidSocio, int iidMutual, int iidAporteCapital, int iidSolidaridad, int iidCuotaSocial, int iidFondoPrevision, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, int iidInteresMoratorio, int iidInteresPunitorio, double utasaInteresPunitorio, int iidInteresPrestamo, int iidEstadoOrdenCredito, int iidEstadoPrestamo, String sorden) {
        return "SELECT * FROM (" +
                   "SELECT 'P' AS tipo, mo.id AS idmovimiento, c.descripcion||' ('||e.descripcion||')' AS denominacion, mo.idcuenta, c.descripcion AS cuenta, mo.identidad, e.descripcion AS entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "0.0 AS tasainteres, 0.0 AS tasainterespunitorio, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.montoaprobado AS montototal, mo.idtipocredito, tc.descripcionbreve AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoOrdenCredito+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito = tc.id "+ // ORDEN DE CREDITO
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'P' AS tipo, mo.id AS idmovimiento, c.descripcion||' '||tc.descripcionbreve AS denominacion, mo.idcuenta, c.descripcion AS cuenta, mo.identidad, e.descripcion AS entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres "+
                       "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                       "     ELSE mo.tasainteres END AS tasainteres, "+
                       "CASE WHEN mo.tasainteres=0 THEN e.tasainteres*"+utasaInteresPunitorio+"/100 "+
                       "     WHEN mo.tasainteres>5 THEN (mo.tasainteres/mo.plazoaprobado)*"+utasaInteresPunitorio+"/100 "+
                       "     ELSE mo.tasainteres*"+utasaInteresPunitorio+"/100 END AS tasainterespunitorio, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.montoaprobado AS montototal, mo.idtipocredito, tc.descripcionbreve AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoPrestamo+" AND identidad="+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito = tc.id "+ // PRESTAMO MUTUAL
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'P' AS tipo, mo.id AS idmovimiento, c.descripcion||' '||tc.descripcionbreve AS denominacion, "+iidInteresPrestamo+" AS idcuenta, c.descripcion AS cuenta, mo.identidad, e.descripcion AS entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "0.0 AS tasainteres, 0.0 AS tasainterespunitorio, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, 0 AS montototal, mo.idtipocredito, tc.descripcionbreve AS tipocredito, dop.fechavencimiento, dop.montointeres AS cuota, dop.saldointeres AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoPrestamo+" AND identidad="+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=mo.id "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito=tc.id "+ // INTERES PRESTAMO
                       "LEFT JOIN cuenta AS c ON "+iidInteresPrestamo+"=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'P' AS tipo, mo.id AS idmovimiento, c.descripcion||' '||tc.descripcionbreve||' ('||e.descripcion||')' AS denominacion, mo.idcuenta, c.descripcion AS cuenta, mo.identidad, e.descripcion AS entidad, dop.tabla AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                       "0.0 AS tasainteres, 0.0 AS tasainterespunitorio, "+
                       "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.montoaprobado AS montototal, mo.idtipocredito, tc.descripcionbreve AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM movimiento WHERE COALESCE(cancelado='FALSE', cancelado ISNULL) AND idsocio="+iidSocio+" AND idestado="+iidEstadoPrestamo+" AND identidad<>"+iidMutual+") AS mo LEFT JOIN detalleoperacion AS dop ON mo.id=dop.idmovimiento "+
                       "LEFT JOIN tipocredito tc ON mo.idtipocredito = tc.id "+ // PRESTAMO FINANCIERA
                       "LEFT JOIN cuenta AS c ON mo.idcuenta=c.id "+
                       "LEFT JOIN entidad AS e ON mo.identidad=e.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                       "WHERE dop.tabla='mo' "+
                   "UNION " +
                   "SELECT 'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS denominacion, "+iidInteresMoratorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS cuenta, 0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, 0 AS montototal, 0 AS idtipocredito, '' AS tipocredito, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo, '' AS rubro, 0 AS numerocuota "+
                   "UNION " +
                   "SELECT 'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS denominacion, "+iidInteresPunitorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS cuenta, 0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, 0 AS montototal, 0 AS idtipocredito, '' AS tipocredito, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo, '' AS rubro, 0 AS numerocuota "+
                   "UNION " +
                   "SELECT 'O' AS tipo, ap.id AS idmovimiento, c.descripcion||' ('||pa.descripcion||')' AS denominacion, ap.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, dop.tabla, ap.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, ap.plazo, FALSE AS cancelacion, ap.numerooperacion, 0 AS numerosolicitud, ap.importe*ap.plazo AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM ahorroprogramado WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoAhorro+") AS ap LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=ap.id "+
                       "LEFT JOIN cuenta AS c ON ap.idcuenta=c.id "+
                       "LEFT JOIN planahorroprogramado AS pa ON ap.idplan=pa.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=ap.id AND cr.tabla='ap' "+
                       "WHERE dop.tabla='ap' "+
                   "UNION " +
                   "SELECT 'O' AS tipo, js.id AS idmovimiento, c.descripcion AS denominacion, js.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, dop.tabla, js.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 0 AS plazo, FALSE AS cancelacion, js.numerooperacion, 0 AS numerosolicitud, js.importetitular+js.importeadherente*js.cantidadadherente AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM fondojuridicosepelio WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoFondo+") AS js LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=js.id "+
                       "LEFT JOIN cuenta AS c ON js.idcuenta=c.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=js.id AND cr.tabla='js' "+
                       "WHERE dop.tabla='js' "+
                   "UNION " +
                   "SELECT 'O' AS tipo, of.id AS idmovimiento, c.descripcion AS denominacion, of.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, dop.tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, of.importe AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM operacionfija WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoOperacion+" AND idcuenta<>"+iidAporteCapital+" AND idcuenta<>"+iidSolidaridad+" AND idcuenta<>"+iidCuotaSocial+" AND idcuenta<>"+iidFondoPrevision+") AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                       "WHERE dop.tabla='of' "+
                   "UNION " +
                   "SELECT 'A' AS tipo, of.id AS idmovimiento, c.descripcion AS denominacion, of.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, dop.tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, of.importe AS montototal, 0 AS idtipocredito, '' AS tipocredito, dop.fechavencimiento, dop.montocapital AS cuota, dop.saldocapital AS saldo, "+
                       "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                       "     ELSE cr.giraduriabreve||' '||cr.rubrobreve END AS rubro, dop.numerocuota "+
                       "FROM (SELECT * FROM operacionfija WHERE idsocio="+iidSocio+" AND idestado="+iidEstadoOperacion+" AND (idcuenta="+iidAporteCapital+" OR idcuenta="+iidSolidaridad+" OR idcuenta="+iidCuotaSocial+" OR idcuenta="+iidFondoPrevision+")) AS of LEFT JOIN detalleoperacion AS dop ON dop.idmovimiento=of.id "+
                       "LEFT JOIN cuenta AS c ON of.idcuenta=c.id "+
                       "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                       "WHERE dop.tabla='of' "+
                   "UNION " +
                   "SELECT 'O' AS tipo, ex.id AS idmovimiento, c.descripcion AS denominacion, ex.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, 'ce' AS tabla, ex.fecha AS fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 0 AS plazo, FALSE AS cancelacion, ex.id, 0 AS numerosolicitud, ex.monto AS montototal, 0 AS idtipocredito, '' AS tipocredito, doe.fechavencimiento, doe.montocapital AS cuota, doe.saldocapital AS saldo, '' AS rubro, doe.numerocuota "+
                       "FROM (SELECT * FROM operacionexterna WHERE idsocio="+iidSocio+") AS ex LEFT JOIN detalleoperacionexterno AS doe ON doe.idmovimiento=ex.id "+
                       "LEFT JOIN cuenta AS c ON ex.idcuenta=c.id "+
                       "WHERE TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"'"+
               ") AS detalle "+sorden;
    }*/
    
    public String getConsultaX(java.util.Date dfecha, int iidSocio, int iidMutual, int iidAporteCapital, int iidSolidaridad, int iidCuotaSocial, int iidFondoPrevision, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, int iidInteresMoratorio, int iidInteresPunitorio, double utasaInteresPunitorio, int iidInteresPrestamo, int iidEstadoOrdenCredito, int iidEstadoPrestamo, String sorden) {
        return "SELECT ope.*, dop.numerocuota, dop.fechavencimiento, "+
            "CASE WHEN ope.idcuenta="+iidInteresPrestamo+" THEN dop.montointeres "+
            "     ELSE dop.montocapital "+
            "END AS cuota, "+
            "CASE WHEN ope.idcuenta="+iidInteresPrestamo+" THEN dop.saldointeres "+
            "     ELSE dop.saldocapital "+
            "END AS saldo FROM("+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' ('||en.descripcion||')' AS denominacion, mo.idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, "+
                "mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoOrdenCredito+" "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve AS denominacion, mo.idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "CASE WHEN mo.tasainteres=0 THEN en.tasainteres  "+
                "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                "     ELSE mo.tasainteres END AS tasainteres, "+
                "CASE WHEN mo.tasainteres=0 THEN en.tasainteres*"+utasaInteresPunitorio+"/100 "+
                "     WHEN mo.tasainteres>5 THEN (mo.tasainteres/mo.plazoaprobado)*"+utasaInteresPunitorio+"/100 "+
                "     ELSE mo.tasainteres*"+utasaInteresPunitorio+"/100 END AS tasainterespunitorio, "+
                "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoPrestamo+" AND mo.identidad="+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve AS denominacion, "+iidInteresPrestamo+" AS idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, "+
                "mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, 0 AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON "+iidInteresPrestamo+"=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoPrestamo+" AND mo.identidad="+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve||' ('||en.descripcion||')' AS denominacion, "+
                "mo.idcuenta, cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, "+
                "mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoPrestamo+" AND mo.identidad<>"+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, ap.id AS idmovimiento, cu.descripcion||' ('||pa.descripcion||')' AS denominacion, ap.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'ap' AS tabla, ap.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, ap.plazo, "+
                "FALSE AS cancelacion, ap.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, ap.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM ahorroprogramado AS ap "+
                "LEFT JOIN planahorroprogramado AS pa ON ap.idplan=pa.id "+
                "LEFT JOIN socio AS so ON ap.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON ap.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=ap.id AND cr.tabla='ap' "+
                "WHERE ap.idsocio="+iidSocio+" AND ap.idestado="+iidEstadoAhorro+" "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, of.id AS idmovimiento, cu.descripcion AS denominacion, of.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, "+
                "of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, of.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                "WHERE of.idsocio="+iidSocio+" AND of.idestado="+iidEstadoOperacion+" AND of.idcuenta<>"+iidAporteCapital+" "+
                "AND of.idcuenta<>"+iidSolidaridad+" AND of.idcuenta<>"+iidCuotaSocial+" AND of.idcuenta<>"+iidFondoPrevision+" "+
                //"AND of.fechavencimiento>="+iidSolidaridad+" "+
            "UNION "+
            "SELECT "+
                "'A' AS tipo, of.id AS idmovimiento, cu.descripcion AS denominacion, of.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, "+
                "of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, of.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                "WHERE of.idsocio="+iidSocio+" AND of.idestado="+iidEstadoOperacion+" AND (of.idcuenta="+iidAporteCapital+" "+
                "OR of.idcuenta="+iidSolidaridad+" OR of.idcuenta="+iidCuotaSocial+" OR of.idcuenta="+iidFondoPrevision+") "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, js.id AS idmovimiento, cu.descripcion AS denominacion, js.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'js' AS tabla, js.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, "+
                "0 AS plazo, FALSE AS cancelacion, js.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, "+
                "js.importetitular+js.importeadherente*js.cantidadadherente AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM fondojuridicosepelio AS js "+
                "LEFT JOIN socio AS so ON js.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON js.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=js.id AND cr.tabla='js' "+
                "WHERE js.idsocio="+iidSocio+" AND js.idestado="+iidEstadoFondo+" "+
            ") AS ope "+
            "INNER JOIN detalleoperacion AS dop ON dop.idmovimiento=ope.idmovimiento AND dop.tabla=ope.tabla "+
            //int//"UNION "+
            //int//"SELECT "+
            //int//    "'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS denominacion, "+
            //int//    ""+iidInteresMoratorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS cuenta, "+
            //int//    "0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, "+
            //int//    "0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, "+
            //int//    "0 AS idtipocredito, '' AS tipocredito, 0 AS montototal, '' AS rubro, 0 AS numerocuota, "+
            //int//    ""+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo "+
            //int//"UNION "+
            //int//"SELECT "+
            //int//    "'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS denominacion, "+
            //int//    ""+iidInteresPunitorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS cuenta, "+
            //int//    "0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, "+
            //int//    "0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, "+
            //int//    "0 AS idtipocredito, '' AS tipocredito, 0 AS montototal, '' AS rubro, 0 AS numerocuota, "+
            //int//    ""+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo "+
            "UNION " +
            "SELECT "+
                "'O' AS tipo, ex.id AS idmovimiento, c.descripcion AS denominacion, ex.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, "+
                "'ce' AS tabla, ex.fecha AS fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 0 AS plazo, FALSE AS cancelacion, "+
                "ex.id AS numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, ex.monto AS montototal, '' AS rubro, "+
                "doe.numerocuota, doe.fechavencimiento, doe.montocapital AS cuota, doe.saldocapital AS saldo "+
                "FROM (SELECT * FROM operacionexterna WHERE idsocio="+iidSocio+") AS ex "+
                "LEFT JOIN detalleoperacionexterno AS doe ON doe.idmovimiento=ex.id "+
                "LEFT JOIN cuenta AS c ON ex.idcuenta=c.id "+
                "WHERE TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' "+
            sorden;
    }
    
    public String getConsultaX2(java.util.Date dfecha, int iidSocio, int iidMutual, int iidAporteCapital, int iidSolidaridad, int iidCuotaSocial, int iidFondoPrevision, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, int iidInteresMoratorio, int iidInteresPunitorio, double utasaInteresPunitorio, int iidInteresPrestamo, int iidEstadoOrdenCredito, int iidEstadoPrestamo, String sorden) {
        return "SELECT ope.*, dop.numerocuota, dop.fechavencimiento, "+
            "CASE WHEN ope.idcuenta="+iidInteresPrestamo+" THEN dop.montointeres "+
            "     ELSE dop.montocapital "+
            "END AS cuota, "+
            "CASE WHEN ope.idcuenta="+iidInteresPrestamo+" THEN dop.saldointeres "+
            "     ELSE dop.saldocapital "+
            "END AS saldo FROM("+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' ('||en.descripcion||')' AS denominacion, mo.idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, "+
                "mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoOrdenCredito+" "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve AS denominacion, mo.idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "CASE WHEN mo.tasainteres=0 THEN en.tasainteres  "+
                "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                "     ELSE mo.tasainteres END AS tasainteres, "+
                "CASE WHEN mo.tasainteres=0 THEN en.tasainteres*"+utasaInteresPunitorio+"/100 "+
                "     WHEN mo.tasainteres>5 THEN (mo.tasainteres/mo.plazoaprobado)*"+utasaInteresPunitorio+"/100 "+
                "     ELSE mo.tasainteres*"+utasaInteresPunitorio+"/100 END AS tasainterespunitorio, "+
                "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoPrestamo+" AND mo.identidad="+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve AS denominacion, "+iidInteresPrestamo+" AS idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, "+
                "mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, 0 AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON "+iidInteresPrestamo+"=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoPrestamo+" AND mo.identidad="+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve||' ('||en.descripcion||')' AS denominacion, "+
                "mo.idcuenta, cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, "+
                "mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoPrestamo+" AND mo.identidad<>"+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, ap.id AS idmovimiento, cu.descripcion||' ('||pa.descripcion||')' AS denominacion, ap.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'ap' AS tabla, ap.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, ap.plazo, "+
                "FALSE AS cancelacion, ap.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, ap.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM ahorroprogramado AS ap "+
                "LEFT JOIN planahorroprogramado AS pa ON ap.idplan=pa.id "+
                "LEFT JOIN socio AS so ON ap.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON ap.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=ap.id AND cr.tabla='ap' "+
                "WHERE ap.idsocio="+iidSocio+" AND ap.idestado="+iidEstadoAhorro+" "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, of.id AS idmovimiento, cu.descripcion AS denominacion, of.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, "+
                "of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, of.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id AND of.idcuenta<>22 "+ // no debe ser del tipo Seguro Medico
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                "WHERE of.idsocio="+iidSocio+" AND of.idestado="+iidEstadoOperacion+" AND of.idcuenta<>"+iidAporteCapital+" "+
                "AND of.idcuenta<>"+iidSolidaridad+" AND of.idcuenta<>"+iidCuotaSocial+" AND of.idcuenta<>"+iidFondoPrevision+" "+
                "AND of.idcuenta<>85 AND of.idcuenta<>17 "+
            "UNION "+
            "SELECT "+
                "'A' AS tipo, of.id AS idmovimiento, cu.descripcion AS denominacion, of.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, "+
                "of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, of.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                "WHERE of.idsocio="+iidSocio+" AND of.idestado="+iidEstadoOperacion+" AND (of.idcuenta="+iidAporteCapital+" "+
                "OR of.idcuenta="+iidSolidaridad+" OR of.idcuenta="+iidCuotaSocial+" OR of.idcuenta="+iidFondoPrevision+") "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, js.id AS idmovimiento, cu.descripcion AS denominacion, js.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'js' AS tabla, js.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, "+
                "0 AS plazo, FALSE AS cancelacion, js.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, "+
                "js.importetitular+js.importeadherente*js.cantidadadherente AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM fondojuridicosepelio AS js "+
                "LEFT JOIN socio AS so ON js.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON js.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=js.id AND cr.tabla='js' "+
                "WHERE js.idsocio="+iidSocio+" AND js.idestado="+iidEstadoFondo+" "+
            ") AS ope "+
            "INNER JOIN detalleoperacion AS dop ON dop.idmovimiento=ope.idmovimiento AND dop.tabla=ope.tabla "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS denominacion, "+
                ""+iidInteresMoratorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, "+
                "0 AS idtipocredito, '' AS tipocredito, 0 AS montototal, '' AS rubro, 0 AS numerocuota, "+
                ""+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS denominacion, "+
                ""+iidInteresPunitorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, "+
                "0 AS idtipocredito, '' AS tipocredito, 0 AS montototal, '' AS rubro, 0 AS numerocuota, "+
                ""+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo "+
            "UNION " +
            "SELECT "+
                "'O' AS tipo, ex.id AS idmovimiento, c.descripcion AS denominacion, ex.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, "+
                "'ce' AS tabla, ex.fecha AS fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 0 AS plazo, FALSE AS cancelacion, "+
                "ex.id AS numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, ex.monto AS montototal, '' AS rubro, "+
                "doe.numerocuota, doe.fechavencimiento, doe.montocapital AS cuota, doe.saldocapital AS saldo "+
                "FROM (SELECT * FROM operacionexterna WHERE idsocio="+iidSocio+") AS ex "+
                "LEFT JOIN detalleoperacionexterno AS doe ON doe.idmovimiento=ex.id "+
                "LEFT JOIN cuenta AS c ON ex.idcuenta=c.id "+
                "WHERE TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' "+
            sorden;
    }
    
    public String getConsultaGarantia(String smovimiento, String scedula) {
        if (smovimiento.isEmpty()) smovimiento = "0=1";
        return "SELECT 'Ga' AS tipo, ga.cedula, ga.nombre, ga.apellido, mo.numerooperacion, 0 AS saldo "+
            "FROM movimientogarante AS mg "+
            "LEFT JOIN garante AS ga ON mg.idgarante=ga.id "+
            "LEFT JOIN movimiento AS mo ON mg.idmovimiento=mo.id "+
            "WHERE 0=0 AND ("+smovimiento+") "+
            "UNION "+
            "SELECT 'Co' AS tipo, so.cedula, so.nombre, so.apellido, mo.numerooperacion, SUM(dop.saldocapital+dop.saldointeres) AS saldo "+
            "FROM movimientogarante AS mg "+
            "LEFT JOIN garante AS ga ON mg.idgarante=ga.id "+
            "LEFT JOIN movimiento AS mo ON mg.idmovimiento=mo.id "+
            "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
            "LEFT JOIN detalleoperacion AS dop on mo.id=dop.idmovimiento AND dop.tabla='mo' "+
            "WHERE mg.idgarante=(SELECT id FROM garante WHERE cedula='"+scedula+"') "+
            "GROUP BY tipo, so.cedula, so.nombre, so.apellido, mo.numerooperacion ";
    }
    
    public String getConsultaRegional(java.util.Date dfecha, int iidSocio, int iidMutual, int iidAporteCapital, int iidSolidaridad, int iidCuotaSocial, int iidFondoPrevision, int iidEstadoAhorro, int iidEstadoOperacion, int iidEstadoFondo, int iidInteresMoratorio, int iidInteresPunitorio, double utasaInteresPunitorio, int iidInteresPrestamo, int iidEstadoOrdenCredito, int iidEstadoPrestamo, String sorden) {
        return "SELECT ope.*, dop.num nberocuota, dop.fechavencimiento, "+
            "CASE WHEN ope.idcuenta="+iidInteresPrestamo+" THEN dop.montointeres "+
            "     ELSE dop.montocapital "+
            "END AS cuota, "+
            "CASE WHEN ope.idcuenta="+iidInteresPrestamo+" THEN dop.saldointeres "+
            "     ELSE dop.saldocapital "+
            "END AS saldo FROM("+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' ('||en.descripcion||')' AS denominacion, mo.idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, "+
                "mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoOrdenCredito+" "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve AS denominacion, mo.idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "CASE WHEN mo.tasainteres=0 THEN en.tasainteres  "+
                "     WHEN mo.tasainteres>5 THEN mo.tasainteres/mo.plazoaprobado "+
                "     ELSE mo.tasainteres END AS tasainteres, "+
                "CASE WHEN mo.tasainteres=0 THEN en.tasainteres*"+utasaInteresPunitorio+"/100 "+
                "     WHEN mo.tasainteres>5 THEN (mo.tasainteres/mo.plazoaprobado)*"+utasaInteresPunitorio+"/100 "+
                "     ELSE mo.tasainteres*"+utasaInteresPunitorio+"/100 END AS tasainterespunitorio, "+
                "mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoPrestamo+" AND mo.identidad="+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve AS denominacion, "+iidInteresPrestamo+" AS idcuenta, "+
                "cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, "+
                "mo.numerosolicitud, mo.idtipocredito, tc.descripcionbreve AS tipocredito, 0 AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON "+iidInteresPrestamo+"=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoPrestamo+" AND mo.identidad="+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, mo.id AS idmovimiento, cu.descripcion||' '||tc.descripcionbreve||' ('||en.descripcion||')' AS denominacion, "+
                "mo.idcuenta, cu.descripcion AS cuenta, mo.identidad, en.descripcion AS entidad, 'mo' AS tabla, mo.fechaaprobado AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, mo.plazoaprobado AS plazo, mo.cancelacion, mo.numerooperacion, mo.numerosolicitud, "+
                "mo.idtipocredito, tc.descripcionbreve AS tipocredito, mo.montoaprobado AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM movimiento AS mo "+
                "LEFT JOIN socio AS so ON mo.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "+
                "LEFT JOIN entidad AS en ON mo.identidad=en.id "+
                "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=mo.id AND cr.tabla='mo' "+
                "WHERE mo.idsocio="+iidSocio+" AND mo.idestado="+iidEstadoPrestamo+" AND mo.identidad<>"+iidMutual+" "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, ap.id AS idmovimiento, cu.descripcion||' ('||pa.descripcion||')' AS denominacion, ap.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'ap' AS tabla, ap.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, ap.plazo, "+
                "FALSE AS cancelacion, ap.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, ap.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM ahorroprogramado AS ap "+
                "LEFT JOIN planahorroprogramado AS pa ON ap.idplan=pa.id "+
                "LEFT JOIN socio AS so ON ap.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON ap.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=ap.id AND cr.tabla='ap' "+
                "WHERE ap.idsocio="+iidSocio+" AND ap.idestado="+iidEstadoAhorro+" "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, of.id AS idmovimiento, cu.descripcion AS denominacion, of.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, "+
                "of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, of.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                "WHERE of.idsocio="+iidSocio+" AND of.idestado="+iidEstadoOperacion+" AND of.idcuenta<>"+iidAporteCapital+" "+
                "AND of.idcuenta<>"+iidSolidaridad+" AND of.idcuenta<>"+iidCuotaSocial+" AND of.idcuenta<>"+iidFondoPrevision+" "+
            "UNION "+
            "SELECT "+
                "'A' AS tipo, of.id AS idmovimiento, cu.descripcion AS denominacion, of.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, of.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, "+
                "of.plazo, FALSE AS cancelacion, of.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, of.importe AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM operacionfija AS of "+
                "LEFT JOIN socio AS so ON of.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON of.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=of.id AND cr.tabla='of' "+
                "WHERE of.idsocio="+iidSocio+" AND of.idestado="+iidEstadoOperacion+" AND (of.idcuenta="+iidAporteCapital+" "+
                "OR of.idcuenta="+iidSolidaridad+" OR of.idcuenta="+iidCuotaSocial+" OR of.idcuenta="+iidFondoPrevision+") "+
            "UNION "+
            "SELECT "+
                "'O' AS tipo, js.id AS idmovimiento, cu.descripcion AS denominacion, js.idcuenta, cu.descripcion AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'js' AS tabla, js.fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, "+
                "0 AS plazo, FALSE AS cancelacion, js.numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, "+
                "js.importetitular+js.importeadherente*js.cantidadadherente AS montototal, "+
                "CASE WHEN cr.giraduriabreve ISNULL THEN '' "+
                "     ELSE cr.giraduriabreve||' '||cr.rubrobreve "+
                "END AS rubro "+
                "FROM fondojuridicosepelio AS js "+
                "LEFT JOIN socio AS so ON js.idsocio=so.id "+
                "LEFT JOIN cuenta AS cu ON js.idcuenta=cu.id "+
                "LEFT JOIN vwcambiorubro1 AS cr ON cr.idoperacion=js.id AND cr.tabla='js' "+
                "WHERE js.idsocio="+iidSocio+" AND js.idestado="+iidEstadoFondo+" "+
            ") AS ope "+
                "INNER JOIN detalleoperacion AS dop ON dop.idmovimiento=ope.idmovimiento AND dop.tabla=ope.tabla "+
                "UNION "+
                "SELECT "+
                "'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS denominacion, "+
                ""+iidInteresMoratorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresMoratorio+") AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, "+
                "0 AS idtipocredito, '' AS tipocredito, 0 AS montototal, '' AS rubro, 0 AS numerocuota, "+
                ""+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo "+
            "UNION "+
            "SELECT "+
                "'P' AS tipo, 0 AS idmovimiento, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS denominacion, "+
                ""+iidInteresPunitorio+" AS idcuenta, (SELECT descripcion FROM cuenta WHERE id="+iidInteresPunitorio+") AS cuenta, "+
                "0 AS identidad, '' AS entidad, 'of' AS tabla, "+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechaoperacion, "+
                "0 AS tasainteres, 0 AS tasainterespunitorio, 1 AS plazo, FALSE AS cancelacion, 0 AS numerooperacion, 0 AS numerosolicitud, "+
                "0 AS idtipocredito, '' AS tipocredito, 0 AS montototal, '' AS rubro, 0 AS numerocuota, "+
                ""+utilitario.utiFecha.getFechaGuardado(dfecha)+" AS fechavencimiento, 0 AS cuota, 0 AS saldo "+
            "UNION " +
            "SELECT "+
                "'O' AS tipo, ex.id AS idmovimiento, c.descripcion AS denominacion, ex.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, "+
                "'ce' AS tabla, ex.fecha AS fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 0 AS plazo, FALSE AS cancelacion, "+
                "ex.id AS numerooperacion, 0 AS numerosolicitud, 0 AS idtipocredito, '' AS tipocredito, ex.monto AS montototal, '' AS rubro, "+
                "doe.numerocuota, doe.fechavencimiento, doe.montocapital AS cuota, doe.saldocapital AS saldo "+
                "FROM (SELECT * FROM operacionexterna WHERE idsocio="+iidSocio+") AS ex "+
                "LEFT JOIN detalleoperacionexterno AS doe ON doe.idmovimiento=ex.id "+
                "LEFT JOIN cuenta AS c ON ex.idcuenta=c.id "+
                "WHERE TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' "+
            sorden;
    }
    
    public String getConsultaFuncionarioMSP(java.util.Date dfecha, String scedula, String sorden) {
        return "SELECT 'O' AS tipo, ex.id AS idmovimiento, c.descripcion AS denominacion, ex.idcuenta, c.descripcion AS cuenta, 0 AS identidad, '' AS entidad, 'ce' AS tabla, ex.fecha AS fechaoperacion, 0 AS tasainteres, 0 AS tasainterespunitorio, 0 AS plazo, FALSE AS cancelacion, ex.id, 0 AS numerosolicitud, ex.monto AS montototal, 0 AS idtipocredito, '' AS tipocredito, doe.fechavencimiento, doe.montocapital AS cuota, doe.saldocapital AS saldo, '' AS rubro, doe.numerocuota "+
            "FROM (SELECT * FROM operacionexterna WHERE cedula='"+scedula+"') AS ex LEFT JOIN detalleoperacionexterno AS doe ON doe.idmovimiento=ex.id "+
            "LEFT JOIN cuenta AS c ON ex.idcuenta=c.id "+
            "WHERE TO_CHAR(doe.periodo,'YYYYMM00')='"+utilitario.utiFecha.getAM(dfecha)+"' "+sorden;
    }
    
    public String getConsultaEstado(String sfiltro, String sorden) {
        return "SELECT * FROM detalleoperacion WHERE "+sfiltro+sorden;
    }
    
    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        return lst;
    }

}
