/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class entcPresupuesto {

    private int iid;
    private int iidProveedor;
    private String sproveedor;
    private java.util.Date dfecha;
    private java.util.Date dfechaIngreso;
    private int iidMoneda;
    private String smoneda;
    private int ivalidez;
    private double utotalPresupuesto;
    private double ucambio;
    private double utotalGs;
    private short hestadoRegistro;
    private boolean bconfirma;
    private String smensaje;

    public static final String TEXTO_ID = "Id (no editable)";
    public static final String TEXTO_PROVEEDOR = "Proveedor del artículo (no vacía)";
    public static final String TEXTO_FECHA = "Fecha del presupuesto (no vacía)";
    public static final String TEXTO_FECHA_INGRESO = "Fecha de carga del Presupuesto (no editable)";
    public static final String TEXTO_TOTAL_PRESUPUESTO = "Monto total del presupuesto (no editable)";
    public static final String TEXTO_CAMBIO = "Monto del cambio del día de la moneda (no vacía, 1 si en Gs)";
    public static final String TEXTO_TOTAL_GS = "Monto total del presupuesto en Gs (no editable)";
    public static final String TEXTO_MONEDA = "Moneda en que se presupuesta (no vacía)";
    public static final String TEXTO_VALIDEZ = "Por cuántos días es válida la oferta (no vacía, valor positivo)";
    
    
    public entcPresupuesto() {
        this.iid = 0;
        this.iidProveedor = 0;
        this.sproveedor = "";
        this.dfecha = null;
        this.dfechaIngreso = null;
        this.iidMoneda = 0;
        this.smoneda = "";
        this.ivalidez = 0;
        this.utotalPresupuesto = 0.0;
        this.ucambio = 0.0;
        this.utotalGs = 0.0;
        this.bconfirma = false;
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
    }

    public entcPresupuesto(int iid, int iidProveedor, String sproveedor, java.util.Date dfecha, java.util.Date dfechaIngreso, int iidMoneda,  String smoneda, int ivalidez, double utotalPresupuesto, double ucambio, double utotalGs, boolean bconfirma, short hestadoRegistro) {
        this.iid = iid;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.dfecha = dfecha;
        this.dfechaIngreso = dfechaIngreso;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.ivalidez = ivalidez;
        this.utotalPresupuesto = utotalPresupuesto;
        this.ucambio = ucambio;
        this.utotalGs = utotalGs;
        this.bconfirma = bconfirma;
        this.hestadoRegistro = hestadoRegistro;
    }

    public void setEntidad(int iid, int iidProveedor, String sproveedor, java.util.Date dfecha, java.util.Date dfechaIngreso, int iidMoneda,  String smoneda, int ivalidez, double utotalPresupuesto, double ucambio, double utotalGs, boolean bconfirma, short hestadoRegistro) {
        this.iid = iid;
        this.iidProveedor = iidProveedor;
        this.sproveedor = sproveedor;
        this.dfecha = dfecha;
        this.dfechaIngreso = dfechaIngreso;
        this.iidMoneda = iidMoneda;
        this.smoneda = smoneda;
        this.ivalidez = ivalidez;
        this.utotalPresupuesto = utotalPresupuesto;
        this.ucambio = ucambio;
        this.utotalGs = utotalGs;
        this.bconfirma = bconfirma;
        this.hestadoRegistro = hestadoRegistro;
    }

    public entcPresupuesto copiar(entcPresupuesto destino) {
        destino.setEntidad(this.getId(), this.getIdProveedor(), this.getProveedor(), this.getFecha(), this.getFechaIngreso(), this.getIdMoneda(), this.getMoneda(), this.getValidez(), this.getTotalPresupuesto(), this.getCambio(), this.getTotalGs(), this.getConfirma(), this.getEstadoRegistro());
        return destino;
    }
    
    public entcPresupuesto cargar(java.sql.ResultSet rs) {
        try { this.setId(rs.getInt("id")); }
        catch(Exception e) {}
        try { this.setIdProveedor(rs.getInt("idproveedor")); }
        catch(Exception e) {}
        try { this.setProveedor(rs.getString("proveedor")); }
        catch(Exception e) {}
        try { this.setFecha(rs.getDate("fecha")); }
        catch(Exception e) {}
        try { this.setFechaIngreso(rs.getDate("fechaingreso")); }
        catch(Exception e) {}
        try { this.setTotalPresupuesto(rs.getDouble("totalpresupuesto")); }
        catch(Exception e) {}
        try { this.setTotalGs(rs.getDouble("totalgs")); }
        catch(Exception e) {}
        try { this.setCambio(rs.getDouble("cambio")); }
        catch(Exception e) {}
        try { this.setIdMoneda(rs.getInt("idmoneda")); }
        catch(Exception e) {}
        try { this.setMoneda(rs.getString("moneda")); }
        catch(Exception e) {}
        try { this.setValidez(rs.getInt("validez")); }
        catch(Exception e) {}
        return this;
    }
    
    public void setId(int iid) {
        this.iid = iid;
    }

       public void setIdProveedor(int iidProveedor) {
        this.iidProveedor = iidProveedor;
    }
    
    public void setProveedor(String sproveedor) {
        this.sproveedor = sproveedor;
    }

    public void setFecha(java.util.Date dfecha) {
        this.dfecha = dfecha;
    }

    public void setFechaIngreso(java.util.Date dfechaIngreso) {
        this.dfechaIngreso = dfechaIngreso;
    }

    public void setIdMoneda(int iidMoneda) {
        this.iidMoneda = iidMoneda;
    }

    public void setMoneda(String smoneda) {
        this.smoneda = smoneda;
    }
    
    public void setValidez(int ivalidez) {
        this.ivalidez = ivalidez;
    }
    
    public void setTotalPresupuesto(double utotalPresupuesto) {
        this.utotalPresupuesto = utotalPresupuesto;
    }
        
    public void setCambio(double ucambio) {
        this.ucambio = ucambio;
    }
        
    public void setTotalGs(double utotalGs) {
        this.utotalGs = utotalGs;
    }
    
    public void setConfirma(boolean bconfirma) {
        this.bconfirma = bconfirma;
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public int getIdProveedor() {
        return this.iidProveedor;
    }
    
    public String getProveedor() {
        return this.sproveedor;
    }

    public java.util.Date getFecha() {
        return this.dfecha;
    }
    
    public java.util.Date getFechaIngreso() {
        return this.dfechaIngreso;
    }

    public int getIdMoneda() {
        return this.iidMoneda;
    }

    public String getMoneda() {
        return this.smoneda;
    }

    public int getValidez() {
        return this.ivalidez;
    }

    public double getTotalPresupuesto() {
        return this.utotalPresupuesto;
    }

    public double getCambio() {
        return this.ucambio;
    }

    public double getTotalGs() {
        return this.utotalGs;
    }
    
    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public boolean getConfirma() {
        return this.bconfirma;
    }

    public String getMensaje() {
        return this.smensaje;
    }

    public boolean esValidoRegistrar() {
        boolean bvalido = true;
        this.smensaje = "";
        if (this.getFecha() == null) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_FECHA;
            bvalido = false;
        }
        if (this.getIdProveedor()<=0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_PROVEEDOR;
            bvalido = false;
        }
        if (this.getValidez()<0) {
            if (!this.smensaje.isEmpty()) this.smensaje += "\n";
            this.smensaje += this.TEXTO_VALIDEZ;
            bvalido = false;
        }
        return bvalido;
    }

    public String getSentencia() {
        return "SELECT compras.presupuesto("+
            this.getId()+","+
            this.getIdProveedor()+","+
            utilitario.utiFecha.getFechaGuardado(this.getFecha())+","+
            utilitario.utiFecha.getFechaGuardado(this.getFechaIngreso())+","+
            this.getTotalPresupuesto()+","+
            this.getIdMoneda()+","+
            this.getValidez()+","+
            this.getCambio()+","+
            this.getTotalGs()+","+
            this.getEstadoRegistro()+")";
    }

    public java.util.ArrayList<generico.entLista> getCampo() {
        java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
        lst.add(new generico.entLista(1, "Fecha Presupuesto", "fecha", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(2, "Nº Presupuesto", "id", new generico.entLista().tipo_numero));
        lst.add(new generico.entLista(3, "Fecha Ingreso", "fechaingreso", new generico.entLista().tipo_fecha));
        lst.add(new generico.entLista(4, "Proveedor", "proveedor", new generico.entLista().tipo_texto));
        return lst;
    }
    
}
