/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entGenericaPersona {
    
    protected int iid;
    protected String snombre;
    protected String sapellido;
    protected String scedula;
    protected short hestadoRegistro;
    protected String smensaje;

    protected entGenericaPersona() {
        this.iid = 0;
        this.snombre = "";
        this.sapellido = "";
        this.scedula = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    protected entGenericaPersona(int iid, String snombre, String sapellido, String scedula, short hestadoRegistro) { // a eliminar
        this.iid = iid;
        this.snombre = utilitario.utiCadena.sacarCaracter(snombre.trim().toUpperCase(), "'", "-");
        this.sapellido = utilitario.utiCadena.sacarCaracter(sapellido.trim().toUpperCase(), "'", "-");
        this.scedula = utilitario.utiCadena.sacarCaracter(scedula, "'", "-");
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setNombre(String snombre) {
        this.snombre = utilitario.utiCadena.sacarCaracter(snombre.trim().toUpperCase(), "'", "-");
    }

    public void setApellido(String sapellido) {
        this.sapellido = utilitario.utiCadena.sacarCaracter(sapellido.trim().toUpperCase(), "'", "-");
    }

    public void setCedula(String scedula) {
        this.scedula = utilitario.utiCadena.sacarCaracter(scedula, "'", "-");
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public String getNombre() {
        return this.snombre;
    }

    public String getApellido() {
        return this.sapellido;
    }

    public String getCedula() {
        return this.scedula;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

}
