/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package generico;

/**
 *
 * @author Informatica
 */
public class vista {
    public static final short vwbarrio1 = 0;              // vista vwbarrio1: todos los datos de barrio (para Mantenimiento)
    public static final short vwbeneficiario1 = 1;        // vista vwbeneficiario1: todos los datos de beneficiario con parentesco (para Mantenimiento)
    public static final short vwcargo1 = 2;               // vista vwcargo1: todos los datos de cargo (para Mantenimiento)
    public static final short vwciudad1 = 3;              // vista vwciudad1: todos los datos de ciudad (para Mantenimiento)
    public static final short vwdepartamento1 = 4;        // vista vwdepartamento1: todos los datos de departamento (para Mantenimiento)
    public static final short vwentidad1 = 5;             // vista vwentidad1: todos los datos de entidad con ciudad, ramo (para Mantenimiento)
    public static final short vwestadocivil1 = 6;         // vista vwestadocivil1: todos los datos de estadocivil (para Mantenimiento)
    public static final short vwfuncionario1 = 7;         // vista vwfuncionario1: todos los datos de funcionario con cargo (para Mantenimiento)
    public static final short vwgiraduria1 = 8;           // vista vwgiraduria1: todos los datos de giraduria (para Mantenimiento)
    public static final short vwinstitucion1 = 9;         // vista vwinstitucion1: todos los datos de institucion con ciudad, regional (para Mantenimiento)
    public static final short vwlugarlaboral1 = 10;       // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwmoneda1 = 11;             // vista vwmoneda1: todos los datos de moneda con pais (para Mantenimiento)
    public static final short vwpais1 = 12;               // vista vwpais1: todos los datos de pais (para Mantenimiento)
    public static final short vwparentesco1 = 13;         // vista vwparentesco1: todos los datos de parentesco (para Mantenimiento)
    public static final short vwprofesion1 = 14;          // vista vwprofesion1: todos los datos de profesion (para Mantenimiento)
    public static final short vwramo1 = 15;               // vista vwramo1: todos los datos de profesion (para Mantenimiento)
    public static final short vwreferenciacomercial1 = 16;// vista vwreferenciacomercial1: todos los datos de referenciacomercial con entidad (para Mantenimiento)
    public static final short vwreferenciapersonal1 = 17; // vista vwreferenciapersonal1: todos los datos de referenciapersonal (para Mantenimiento)
    public static final short vwregional1 = 18;           // vista vwregional1: todos los datos de regional (para Mantenimiento)
    public static final short vwrubro1 = 19;              // vista vwrubro1: todos los datos de rubro (para Mantenimiento)
    public static final short vwsubordinado1 = 20;        // vista vwsubordinado1: todos los datos de subordinado con parentesco (para Mantenimiento)
    public static final short vwmovimiento1 = 21;         // vista vwmovimiento1: todos los datos de movimiento (para SolicitudOrdenCompra)
    public static final short vwsocio1 = 22;              // vista vwsocio1: todos los datos de socio con barrio, ciudad, lugarlaboral, institucion, rubro, regional, ciudad, pais, profesion, giraduria, moneda (para Mantenimiento)
    public static final short vwsocio2 = 23;              // vista vwsocio2: id, cedula, nombre, apellido de socio (para Combobox)
    public static final short vwentidad2 = 24;            // vista vwentidad2: todos los datos de entidad filtrado por comercio (para Combobox)
    public static final short vwautorizador1 = 25;        // vista vwautorizador1: todos los autorizadores (para Mantenimiento)
    public static final short vwpromotor1 = 26;           // vista vwpromotor1: todos los promotores (para Mantenimiento)
    public static final short vwusuario1 = 27;            // vista vwusuario1: todos los usuarios (para Mantenimiento)
    public static final short vwfuncionario2 = 28;        // vista vwfuncionario2: id, nombre y apellido (Mant Usuario, Mant Autorizador, Mant Promotor) 
    public static final short vwsubtipo = 29;             // vista vwfuncionario2: 
    public static final short vwtipo = 30;                // vista vwfuncionario2: 
    public static final short vwgarante1 = 31;            // vista vwfuncionario2: 
    public static final short vwgarante2 = 32;            // vista vwfuncionario2: 
    public static final short vwmovimientogarante1 = 33;  // vista vwmovimientogarante1: todos los movimientos 
    public static final short vwautorizador2 = 34;        // vista vwmovimientogarante1: todos los movimientos 
    public static final short vwdescuento1 = 35;          // vista vwmovimientogarante1: todos los movimientos 
    public static final short vwmovimientodescuento1 = 36;// vista vwmovimientogarante1: todos los movimientos 
    public static final short vwahorroprogramadoplan1 = 37; // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwahorroprogramado1 = 38;   // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwfondojuridicosepelio1 = 39; // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwbeneficiariofondo1 = 40; // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwcuenta1 = 41;             // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwpuntoexpedicion1 = 42;         // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwsucursal1 = 43;         // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwtalonario1 = 44;         // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwperfil1 = 45;         // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwusuario2 = 46;         // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwpermisotalonarioperfil1 = 47;         // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwpermisotalonariousuario1 = 48;         // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwemisortalonario1 = 49;         // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwoperacionfija1 = 50;         // vista vwlugarlaboral1: todos los datos de lugarlaboral con institucion, cargo (para Mantenimiento)
    public static final short vwsocio4 = 51;              // vista vwsocio2: id, cedula, nombre, apellido de socio (para Combobox)
    public static final short vwmovimientoperfil1 = 52;              // vista vwsocio2: id, cedula, nombre, apellido de socio (para Combobox)
    public static final short vwciudad2 = 53;              // vista vwsocio2: id, cedula, nombre, apellido de socio (para Combobox)
    public static final short vwdefecto1 = 54;              // vista vwsocio2: id, cedula, nombre, apellido de socio (para Combobox)
    public static final short vwarticulo1 = 55;              // vista vwsocio2: id, cedula, nombre, apellido de socio (para Combobox)
    public static final short vwunidadmedida1 = 56;              // vista vwsocio2: id, cedula, nombre, apellido de socio (para Combobox)
    public static final short vwentidad3 = 57;              // vista vwsocio2: id, cedula, nombre, apellido de socio (para Combobox)
    public static final short vwperiodo1 = 58;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwboleta1 = 59;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwfuncionariomsp1 = 60;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmovimientoarticulo1 = 61;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwoperacion1 = 62;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtalonario2 = 63;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalletalonario1 = 64;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwgarante3 = 65;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleahorroprogramado = 66;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallemovimiento = 67;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdocumento1 = 68;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallecobertura1 = 69;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcobertura1 = 70;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwsolidaridad1 = 71;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcobertura2 = 72;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallesolidaridad1 = 73;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwverificador1 = 74;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwformulario1 = 75;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmodalidad1 = 76;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwitemcontrol1 = 77;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vweventoformulario1 = 78;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipocredito1 = 79;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleplazo1 = 80;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwentidad4 = 81;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdesautorizacion1 = 82;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipoproducto1 = 83;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwimpuesto1 = 84;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwproducto1 = 85;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwfacturaegreso1 = 86;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallefacturaegreso1 = 87;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwpersona1 = 88;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipovalor1 = 89;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwplanillaingreso1 = 90;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallevalor1 = 91;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwanalisis1 = 92;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwingreso1 = 93;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcajero1 = 94;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdependencia1 = 95;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipocuenta1 = 96;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwsepelio1 = 97;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcasojuridico1 = 98;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcoberturasepelio1 = 99;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwasesor1 = 100;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwjuzgado1 = 101;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwestadojuridico1 = 102;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwprograma1 = 103;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcentrocosto1 = 104;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwllamada1 = 105;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwobjetollamada1 = 106;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwsolucionllamada1 = 107;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtelefono1 = 108;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwatencionsocio1 = 109;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwobjetoatencion1 = 110;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwsolucionatencion1 = 111;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwahorroprogramado2 = 112;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallecasojuridico1 = 113;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleoperacionfija1 = 114;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwbeneficiariosepelio1 = 115;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallesepelio1 = 116;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwsocio3 = 117;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmovimiento2 = 118;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwpermisoperfil1 = 119;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwoperacionmaster = 120;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallecoberturasepelio1 = 121;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleestadosocio1 = 122;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallecancelacion1 = 123;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwbancosucursal1 = 124;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwbanco1 = 125;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwchequeemitido1 = 126;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwreceptorcheque1 = 127;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwfacturaingreso1 = 128;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallefacturaingreso1 = 129;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleingreso1 = 130;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwrecibo1 = 131;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallerecibo1 = 132;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallefondojuridicosepelio = 133;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipollamada1 = 134;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwintermediario1 = 135;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmotivoatencion1 = 136;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwllamadaregional1 = 137;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdestino1 = 138;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmovimientooperacion1 = 139;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcambiorubro1 = 140;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwanalisis2 = 141;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleoperacion = 142;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmotivonota1 = 143;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwnotacredito1 = 144;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallenotacredito1 = 145;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipoplanillalegajo1 = 146;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwplanillalegajo1 = 147;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcontrollegajo1 = 148;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipooperacion1 = 149;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwempresa1 = 150;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwretencion1 = 151;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwconceptocajachica1 = 152;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwplanillacajachica1 = 153;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmovimientocajachica1 = 154;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipodocumentoarchivo1 = 155;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipooperacionarchivo1 = 156;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcontrolarchivo1 = 157;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwplanillamovimiento1 = 158;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleplanillamovimiento1 = 159;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleplanillamovimiento2 = 160;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwplanillaenvio1 = 161;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleplanillaenvio1 = 162;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcierre1 = 163;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcontrollegajo2 = 164;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwplanillalegajo2 = 165;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleplanillalegajo2 = 166;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwplancuenta1 = 167;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcuentatalonario1 = 168;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwordenpago1 = 169;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwrp_ordenpago1 = 170;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdeposito1 = 171;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmovimientocuenta1 = 172;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleordenpago1 = 173;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalledeposito1 = 174;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwbancocuenta1 = 175;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetallemovimientocuenta1 = 176;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcdatipo1 = 177;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcda1 = 178;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcdatitular1 = 179;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcdatipodetalle1 = 180;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcdadetalle1 = 181;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwcdatitulardetalle1 = 182;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmovimiento3 = 183;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwrepresentante1 = 184;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwanalisis3 = 185;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwacuerdoatencion1 = 186;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmovimientocuenta2 = 187;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwordenpago2 = 188;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipomovimiento1 = 189;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwentregacheque1 = 190;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwatenciongeneral1 = 191;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwoperacionexterna1 = 192;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleatencion1 = 193;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwtipoatencion1 = 194;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmotivollamada1 = 195;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwestadogestion1 = 196;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwmotivoagenda1 = 197;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwprioridadagenda1 = 198;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdetalleagenda1 = 199;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwsocio5 = 200;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwsocio6 = 201;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwdepositosocio1 = 202;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwconceptodeposito1 = 203;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwprueba1 = 204;   // vista vwtalonariocuenta1: para emision de cheque de cierto talonario, teniendo el nro de cuenta
    public static final short vwarticulocompra1 = 205;   // vista vwarticulocompra1: todos los datos de articulos en compras y suministros (para Mantenimiento)
    public static final short vwcategoriaarticulo1 = 206;   // vista vwcategoriaarticulo1: todos los datos de categoría de articulos en compras y suministros (para Mantenimiento)
    public static final short vwpresupuesto1 = 207;   // vista vwpresupuesto1: todos los datos de presupuesto de articulos en compras y suministros (para Mantenimiento)
    public static final short vwpresupuestodetalle1 = 208;   // vista vwpresupuestodetalle1: todos los datos de detalle de presupuesto en compras y suministros (para Mantenimiento)
    public static final short vwrp_presupuesto1 = 209;   // vista vwpresupuestodetalle1: todos los datos de detalle de presupuesto en compras y suministros (para Mantenimiento
    public static final short vwordencompra1 = 210;   // vista vwordencompra1: todos los datos de orden de compra en compras y suministros (para Mantenimiento)
    public static final short vwordencompradetalle1 = 211;   // vista vwordencompradetalle1: todos los datos de detalle de orden de compra en compras y suministros (para Mantenimiento)
    public static final short vwrp_ordencompra1 = 212;   // vista vwrp_ordencompra1: todos los datos de orden de compra en compras y suministros (para reporte)
    public static final short vwordenpagocheque1 = 213;   // vista vwordenpagocheque1: todos los datos de orden de pago para impresión de cheques 
    public static final short vwfacturaegresodetalle1 = 214;   // vista vwfacturaegresodetalle1: para detalle de factura egreso en modulo compras
    public static final short vwinventario1 = 215;   // vista vwinventario1: para inventario en modulo compras
    public static final short vwinventariodetalle1 = 216;   // vista vwinventariodetalle1: para detalle de inventario en modulo compras
    public static final short vwpedido1 = 217;   // vista vwpedido1: para pedido de utiles en modulo compras
    public static final short vwpedidodetalle1 = 218;   // vista vwpedidodetalle1: para detalle de pedido en modulo compras
    public static final short vwrp_pedido1 = 219;   // vista vwrp_pedido1: para reporte de pedido en modulo compras
    public static final short vwrp_inventario1 = 220;   // vista vwrp_inventario1: para reporte de inventario en modulo compras
    public static final short vwarticulocompradetalle1 = 221;   // vista vwarticulocompradetalle1: para detalle stock de articulo
    public static final short vwtransferencia1 = 222;   // vista vwarticulocompradetalle1: para detalle stock de articulo
    public static final short vwpersonadetalle1 = 223;   // vista vwpersonadetalle1: para detalle persona
    public static final short vwdevolucion1 = 224;   // vista vwdevolucion1: para devoluciones de articulos
    public static final short vwremision1 = 225;   // vista vwremision1: para remision de documentos al interior
    public static final short vwremisiondetalle1 = 226;   // vista vwremisiondetalle1: para remision de documentos al interior
    public static final short vwenvio1 = 227;   // vista vwenvio1: para remision de documentos al interior
    public static final short vwrp_remision1 = 228;   // vista vwrp_remision1: para reportes de remision de documentos al interior
    public static final short vwrequisito1 = 229;   // vista vwrequisito: para requisito de verificacion
    public static final short vwrequisitooperacion1 = 230;   // vista vwrequisitooperacion1: para requisito de verificacion en operacion
    public static final short vwmovimiento4 = 231;   // vista vwrequisitooperacion1: para requisito de verificacion en operacion
    
    
    public static final String getNombre(int i) {
        Class clase;
        try {
            clase = Class.forName("generico.vista");
            return clase.getDeclaredFields()[i].getName(); 
            //for (java.lang.reflect.Field field : clase.getDeclaredFields()) {
            //    System.out.println(field.getName());
            //}
        } catch (Exception e) { }
        return "";
    }

}
