/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class entGenerica {
    
    protected int iid;
    protected String sdescripcion;
    protected short hestadoRegistro;
    protected String smensaje;
    
    protected entGenerica() {
        this.iid = 0;
        this.sdescripcion = "";
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    protected entGenerica(int iid, String sdescripcion) {
        this.iid = iid;
        this.sdescripcion = sdescripcion.trim();
        this.hestadoRegistro = generico.entConstante.estadoregistro_predeterminado;
        this.smensaje = "";
    }

    protected entGenerica(int iid, String sdescripcion, short hestadoRegistro) {
        this.iid = iid;
        this.sdescripcion = sdescripcion.trim();
        this.hestadoRegistro = hestadoRegistro;
        this.smensaje = "";
    }

    public void setId(int iid) {
        this.iid = iid;
    }

    public void setDescripcion(String sdescripcion) {
        this.sdescripcion = sdescripcion.trim();
    }

    public void setEstadoRegistro(short hestadoRegistro) {
        this.hestadoRegistro = hestadoRegistro;
    }

    public int getId() {
        return this.iid;
    }

    public String getDescripcion() {
        return this.sdescripcion;
    }

    public short getEstadoRegistro() {
        return this.hestadoRegistro;
    }

    public String getMensaje() {
        return this.smensaje;
    }

}
