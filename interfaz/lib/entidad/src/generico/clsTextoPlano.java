/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package generico;

/**
 *
 * @author Informatica
 */
public class clsTextoPlano {

    public clsTextoPlano() {
    }

    public static final void crear(int id) {
        java.io.File carpeta = null;
        java.io.FileWriter fw = null;
        java.io.PrintWriter pw = null;
        try {
            carpeta = new java.io.File("c:\\systemmutual\\");
            carpeta.mkdirs();
            fw = new java.io.FileWriter("c:\\systemmutual\\prioridad.txt");
            pw = new java.io.PrintWriter(fw);
            pw.println(id);
            javax.swing.JOptionPane.showMessageDialog(null, "Prioridad creada para el usuario");
        } catch (Exception e) {
        } finally {
            try {
                if (null != fw) fw.close();
            } catch (Exception e2) { }
        }
    }

    public static final int leer() {
        java.io.File archivo = null;
        java.io.FileReader fr = null;
        java.io.BufferedReader br = null;
        try { // Apertura del fichero y creacion de BufferedReader para poder hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new java.io.File ("c:\\systemmutual\\prioridad.txt");
            fr = new java.io.FileReader(archivo);
            br = new java.io.BufferedReader(fr);
            String prioridad = br.readLine();
            return utilitario.utiNumero.convertirToInt(prioridad.trim());
        } catch(Exception e){
            return 0;
        } finally {
            try {
                if( null != fr ) fr.close();
            } catch (Exception e2) {
                return 0;
            }
        }
    }
    
}
