/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modFormatoFecha extends javax.swing.text.MaskFormatter {

    public modFormatoFecha() throws java.text.ParseException {
        //super ("##/##/## ##:##:##");
        super ("##/##/####");
    }

    //private java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("dd/MM/yy kk:mm:ss");
    private java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("dd/MM/yyyy");
    
    @Override
    public Object stringToValue(String text) throws java.text.ParseException {
        return formato.parseObject(text);
    }
    
    @Override
    public String valueToString(Object value) throws java.text.ParseException {
        if (value instanceof java.util.Date) {
            return formato.format((java.util.Date)value);
        }
        return formato.format(new java.util.Date());
    }
    
}
