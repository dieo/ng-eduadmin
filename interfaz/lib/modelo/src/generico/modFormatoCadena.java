/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modFormatoCadena extends javax.swing.text.PlainDocument {
    
    private javax.swing.JFormattedTextField editor0;
    private javax.swing.JTextArea editor1;
    private javax.swing.JPasswordField editor2;
    private int itipoObjeto;
    private int inumeroMaximoCaracteres;
    private boolean bmayuscula;
    
    public modFormatoCadena(javax.swing.JFormattedTextField editor, int inumeroMaximoCaracteres, boolean bmayuscula) { 
        this.editor0 = editor;
        this.itipoObjeto = 0; // JFormattedTextField
        this.inumeroMaximoCaracteres = inumeroMaximoCaracteres;
        this.bmayuscula = bmayuscula;
    }
    
    public modFormatoCadena(javax.swing.JTextArea editor, int inumeroMaximoCaracteres, boolean bmayuscula) { 
        this.editor1 = editor;
        this.itipoObjeto = 1; // JTextArea
        this.inumeroMaximoCaracteres = inumeroMaximoCaracteres;
        this.bmayuscula = bmayuscula;
    }
    
    public modFormatoCadena(javax.swing.JPasswordField editor, int inumeroMaximoCaracteres, boolean bmayuscula) { 
        this.editor2 = editor;
        this.itipoObjeto = 2; // JFormattedTextField
        this.inumeroMaximoCaracteres = inumeroMaximoCaracteres;
        this.bmayuscula = bmayuscula;
    }
    
    @Override
    public void insertString(int arg0, String arg1, javax.swing.text.AttributeSet arg2) throws javax.swing.text.BadLocationException { 
        if (this.itipoObjeto == 0) {
            if ((this.editor0.getText().length()+arg1.length()) > this.inumeroMaximoCaracteres) {
                return;
            }
        }
        if (this.itipoObjeto == 1) {
            if ((this.editor1.getText().length()+arg1.length()) > this.inumeroMaximoCaracteres) {
                return;
            }
        }
        if (this.itipoObjeto == 2) {
            if ((this.editor2.getText().length()+arg1.length()) > this.inumeroMaximoCaracteres) {
                return;
            }
        }
        if (this.bmayuscula) {
            super.insertString(arg0, arg1.toUpperCase(), arg2);
        } else {
            super.insertString(arg0, arg1, arg2);
        }
    }
    
}
