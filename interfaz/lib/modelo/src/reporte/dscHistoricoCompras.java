/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dscHistoricoCompras implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet facturas;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dscHistoricoCompras(Object mod) {
        this.facturas = (java.sql.ResultSet)mod;
        try {
            this.facturas.last();
            total = this.facturas.getRow()+1;
            this.facturas.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.facturas.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        try {
            if("descripcion".equals(jrCampo.getName())) {
                valor = this.facturas.getString("descripcion");
            } else if("filial".equals(jrCampo.getName())) {
                valor = this.facturas.getString("filial");
            } else if("categoria".equals(jrCampo.getName())) {
                valor = this.facturas.getString("categoria");
            } else if("proveedor".equals(jrCampo.getName())) {
                valor = this.facturas.getString("proveedor");
            } else if("estado".equals(jrCampo.getName())) {
                valor = this.facturas.getString("estadofactura");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = this.facturas.getDate("fechafactura");
            } else if("numerofactura".equals(jrCampo.getName())) {
                valor = this.facturas.getString("numerofactura");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = this.facturas.getDouble("cantidad");
            } else if("costo".equals(jrCampo.getName())) {
                valor = this.facturas.getDouble("costo");
            } else if("total".equals(jrCampo.getName())) {
                valor = this.facturas.getDouble("total");
            } else if("unidad".equals(jrCampo.getName())) {
                valor = this.facturas.getString("unidadmedida");
            }
        } catch (Exception e){            
        }
        return valor;
    }
    
}
