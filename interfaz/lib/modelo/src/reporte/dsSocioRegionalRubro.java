/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsSocioRegionalRubro implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet rs;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsSocioRegionalRubro(Object mod) {
        this.rs = (java.sql.ResultSet)mod;
        try {
            this.rs.last();
            total = this.rs.getRow()+1;
            this.rs.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.rs.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("descripcion".equals(jrCampo.getName())) {
                valor = this.rs.getString("descripcion");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = this.rs.getInt("cantidad");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
