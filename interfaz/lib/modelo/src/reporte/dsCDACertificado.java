/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsCDACertificado implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modCDA mod;
    private int index = -1;

    public dsCDACertificado(Object mod) {
        this.mod = (especifico.modCDA)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        int i=15;
        if("fechainicio".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaInicio();
        } else if("fechafin".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaFin();
        } else if("tasa".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getTasaInteres();
        } else if("plazo".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getDias();
        } else if("monto".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCapitalInicial();
        } else if("montoletras".equals(jrCampo.getName())) {
            String letra = new utilitario.utiLetra().leerNumero(mod.getEntidad(index).getCapitalInicial());
            if (letra.length()<100) letra+=" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
            valor=letra;
        } else if("nombre".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNombre().substring(0, mod.getEntidad(index).getNombre().length()-2);
        } else if("mesinicio".equals(jrCampo.getName())) {
            valor = utilitario.utiFecha.getMeses()[mod.getEntidad(index).getFechaInicio().getMonth()];
        } else if("mesfin".equals(jrCampo.getName())) {
            valor = utilitario.utiFecha.getMeses()[mod.getEntidad(index).getFechaFin().getMonth()];
        } 
        return valor;
    }
    
}
