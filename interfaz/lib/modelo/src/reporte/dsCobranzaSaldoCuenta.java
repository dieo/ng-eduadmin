/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsCobranzaSaldoCuenta implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsCobranzaSaldoCuenta(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("tipo".equals(jrCampo.getName())) {
                valor = listado.getString("tipo");
            } else if("idcuenta".equals(jrCampo.getName())) {
                valor = listado.getInt("idcuenta2");
            } else if("descripcion".equals(jrCampo.getName())) {
                valor = listado.getString("descripcion2");
            } else if("monto".equals(jrCampo.getName())) {
                valor = listado.getDouble("monto");
            } else if("cobrado".equals(jrCampo.getName())) {
                valor = listado.getDouble("cobrado");
            } else if("saldo".equals(jrCampo.getName())) {
                valor = listado.getDouble("monto")-listado.getDouble("cobrado");
            } else if("devengado".equals(jrCampo.getName())) {
                valor = listado.getDouble("devengado");
            } else if("adevengar".equals(jrCampo.getName())) {
                valor = listado.getDouble("adevengar");
            } 
        } catch (Exception e) { }
        return valor;
    }
    
}
