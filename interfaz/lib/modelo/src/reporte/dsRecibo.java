/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsRecibo implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet recibo;
    private java.sql.ResultSet detalle;
    private int index = 0;
    private int totalDetalle = 0;

    public dsRecibo(Object mod, Object mod2) {
        this.recibo = (java.sql.ResultSet)mod;
        this.detalle = (java.sql.ResultSet)mod2;
        try {
            this.detalle.last();
            totalDetalle = this.detalle.getRow()+1;
            this.detalle.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { detalle.next();
        } catch (Exception e) { }
        if (index==0) { // la primera vez hace el salto al primer registro
            try { recibo.next();
            } catch (Exception e) { }
        }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("dia".equals(jrCampo.getName())) {
                valor = recibo.getDate("fecharecibo").getDate();
            } else if("mes".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.getMeses()[recibo.getDate("fecharecibo").getMonth()];
            } else if("ano".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.getAnho(recibo.getDate("fecharecibo"));
            } else if("ruc".equals(jrCampo.getName())) {
                valor = recibo.getString("ruc");
            } else if("razonsocial".equals(jrCampo.getName())) {
                valor = recibo.getString("razonsocial");
            } else if("numero".equals(jrCampo.getName())) {
                valor = recibo.getInt("numerorecibo");
            } else if("importeletra".equals(jrCampo.getName())) {
                String sletra = new utilitario.utiLetra().leerNumero(recibo.getDouble("total")-recibo.getDouble("exoneracion")).toLowerCase();
                valor = sletra.substring(0,1).toUpperCase()+sletra.substring(1,sletra.length())+".-";
            } else if("concepto".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(detalle.getString("producto") + " " + detalle.getString("referencia"));
            } else if("documento".equals(jrCampo.getName())) {
                valor = "";
            } else if("cuota".equals(jrCampo.getName())) {
                valor = "";
            } else if("monto".equals(jrCampo.getName())) {
                valor = detalle.getDouble("monto");
            } else if("total".equals(jrCampo.getName())) {
                valor = detalle.getInt("cantidad") * detalle.getDouble("monto");
            } else if("monto".equals(jrCampo.getName())) {
                valor = detalle.getDouble("monto");
            } else if("montoexoneracion".equals(jrCampo.getName())) {
                valor = (double)0;
                if (recibo.getDouble("exoneracion")>0) valor = recibo.getDouble("exoneracion");
            } else if("exoneracion".equals(jrCampo.getName())) {
                valor = "";
                if (recibo.getDouble("exoneracion")>0) valor = "Exoneración (-)";
            } else if("importe".equals(jrCampo.getName())) {
                valor = recibo.getDouble("total")-recibo.getDouble("exoneracion");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
