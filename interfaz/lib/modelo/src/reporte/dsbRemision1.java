/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbRemision1 implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet OrdenPago;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsbRemision1(Object mod) {
        this.OrdenPago = (java.sql.ResultSet)mod;
        try {
            this.OrdenPago.last();
            total = this.OrdenPago.getRow()+1;
            this.OrdenPago.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.OrdenPago.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("numerocheque".equals(jrCampo.getName())) {
                valor = OrdenPago.getInt("numerocheque");
            } else if("referente".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("remision");
            } else if("banco".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("banco");
            } else if("monto".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getDouble("montocheque");
            } else if("numeronota".equals(jrCampo.getName())) {
                valor = OrdenPago.getInt("numeronota");
            } else if("concepto".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getString("descripcion");
            } 
        } catch (Exception e) { 
        }
        return valor;
    }
    
}
