/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAutorizacionDescuento implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet autorizacion;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsAutorizacionDescuento(Object mod) {
        this.autorizacion = (java.sql.ResultSet)mod;
        try {
            this.autorizacion.last();
            total = this.autorizacion.getRow()+1;
            this.autorizacion.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.autorizacion.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("descripcion".equals(jrCampo.getName())) {
                valor = autorizacion.getString("descripcion");
            } else if("cuotas".equals(jrCampo.getName())) {
                valor = autorizacion.getInt("cuotas");
            } else if("tipodescuento".equals(jrCampo.getName())) {
                if(autorizacion.getString("frecuenciadescuento").contains("QUINCENA")) {
                    valor = "QUINCENALES";
                } else valor = "MENSUALES";
            } else if("mes".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.getMes(autorizacion.getDate("fechavenc"));
            } else if("anho".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.getAnho(autorizacion.getDate("fechavenc"));
            } else if("costo".equals(jrCampo.getName())) {
                valor = Math.round(autorizacion.getDouble("costo"));
            } else if("montoletras".equals(jrCampo.getName())) {
                String letra;
                letra = new utilitario.utiLetra().leerNumero(autorizacion.getDouble("costo"));
                valor = letra;
            } else if("mensaje".equals(jrCampo.getName())) {
                String tipodescuento;
                int mes=autorizacion.getDate("fechavenc").getMonth()+1;
                if(autorizacion.getString("frecuenciadescuento").contains("QUINCENA")) {
                    tipodescuento = "QUINCENALES";
                } else tipodescuento = "MENSUALES";
                String letra ="Autorizo a la MUTUAL NACIONAL DE FUNCIONARIOS DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL, el descuento mensual de mis haberes, la suma de Gs. " + Math.round(autorizacion.getDouble("costo")) + "  (Guaraníes "+ new utilitario.utiLetra().leerNumero(autorizacion.getDouble("costo")) + "-), en "+ autorizacion.getInt("cuotas") + " cuotas " + tipodescuento +", con primer vencimiento el día "+autorizacion.getDate("fechavenc").getDate()+"/"+mes+"/"+utilitario.utiFecha.getAnho(autorizacion.getDate("fechavenc"))+" a partir del mes de "+ utilitario.utiFecha.getMes(autorizacion.getDate("fechavenc")) +" del año "+ utilitario.utiFecha.getAnho(autorizacion.getDate("fechavenc")) +", en concepto de pago por "+ autorizacion.getString("descripcion");
                valor = letra;
            } 
        } catch (Exception e) { }
        return valor;
    }
    
    
}
