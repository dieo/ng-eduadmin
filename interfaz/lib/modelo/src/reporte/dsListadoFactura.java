/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsListadoFactura implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsListadoFactura(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("numerofactura".equals(jrCampo.getName())) {
                valor = listado.getString("numerofactura");
            } else if("fechaemision".equals(jrCampo.getName())) {
                valor = listado.getDate("fechafactura");
            } else if("fechavencimiento".equals(jrCampo.getName())) {
                valor = listado.getDate("fechavencimiento");
            } else if("ruc".equals(jrCampo.getName())) {
                valor = listado.getString("ruc");
            } else if("razonsocial".equals(jrCampo.getName())) {
                valor = listado.getString("razonsocial");
            } else if("tipo".equals(jrCampo.getName())) {
                valor = listado.getString("tipofactura");
            } else if("exenta".equals(jrCampo.getName())) {
                valor = listado.getDouble("exenta");
            } else if("gravada5".equals(jrCampo.getName())) {
                valor = listado.getDouble("impuesto5");
            } else if("gravada10".equals(jrCampo.getName())) {
                valor = listado.getDouble("impuesto10");
            } else if("total".equals(jrCampo.getName())) {
                valor = listado.getDouble("total");
            } else if("estado".equals(jrCampo.getName())) {
                valor = listado.getString("estado");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
