/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsControlLegajoTipo implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet legajo;
    private String p="";
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsControlLegajoTipo(Object mod, Object mod2) {
        this.legajo = (java.sql.ResultSet)mod;
        this.p = (String)mod2;
        try {
            this.legajo.last();
            total = this.legajo.getRow()+1;
            this.legajo.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.legajo.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = legajo.getInt("numerooperacion");
            } else if("fecha".equals(jrCampo.getName())) {
                valor = legajo.getDate("fecha");
            } else if("numerosolicitud".equals(jrCampo.getName())) {
                valor = legajo.getInt("numerosolicitud");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = legajo.getString("cedula");
            } else if("nombreapellido".equals(jrCampo.getName())) {
                valor = legajo.getString("nombre");
            } else if("monto".equals(jrCampo.getName())) {
                valor = legajo.getDouble("monto");
            } else if("cheque".equals(jrCampo.getName())) {
                valor = legajo.getString("cheque");
            } else if("banco".equals(jrCampo.getName())) {
                valor = legajo.getString("banco");
            } else if("procedencia".equals(jrCampo.getName())) {
                valor = legajo.getString("procedencia");
            } else if("tipooperacion".equals(jrCampo.getName())) {
                valor = legajo.getString("tipooperacion");
            }
            if (p.equals("ERA")) {
                if("codigo1".equals(jrCampo.getName())) {
                    valor = legajo.getString("codigo1");
                } else if("codigo2".equals(jrCampo.getName())) {
                    valor = legajo.getString("codigo2");
                } else if("codigo3".equals(jrCampo.getName())) {
                    valor = legajo.getString("codigo3");
                }
                if("fecha1".equals(jrCampo.getName())) {
                    valor = legajo.getDate("fecha1");
                } else if("fecha2".equals(jrCampo.getName())) {
                    valor = legajo.getDate("fecha2");
                } else if("fecha3".equals(jrCampo.getName())) {
                    valor = legajo.getDate("fecha3");
                }
            }
            if (p.equals("REA")) {
                if("codigo1".equals(jrCampo.getName())) {
                    valor = legajo.getString("codigo2");
                } else if("codigo2".equals(jrCampo.getName())) {
                    valor = legajo.getString("codigo1");
                } else if("codigo3".equals(jrCampo.getName())) {
                    valor = legajo.getString("codigo3");
                }
                if("fecha1".equals(jrCampo.getName())) {
                    valor = legajo.getDate("fecha2");
                } else if("fecha2".equals(jrCampo.getName())) {
                    valor = legajo.getDate("fecha1");
                } else if("fecha3".equals(jrCampo.getName())) {
                    valor = legajo.getDate("fecha3");
                }
            }
            if (p.equals("AER")) {
                if("codigo1".equals(jrCampo.getName())) {
                    valor = legajo.getString("codigo2");
                } else if("codigo2".equals(jrCampo.getName())) {
                    valor = legajo.getString("codigo3");
                } else if("codigo3".equals(jrCampo.getName())) {
                    valor = legajo.getString("codigo1");
                }
                if("fecha1".equals(jrCampo.getName())) {
                    valor = legajo.getDate("fecha2");
                } else if("fecha2".equals(jrCampo.getName())) {
                    valor = legajo.getDate("fecha3");
                } else if("fecha3".equals(jrCampo.getName())) {
                    valor = legajo.getDate("fecha1");
                }
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
