/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsLibroEgreso implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modFacturaEgreso mod;
    private int index = -1;

    public dsLibroEgreso(Object mod) {
        this.mod = (especifico.modFacturaEgreso)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null; 
        if("numerofactura".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNumeroFactura();
        }
        if("fechaemision".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFechaFactura();
        }
        if("fechavencimiento".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFechaVencimiento();
        }
        if("tipo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTipoFactura();
        }
        if("proveedor".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getProveedor();
        }
        if("ruc".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getRuc();
        }
        if("importetotal".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getImporteTotal();
        }
        if("exenta".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getExenta();
        }
        if("impuesto5".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getImpuesto5();
        }
        if("impuesto10".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getImpuesto10();
        }
        if("totalimpuesto".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTotalImpuesto();
        }
        return valor;
    }
    
}
