/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPromotorOperacion implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet operacion;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsPromotorOperacion(Object mod) {
        this.operacion = (java.sql.ResultSet)mod;
        try {
            this.operacion.last();
            total = this.operacion.getRow()+1;
            this.operacion.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.operacion.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("operacion".equals(jrCampo.getName())) {
                valor = operacion.getString("tabla");
            } else if("regional".equals(jrCampo.getName())) {
                valor = operacion.getString("regional");
            } else if("promotor".equals(jrCampo.getName())) {
                valor = operacion.getString("promotor");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = operacion.getInt("numerooperacion");
            } else if("tasainteres".equals(jrCampo.getName())) {
                valor = operacion.getDouble("tasainteres");
            } else if("montointeres".equals(jrCampo.getName())) {
                valor = operacion.getDouble("montointeres");
            } else if("plazo".equals(jrCampo.getName())) {
                valor = operacion.getInt("plazo");
            } else if("importe".equals(jrCampo.getName())) {
                valor = operacion.getDouble("importe");
            } else if("fechaoperacion".equals(jrCampo.getName())) {
                valor = operacion.getDate("fechaoperacion");
            } else if("estado".equals(jrCampo.getName())) {
                valor = operacion.getString("estado");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = operacion.getString("cedula");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = operacion.getString("apellidonombre");
            } else if("origen".equals(jrCampo.getName())) {
                valor = operacion.getString("origen");
            } else if("tipooperacion".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(operacion.getString("tipooperacion"));
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
