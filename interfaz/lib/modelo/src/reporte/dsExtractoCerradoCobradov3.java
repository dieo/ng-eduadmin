/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsExtractoCerradoCobradov3 implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entSocio socio;
    private especifico.entLugarLaboral labor;
    private especifico.modExtractoCerradoCobradov3 ope;
    private int index = -1;
    private String rubro = "";

    public dsExtractoCerradoCobradov3(Object mod, Object mod2, Object mod3) {
        this.socio = (especifico.entSocio)mod;
        this.labor = (especifico.entLugarLaboral)mod2;
        this.ope = (especifico.modExtractoCerradoCobradov3)mod3;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < ope.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if ("cedula".equals(jrCampo.getName())) {
            valor = socio.getCedula();
        } else if ("numerosocio".equals(jrCampo.getName())) {
            valor = socio.getNumeroSocio();
        } else if ("nombreapellido".equals(jrCampo.getName())) {
            valor = socio.getNombre()+" "+socio.getApellido();
        } else if ("fecha".equals(jrCampo.getName())) {
            valor = new java.util.Date();
        } else if ("estado".equals(jrCampo.getName())) {
            valor = socio.getEstado();
        } else if ("giraduria".equals(jrCampo.getName())) {
            valor = labor.getGiraduria();
        } else if ("rubro".equals(jrCampo.getName())) {
            valor = labor.getRubro();
        } else if ("periodo".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getPeriodo();
        } else if ("cuenta".equals(jrCampo.getName())) {
            valor = "- "+ope.getEntidad(index).getDescripcion();
        } else if ("rubrodetalle".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getRubro();
        } else if ("montoatraso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getAtraso();
        } else if ("montocerrado".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getCerrado();
        } else if ("montogiraduria".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getGiraduria();
        } else if ("montoventanilla".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getVentanilla();
        } else if ("montoasignacion".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getAsignacion();
        } else if ("montoaplicado".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getAplicado();
        }
        return valor;
    }
    
}
