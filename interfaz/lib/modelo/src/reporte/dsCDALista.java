/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsCDALista implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet CDA;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;
    
    public dsCDALista(Object mod) {
        this.CDA = (java.sql.ResultSet)mod;
        try {
            this.CDA.last();
            total = this.CDA.getRow()+1;
            this.CDA.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.CDA.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("numerocda".equals(jrCampo.getName())) {
                valor = CDA.getInt("numerocda");
            } else if("serie".equals(jrCampo.getName())) {
                valor = CDA.getString("serie");
            } else if("tipocda".equals(jrCampo.getName())) {
                valor = CDA.getString("tipocda");
            } else if("estadocda".equals(jrCampo.getName())) {
                valor = CDA.getString("estadocda");
            } else if("fechainicio".equals(jrCampo.getName())) {
                valor = CDA.getDate("fechainicio");
            } else if("fechafin".equals(jrCampo.getName())) {
                valor = CDA.getDate("fechafin");
            } else if("dias".equals(jrCampo.getName())) {
                valor = CDA.getInt("dias");
            } else if("orden".equals(jrCampo.getName())) {
                valor = CDA.getInt("orden");
            } else if("capital".equals(jrCampo.getName())) {
                valor = CDA.getDouble("capitalactual");
            } else if("total".equals(jrCampo.getName())) {
                valor = CDA.getDouble("total");
            } else if("tasa".equals(jrCampo.getName())) {
                valor = CDA.getDouble("tasainteres");
            } else if("observacion".equals(jrCampo.getName())) {
                valor = CDA.getString("observacion");
            } else if("tipocapitalizacion".equals(jrCampo.getName())) {
                valor = CDA.getString("tipocapitalizacion");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = CDA.getString("cedula");
            } else if("nombre".equals(jrCampo.getName())) {
                valor = CDA.getString("nombre");
            }
        } catch (Exception e) { 
        }return valor;
    }
    
}
