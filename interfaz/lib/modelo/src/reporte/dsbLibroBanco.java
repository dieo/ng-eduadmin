/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbLibroBanco implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modbLibroBanco mod;
    private int index = -1;

    public dsbLibroBanco(Object mod) {
        this.mod = (especifico.modbLibroBanco)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("fechaoperacion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFechaOperacion();
        } else if("tipo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getTipo();
        } else if("comprobante".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getComprobante();
        } else if("concepto".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getConcepto();
        } else if("debe".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDebe();
        } else if("haber".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getHaber();
        } else if("saldo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getSaldo();
        } else if("cuentabanco".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getBancoCuenta();
        }return valor;
    }
    
}
