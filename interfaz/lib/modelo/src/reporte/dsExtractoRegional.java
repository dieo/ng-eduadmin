/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsExtractoRegional implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modExtractoRegional ope;
    private int index = 0;

    public dsExtractoRegional(Object mod) {
        this.ope = (especifico.modExtractoRegional)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.ope.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if ("cedula".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getCedula();
        } else if ("numerosocio".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getNumeroSocio();
        } else if ("nombreapellido".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getNombre()+" "+ope.getEntidad(index).getApellido();
        } else if ("fechaingreso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getFechaIngreso();
        } else if ("antiguedad".equals(jrCampo.getName())) {
            valor = utilitario.utiFecha.obtenerAntiguedad(ope.getEntidad(index).getFechaIngreso(), new java.util.Date());
        } else if ("estado".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getEstado();
        } else if ("giraduria".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getGiraduria();
        } else if ("rubro".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getRubroPrincipal();
        } else if ("institucion".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getInstitucion();
        } else if ("regional".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getRegional();
        } else if ("cuenta".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getCuenta();
        } else if ("denominacion".equals(jrCampo.getName())) {
            valor = "- "+utilitario.utiCadena.convertirMayusMinus(ope.getEntidad(index).getDenominacion());
        } else if ("rubrodetalle".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getRubro();
        } else if ("numerooperacion".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getNumeroOperacion();
        } else if ("fechaemision".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getFechaOperacion();
        } else if ("plazo".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getNumeroCuota()+"/"+ope.getEntidad(index).getPlazo();
        } else if ("diaatraso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getDiaAtraso();
        } else if ("monto".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getMontoTotal();
        } else if ("cobro".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getCobro();
        } else if ("atraso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getAtraso();
        } else if ("actual".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getActual();
        } else if ("saldo".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getSaldo();
        } else if ("tipoinforme".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getTipoInforme();
        } else if ("fecha".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getFecha();
        } else if ("cantidades".equals(jrCampo.getName())) {
            valor = "Prést. Mutual: "+ope.getEntidad(index).getCantidadPrestamoMutual();
            valor += " - Prést. Financiera: "+ope.getEntidad(index).getCantidadPrestamoFinanciera();
            valor += " - Orden Crédito: "+ope.getEntidad(index).getCantidadOrdenCredito();
        }
        return valor;
    }
    
}
