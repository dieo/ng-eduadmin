/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dscArticulo implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet articulo;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dscArticulo(Object mod) {
        this.articulo = (java.sql.ResultSet)mod;
        try {
            this.articulo.last();
            total = this.articulo.getRow()+1;
            this.articulo.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.articulo.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        try {
            if("descripcion".equals(jrCampo.getName())) {
                valor = this.articulo.getString("descripcion");
            } else if("filial".equals(jrCampo.getName())) {
                valor = this.articulo.getString("filial");
            } else if("categoria".equals(jrCampo.getName())) {
                valor = this.articulo.getString("categoria");
            } else if("stock".equals(jrCampo.getName())) {
                valor = this.articulo.getDouble("stock");
            } else if("minimo".equals(jrCampo.getName())) {
                valor = this.articulo.getDouble("minimo");
            } else if("maximo".equals(jrCampo.getName())) {
                valor = this.articulo.getDouble("maximo");
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = this.articulo.getDouble("cantidad");
            } else if("fechacompra".equals(jrCampo.getName())) {
                valor = this.articulo.getDate("fechacompra");
            } else if("unidad".equals(jrCampo.getName())) {
                valor = this.articulo.getString("unidadmedida");
            }
        } catch (Exception e){            
        }
        return valor;
    }
    
}
