/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbTipoMovimiento implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modbTipoMovimiento mod;
    private int index = -1;

    public dsbTipoMovimiento(Object mod) {
        this.mod = (especifico.modbTipoMovimiento)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("descripcion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDescripcion();
        } else if("tipomovimiento".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDebitoCredito();
        }
        return valor;
    }
    
}
