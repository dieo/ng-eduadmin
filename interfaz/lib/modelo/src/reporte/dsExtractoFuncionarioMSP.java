/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsExtractoFuncionarioMSP implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entListaFuncionarioMSP funcionario;
    private especifico.modExtractoPrestamo ope;
    private int index = -1;
    private String rubro = "";

    public dsExtractoFuncionarioMSP(Object mod, Object mod2) {
        this.funcionario = (especifico.entListaFuncionarioMSP)mod;
        this.ope = (especifico.modExtractoPrestamo)mod2;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < ope.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if ("cedula".equals(jrCampo.getName())) {
            valor = funcionario.getCedula();
        } else if ("numerosocio".equals(jrCampo.getName())) {
            valor = 0;
        } else if ("nombreapellido".equals(jrCampo.getName())) {
            valor = funcionario.getNombreApellido();
        } else if ("fechaingreso".equals(jrCampo.getName())) {
            valor = new java.util.Date();
        } else if ("fecha".equals(jrCampo.getName())) {
            valor = new java.util.Date();
        } else if ("antiguedad".equals(jrCampo.getName())) {
            valor = "";
        } else if ("estado".equals(jrCampo.getName())) {
            valor = "NO SOCIO";
        } else if ("giraduria".equals(jrCampo.getName())) {
            valor = funcionario.getGiraduria();
        } else if ("rubro".equals(jrCampo.getName())) {
            valor = funcionario.getRubro();
        } else if ("institucion".equals(jrCampo.getName())) {
            valor = "";
        } else if ("regional".equals(jrCampo.getName())) {
            valor = "";
        } else if ("cuenta".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getCuenta();
        } else if ("denominacion".equals(jrCampo.getName())) {
            valor = "- "+utilitario.utiCadena.convertirMayusMinus(ope.getEntidad(index).getDenominacion());
        } else if ("rubrodetalle".equals(jrCampo.getName())) {
            rubro = "";
            if (!ope.getEntidad(index).getRubro().isEmpty()) rubro = ope.getEntidad(index).getRubro();
            valor = rubro;
        } else if ("numerooperacion".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getNumeroOperacion();
        } else if ("fechaemision".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getFechaOperacion();
        } else if ("plazo".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getNumeroCuota()+"/"+ope.getEntidad(index).getPlazo();
        } else if ("diaatraso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getDiaAtraso();
        } else if ("monto".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getMontoTotal();
        } else if ("cobro".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getCobro();
        } else if ("atraso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getAtraso();
        } else if ("actual".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getActual();
        } else if ("saldo".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getSaldo();
        }
        return valor;
    }
    
}
