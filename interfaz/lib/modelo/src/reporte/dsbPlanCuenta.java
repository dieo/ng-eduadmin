/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbPlanCuenta implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modbPlanCuenta mod;
    private int index = -1;

    public dsbPlanCuenta(Object mod) {
        this.mod = (especifico.modbPlanCuenta)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("nroplancuenta".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNroPlanCuenta();
        } else if("descripcion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDescripcion();
        } else if("nivel".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getNivel();
        } else if("imputable".equals(jrCampo.getName())) {
            if(this.mod.getEntidad(index).getImputable()) valor = "SI";
            else valor = "NO";
        } else if("activo".equals(jrCampo.getName())) {
            if(this.mod.getEntidad(index).getActivo()) valor = "SI";
            else valor = "NO";
        } else if("periodo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getPeriodo();
        }
        return valor;
    }
    
}
