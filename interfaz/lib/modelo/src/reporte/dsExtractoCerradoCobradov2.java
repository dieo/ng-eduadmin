/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsExtractoCerradoCobradov2 implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entSocio socio;
    private especifico.entLugarLaboral labor;
    private especifico.modExtractoCerradoCobradov2 ope;
    private int index = -1;
    private String rubro = "";

    public dsExtractoCerradoCobradov2(Object mod, Object mod2, Object mod3) {
        this.socio = (especifico.entSocio)mod;
        this.labor = (especifico.entLugarLaboral)mod2;
        this.ope = (especifico.modExtractoCerradoCobradov2)mod3;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < ope.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if ("cedula".equals(jrCampo.getName())) {
            valor = socio.getCedula();
        } else if ("numerosocio".equals(jrCampo.getName())) {
            valor = socio.getNumeroSocio();
        } else if ("nombreapellido".equals(jrCampo.getName())) {
            valor = socio.getNombre()+" "+socio.getApellido();
        } else if ("fecha".equals(jrCampo.getName())) {
            valor = new java.util.Date();
        } else if ("estado".equals(jrCampo.getName())) {
            valor = socio.getEstado();
        } else if ("giraduria".equals(jrCampo.getName())) {
            valor = labor.getGiraduria();
        } else if ("rubro".equals(jrCampo.getName())) {
            valor = labor.getRubro();
        } else if ("periodo".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getPeriodo();
        } else if ("cuenta".equals(jrCampo.getName())) {
            valor = "- "+ope.getEntidad(index).getCuenta();
        } else if ("rubrodetalle".equals(jrCampo.getName())) {
            rubro = "";
            if (!ope.getEntidad(index).getRubro().isEmpty()) rubro = ope.getEntidad(index).getRubro();
            valor = rubro;
        } else if ("montoatraso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getMontoAtraso();
        } else if ("montocerrado".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getMontoCierre();
        } else if ("montogiraduria".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getMontoGiraduria();
        } else if ("montoventanilla".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getMontoVentanilla();
        } else if ("montoasignacion".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getMontoAsignacion();
        } else if ("montoreembolso".equals(jrCampo.getName())) {
            valor = ope.getEntidad(index).getMontoReembolso();
        } else if ("montodiferencia".equals(jrCampo.getName())) {
            double diferencia = ope.getEntidad(index).getMontoAtraso()+ope.getEntidad(index).getMontoCierre()-ope.getEntidad(index).getMontoGiraduria()-ope.getEntidad(index).getMontoVentanilla()-ope.getEntidad(index).getMontoAsignacion()-ope.getEntidad(index).getMontoReembolso();
            valor = diferencia;
            if (diferencia<0) valor = 0.0;
        }
        return valor;
    }
    
}
