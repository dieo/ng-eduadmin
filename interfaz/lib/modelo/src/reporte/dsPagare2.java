/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsPagare2 implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entMovimiento orden;
    private especifico.entSocio socio;
    private especifico.modMovimientoGarante garante;
    private int index = -1;

    public dsPagare2(Object mod, Object mod1, Object mod2) {
        this.orden = (especifico.entMovimiento)mod;
        this.socio = (especifico.entSocio)mod1;
        this.garante = (especifico.modMovimientoGarante)mod2;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("numerosolicitud".equals(jrCampo.getName())) {
            valor = this.orden.getNumeroSolicitud();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = this.orden.getNumeroOperacion();
        } else if("fechaoperacion".equals(jrCampo.getName())) {
            valor = this.orden.getFechaGeneracion();
        } else if("fechavencimiento".equals(jrCampo.getName())) {
            valor = this.orden.getFechaVencimiento();
        } else if("textoprincipal".equals(jrCampo.getName())) {
            double utotal = this.orden.getMontoAprobado()+this.orden.getMontoInteres();
            String sletra = new utilitario.utiLetra().leerNumero(utotal).trim().toLowerCase();
            valor = "Pagaré(mos) a la orden de la Mutual Nacional de Funcionarios del Ministerio de Salud Pública y Bienestar Social, ";
            valor += "sin protesto y solidariamente en su domicilio, sito en Brasil Nº 999 esq. Tte. Fariña, la suma de Gs. ";
            valor += new java.text.DecimalFormat("###,###,###,###").format(utotal)+" ";
            valor += "(" + sletra.substring(0,1).toUpperCase()+sletra.substring(1,sletra.length()) + " Guaraníes) ";
            valor += "por igual valor recibido en efectivo a mi (nuestra) entrega satisfacción. Expreso(amos) mi (nuestra) conformidad a todos los ";
            valor += "recaudos solicitados por la Mutual y los siguientes puntos que se enumera: ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
        } else if("item1".equals(jrCampo.getName())) {
            valor = "1) La falta de pago de este documento a su vencimiento, generará de pleno derecho, además del interés moratorio, ";
            valor += "un interés punitorio adicional del 30% de la tasa a percibirse en concepto de interés moratorio, sin que ello ";
            valor += "implique renovación, prórroga o espera. - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
        } else if("item2".equals(jrCampo.getName())) {
            valor = "2) La constitución en mora se producirá por el mero vencimiento del plazo, y sin necesidad de protesto, intimación ";
            valor += "judicial ni extrajudicial de pago por parte del deudor. - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
        } else if("item3".equals(jrCampo.getName())) {
            valor = "3) El pago de la pena no implicará la extinción de la obligación principal. - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
        } else if("item4".equals(jrCampo.getName())) {
            valor = "4) El acreedor podrá ir modificando el porcentaje de interés pactado, si así lo considerase, reajustándolo de acuerdo ";
            valor += "con el promedio ponderado de las tasas activas aplicadas por los bancos y otras entidades financieras, dentro de ";
            valor += "las disposiciones legales pertinentes. - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
        } else if("item5".equals(jrCampo.getName())) {
            valor = "5) A todos los efectos legales y procesales, en caso de divergencia, las partes se someten a la jurisdicción y ";
            valor += "competencia de los Tribunales de la ciudad de Asunción. - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
        } else if("item6".equals(jrCampo.getName())) {
            valor = "6) A todos los efectos legales, para las notificaciones se fija(n) domicilio(s) en las direcciones indicadas más abajo ";
            valor += "por el socio y/o codeudor. - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            valor += "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
        } else if("socio".equals(jrCampo.getName())) {
            try                 { valor = "C.I.C. "+this.socio.getCedula()+" - "+this.socio.getNombre()+" "+this.socio.getApellido(); }
            catch (Exception e) { valor = ""; }
        } else if("garante1".equals(jrCampo.getName())) {
            try                 { valor = "C.I.C. "+this.garante.getEntidad(0).getCedula()+" - "+this.garante.getEntidad(0).getNombre()+" "+this.garante.getEntidad(0).getApellido(); }
            catch (Exception e) { valor = ""; }
        } else if("garante2".equals(jrCampo.getName())) {
            try                 { valor = "C.I.C. "+this.garante.getEntidad(1).getCedula()+" - "+this.garante.getEntidad(1).getNombre()+" "+this.garante.getEntidad(1).getApellido(); }
            catch (Exception e) { valor = ""; }
        } else if("garante3".equals(jrCampo.getName())) {
            try                 { valor = "C.I.C. "+this.garante.getEntidad(2).getCedula()+" - "+this.garante.getEntidad(2).getNombre()+" "+this.garante.getEntidad(2).getApellido(); }
            catch (Exception e) { valor = ""; }
        } else if("textopie".equals(jrCampo.getName())) {
            double utotal = this.orden.getMontoAprobado()+this.orden.getMontoInteres();
            String sletra = new utilitario.utiLetra().leerNumero(utotal).trim().toLowerCase();
            valor = "Conste que "+this.socio.getNombre()+" "+this.socio.getApellido()+" con C.I.C. "+this.socio.getCedula()+" firmó en mi presencia ";
            valor += "el pagaré Nº "+this.orden.getNumeroOperacion()+" de Gs. "+new java.text.DecimalFormat("###,###,###,###").format(utotal)+" (";
            valor += sletra.substring(0,1).toUpperCase()+sletra.substring(1,sletra.length()) + " Guaraníes), contra entrega del respectivo cheque.";
        }
        return valor;
    }
    
}
