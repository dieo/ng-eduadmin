/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsLlamadaSocioRegional implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modLlamadaRegional mod;
    private int index = -1;

    public dsLlamadaSocioRegional(Object mod) {
        this.mod = (especifico.modLlamadaRegional)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("cedula".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCedula();
        } else if("apellidonombre".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNombreApellido();
        } else if("regional".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getRegional();
        } else if("fecha".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaLlamada();
        } else if("hora".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getHoraLlamada();
        } else if("duracion".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getDuracion();
        } else if("telefono".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getTelefono();
        } else if("tipo".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getTipoLlamada();
        } else if("motivo".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getMotivoLlamada();
        } else if("intermediario".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNombreApellidoIntermediario();
        } else if("observacion".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getObservacion();
        }
        return valor;
    }
    
}
