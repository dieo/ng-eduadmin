/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbOrdenPago1 implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet OrdenPago;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;
    private String snombreCuentaOP = "";
    private String snumeroPlanCuentaOP = "";
    private String smontoDetalleOP = "";

    public dsbOrdenPago1(Object mod) {
        this.OrdenPago = (java.sql.ResultSet)mod;
        //try {
        //    this.OrdenPago.last();
        //    total = this.OrdenPago.getRow()+1;
        //    this.OrdenPago.beforeFirst();
        //} catch (Exception e) { }
        int inumeroOrden = 0;
        snombreCuentaOP = "";
        snumeroPlanCuentaOP = "";
        smontoDetalleOP = "";
        try {
            this.OrdenPago.beforeFirst();
            while (this.OrdenPago.next()) {
                if (inumeroOrden!=this.OrdenPago.getInt("numeroorden")){
                    inumeroOrden = this.OrdenPago.getInt("numeroorden"); 
                    snombreCuentaOP = "";
                    snumeroPlanCuentaOP = "";
                    smontoDetalleOP = "";
                    total += 1;
                } 
                snombreCuentaOP = snombreCuentaOP + this.OrdenPago.getString("nombrecuentaop") + "\n";
                snumeroPlanCuentaOP = snumeroPlanCuentaOP + this.OrdenPago.getString("nroplancuentaop") + "\n";
                smontoDetalleOP = smontoDetalleOP + utilitario.utiNumero.getMascara(this.OrdenPago.getDouble("montodetalleop"),0) + "\n";
            }
        } catch(Exception e) {
        }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.OrdenPago.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("numeroorden".equals(jrCampo.getName())) {
                valor = OrdenPago.getInt("numeroorden");
            } else if("fechaorden".equals(jrCampo.getName())) {
                valor = OrdenPago.getDate("fecha");
            } else if("numerosolicitud".equals(jrCampo.getName())) {
                valor = OrdenPago.getInt("nrosolicitud");
            } else if("receptor".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("receptor");
            } else if("numerocuenta".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("numerocuenta");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("cedularuc");
            } else if("numeroplancuentaop".equals(jrCampo.getName())) {
                //valor = OrdenPago.getString("nroplancuentaop");
                valor = snumeroPlanCuentaOP.substring(0, snumeroPlanCuentaOP.length()-1);
            } else if("nombrecuentaop".equals(jrCampo.getName())) {
                //valor = this.OrdenPago.getString("nombrecuentaop");
                valor = snombreCuentaOP.substring(0, snombreCuentaOP.length()-1);
            } else if("explicacionop".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getString("descripcion");
            } else if("importeop".equals(jrCampo.getName())) {
                //valor = this.OrdenPago.getDouble("montodetalleop");
                valor = smontoDetalleOP.substring(0, smontoDetalleOP.length()-1);
            } else if("numeroplancuentacheque".equals(jrCampo.getName())) {
                valor = OrdenPago.getString("nroplancuentacheque");
            } else if("nombrecuentacheque".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getString("nombrecuentacheque");
            } else if("explicacioncheque".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getString("explicacioncheque");
            } else if("importecheque".equals(jrCampo.getName())) {
                valor = this.OrdenPago.getDouble("montocheque");
            } else if("montoletras".equals(jrCampo.getName())) {
                String letra = "RECIBI DE LA MUTUAL NACIONAL DE FUNCIONARIOS DEL M.S.P. y B.S., LA SUMA DE GUARANÍES: ";
                letra += new utilitario.utiLetra().leerNumero(this.OrdenPago.getDouble("total"));
                valor = letra+ ".- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
            } 
        } catch (Exception e) { 
        }
        return valor;
    }
    
}
