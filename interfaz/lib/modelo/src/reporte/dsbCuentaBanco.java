/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbCuentaBanco implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet CuentaBanco;
    private int index = 0;
    private int total = 0;

    public dsbCuentaBanco(Object mod) {
        this.CuentaBanco = (java.sql.ResultSet)mod;
        try {
            this.CuentaBanco.last();
            total = this.CuentaBanco.getRow()+1;
            this.CuentaBanco.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.CuentaBanco.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("banco".equals(jrCampo.getName())) {
                valor = CuentaBanco.getString("banco");
            } else if("numerocuenta".equals(jrCampo.getName())) {
                valor = CuentaBanco.getString("numerocuenta");
            } else if("tipocuenta".equals(jrCampo.getName())) {
                valor = CuentaBanco.getString("tipocuenta");
            } else if("moneda".equals(jrCampo.getName())) {
                valor = CuentaBanco.getString("moneda");
            } else if("fechaapertura".equals(jrCampo.getName())) {
                valor = CuentaBanco.getDate("fechaapertura");
            } else if("saldo".equals(jrCampo.getName())) {
                valor = CuentaBanco.getDouble("saldo");
            }
        } catch (Exception e) { 
        }    
        return valor;
    }
    
}
