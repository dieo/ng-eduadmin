/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAhorroProgramadoRegionEstado implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet ahorro;
    private double usaldo;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsAhorroProgramadoRegionEstado(Object mod) {
        this.ahorro = (java.sql.ResultSet)mod;
        this.usaldo = 0.0;
        try {
            this.ahorro.last();
            total = this.ahorro.getRow()+1;
            this.ahorro.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.ahorro.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            //this.ahorro.beforeFirst();
            //for (int i=0; i<index; i++) this.ahorro.next();
            if("cedula".equals(jrCampo.getName())) {
                valor = ahorro.getString("cedula");
            } else if("apellidonombre".equals(jrCampo.getName())) {
                valor = ahorro.getString("apellidonombre");
            } else if("estado".equals(jrCampo.getName())) {
                valor = ahorro.getString("estado");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = ahorro.getInt("numerooperacion");
            } else if("contrato".equals(jrCampo.getName())) {
                valor = ahorro.getString("contrato");
            } else if("promotor".equals(jrCampo.getName())) {
                valor = ahorro.getString("promotor");
            } else if("regional".equals(jrCampo.getName())) {
                valor = ahorro.getString("regional");
            } else if("plan".equals(jrCampo.getName())) {
                valor = ahorro.getString("plan");
            } else if("plazo".equals(jrCampo.getName())) {
                valor = ahorro.getInt("plazo");
            } else if("importe".equals(jrCampo.getName())) {
                valor = ahorro.getDouble("importe");
            } else if("moneda".equals(jrCampo.getName())) {
                valor = ahorro.getString("moneda");
            } else if("tasainteres".equals(jrCampo.getName())) {
                valor = ahorro.getDouble("tasainteres");
            } else if("fechaoperacion".equals(jrCampo.getName())) {
                valor = ahorro.getDate("fechaoperacion");
            } else if("fechainicio".equals(jrCampo.getName())) {
                valor = ahorro.getDate("fechaprimervencimiento");
            } else if("fechavencimiento".equals(jrCampo.getName())) {
                valor = ahorro.getDate("fechavencimiento");
            } else if("compromiso".equals(jrCampo.getName())) {
                valor = ahorro.getDouble("montocapital");
            } else if("pagado".equals(jrCampo.getName())) {
                valor = ahorro.getDouble("montocapital")-ahorro.getDouble("saldocapital");
            } else if("interes".equals(jrCampo.getName())) {
                //valor = ahorro.getDouble("montocapital")*ahorro.getDouble("tasainteres")/200;
                double capital = ahorro.getDouble("montocapital");
                int plazo = ahorro.getInt("plazo");
                double tasa = ahorro.getDouble("tasainteres");
                java.util.Date fecha= ahorro.getDate("fechaprimervencimiento");
                valor = this.calculoInteres(capital, plazo, tasa, fecha);
            }
        } catch (Exception e) { }
        return valor;
    }
    
    private double calculoInteres(double capital, int plazo, double tasa, java.util.Date fecha) {
        double interes = 0;
        double acumulado = 0;
        double cuota = capital/plazo;
        double tasaDia = tasa/365;
        int cantDia = 0;
        java.util.Date fechaAux = (java.util.Date)fecha.clone();
        for (int i=0; i<plazo; i++) {
            cantDia = fechaAux.getDate();
            interes = interes + tasaDia * cantDia * acumulado / 100;
            acumulado = acumulado + cuota;
            fechaAux = utilitario.utiFecha.calcularSiguienteVencimiento(fechaAux);
        }
        interes = interes + tasaDia * 11 * acumulado / 100;
        return interes;
    }
    
}
