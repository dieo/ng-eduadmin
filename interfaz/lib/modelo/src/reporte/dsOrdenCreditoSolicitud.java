/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsOrdenCreditoSolicitud implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entMovimiento orden;
    private especifico.modMovimientoGarante garante;
    private int index = -1;

    public dsOrdenCreditoSolicitud(Object mod, Object mod2) {
        this.orden = (especifico.entMovimiento)mod;
        this.garante = (especifico.modMovimientoGarante)mod2;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("fechasolicitud".equals(jrCampo.getName())) {
            int ano = orden.getFechaSolicitud().getYear()+1900;
            valor = "Asunción, "+orden.getFechaSolicitud().getDate()+" de "+utilitario.utiFecha.getMeses()[orden.getFechaSolicitud().getMonth()].toLowerCase()+" de "+ano;
        } else if("encabezado".equals(jrCampo.getName())) {
            valor = "Señor\nPresidente de la Comisión Directiva\nMutual Nacional de Funcionarios del M.S.P. y B.S.\nPresente";
        } else if("remision".equals(jrCampo.getName())) {
            valor = orden.getRemision();
        } else if("texto".equals(jrCampo.getName())) {
            String sletra = new utilitario.utiLetra().leerNumero(orden.getMontoSolicitud()).trim().toLowerCase();
            sletra = sletra.substring(0,1).toUpperCase()+sletra.substring(1,sletra.length());
            double umontoCuota = (orden.getMontoSolicitud()+(orden.getMontoSolicitud()*orden.getPlazoSolicitud()*orden.getTasaInteres()/100))/orden.getPlazoSolicitud();
            valor = "\t"+orden.getNombreApellido()+", en mi carácter de socio/a, con C.I.C. Nº "+utilitario.utiNumero.getMascara(orden.getCedula(),0)+" solicito una Orden de Crédito de Gs. "+
                    utilitario.utiNumero.getMascara(orden.getMontoSolicitud(),0) + " (" + sletra + " Guaraníes) "+
                    "en el comercio " + orden.getEntidad() + ", a ser destinado a "+orden.getDestino() +
                    ", en un plazo de "+orden.getPlazoSolicitud()+" meses, en cuotas mensuales de Gs. "+utilitario.utiNumero.getMascara(umontoCuota,0);
        } else if("codeudor".equals(jrCampo.getName())) {
            int ipos = 0;
            String snombre = "";
            String sapellido = "";
            valor = "Codeudor: ";
            if (garante.getRowCount()==2) valor = "Codeudores: ";
            if (garante.getRowCount()==1 || garante.getRowCount()==2) {
                ipos = garante.getEntidad(0).getApellidoNombre().indexOf(",");
                snombre = garante.getEntidad(0).getApellidoNombre().substring(ipos+2,garante.getEntidad(0).getApellidoNombre().length());
                sapellido = garante.getEntidad(0).getApellidoNombre().substring(0,ipos);
                valor += utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(0).getCedula(),0);
                if (garante.getRowCount()==2) {
                    valor += "  ---  ";
                    ipos = garante.getEntidad(1).getApellidoNombre().indexOf(",");
                    snombre = garante.getEntidad(1).getApellidoNombre().substring(ipos+2,garante.getEntidad(1).getApellidoNombre().length());
                    sapellido = garante.getEntidad(1).getApellidoNombre().substring(0,ipos);
                    valor += utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(1).getCedula(),0);
                }
            }
        } else if("solicitante".equals(jrCampo.getName())) {
            valor = utilitario.utiCadena.convertirMayusMinus(orden.getNombreApellido().toLowerCase())+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(orden.getCedula(),0);
        } else if("codeudor1".equals(jrCampo.getName())) {
            if (garante.getRowCount()==1 || garante.getRowCount()==2) {
                int ipos = garante.getEntidad(0).getApellidoNombre().indexOf(",");
                String snombre = garante.getEntidad(0).getApellidoNombre().substring(ipos+2,garante.getEntidad(0).getApellidoNombre().length());
                String sapellido = garante.getEntidad(0).getApellidoNombre().substring(0,ipos);
                valor = utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(0).getCedula(),0);
            }
        } else if("codeudor2".equals(jrCampo.getName())) {
            if (garante.getRowCount()==2) {
                int ipos = garante.getEntidad(1).getApellidoNombre().indexOf(",");
                String snombre = garante.getEntidad(1).getApellidoNombre().substring(ipos+2,garante.getEntidad(1).getApellidoNombre().length());
                String sapellido = garante.getEntidad(1).getApellidoNombre().substring(0,ipos);
                valor = utilitario.utiCadena.convertirMayusMinus(snombre+" "+sapellido)+" - C.I.C. Nº "+utilitario.utiNumero.getMascara(garante.getEntidad(1).getCedula(),0);
            }
        } else if("cantidadcodeudor".equals(jrCampo.getName())) {
            valor = garante.getRowCount();
        }
        return valor;
    }
    
}
