/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAsientoBorrador implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsAsientoBorrador(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
            try {
                if("item".equals(jrCampo.getName())) {
                    valor = index;
                } else if("id".equals(jrCampo.getName())) {
                    valor = listado.getInt("id");
                } else if("numeroasiento".equals(jrCampo.getName())) {
                    valor = listado.getInt("numeroasiento");
                } else if("idoperacion".equals(jrCampo.getName())) {
                    valor = listado.getInt("idoperacion");
                } else if("detalle".equals(jrCampo.getName())) {
                    valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("detalle"));
                } else if("nrocuentacontable".equals(jrCampo.getName())) {
                    valor = listado.getString("nrocuentacontable");
                } else if("cuentacontable".equals(jrCampo.getName())) {
                    valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("cuentacontable"));
                } else if("usuario".equals(jrCampo.getName())) {
                    valor = listado.getString("usuario");
                } else if("fecha".equals(jrCampo.getName())) {
                    valor = listado.getDate("fecha");
                } else if("periodocontable".equals(jrCampo.getName())) {
                    valor = listado.getInt("periodocontable");
                } else if("empresa".equals(jrCampo.getName())) {
                    valor = "MUTUAL NACIONAL DE FUNCIONARIOS DEL MSP Y BS - "+listado.getString("empresa");
                } else if("idorigen".equals(jrCampo.getName())) {
                    valor = listado.getInt("idorigen");
                } else if("estado".equals(jrCampo.getName())) {
                    valor = listado.getString("estado");
                } else if("debito".equals(jrCampo.getName())) {
                    valor = listado.getDouble("debito");
                } else if("credito".equals(jrCampo.getName())) {
                    valor = listado.getDouble("credito");
                } 
            } catch (Exception e) { }
        return valor;
    }
}
