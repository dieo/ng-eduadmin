/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsListadoBoletaOrdenCredito implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsListadoBoletaOrdenCredito(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
        
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("boleta".equals(jrCampo.getName())) {
                valor = listado.getInt("numeroboleta");
            } else if("operacion".equals(jrCampo.getName())) {
                valor = listado.getInt("operacion");
            } else if("fechaaprobacion".equals(jrCampo.getName())) {
                valor = listado.getDate("fechaaprobado");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = listado.getString("cedula");
            } else if("nombreapellido".equals(jrCampo.getName())) {
                valor = listado.getString("nombre")+" "+listado.getString("apellido");
            } else if("comercio".equals(jrCampo.getName())) {
                valor = listado.getString("comercio");
            } else if("plazo".equals(jrCampo.getName())) {
                valor = listado.getInt("plazoaprobado");
            } else if("montoaprobado".equals(jrCampo.getName())) {
                valor = listado.getDouble("montoaprobado");
            } else if("estado".equals(jrCampo.getName())) {
                valor = listado.getString("estado");
            } else if("emisor".equals(jrCampo.getName())) {
                valor = listado.getString("emisor");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
