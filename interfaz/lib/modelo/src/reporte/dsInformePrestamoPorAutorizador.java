/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsInformePrestamoPorAutorizador implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsInformePrestamoPorAutorizador(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = index;
            } else if("autorizador".equals(jrCampo.getName())) {
                valor = listado.getString("autorizador");
            } else if("cedula".equals(jrCampo.getName())) {
                valor = listado.getString("cedula");
            } else if("socio".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("socio"));
            } else if("fechasolicitud".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.convertirToStringDMACorto(listado.getDate("fechasolicitud"));
            } else if("fechaporestado".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.convertirToStringDMACorto(listado.getDate("fechaporestado"));
            } else if("numerosolicitud".equals(jrCampo.getName())) {
                valor = listado.getInt("numerosolicitud");
            } else if("numerooperacion".equals(jrCampo.getName())) {
                valor = listado.getInt("numerooperacion");
            } else if("plazo".equals(jrCampo.getName())) {
                valor = listado.getInt("plazosolicitud");
            } else if("montoaprobado".equals(jrCampo.getName())) {
                valor = listado.getDouble("montosolicitud");
            } else if("tipocredito".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("tipocredito"));;
            } else if("cuenta".equals(jrCampo.getName())) {
                valor = listado.getString("cuenta");
            } else if("estado".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(listado.getString("estado"));
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
