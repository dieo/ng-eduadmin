/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsOrdenCredito implements net.sf.jasperreports.engine.JRDataSource {

    private final especifico.entMovimiento orden;
    private final especifico.entSocio socio;
    private int index = -1;

    public dsOrdenCredito(Object orden, Object socio) {
        this.orden = (especifico.entMovimiento)orden;
        this.socio = (especifico.entSocio)socio;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < 1;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("plazoaprobado".equals(jrCampo.getName())) {
            valor = this.orden.getPlazoAprobado()+" cuota(s)";
        } else if("fechaprimervencimiento".equals(jrCampo.getName())) {
            valor = this.orden.getFechaPrimerVencimiento();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = this.orden.getNumeroOperacion();
        } else if("montoletra".equals(jrCampo.getName())) {
            valor = new utilitario.utiLetra().leerNumero(this.orden.getMontoAprobado());
        } else if("cedula".equals(jrCampo.getName())) {
            valor = this.socio.getCedula();
        } else if("entidad".equals(jrCampo.getName())) {
            valor = this.orden.getEntidad();
        } else if("fechaoperacion".equals(jrCampo.getName())) {
            valor = this.orden.getFechaGeneracion();
        } else if("numeroboleta".equals(jrCampo.getName())) {
            valor = this.orden.getNumeroBoleta();
        } else if("nombreapellido".equals(jrCampo.getName())) {
            valor = this.socio.getNombre()+" "+this.socio.getApellido();
        } else if("monto".equals(jrCampo.getName())) {
            valor = this.orden.getMontoAprobado();
        //} else if("montoretencion".equals(jrCampo.getName())) {
        //    valor = utilitario.utiNumero.redondearMas(this.orden.getMontoCancelacion()/100,0)*100 ;
        } else if("diafechaoperacion".equals(jrCampo.getName())) {
            valor = this.orden.getFechaGeneracion().getDate();
        } else if("mesfechaoperacion".equals(jrCampo.getName())) {
            valor = utilitario.utiFecha.getMeses()[this.orden.getFechaGeneracion().getMonth()];
        } else if("anofechaoperacion".equals(jrCampo.getName())) {
            valor = this.orden.getFechaGeneracion().getYear()+1900;
        }
        return valor;
    }
    
}
