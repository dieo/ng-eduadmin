/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsOperacionFijaLiquidacion implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entOperacionFija operacion;
    private especifico.modDetalleOperacion mod;
    private int index = -1;

    public dsOperacionFijaLiquidacion(Object mod, Object mod2) {
        this.operacion = (especifico.entOperacionFija)mod;
        this.mod = (especifico.modDetalleOperacion)mod2;
        // agrego una linea vacía en caso que no tenga detalle
        if (this.mod.getRowCount()==0) this.mod.insertar(new especifico.entDetalleOperacion());
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("cedula".equals(jrCampo.getName())) {
            valor = operacion.getCedula();
        } else if("apellidonombre".equals(jrCampo.getName())) {
            valor = operacion.getApellidoNombre();
        } else if("estado".equals(jrCampo.getName())) {
            valor = operacion.getEstadoOperacionFija();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = operacion.getNumeroOperacion();
        } else if("cuenta".equals(jrCampo.getName())) {
            valor = operacion.getCuenta();
        } else if("promotor".equals(jrCampo.getName())) {
            valor = operacion.getPromotor();
        } else if("regional".equals(jrCampo.getName())) {
            valor = operacion.getRegional();
        } else if("plazo".equals(jrCampo.getName())) {
            valor = operacion.getPlazo();
        } else if("importe".equals(jrCampo.getName())) {
            valor = operacion.getImporte();
            if (operacion.getImporteTotal()>0) valor = operacion.getImporteTotal();
        } else if("moneda".equals(jrCampo.getName())) {
            valor = operacion.getMoneda();
        } else if("tasainteres".equals(jrCampo.getName())) {
            valor = operacion.getTasaInteres();
        } else if("fechaoperacion".equals(jrCampo.getName())) {
            valor = operacion.getFechaOperacion();
        } else if("fechainicio".equals(jrCampo.getName())) {
            valor = operacion.getFechaPrimerVencimiento();
        } else if("cuota".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCuota() + "/" + this.operacion.getPlazo();
        } else if("fechavencimiento".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFechaVencimiento();
        } else if("monto".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoCapital()+this.mod.getEntidad(index).getMontoInteres();
        } else if("pago".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoCapital()+this.mod.getEntidad(index).getMontoInteres()-this.mod.getEntidad(index).getSaldoCapital()+this.mod.getEntidad(index).getSaldoInteres();
        } else if("saldo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getSaldoCapital()+this.mod.getEntidad(index).getSaldoInteres();
        }
        return valor;
    }
    
}
