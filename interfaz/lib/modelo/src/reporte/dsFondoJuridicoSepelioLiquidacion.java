/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsFondoJuridicoSepelioLiquidacion implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.entFondoJuridicoSepelio fondo;
    private especifico.modDetalleOperacion mod;
    private int index = -1;

    public dsFondoJuridicoSepelioLiquidacion(Object mod, Object mod2) {
        this.fondo = (especifico.entFondoJuridicoSepelio)mod;
        this.mod = (especifico.modDetalleOperacion)mod2;
        // agrego una linea vacía en caso que no tenga detalle
        if (this.mod.getRowCount()==0) this.mod.insertar(new especifico.entDetalleOperacion());
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("cedula".equals(jrCampo.getName())) {
            valor = fondo.getCedula();
        } else if("apellidonombre".equals(jrCampo.getName())) {
            valor = fondo.getApellidoNombre();
        } else if("estado".equals(jrCampo.getName())) {
            valor = fondo.getEstadoFondoJuridicoSepelio();
        } else if("numerooperacion".equals(jrCampo.getName())) {
            valor = fondo.getNumeroOperacion();
        } else if("promotor".equals(jrCampo.getName())) {
            valor = fondo.getPromotor();
        } else if("regional".equals(jrCampo.getName())) {
            valor = fondo.getRegional();
        } else if("importetitular".equals(jrCampo.getName())) {
            valor = fondo.getImporteTitular();
        } else if("cantidadadherente".equals(jrCampo.getName())) {
            valor = fondo.getCantidadAdherente();
        } else if("importeadherente".equals(jrCampo.getName())) {
            valor = fondo.getImporteAdherente();
        } else if("importetotal".equals(jrCampo.getName())) {
            valor = fondo.getImporteTitular()+fondo.getCantidadAdherente()*fondo.getImporteAdherente();
        } else if("moneda".equals(jrCampo.getName())) {
            valor = fondo.getMoneda();
        } else if("fechaoperacion".equals(jrCampo.getName())) {
            valor = fondo.getFechaOperacion();
        } else if("fechainicio".equals(jrCampo.getName())) {
            valor = fondo.getFechaPrimerVencimiento();
        } else if("cuota".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getCuota()+"";
        } else if("fechavencimiento".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getFechaVencimiento();
        } else if("monto".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoCapital()+this.mod.getEntidad(index).getMontoInteres();
        } else if("pago".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getMontoCapital()+this.mod.getEntidad(index).getMontoInteres()-this.mod.getEntidad(index).getSaldoCapital()+this.mod.getEntidad(index).getSaldoInteres();
        } else if("saldo".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getSaldoCapital()+this.mod.getEntidad(index).getSaldoInteres();
        }
        return valor;
    }
    
}
