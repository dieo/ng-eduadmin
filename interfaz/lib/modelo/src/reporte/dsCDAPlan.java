/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsCDAPlan implements net.sf.jasperreports.engine.JRDataSource {

    private final especifico.entCDA entCDA;
    private final especifico.modCDADetalle modCDADetalle;
    private int index = -1;

    public dsCDAPlan(Object mod, Object mod2) {
        this.entCDA = (especifico.entCDA)mod;
        this.modCDADetalle = (especifico.modCDADetalle)mod2;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.modCDADetalle.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("nombre".equals(jrCampo.getName())) {
            valor = entCDA.getNombre();
        } else if("numerocda".equals(jrCampo.getName())) {
            valor = entCDA.getNumeroCDA();
        } else if("cedula".equals(jrCampo.getName())) {
            valor = entCDA.getCedula();
        } else if("serie".equals(jrCampo.getName())) {
            valor = entCDA.getSerie();
        } else if("capital".equals(jrCampo.getName())) {
            valor = entCDA.getCapitalInicial();
        } else if("tasa".equals(jrCampo.getName())) {
            valor = entCDA.getTasaInteres();
        } else if("fechacolocacion".equals(jrCampo.getName())) {  
            valor = entCDA.getFechaInicio();
        } else if("fechavencimiento".equals(jrCampo.getName())) {
            valor = entCDA.getFechaFin();
        } else if("plazo".equals(jrCampo.getName())) {
            valor = entCDA.getDias();
        } else if("estado".equals(jrCampo.getName())) {
            valor = entCDA.getEstadoCDA();
        } else if("observacion".equals(jrCampo.getName())) {
            valor = entCDA.getObservacion();
        } else if("cdatipo".equals(jrCampo.getName())) {
            valor = entCDA.getTipoCDA();
        } else if("capitalizacion".equals(jrCampo.getName())) {
            valor = entCDA.getTipoCapitalizacion();
        } else if("numerocuota".equals(jrCampo.getName())) {
            valor = modCDADetalle.getEntidad(index).getNumeroCuota();
        } else if("fechainicio".equals(jrCampo.getName())) {
            valor = modCDADetalle.getEntidad(index).getFechaInicio();
        } else if("fechafin".equals(jrCampo.getName())) {
            valor = modCDADetalle.getEntidad(index).getFechaFin();
        } else if("dias".equals(jrCampo.getName())) {
            valor = modCDADetalle.getEntidad(index).getCantidadDias();
        } else if("monto".equals(jrCampo.getName())) {
            valor = modCDADetalle.getEntidad(index).getInteres();
        } 
        return valor;
    }
    
}
