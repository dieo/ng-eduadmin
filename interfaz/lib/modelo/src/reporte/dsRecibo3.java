/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsRecibo3 implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet recibo;
    private String sconcepto = "";
    private String scodigo = "";
    private String satraso = "";
    private String sactual = "";
    private String ssaldo = "";
    private String sexoneracion = "";
    private double utotalAtraso = 0.0;
    private double utotalActual = 0.0;
    private double utotalSaldo = 0.0;
    private double utotalExoneracion = 0.0;
    private String ssubtotal = "";
    private int index = 0;
    private int totalDetalle = 0;

    public dsRecibo3(Object mod) {
        this.recibo = (java.sql.ResultSet)mod;
        try {
            this.recibo.beforeFirst();
            while (this.recibo.next()){
                sconcepto += utilitario.utiCadena.convertirMayusMinus(recibo.getString("producto") + " " + recibo.getString("referencia")) + "\n";
                scodigo += utilitario.utiNumero.getMascara(recibo.getInt("idproducto"),0) + "\n";
                utotalAtraso += recibo.getDouble("atraso");
                utotalActual += recibo.getDouble("actual");
                utotalSaldo += recibo.getDouble("saldo");
                utotalExoneracion += recibo.getDouble("exoneraciondetalle");
                satraso += utilitario.utiNumero.getMascara(recibo.getDouble("atraso"),0) + "\n";
                sactual += utilitario.utiNumero.getMascara(recibo.getDouble("actual"),0) + "\n";
                ssaldo += utilitario.utiNumero.getMascara(recibo.getDouble("saldo"),0) + "\n";
                sexoneracion += utilitario.utiNumero.getMascara(recibo.getDouble("exoneraciondetalle"),0) + "\n";
                double subtotal = recibo.getDouble("cantidad") * recibo.getDouble("monto");
                ssubtotal += utilitario.utiNumero.getMascara(subtotal,0) + "\n";
            }
        } catch (Exception e) { }
        try {
            this.recibo.last();
            totalDetalle = this.recibo.getRow()+1;
            this.recibo.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { recibo.next();
        } catch (Exception e) { }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if (index==1) {
            try {
                if ("fecha".equals(jrCampo.getName())) {
                    int idia = recibo.getDate("fecharecibo").getDate();
                    String smes = utilitario.utiCadena.convertirMayusMinus(utilitario.utiFecha.getMes(recibo.getDate("fecharecibo")));
                    int iano = utilitario.utiFecha.getAnho(recibo.getDate("fecharecibo"));
                    valor = "Asunción, "+idia+" de "+smes+" de "+iano;
                } else if ("ruc".equals(jrCampo.getName())) {
                    valor = recibo.getString("ruc");
                } else if ("razonsocial".equals(jrCampo.getName())) {
                    valor = utilitario.utiCadena.convertirMayusMinus(recibo.getString("razonsocial"));
                } else if ("numero".equals(jrCampo.getName())) {
                    valor = "Nº  "+utilitario.utiCadena.getNumeroDocumento(recibo.getString("local"), recibo.getString("puntoexpedicion"), recibo.getInt("numerorecibo"));
                } else if ("numerocorto".equals(jrCampo.getName())) {
                    valor = utilitario.utiNumero.convertirToString(recibo.getInt("numerorecibo"));
                } else if ("importeletra".equals(jrCampo.getName())) {
                    String sletra = new utilitario.utiLetra().leerNumero(recibo.getDouble("total")).toLowerCase();
                    valor = sletra.substring(0, 1).toUpperCase() + sletra.substring(1, sletra.length()) + ".-";
                } else if ("concepto".equals(jrCampo.getName())) {
                    valor = sconcepto;
                } else if ("monto".equals(jrCampo.getName())) {
                    valor = recibo.getDouble("monto");
                } else if ("importe".equals(jrCampo.getName())) {
                    valor = recibo.getDouble("total");
                } else if ("codigo".equals(jrCampo.getName())) {
                    valor = scodigo;
                } else if ("atraso".equals(jrCampo.getName())) {
                    valor = satraso;
                } else if ("actual".equals(jrCampo.getName())) {
                    valor = sactual;
                } else if ("saldo".equals(jrCampo.getName())) {
                    valor = ssaldo;
                } else if ("exoneracion".equals(jrCampo.getName())) {
                    valor = sexoneracion;
                } else if ("total".equals(jrCampo.getName())) {
                    valor = ssubtotal;
                } else if ("totalatraso".equals(jrCampo.getName())) {
                    valor = utotalAtraso;
                } else if ("totalactual".equals(jrCampo.getName())) {
                    valor = utotalActual;
                } else if ("totalsaldo".equals(jrCampo.getName())) {
                    valor = utotalSaldo;
                } else if ("totalexoneracion".equals(jrCampo.getName())) {
                    valor = utotalExoneracion;
                }
            } catch (Exception e) {
            }
        }
        return valor;
    }
    
}
