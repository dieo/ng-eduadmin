/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsRubro implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modRubro mod;
    private int index = -1;

    public dsRubro(Object mod) {
        this.mod = (especifico.modRubro)mod;
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;  
        if("descripcion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getDescripcion();
        } else if("jubilacion".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getJubilacion();
        } else if("iva".equals(jrCampo.getName())) {
            valor = this.mod.getEntidad(index).getIva();
        }
        return valor;
    }
    
}
