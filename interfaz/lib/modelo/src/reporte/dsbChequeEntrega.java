/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Lic. Didier Barreto
 */
public class dsbChequeEntrega implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modbChequeEntrega mod;
    private int index = -1;
    
    public dsbChequeEntrega(Object mod) {
        this.mod = (especifico.modbChequeEntrega)mod;
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        return ++index < this.mod.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        if("numerocheque".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNumeroCheque();
        } else if("fechaemision".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaEmisionCheque();
        } else if("fechaenvio".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaEnvio();
        } else if("fechaentrega".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaEntrega();
        } else if("fechadevolucion".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getFechaDevolucion();
        } else if("receptor".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNombre();
        } else if("telefono".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getTelefono();
        } else if("centrocosto".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getCentroCosto();
        } else if("monto".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getMonto();
        } else if("numerocuenta".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getNumeroCuenta();
        } else if("cuentabanco".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getBancoCuenta();
        } else if("estadoentrega".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getEstadoCheque();
        } else if("concepto".equals(jrCampo.getName())) {
            valor = mod.getEntidad(index).getDescripcion();
        } 
        return valor;
    }
    
}
