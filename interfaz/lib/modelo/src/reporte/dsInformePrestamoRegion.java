/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsInformePrestamoRegion implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet listado;
    private int index = 0; // desde 0 porque el ResultSet el primer registro es 1
    private int total = 0;

    public dsInformePrestamoRegion(Object mod) {
        this.listado = (java.sql.ResultSet)mod;
        try {
            this.listado.last();
            total = this.listado.getRow()+1;
            this.listado.beforeFirst();
        } catch (Exception e) { }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { this.listado.next();
        } catch (Exception e) { }
        return ++index < total;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("item".equals(jrCampo.getName())) {
                valor = listado.getInt("item");
            } else if("zona".equals(jrCampo.getName())) {
                valor = listado.getString("zona");
            } else if("region".equals(jrCampo.getName())) {
                valor = listado.getString("region");
            } else if("cantidadprestamo".equals(jrCampo.getName())) {
                valor = listado.getInt("cantidadprestamo");
            } else if("montocapital".equals(jrCampo.getName())) {
                valor = listado.getDouble("prestamo");
            } else if("montointeres".equals(jrCampo.getName())) {
                valor = listado.getDouble("interes");
            } else if("montocheque".equals(jrCampo.getName())) {
                valor = listado.getDouble("montocheque");
            } else if("cantidadfinanciera".equals(jrCampo.getName())) {
                valor = listado.getInt("cantidadfinanciera");
            } else if("montofinanciera".equals(jrCampo.getName())) {
                valor = listado.getDouble("financiera");
            } else if("cantidadordencredito".equals(jrCampo.getName())) {
                valor = listado.getInt("cantidadoc");
            } else if("montoordencredito".equals(jrCampo.getName())) {
                valor = listado.getDouble("ordencredito");
            } else if("cantidadreprogramacion".equals(jrCampo.getName())) {
                valor = listado.getInt("cantidadrep");
            } else if("montoreprogramacion".equals(jrCampo.getName())) {
                valor = listado.getDouble("reprogramacion");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
