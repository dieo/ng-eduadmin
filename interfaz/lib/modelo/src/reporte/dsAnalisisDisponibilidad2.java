/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsAnalisisDisponibilidad2 implements net.sf.jasperreports.engine.JRDataSource {

    private especifico.modMovimientoDescuento descuento;
    private especifico.modReferenciaPersonal personal;
    private especifico.modAnalisisDisponibilidad analisis;
    private java.sql.ResultSet persona;
    private boolean primeraVez = true;
    private int index = -1;
    private int indexPersonal = 0;
    private int indexDescuento = 0;
    private String snombreApellidoReferencia = "";
    private String sdireccionReferencia = "";
    private String stelefonoReferencia = "";

    public dsAnalisisDisponibilidad2(Object mod, Object mod2, Object mod3, Object mod4) {
        this.descuento = (especifico.modMovimientoDescuento)mod;
        this.personal = (especifico.modReferenciaPersonal)mod2;
        this.analisis = (especifico.modAnalisisDisponibilidad)mod3;
        this.persona = (java.sql.ResultSet)mod4;
        try {
            this.persona.beforeFirst();
        } catch (Exception e) { }
        for (int i=0; i<personal.getRowCount(); i++) {
            snombreApellidoReferencia = snombreApellidoReferencia + personal.getEntidad(i).getNombre()+" "+personal.getEntidad(i).getApellido()+"\n";
            sdireccionReferencia = sdireccionReferencia + personal.getEntidad(i).getDireccion()+"\n";
            stelefonoReferencia = stelefonoReferencia + personal.getEntidad(i).getTelefono()+"\n";
        }
        if (personal.getRowCount()>0) {
            snombreApellidoReferencia = snombreApellidoReferencia.substring(0, snombreApellidoReferencia.length()-1);
            sdireccionReferencia = sdireccionReferencia.substring(0, sdireccionReferencia.length()-1);
            stelefonoReferencia = stelefonoReferencia.substring(0, stelefonoReferencia.length()-1);
            if (snombreApellidoReferencia==null) snombreApellidoReferencia = "";
            if (sdireccionReferencia==null) sdireccionReferencia = "";
            if (stelefonoReferencia==null) stelefonoReferencia = "";
        }
    }

    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        if (primeraVez) {
            primeraVez = false;
            try { this.persona.next();
            } catch (Exception e) { }
        }
        return ++index < descuento.getRowCount();
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if ("cedula".equals(jrCampo.getName())) {
                valor = persona.getString("cedula");
            } else if ("nombreapellido".equals(jrCampo.getName())) {
                valor = persona.getString("nombre") + " " + persona.getString("apellido");
            } else if ("tipocasa".equals(jrCampo.getName())) {
                valor = persona.getString("tipocasa");
            } else if ("direccion".equals(jrCampo.getName())) {
                valor = persona.getString("direccion");
            } else if ("numero".equals(jrCampo.getName())) {
                valor = persona.getInt("numerocasa");
            } else if ("barrio".equals(jrCampo.getName())) {
                valor = persona.getString("barrio");
            } else if ("ciudad".equals(jrCampo.getName())) {
                valor = persona.getString("ciudadresidencia");
            } else if ("celular".equals(jrCampo.getName())) {
                valor = persona.getString("telefonocelular");
            } else if ("lineabaja".equals(jrCampo.getName())) {
                valor = persona.getString("telefonolineabaja");
            } else if ("institucion".equals(jrCampo.getName())) {
                valor = persona.getString("institucion");
            } else if ("fechaingreso".equals(jrCampo.getName())) {
                valor = persona.getDate("fechaingreso");
            } else if ("direccionlaboral".equals(jrCampo.getName())) {
                valor = persona.getString("direccionlaboral");
            } else if ("telefonolaboral".equals(jrCampo.getName())) {
                valor = persona.getString("telefonolaboral");
            } else if ("regional".equals(jrCampo.getName())) {
                valor = persona.getString("regional");
            } else if ("cargo".equals(jrCampo.getName())) {
                valor = persona.getString("cargo");
            } else if ("nombreapellidoreferencia".equals(jrCampo.getName())) {
                valor = snombreApellidoReferencia;
            } else if ("direccionreferencia".equals(jrCampo.getName())) {
                valor = sdireccionReferencia;
            } else if ("telefonoreferencia".equals(jrCampo.getName())) {
                valor = stelefonoReferencia;
            } else if ("rubro".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getRubro();
            } else if ("giraduria".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getGiraduria();
            } else if ("fechacalculo".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getFecha();
            } else if ("sueldobruto".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getSueldo();
            } else if ("porcentajejubilacion".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getPorcentajeJubilacion();
            } else if ("jubilacion".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getJubilacion();
            } else if ("porcentajeiva".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getPorcentajeIva();
            } else if ("iva".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getIva();
            } else if ("sueldoliquido".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getSueldoLiquido();
            } else if ("porcentajedisponible".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getPorcentajeDisponible();
            } else if ("disponible".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getDisponible();
            } else if ("totaldescuentoactual".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getTotalDescuentoActual();
            } else if ("totaldescuentosiguiente".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getTotalDescuentoSiguiente();
            } else if ("saldodisponibleactual".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getSaldoDisponibleActual();
            } else if ("saldodisponiblesiguiente".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getSaldoDisponibleSiguiente();
            } else if ("saldo75actual".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getSaldo75Actual();
            } else if ("saldo75siguiente".equals(jrCampo.getName())) {
                valor = analisis.getEntidad(0).getSaldo75Siguiente();
            } else if ("descuento".equals(jrCampo.getName())) {
                valor = descuento.getEntidad(index).getDescuento();
            } else if ("importeactual".equals(jrCampo.getName())) {
                valor = descuento.getEntidad(index).getImporteActual();
            } else if ("importesiguiente".equals(jrCampo.getName())) {
                valor = descuento.getEntidad(index).getImporteSiguiente();
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
