/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

/**
 *
 * @author Ing. Edison Martinez
 */
public class dsFactura implements net.sf.jasperreports.engine.JRDataSource {

    private java.sql.ResultSet factura;
    private java.sql.ResultSet detalle;
    private int index = 0;
    private int totalDetalle = 0;

    public dsFactura(Object mod, Object mod2) {
        this.factura = (java.sql.ResultSet)mod;
        this.detalle = (java.sql.ResultSet)mod2;
        try {
            this.detalle.last();
            totalDetalle = this.detalle.getRow()+1;
            this.detalle.beforeFirst();
        } catch (Exception e) { }
    }
    
    public net.sf.jasperreports.engine.JasperPrint cargarInforme(String sruta, java.util.HashMap parametro) throws net.sf.jasperreports.engine.JRException {
        net.sf.jasperreports.engine.JasperPrint informe = net.sf.jasperreports.engine.JasperFillManager.fillReport(sruta, parametro, this);
        return informe;
    }
    
    @Override
    public boolean next() throws net.sf.jasperreports.engine.JRException {
        try { detalle.next();
        } catch (Exception e) { }
        if (index==0) { // la primera vez hace el salto al primer registro
            try { factura.next();
            } catch (Exception e) { }
        }
        return ++index < this.totalDetalle;
    }

    @Override
    public Object getFieldValue(net.sf.jasperreports.engine.JRField jrCampo) throws net.sf.jasperreports.engine.JRException { 
        Object valor = null;
        try {
            if("fecha".equals(jrCampo.getName())) {
                valor = utilitario.utiFecha.convertirToStringDMA(factura.getDate("fechafactura"));
            } else if("contado".equals(jrCampo.getName())) {
                if (factura.getString("tipofactura").contains("CONTADO")) valor = "X";
            } else if("credito".equals(jrCampo.getName())) {
                if (factura.getString("tipofactura").contains("CREDITO")) valor = "X";
            } else if("numerofactura".equals(jrCampo.getName())) {
                valor = factura.getInt("numerofactura");
            } else if("ruc".equals(jrCampo.getName())) {
                valor = factura.getString("ruc");
            } else if("razonsocial".equals(jrCampo.getName())) {
                valor = factura.getString("razonsocial");
            } else if("notaremision".equals(jrCampo.getName())) {
                valor = factura.getInt("notaremision");
            } else if("direccion".equals(jrCampo.getName())) {
                valor = factura.getString("direccion");
            } else if("telefono".equals(jrCampo.getName())) {
                valor = factura.getString("telefono");
            } else if("montoexenta".equals(jrCampo.getName())) {
                valor = detalle.getDouble("exenta");
            } else if("montogravada5".equals(jrCampo.getName())) {
                valor = detalle.getDouble("gravada5");
            } else if("montogravada10".equals(jrCampo.getName())) {
                valor = detalle.getDouble("gravada10");
            } else if("total".equals(jrCampo.getName())) {
                valor = factura.getDouble("total");
            } else if("montoletra".equals(jrCampo.getName())) {
                String sletra = new utilitario.utiLetra().leerNumero(factura.getDouble("total")).toLowerCase();
                valor = sletra.substring(0,1).toUpperCase()+sletra.substring(1,sletra.length())+".-";
            } else if("cantidad".equals(jrCampo.getName())) {
                valor = detalle.getInt("cantidad");
            } else if("descripcion".equals(jrCampo.getName())) {
                valor = utilitario.utiCadena.convertirMayusMinus(detalle.getString("producto"))+" "+detalle.getString("referencia");
            } else if("preciounitario".equals(jrCampo.getName())) {
                valor = detalle.getDouble("precio");
            } else if("exenta".equals(jrCampo.getName())) {
                valor = factura.getDouble("exenta");
            } else if("impuesto5".equals(jrCampo.getName())) {
                valor = factura.getDouble("impuesto5");
            } else if("impuesto10".equals(jrCampo.getName())) {
                valor = factura.getDouble("impuesto10");
            }
        } catch (Exception e) { }
        return valor;
    }
    
}
