/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modMovimiento implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";
    
    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entMovimiento getEntidad() {
        return (especifico.entMovimiento)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entMovimiento nuevo) {
        nuevo.copiar((especifico.entMovimiento)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "FECHA";
            case 1: return "NUMERO";
            case 2: return "CEDULA";
            case 3: return "MONTO";
            case 4: return "PLAZO";
            case 5: return "ESTADO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return java.util.Date.class;
            case 1: return Integer.class;
            case 2: return String.class;
            case 3: return Double.class;
            case 4: return Integer.class;
            case 5: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entMovimiento ent;
        ent = (especifico.entMovimiento)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getFechaSolicitud();
            case 1: return ent.getNumeroSolicitud();
            case 2: return ent.getCedula();
            case 3: return ent.getMontoSolicitud();
            case 4: return ent.getPlazoSolicitud();
            case 5: return ent.getEstadoSolicitud();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entMovimiento ent;
        ent = (especifico.entMovimiento)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setFechaSolicitud((java.util.Date)aValue); break;
            case 1: ent.setNumeroSolicitud((Integer)aValue); break;
            case 2: ent.setCedula((String)aValue); break;
            case 3: ent.setMontoSolicitud((Double)aValue); break;
            case 4: ent.setPlazoSolicitud((Integer)aValue); break;
            case 5: ent.setEstadoSolicitud((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entMovimiento getEntidad(int rowIndex) {
        return (especifico.entMovimiento)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entMovimiento nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entMovimiento nuevo, int rowIndex) {
        nuevo.copiar((especifico.entMovimiento)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entMovimiento().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    /*public void establecerFormato(javax.swing.JTable tbl) {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(60);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(90);
        // alineación a la derecha
        javax.swing.table.DefaultTableCellRenderer tcr = new javax.swing.table.DefaultTableCellRenderer();
        tcr.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tbl.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tbl.getColumnModel().getColumn(2).setCellRenderer(tcr);
        tbl.getColumnModel().getColumn(3).setCellRenderer(tcr);
        tbl.getColumnModel().getColumn(4).setCellRenderer(tcr);
    }*/

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(60);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(50);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(20);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(90);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public boolean esValido(especifico.modDetallePlazo mod, especifico.entDetallePlazo ent) {
        for (int i=0; i<mod.getRowCount(); i++) {
            if (this.getEntidad(this.tbl.getSelectedRow()).getIdTipoCredito()==mod.getEntidad(i).getIdTipoCredito() && this.getEntidad(this.tbl.getSelectedRow()).getPlazoSolicitud()>=mod.getEntidad(i).getPlazoInicial() && this.getEntidad(this.tbl.getSelectedRow()).getPlazoSolicitud()<=mod.getEntidad(i).getPlazoFinal() && mod.getEntidad(i).getActivo()==true) {
                this.smensaje = "Plazo fuera del rango establecido";
            }
        }
        return true;
    }

    public String getMensaje() {
        return this.smensaje;
    }
    
}
