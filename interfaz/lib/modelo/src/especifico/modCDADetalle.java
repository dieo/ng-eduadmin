/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

import java.sql.SQLException;


/**
 *
 * @author Lic. Didier Barreto
 */
public class modCDADetalle implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";
    
    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public void modificar(especifico.entCDADetalle nuevo) {
        nuevo.copiar((especifico.entCDADetalle)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "Nº DE CUOTA";
            case 1: return "FECHA INICIO";
            case 2: return "FECHA FIN";
            case 3: return "DIAS";
            case 4: return "CAPITAL";
            case 5: return "INTERES";
            case 6: return "RETIRO CAPITAL";
            case 7: return "RETIRO INTERES";
            case 8: return "FECHA RETIRO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return java.util.Date.class;
            case 2: return java.util.Date.class;
            case 3: return Integer.class;
            case 4: return Double.class;
            case 5: return Double.class;
            case 6: return Double.class;
            case 7: return Double.class;
            case 8: return java.util.Date.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entCDADetalle ent;
        ent = (especifico.entCDADetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getNumeroCuota();
            case 1: return ent.getFechaInicio();
            case 2: return ent.getFechaFin();
            case 3: return ent.getCantidadDias();
            case 4: return ent.getCapital();
            case 5: return ent.getInteres();
            case 6: return ent.getRetiroCapital();
            case 7: return ent.getRetiroInteres();
            case 8: return ent.getFechaRetiro();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entCDADetalle ent;
        ent = (especifico.entCDADetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setNumeroCuota((Integer)aValue); break;
            case 1: ent.setFechaInicio((java.util.Date)aValue); break;
            case 2: ent.setFechaFin((java.util.Date)aValue); break;
            case 3: ent.setCantidadDias((Integer)aValue); break;
            case 4: ent.setCapital((Double)aValue); break;
            case 5: ent.setInteres((Double)aValue); break;
            case 6: ent.setRetiroCapital((Double)aValue); break;
            case 7: ent.setRetiroInteres((Double)aValue); break;
            case 8: ent.setFechaRetiro((java.util.Date)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }
    
    public especifico.entCDADetalle getEntidad() {
        return (especifico.entCDADetalle)(data.get(tbl.getSelectedRow()));
    }

    public especifico.entCDADetalle getEntidad(int rowIndex) {
        return (especifico.entCDADetalle)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar(especifico.entCDADetalle nuevo) {
        try {
            data.add(nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entCDADetalle nuevo, int rowIndex) {
        nuevo.copiar((especifico.entCDADetalle)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar(int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }
    
    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entCDADetalle().cargar(rs));
            }
        } catch (SQLException e) {
        }
    }

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(7).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(8).setPreferredWidth(30);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        //tbl.setDefaultRenderer(short.class, new generico.modCeldaString());
        /*tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());*/
    }
    
    public void calcularCampos(double dtasaInteres, int idiaExtraccion) {
        double dtasaDia = dtasaInteres/365;
        double dsaldo = 0;
        java.util.Date dfecha;
        this.insertar(new especifico.entCDADetalle()); // inserta una línea más
        this.getEntidad(this.getRowCount()-1).setNumeroCuota((short) (this.getEntidad(this.getRowCount()-2).getNumeroCuota()+1));
        dfecha = (java.util.Date) this.getEntidad(this.getRowCount()-2).getFechaFin().clone();
        dfecha.setDate(1); // asigno 1 para aumentar el mes
        dfecha.setMonth(dfecha.getMonth()+1); // aumento el mes para calcular el siguiente
        if (idiaExtraccion==31 && (dfecha.getMonth()+1==4 || dfecha.getMonth()+1==6 || dfecha.getMonth()+1==9 || dfecha.getMonth()+1==11)) idiaExtraccion=30;
        if (idiaExtraccion>=29 && idiaExtraccion<=31 && dfecha.getMonth()+1==2) {
            if (utilitario.utiFecha.esBisiesto(dfecha.getYear()+1900)) idiaExtraccion=28;
            else idiaExtraccion=29;
        }
        dfecha.setDate(idiaExtraccion); // asigno el dia de extraccion
        this.getEntidad(this.getRowCount()-1).setFechaFin(dfecha); // asigna a la ultima linea la siguiente fecha
        dfecha = this.getEntidad(0).getFechaFin();
        for (int i=0; i<this.getRowCount(); i++) {
            this.getEntidad(i).setCantidadDias((short) utilitario.utiFecha.obtenerDiferenciaDia(dfecha, this.getEntidad(i).getFechaFin())); // calcula y asigna la cantidad de dias\
            //this.getEntidad(i).setInteres(this.getEntidad(i).getCantidadDias() * dtasaDia * dsaldo / 100); // calcula y asigna el interes
            //dsaldo += this.getEntidad(i).getImporte(); // incrementa el importe para calculo de saldo
            //his.getEntidad(i).setSaldo(dsaldo); // calcula y asigna el saldo
            dfecha = (java.util.Date)this.getEntidad(i).getFechaFin().clone(); // guarda la fecha actual para luego obtener la diferencia
        }
    }
    
    public especifico.entCDADetalle calcularTotales() {
        especifico.entCDADetalle ent = new especifico.entCDADetalle();
        for (int i=0; i<this.getRowCount(); i++) {
            ent.setCantidadDias(ent.getCantidadDias() + this.getEntidad(i).getCantidadDias());
            ent.setInteres(ent.getInteres() + this.getEntidad(i).getInteres());
            ent.setRetiroInteres(ent.getRetiroInteres()+ this.getEntidad(i).getRetiroInteres());
            ent.setRetiroInteres(ent.getRetiroCapital()+ this.getEntidad(i).getRetiroCapital());
        }
        //ent.setSaldo(ent.getInteres() + ent.getImporte());
        return ent;
    }
    
    public double getTotalInteres() {
        double total = 0;
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getEstadoRegistro()!=generico.entConstante.estadoregistro_eliminado){
                total += this.getEntidad(i).getInteres();
            }
        }
        return total;
    }

    public double getTotalRetiroCapital() {
        double total = 0;
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getEstadoRegistro()!=generico.entConstante.estadoregistro_eliminado){
                total += this.getEntidad(i).getRetiroCapital();
            }
        }
        return total;
    }
}
