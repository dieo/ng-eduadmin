/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modDetalleCancelacionCabecera implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    public especifico.modDetalleIngreso detalle = new especifico.modDetalleIngreso();
    public especifico.modDetalleIngreso detalleVer = new especifico.modDetalleIngreso();
    private String smensaje = "";

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entDetalleCancelacionCabecera getEntidad() {
        return (especifico.entDetalleCancelacionCabecera)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entDetalleCancelacionCabecera nuevo) {
        nuevo.copiar((especifico.entDetalleCancelacionCabecera)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 13;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "OPERACION";
            case 1: return "DESC.";
            case 2: return "NRO.OP.";
            case 3: return "FECHA OP.";
            case 4: return "PLAZO";
            case 5: return "TASA";
            case 6: return "CAPITAL";
            case 7: return "INTERES";
            case 8: return "ATRASO";
            case 9: return "SALDO";
            case 10: return "EXONERACION";
            case 11: return "A";
            case 12: return "C";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return String.class;
            case 2: return Integer.class;
            case 3: return java.util.Date.class;
            case 4: return Integer.class;
            case 5: return Double.class;
            case 6: return Double.class;
            case 7: return Double.class;
            case 8: return Double.class;
            case 9: return Double.class;
            case 10: return Double.class;
            case 11: return Boolean.class;
            case 12: return Boolean.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entDetalleCancelacionCabecera ent;
        ent = (especifico.entDetalleCancelacionCabecera)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getCuenta();
            case 1: return ent.getDescripcion();
            case 2: return ent.getNumeroOperacion();
            case 3: return ent.getFechaOperacion();
            case 4: return ent.getPlazo();
            case 5: return ent.getTasaInteres();
            case 6: return ent.getMontoCapital();
            case 7: return ent.getMontoInteres();
            case 8: return ent.getMontoAtraso();
            case 9: return ent.getMontoSaldo();
            case 10: return ent.getMontoExoneracion();
            case 11: return ent.getAtraso();
            case 12: return ent.getCancela();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entDetalleCancelacionCabecera ent;
        ent = (especifico.entDetalleCancelacionCabecera)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setCuenta((String)aValue); break;
            case 1: ent.setDescripcion((String)aValue); break;
            case 2: ent.setNumeroOperacion((Integer)aValue); break;
            case 3: ent.setFechaOperacion((java.util.Date)aValue); break;
            case 4: ent.setPlazo((Integer)aValue); break;
            case 5: ent.setTasaInteres((Double)aValue); break;
            case 6: ent.setMontoCapital((Double)aValue); break;
            case 7: ent.setMontoInteres((Double)aValue); break;
            case 8: ent.setMontoAtraso((Double)aValue); break;
            case 9: ent.setMontoSaldo((Double)aValue); break;
            case 10: ent.setMontoExoneracion((Double)aValue); break;
            case 11: ent.setAtraso((Boolean)aValue); break;
            case 12: ent.setCancela((Boolean)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entDetalleCancelacionCabecera getEntidad(int rowIndex) {
        return (especifico.entDetalleCancelacionCabecera)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entDetalleCancelacionCabecera nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entDetalleCancelacionCabecera nuevo, int rowIndex) {
        nuevo.copiar((especifico.entDetalleCancelacionCabecera)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entDetalleCancelacionCabecera().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        // establece el tamaño de las columnas (620-20)
        tbl.getColumnModel().getColumn(0).setMinWidth(60);
        tbl.getColumnModel().getColumn(1).setMinWidth(20);
        tbl.getColumnModel().getColumn(2).setMinWidth(45);
        tbl.getColumnModel().getColumn(3).setMinWidth(50);
        tbl.getColumnModel().getColumn(4).setMinWidth(20);
        tbl.getColumnModel().getColumn(5).setMinWidth(20);
        tbl.getColumnModel().getColumn(6).setMinWidth(70);
        tbl.getColumnModel().getColumn(7).setMinWidth(70);
        tbl.getColumnModel().getColumn(8).setMinWidth(70);
        tbl.getColumnModel().getColumn(9).setMinWidth(70);
        tbl.getColumnModel().getColumn(10).setMinWidth(65);
        tbl.getColumnModel().getColumn(11).setMinWidth(20);
        tbl.getColumnModel().getColumn(12).setMinWidth(20);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox());*/
    }
    
    public String getMensaje() {
        return this.smensaje;
    }

    public void crearCabecera() {
        int iidMovimiento=0, iidCuenta=0; // para realizar el corte
        int iindice = -1;
        String movcue1="", movcue2="";
        for (int i=0; i<detalle.getRowCount(); i++) {
            movcue1 = utilitario.utiNumero.convertirToString(detalle.getEntidad(i).getIdMovimiento())+"-"+utilitario.utiNumero.convertirToString(detalle.getEntidad(i).getIdCuenta());
            movcue2 = utilitario.utiNumero.convertirToString(iidMovimiento)+"-"+utilitario.utiNumero.convertirToString(iidCuenta);
            if (!movcue1.equals(movcue2)) { // realizo el corte para insertar entidad en la cabecera
                iidMovimiento = detalle.getEntidad(i).getIdMovimiento(); // guardo el nuevo idmovimiento
                iidCuenta = detalle.getEntidad(i).getIdCuenta(); // guardo el nuevo idmovimiento
                this.insertar(new especifico.entDetalleCancelacionCabecera());
                this.getEntidad(this.getRowCount()-1).setIdMovimiento(detalle.getEntidad(i).getIdMovimiento());
                this.getEntidad(this.getRowCount()-1).setIdMovimientoCancela(detalle.getEntidad(i).getIdMovimientoCancela());
                this.getEntidad(this.getRowCount()-1).setTabla(detalle.getEntidad(i).getTabla());
                this.getEntidad(this.getRowCount()-1).setIdDetalleOperacion(detalle.getEntidad(i).getIdDetalleOperacion());
                this.getEntidad(this.getRowCount()-1).setNumeroOperacion(detalle.getEntidad(i).getNumeroOperacion());
                this.getEntidad(this.getRowCount()-1).setFechaOperacion((java.util.Date)detalle.getEntidad(i).getFechaOperacion().clone());
                this.getEntidad(this.getRowCount()-1).setTasaInteres(detalle.getEntidad(i).getTasaInteresMoratorio());
                this.getEntidad(this.getRowCount()-1).setPlazo(detalle.getEntidad(i).getPlazo());
                this.getEntidad(this.getRowCount()-1).setAtraso(detalle.getEntidad(i).getAtraso());
                this.getEntidad(this.getRowCount()-1).setCancela(detalle.getEntidad(i).getCancela());
                this.getEntidad(this.getRowCount()-1).setIdCuenta(detalle.getEntidad(i).getIdCuenta());
                this.getEntidad(this.getRowCount()-1).setCuenta(detalle.getEntidad(i).getCuenta());
                this.getEntidad(this.getRowCount()-1).setDescripcion(detalle.getEntidad(i).getDescripcion());
                this.getEntidad(this.getRowCount()-1).setPrioridad(detalle.getEntidad(i).getPrioridad());
                iindice ++;
                if (iindice <= this.getRowCount()) this.getEntidad(iindice).setDesde(i);
            }
            if (iindice <= this.getRowCount()) this.getEntidad(iindice).setHasta(i);
        }
    }

    public void establecerMovimiento(int iidMovimientoCancela) {
        for (int i=0; i<detalle.getRowCount(); i++) {
            detalle.getEntidad(i).setIdMovimientoCancela(iidMovimientoCancela);
        }
    }

    public void establecerAtraso(java.util.Date dfecha) {
        if (dfecha == null) return;
        java.util.Date dfechaSaldo = utilitario.utiFecha.getUltimoDia(dfecha);
        for (int i=0; i<detalle.getRowCount(); i++) {
            if (utilitario.utiFecha.getAMD(detalle.getEntidad(i).getFechaVencimiento()) < utilitario.utiFecha.getAMD(dfechaSaldo) && detalle.getEntidad(i).getMontoSaldo()>0) {
                detalle.getEntidad(i).setEstado(especifico.entDetalleCancelacion.estado_atraso);
            }
        }
    }

    public void establecerCobro(java.util.Date dfecha, boolean esCuotaMes) {
        if (dfecha == null) return;
        java.util.Date dfechaSaldo = utilitario.utiFecha.getUltimoDia(dfecha);
        for (int k=0; k<this.getRowCount(); k++) {
            if (!this.getEntidad(k).getCancela()) {
                for (int i=this.getEntidad(k).getDesde(); i<=this.getEntidad(k).getHasta(); i++) {
                    if (utilitario.utiFecha.getAMD(detalle.getEntidad(i).getFechaVencimiento()) == utilitario.utiFecha.getAMD(dfechaSaldo) && detalle.getEntidad(i).getMontoSaldo()>0) {
                        if (esCuotaMes) detalle.getEntidad(i).setEstado(especifico.entDetalleCancelacion.estado_cobro);
                        else detalle.getEntidad(i).setEstado("");
                    }
                }
            }
        }
    }

    public void rellenarSaldo(boolean rellenar, java.util.Date dfecha, boolean esCuotaMes, int indice) {
        java.util.Date dfechaSaldo = utilitario.utiFecha.getUltimoDia(dfecha);
        for (int i=this.getEntidad(indice).getDesde(); i<=this.getEntidad(indice).getHasta(); i++) {
            if (rellenar) {
                if (utilitario.utiFecha.getAMD(detalle.getEntidad(i).getFechaVencimiento()) == utilitario.utiFecha.getAMD(dfechaSaldo)) {
                    detalle.getEntidad(i).setEstado(especifico.entDetalleCancelacion.estado_cobro);
                }
                if (utilitario.utiFecha.getAMD(detalle.getEntidad(i).getFechaVencimiento()) > utilitario.utiFecha.getAMD(dfechaSaldo)) {
                    detalle.getEntidad(i).setEstado(especifico.entDetalleCancelacion.estado_saldo);
                }
            } else {
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_saldo)) {
                    detalle.getEntidad(i).setEstado("");
                }
                if (!esCuotaMes && detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_cobro)) {
                    detalle.getEntidad(i).setEstado("");
                }
            }
        }
    }

    public void rellenarExoneracion(boolean rellenar, java.util.Date dfecha, int indice) {
        for (int i=this.getEntidad(indice).getDesde(); i<=this.getEntidad(indice).getHasta(); i++) {
            if (rellenar) {
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_saldo)) {
                    detalle.getEntidad(i).setMontoExoneracion(detalle.getEntidad(i).getMontoSaldo() * detalle.getEntidad(i).getExoneracion() / 100);
                }
            } else {
                detalle.getEntidad(i).setMontoExoneracion(0.0);
            }
        }
    }
    
    public boolean esValidoRellenar(int indice) {
        int icantidadAtraso = 0;
        int icantidadCobro = 0;
        int icantidadSaldo = 0;
        int icantidadTotal = 0;
        for (int i=this.getEntidad(indice).getDesde(); i<=this.getEntidad(indice).getHasta(); i++) {
            if (detalle.getEntidad(i).getMontoSaldo()>0) {
                icantidadTotal ++;
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleIngreso.estado_atraso)) icantidadAtraso ++;
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleIngreso.estado_cobro))  icantidadCobro ++;
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleIngreso.estado_saldo))  icantidadSaldo ++;
            }
        }
        if (icantidadAtraso+icantidadCobro+icantidadSaldo == icantidadTotal && icantidadSaldo > 2) return true;
        else return false;
    }
    
    public void establecerAtrasoCabecera() {
        for (int i=0; i<this.getRowCount(); i++) { // relleno todos los registros que tienen atraso
            if (this.getEntidad(i).getMontoAtraso()>0) {
                this.getEntidad(i).setAtraso(true);
            }
        }
    }
    
    public void calcularSubtotal() {
        for (int k=0; k<this.getRowCount(); k++) { //if (indice==-1 || (indice!=-1 && indice==k)) { // si es 0 es para todos, si es un ítem solo cuando sea igual a k
            this.getEntidad(k).setMontoCapital(0.0);
            this.getEntidad(k).setMontoInteres(0.0);
            this.getEntidad(k).setMontoAtraso(0.0);
            this.getEntidad(k).setMontoSaldo(0.0);
            this.getEntidad(k).setMontoCobro(0.0);
            this.getEntidad(k).setMontoExoneracion(0.0);
            for (int i=this.getEntidad(k).getDesde(); i<=this.getEntidad(k).getHasta(); i++) {
                this.getEntidad(k).setMontoCapital(this.getEntidad(k).getMontoCapital() + detalle.getEntidad(i).getMontoCapital());
                this.getEntidad(k).setMontoInteres(this.getEntidad(k).getMontoInteres() + detalle.getEntidad(i).getMontoInteres());
                this.getEntidad(k).setMontoCobro(this.getEntidad(k).getMontoCobro() + detalle.getEntidad(i).getMontoCobro());
                this.getEntidad(k).setMontoExoneracion(this.getEntidad(k).getMontoExoneracion() + detalle.getEntidad(i).getMontoExoneracion());
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_atraso)) {
                    this.getEntidad(k).setMontoAtraso(this.getEntidad(k).getMontoAtraso() + detalle.getEntidad(i).getMontoSaldo());
                }
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_cobro) || detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_saldo)) {
                    this.getEntidad(k).setMontoSaldo(this.getEntidad(k).getMontoSaldo() + detalle.getEntidad(i).getMontoSaldo());
                }
            }
        }
    }
    
    public void crearInteresMoratorio(java.util.Date dfecha, int iidMovimientoCancela, int iidInteresMoratorio, String sinteresMoratorio, String sprioridad) {
        if (dfecha == null) return;
        double interesMoratorio = 0.0; // para calcular el total del interés moratorio
        for (int i=0; i<detalle.getRowCount(); i++) {
            int diasMora = utilitario.utiFecha.obtenerDiferenciaDia(detalle.getEntidad(i).getFechaVencimiento(), (java.util.Date)dfecha.clone());
            if (diasMora>0) {
                double tasaMora = detalle.getEntidad(i).getTasaInteresMoratorio()*12/365*diasMora;
                detalle.getEntidad(i).setInteresMoratorio(detalle.getEntidad(i).getMontoSaldo()*tasaMora/100);
            }
            interesMoratorio += detalle.getEntidad(i).getInteresMoratorio();
        }
        if (interesMoratorio>0) { // inserto nuevo registro para el interés moratorio
            detalle.insertar(this.insertarCuenta(dfecha, iidMovimientoCancela, 0, iidInteresMoratorio, sinteresMoratorio, sprioridad, interesMoratorio));
        }
    }
    
    public void crearInteresPunitorio(java.util.Date dfecha, int iidMovimientoCancela, int icantidadDiaGracia, int iidInteresPunitorio, String sinteresPunitorio, String sprioridad) {
        if (dfecha == null) return;
        double interesPunitorio = 0.0; // para calcular el total del interés punitorio
        for (int i=0; i<detalle.getRowCount(); i++) {
            int diasMora = utilitario.utiFecha.obtenerDiferenciaDia(detalle.getEntidad(i).getFechaVencimiento(), (java.util.Date)dfecha.clone());
            if (diasMora>icantidadDiaGracia) {
                double tasaMora = detalle.getEntidad(i).getTasaInteresPunitorio()/365*diasMora;
                detalle.getEntidad(i).setInteresPunitorio(detalle.getEntidad(i).getMontoSaldo()*tasaMora/100);
            }
            interesPunitorio += detalle.getEntidad(i).getInteresPunitorio();
        }
        if (interesPunitorio>0) { // inserto nuevo registro para el interés punitorio
            detalle.insertar(this.insertarCuenta(dfecha, iidMovimientoCancela, 0, iidInteresPunitorio, sinteresPunitorio, sprioridad, interesPunitorio));
        }
    }

    private especifico.entDetalleIngreso insertarCuenta(java.util.Date dfecha, int iidMovimientoCancela, int inumeroOperacion, int iidCuenta, String scuenta, String sprioridad, double umonto) {
        especifico.entDetalleIngreso ent = new especifico.entDetalleIngreso();
        ent.setIdMovimiento(0);
        ent.setIdMovimientoCancela(iidMovimientoCancela);
        ent.setTabla(especifico.entDetalleOperacion.tabla_operacionfija);
        ent.setIdDetalleOperacion(0);
        ent.setNumeroOperacion(inumeroOperacion);
        ent.setFechaVencimiento((java.util.Date)dfecha.clone());
        ent.setFechaOperacion((java.util.Date) dfecha.clone());
        ent.setTasaInteresMoratorio(0.0);
        ent.setCuota(1);
        ent.setPlazo(1);
        ent.setAtraso(true);
        ent.setCancela(true);
        ent.setEstado(especifico.entDetalleCancelacion.estado_atraso);
        ent.setIdCuenta(iidCuenta);
        ent.setCuenta(scuenta);
        ent.setDescripcion("");
        ent.setPrioridad(sprioridad);
        ent.setMontoCapital(utilitario.utiNumero.redondear(umonto, 0));
        ent.setMontoInteres(0.0);
        ent.setMontoSaldo(utilitario.utiNumero.redondear(umonto, 0));
        return ent;
    }
    
    public void eliminarDetalle(int indiceCapital, int indiceInteres) {
        if (indiceCapital>=0) {
            for (int i=this.getEntidad(indiceCapital).getDesde(); i<=this.getEntidad(indiceCapital).getHasta(); i++) {
                detalle.getEntidad(i).setId(-1);
            }
        }
        if (indiceInteres>=0) {
            for (int i=this.getEntidad(indiceInteres).getDesde(); i<=this.getEntidad(indiceInteres).getHasta(); i++) {
                detalle.getEntidad(i).setId(-1);
            }
        }
        int i = 0;
        while (i<detalle.getRowCount()) {
            if (detalle.getEntidad(i).getId() == -1) {
                detalle.eliminar(i);
            } else {
                i++;
            }
        }
    }

    public void establecerOrden() {
        java.util.Date auxfecha;
        especifico.modDetalleIngreso modAux = new especifico.modDetalleIngreso();
        String auxprioridad;
        int auxorden;
        for(int i=0; i<detalle.getRowCount(); i++) {
            modAux.insertar(new especifico.entDetalleIngreso());
            modAux.getEntidad(i).setFechaVencimiento((java.util.Date)detalle.getEntidad(i).getFechaVencimiento().clone());
            modAux.getEntidad(i).setPrioridad(detalle.getEntidad(i).getPrioridad());
            modAux.getEntidad(i).setOrden(i);
        }
        for(int i=0; i<modAux.getRowCount(); i++) {
            for(int k=i+1; k<modAux.getRowCount(); k++) {
                if (utilitario.utiFecha.getAMD(modAux.getEntidad(i).getFechaVencimiento()) > utilitario.utiFecha.getAMD(modAux.getEntidad(k).getFechaVencimiento())) {
                    auxfecha = (java.util.Date)modAux.getEntidad(i).getFechaVencimiento().clone();
                    auxprioridad = modAux.getEntidad(i).getPrioridad();
                    auxorden = modAux.getEntidad(i).getOrden();
                    modAux.getEntidad(i).setFechaVencimiento((java.util.Date)modAux.getEntidad(k).getFechaVencimiento().clone());
                    modAux.getEntidad(i).setPrioridad(modAux.getEntidad(k).getPrioridad());
                    modAux.getEntidad(i).setOrden(modAux.getEntidad(k).getOrden());
                    modAux.getEntidad(k).setFechaVencimiento(auxfecha);
                    modAux.getEntidad(k).setPrioridad(auxprioridad);
                    modAux.getEntidad(k).setOrden(auxorden);
                }
                if (utilitario.utiFecha.getAMD(modAux.getEntidad(i).getFechaVencimiento()) == utilitario.utiFecha.getAMD(modAux.getEntidad(k).getFechaVencimiento())) {
                    if (utilitario.utiCadena.obtenerPrioridad(modAux.getEntidad(i).getPrioridad()) > utilitario.utiCadena.obtenerPrioridad(modAux.getEntidad(k).getPrioridad())) {
                        auxfecha = (java.util.Date)modAux.getEntidad(i).getFechaVencimiento().clone();
                        auxprioridad = modAux.getEntidad(i).getPrioridad();
                        auxorden = modAux.getEntidad(i).getOrden();
                        modAux.getEntidad(i).setFechaVencimiento((java.util.Date)modAux.getEntidad(k).getFechaVencimiento().clone());
                        modAux.getEntidad(i).setPrioridad(modAux.getEntidad(k).getPrioridad());
                        modAux.getEntidad(i).setOrden(modAux.getEntidad(k).getOrden());
                        modAux.getEntidad(k).setFechaVencimiento(auxfecha);
                        modAux.getEntidad(k).setPrioridad(auxprioridad);
                        modAux.getEntidad(k).setOrden(auxorden);
                    }
                }
            }
        }
        for(int i=0; i<modAux.getRowCount(); i++) {
            detalle.getEntidad(modAux.getEntidad(i).getOrden()).setOrden(i);
        }
    }

    public void generarResumenCuota() {
        for(int k=0; k<this.getRowCount(); k++) {
            java.util.ArrayList<generico.entLista> lst = new java.util.ArrayList<generico.entLista>();
            for(int i=this.getEntidad(k).getDesde(); i<=this.getEntidad(k).getHasta(); i++) {
                if (detalle.getEntidad(i).getMontoSaldo()>0) {
                    lst.add(new generico.entLista(detalle.getEntidad(i).getCuota(), "", "", (short)0));
                }
            }
            this.getEntidad(k).setReferencia(utilitario.utiCadena.resumirCuota(lst)+"/"+this.getEntidad(k).getPlazo());
        }
    }

    public especifico.entDetalleIngreso getTotal() {
        especifico.entDetalleIngreso ent = new especifico.entDetalleIngreso();
        for (int k=0; k<this.getRowCount(); k++) {
            for (int i=this.getEntidad(k).getDesde(); i<=this.getEntidad(k).getHasta(); i++) {
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_atraso)) {
                    ent.setMontoAtraso(ent.getMontoAtraso()+detalle.getEntidad(i).getMontoSaldo());
                }
                if (this.getEntidad(k).getCancela()) {
                    if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_saldo) || detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_cobro)) {
                        ent.setMontoSaldo(ent.getMontoSaldo()+detalle.getEntidad(i).getMontoSaldo());
                        ent.setMontoExoneracion(ent.getMontoExoneracion()+detalle.getEntidad(i).getMontoExoneracion());
                    }
                } else {
                    if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_cobro)) {
                        ent.setMontoSaldo(ent.getMontoSaldo()+detalle.getEntidad(i).getMontoSaldo());
                    }
                }
            }
        }
        return ent;
    }
    
    public void marcarCancelacionDetalle() {
        for (int k=0; k<this.getRowCount(); k++) {
            for (int i=this.getEntidad(k).getDesde(); i<=this.getEntidad(k).getHasta(); i++) {
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_cobro) || detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_saldo)) {
                    detalle.getEntidad(i).setCancela(this.getEntidad(k).getCancela());
                }
                if (detalle.getEntidad(i).getEstado().equals(especifico.entDetalleCancelacion.estado_atraso)) { // solo a los registros con atraso
                    detalle.getEntidad(i).setCancela(true);
                    detalle.getEntidad(i).setAtraso(this.getEntidad(k).getAtraso());
                }
            }
        }
    }
    
    public void distribuirCobro(double ucobro) {
        for(int i=0; i<detalle.getRowCount(); i++) {
            detalle.getEntidad(i).setMontoCobro(0);
        }
        int i = 0, indice = 0;
        while (i < detalle.getRowCount() && ucobro > 0) { // carga nuevamente el cobro de acuerdo al efectivo
            indice = buscar(i);
            if (detalle.getEntidad(indice).getEstado().equals(especifico.entDetalleIngreso.estado_atraso) || detalle.getEntidad(indice).getEstado().equals(especifico.entDetalleIngreso.estado_cobro) || detalle.getEntidad(indice).getEstado().equals(especifico.entDetalleIngreso.estado_saldo)) {
                if (ucobro > detalle.getEntidad(indice).getMontoSaldo()) {
                    ucobro = ucobro - detalle.getEntidad(indice).getMontoSaldo();
                    detalle.getEntidad(indice).setMontoCobro(detalle.getEntidad(indice).getMontoSaldo());
                } else {
                    detalle.getEntidad(indice).setMontoCobro(ucobro);
                    ucobro = 0.0;
                }
            }
            i++;
        }
    }
    
    public void copiar(int indice) {
        detalleVer.removerTodo();
        for (int i=this.getEntidad(indice).getDesde(); i<=this.getEntidad(indice).getHasta(); i++) {
            detalleVer.insertar(detalle.getEntidad(i).copiar(new especifico.entDetalleIngreso()));
        }
    }
    
    //===========================
    // procedimientos privados
    //===========================
    
    private int buscar(int iorden) {
        int i = 0;
        while (i < detalle.getRowCount()) {
            if (detalle.getEntidad(i).getOrden()==iorden) return i;
            i++;
        }
        return -1;
    }

}
