/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modListaDetalle implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entListaDetalle getEntidad() {
        return (especifico.entListaDetalle)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entListaDetalle nuevo) {
        nuevo.copiar((especifico.entListaDetalle)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "CUOTA";
            case 1: return "FECHA VENCIM.";
            case 2: return "MONTO CAPITAL";
            case 3: return "MONTO INTERES";
            case 4: return "COBRO CAPITAL";
            case 5: return "COBRO INTERES";
            case 6: return "SALDO CAPITAL";
            case 7: return "SALDO INTERES";
            case 8: return "ESTADO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return java.util.Date.class;
            case 2: return Double.class;
            case 3: return Double.class;
            case 4: return Double.class;
            case 5: return Double.class;
            case 6: return Double.class;
            case 7: return Double.class;
            case 8: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entListaDetalle ent;
        ent = (especifico.entListaDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getCuota();
            case 1: return ent.getFechaVencimiento();
            case 2: return ent.getMontoCapital();
            case 3: return ent.getMontoInteres();
            case 4: return ent.getCobroCapital();
            case 5: return ent.getCobroInteres();
            case 6: return ent.getSaldoCapital();
            case 7: return ent.getSaldoInteres();
            case 8: return ent.getEstado();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entListaDetalle ent;
        ent = (especifico.entListaDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setCuota((Integer)aValue); break;
            case 1: ent.setFechaVencimiento((java.util.Date)aValue); break;
            case 2: ent.setMontoCapital((Double)aValue); break;
            case 3: ent.setMontoInteres((Double)aValue); break;
            case 4: ent.setCobroCapital((Double)aValue); break;
            case 5: ent.setCobroInteres((Double)aValue); break;
            case 6: ent.setSaldoCapital((Double)aValue); break;
            case 7: ent.setSaldoInteres((Double)aValue); break;
            case 8: ent.setEstado((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entListaDetalle getEntidad(int rowIndex) {
        return (especifico.entListaDetalle)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entListaDetalle nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entListaDetalle nuevo, int rowIndex) {
        nuevo.copiar((especifico.entListaDetalle)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entListaDetalle().cargar(rs));
            }
        } catch (Exception e) {
        }
    }

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(15);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(7).setPreferredWidth(40);
        tbl.getColumnModel().getColumn(8).setPreferredWidth(5);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public especifico.entListaDetalle getTotales() {
        especifico.entListaDetalle ent = new especifico.entListaDetalle();
        for (int i=0; i<this.getRowCount(); i++) {
            ent.setMontoCapital(ent.getMontoCapital()+this.getEntidad(i).getMontoCapital());
            ent.setMontoInteres(ent.getMontoInteres()+this.getEntidad(i).getMontoInteres());
            ent.setCobroCapital(ent.getCobroCapital()+this.getEntidad(i).getCobroCapital());
            ent.setCobroInteres(ent.getCobroInteres()+this.getEntidad(i).getCobroInteres());
            ent.setSaldoCapital(ent.getSaldoCapital()+this.getEntidad(i).getSaldoCapital());
            ent.setSaldoInteres(ent.getSaldoInteres()+this.getEntidad(i).getSaldoInteres());
            ent.setInteresMoratorio(ent.getInteresMoratorio()+this.getEntidad(i).getInteresMoratorio());
            ent.setInteresPunitorio(ent.getInteresPunitorio()+this.getEntidad(i).getInteresPunitorio());
        }
        return ent;
    }
    
    public void setEstado(java.util.Date dfecha) {
        if (dfecha==null) return;
        dfecha = utilitario.utiFecha.getUltimoDia(dfecha);
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getSaldoCapital()+this.getEntidad(i).getSaldoInteres()>0) {
                if (utilitario.utiFecha.getAM(this.getEntidad(i).getFechaVencimiento())<utilitario.utiFecha.getAM(dfecha)) {
                    this.getEntidad(i).setEstado("A");
                }
                if (utilitario.utiFecha.getAM(this.getEntidad(i).getFechaVencimiento())==utilitario.utiFecha.getAM(dfecha)) {
                    this.getEntidad(i).setEstado("C");
                }
                if (utilitario.utiFecha.getAM(this.getEntidad(i).getFechaVencimiento())>utilitario.utiFecha.getAM(dfecha)) {
                    this.getEntidad(i).setEstado("S");
                }
            }
        }
    }

    public void setInteresMoratorio(double uinteres, java.util.Date dfecha, int idiaGracia) {
        int diferencia = 0;
        double tasaDiaria = 0.0;
        for (int i=0; i<this.getRowCount(); i++) {
            diferencia = utilitario.utiFecha.obtenerDiferenciaDia(this.getEntidad(i).getFechaVencimiento(), (java.util.Date)dfecha.clone());
            if (diferencia>idiaGracia) {
                tasaDiaria = uinteres*12/365*(diferencia-idiaGracia);
                this.getEntidad(i).setInteresMoratorio(utilitario.utiNumero.redondear(this.getEntidad(i).getSaldoCapital()*tasaDiaria/100, 0));
            }
        }
    }
    
    public void setInteresPunitorio(double uinteres, java.util.Date dfecha, int idiaGracia) {
        int diferencia = 0;
        double tasaDiaria = 0.0;
        for (int i=0; i<this.getRowCount(); i++) {
            diferencia = utilitario.utiFecha.obtenerDiferenciaDia(this.getEntidad(i).getFechaVencimiento(), (java.util.Date)dfecha.clone());
            if (diferencia>idiaGracia) {
                tasaDiaria = uinteres*12/365*diferencia;
                this.getEntidad(i).setInteresPunitorio(utilitario.utiNumero.redondear(this.getEntidad(i).getSaldoCapital()*tasaDiaria/100, 0));
            }
        }
    }
    
}
