/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class modbDepositoCuentaDetalle implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje = "";
    
    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entbDepositoCuentaDetalle getEntidad() {
        return (especifico.entbDepositoCuentaDetalle)(data.get(tbl.getSelectedRow()));
    }

    public void modificar(especifico.entbDepositoCuentaDetalle nuevo) {
        nuevo.copiar((especifico.entbDepositoCuentaDetalle)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "NÚMERO DE CHEQUE";
            case 1: return "BANCO";
            case 2: return "OTRO BANCO";
            case 3: return "MISMO BANCO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return String.class;
            case 2: return Double.class;
            case 3: return Double.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entbDepositoCuentaDetalle ent;
        ent = (especifico.entbDepositoCuentaDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getNumeroCheque();
            case 1: return ent.getBanco();
            case 2: return ent.getMontoOtro();
            case 3: return ent.getMontoPropio();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entbDepositoCuentaDetalle ent;
        ent = (especifico.entbDepositoCuentaDetalle)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setNumeroCheque((Integer)aValue); break;
            case 1: ent.setBanco((String)aValue); break;
            case 2: ent.setMontoOtro((Double)aValue); break;
            case 3: ent.setMontoPropio((Double)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entbDepositoCuentaDetalle getEntidad(int rowIndex) {
        return (especifico.entbDepositoCuentaDetalle)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar(especifico.entbDepositoCuentaDetalle nuevo) {
        try {
            data.add(nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entbDepositoCuentaDetalle nuevo, int rowIndex) {
        nuevo.copiar((especifico.entbDepositoCuentaDetalle)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar(int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }
    
    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable(java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entbDepositoCuentaDetalle().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(75);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(75);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(75);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(75);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }
    
    public double getTotalOtro() {
        double total = 0;
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getEstadoRegistro()!=generico.entConstante.estadoregistro_eliminado){
                total += this.getEntidad(i).getMontoOtro();
            }
        }
        return total;
    }

    public double getTotalPropio() {
        double total = 0;
        for (int i=0; i<this.getRowCount(); i++) {
            if (this.getEntidad(i).getEstadoRegistro()!=generico.entConstante.estadoregistro_eliminado){
                total += this.getEntidad(i).getMontoPropio();
            }
        }
        return total;
    }

}
