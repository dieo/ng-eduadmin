/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modNotaCredito implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();
    private String smensaje;

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entNotaCredito getEntidad() {
        return (especifico.entNotaCredito)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entNotaCredito nuevo) {
        nuevo.copiar((especifico.entNotaCredito)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "NRO.NOTA CRÉD.";
            case 1: return "FECHA NOTA CRÉD.";
            case 2: return "RUC";
            case 3: return "RAZON SOCIAL";
            case 4: return "TOTAL NOTA CRÉD.";
            case 5: return "ESTADO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return java.util.Date.class;
            case 2: return String.class;
            case 3: return String.class;
            case 4: return Double.class;
            case 5: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entNotaCredito ent;
        ent = (especifico.entNotaCredito)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return utilitario.utiCadena.getNumeroDocumento(ent.getLocal(), ent.getPuntoExpedicion(), ent.getNumeroNota());
            case 1: return ent.getFechaNota();
            case 2: return ent.getRuc();
            case 3: return ent.getRazonSocial();
            case 4: return ent.getTotal();
            case 5: return ent.getEstadoNota();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entNotaCredito ent;
        ent = (especifico.entNotaCredito)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setNumeroNota((Integer)aValue); break;
            case 1: ent.setFechaNota((java.util.Date)aValue); break;
            case 2: ent.setRuc((String)aValue); break;
            case 3: ent.setRazonSocial((String)aValue); break;
            case 4: ent.setTotal((Integer)aValue); break;
            case 5: ent.setEstadoNota((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entNotaCredito getEntidad(int rowIndex) {
        return (especifico.entNotaCredito)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++){
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entNotaCredito nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entNotaCredito nuevo, int rowIndex) {
        nuevo.copiar((especifico.entNotaCredito)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entNotaCredito().cargar(rs));
            }
        } catch (Exception e) {
        }
    }
    
    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(10);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(25);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(100);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(25);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(20);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        //tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        //tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        //tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        //tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        //tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); 
    }

    public String getMensaje() {
        return this.smensaje;
    }

}
