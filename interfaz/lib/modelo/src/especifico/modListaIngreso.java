/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class modListaIngreso implements javax.swing.table.TableModel {

    private javax.swing.JTable tbl;
    private java.util.LinkedList data = new java.util.LinkedList();
    private java.util.LinkedList list = new java.util.LinkedList();

    public void setTable(javax.swing.JTable tbl) {
        this.tbl = tbl;
        this.tbl.setModel(this);
        this.establecerFormato();
    }
    
    public javax.swing.JTable getTable() {
        return tbl;
    }

    public especifico.entListaIngreso getEntidad() {
        return (especifico.entListaIngreso)(data.get(tbl.getSelectedRow()));
    }
    
    public void modificar(especifico.entListaIngreso nuevo) {
        nuevo.copiar((especifico.entListaIngreso)(data.get(tbl.getSelectedRow())));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar () {
        try {
            data.remove(tbl.getSelectedRow());
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, tbl.getSelectedRow(), tbl.getSelectedRow(), javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: return "INGRESO";
            case 1: return "FECHA OP.";
            case 2: return "FECHA";
            case 3: return "CUOTA";
            case 4: return "CUENTA";
            case 5: return "AMORTIZACION";
            case 6: return "EXONERACION";
            case 7: return "ESTADO";
            default: return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return Integer.class;
            case 1: return java.util.Date.class;
            case 2: return java.util.Date.class;
            case 3: return Integer.class;
            case 4: return String.class;
            case 5: return Double.class;
            case 6: return Double.class;
            case 7: return String.class;
            default: return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        especifico.entListaIngreso ent;
        ent = (especifico.entListaIngreso)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: return ent.getNumeroIngreso();
            case 1: return ent.getFechaOperacionIngreso();
            case 2: return ent.getFechaOperacion();
            case 3: return ent.getCuota();
            case 4: return ent.getCuenta();
            case 5: return ent.getMontoAmortizacion();
            case 6: return ent.getMontoExoneracion();
            case 7: return ent.getEstado();
            default: return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        especifico.entListaIngreso ent;
        ent = (especifico.entListaIngreso)(data.get(rowIndex));
        switch (columnIndex) {
            case 0: ent.setNumeroIngreso((Integer)aValue); break;
            case 1: ent.setFechaOperacionIngreso((java.util.Date)aValue); break;
            case 2: ent.setFechaOperacion((java.util.Date)aValue); break;
            case 3: ent.setCuota((Integer)aValue); break;
            case 4: ent.setCuenta((String)aValue); break;
            case 5: ent.setMontoAmortizacion((Double)aValue); break;
            case 6: ent.setMontoExoneracion((Double)aValue); break;
            case 7: ent.setEstado((String)aValue); break;
            default: break;
        }
        javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, rowIndex, rowIndex, columnIndex);
        avisaSuscriptores(evento);
    }

    @Override
    public void addTableModelListener(javax.swing.event.TableModelListener l) {
        list.add(l);
    }

    @Override
    public void removeTableModelListener(javax.swing.event.TableModelListener l) {
        list.remove(l);
    }

    public especifico.entListaIngreso getEntidad(int rowIndex) {
        return (especifico.entListaIngreso)(data.get(rowIndex));
    }
    
    private void avisaSuscriptores (javax.swing.event.TableModelEvent evento) {
        for (int i=0; i<list.size(); i++) {
            ((javax.swing.event.TableModelListener)list.get(i)).tableChanged(evento);
        }
    }

    public void insertar (especifico.entListaIngreso nuevo) {
        try {
            data.add (nuevo);
            javax.swing.event.TableModelEvent evento;
            evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.INSERT);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void modificar(especifico.entListaIngreso nuevo, int rowIndex) {
        nuevo.copiar((especifico.entListaIngreso)(data.get(rowIndex)));
        javax.swing.event.TableModelEvent evento;
        evento = new javax.swing.event.TableModelEvent (this, this.getRowCount()-1, this.getRowCount()-1, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.UPDATE);
        avisaSuscriptores (evento);
    }

    public void eliminar (int fila) {
        try {
            data.remove(fila);
            javax.swing.event.TableModelEvent evento = new javax.swing.event.TableModelEvent (this, fila, fila, javax.swing.event.TableModelEvent.ALL_COLUMNS, javax.swing.event.TableModelEvent.DELETE);
            avisaSuscriptores (evento);
        } catch (Exception e) {
        }
    }

    public void removerTodo() {
        data.removeAll(data);
    }
    
    public void cargarTable (java.sql.ResultSet rs) {
        this.removerTodo();
        try {
            rs.beforeFirst();
            while (rs.next()) {
                this.insertar(new especifico.entListaIngreso().cargar(rs));
            }
        } catch (Exception e) {
        }
    }

    private void establecerFormato() {
        // establece el tamaño de las columnas (300)
        tbl.getColumnModel().getColumn(0).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(2).setPreferredWidth(30);
        tbl.getColumnModel().getColumn(3).setPreferredWidth(5);
        tbl.getColumnModel().getColumn(4).setPreferredWidth(150);
        tbl.getColumnModel().getColumn(5).setPreferredWidth(60);
        tbl.getColumnModel().getColumn(6).setPreferredWidth(60);
        tbl.getColumnModel().getColumn(7).setPreferredWidth(5);
        tbl.getTableHeader().setFont(new java.awt.Font("Tahoma", 1, 8));
        tbl.setFont(new java.awt.Font("Arial", 1, 10));
        /*tbl.setDefaultRenderer(String.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Integer.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Double.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(java.util.Date.class, new generico.modCeldaString());
        tbl.setDefaultRenderer(Boolean.class, new generico.modCeldaCheckBox()); */
    }

    public especifico.entListaIngreso getTotales(int iidInteresPrestamo) {
        especifico.entListaIngreso ent = new especifico.entListaIngreso();
        for (int i=0; i<this.getRowCount(); i++) {
            ent.setMontoAmortizacion(ent.getMontoAmortizacion()+this.getEntidad(i).getMontoAmortizacion());
            ent.setMontoExoneracion(ent.getMontoExoneracion()+this.getEntidad(i).getMontoExoneracion());
            if (this.getEntidad(i).getIdCuenta()==iidInteresPrestamo) {
                ent.setMontoInteres(ent.getMontoInteres()+this.getEntidad(i).getMontoAmortizacion());
            } else {
                ent.setMontoCapital(ent.getMontoCapital()+this.getEntidad(i).getMontoAmortizacion());
            }
        }
        return ent;
    }
    
}
