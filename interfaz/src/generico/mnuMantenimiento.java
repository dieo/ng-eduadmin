/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class mnuMantenimiento extends javax.swing.JPopupMenu {
    
    public javax.swing.JMenuItem mnuInsertar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuModificar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuEliminar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuGuardar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuCancelar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuRefrescar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuBuscar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuFiltrar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuOrdenar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuImprimir = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuAprobar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuGenerar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuAnular = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuRechazar = new javax.swing.JMenuItem();

    public mnuMantenimiento() {
        this.aplicarFormato();
        this.add(mnuInsertar);
        this.add(mnuModificar);
        this.add(mnuEliminar);
        this.add(new javax.swing.JPopupMenu.Separator());
        this.add(mnuGuardar);
        this.add(mnuCancelar);
        this.add(new javax.swing.JPopupMenu.Separator());
        this.add(mnuRefrescar);
        this.add(mnuBuscar);
        this.add(mnuFiltrar);
        this.add(mnuOrdenar);
        this.add(mnuImprimir);
        this.add(new javax.swing.JPopupMenu.Separator());
        this.add(mnuAprobar);
        this.add(mnuGenerar);
        this.add(mnuAnular);
        this.add(mnuRechazar);
    }

    public void setEnabled(boolean bmodo) {
        for (int i=0; i<this.getComponentCount(); i++) {
            ((javax.swing.JMenuItem)this.getComponent(i)).setEnabled(bmodo);
        }
    }

    private void aplicarFormato() {
        java.awt.Font fuente = new java.awt.Font("Arial", 0, 11);
        //for (int i=0; i<this.getComponentCount(); i++) {
        //    ((javax.swing.JMenuItem)this.getComponent(i)).setFont(fuente);
        //    ((javax.swing.JMenuItem)this.getComponent(i)).setText(((javax.swing.JMenuItem)this.getComponent(i)).getName().substring(3));
        //}
        mnuInsertar.setFont(fuente);
        mnuInsertar.setText("Insertar");
        mnuModificar.setFont(fuente);
        mnuModificar.setText("Modificar");
        mnuEliminar.setFont(fuente);
        mnuEliminar.setText("Eliminar");
        mnuGuardar.setFont(fuente);
        mnuGuardar.setText("Guardar");
        mnuCancelar.setFont(fuente);
        mnuCancelar.setText("Cancelar");
        mnuRefrescar.setFont(fuente);
        mnuRefrescar.setText("Refrescar");
        mnuBuscar.setFont(fuente);
        mnuBuscar.setText("Buscar");
        mnuFiltrar.setFont(fuente);
        mnuFiltrar.setText("Filtrar");
        mnuOrdenar.setFont(fuente);
        mnuOrdenar.setText("Ordenar");
        mnuImprimir.setFont(fuente);
        mnuImprimir.setText("Imprimir");
        mnuAprobar.setFont(fuente);
        mnuAprobar.setText("Aprobar");
        mnuGenerar.setFont(fuente);
        mnuGenerar.setText("Generar");
        mnuAnular.setFont(fuente);
        mnuAnular.setText("Anular");
        mnuRechazar.setFont(fuente);
        mnuRechazar.setText("Rechazar");
        mnuAprobar.setVisible(false);
        mnuGenerar.setVisible(false);
        mnuAnular.setVisible(false);
        mnuRechazar.setVisible(false);
    }
    
}
