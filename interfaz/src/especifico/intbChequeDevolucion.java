/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intbChequeDevolucion.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class intbChequeDevolucion extends javax.swing.JPanel {

    public generico.intMantenimiento mnt;
    public especifico.modbChequeEntrega mod = new especifico.modbChequeEntrega();
    private String svista="", sfiltro="", sorden="";

    /** Creates new form intbChequeDevolucion 
     @param mnt.
     @param tbl.
     */
    public intbChequeDevolucion(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblObservacion = new javax.swing.JLabel();
        txtfObservacion = new javax.swing.JFormattedTextField();
        lblFechaAnulado = new javax.swing.JLabel();
        try {
            txtFechaDevolucion = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblVerCheque = new javax.swing.JLabel();
        btnCheque = new javax.swing.JButton();
        lblEstado = new javax.swing.JLabel();
        txtfEstado = new javax.swing.JFormattedTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(610, 95));
        setPreferredSize(new java.awt.Dimension(610, 95));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblObservacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblObservacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObservacion.setText("Observación");
        add(lblObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 90, 25));

        txtfObservacion.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        add(txtfObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 500, 25));

        lblFechaAnulado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaAnulado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaAnulado.setText("Fecha Devolución");
        add(lblFechaAnulado, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 90, 25));
        add(txtFechaDevolucion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, -1, -1));

        lblVerCheque.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblVerCheque.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblVerCheque.setText("Ver Cheque");
        add(lblVerCheque, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 90, 25));

        btnCheque.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/visualizar.png"))); // NOI18N
        btnCheque.setToolTipText("Ver Ahorro");
        btnCheque.setFocusable(false);
        add(btnCheque, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 25, 25));

        lblEstado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstado.setText("Estado");
        add(lblEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 90, 25));

        txtfEstado.setBorder(null);
        txtfEstado.setFocusable(false);
        txtfEstado.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        txtfEstado.setOpaque(false);
        add(txtfEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, 270, 25));
    }// </editor-fold>//GEN-END:initComponents

    private void btnChequeActionPerformed(java.awt.event.ActionEvent evt) {
        this.configurarPanel();
    }                                        
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnCheque;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblFechaAnulado;
    private javax.swing.JLabel lblObservacion;
    private javax.swing.JLabel lblVerCheque;
    private panel.fecha txtFechaDevolucion;
    private javax.swing.JFormattedTextField txtfEstado;
    private javax.swing.JFormattedTextField txtfObservacion;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnCheque.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChequeActionPerformed(evt);
            }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfObservacion);
        uti.agregarEventoKeyReleased(this.txtfObservacion);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtFechaDevolucion.setEnabled(false);
        this.txtfObservacion.setEnabled(bmodo);
    }
    
    private void iniciarComponentes() {
        this.txtfObservacion.setDocument(new generico.modFormatoCadena(this.txtfObservacion, especifico.entbChequeEntrega.LONGITUD_OBSERVACION, true));
        this.txtFechaDevolucion.setToolTipText(especifico.entbChequeEntrega.TEXTO_FECHA_ANULADO);
        this.txtfObservacion.setToolTipText(especifico.entbChequeEntrega.TEXTO_OBSERVACION_DEVOLUCION);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entbChequeEntrega());
        }
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        // activo
        if (mod.getEntidad().getIdEstadoEntrega()== mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "ENVIADO") || mod.getEntidad().getIdEstadoEntrega()== mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "ENTREGADO")) {
            especifico.entbChequeEntrega ent = new especifico.entbChequeEntrega();
            ent.setIdEstadoEntrega(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "DEVUELTO"));
            ent.setEstadoEntrega(mnt.intPrincipal.pnlEstado.conexion.obtenerDescripcionSubtipo(ent.getIdEstadoEntrega()));
            ent.setFechaDevolucion(new java.util.Date());
            ent.setObservacionDevolucion(mod.getEntidad().getObservacionDevolucion());
            this.setInterfaz(ent);
        } else {
            return false;
        }
        return true;
    }

    public boolean establecer() {
        especifico.entbChequeEntrega ent = mod.getEntidad().copiar(new especifico.entbChequeEntrega()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValidoDevolver()) { // valida la anulación
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        ent.setIdEstadoEntrega(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "DEVUELTO"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }
    
    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entbChequeEntrega ent) {
        ent.setIdEstadoEntrega(ent.getIdEstadoEntrega());
        ent.setEstadoEntrega(ent.getEstadoEntrega());
        ent.setFechaDevolucion(this.txtFechaDevolucion.getFecha());
        ent.setObservacionDevolucion(this.txtfObservacion.getText());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entbChequeEntrega ent) {
        try                 {this.txtFechaDevolucion.setFecha(ent.getFechaDevolucion());}
        catch (Exception e) {this.txtFechaDevolucion.setFechaVacia();}
        try                 {this.txtfObservacion.setText(ent.getObservacionDevolucion());}
        catch (Exception e) {this.txtfObservacion.setText("");}
        try                 {this.txtfEstado.setText(ent.getEstadoEntrega());}
        catch (Exception e) {this.txtfEstado.setText("");}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

    
    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================

    private void configurarPanel() {
        especifico.intbChequeEntrega intCheque = new especifico.intbChequeEntrega(mnt, null);
        generico.intPanel panel = new generico.intPanel("Cheque :: Mantenimiento :: " + generico.mnuMantenimientoDetalle.visualizar, intCheque);
        intCheque.setEnabled(false);
        especifico.entbChequeEntrega ent = mod.getEntidad().copiar(new especifico.entbChequeEntrega()); // obtiene la entidad del registro seleccionado
        intCheque.mod.insertar(ent);
        intCheque.mod.getTable().setRowSelectionInterval(0,0);
        intCheque.cargarEditor();
        if (intCheque.mod.getTable().getSelectedRow()>=0) panel.abrir();
    }
    
}
