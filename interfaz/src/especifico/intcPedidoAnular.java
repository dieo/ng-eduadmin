/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intcPedidoAnular.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class intcPedidoAnular extends javax.swing.JPanel {

    public generico.intMantenimiento mnt;
    public especifico.modcPedido mod = new especifico.modcPedido();
    private String svista="", sfiltro="", sorden="", sfiltroFormulario = "";

    /** Creates new form intcPedidoAnular 
     @param mnt.
     @param tbl.
     */
    public intcPedidoAnular(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblObservacion = new javax.swing.JLabel();
        txtfObservacion = new javax.swing.JFormattedTextField();
        lblFechaAnulado = new javax.swing.JLabel();
        try {
            txtFechaAnulado = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblEstado = new javax.swing.JLabel();
        txtfEstado = new javax.swing.JFormattedTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(605, 90));
        setPreferredSize(new java.awt.Dimension(605, 90));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblObservacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblObservacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObservacion.setText("Observación");
        add(lblObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 90, 25));

        txtfObservacion.setForeground(new java.awt.Color(0, 102, 204));
        txtfObservacion.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        add(txtfObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 500, 25));

        lblFechaAnulado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaAnulado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaAnulado.setText("Fecha Anulado");
        add(lblFechaAnulado, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 90, 25));
        add(txtFechaAnulado, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, -1, -1));

        lblEstado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstado.setText("Estado");
        add(lblEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 90, 25));

        txtfEstado.setBorder(null);
        txtfEstado.setForeground(new java.awt.Color(0, 102, 204));
        txtfEstado.setFocusable(false);
        txtfEstado.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        txtfEstado.setOpaque(false);
        add(txtfEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, 270, 25));
    }// </editor-fold>//GEN-END:initComponents
                                          
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblFechaAnulado;
    private javax.swing.JLabel lblObservacion;
    private panel.fecha txtFechaAnulado;
    private javax.swing.JFormattedTextField txtfEstado;
    private javax.swing.JFormattedTextField txtfObservacion;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfObservacion);
        uti.agregarEventoKeyReleased(this.txtfObservacion);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtFechaAnulado.setEnabled(false);
        this.txtfObservacion.setEnabled(bmodo);
    }
    
    private void iniciarComponentes() {
        this.txtfObservacion.setDocument(new generico.modFormatoCadena(this.txtfObservacion, especifico.entcPedido.LONGITUD_OBSERVACION, true));
        this.txtFechaAnulado.setToolTipText(especifico.entcPedido.TEXTO_FECHA_ANULADO);
        this.txtfObservacion.setToolTipText(especifico.entcPedido.TEXTO_OBSERVACION_ANULADO);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entcPedido());
        }
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        // PENDIENTE O AUTORIZADO
        if (mod.getEntidad().getIdEstadoPedido()== mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%PEDIDO%", "PENDIENTE") || mod.getEntidad().getIdEstadoPedido()== mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%PEDIDO%", "AUTORIZADO") || mod.getEntidad().getIdEstadoPedido()== mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%PEDIDO%", "ENTREGADO")) {
            especifico.entcPedido ent = new especifico.entcPedido();
            ent.setIdEstadoPedido(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%PEDIDO%", "ANULADO"));
            ent.setEstadoPedido(mnt.intPrincipal.pnlEstado.conexion.obtenerDescripcionSubtipo(ent.getIdEstadoPedido()));
            ent.setFechaAnulado(new java.util.Date());
            ent.setObservacionAnulado(mod.getEntidad().getObservacionAnulado());
            this.setInterfaz(ent);
        } else {
            return false;
        }
        return true;
    }

    public boolean establecer() {
        especifico.entcPedido ent = mod.getEntidad().copiar(new especifico.entcPedido()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValidoAnular()) { // valida la anulación
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        ent.setIdEstadoPedido(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%PEDIDO%", "ANULADO"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        this.procesarPedido();
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro+sfiltroFormulario, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }
    
    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entcPedido ent) {
        ent.setIdEstadoPedido(ent.getIdEstadoPedido());
        ent.setEstadoPedido(ent.getEstadoPedido());
        ent.setFechaAnulado(this.txtFechaAnulado.getFecha());
        ent.setObservacionAnulado(this.txtfObservacion.getText());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entcPedido ent) {
        try                 {this.txtFechaAnulado.setFecha(ent.getFechaAnulado());}
        catch (Exception e) {this.txtFechaAnulado.setFechaVacia();}
        try                 {this.txtfObservacion.setText(ent.getObservacionAnulado());}
        catch (Exception e) {this.txtfObservacion.setText("");}
        try                 {this.txtfEstado.setText(ent.getEstadoPedido());}
        catch (Exception e) {this.txtfEstado.setText("");}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }


    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================

    private void procesarPedido() {
        if (!mod.getEntidad().getEstadoPedido().contains("ENTREGADO")) {
            return;
        } else    {
            especifico.intcArticuloDetalle articulo = new especifico.intcArticuloDetalle(mnt, null);
            especifico.intcPedidoDetalle intcPedidoDetalle = new especifico.intcPedidoDetalle(mnt, null);
            intcPedidoDetalle.setVistaFiltroOrden("compras."+generico.vista.getNombre(generico.vista.vwpedidodetalle1), " AND idpedido="+mod.getEntidad().getId(), " ORDER BY id");
            intcPedidoDetalle.obtenerDato();
            String sentencia;
            double unidades=0.0;
            for (int i=0;i<intcPedidoDetalle.mod.getRowCount();i++){
                if(intcPedidoDetalle.mod.getEntidad(i).getEntregado()){
                    articulo.setVistaFiltroOrden("compras."+generico.vista.getNombre(generico.vista.vwarticulocompradetalle1), " AND idarticulo="+intcPedidoDetalle.mod.getEntidad(i).getIdArticulo()+" AND idfilial="+mod.getEntidad().getIdFilial(), "");
                    articulo.obtenerDato();
                    unidades=articulo.mod.getEntidad(0).getStock()+intcPedidoDetalle.mod.getEntidad(i).getCantidad();
                    sentencia="UPDATE compras.articulodetalle SET stock="+unidades;
                    sentencia+=" WHERE idarticulo="+intcPedidoDetalle.mod.getEntidad(i).getIdArticulo()+" AND idfilial="+mod.getEntidad().getIdFilial()+";";
                    System.out.println(sentencia);
                    mnt.intPrincipal.pnlEstado.conexion.ejecutarActualizacion(sentencia);
                }
            }
            mod.getEntidad().setIdEstadoPedido(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%PEDIDO%", "ANULADO"));
            mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
            mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia());
            mnt.btnRefrescar.doClick();
        }        
    }
}
