/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
 * intRubro.java
 *
 * Created on 04-abr-2012, 18:23:29
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intListaDetalleBuscar extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modListaDetalle mod = new especifico.modListaDetalle(); // para crear la cabecera
    private int iidOperacion = 0;
    private String stabla = "";
    private String svista = "", sfiltro = "", sorden = "";

    /**
     * Creates new form intListaDetalleCancelacion
     *
     * @param mnt.
     */
    public intListaDetalleBuscar(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        initComponents();
        this.mod.setTable(this.tblDetalle);
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scpDetalle = new javax.swing.JScrollPane();
        tblDetalle = new javax.swing.JTable();
        lblTotalesDetallado = new javax.swing.JLabel();
        txtfSaldoCapital = new javax.swing.JFormattedTextField();
        txtfMontoInteres = new javax.swing.JFormattedTextField();
        txtfMontoCapital = new javax.swing.JFormattedTextField();
        btnRellenar = new javax.swing.JButton();
        txtfSaldoInteres = new javax.swing.JFormattedTextField();
        txtfCobroCapital = new javax.swing.JFormattedTextField();
        txtfCobroInteres = new javax.swing.JFormattedTextField();
        txtfMonto = new javax.swing.JFormattedTextField();
        txtfCobro = new javax.swing.JFormattedTextField();
        txtfSaldo = new javax.swing.JFormattedTextField();
        lblTotalesGeneral = new javax.swing.JLabel();
        btnImprimirDetalleOperacion = new javax.swing.JButton();
        btnImprimirDetalleIngreso = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(650, 376));
        setPreferredSize(new java.awt.Dimension(650, 376));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        scpDetalle.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scpDetalle.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tblDetalle.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        scpDetalle.setViewportView(tblDetalle);

        add(scpDetalle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 640, 370));

        lblTotalesDetallado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTotalesDetallado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalesDetallado.setText("Totales detallado");
        add(lblTotalesDetallado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 100, 25));

        txtfSaldoCapital.setForeground(new java.awt.Color(0, 102, 204));
        txtfSaldoCapital.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfSaldoCapital.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfSaldoCapital.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfSaldoCapital.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        add(txtfSaldoCapital, new org.netbeans.lib.awtextra.AbsoluteConstraints(427, 370, 75, 25));

        txtfMontoInteres.setForeground(new java.awt.Color(0, 102, 204));
        txtfMontoInteres.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMontoInteres.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfMontoInteres.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfMontoInteres.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        add(txtfMontoInteres, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 370, 75, 25));

        txtfMontoCapital.setForeground(new java.awt.Color(0, 102, 204));
        txtfMontoCapital.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMontoCapital.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfMontoCapital.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfMontoCapital.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        add(txtfMontoCapital, new org.netbeans.lib.awtextra.AbsoluteConstraints(125, 370, 75, 25));

        btnRellenar.setEnabled(false);
        btnRellenar.setOpaque(false);
        add(btnRellenar, new org.netbeans.lib.awtextra.AbsoluteConstraints(577, 370, 40, 50));

        txtfSaldoInteres.setForeground(new java.awt.Color(0, 102, 204));
        txtfSaldoInteres.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfSaldoInteres.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfSaldoInteres.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfSaldoInteres.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        add(txtfSaldoInteres, new org.netbeans.lib.awtextra.AbsoluteConstraints(502, 370, 75, 25));

        txtfCobroCapital.setForeground(new java.awt.Color(0, 102, 204));
        txtfCobroCapital.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfCobroCapital.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfCobroCapital.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfCobroCapital.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        add(txtfCobroCapital, new org.netbeans.lib.awtextra.AbsoluteConstraints(276, 370, 75, 25));

        txtfCobroInteres.setForeground(new java.awt.Color(0, 102, 204));
        txtfCobroInteres.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfCobroInteres.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfCobroInteres.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfCobroInteres.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        add(txtfCobroInteres, new org.netbeans.lib.awtextra.AbsoluteConstraints(351, 370, 75, 25));

        txtfMonto.setForeground(new java.awt.Color(0, 102, 204));
        txtfMonto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfMonto.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfMonto.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfMonto.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        add(txtfMonto, new org.netbeans.lib.awtextra.AbsoluteConstraints(125, 395, 150, 25));

        txtfCobro.setForeground(new java.awt.Color(0, 102, 204));
        txtfCobro.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfCobro.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfCobro.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfCobro.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        add(txtfCobro, new org.netbeans.lib.awtextra.AbsoluteConstraints(276, 395, 150, 25));

        txtfSaldo.setForeground(new java.awt.Color(0, 102, 204));
        txtfSaldo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfSaldo.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfSaldo.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfSaldo.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        add(txtfSaldo, new org.netbeans.lib.awtextra.AbsoluteConstraints(427, 395, 150, 25));

        lblTotalesGeneral.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTotalesGeneral.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalesGeneral.setText("Totales general");
        add(lblTotalesGeneral, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 395, 100, 25));

        btnImprimirDetalleOperacion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirDetalleOperacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirDetalleOperacion.setToolTipText("Imprimir Detalle");
        btnImprimirDetalleOperacion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirDetalleOperacion.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirDetalleOperacion.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirDetalleOperacion.setOpaque(false);
        btnImprimirDetalleOperacion.setPreferredSize(new java.awt.Dimension(65, 35));
        add(btnImprimirDetalleOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(615, 370, 25, 25));

        btnImprimirDetalleIngreso.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirDetalleIngreso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirDetalleIngreso.setToolTipText("Imprimir Detalle de Pago");
        btnImprimirDetalleIngreso.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirDetalleIngreso.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirDetalleIngreso.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirDetalleIngreso.setOpaque(false);
        btnImprimirDetalleIngreso.setPreferredSize(new java.awt.Dimension(65, 35));
        add(btnImprimirDetalleIngreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(615, 395, 25, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnImprimirDetalleIngreso;
    private javax.swing.JButton btnImprimirDetalleOperacion;
    private javax.swing.JButton btnRellenar;
    private javax.swing.JLabel lblTotalesDetallado;
    private javax.swing.JLabel lblTotalesGeneral;
    private javax.swing.JScrollPane scpDetalle;
    public javax.swing.JTable tblDetalle;
    private javax.swing.JFormattedTextField txtfCobro;
    private javax.swing.JFormattedTextField txtfCobroCapital;
    private javax.swing.JFormattedTextField txtfCobroInteres;
    private javax.swing.JFormattedTextField txtfMonto;
    private javax.swing.JFormattedTextField txtfMontoCapital;
    private javax.swing.JFormattedTextField txtfMontoInteres;
    private javax.swing.JFormattedTextField txtfSaldo;
    private javax.swing.JFormattedTextField txtfSaldoCapital;
    private javax.swing.JFormattedTextField txtfSaldoInteres;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnImprimirDetalleOperacion.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirDetalleOperacion();
            }
        });
        btnImprimirDetalleIngreso.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirDetalleIngreso();
            }
        });

//        tblDetalle.addMouseListener(new java.awt.event.MouseAdapter() {
//            @Override
//            public void mouseClicked(java.awt.event.MouseEvent evt) {
//                if (evt.getClickCount() == 2) { // doble click
//                    if (mod.getTable().getSelectedRow() < 0) {
//                        return; // no tiene datos
//                    }
//                    especifico.intListaIngreso lista = new especifico.intListaIngreso(mnt);
//                    lista.configurarPanel(mod.getEntidad().getId());
//                }
//            }
//        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoKeyPressed(this.tblDetalle);
    }

    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfMontoCapital.setVisible(false);
        this.txtfMontoInteres.setVisible(false);
        this.txtfCobroCapital.setVisible(false);
        this.txtfCobroInteres.setVisible(false);
        this.txtfSaldoCapital.setVisible(false);
        this.txtfSaldoInteres.setVisible(false);
        this.txtfMonto.setVisible(false);
        this.txtfCobro.setVisible(false);
        this.txtfSaldo.setVisible(false);
        
        this.lblTotalesDetallado.setVisible(false);
        this.lblTotalesGeneral.setVisible(false);
        this.btnRellenar.setVisible(false);
        this.btnImprimirDetalleOperacion.setVisible(false);
        this.btnImprimirDetalleIngreso.setVisible(false);
    }

    private void iniciarComponentes() {
        this.txtfMontoCapital.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfMontoInteres.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfCobroCapital.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfCobroInteres.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfSaldoCapital.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfSaldoInteres.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfMonto.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfCobro.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfSaldo.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfMontoCapital.setToolTipText("Total Monto Capital");
        this.txtfMontoInteres.setToolTipText("Total Monto Interés");
        this.txtfCobroCapital.setToolTipText("Total Pago Capital");
        this.txtfCobroInteres.setToolTipText("Total Pago Interés");
        this.txtfSaldoCapital.setToolTipText("Total Saldo Capital");
        this.txtfSaldoInteres.setToolTipText("Total Saldo Interés");
        this.txtfMonto.setToolTipText("Total Monto");
        this.txtfCobro.setToolTipText("Total Pago");
        this.txtfSaldo.setToolTipText("Total Saldo");
    }

    public void cargarEditor() {
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        return true;
    }

    public boolean establecer() {
        return true;
    }

    public boolean eliminar() {
        return true;
    }

    public boolean cancelar() {
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        System.out.println(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden));
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entListaDetalle ent) {
    }

    public void setInterfaz(especifico.entListaDetalle ent) {
        try {
            this.txtfMontoCapital.setValue(ent.getMontoCapital());
        } catch (Exception e) {
            this.txtfMontoCapital.setValue(0);
        }
        try {
            this.txtfMontoInteres.setValue(ent.getMontoInteres());
        } catch (Exception e) {
            this.txtfMontoInteres.setValue(0);
        }
        try {
            this.txtfCobroCapital.setValue(ent.getCobroCapital());
        } catch (Exception e) {
            this.txtfCobroCapital.setValue(0);
        }
        try {
            this.txtfCobroInteres.setValue(ent.getCobroInteres());
        } catch (Exception e) {
            this.txtfCobroInteres.setValue(0);
        }
        try {
            this.txtfSaldoCapital.setValue(ent.getSaldoCapital());
        } catch (Exception e) {
            this.txtfSaldoCapital.setValue(0);
        }
        try {
            this.txtfSaldoInteres.setValue(ent.getSaldoInteres());
        } catch (Exception e) {
            this.txtfSaldoInteres.setValue(0);
        }
        try {
            this.txtfMonto.setValue(ent.getMontoCapital() + ent.getMontoInteres());
        } catch (Exception e) {
            this.txtfMonto.setValue(0);
        }
        try {
            this.txtfCobro.setValue(ent.getCobroCapital() + ent.getCobroInteres());
        } catch (Exception e) {
            this.txtfCobro.setValue(0);
        }
        try {
            this.txtfSaldo.setValue(ent.getSaldoCapital() + ent.getSaldoInteres());
        } catch (Exception e) {
            this.txtfSaldo.setValue(0);
        }
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty()) {
            this.svista = svista;
        }
        if (!sfiltro.isEmpty()) {
            this.sfiltro = sfiltro;
        }
        if (!sorden.isEmpty()) {
            this.sorden = sorden;
        }
    }

    public void imprimir() {
    }

    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================
    public void configurarPanel(int iidOperacion, String stabla, java.util.Date dfecha) {
        this.iidOperacion = iidOperacion;
        this.stabla = stabla;
        this.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwdetalleoperacion), " AND idmovimiento=" + iidOperacion + " AND tabla='" + stabla + "'", " ORDER BY fechavencimiento");
        this.obtenerDato();
        this.mod.setEstado(dfecha);
        this.setInterfaz(this.mod.getTotales());
        try {
            this.mod.getTable().setRowSelectionInterval(0, 0);
        } catch (Exception e) {
        }
        this.setEnabled(false);
        generico.intPanel panel = new generico.intPanel("Detalle de Cuota", this);
        panel.abrir();
    }

    public void obtenerDetalle(int iidOperacion, String stabla) {
        this.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwdetalleoperacion), " AND idmovimiento=" + iidOperacion + " AND tabla='" + stabla + "'", " ORDER BY fechavencimiento");
        this.obtenerDato();
    }

    private void imprimirDetalleOperacion() {
        String sentencia = "";
        especifico.entDetalleOperacion ent = new especifico.entDetalleOperacion();
        if (this.stabla.equals(ent.tabla_movimiento)) {
            sentencia = "SELECT s.cedula, s.apellido|| ', '||s.nombre AS apellidonombre, mo.id, mo.numerooperacion, mo.numerosolicitud, mo.fechaaprobado AS fechaoperacion, mo.plazoaprobado AS plazo, mo.tasainteres AS tasa, "
                    + "(CASE WHEN mo.idcuenta=98 THEN mo.montosolicitud ELSE mo.montoaprobado END)::NUMERIC(10,0) AS capital, mo.montointeres::numeric(10,0) AS interes, cu.descripcion AS operacion, en.descripcion AS fuente, tc.descripcion AS tipooperacion, "
                    + "det.numerocuota, det.fechavencimiento, det.montocapital::NUMERIC(10,0), det.montointeres::NUMERIC(10,0), "
                    + "((det.montocapital+det.montointeres)-(det.saldocapital+det.saldointeres))::NUMERIC(10,0) AS cobro, det.saldocapital::NUMERIC(10,0), det.saldointeres::NUMERIC(10,0), "
                    + "(CASE WHEN mo.id = ca.idoperacion THEN ca.rubro ELSE ru.descripcion END) AS rubro, "
                    + "(CASE WHEN mo.id = ca.idoperacion THEN ca.giraduria ELSE gir.descripcion END) AS giraduria, "
                    + "(CASE WHEN det.saldocapital+det.saldointeres>0 THEN (CASE WHEN det.fechavencimiento<NOW() THEN 'A' ELSE 'S' END) ELSE '' END) AS estadodetalle "
                    + "FROM movimiento AS mo "
                    + "LEFT JOIN socio AS s ON mo.idsocio=s.id "
                    + "LEFT JOIN cuenta AS cu ON mo.idcuenta=cu.id "
                    + "LEFT JOIN tipocredito AS tc ON mo.idtipocredito=tc.id "
                    + "LEFT JOIN entidad AS en ON mo.identidad=en.id "
                    + "LEFT JOIN detalleoperacion AS det ON mo.id=det.idmovimiento AND det.tabla='" + this.stabla + "' "
                    + "LEFT JOIN lugarlaboral AS lu ON s.id=lu.idsocio "
                    + "LEFT JOIN rubro AS ru ON lu.idrubro=ru.id "
                    + "LEFT JOIN vwcambiorubro1 AS ca ON mo.id=ca.idoperacion AND ca.tabla='" + this.stabla + "' "
                    + "LEFT JOIN giraduria AS gir ON lu.idgiraduria=gir.id "
                    + "WHERE det.idmovimiento=" + iidOperacion + " "
                    + "ORDER BY det.fechavencimiento ";
        }
        if (this.stabla.equals(ent.tabla_operacionfija)) {
            sentencia = "SELECT s.cedula, s.apellido|| ', '||s.nombre AS apellidonombre, op.id, op.numerooperacion, 0 AS numerosolicitud, op.fechaoperacion, op.plazo, 0 AS tasa, "
                    + "(CASE WHEN op.plazo = 0 THEN op.importe ELSE op.importe * op.plazo::numeric(10,0) END) AS capital, 0 AS interes, "
                    + "cu.descripcion AS operacion, '---' AS fuente, '---' AS tipooperacion, det.numerocuota, det.fechavencimiento, det.montocapital::NUMERIC(10,0), 0 AS montointeres, "
                    + "(det.montocapital-det.saldocapital)::NUMERIC(10,0) AS cobro, det.saldocapital::NUMERIC(10,0), det.saldointeres::NUMERIC(10,0), "
                    + "(CASE WHEN op.id = ca.idoperacion THEN ca.rubro ELSE ru.descripcion END) AS rubro, "
                    + "(CASE WHEN op.id = ca.idoperacion THEN ca.giraduria ELSE gir.descripcion END) AS giraduria, "
                    + "(CASE WHEN det.saldocapital+det.saldointeres>0 THEN (CASE WHEN det.fechavencimiento<NOW() THEN 'A' ELSE 'S' END) ELSE '' END) AS estadodetalle "
                    + "FROM operacionfija AS op "
                    + "LEFT JOIN socio AS s ON op.idsocio=s.id "
                    + "LEFT JOIN cuenta AS cu ON op.idcuenta=cu.id "
                    + "LEFT JOIN detalleoperacion AS det ON op.id=det.idmovimiento AND det.tabla='" + this.stabla + "' "
                    + "LEFT JOIN lugarlaboral AS lu ON s.id=lu.idsocio "
                    + "LEFT JOIN rubro AS ru ON lu.idrubro=ru.id "
                    + "LEFT JOIN vwcambiorubro1 AS ca ON op.id=ca.idoperacion AND ca.tabla='" + this.stabla + "' "
                    + "LEFT JOIN giraduria AS gir ON lu.idgiraduria=gir.id "
                    + "WHERE det.idmovimiento=" + iidOperacion + " "
                    + "ORDER BY det.fechavencimiento ";
        }
        if (this.stabla.equals(ent.tabla_fondojuridicosepelio)) {
            sentencia = "SELECT s.cedula, s.apellido|| ', '||s.nombre AS apellidonombre, fj.id, fj.numerooperacion, 0 AS numerosolicitud, fj.fechaoperacion, 0 AS plazo, 0 AS tasa, (fj.importetitular + fj.importeadherente * fj.cantidadadherente)::numeric(10,0) AS capital, 0 AS interes, "
                    + "cu.descripcion AS operacion, '---' AS fuente, '---' AS tipooperacion, det.numerocuota, det.fechavencimiento, det.montocapital::NUMERIC(10,0), 0 AS montointeres,(det.montocapital-det.saldocapital)::NUMERIC(10,0) AS cobro, "
                    + "det.saldocapital::NUMERIC(10,0), det.saldointeres::NUMERIC(10,0), "
                    + "(CASE WHEN fj.id = ca.idoperacion THEN ca.rubro ELSE ru.descripcion END) AS rubro, "
                    + "(CASE WHEN fj.id = ca.idoperacion THEN ca.giraduria ELSE gir.descripcion END) AS giraduria, "
                    + "(CASE WHEN det.saldocapital+det.saldointeres>0 THEN (CASE WHEN det.fechavencimiento<NOW() THEN 'A' ELSE 'S' END) ELSE '' END) AS estadodetalle "
                    + "FROM fondojuridicosepelio AS fj "
                    + "LEFT JOIN socio AS s ON fj.idsocio=s.id "
                    + "LEFT JOIN cuenta AS cu ON fj.idcuenta=cu.id "
                    + "LEFT JOIN detalleoperacion AS det ON fj.id=det.idmovimiento AND det.tabla='" + this.stabla + "' "
                    + "LEFT JOIN lugarlaboral AS lu ON s.id=lu.idsocio "
                    + "LEFT JOIN rubro AS ru ON lu.idrubro=ru.id "
                    + "LEFT JOIN vwcambiorubro1 AS ca ON fj.id=ca.idoperacion AND ca.tabla='" + this.stabla + "' "
                    + "LEFT JOIN giraduria AS gir ON lu.idgiraduria = gir.id "
                    + "WHERE det.idmovimiento=" + iidOperacion + " "
                    + "ORDER BY det.fechavencimiento ";
        }
        if (this.stabla.equals(ent.tabla_ahorroprogramado)) {
            sentencia = "SELECT s.cedula, s.apellido|| ', '||s.nombre AS apellidonombre, ap.id, ap.numerooperacion, 0 AS numerosolicitud, ap.fechaoperacion, ap.plazo, 0 AS tasa, "
                    + "(det.montocapital * ap.plazo)::numeric(10,0) AS capital, 0 AS interes, "
                    + "cu.descripcion AS operacion, '---' AS fuente, pl.descripcion AS tipooperacion, det.numerocuota, det.fechavencimiento, det.montocapital::NUMERIC(10,0), 0 AS montointeres, "
                    + "(det.montocapital-det.saldocapital)::NUMERIC(10,0) AS cobro, det.saldocapital::NUMERIC(10,0), det.saldointeres::NUMERIC(10,0), "
                    + "(CASE WHEN ap.id = ca.idoperacion THEN ca.rubro ELSE ru.descripcion END) AS rubro, "
                    + "(CASE WHEN ap.id = ca.idoperacion THEN ca.giraduria ELSE gir.descripcion END) AS giraduria, "
                    + "(CASE WHEN det.saldocapital+det.saldointeres>0 THEN (CASE WHEN det.fechavencimiento<NOW() THEN 'A' ELSE 'S' END) ELSE '' END) AS estadodetalle "
                    + "FROM ahorroprogramado AS ap "
                    + "LEFT JOIN planahorroprogramado AS pl ON ap.idplan=pl.id "
                    + "LEFT JOIN socio AS s ON ap.idsocio=s.id "
                    + "LEFT JOIN cuenta AS cu ON ap.idcuenta=cu.id "
                    + "LEFT JOIN detalleoperacion AS det ON ap.id=det.idmovimiento AND det.tabla='" + this.stabla + "' "
                    + "LEFT JOIN lugarlaboral AS lu ON s.id=lu.idsocio "
                    + "LEFT JOIN rubro AS ru ON lu.idrubro=ru.id "
                    + "LEFT JOIN vwcambiorubro1 AS ca ON ap.id=ca.idoperacion AND ca.tabla='" + this.stabla + "' "
                    + "LEFT JOIN giraduria AS gir ON lu.idgiraduria=gir.id "
                    + "WHERE det.idmovimiento=" + iidOperacion + " "
                    + "ORDER BY det.fechavencimiento ";
        }
        System.out.println(sentencia);
        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(sentencia);
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repDetalleOperacion, mnt.intPrincipal.pnlEstado.conexion.rs, null, null, null, "Detalle de Operación", "", mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }

    private void imprimirDetalleIngreso() {
        String sentencia = "";
        String scondicion = "";
        String sentidad = "";
        if (this.stabla.equals("mo")) {
            scondicion = "left join movimiento as mo on mo.id = di.idmovimiento left join entidad as en on en.id = mo.identidad ";
        }
        if (this.stabla.equals("mo")) {
            sentidad = " en.descripcion as entidad,";
        }
        especifico.entDetalleOperacion ent = new especifico.entDetalleOperacion();
        sentencia = "select obtenercalificacion(totaldiasatraso, cantidad) as calificacion, * from ("
                + "       select (select sum(case when i.fechaoperacion > di.fechavencimiento then i.fechaoperacion - di.fechavencimiento else 0 end) as atraso "
                + "	from ingreso i "
                + "	left join detalleingreso di on di.idingreso = i.id and di.tabla = '" + this.stabla + "' "
                + "	where di.idmovimiento = " + iidOperacion + ") as totaldiasatraso, "
                + "	(select count(*) from detalleingreso where idmovimiento=" + iidOperacion + ") as cantidad, i.ruc as cedula, i.razonsocial, st.descripcion as viacobro, tv.descripcion as valor, i.numerooperacion as ingreso, i.fechaoperacion as fecha, "
                + "case when i.fechaoperacion > di.fechavencimiento then i.fechaoperacion - di.fechavencimiento else 0 end as diasatraso, "
                + "cu.id as idcuenta, cu.descripcion as cuenta," + sentidad + " di.cuota, di.plazo, di.fechavencimiento, di.numerooperacion, di.montocobro as montocobro, di.montoexoneracion, di.estado, "
                + "cast(case when di.idcuenta = 35 then di.montointeres else di.montocapital end as numeric(12)) montocuota "
                + "from ingreso as i "
                + "left join detalleingreso as di on di.idingreso = i.id AND di.tabla ='" + this.stabla + "' "
                + "left join planillaingreso as pi on pi.id = i.idplanilla " + scondicion
                + "left join vwsubtipo as st on st.id = pi.idviacobro "
                + "left join cuenta as cu on di.idcuenta=cu.id "
                + "left join detallevalor as dv on dv.idingreso = i.id "
                + "left join tipovalor as tv on tv.id = dv.idtipovalor "
                + "where 0=0 "
                + "and i.idestado= 146 "
                + "and di.idmovimiento =" + iidOperacion + " "
                + "order by di.idcuenta, di.cuota"
                + ") as detalle";
        System.out.println(sentencia);
        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(sentencia);
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repDetalleIngreso, mnt.intPrincipal.pnlEstado.conexion.rs, null, null, null, "Detalle de Pagos por Operación", "Operación", mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }

}
