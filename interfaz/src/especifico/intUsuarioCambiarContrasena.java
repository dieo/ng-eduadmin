/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intUsuario.java
 *
 * Created on 14-feb-2013, 16:32:23
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intUsuarioCambiarContrasena extends javax.swing.JPanel {

    private generico.intPrincipal pcp;
    public especifico.modUsuario mod = new especifico.modUsuario();
    private String svista="", sfiltro="", sorden="";

    /** Creates new form intUsuario 
     @param pcp.
     @param tbl.
     */
    public intUsuarioCambiarContrasena(generico.intPrincipal pcp, javax.swing.JTable tbl) {
        this.pcp = pcp;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groNivelUsuario = new javax.swing.ButtonGroup();
        txtfUsuario = new javax.swing.JFormattedTextField();
        txtfContrasena = new javax.swing.JPasswordField();
        lblUsuario = new javax.swing.JLabel();
        lblContrasena = new javax.swing.JLabel();
        lblConfirmacion = new javax.swing.JLabel();
        txtfConfirmacion = new javax.swing.JPasswordField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(300, 90));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(300, 90));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfUsuario.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 200, 25));

        txtfContrasena.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        txtfContrasena.setText("jPasswordField1");
        txtfContrasena.setPreferredSize(new java.awt.Dimension(6, 20));
        add(txtfContrasena, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 35, 200, 25));

        lblUsuario.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUsuario.setText("Usuario");
        add(lblUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 25));

        lblContrasena.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblContrasena.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblContrasena.setText("Contraseña");
        add(lblContrasena, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 70, 25));

        lblConfirmacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblConfirmacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblConfirmacion.setText("Confirmación");
        add(lblConfirmacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 70, 25));

        txtfConfirmacion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        txtfConfirmacion.setText("jPasswordField1");
        txtfConfirmacion.setPreferredSize(new java.awt.Dimension(6, 20));
        add(txtfConfirmacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 200, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup groNivelUsuario;
    private javax.swing.JLabel lblConfirmacion;
    private javax.swing.JLabel lblContrasena;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPasswordField txtfConfirmacion;
    private javax.swing.JPasswordField txtfContrasena;
    private javax.swing.JFormattedTextField txtfUsuario;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfContrasena);
        uti.agregarEventoFocusGained(this.txtfConfirmacion);
        uti.agregarEventoKeyReleased(this.txtfContrasena);
        uti.agregarEventoKeyReleased(this.txtfConfirmacion);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfUsuario.setEnabled(false);
        this.txtfContrasena.setEnabled(bmodo);
        this.txtfConfirmacion.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.txtfContrasena.setDocument(new generico.modFormatoCadena(this.txtfContrasena, especifico.entUsuario.LONGITUD_CONTRASENA, false));
        this.txtfConfirmacion.setDocument(new generico.modFormatoCadena(this.txtfConfirmacion, especifico.entUsuario.LONGITUD_CONTRASENA, false));
        this.txtfUsuario.setToolTipText(especifico.entUsuario.TEXTO_USUARIO);
        this.txtfContrasena.setToolTipText(especifico.entUsuario.TEXTO_CONTRASENA);
        this.txtfConfirmacion.setToolTipText(especifico.entUsuario.TEXTO_CONFIRMACION);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) { 
            this.setInterfaz(new especifico.entUsuario());
        }
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        especifico.entUsuario ent = mod.getEntidad().copiar(new especifico.entUsuario()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValidoCambiarContrasena()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (pcp.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        if (!pcp.pnlEstado.usuario.getPerfilUsuario().contains("ADMINISTRADOR")) return false;
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!pcp.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, pcp.pnlEstado.conexion.getMensaje());
            return false;
        }
        pcp.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getCambiarContrasenaUsuarioBD(mod.getEntidad().getUsuario(), mod.getEntidad().getContrasena()));
        return true;
    }

    public boolean obtenerDato() {
        if (pcp.pnlEstado.conexion.ejecutarSentencia(pcp.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(pcp.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, pcp.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entUsuario ent) {
        ent.setUsuario(this.txtfUsuario.getText());
        ent.setContrasena(this.txtfContrasena.getText());
        ent.setConfirmacion(this.txtfConfirmacion.getText());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entUsuario ent) {
        try                 {this.txtfUsuario.setText(ent.getUsuario());}
        catch (Exception e) {this.txtfUsuario.setText("");}
        try                 {this.txtfContrasena.setText(ent.getContrasena());}
        catch (Exception e) {this.txtfContrasena.setText("");}
        try                 {this.txtfConfirmacion.setText(ent.getConfirmacion());}
        catch (Exception e) {this.txtfConfirmacion.setText("");}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }
    
    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================

    public void configurarPanel() {
        especifico.intUsuarioCambiarContrasena intUsuario = this;
        intUsuario.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwusuario1), " AND id="+pcp.pnlEstado.usuario.getId(), "");
        intUsuario.obtenerDato();
        intUsuario.setEnabled(true);
        intUsuario.mod.getTable().setRowSelectionInterval(intUsuario.mod.getTable().getRowCount()-1, intUsuario.mod.getTable().getRowCount()-1);
        if (intUsuario.mod.getTable().getSelectedRow()>=0) intUsuario.cargarEditor();
        generico.intPanel panel = new generico.intPanel("Cambio de Contaseña :: Mantenimiento :: " + generico.mnuMantenimientoDetalle.modificar, intUsuario);
        while (true && intUsuario.mod.getTable().getSelectedRow()>=0) {
            panel.abrir();
            if (panel.establecido()) {
                if (intUsuario.establecer()) { if (intUsuario.guardar()) { javax.swing.JOptionPane.showMessageDialog(null, "La contraseña ha sido cambiada"); break; } }
            } else {
                intUsuario.cancelar(); break;
            }
        }
    }

}
