 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intBarrio.java
 *
 * Created on 03-abr-2012, 10:25:12
 */
package especifico;
/**
 *
 * @author Ing. Edison Martinez
 */
public class intProcesoDescarga extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    private utilitario.comComboBox cmbRubro;
    private utilitario.comComboBox cmbPlanilla;
    private especifico.modArchivoGiraduria modArchivo = new especifico.modArchivoGiraduria();
    private especifico.modArchivoGiraduria modArchivoIndividual = new especifico.modArchivoGiraduria();
    private especifico.modDescargaDato modDescarga = new especifico.modDescargaDato();
    private int iidEstadoOrdenCreditoGenerado;
    private int iidEstadoPrestamoGenerado;
    private int iidEstadoAhorroActivo;
    private int iidEstadoOperacionActivo;
    private int iidEstadoFondoActivo;
    public int iidPrestamoMutual;
    public int iidPrestamoFinanciera;
    public int iidOrdenCredito;
    public int iidInteresPrestamo;
    private int iidMutual;
    public int iidInteresMoratorio;
    public int iidInteresPunitorio;
    public int iidIvaInteresCompensatorio;
    private double utasaInteresPunitorio;
    //private utilitario.negVencimiento vencimiento = new utilitario.negVencimiento();
    
    /** Creates new form intBarrio 
     @param mnt.
     */
    public intProcesoDescarga(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlCierre = new javax.swing.JPanel();
        lblFechaProceso = new javax.swing.JLabel();
        try {
            txtFechaProceso = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblProcesar = new javax.swing.JLabel();
        btnProcesar = new javax.swing.JButton();
        lblDescargarArchivo = new javax.swing.JLabel();
        cmboRubro = new javax.swing.JComboBox();
        lblRubro = new javax.swing.JLabel();
        lblPlanilla = new javax.swing.JLabel();
        cmboPlanilla = new javax.swing.JComboBox();
        btnDescargarSocio = new javax.swing.JButton();
        btnArchivoGiraduria = new javax.swing.JButton();
        lblCargarArchivo = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(475, 140));
        setPreferredSize(new java.awt.Dimension(475, 140));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlCierre.setBackground(new java.awt.Color(255, 255, 255));
        pnlCierre.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlCierre.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblFechaProceso.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaProceso.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaProceso.setText("Fecha Proceso");
        pnlCierre.add(lblFechaProceso, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 120, 25));
        pnlCierre.add(txtFechaProceso, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, -1, 25));

        lblProcesar.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblProcesar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblProcesar.setText("Procesar");
        pnlCierre.add(lblProcesar, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 85, 120, 25));

        btnProcesar.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnProcesar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/generar20.png"))); // NOI18N
        btnProcesar.setToolTipText("");
        btnProcesar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnProcesar.setMaximumSize(new java.awt.Dimension(65, 35));
        btnProcesar.setMinimumSize(new java.awt.Dimension(65, 35));
        btnProcesar.setOpaque(false);
        btnProcesar.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlCierre.add(btnProcesar, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 85, 25, 25));

        lblDescargarArchivo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblDescargarArchivo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDescargarArchivo.setText("Descargar Archivo");
        pnlCierre.add(lblDescargarArchivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 85, 120, 25));

        cmboRubro.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        pnlCierre.add(cmboRubro, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 35, 305, 25));

        lblRubro.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblRubro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblRubro.setText("Rubro");
        pnlCierre.add(lblRubro, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 120, 25));

        lblPlanilla.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblPlanilla.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPlanilla.setText("Planilla Ingreso");
        pnlCierre.add(lblPlanilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 120, 25));

        cmboPlanilla.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        pnlCierre.add(cmboPlanilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 60, 305, 25));

        btnDescargarSocio.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnDescargarSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/visualizar-2.png"))); // NOI18N
        btnDescargarSocio.setToolTipText("");
        btnDescargarSocio.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDescargarSocio.setMaximumSize(new java.awt.Dimension(65, 35));
        btnDescargarSocio.setMinimumSize(new java.awt.Dimension(65, 35));
        btnDescargarSocio.setOpaque(false);
        btnDescargarSocio.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlCierre.add(btnDescargarSocio, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 85, 25, 25));

        btnArchivoGiraduria.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnArchivoGiraduria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/visualizar-2.png"))); // NOI18N
        btnArchivoGiraduria.setToolTipText("Insertar Registros en Archivo GIraduria");
        btnArchivoGiraduria.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnArchivoGiraduria.setMaximumSize(new java.awt.Dimension(65, 35));
        btnArchivoGiraduria.setMinimumSize(new java.awt.Dimension(65, 35));
        btnArchivoGiraduria.setOpaque(false);
        btnArchivoGiraduria.setPreferredSize(new java.awt.Dimension(65, 35));
        btnArchivoGiraduria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArchivoGiraduriaActionPerformed(evt);
            }
        });
        pnlCierre.add(btnArchivoGiraduria, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 10, 25, 25));

        lblCargarArchivo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblCargarArchivo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCargarArchivo.setText("Archivo Giraduría");
        pnlCierre.add(lblCargarArchivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 10, 100, 25));

        add(pnlCierre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 455, 120));
    }// </editor-fold>//GEN-END:initComponents

    private void btnArchivoGiraduriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArchivoGiraduriaActionPerformed
        try {
            jxl.Workbook archivoExcel = jxl.Workbook.getWorkbook(new java.io.File("c:\\mutual\\archivogiraduria.xls"));
            jxl.Sheet hoja = archivoExcel.getSheet(0);
            especifico.entArchivoGiraduria ent = new especifico.entArchivoGiraduria();
            int cant = 0;
            for (int i=1; i<hoja.getRows(); i++) {
                ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("archivogiraduria"));
                ent.setIdSocio(obtenerIdSocio(hoja.getCell(0,i).getContents()));
                ent.setCedula(hoja.getCell(0,i).getContents());
                ent.setNombre(hoja.getCell(1,i).getContents());
                ent.setApellido(hoja.getCell(2,i).getContents());
                ent.setIdGiraduria(utilitario.utiNumero.convertirToInt(hoja.getCell(4,i).getContents()));
                ent.setIdRubro(utilitario.utiNumero.convertirToInt(hoja.getCell(5,i).getContents()));
                ent.setMonto(utilitario.utiNumero.convertirToDoubleMac(hoja.getCell(4,i).getContents()));
                ent.setMontoAplicado(0.0);
                ent.setFechaAplicada(null);
                ent.setPeriodo(this.txtFechaProceso.getFecha());
                ent.setProcesado(false);
                ent.setEstadoRegistro((short)2); // para insertar
                System.out.println(ent.getSentencia());
                mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(ent.getSentencia());
                cant ++;
            }
            javax.swing.JOptionPane.showMessageDialog(null, "se insertó "+cant+" filas");
        } catch (Exception e) {
             javax.swing.JOptionPane.showMessageDialog(null, "No se pudo migrar "+ e);
        } 
    }//GEN-LAST:event_btnArchivoGiraduriaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnArchivoGiraduria;
    private javax.swing.JButton btnDescargarSocio;
    private javax.swing.JButton btnProcesar;
    private javax.swing.JComboBox cmboPlanilla;
    private javax.swing.JComboBox cmboRubro;
    private javax.swing.JLabel lblCargarArchivo;
    private javax.swing.JLabel lblDescargarArchivo;
    private javax.swing.JLabel lblFechaProceso;
    private javax.swing.JLabel lblPlanilla;
    private javax.swing.JLabel lblProcesar;
    private javax.swing.JLabel lblRubro;
    private javax.swing.JPanel pnlCierre;
    private panel.fecha txtFechaProceso;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnDescargarSocio.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                descargarSocio();
            }
        });
        btnProcesar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesar();
            }
        });
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtFechaProceso.setEnabled(true);
    }

    private void iniciarComponentes() {
        mnt.visualizarBarra(false);
        this.txtFechaProceso.setFecha(new java.util.Date());
        this.cmbRubro = new utilitario.comComboBox(this.cmboRubro, this.lblRubro, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwrubro1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbPlanilla = new utilitario.comComboBox(this.cmboPlanilla, this.lblPlanilla, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwplanillaingreso1)+" WHERE activo='TRUE' AND idfuncionario="+mnt.intPrincipal.pnlEstado.usuario.getIdFuncionario()+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        if (this.cmbPlanilla.mod.getSize()>0) this.cmbPlanilla.cmb.setSelectedIndex(0);
        else this.cmbPlanilla.cmb.setSelectedIndex(-1);
        this.setEnabled(false);

        iidEstadoOrdenCreditoGenerado = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%ORDEN%CREDITO%", "GENERADO");
        iidEstadoPrestamoGenerado = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%PRESTAMO%", "GENERADO");
        iidEstadoAhorroActivo = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%AHORRO%PROGRAMADO%", "ACTIVO");
        iidEstadoOperacionActivo = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%OPERACION%FIJA%", "ACTIVO");
        iidEstadoFondoActivo = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%FONDO%JURIDICO%SEPELIO%", "ACTIVO");
        iidPrestamoMutual = mnt.intPrincipal.pnlEstado.conexion.getId(generico.codigo.getNombre(generico.codigo.PrestamoMutual_PMU));
        iidPrestamoFinanciera = mnt.intPrincipal.pnlEstado.conexion.getId(generico.codigo.getNombre(generico.codigo.PrestamoFinanciera_PFI));
        iidOrdenCredito = mnt.intPrincipal.pnlEstado.conexion.getId(generico.codigo.getNombre(generico.codigo.OrdenCredito_OCR));
        iidInteresPrestamo = mnt.intPrincipal.pnlEstado.conexion.getId(generico.codigo.getNombre(generico.codigo.InteresPrestamo_IPR));
        iidInteresMoratorio = mnt.intPrincipal.pnlEstado.conexion.getId(generico.codigo.getNombre(generico.codigo.InteresMoratorio_IMO));
        iidInteresPunitorio = mnt.intPrincipal.pnlEstado.conexion.getId(generico.codigo.getNombre(generico.codigo.InteresPunitorio_IPU));
        iidIvaInteresCompensatorio = mnt.intPrincipal.pnlEstado.conexion.getId(generico.codigo.getNombre(generico.codigo.ImpuestoValorAgregadoInteresCompensatorio_IIC));
        iidMutual = mnt.intPrincipal.pnlEstado.conexion.obtenerId(generico.vista.getNombre(generico.vista.vwentidad1), " 0=0 AND descripcion LIKE '%MUTUAL%NAC%FUNC%' AND tipoentidad LIKE '%FINANCIERA%'");
        utasaInteresPunitorio = mnt.intPrincipal.pnlEstado.defecto.getTasaInteresPunitorio();

        this.txtFechaProceso.setToolTipText("Fecha de proceso");
        this.cmbRubro.setToolTipText("Rubro a procesar");
        this.cmbPlanilla.setToolTipText("Planilla de Ingreso");
        this.btnDescargarSocio.setToolTipText("Descargar archivo");
        this.btnProcesar.setToolTipText("Procesar Ingreso");
    }

    public void cargarEditor() {
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        return true;
    }

    public boolean eliminar() {
        return true;
    }

    public boolean cancelar() {
        return true;
    }

    private int obtenerIdSocio(String cedula){
        especifico.intSocio IntSocio = new especifico.intSocio(mnt);
        IntSocio.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwsocio1), " AND cedula="+utilitario.utiCadena.getTextoGuardado(cedula), "ORDER BY id");
        IntSocio.obtenerDato();
        if (IntSocio.mod.getRowCount()<=0) return 0;
        else return IntSocio.mod.getEntidad(0).getId();
    }
    
    public boolean obtenerDato() {
        return true;
    }

    public void refrescarComponente() {
    }
    
    private void getInterfaz(utilitario.entFechaCierre ent) {
    }
    
    private void setInterfaz(utilitario.entFechaCierre ent) {
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
    }

    public void imprimir() {
    }

    private void descargarSocio() {
        System.out.println(new especifico.entArchivoGiraduria().getConsultaSocio(this.cmbRubro.getIdSeleccionado(), utilitario.utiFecha.getUltimoDia(this.txtFechaProceso.getFecha())));
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(new especifico.entArchivoGiraduria().getConsultaSocio(this.cmbRubro.getIdSeleccionado(), utilitario.utiFecha.getUltimoDia(this.txtFechaProceso.getFecha())))) { // obtiene datos de la base de datos
            modArchivo.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        }
        double umonto = 0;
        for (int i=0; i<modArchivo.getRowCount(); i++) {
            umonto = umonto + modArchivo.getEntidad(i).getMonto();
        }
        javax.swing.JOptionPane.showMessageDialog(null, "Cantidad a procesar: "+modArchivo.getRowCount()+" - Monto a procesar: "+utilitario.utiNumero.getMascara(umonto, 0));
    }
    
    private especifico.entArchivoGiraduria getDescuento(String scedula) {
        System.out.println(new especifico.entArchivoGiraduria().getConsultaSocio(this.cmbRubro.getIdSeleccionado(), utilitario.utiFecha.getUltimoDia(this.txtFechaProceso.getFecha()), scedula));
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(new especifico.entArchivoGiraduria().getConsultaSocio(this.cmbRubro.getIdSeleccionado(), utilitario.utiFecha.getUltimoDia(this.txtFechaProceso.getFecha()), scedula))) { // obtiene datos de la base de datos
            modArchivo.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        }
        if (modArchivo.getRowCount()==0) return new especifico.entArchivoGiraduria();
        else return modArchivo.getEntidad(0);
    }
    
    private void procesar() {
        System.out.println(new especifico.entDescargaDato().getConsultaConEstadoxxx(utilitario.utiFecha.getUltimoDia(this.txtFechaProceso.getFecha())));
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(new especifico.entDescargaDato().getConsultaConEstadoxxx(utilitario.utiFecha.getUltimoDia(this.txtFechaProceso.getFecha())))) { // obtiene datos de la base de datos
            modDescarga.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        }
        
        //if (true) return;
        
        especifico.entIngreso ingreso = new especifico.entIngreso();
        especifico.entDetalleIngresonew detalle = new especifico.entDetalleIngresonew();
        //especifico.intCambioRubro cambio = new especifico.intCambioRubro(mnt);
        especifico.entArchivoGiraduria descuento = new especifico.entArchivoGiraduria();
        int icantidadExterno = 0;
        int k = 0;
        while (k < modDescarga.getRowCount()) {
            if (!(modDescarga.getEntidad(k).getCedula().trim().equals(descuento.getCedula().trim()))) {
                if (ingreso.getId() > 0) { // SI EL INGRESO CONTIENE VALORES
                    System.out.println(ingreso.getSentencia());
                    mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(ingreso.getSentencia());
                    System.out.println(descuento.getSentencia2());
                    mnt.intPrincipal.pnlEstado.conexion.ejecutarActualizacion(descuento.getSentencia2());
                }
                descuento = this.getDescuento(modDescarga.getEntidad(k).getCedula().trim());
                ingreso = new especifico.entIngreso();
                if (!descuento.getCedula().isEmpty()) { // SI ENCUENTRA DESCUENTO
                    ingreso.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("ingreso"));
                    ingreso.setNumeroIngreso(mnt.intPrincipal.pnlEstado.conexion.obtenerId("numeroingreso"));
                    ingreso.setIdPlanilla(this.cmbPlanilla.getIdSeleccionado());
                    ingreso.setFechaOperacion(new java.util.Date());
                    ingreso.setEsSocio(true);
                    // modificacion realizada el 08/11/2017
                    //ingreso.setIdSocio(descuento.getIdSocio());
                    ingreso.setIdSocio(modDescarga.getEntidad(k).getIdSocio());
                    // -----------------------------------------
                    ingreso.setFechaAAplicar(descuento.getPeriodo());
                    ingreso.setRuc(descuento.getCedula());
                    ingreso.setImporte(descuento.getMonto());
                    ingreso.setSaldo(descuento.getMonto());
                    ingreso.setIdMoneda(mnt.intPrincipal.pnlEstado.defecto.getIdMoneda());
                    ingreso.setIdUsuario(mnt.intPrincipal.pnlEstado.usuario.getIdFuncionario());
                    ingreso.setObservacion("Ingreso por Giraduría");
                    ingreso.setIdEstado(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%INGRESO%", "EMITIDO"));
                    ingreso.setIdMovimiento(0); // para cancelacion
                    try { descuento.setNombre(utilitario.utiCadena.sacarCaracter(descuento.getNombre(), "'", "-"));
                    } catch (Exception e) { descuento.setNombre(""); }
                    try { descuento.setApellido(utilitario.utiCadena.sacarCaracter(descuento.getApellido(), "'", "-"));
                    } catch (Exception e) { descuento.setApellido(""); }
                    try { descuento.setDireccion(utilitario.utiCadena.sacarCaracter(descuento.getDireccion(), "'", "-"));
                    } catch (Exception e) { descuento.setDireccion(""); }
                    try { descuento.setTelefono(utilitario.utiCadena.sacarCaracter(descuento.getTelefono(), "'", "-"));
                    } catch (Exception e) { descuento.setTelefono(""); }
                    descuento.setMontoAplicado(0.0);
                    descuento.setFechaAplicada(new java.util.Date());
                    descuento.setProcesado(true);
                    descuento.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
                    ingreso.setRazonSocial(descuento.getNombre()+" "+descuento.getApellido());
                    ingreso.setDireccion(descuento.getDireccion());
                    ingreso.setTelefono(descuento.getTelefono());
                    ingreso.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
                    icantidadExterno = this.getCantidadExterno(k, descuento.getCedula());
                }
            }
            if (ingreso.getSaldo() > 0) {
                //cambio.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwcambiorubro1), " AND numero="+modDescarga.getEntidad(k).getNumeroOperacion()+" AND tabla='mo' AND idgiraduria=7", "");
                //cambio.obtenerDato();
                //if (cambio.mod.getRowCount() == 0) {
                detalle = new especifico.entDetalleIngresonew();
                detalle.setcIdMovimiento(modDescarga.getEntidad(k).getIdMovimiento());
                detalle.setcIdIngreso(ingreso.getId());
                detalle.setdIdDetalleOperacion(modDescarga.getEntidad(k).getId());
                detalle.setdCuota(modDescarga.getEntidad(k).getCuota());
                detalle.setdFechaVencimiento(modDescarga.getEntidad(k).getFechaVencimiento());
                detalle.setdMontoCapital(modDescarga.getEntidad(k).getMontoCapital());
                detalle.setdMontoInteres(modDescarga.getEntidad(k).getMontoInteres());
                detalle.setdMontoExoneracion(0.0);
                detalle.setdEstado("A");
                if (utilitario.utiFecha.getAM(detalle.getdFechaVencimiento())==utilitario.utiFecha.getAM(this.txtFechaProceso.getFecha())) detalle.setdEstado("C");
                detalle.setcTabla(modDescarga.getEntidad(k).getTabla());
                detalle.setcNumeroOperacion(modDescarga.getEntidad(k).getNumeroOperacion());
                detalle.setcPlazo(modDescarga.getEntidad(k).getPlazo());
                detalle.setcFechaOperacion(modDescarga.getEntidad(k).getFechaOperacion());
                detalle.setcTasaInteresMoratorio(0.0);
                detalle.setcTasaInteresPunitorio(0.0);
                detalle.setdMontoInteresMoratorio(0.0);
                detalle.setdMontoInteresPunitorio(0.0);
                detalle.setcTasaExoneracion(0.0);
                detalle.setcCancela(false);
                detalle.setcIdEntidad(modDescarga.getEntidad(k).getIdEntidad());
                detalle.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
                if (!modDescarga.getEntidad(k).getTabla().equals(especifico.entDetalleOperacion.tabla_comercioexterno)) { // CUENTAS INTERNAS
                    if (modDescarga.getEntidad(k).getSaldoInteres() > 0 && ingreso.getSaldo() > 0) {
                        detalle.setdId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("detalleingreso"));
                        detalle.setcIdCuenta(iidInteresPrestamo);
                        if (ingreso.getSaldo() > modDescarga.getEntidad(k).getSaldoInteres()) {
                            ingreso.setSaldo(ingreso.getSaldo() - modDescarga.getEntidad(k).getSaldoInteres());
                            descuento.setMontoAplicado(descuento.getMontoAplicado() + modDescarga.getEntidad(k).getSaldoInteres());
                            detalle.setdMontoCobro(modDescarga.getEntidad(k).getSaldoInteres());
                        } else {
                            detalle.setdMontoCobro(ingreso.getSaldo());
                            descuento.setMontoAplicado(descuento.getMontoAplicado() + ingreso.getSaldo());
                            ingreso.setSaldo(0.0);
                        }
                        System.out.println(detalle.getSentencia());
                        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(detalle.getSentencia());
                        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(detalle.getActualizarMovimiento(true, false));
                    }
                    if (modDescarga.getEntidad(k).getSaldoCapital() > 0 && ingreso.getSaldo() > 0) {
                        detalle.setdId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("detalleingreso"));
                        detalle.setcIdCuenta(modDescarga.getEntidad(k).getIdCuenta());
                        if (ingreso.getSaldo() > modDescarga.getEntidad(k).getSaldoCapital()) {
                            ingreso.setSaldo(ingreso.getSaldo() - modDescarga.getEntidad(k).getSaldoCapital());
                            descuento.setMontoAplicado(descuento.getMontoAplicado() + modDescarga.getEntidad(k).getSaldoCapital());
                            detalle.setdMontoCobro(modDescarga.getEntidad(k).getSaldoCapital());
                        } else {
                            detalle.setdMontoCobro(ingreso.getSaldo());
                            descuento.setMontoAplicado(descuento.getMontoAplicado() + ingreso.getSaldo());
                            ingreso.setSaldo(0.0);
                        }
                        System.out.println(detalle.getSentencia());
                        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(detalle.getSentencia());
                        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(detalle.getActualizarMovimiento(false, false));
                    }
                } else { // CUENTAS EXTERNAS
                    detalle.setdId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("detalleingreso"));
                    detalle.setcIdCuenta(modDescarga.getEntidad(k).getIdCuenta());
                    double umontoAplicar = utilitario.utiNumero.redondear(ingreso.getSaldo()/icantidadExterno,0);
                    if (modDescarga.getEntidad(k).getSaldoCapital() < umontoAplicar) {
                        detalle.setdMontoCobro(modDescarga.getEntidad(k).getSaldoCapital());
                        descuento.setMontoAplicado(descuento.getMontoAplicado() + modDescarga.getEntidad(k).getSaldoCapital());
                        ingreso.setSaldo(ingreso.getSaldo() - modDescarga.getEntidad(k).getSaldoCapital());
                    } else {
                        detalle.setdMontoCobro(umontoAplicar);
                        descuento.setMontoAplicado(descuento.getMontoAplicado() + umontoAplicar);
                        ingreso.setSaldo(ingreso.getSaldo() - umontoAplicar);
                    }
                    icantidadExterno = icantidadExterno-1;
                    System.out.println(detalle.getSentencia());
                    mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(detalle.getSentencia());
                    mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(detalle.getActualizarMovimientoExterno(false));
                }
                //}
            }
            k++;
            if (k == modDescarga.getRowCount()) {
                System.out.println(ingreso.getSentencia());
                mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(ingreso.getSentencia());
                System.out.println(descuento.getSentencia2());
                mnt.intPrincipal.pnlEstado.conexion.ejecutarActualizacion(descuento.getSentencia2());
            }
        }
        javax.swing.JOptionPane.showMessageDialog(null, "Proceso concluido.");
    }

    /*private especifico.entArchivoGiraduria getDescuento2(String scedula) {
        especifico.entArchivoGiraduria ent = new especifico.entArchivoGiraduria();
        for (int i=0; i<modArchivo.getRowCount(); i++) {
            //System.out.println(modArchivo.getEntidad(i).getCedula()+"-"+scedula);
            if (modArchivo.getEntidad(i).getCedula().trim().equals(scedula.trim())) {
                ent.setMonto(ent.getMonto() + modArchivo.getEntidad(i).getMonto());
                ent.setCedula(modArchivo.getEntidad(i).getCedula());
                ent.setIdSocio(modArchivo.getEntidad(i).getIdSocio());
                ent.setPeriodo(modArchivo.getEntidad(i).getPeriodo());
                ent.setNombre(utilitario.utiCadena.sacarCaracter(modArchivo.getEntidad(i).getNombre(), "'", "-"));
                ent.setApellido(utilitario.utiCadena.sacarCaracter(modArchivo.getEntidad(i).getApellido(), "'", "-"));
                ent.setDireccion(utilitario.utiCadena.sacarCaracter(modArchivo.getEntidad(i).getDireccion(), "'", "-"));
                ent.setTelefono(utilitario.utiCadena.sacarCaracter(modArchivo.getEntidad(i).getTelefono(), "'", "-"));
                ent.setMontoAplicado(0.0);
                ent.setFechaAplicada(new java.util.Date());
                ent.setProcesado(true);
                ent.setIdGiraduria(modArchivo.getEntidad(i).getIdGiraduria());
                ent.setIdRubro(modArchivo.getEntidad(i).getIdRubro());
                ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
            }
        }
        return ent;
    }*/
    
    private int getCantidadExterno(int i, String scedula) {
        int icantidad = 0;
        while (i<modDescarga.getRowCount() && modDescarga.getEntidad(i).getCedula().trim().equals(scedula.trim())) {
            if (modDescarga.getEntidad(i).getTabla().equals(especifico.entDetalleOperacion.tabla_comercioexterno)) {
                icantidad++;
            }
            i++;
        }
        return icantidad;
    }
    
}
