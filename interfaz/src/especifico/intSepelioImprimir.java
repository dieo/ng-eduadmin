/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intAhorroProgramadoAnular.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intSepelioImprimir extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    private utilitario.comComboBox cmbCobertura;
    private utilitario.comComboBox cmbRegional;
    private utilitario.comComboBox cmbEstado;
    
    /** Creates new form intAhorroProgramadoRetirar 
     @param mnt.
     */
    public intSepelioImprimir(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
        this.setEnabled();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfIdEstadoAhorro = new javax.swing.JFormattedTextField();
        pnlEstado = new javax.swing.JPanel();
        btnImprimirFecha = new javax.swing.JButton();
        try {
            txtFechaDesde = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        try {
            txtFechaHasta = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblDesde = new javax.swing.JLabel();
        lblHasta = new javax.swing.JLabel();
        chkCobertura = new javax.swing.JCheckBox();
        cmboCobertura = new javax.swing.JComboBox();
        lblCobertura = new javax.swing.JLabel();
        lblRegional = new javax.swing.JLabel();
        chkRegional = new javax.swing.JCheckBox();
        cmboRegional = new javax.swing.JComboBox();
        lblEstado = new javax.swing.JLabel();
        chkEstado = new javax.swing.JCheckBox();
        cmboEstado = new javax.swing.JComboBox();
        pnlImprimirLiquidacion = new javax.swing.JPanel();
        chkBeneficiario = new javax.swing.JCheckBox();
        lblCedula = new javax.swing.JLabel();
        btnImprimirSocioBeneficiario = new javax.swing.JButton();
        txtfCedula = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(405, 220));
        setPreferredSize(new java.awt.Dimension(405, 220));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfIdEstadoAhorro.setFocusable(false);
        txtfIdEstadoAhorro.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfIdEstadoAhorro, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 0, 25));

        pnlEstado.setBackground(new java.awt.Color(255, 255, 255));
        pnlEstado.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlEstado.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnImprimirFecha.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirFecha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirFecha.setToolTipText("Imprimir por Fecha");
        btnImprimirFecha.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirFecha.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirFecha.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirFecha.setOpaque(false);
        btnImprimirFecha.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlEstado.add(btnImprimirFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 110, 25, 25));
        pnlEstado.add(txtFechaDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(85, 85, -1, 25));
        pnlEstado.add(txtFechaHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(85, 110, -1, 25));

        lblDesde.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblDesde.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDesde.setText("Desde");
        pnlEstado.add(lblDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 85, 65, 25));

        lblHasta.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblHasta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblHasta.setText("Hasta");
        pnlEstado.add(lblHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 65, 25));

        chkCobertura.setToolTipText("");
        chkCobertura.setOpaque(false);
        pnlEstado.add(chkCobertura, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 20, 25));

        cmboCobertura.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboCobertura, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 275, 25));

        lblCobertura.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblCobertura.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCobertura.setText("Cobertura");
        pnlEstado.add(lblCobertura, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 65, 25));

        lblRegional.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblRegional.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblRegional.setText("Regional");
        pnlEstado.add(lblRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 65, 25));

        chkRegional.setToolTipText("");
        chkRegional.setOpaque(false);
        pnlEstado.add(chkRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 35, 20, 25));

        cmboRegional.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, 275, 25));

        lblEstado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstado.setText("Estado");
        pnlEstado.add(lblEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 65, 25));

        chkEstado.setToolTipText("");
        chkEstado.setOpaque(false);
        pnlEstado.add(chkEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, 20, 25));

        cmboEstado.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 275, 25));

        add(pnlEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 65, 385, 145));

        pnlImprimirLiquidacion.setBackground(new java.awt.Color(255, 255, 255));
        pnlImprimirLiquidacion.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlImprimirLiquidacion.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        chkBeneficiario.setToolTipText("");
        chkBeneficiario.setOpaque(false);
        pnlImprimirLiquidacion.add(chkBeneficiario, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 20, 25));

        lblCedula.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblCedula.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCedula.setText("Cédula");
        pnlImprimirLiquidacion.add(lblCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 65, 25));

        btnImprimirSocioBeneficiario.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirSocioBeneficiario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirSocioBeneficiario.setToolTipText("Imprimir por Socio");
        btnImprimirSocioBeneficiario.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirSocioBeneficiario.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirSocioBeneficiario.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirSocioBeneficiario.setOpaque(false);
        btnImprimirSocioBeneficiario.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlImprimirLiquidacion.add(btnImprimirSocioBeneficiario, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, 25, 25));

        txtfCedula.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pnlImprimirLiquidacion.add(txtfCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 10, 90, 25));

        add(pnlImprimirLiquidacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 385, 45));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnImprimirFecha;
    private javax.swing.JButton btnImprimirSocioBeneficiario;
    private javax.swing.JCheckBox chkBeneficiario;
    private javax.swing.JCheckBox chkCobertura;
    private javax.swing.JCheckBox chkEstado;
    private javax.swing.JCheckBox chkRegional;
    private javax.swing.JComboBox cmboCobertura;
    private javax.swing.JComboBox cmboEstado;
    private javax.swing.JComboBox cmboRegional;
    private javax.swing.JLabel lblCedula;
    private javax.swing.JLabel lblCobertura;
    private javax.swing.JLabel lblDesde;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblHasta;
    private javax.swing.JLabel lblRegional;
    private javax.swing.JPanel pnlEstado;
    private javax.swing.JPanel pnlImprimirLiquidacion;
    private panel.fecha txtFechaDesde;
    private panel.fecha txtFechaHasta;
    public javax.swing.JTextField txtfCedula;
    private javax.swing.JFormattedTextField txtfIdEstadoAhorro;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnImprimirSocioBeneficiario.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirSocio();
            }
        });
        btnImprimirFecha.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirFecha();
            }
        });
        chkCobertura.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbCobertura.setId(0);
                cmbCobertura.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkRegional.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbRegional.setId(0);
                cmbRegional.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkEstado.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbEstado.setId(0);
                cmbEstado.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
    }

    private void setEnabled() {
        this.chkCobertura.setSelected(true);
        this.chkRegional.setSelected(true);
        this.chkEstado.setSelected(true);
    }
    
    private void iniciarComponentes() {
        this.cmbCobertura = new utilitario.comComboBox(this.cmboCobertura, this.lblCobertura, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwcoberturasepelio1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbRegional = new utilitario.comComboBox(this.cmboRegional, this.lblRegional, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwregional1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbEstado = new utilitario.comComboBox(this.cmboEstado, this.lblEstado, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwsubtipo)+" WHERE tipo LIKE '%ESTADO SEPELIO%' ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        
        this.chkBeneficiario.setToolTipText("Imprime por Beneficiario");
        this.txtfCedula.setToolTipText("Cédula del Socio o Beneficiario");
        this.btnImprimirSocioBeneficiario.setToolTipText("Imprime por Socio o por Beneficiario");
        this.chkCobertura.setToolTipText("Habilita/Deshabilita la selección de Cobertura");
        this.chkRegional.setToolTipText("Habilita/Deshabilita la selección de Regional");
        this.chkEstado.setToolTipText("Habilita/Deshabilita la selección de Estado");
        this.cmbCobertura.setToolTipText("Cobertura");
        this.cmbRegional.setToolTipText("Regional");
        this.cmbEstado.setToolTipText("Estado");
        this.txtFechaDesde.setToolTipText("Fecha desde");
        this.txtFechaHasta.setToolTipText("Fecha hasta");
        this.btnImprimirSocioBeneficiario.setToolTipText("Imprime según el criterio");
    }

    private void imprimirSocio() {
        if (this.chkBeneficiario.isSelected()) {
            java.sql.ResultSet sepelio = null;
            String ssentencia = "SELECT s.*, bs.idsepelio, bs.cedula AS cedulabeneficiario, bs.nombre AS nombrebeneficiario, bs.apellido AS apellidobeneficiario, bs.fechanacimiento AS fechanacimientobeneficiario, bs.parentesco AS parentescobeneficiario, bs.montoaprobado AS montoaprobadobeneficiario FROM "+generico.vista.getNombre(generico.vista.vwsepelio1)+" AS s LEFT JOIN "+generico.vista.getNombre(generico.vista.vwbeneficiariosepelio1)+" AS bs ON s.id=bs.idsepelio WHERE bs.cedula="+utilitario.utiCadena.getTextoGuardado(this.txtfCedula.getText().trim())+" ORDER BY s.numerosolicitud";
            if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(ssentencia)) { // obtiene datos de la base de datos
                javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            }
            try {
                sepelio = mnt.intPrincipal.pnlEstado.conexion.rs;
                mnt.intPrincipal.pnlEstado.conexion.rs.last();
                if (mnt.intPrincipal.pnlEstado.conexion.rs.getRow()<=0) {
                    javax.swing.JOptionPane.showMessageDialog(null, "Cédula "+this.txtfCedula.getText().trim()+" no existe");
                    return;
                }
            } catch (Exception e) {
                return;
            }
            // =====================
            reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
            if (!rep.imprimir(generico.reporte.repSepelioIndividualBeneficiario, sepelio, null, null, null, "Sepelio por Beneficiario", mnt.intPrincipal.pnlEstado.sfiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
                javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
            }
        } else { // para socios
            int idSocio = mnt.intPrincipal.pnlEstado.conexion.obtenerId(generico.vista.getNombre(generico.vista.vwsocio1), "cedula="+utilitario.utiCadena.getTextoGuardado(this.txtfCedula.getText().trim()));
            java.sql.ResultSet sepelio = null;
            java.sql.ResultSet documento = null;
            String ssentencia = "SELECT s.*, bs.idsepelio, bs.cedula AS cedulabeneficiario, bs.nombre AS nombrebeneficiario, bs.apellido AS apellidobeneficiario, bs.fechanacimiento AS fechanacimientobeneficiario, bs.parentesco AS parentescobeneficiario, bs.montoaprobado AS montoaprobadobeneficiario FROM "+generico.vista.getNombre(generico.vista.vwsepelio1)+" AS s LEFT JOIN "+generico.vista.getNombre(generico.vista.vwbeneficiariosepelio1)+" AS bs ON s.id=bs.idsepelio WHERE s.idsocio="+idSocio+" ORDER BY s.numerosolicitud";
            if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(ssentencia)) { // obtiene datos de la base de datos
                javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            }
            try {
                sepelio = mnt.intPrincipal.pnlEstado.conexion.rs;
                mnt.intPrincipal.pnlEstado.conexion.rs.last();
                if (mnt.intPrincipal.pnlEstado.conexion.rs.getRow()<=0) {
                    javax.swing.JOptionPane.showMessageDialog(null, "Cédula "+this.txtfCedula.getText().trim()+" no existe");
                    return;
                }
            } catch (Exception e) {
                return;
            }
            ssentencia = "SELECT s.*, ds.idsepelio, ds.descripcion AS documento, CASE chequeado WHEN true THEN 'SI' WHEN false THEN 'NO' END AS chequeado, ds.observacion FROM "+generico.vista.getNombre(generico.vista.vwsepelio1)+" AS s LEFT JOIN "+generico.vista.getNombre(generico.vista.vwdetallesepelio1)+" AS ds ON s.id=ds.idsepelio WHERE s.idsocio="+idSocio+" ORDER BY s.numerosolicitud";
            if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(ssentencia)) { // obtiene datos de la base de datos
                javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            }
            documento = mnt.intPrincipal.pnlEstado.conexion.rs;
            // =====================
            reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
            if (!rep.imprimir(generico.reporte.repSepelioIndividual, sepelio, documento, null, null, "Sepelio por Socio", mnt.intPrincipal.pnlEstado.sfiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
                javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
            }
        }
    }

    private void imprimirFecha() {
        String sfiltro = "";
        String stextoFiltro = "";
        if (this.cmbCobertura.getIdSeleccionado()>0) {
            sfiltro += " AND idcoberturasepelio="+this.cmbCobertura.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbCobertura.getDescripcionSeleccionado();
        }
        if (this.cmbRegional.getIdSeleccionado()>0) {
            sfiltro += " AND idregional="+this.cmbRegional.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbRegional.getDescripcionSeleccionado();
        }
        if (this.cmbEstado.getIdSeleccionado()>0) {
            sfiltro += " AND idestado="+this.cmbEstado.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbEstado.getDescripcionSeleccionado();
        }
        if (this.txtFechaDesde.getFecha()==null && this.txtFechaHasta.getFecha()!=null) {
            sfiltro += " AND fechasolicitud="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "FECHA SOLICITUD: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()==null) {
            sfiltro += " AND fechasolicitud="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "FECHA SOLICITUD: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()!=null) {
            sfiltro += " AND fechasolicitud>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND fechasolicitud<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "FECHA SOLICITUD: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" a "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        especifico.intSepelio sepelio = new especifico.intSepelio(mnt);
        sepelio.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwsepelio1), sfiltro, " ORDER BY estado, regional, fechasolicitud");
        sepelio.obtenerDato();
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repSepelio, sepelio.mod, null, null, null, "Informe Sepelio", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }
    
}
