/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intDepositoCuentaDetalle.java
 *
 * Created on 07-abr-2012, 21:48:31
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class intbDepositoCuentaDetalle extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modbDepositoCuentaDetalle mod = new especifico.modbDepositoCuentaDetalle();
    private utilitario.comComboBox cmbBanco;
    private String svista="", sfiltro="", sorden="";
    
    /** Creates new form intDepositoCuentaDetalle 
     @param mnt.
     @param tbl.
     */
    public intbDepositoCuentaDetalle(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        txtfMontoOtro = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblNumeroCheque = new javax.swing.JLabel();
        lblMonto = new javax.swing.JLabel();
        lblBanco = new javax.swing.JLabel();
        txtfNumeroCheque = new javax.swing.JFormattedTextField();
        txtfMontoPropio = new javax.swing.JFormattedTextField();
        lblMonto1 = new javax.swing.JLabel();
        cmboBanco = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(520, 144));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(520, 144));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, 110, 25));

        txtfMontoOtro.setForeground(new java.awt.Color(0, 102, 204));
        txtfMontoOtro.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMontoOtro.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfMontoOtro.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfMontoOtro.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfMontoOtro, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 85, 110, 25));

        lblId.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 80, 25));

        lblNumeroCheque.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblNumeroCheque.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumeroCheque.setText("Cheque Número");
        add(lblNumeroCheque, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 35, 80, 25));

        lblMonto.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblMonto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMonto.setText("Cheque Otro Banco");
        add(lblMonto, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 85, 110, 25));

        lblBanco.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblBanco.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblBanco.setText("Banco");
        add(lblBanco, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 80, 25));

        txtfNumeroCheque.setForeground(new java.awt.Color(0, 102, 204));
        txtfNumeroCheque.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfNumeroCheque.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfNumeroCheque.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfNumeroCheque, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 35, 110, 25));

        txtfMontoPropio.setForeground(new java.awt.Color(0, 102, 204));
        txtfMontoPropio.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMontoPropio.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfMontoPropio.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfMontoPropio.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfMontoPropio, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 110, 110, 25));

        lblMonto1.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblMonto1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMonto1.setText("Cheque Mismo Banco");
        add(lblMonto1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 110, 110, 25));

        cmboBanco.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboBanco, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 60, 390, 25));
    }// </editor-fold>//GEN-END:initComponents

    private void tblListadoChange(javax.swing.event.ListSelectionEvent e) {
        this.cargarEditor();
    }
    
 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmboBanco;
    private javax.swing.JLabel lblBanco;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblMonto;
    private javax.swing.JLabel lblMonto1;
    private javax.swing.JLabel lblNumeroCheque;
    private javax.swing.JFormattedTextField txtfId;
    private javax.swing.JFormattedTextField txtfMontoOtro;
    private javax.swing.JFormattedTextField txtfMontoPropio;
    private javax.swing.JFormattedTextField txtfNumeroCheque;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        this.mod.getTable().getSelectionModel().addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            @Override
            public void valueChanged(javax.swing.event.ListSelectionEvent e){
                tblListadoChange(e);
            }
        });
        this.cmboBanco.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                habilitarmonto();
            }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfNumeroCheque);
        uti.agregarEventoFocusGained(this.txtfMontoOtro);
        uti.agregarEventoFocusGained(this.txtfMontoPropio);
        uti.agregarEventoKeyReleased(this.txtfNumeroCheque);
        uti.agregarEventoKeyReleased(this.txtfMontoOtro);
        uti.agregarEventoKeyReleased(this.txtfMontoPropio);
    }
    
    public boolean[] getEstadoHerramienta(boolean on) {
        if (on) return new boolean[] {true , true , true };
        else    return new boolean[] {false, false, false};
    }

    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.txtfNumeroCheque.setEnabled(bmodo);
        this.cmbBanco.setEnabled(bmodo);
        this.txtfMontoOtro.setEnabled(bmodo);
        this.txtfMontoPropio.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.cmbBanco = new utilitario.comComboBox(this.cmboBanco, this.lblBanco, "SELECT id, descripcion FROM banco.vwbanco1 ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);

        this.txtfNumeroCheque.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfMontoOtro.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfMontoPropio.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfId.setToolTipText(especifico.entbDepositoCuentaDetalle.TEXTO_ID);
        this.cmbBanco.setToolTipText(especifico.entbDepositoCuentaDetalle.TEXTO_BANCO);
        this.txtfNumeroCheque.setToolTipText(especifico.entbDepositoCuentaDetalle.TEXTO_NUMEROCHEQUE);
        this.txtfMontoOtro.setToolTipText(especifico.entbDepositoCuentaDetalle.TEXTO_MONTO_OTRO);
        this.txtfMontoPropio.setToolTipText(especifico.entbDepositoCuentaDetalle.TEXTO_MONTO_PROPIO);
    }
    
    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entbDepositoCuentaDetalle());
        }
    }

    public void habilitarmonto() {
        especifico.intbDepositoCuenta deposito = (especifico.intbDepositoCuenta)generico.clsPadre.getPadre(mod.getTable(), especifico.intbDepositoCuenta.class);
        if (this.cmbBanco.getIdSeleccionado()==deposito.iidBanco) {
            this.txtfMontoPropio.requestFocus();
            this.txtfMontoPropio.setEnabled(true);
            this.txtfMontoOtro.setEnabled(false);
            this.txtfMontoOtro.setValue(0);
        } else {
            this.txtfMontoOtro.requestFocus();
            this.txtfMontoPropio.setEnabled(false);
            this.txtfMontoOtro.setEnabled(true);
            this.txtfMontoPropio.setValue(0);
        }
    }
    
    public void insertar() {
        especifico.entbDepositoCuentaDetalle ent = new especifico.entbDepositoCuentaDetalle();
        //ent.setIdDepositoCuenta(((especifico.modbDepositoCuenta)mnt.tblListado.getModel()).getEntidad(mnt.tblListado.getSelectedRow()).getId()); // obtiene el id de deposito del modelo y asigna
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
    }

    public boolean establecer() {
        especifico.entbDepositoCuentaDetalle ent = mod.getEntidad().copiar(new especifico.entbDepositoCuentaDetalle());
        this.getInterfaz(ent);
        if (!ent.esValido()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("detalledeposito"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
        this.cmbBanco.refresh();
    }
    
    private void getInterfaz(especifico.entbDepositoCuentaDetalle ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setIdBanco(this.cmbBanco.getIdSeleccionado());
        ent.setBanco(this.cmbBanco.getDescripcionSeleccionado());
        ent.setNumeroCheque(utilitario.utiNumero.convertirToInt(this.txtfNumeroCheque.getText()));
        ent.setMontoOtro(utilitario.utiNumero.convertirToDoubleMac(this.txtfMontoOtro.getText()));
        ent.setMontoPropio(utilitario.utiNumero.convertirToDoubleMac(this.txtfMontoPropio.getText()));
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entbDepositoCuentaDetalle ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.cmbBanco.setId(ent.getIdBanco());}
        catch (Exception e) {this.cmbBanco.setId(0);}
        try                 {this.txtfNumeroCheque.setValue(ent.getNumeroCheque());}
        catch (Exception e) {this.txtfNumeroCheque.setValue(0);}
        try                 {this.txtfMontoOtro.setValue(ent.getMontoOtro());}
        catch (Exception e) {this.txtfMontoOtro.setValue(0);}
        try                 {this.txtfMontoPropio.setValue(ent.getMontoPropio());}
        catch (Exception e) {this.txtfMontoPropio.setValue(0);}
    }
    
    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

}
