/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intOrdenCredito.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intControlArchivoCancela extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modControlArchivo mod = new especifico.modControlArchivo();
    private String svista="", sfiltro="", sorden="", sfiltroFormulario="";

    /** Creates new form intOrdenCredito
     @param mnt.
     @param tbl.
     */
    public intControlArchivoCancela(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblObservacion = new javax.swing.JLabel();
        txtfObservacion = new javax.swing.JFormattedTextField();
        lblFecha = new javax.swing.JLabel();
        try {
            txtFecha = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        tblOrdenCredito = new javax.swing.JTable();
        txtfIdEstadoSolicitud = new javax.swing.JFormattedTextField();
        lblUsuario = new javax.swing.JLabel();
        txtfUsuario = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        txtfId = new javax.swing.JFormattedTextField();
        txtfIdUsuario = new javax.swing.JFormattedTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setMinimumSize(new java.awt.Dimension(635, 95));
        setPreferredSize(new java.awt.Dimension(635, 95));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblObservacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblObservacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObservacion.setText("Observación");
        add(lblObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 80, 25));

        txtfObservacion.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        add(txtfObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 525, 25));

        lblFecha.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFecha.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFecha.setText("Fecha");
        add(lblFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 80, 25));
        add(txtFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, -1, -1));

        tblOrdenCredito.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        add(tblOrdenCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 0, 0));

        txtfIdEstadoSolicitud.setBorder(null);
        txtfIdEstadoSolicitud.setFocusable(false);
        txtfIdEstadoSolicitud.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfIdEstadoSolicitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, 0, 25));

        lblUsuario.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUsuario.setText("Usuario");
        add(lblUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 80, 25));

        txtfUsuario.setBorder(null);
        txtfUsuario.setFocusable(false);
        txtfUsuario.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        txtfUsuario.setOpaque(false);
        add(txtfUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 10, 305, 25));

        lblId.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 80, 25));

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 100, 25));

        txtfIdUsuario.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfIdUsuario.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfIdUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 10, 10, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblObservacion;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JTable tblOrdenCredito;
    private panel.fecha txtFecha;
    private javax.swing.JFormattedTextField txtfId;
    public javax.swing.JFormattedTextField txtfIdEstadoSolicitud;
    private javax.swing.JFormattedTextField txtfIdUsuario;
    private javax.swing.JFormattedTextField txtfObservacion;
    private javax.swing.JFormattedTextField txtfUsuario;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfObservacion);
        uti.agregarEventoKeyReleased(this.txtfObservacion);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.txtFecha.setEnabled(bmodo);
        this.txtfObservacion.setEnabled(bmodo);
    }
    
    public final boolean isEnabled() {
        return this.txtfObservacion.isEnabled();
    }
    
    private void iniciarComponentes() {
        this.txtfIdUsuario.setVisible(false);
        this.txtfObservacion.setDocument(new generico.modFormatoCadena(this.txtfObservacion, especifico.entControlArchivo.LONGITUD_OBSERVACION_CANCELA, true));
        this.txtFecha.setToolTipText(especifico.entControlArchivo.TEXTO_FECHA_CANCELA);
        this.txtfObservacion.setToolTipText(especifico.entControlArchivo.TEXTO_OBSERVACION_CANCELA);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entControlArchivo());
        }
    }

    public boolean insertar() {
        especifico.entControlArchivo ent = new especifico.entControlArchivo();
        ent.setId(mod.getEntidad().getId());
        ent.setFechaCancela(new java.util.Date());
        ent.setIdFuncionarioCancela(mnt.intPrincipal.pnlEstado.usuario.getIdFuncionario());
        ent.setFuncionarioCancela(mnt.intPrincipal.pnlEstado.usuario.getFuncionario());
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar(especifico.entControlArchivo ent) {
        this.setInterfaz(ent);
        return true;
    }

    public boolean establecer() {
        especifico.entControlArchivo ent = mod.getEntidad().copiar(new especifico.entControlArchivo()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValidoCancela()) { // valida la cancelacion
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro+sfiltroFormulario, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }
    
    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entControlArchivo ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setFechaCancela(this.txtFecha.getFecha());
        ent.setIdFuncionarioCancela(utilitario.utiNumero.convertirToInt(this.txtfIdUsuario.getText()));
        ent.setFuncionarioCancela(this.txtfUsuario.getText());
        ent.setObservacionCancela(this.txtfObservacion.getText());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }

    private void setInterfaz(especifico.entControlArchivo ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.txtFecha.setFecha(ent.getFechaCancela());}
        catch (Exception e) {this.txtFecha.setFechaVacia();}
        try                 {this.txtfIdUsuario.setValue(ent.getIdFuncionarioCancela());}
        catch (Exception e) {this.txtfIdUsuario.setValue(0);}
        try                 {this.txtfUsuario.setText(ent.getFuncionarioCancela());}
        catch (Exception e) {this.txtfUsuario.setText("");}
        try                 {this.txtfObservacion.setText(ent.getObservacionCancela());}
        catch (Exception e) {this.txtfObservacion.setText("");}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================

}
