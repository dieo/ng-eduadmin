/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intOrdenCreditoSolicitar.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intSepelioAprobar extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modSepelio mod = new especifico.modSepelio();
    private String svista="", sfiltro="", sorden="", sfiltroFormulario="";
    
    /** Creates new form intOrdenCreditoSolicitar 
     @param mnt.
     @param tbl.
     */
    public intSepelioAprobar(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        txtfNumeroSolicitud = new javax.swing.JFormattedTextField();
        txtfEstadoSolicitud = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblNumeroSolicitud = new javax.swing.JLabel();
        lblEstadoSolicitud = new javax.swing.JLabel();
        lblFechaAprobado = new javax.swing.JLabel();
        try {
            txtFechaAprobado = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        txtfObservacion = new javax.swing.JFormattedTextField();
        lblObservacion = new javax.swing.JLabel();
        lblVerSolidaridad = new javax.swing.JLabel();
        btnSolicitud = new javax.swing.JButton();
        txtfIdEstadoSolicitud = new javax.swing.JFormattedTextField();
        lblMontoAprobado = new javax.swing.JLabel();
        txtfMontoAprobado = new javax.swing.JFormattedTextField();
        lblFechaDictamen = new javax.swing.JLabel();
        txtfNumeroDictamen = new javax.swing.JFormattedTextField();
        lblNumeroDictamen = new javax.swing.JLabel();
        try {
            txtFechaDictamen = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(625, 140));
        setPreferredSize(new java.awt.Dimension(625, 140));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 110, 25));

        txtfNumeroSolicitud.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfNumeroSolicitud.setFocusable(false);
        txtfNumeroSolicitud.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfNumeroSolicitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, 110, 25));

        txtfEstadoSolicitud.setBorder(null);
        txtfEstadoSolicitud.setFocusable(false);
        txtfEstadoSolicitud.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        txtfEstadoSolicitud.setOpaque(false);
        add(txtfEstadoSolicitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 10, 300, 25));

        lblId.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 90, 25));

        lblNumeroSolicitud.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblNumeroSolicitud.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumeroSolicitud.setText("Número Solicitud");
        add(lblNumeroSolicitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 90, 25));

        lblEstadoSolicitud.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblEstadoSolicitud.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstadoSolicitud.setText("Estado Solicitud");
        add(lblEstadoSolicitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, 90, 25));

        lblFechaAprobado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaAprobado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaAprobado.setText("Fecha Aprobación");
        add(lblFechaAprobado, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 90, 25));
        add(txtFechaAprobado, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, -1, -1));

        txtfObservacion.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        add(txtfObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 85, 520, 25));

        lblObservacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblObservacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObservacion.setText("Observación");
        add(lblObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 85, 90, 25));

        lblVerSolidaridad.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblVerSolidaridad.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblVerSolidaridad.setText("Ver Solidaridad");
        add(lblVerSolidaridad, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 35, 90, 25));

        btnSolicitud.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/visualizar.png"))); // NOI18N
        btnSolicitud.setToolTipText("Ver Ahorro");
        btnSolicitud.setFocusable(false);
        btnSolicitud.setOpaque(false);
        add(btnSolicitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 35, 25, 25));

        txtfIdEstadoSolicitud.setBorder(null);
        txtfIdEstadoSolicitud.setFocusable(false);
        txtfIdEstadoSolicitud.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfIdEstadoSolicitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 0, 25));

        lblMontoAprobado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblMontoAprobado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMontoAprobado.setText("Monto Aprobado");
        add(lblMontoAprobado, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 60, 90, 25));

        txtfMontoAprobado.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMontoAprobado.setFocusable(false);
        txtfMontoAprobado.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        add(txtfMontoAprobado, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 60, 110, 25));

        lblFechaDictamen.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaDictamen.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaDictamen.setText("Fecha Dictamen");
        add(lblFechaDictamen, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 110, 90, 25));

        txtfNumeroDictamen.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfNumeroDictamen.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfNumeroDictamen, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 110, 25));

        lblNumeroDictamen.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblNumeroDictamen.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumeroDictamen.setText("Número Dictamen");
        add(lblNumeroDictamen, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 110, 90, 25));
        add(txtFechaDictamen, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 110, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnSolicitud;
    private javax.swing.JLabel lblEstadoSolicitud;
    private javax.swing.JLabel lblFechaAprobado;
    private javax.swing.JLabel lblFechaDictamen;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblMontoAprobado;
    private javax.swing.JLabel lblNumeroDictamen;
    private javax.swing.JLabel lblNumeroSolicitud;
    private javax.swing.JLabel lblObservacion;
    private javax.swing.JLabel lblVerSolidaridad;
    private panel.fecha txtFechaAprobado;
    private panel.fecha txtFechaDictamen;
    private javax.swing.JFormattedTextField txtfEstadoSolicitud;
    private javax.swing.JFormattedTextField txtfId;
    public javax.swing.JFormattedTextField txtfIdEstadoSolicitud;
    private javax.swing.JFormattedTextField txtfMontoAprobado;
    private javax.swing.JFormattedTextField txtfNumeroDictamen;
    private javax.swing.JFormattedTextField txtfNumeroSolicitud;
    private javax.swing.JFormattedTextField txtfObservacion;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnSolicitud.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configurarPanelSolidaridad();
            }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfNumeroDictamen);
        uti.agregarEventoFocusGained(this.txtfMontoAprobado);
        uti.agregarEventoFocusGained(this.txtfObservacion);
        uti.agregarEventoKeyReleased(this.txtfNumeroDictamen);
        uti.agregarEventoKeyReleased(this.txtfMontoAprobado);
        uti.agregarEventoKeyReleased(this.txtfObservacion);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.txtfNumeroSolicitud.setFocusable(false);
        this.txtfMontoAprobado.setEnabled(bmodo);
        this.txtFechaAprobado.setEnabled(bmodo);
        this.txtfObservacion.setEnabled(bmodo);
        this.txtfNumeroDictamen.setEnabled(bmodo);
        this.txtFechaDictamen.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.txtfObservacion.setDocument(new generico.modFormatoCadena(this.txtfObservacion, especifico.entSepelio.LONGITUD_OBSERVACION, true));
        this.txtfNumeroDictamen.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(false, 0));
        this.txtfMontoAprobado.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfId.setToolTipText(especifico.entSepelio.TEXTO_ID);
        this.txtfEstadoSolicitud.setToolTipText(especifico.entSepelio.TEXTO_ESTADO);
        this.txtfNumeroSolicitud.setToolTipText(especifico.entSepelio.TEXTO_NUMERO_SOLICITUD);
        this.txtfMontoAprobado.setToolTipText(especifico.entSepelio.TEXTO_MONTO_APROBADO);
        this.txtFechaAprobado.setToolTipText(especifico.entSepelio.TEXTO_FECHA_APROBADO);
        this.txtfObservacion.setToolTipText(especifico.entSepelio.TEXTO_OBSERVACION_APROBADO);
        this.txtfNumeroDictamen.setToolTipText(especifico.entSepelio.TEXTO_NUMERO_DICTAMEN);
        this.txtFechaDictamen.setToolTipText(especifico.entSepelio.TEXTO_FECHA_DICTAMEN);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entSepelio());
        }
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        // pendiente de analisis
        if (mod.getEntidad().getIdEstadoSolicitud()== mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SEPELIO%", "PENDIENTE ANALISIS")) {
            especifico.entSepelio ent = new especifico.entSepelio();
            ent.setId(mod.getEntidad().getId());
            ent.setFechaAprobado(new java.util.Date());
            ent.setNumeroSolicitud(mod.getEntidad().getNumeroSolicitud());
            ent.setMontoAprobado(mod.getEntidad().getMontoAprobado());
            ent.setObservacionAprobado("---");
            ent.setFechaDictamen(new java.util.Date());
            ent.setIdEstadoSolicitud(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SEPELIO%", "APROBADO"));
            ent.setEstadoSolicitud(mnt.intPrincipal.pnlEstado.conexion.obtenerDescripcionSubtipo(ent.getIdEstadoSolicitud()));
            this.setInterfaz(ent);
        } else {
            return false;
        }
        return true;
    }
    
    public boolean establecer() {
        especifico.entSepelio ent = mod.getEntidad().copiar(new especifico.entSepelio()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValidoAprobar()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro+sfiltroFormulario, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }
    
    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entSepelio ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setNumeroSolicitud(utilitario.utiNumero.convertirToInt(this.txtfNumeroSolicitud.getText()));
        ent.setMontoAprobado(utilitario.utiNumero.convertirToDoubleMac(this.txtfMontoAprobado.getText()));
        ent.setFechaAprobado(this.txtFechaAprobado.getFecha());
        ent.setObservacionAprobado(this.txtfObservacion.getText());
        ent.setIdEstadoSolicitud((short)utilitario.utiNumero.convertirToInt(this.txtfIdEstadoSolicitud.getText()));
        ent.setNumeroDictamen(utilitario.utiNumero.convertirToInt(this.txtfNumeroDictamen.getText()));
        ent.setFechaDictamen(this.txtFechaDictamen.getFecha());
        ent.setEstadoSolicitud(this.txtfEstadoSolicitud.getText());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    public void setInterfaz(especifico.entSepelio ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.txtfIdEstadoSolicitud.setValue(ent.getIdEstadoSolicitud());}
        catch (Exception e) {this.txtfIdEstadoSolicitud.setValue(0);}
        try                 {this.txtfEstadoSolicitud.setText(ent.getEstadoSolicitud());}
        catch (Exception e) {this.txtfEstadoSolicitud.setText("");}
        try                 {this.txtfNumeroSolicitud.setValue(ent.getNumeroSolicitud());}
        catch (Exception e) {this.txtfNumeroSolicitud.setValue(0);}
        try                 {this.txtfMontoAprobado.setValue(ent.getMontoAprobado());}
        catch (Exception e) {this.txtfMontoAprobado.setValue(0);}
        try                 {this.txtFechaAprobado.setFecha(ent.getFechaAprobado());}
        catch (Exception e) {this.txtFechaAprobado.setFechaVacia();}
        try                 {this.txtfObservacion.setText(ent.getObservacionAprobado());}
        catch (Exception e) {this.txtfObservacion.setText("");}
        try                 {this.txtfNumeroDictamen.setValue(ent.getNumeroDictamen());}
        catch (Exception e) {this.txtfNumeroDictamen.setValue(0);}
        try                 {this.txtFechaDictamen.setFecha(ent.getFechaDictamen());}
        catch (Exception e) {this.txtFechaDictamen.setFechaVacia();}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }
    
    public void setFiltroFormulario(String sfiltroFormulario) {
        this.sfiltroFormulario = sfiltroFormulario;
    }
    
    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================
    
    private void configurarPanelSolidaridad() {
        especifico.intSepelio intSepelio = new especifico.intSepelio(mnt, null);
        generico.intPanel panel = new generico.intPanel("Sepelio :: Mantenimiento :: " + generico.mnuMantenimientoDetalle.visualizar, intSepelio);
        intSepelio.setEnabled(false);
        especifico.entSepelio ent = mod.getEntidad().copiar(new especifico.entSepelio()); // obtiene la entidad del registro seleccionado
        intSepelio.mod.insertar(ent);
        intSepelio.mod.getTable().setRowSelectionInterval(0,0);
        intSepelio.cargarEditor();
        if (intSepelio.mod.getTable().getSelectedRow()>=0) panel.abrir();
    }

}
