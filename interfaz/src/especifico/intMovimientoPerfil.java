/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intMoneda.java
 *
 * Created on 11-abr-2012, 6:50:50
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intMovimientoPerfil extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modMovimientoPerfil mod = new especifico.modMovimientoPerfil();
    private utilitario.comComboBox cmbPerfil;
    private String svista="", sfiltro="", sorden="";
    
    /** Creates new form intMoneda 
     @param mnt.
     @param tbl.
     */
    public intMovimientoPerfil(generico.intMantenimiento mnt, javax.swing.JTable tbl) { // llamado desde garante y orden de credito entregar
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblPerfil = new javax.swing.JLabel();
        cmboPerfil = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(430, 45));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(430, 45));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblPerfil.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblPerfil.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPerfil.setText("Perfil");
        add(lblPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 40, 25));

        cmboPerfil.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 10, 360, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmboPerfil;
    private javax.swing.JLabel lblPerfil;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        //this.txtfId.setEnabled(false);
        this.cmbPerfil.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.cmbPerfil = new utilitario.comComboBox(this.cmboPerfil, this.lblPerfil, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwperfil1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        //this.txtfId.setToolTipText(especifico.entMovimientoDescuento.TEXTO_ID);
        this.cmbPerfil.setToolTipText(especifico.entMovimientoPerfil.TEXTO_PERFIL);
    }
    
    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entMovimientoPerfil());
        }
    }

    public void insertar() {
        especifico.entMovimientoPerfil ent = new especifico.entMovimientoPerfil();
        //especifico.intUsuario usuario = (especifico.intUsuario)new generico.clsPadre().getPadre(mod.getTable(), especifico.intUsuario.class);
        //ent.setIdUsuario(usuario.mod.getEntidad(usuario.mod.getTable().getSelectedRow()).getId()); // obtiene el id de usuario del modelo y asigna
        ent.setIdUsuario(((especifico.modUsuario)mnt.tblListado.getModel()).getEntidad().getId()); // obtiene el id de usuario del modelo y asigna
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
    }

    public boolean establecer() {
        especifico.entMovimientoPerfil ent = mod.getEntidad().copiar(new especifico.entMovimientoPerfil()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValido()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (!mod.esValido(mod.getTable().getSelectedRow(), ent.getIdPerfil())) { // valida la unicidad del perfil
            javax.swing.JOptionPane.showMessageDialog(null, mod.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("movimientoperfil"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        mod.modificar(ent); // modifica
        return true;
    }
    
    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
        this.cmbPerfil.refresh();
    }

    private void getInterfaz(especifico.entMovimientoPerfil ent) {
        //ent.setId(new utilitario.utiGeneral().convertirToInt(this.txtfId.getText()));
        ent.setIdPerfil(this.cmbPerfil.getIdSeleccionado());
        ent.setPerfil(this.cmbPerfil.getDescripcionSeleccionado());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entMovimientoPerfil ent) {
        //try                 {this.txtfId.setValue(ent.getId());}
        //catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.cmbPerfil.setId(ent.getIdPerfil());}
        catch (Exception e) {this.cmbPerfil.setId(0);}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

}
