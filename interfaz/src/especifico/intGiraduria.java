/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intGiraduria.java
 *
 * Created on 04-abr-2012, 7:34:59
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intGiraduria extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modGiraduria mod = new especifico.modGiraduria();
    private String svista="", sfiltro="", sorden="";

    /** Creates new form intGiraduria 
     @param mnt.
     @param tbl.
     */
    public intGiraduria(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        txtfDescripcion = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblDescripcion = new javax.swing.JLabel();
        lblDescripcionBreve = new javax.swing.JLabel();
        txtfDescripcionBreve = new javax.swing.JFormattedTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(455, 90));
        setPreferredSize(new java.awt.Dimension(455, 90));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setToolTipText("");
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, 110, 25));

        txtfDescripcion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 35, 380, 25));

        lblId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 60, 25));

        lblDescripcion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblDescripcion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDescripcion.setText("Descripción");
        add(lblDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 60, 25));

        lblDescripcionBreve.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblDescripcionBreve.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDescripcionBreve.setText("Desc. breve");
        add(lblDescripcionBreve, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 60, 25));

        txtfDescripcionBreve.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfDescripcionBreve, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 60, 30, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblDescripcion;
    private javax.swing.JLabel lblDescripcionBreve;
    private javax.swing.JLabel lblId;
    private javax.swing.JFormattedTextField txtfDescripcion;
    private javax.swing.JFormattedTextField txtfDescripcionBreve;
    private javax.swing.JFormattedTextField txtfId;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfDescripcion);
        uti.agregarEventoFocusGained(this.txtfDescripcionBreve);
        uti.agregarEventoKeyReleased(this.txtfDescripcion);
        uti.agregarEventoKeyReleased(this.txtfDescripcionBreve);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.txtfDescripcion.setEnabled(bmodo);
        this.txtfDescripcionBreve.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.txtfDescripcion.setDocument(new generico.modFormatoCadena(this.txtfDescripcion, especifico.entGiraduria.LONGITUD_DESCRIPCION, true));
        this.txtfDescripcionBreve.setDocument(new generico.modFormatoCadena(this.txtfDescripcionBreve, especifico.entGiraduria.LONGITUD_DESCRIPCION_BREVE, true));

        this.txtfId.setToolTipText(especifico.entGiraduria.TEXTO_ID);
        this.txtfDescripcion.setToolTipText(especifico.entGiraduria.TEXTO_DESCRIPCION);
        this.txtfDescripcionBreve.setToolTipText(especifico.entGiraduria.TEXTO_DESCRIPCION_BREVE);
    }
    
    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entGiraduria());
        }
    }

    public boolean insertar() {
        especifico.entGiraduria ent = new especifico.entGiraduria();
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        especifico.entGiraduria ent = mod.getEntidad().copiar(new especifico.entGiraduria()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValido()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("giraduria"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entGiraduria ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setDescripcion(this.txtfDescripcion.getText());
        ent.setDescripcionBreve(this.txtfDescripcionBreve.getText());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entGiraduria ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.txtfDescripcion.setText(ent.getDescripcion());}
        catch (Exception e) {this.txtfDescripcion.setText("");}
        try                 {this.txtfDescripcionBreve.setText(ent.getDescripcionBreve());}
        catch (Exception e) {this.txtfDescripcionBreve.setText("");}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }
    
    public void imprimir() {
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repGiraduria, this.mod, null, null, null, "Listado de Giraduría", mnt.intPrincipal.pnlEstado.sfiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }

}
