/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intAhorroProgramadoAnular.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intAhorroProgramadoImprimir extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    private utilitario.comComboBox cmbEstado;
    private utilitario.comComboBox cmbRegional;
    private utilitario.comComboBox cmbPlan;
    private utilitario.comComboBox cmbPromotor;
    
    /** Creates new form intAhorroProgramadoRetirar 
     @param mnt.
     */
    public intAhorroProgramadoImprimir(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
        this.setEnabled();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groTipoFecha = new javax.swing.ButtonGroup();
        txtfIdEstadoAhorro = new javax.swing.JFormattedTextField();
        pnlEstado = new javax.swing.JPanel();
        lblEstado = new javax.swing.JLabel();
        cmboEstado = new javax.swing.JComboBox();
        chkEstado = new javax.swing.JCheckBox();
        btnImprimirEstadoRegional = new javax.swing.JButton();
        chkRegional = new javax.swing.JCheckBox();
        lblRegional = new javax.swing.JLabel();
        lblPlan = new javax.swing.JLabel();
        chkPlan = new javax.swing.JCheckBox();
        try {
            txtFechaDesde = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        try {
            txtFechaHasta = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblDesde = new javax.swing.JLabel();
        lblHasta = new javax.swing.JLabel();
        optInicio = new javax.swing.JRadioButton();
        optVencimiento = new javax.swing.JRadioButton();
        cmboRegional = new javax.swing.JComboBox();
        cmboPlan = new javax.swing.JComboBox();
        btnImprimirEstadoRegionalProm = new javax.swing.JButton();
        lblPromotor = new javax.swing.JLabel();
        chkPromotor = new javax.swing.JCheckBox();
        cmboPromotor = new javax.swing.JComboBox();
        chkFecha = new javax.swing.JCheckBox();
        btnImprimirEstadoRegionalInteres = new javax.swing.JButton();
        pnlImprimirLiquidacion = new javax.swing.JPanel();
        lblNumeroOperacion = new javax.swing.JLabel();
        txtfNumeroOperacion = new javax.swing.JFormattedTextField();
        btnImprimirLiquidacion = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(405, 245));
        setPreferredSize(new java.awt.Dimension(405, 245));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfIdEstadoAhorro.setFocusable(false);
        txtfIdEstadoAhorro.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfIdEstadoAhorro, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 0, 25));

        pnlEstado.setBackground(new java.awt.Color(255, 255, 255));
        pnlEstado.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlEstado.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblEstado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstado.setText("Estado");
        pnlEstado.add(lblEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 65, 25));

        cmboEstado.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 275, 25));

        chkEstado.setToolTipText("");
        chkEstado.setOpaque(false);
        pnlEstado.add(chkEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 20, 25));

        btnImprimirEstadoRegional.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirEstadoRegional.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirEstadoRegional.setToolTipText("Imprimir por Estado");
        btnImprimirEstadoRegional.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirEstadoRegional.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirEstadoRegional.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirEstadoRegional.setOpaque(false);
        btnImprimirEstadoRegional.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlEstado.add(btnImprimirEstadoRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 135, 25, 25));

        chkRegional.setToolTipText("");
        chkRegional.setOpaque(false);
        pnlEstado.add(chkRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 35, 20, 25));

        lblRegional.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblRegional.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblRegional.setText("Regional");
        pnlEstado.add(lblRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 65, 25));

        lblPlan.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblPlan.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPlan.setText("Plan");
        pnlEstado.add(lblPlan, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 65, 25));

        chkPlan.setToolTipText("");
        chkPlan.setOpaque(false);
        pnlEstado.add(chkPlan, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, 20, 25));
        pnlEstado.add(txtFechaDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, -1, 25));
        pnlEstado.add(txtFechaHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 110, -1, 25));

        lblDesde.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblDesde.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDesde.setText("Desde");
        pnlEstado.add(lblDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 65, 25));

        lblHasta.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblHasta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblHasta.setText("Hasta");
        pnlEstado.add(lblHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 110, 65, 25));

        groTipoFecha.add(optInicio);
        optInicio.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        optInicio.setSelected(true);
        optInicio.setText("Inicio");
        optInicio.setOpaque(false);
        pnlEstado.add(optInicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 135, 50, 25));

        groTipoFecha.add(optVencimiento);
        optVencimiento.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        optVencimiento.setText("Vencimiento");
        optVencimiento.setOpaque(false);
        pnlEstado.add(optVencimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 135, 90, 25));

        cmboRegional.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, 275, 25));

        cmboPlan.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboPlan, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 275, 25));

        btnImprimirEstadoRegionalProm.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirEstadoRegionalProm.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirEstadoRegionalProm.setToolTipText("Imprimir por Estado (Promotor)");
        btnImprimirEstadoRegionalProm.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirEstadoRegionalProm.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirEstadoRegionalProm.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirEstadoRegionalProm.setOpaque(false);
        btnImprimirEstadoRegionalProm.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlEstado.add(btnImprimirEstadoRegionalProm, new org.netbeans.lib.awtextra.AbsoluteConstraints(345, 135, 25, 25));

        lblPromotor.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblPromotor.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPromotor.setText("Promotor");
        pnlEstado.add(lblPromotor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 85, 65, 25));

        chkPromotor.setToolTipText("");
        chkPromotor.setOpaque(false);
        pnlEstado.add(chkPromotor, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 85, 20, 25));

        cmboPromotor.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboPromotor, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 85, 275, 25));

        chkFecha.setToolTipText("");
        chkFecha.setOpaque(false);
        pnlEstado.add(chkFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 110, 20, 25));

        btnImprimirEstadoRegionalInteres.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirEstadoRegionalInteres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirEstadoRegionalInteres.setToolTipText("Imprimir por Estado con Interes");
        btnImprimirEstadoRegionalInteres.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirEstadoRegionalInteres.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirEstadoRegionalInteres.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirEstadoRegionalInteres.setOpaque(false);
        btnImprimirEstadoRegionalInteres.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlEstado.add(btnImprimirEstadoRegionalInteres, new org.netbeans.lib.awtextra.AbsoluteConstraints(305, 135, 25, 25));

        add(pnlEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 65, 385, 170));

        pnlImprimirLiquidacion.setBackground(new java.awt.Color(255, 255, 255));
        pnlImprimirLiquidacion.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlImprimirLiquidacion.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblNumeroOperacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblNumeroOperacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumeroOperacion.setText("Número Operación");
        pnlImprimirLiquidacion.add(lblNumeroOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 90, 25));

        txtfNumeroOperacion.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfNumeroOperacion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        pnlImprimirLiquidacion.add(txtfNumeroOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 90, 25));

        btnImprimirLiquidacion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirLiquidacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirLiquidacion.setToolTipText("Imprimir Liquidación");
        btnImprimirLiquidacion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirLiquidacion.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirLiquidacion.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirLiquidacion.setOpaque(false);
        btnImprimirLiquidacion.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlImprimirLiquidacion.add(btnImprimirLiquidacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 25, 25));

        add(pnlImprimirLiquidacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 385, 45));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnImprimirEstadoRegional;
    private javax.swing.JButton btnImprimirEstadoRegionalInteres;
    private javax.swing.JButton btnImprimirEstadoRegionalProm;
    private javax.swing.JButton btnImprimirLiquidacion;
    private javax.swing.JCheckBox chkEstado;
    private javax.swing.JCheckBox chkFecha;
    private javax.swing.JCheckBox chkPlan;
    private javax.swing.JCheckBox chkPromotor;
    private javax.swing.JCheckBox chkRegional;
    private javax.swing.JComboBox cmboEstado;
    private javax.swing.JComboBox cmboPlan;
    private javax.swing.JComboBox cmboPromotor;
    private javax.swing.JComboBox cmboRegional;
    private javax.swing.ButtonGroup groTipoFecha;
    private javax.swing.JLabel lblDesde;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblHasta;
    private javax.swing.JLabel lblNumeroOperacion;
    private javax.swing.JLabel lblPlan;
    private javax.swing.JLabel lblPromotor;
    private javax.swing.JLabel lblRegional;
    private javax.swing.JRadioButton optInicio;
    private javax.swing.JRadioButton optVencimiento;
    private javax.swing.JPanel pnlEstado;
    private javax.swing.JPanel pnlImprimirLiquidacion;
    private panel.fecha txtFechaDesde;
    private panel.fecha txtFechaHasta;
    private javax.swing.JFormattedTextField txtfIdEstadoAhorro;
    public javax.swing.JFormattedTextField txtfNumeroOperacion;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnImprimirLiquidacion.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirLiquidacion();
            }
        });
        btnImprimirEstadoRegional.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirEstadoRegional();
            }
        });
        btnImprimirEstadoRegionalInteres.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirEstadoRegionalInteres();
            }
        });
        btnImprimirEstadoRegionalProm.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirEstadoRegionalPromotor();
            }
        });
        chkRegional.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbRegional.setId(0);
                cmbRegional.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkEstado.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmboEstado.setSelectedIndex(-1);
                cmboEstado.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkPlan.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbPlan.setId(0);
                cmbPlan.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkPromotor.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbPromotor.setId(0);
                cmbPromotor.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkFecha.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                if (((javax.swing.JCheckBox)evt.getSource()).isSelected()) {
                    txtFechaDesde.setFecha(new java.util.Date());
                    txtFechaHasta.setFecha(new java.util.Date());
                } else {
                    txtFechaDesde.setFechaVacia();
                    txtFechaHasta.setFechaVacia();
                }
            }
        });
    }

    private void setEnabled() {
        this.chkRegional.setSelected(true);
        this.chkEstado.setSelected(true);
        this.chkPlan.setSelected(true);
        this.chkPromotor.setSelected(true);
    }
    
    private void iniciarComponentes() {
        this.cmbEstado = new utilitario.comComboBox(this.cmboEstado, this.lblEstado, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwsubtipo)+" WHERE tipo LIKE '%ESTADO%AHORRO%PROGRAMADO%' ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbRegional = new utilitario.comComboBox(this.cmboRegional, this.lblRegional, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwregional1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbPlan = new utilitario.comComboBox(this.cmboPlan, this.lblPlan, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwahorroprogramadoplan1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbPromotor = new utilitario.comComboBox(this.cmboPromotor, this.lblPromotor, "SELECT id, apellidonombre AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwpromotor1)+" ORDER BY apellidonombre", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);

        this.txtfNumeroOperacion.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfNumeroOperacion.setToolTipText("Número de Operación");
        this.chkEstado.setToolTipText("Todos los estados de Ahorro Programdo");
        this.cmbEstado.cmb.setToolTipText("Estado de Ahorro Programado");
        this.chkRegional.setToolTipText("Todos las Regionales");
        this.cmbRegional.setToolTipText("Regionales");
        this.chkPlan.setToolTipText("Todos los Planes");
        this.cmbPlan.setToolTipText("Planes de Ahorro Programado");
        this.chkPromotor.setToolTipText("Todos los Promotor");
        this.cmbPromotor.setToolTipText("Promotores");
        this.txtFechaDesde.setToolTipText("Fecha desde");
        this.txtFechaHasta.setToolTipText("Fecha hasta");
    }

    public void imprimir(boolean esRetiro, especifico.entAhorroProgramado ent, especifico.modAhorroProgramadoDetalle det) { // imprime
        String stitulo = "Proyección";
        if (esRetiro) stitulo = "Retiro";
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repAhorroProgramadoLiquidacion, ent, det, null, null, stitulo+" de Ahorro Programado", mnt.intPrincipal.pnlEstado.sfiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }
    
    public void imprimirLiquidacion() { // imprime
        especifico.intAhorroProgramado intAhorro = new especifico.intAhorroProgramado(mnt);
        intAhorro.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwahorroprogramado1), " AND numerooperacion="+utilitario.utiNumero.convertirToInt(this.txtfNumeroOperacion.getText()), "");
        intAhorro.obtenerDato();
        if (intAhorro.mod.getRowCount()<=0) {
            javax.swing.JOptionPane.showMessageDialog(null, "Número de Operación "+this.txtfNumeroOperacion.getText()+" no existe");
            return;
        }
        especifico.intAhorroProgramadoDetalle intDetalle = new especifico.intAhorroProgramadoDetalle(mnt, new javax.swing.JTable());
        intDetalle.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwdetalleahorroprogramado), " AND idmovimiento="+intAhorro.mod.getEntidad(0).getId(), " ORDER BY numerocuota");
        intDetalle.obtenerDato();
        intDetalle.mod.cargarProyeccionPago(intAhorro.mod.getEntidad(0).getImporte(), 1, intAhorro.mod.getEntidad(0).getPlazo());
        intDetalle.mod.cargarSaldo();
        intDetalle.mod.calcularInteres(intAhorro.mod.getEntidad(0).getTasaInteresRetirado(), intAhorro.mod.getEntidad(0).getFechaRetirado());
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repAhorroProgramadoLiquidacion, intAhorro.mod.getEntidad(0), intDetalle.mod, null, null, "Liquidación de Ahorro Programado", mnt.intPrincipal.pnlEstado.sfiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }
    
//05/01/2003 30/12/2013 Inicio       valido
//05/01/2003 30/12/2013 Vencimiento  valido
//                      Inicio       sin efecto
//                      Vencimiento  sin efecto
//05/01/2003            Inicio       los que inician esa fecha
//05/01/2003            Vencimiento  los que vencen esa fecha
//           30/12/2013 Inicio       los que inician esa fecha
//           30/12/2013 Vencimiento  los que vencen esa fecha
    
    private void imprimirEstadoRegional() {
        String sfiltro = "";
        String stextoFiltro = "";
        if (this.cmbEstado.getIdSeleccionado()>0) {
            sfiltro += " AND idestado="+this.cmbEstado.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbEstado.getDescripcionSeleccionado();
        }
        if (this.cmbRegional.getIdSeleccionado()>0) {
            sfiltro += " AND idregional="+this.cmbRegional.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbRegional.getDescripcionSeleccionado();
        }
        if (this.cmbPlan.getIdSeleccionado()>0) {
            sfiltro += " AND idplan="+this.cmbPlan.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbPlan.getDescripcionSeleccionado();
        }
        if (this.txtFechaDesde.getFecha()==null && this.txtFechaHasta.getFecha()!=null && this.optInicio.isSelected()) {
            sfiltro += " AND fechaprimervencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "INICIO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()==null && this.txtFechaHasta.getFecha()!=null && this.optVencimiento.isSelected()) {
            sfiltro += " AND fechavencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "VENCIMIENTO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()==null && this.optInicio.isSelected()) {
            sfiltro += " AND fechaprimervencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "INICIO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()==null && this.optVencimiento.isSelected()) {
            sfiltro += " AND fechavencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "VENCIMIENTO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()!=null && this.optInicio.isSelected()) {
            sfiltro += " AND fechaprimervencimiento>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND fechaprimervencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "INICIO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" a "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()!=null && this.optVencimiento.isSelected()) {
            sfiltro += " AND fechavencimiento>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "VENCIMIENTO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" a "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(generico.vista.getNombre(generico.vista.vwahorroprogramado2), sfiltro, " ORDER BY estado, regional, plan, fechaoperacion"));
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repAhorroProgramadoRegionEstado, mnt.intPrincipal.pnlEstado.conexion.rs, null, null, null, "Informe Ahorro Programado", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }
    
    private void imprimirEstadoRegionalInteres() {
        String sfiltro = "";
        String stextoFiltro = "";
        if (this.cmbEstado.getIdSeleccionado()>0) {
            sfiltro += " AND idestado="+this.cmbEstado.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbEstado.getDescripcionSeleccionado();
        }
        if (this.cmbRegional.getIdSeleccionado()>0) {
            sfiltro += " AND idregional="+this.cmbRegional.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbRegional.getDescripcionSeleccionado();
        }
        if (this.cmbPlan.getIdSeleccionado()>0) {
            sfiltro += " AND idplan="+this.cmbPlan.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbPlan.getDescripcionSeleccionado();
        }
        if (this.txtFechaDesde.getFecha()==null && this.txtFechaHasta.getFecha()!=null && this.optInicio.isSelected()) {
            sfiltro += " AND fechaprimervencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "INICIO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()==null && this.txtFechaHasta.getFecha()!=null && this.optVencimiento.isSelected()) {
            sfiltro += " AND fechavencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "VENCIMIENTO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()==null && this.optInicio.isSelected()) {
            sfiltro += " AND fechaprimervencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "INICIO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()==null && this.optVencimiento.isSelected()) {
            sfiltro += " AND fechavencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "VENCIMIENTO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()!=null && this.optInicio.isSelected()) {
            sfiltro += " AND fechaprimervencimiento>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND fechaprimervencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "INICIO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" a "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()!=null && this.optVencimiento.isSelected()) {
            sfiltro += " AND fechavencimiento>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "VENCIMIENTO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" a "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(generico.vista.getNombre(generico.vista.vwahorroprogramado2), sfiltro, " ORDER BY estado, regional, plan, fechaoperacion"));
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repAhorroProgramadoRegionEstado, mnt.intPrincipal.pnlEstado.conexion.rs, null, null, null, "Informe Ahorro Programado", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }
    
    private void imprimirEstadoRegionalPromotor() {
        String sfiltro = "";
        String stextoFiltro = "";
        if (this.cmbEstado.getIdSeleccionado()>0) {
            sfiltro += " AND idestado="+this.cmbEstado.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbEstado.getDescripcionSeleccionado();
        }
        if (this.cmbRegional.getIdSeleccionado()>0) {
            sfiltro += " AND idregional="+this.cmbRegional.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbRegional.getDescripcionSeleccionado();
        }
        if (this.cmbPlan.getIdSeleccionado()>0) {
            sfiltro += " AND idplan="+this.cmbPlan.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbPlan.getDescripcionSeleccionado();
        }
        if (this.cmbPromotor.getIdSeleccionado()>0) {
            sfiltro += " AND idpromotor="+this.cmbPromotor.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbPromotor.getDescripcionSeleccionado();
        }
        if (this.txtFechaDesde.getFecha()==null && this.txtFechaHasta.getFecha()!=null && this.optInicio.isSelected()) {
            sfiltro += " AND fechaprimervencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "INICIO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()==null && this.txtFechaHasta.getFecha()!=null && this.optVencimiento.isSelected()) {
            sfiltro += " AND fechavencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "VENCIMIENTO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()==null && this.optInicio.isSelected()) {
            sfiltro += " AND fechaprimervencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "INICIO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()==null && this.optVencimiento.isSelected()) {
            sfiltro += " AND fechavencimiento="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "VENCIMIENTO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()!=null && this.optInicio.isSelected()) {
            sfiltro += " AND fechaprimervencimiento>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND fechaprimervencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "INICIO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" a "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()!=null && this.optVencimiento.isSelected()) {
            sfiltro += " AND fechavencimiento>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND fechavencimiento<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "VENCIMIENTO: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" a "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        System.out.println(mnt.intPrincipal.pnlEstado.conexion.getConsulta(generico.vista.getNombre(generico.vista.vwahorroprogramado1), sfiltro, " ORDER BY estado, regional, plan, convertir_especial(cedula)"));
        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(generico.vista.getNombre(generico.vista.vwahorroprogramado1), sfiltro, " ORDER BY estado, regional, plan, convertir_especial(cedula)"));
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repAhorroProgramadoRegionEstadoProm, mnt.intPrincipal.pnlEstado.conexion.rs, null, null, null, "Informe Ahorro Programado", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }
    
}
