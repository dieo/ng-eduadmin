/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intRegional.java
 *
 * Created on 04-abr-2012, 11:53:39
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intRegional extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modRegional mod = new especifico.modRegional();
    private utilitario.comComboBox cmbZona;
    private String svista="", sfiltro="", sorden="";

    /** Creates new form intRegional 
     @param mnt.
     @param tbl.
     */
    public intRegional(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        txtfDescripcion = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblDescripcion = new javax.swing.JLabel();
        lblZona = new javax.swing.JLabel();
        cmboZona = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(460, 95));
        setPreferredSize(new java.awt.Dimension(460, 95));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setToolTipText("");
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, 110, 25));

        txtfDescripcion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 35, 380, 25));

        lblId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 60, 25));

        lblDescripcion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblDescripcion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDescripcion.setText("Descripción");
        add(lblDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 60, 25));

        lblZona.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblZona.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblZona.setText("Zona");
        add(lblZona, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 60, 25));

        cmboZona.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboZona, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 60, 380, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmboZona;
    private javax.swing.JLabel lblDescripcion;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblZona;
    private javax.swing.JFormattedTextField txtfDescripcion;
    private javax.swing.JFormattedTextField txtfId;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfDescripcion);
        uti.agregarEventoKeyReleased(this.txtfDescripcion);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.txtfDescripcion.setEnabled(bmodo);
        this.cmbZona.setEnabled(bmodo);
    }
    
    private void iniciarComponentes() {
        this.cmbZona = new utilitario.comComboBox(this.cmboZona, this.lblZona, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwsubtipo)+" WHERE tipo LIKE '%ZONA%' ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);

        this.txtfDescripcion.setDocument(new generico.modFormatoCadena(this.txtfDescripcion, especifico.entRegional.LONGITUD_DESCRIPCION, true));
        this.txtfId.setToolTipText(especifico.entRegional.TEXTO_ID);
        this.txtfDescripcion.setToolTipText(especifico.entRegional.TEXTO_DESCRIPCION);
        this.cmbZona.setToolTipText(especifico.entRegional.TEXTO_ZONA);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) { 
            this.setInterfaz(new especifico.entRegional());
        }
    }

    public boolean insertar() {
        especifico.entRegional ent = new especifico.entRegional();
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        especifico.entRegional ent = mod.getEntidad().copiar(new especifico.entRegional()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValido()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("regional"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entRegional ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setDescripcion(this.txtfDescripcion.getText());
        ent.setIdZona(this.cmbZona.getIdSeleccionado());
        ent.setZona(this.cmbZona.getDescripcionSeleccionado());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entRegional ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.txtfDescripcion.setText(ent.getDescripcion());}
        catch (Exception e) {this.txtfDescripcion.setText("");}
        try                 {this.cmbZona.setId(ent.getIdZona());}
        catch (Exception e) {this.cmbZona.setId(0);}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }
    
    public void imprimir() {
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repRegional, this.mod, null, null, null, "Listado de Regional", mnt.intPrincipal.pnlEstado.sfiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }

}
