/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intRubro.java
 *
 * Created on 04-abr-2012, 18:23:29
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intListaDetalleIngreso extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    private java.util.Date dfechaOperacion;
    private especifico.intDetalleIngreso intDetalleIngreso;
    private String svista = "", sfiltro = "", sorden = "";

    /**
     * Creates new form intListaDetalleCancelacion
     *
     * @param mnt.
     * @param tbl.
     * @param dfechaOperacion.
     * @param intDetalleIngreso.
     */
    public intListaDetalleIngreso(generico.intMantenimiento mnt, javax.swing.JTable tbl, java.util.Date dfechaOperacion, especifico.intDetalleIngreso intDetalleIngreso) {
        this.mnt = mnt;
        this.dfechaOperacion = (java.util.Date) dfechaOperacion.clone();
        this.intDetalleIngreso = intDetalleIngreso;
        if (tbl != null) {
            this.intDetalleIngreso.modCabecera.detalleVer.setTable(tbl);
        }
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scpDetalle = new javax.swing.JScrollPane();
        tblDetalle = new javax.swing.JTable();
        lblTotales = new javax.swing.JLabel();
        txtfMontoSaldo = new javax.swing.JFormattedTextField();
        txtfMontoInteres = new javax.swing.JFormattedTextField();
        txtfMontoCapital = new javax.swing.JFormattedTextField();
        txtfMontoExoneracion = new javax.swing.JFormattedTextField();
        txtfCobro = new javax.swing.JFormattedTextField();
        btnRellenar = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(655, 400));
        setPreferredSize(new java.awt.Dimension(655, 400));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        scpDetalle.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scpDetalle.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tblDetalle.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        scpDetalle.setViewportView(tblDetalle);

        add(scpDetalle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 645, 370));

        lblTotales.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblTotales.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotales.setText("Totales");
        add(lblTotales, new org.netbeans.lib.awtextra.AbsoluteConstraints(75, 370, 40, 25));

        txtfMontoSaldo.setForeground(new java.awt.Color(0, 102, 204));
        txtfMontoSaldo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMontoSaldo.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfMontoSaldo.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfMontoSaldo.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        add(txtfMontoSaldo, new org.netbeans.lib.awtextra.AbsoluteConstraints(315, 370, 95, 25));

        txtfMontoInteres.setForeground(new java.awt.Color(0, 102, 204));
        txtfMontoInteres.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMontoInteres.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfMontoInteres.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfMontoInteres.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        add(txtfMontoInteres, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 370, 95, 25));

        txtfMontoCapital.setForeground(new java.awt.Color(0, 102, 204));
        txtfMontoCapital.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMontoCapital.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfMontoCapital.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfMontoCapital.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        add(txtfMontoCapital, new org.netbeans.lib.awtextra.AbsoluteConstraints(125, 370, 95, 25));

        txtfMontoExoneracion.setForeground(new java.awt.Color(0, 102, 204));
        txtfMontoExoneracion.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMontoExoneracion.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfMontoExoneracion.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfMontoExoneracion.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        add(txtfMontoExoneracion, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 370, 95, 25));

        txtfCobro.setForeground(new java.awt.Color(0, 102, 204));
        txtfCobro.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfCobro.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfCobro.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfCobro.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        add(txtfCobro, new org.netbeans.lib.awtextra.AbsoluteConstraints(505, 370, 95, 25));

        btnRellenar.setBorder(null);
        btnRellenar.setEnabled(false);
        btnRellenar.setOpaque(false);
        add(btnRellenar, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 370, 45, 25));
    }// </editor-fold>//GEN-END:initComponents

    private void tblDetalleMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) { // doble click
            this.marcar();
        }
    }

    private void tblDetalleKeyReleased(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == 32) { // barra espaciadora
            this.marcar();
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRellenar;
    private javax.swing.JLabel lblTotales;
    private javax.swing.JScrollPane scpDetalle;
    public javax.swing.JTable tblDetalle;
    private javax.swing.JFormattedTextField txtfCobro;
    public javax.swing.JFormattedTextField txtfMontoCapital;
    private javax.swing.JFormattedTextField txtfMontoExoneracion;
    public javax.swing.JFormattedTextField txtfMontoInteres;
    private javax.swing.JFormattedTextField txtfMontoSaldo;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        tblDetalle.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetalleMouseClicked(evt);
            }
        });
        tblDetalle.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tblDetalleKeyReleased(evt);
            }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoKeyPressed(this.tblDetalle);
    }

    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfMontoCapital.setEnabled(false);
        this.txtfMontoInteres.setEnabled(false);
        this.txtfMontoSaldo.setEnabled(false);
        this.txtfMontoExoneracion.setEnabled(false);
        this.txtfCobro.setEnabled(false);
    }

    private void iniciarComponentes() {
        this.intDetalleIngreso.modCabecera.detalleVer.setTable(this.tblDetalle);
        this.txtfMontoCapital.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfMontoInteres.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfMontoSaldo.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfMontoExoneracion.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfCobro.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
    }

    public void cargarEditor() {
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        return true;
    }

    public boolean establecer() {
        return true;
    }

    public boolean eliminar() {
        return true;
    }

    public boolean cancelar() {
        return true;
    }

    public boolean guardar() {
        return true;
    }

    public boolean obtenerDato() {
        return true;
    }

    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entDetalleIngresoCabecera ent) {
    }

    public void setInterfaz(especifico.entDetalleIngreso ent) {
        try {
            this.txtfMontoSaldo.setValue(ent.getMontoSaldo());
        } catch (Exception e) {
            this.txtfMontoSaldo.setValue(0);
        }
        try {
            this.txtfMontoExoneracion.setValue(ent.getMontoExoneracion());
        } catch (Exception e) {
            this.txtfMontoExoneracion.setValue(0);
        }
        try {
            this.txtfCobro.setValue(ent.getMontoCobro());
        } catch (Exception e) {
            this.txtfCobro.setValue(0);
        }
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty()) {
            this.svista = svista;
        }
        if (!sfiltro.isEmpty()) {
            this.sfiltro = sfiltro;
        }
        if (!sorden.isEmpty()) {
            this.sorden = sorden;
        }
    }

    public void imprimir() {
    }

    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================
    
    private void marcar() {
        if (this.tblDetalle.getSelectedRow() >= 0) {
            java.util.Date dfechaSaldo = utilitario.utiFecha.getUltimoDia(this.dfechaOperacion);
            if (intDetalleIngreso.modCabecera.detalleVer.getEntidad().getEstado().equals(especifico.entDetalleIngreso.estado_atraso) || intDetalleIngreso.modCabecera.detalleVer.getEntidad().getEstado().equals(especifico.entDetalleIngreso.estado_saldo) || intDetalleIngreso.modCabecera.detalleVer.getEntidad(this.tblDetalle.getSelectedRow()).getEstado().equals(especifico.entDetalleIngreso.estado_cobro)) {
                intDetalleIngreso.modCabecera.detalleVer.getEntidad().setEstado("");
                intDetalleIngreso.modCabecera.detalleVer.getEntidad().setMontoCobro(0);
            } else {
                if (intDetalleIngreso.modCabecera.detalleVer.getEntidad().getEstado().equals("")) {
                    if (utilitario.utiFecha.getAM(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getFechaVencimiento()) < utilitario.utiFecha.getAM(dfechaSaldo)) {
                        intDetalleIngreso.modCabecera.detalleVer.getEntidad().setEstado(especifico.entDetalleIngreso.estado_atraso);
                    }
                    if (utilitario.utiFecha.getAM(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getFechaVencimiento()) > utilitario.utiFecha.getAM(dfechaSaldo)) {
                        intDetalleIngreso.modCabecera.detalleVer.getEntidad().setEstado(especifico.entDetalleIngreso.estado_saldo);
                    }
                    if (utilitario.utiFecha.getAM(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getFechaVencimiento()) == utilitario.utiFecha.getAM(dfechaSaldo)) {
                        intDetalleIngreso.modCabecera.detalleVer.getEntidad().setEstado(especifico.entDetalleIngreso.estado_cobro);
                    }
                    intDetalleIngreso.modCabecera.detalleVer.getEntidad().setMontoCobro(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getMontoSaldo());
                }
            }
            this.setInterfaz(intDetalleIngreso.modCabecera.detalleVer.calcularTotal());
            this.tblDetalle.updateUI();
        }
    }

    public void establecerMarcado() {
        intDetalleIngreso.modCabecera.descargar();
        int indiceCabecera = -1;
        int iidPrestamoMutual = ((java.math.BigDecimal) mnt.intPrincipal.pnlEstado.conexion.getCampo("cuenta", "descripcion LIKE 'PRESTAMO%MUTUAL'", "id")).intValue();
        int iidInteresPrestamo = ((java.math.BigDecimal) mnt.intPrincipal.pnlEstado.conexion.getCampo("cuenta", "descripcion LIKE 'INTERES%PRESTAMO'", "id")).intValue();
        if (intDetalleIngreso.modCabecera.getEntidad().getIdCuenta() == iidPrestamoMutual || intDetalleIngreso.modCabecera.getEntidad().getIdCuenta() == iidInteresPrestamo) {
            if (intDetalleIngreso.modCabecera.getEntidad().getIdCuenta() == iidPrestamoMutual) indiceCabecera = intDetalleIngreso.obtenerIndiceInteres(intDetalleIngreso.modCabecera.getTable().getSelectedRow());
            if (intDetalleIngreso.modCabecera.getEntidad().getIdCuenta() == iidInteresPrestamo) indiceCabecera = intDetalleIngreso.obtenerIndiceCapital(intDetalleIngreso.modCabecera.getTable().getSelectedRow());
            for (int i=intDetalleIngreso.modCabecera.getEntidad().getDesde(); i<=intDetalleIngreso.modCabecera.getEntidad().getHasta(); i++) {
                int indice = intDetalleIngreso.modCabecera.getEntidad(indiceCabecera).getDesde() + (i - intDetalleIngreso.modCabecera.getEntidad().getDesde());
                if (intDetalleIngreso.modCabecera.detalle.getEntidad(i).getEstado().equals(especifico.entDetalleIngreso.estado_saldo) || intDetalleIngreso.modCabecera.detalle.getEntidad(i).getEstado().equals(especifico.entDetalleIngreso.estado_cobro) || intDetalleIngreso.modCabecera.detalle.getEntidad(i).getEstado().equals(especifico.entDetalleIngreso.estado_atraso)) {
                    intDetalleIngreso.modCabecera.detalle.getEntidad(indice).setEstado(intDetalleIngreso.modCabecera.detalle.getEntidad(i).getEstado());
                    intDetalleIngreso.modCabecera.detalle.getEntidad(indice).setMontoCobro(intDetalleIngreso.modCabecera.detalle.getEntidad(indice).getMontoSaldo());
                } else {
                    intDetalleIngreso.modCabecera.detalle.getEntidad(indice).setEstado("");
                    intDetalleIngreso.modCabecera.detalle.getEntidad(indice).setMontoCobro(0);
                }
            }
        }
        boolean cancela = intDetalleIngreso.modCabecera.esValidoRellenar(intDetalleIngreso.modCabecera.getTable().getSelectedRow());
        intDetalleIngreso.modCabecera.rellenarExoneracion(cancela, this.dfechaOperacion, intDetalleIngreso.obtenerIndiceInteres(intDetalleIngreso.modCabecera.getTable().getSelectedRow())); // rellena exoneracion
        intDetalleIngreso.modCabecera.calcularSubtotal();
        intDetalleIngreso.modCabecera.getTable().updateUI();
    }

    /*private void marcar2() {
        if (this.tblDetalle.getSelectedRow() >= 0) {
            java.util.Date dfechaSaldo = utilitario.utiGeneral.getUltimoDia(this.dfechaOperacion);
            int indiceCuota = -1;
            int indiceCabecera = -1;
            int iidPrestamoMutual = ((java.math.BigDecimal) mnt.intPrincipal.pnlEstado.conexion.getCampo("cuenta", "descripcion LIKE 'PRESTAMO%MUTUAL'", "id")).intValue();
            int iidInteresPrestamo = ((java.math.BigDecimal) mnt.intPrincipal.pnlEstado.conexion.getCampo("cuenta", "descripcion LIKE 'INTERES%PRESTAMO'", "id")).intValue();
            if (intDetalleIngreso.modCabecera.getEntidad().getIdCuenta() == iidPrestamoMutual || intDetalleIngreso.modCabecera.getEntidad().getIdCuenta() == iidInteresPrestamo) {
                if (intDetalleIngreso.modCabecera.getEntidad().getIdCuenta() == iidPrestamoMutual) {
                    indiceCuota = intDetalleIngreso.obtenerIndiceCuotaInteres(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getCuota());
                    indiceCabecera = intDetalleIngreso.obtenerIndiceInteres();
                }
                if (intDetalleIngreso.modCabecera.getEntidad().getIdCuenta() == iidInteresPrestamo) {
                    indiceCuota = intDetalleIngreso.obtenerIndiceCuotaCapital(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getCuota());
                    indiceCabecera = intDetalleIngreso.modCabecera.getTable().getSelectedRow();
                }
            }
            if (intDetalleIngreso.modCabecera.detalleVer.getEntidad().getEstado().equals(especifico.entDetalleIngreso.estado_saldo) || intDetalleIngreso.modCabecera.detalleVer.getEntidad().getEstado().equals(especifico.entDetalleIngreso.estado_cobro)) {
                intDetalleIngreso.modCabecera.detalleVer.getEntidad().setEstado("");
                intDetalleIngreso.modCabecera.detalleVer.getEntidad().setMontoCobro(0);
                intDetalleIngreso.modCabecera.detalle.getEntidad(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getId()).setEstado("");
                intDetalleIngreso.modCabecera.detalle.getEntidad(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getId()).setMontoCobro(0);
                if (indiceCuota > 0) {
                    intDetalleIngreso.modCabecera.detalle.getEntidad(indiceCuota).setEstado("");
                }
                if (indiceCuota > 0) {
                    intDetalleIngreso.modCabecera.detalle.getEntidad(indiceCuota).setMontoCobro(0);
                }
            } else {
                if (intDetalleIngreso.modCabecera.detalleVer.getEntidad().getEstado().equals("")) {
                    if (utilitario.utiGeneral.getAMD(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getFechaVencimiento()) > utilitario.utiGeneral.getAMD(dfechaSaldo)) {
                        intDetalleIngreso.modCabecera.detalleVer.getEntidad().setEstado(especifico.entDetalleIngreso.estado_saldo);
                        intDetalleIngreso.modCabecera.detalle.getEntidad(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getId()).setEstado(especifico.entDetalleIngreso.estado_saldo);
                        if (indiceCuota > 0) {
                            intDetalleIngreso.modCabecera.detalle.getEntidad(indiceCuota).setEstado(especifico.entDetalleIngreso.estado_saldo);
                        }
                    }
                    if (utilitario.utiGeneral.getAMD(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getFechaVencimiento()) == utilitario.utiGeneral.getAMD(dfechaSaldo)) {
                        intDetalleIngreso.modCabecera.detalleVer.getEntidad().setEstado(especifico.entDetalleIngreso.estado_cobro);
                        intDetalleIngreso.modCabecera.detalle.getEntidad(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getId()).setEstado(especifico.entDetalleIngreso.estado_cobro);
                        if (indiceCuota > 0) {
                            intDetalleIngreso.modCabecera.detalle.getEntidad(indiceCuota).setEstado(especifico.entDetalleIngreso.estado_cobro);
                        }
                    }
                    intDetalleIngreso.modCabecera.detalleVer.getEntidad().setMontoCobro(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getMontoSaldo());
                    intDetalleIngreso.modCabecera.detalle.getEntidad(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getId()).setMontoCobro(intDetalleIngreso.modCabecera.detalle.getEntidad(intDetalleIngreso.modCabecera.detalleVer.getEntidad().getId()).getMontoSaldo());
                    if (indiceCuota > 0) {
                        intDetalleIngreso.modCabecera.detalle.getEntidad(indiceCuota).setMontoCobro(intDetalleIngreso.modCabecera.detalle.getEntidad(indiceCuota).getMontoSaldo());
                    }
                }
            }
            boolean cancela = intDetalleIngreso.modCabecera.esValidoRellenar();
            intDetalleIngreso.modCabecera.rellenarExoneracion(cancela, dfechaOperacion, indiceCabecera); // rellena exoneracion
            for (int i = intDetalleIngreso.modCabecera.getEntidad().idesde; i <= intDetalleIngreso.modCabecera.getEntidad().ihasta; i++) {
                intDetalleIngreso.modCabecera.detalleVer.getEntidad(i - intDetalleIngreso.modCabecera.getEntidad().idesde).setMontoExoneracion(intDetalleIngreso.modCabecera.detalle.getEntidad(i).getMontoExoneracion());
            }
            intDetalleIngreso.modCabecera.calcularSubtotal();
            this.setInterfaz(intDetalleIngreso.modCabecera.detalleVer.calcularTotal());
            this.tblDetalle.updateUI();
        }
    }*/

}
