/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intOrdenCreditoSolicitar.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intOperacionFijaAnular extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modOperacionFija mod = new especifico.modOperacionFija();
    private String svista="", sfiltro="", sorden="", sfiltroFormulario="";

    /** Creates new form intOrdenCreditoSolicitar
     @param mnt.
     @param tbl.
     */
    public intOperacionFijaAnular(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblObservacion = new javax.swing.JLabel();
        txtfObservacion = new javax.swing.JFormattedTextField();
        lblFechaAnulado = new javax.swing.JLabel();
        try {
            txtFechaAnulado = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        btnOperacion = new javax.swing.JButton();
        lblEstadoOperacion = new javax.swing.JLabel();
        txtfEstadoOperacion = new javax.swing.JFormattedTextField();
        lblOperacion = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(605, 90));
        setPreferredSize(new java.awt.Dimension(605, 90));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblObservacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblObservacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObservacion.setText("Observación");
        add(lblObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 90, 25));

        txtfObservacion.setForeground(new java.awt.Color(0, 102, 204));
        txtfObservacion.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfObservacion.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfObservacion.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        add(txtfObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 500, 25));

        lblFechaAnulado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaAnulado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaAnulado.setText("Fecha Anulado");
        add(lblFechaAnulado, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 90, 25));
        add(txtFechaAnulado, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, -1, -1));

        btnOperacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/visualizar.png"))); // NOI18N
        btnOperacion.setToolTipText("Ver Aprobación");
        btnOperacion.setFocusable(false);
        btnOperacion.setOpaque(false);
        add(btnOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 25, 25));

        lblEstadoOperacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblEstadoOperacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstadoOperacion.setText("Estado Solicitud");
        add(lblEstadoOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 80, 25));

        txtfEstadoOperacion.setBorder(null);
        txtfEstadoOperacion.setFocusable(false);
        txtfEstadoOperacion.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        txtfEstadoOperacion.setOpaque(false);
        add(txtfEstadoOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, 300, 25));

        lblOperacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblOperacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblOperacion.setText("Operación");
        add(lblOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 90, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnOperacion;
    private javax.swing.JLabel lblEstadoOperacion;
    private javax.swing.JLabel lblFechaAnulado;
    private javax.swing.JLabel lblObservacion;
    private javax.swing.JLabel lblOperacion;
    private panel.fecha txtFechaAnulado;
    public javax.swing.JFormattedTextField txtfEstadoOperacion;
    private javax.swing.JFormattedTextField txtfObservacion;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnOperacion.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configurarPanel();
            }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfObservacion);
        uti.agregarEventoKeyReleased(this.txtfObservacion);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtFechaAnulado.setEnabled(false);
        this.txtfObservacion.setEnabled(bmodo);
    }
    
    public final boolean isEnabled() {
        return this.txtfObservacion.isEnabled();
    }
    
    private void iniciarComponentes() {
        sfiltroFormulario = " AND idestado="+mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%OPERACION%FIJA%", "ACTIVO");

        this.txtfObservacion.setDocument(new generico.modFormatoCadena(this.txtfObservacion, especifico.entOperacionFija.LONGITUD_OBSERVACION, true));
        this.txtFechaAnulado.setToolTipText(especifico.entOperacionFija.TEXTO_FECHA_ANULADO);
        this.txtfObservacion.setToolTipText(especifico.entOperacionFija.TEXTO_OBSERVACION_ANULADO);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entOperacionFija());
        }
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        // activo
        if (mod.getEntidad().getIdEstadoOperacionFija() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%OPERACION%FIJA%", "ACTIVO")) {
            especifico.entOperacionFija ent = new especifico.entOperacionFija();
            ent.setIdEstadoOperacionFija(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%OPERACION%FIJA%", "ANULADO"));
            ent.setEstadoOperacionFija(mnt.intPrincipal.pnlEstado.conexion.obtenerDescripcionSubtipo(ent.getIdEstadoOperacionFija()));
            ent.setFechaAnulado(new java.util.Date());
            ent.setObservacionAnulado(mod.getEntidad().getObservacionAnulado());
            this.setInterfaz(ent);
        } else {
            return false;
        }
        return true;
    }

    public boolean establecer() {
        especifico.entOperacionFija ent = mod.getEntidad().copiar(new especifico.entOperacionFija()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValidoAnular()) { // valida la anulación
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        ent.setIdEstadoOperacionFija(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%OPERACION%FIJA%", "ANULADO"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro+sfiltroFormulario, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }
    
    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entOperacionFija ent) {
        ent.setFechaAnulado(this.txtFechaAnulado.getFecha());
        ent.setObservacionAnulado(this.txtfObservacion.getText());
        ent.setIdEstadoOperacionFija(ent.getIdEstadoOperacionFija());
        ent.setEstadoOperacionFija(ent.getEstadoOperacionFija());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }

    private void setInterfaz(especifico.entOperacionFija ent) {
        try                 {this.txtFechaAnulado.setFecha(ent.getFechaAnulado());}
        catch (Exception e) {this.txtFechaAnulado.setFechaVacia();}
        try                 {this.txtfObservacion.setText(ent.getObservacionAnulado());}
        catch (Exception e) {this.txtfObservacion.setText("");}
        try                 {this.txtfEstadoOperacion.setText(ent.getEstadoOperacionFija());}
        catch (Exception e) {this.txtfEstadoOperacion.setText("");}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================

    private void configurarPanel() {
        especifico.intOperacionFija intOperacionFija = new especifico.intOperacionFija(mnt, null);
        generico.intPanel panel = new generico.intPanel("Registro Operación Fija :: Mantenimiento :: " + generico.mnuMantenimientoDetalle.visualizar, intOperacionFija);
        intOperacionFija.setEnabled(false);
        intOperacionFija.setEnabled(false);
        especifico.entOperacionFija ent = mod.getEntidad().copiar(new especifico.entOperacionFija()); // obtiene la entidad del registro seleccionado
        intOperacionFija.mod.insertar(ent);
        intOperacionFija.mod.insertar(ent);
        intOperacionFija.mod.getTable().setRowSelectionInterval(0,0);
        intOperacionFija.cargarEditor();
        intOperacionFija.cargarEditor();
        if (intOperacionFija.mod.getTable().getSelectedRow()>=0) panel.abrir();
    }

    public boolean anularOperacion(int iidOperacion, java.util.Date dfecha, String sobservacion) {
        this.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwoperacionfija1), " AND id="+iidOperacion, "");
        this.obtenerDato();
        String snumero = "";
        for (int i=0; i<this.mod.getRowCount(); i++) {
            this.mod.getEntidad(i).setIdEstadoOperacionFija(mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%OPERACION%FIJA%", "ANULADO"));
            this.mod.getEntidad(i).setFechaAnulado((java.util.Date)dfecha.clone());
            this.mod.getEntidad(i).setObservacionAnulado(sobservacion);
            this.mod.getEntidad(i).setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
            snumero = snumero + this.mod.getEntidad(i).getNumeroOperacion() + ", ";
            if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(this.mod.getEntidad(i).getSentencia())) { // obtiene datos de la base de datos
                javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
                return false;
            }
        }
        if (snumero.length()>2) {
            javax.swing.JOptionPane.showMessageDialog(null, "Operación Fija anulada: "+snumero.substring(0,snumero.length()-2));
            return true;
        }
        return false;
    }
    
}
