/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intEntidad.java
 *
 * Created on 06-abr-2012, 9:29:19
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intTalonarioOtorgarPermiso extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modTalonario mod = new especifico.modTalonario();
    private especifico.intTalonario intTalonario;
    private utilitario.comRadioButton radEstadoTalonario;
    private utilitario.comRadioButton radTipoPermiso;
    private String svista="", sfiltro="", sorden="", sfiltroFormulario="";
    
    /** Creates new form intEntidad 
     @param mnt.
     @param tbl.
     */
    public intTalonarioOtorgarPermiso(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groEstado = new javax.swing.ButtonGroup();
        groTipoPermiso = new javax.swing.ButtonGroup();
        lblEstado = new javax.swing.JLabel();
        optActivo = new javax.swing.JRadioButton();
        optAnulado = new javax.swing.JRadioButton();
        optInactivo = new javax.swing.JRadioButton();
        optTerminado = new javax.swing.JRadioButton();
        optVencido = new javax.swing.JRadioButton();
        optPerfil = new javax.swing.JRadioButton();
        optUsuario = new javax.swing.JRadioButton();
        scpPerfil = new javax.swing.JScrollPane();
        lstPerfil = new javax.swing.JList();
        scpPerfilPermitido = new javax.swing.JScrollPane();
        lstPerfilPermitido = new javax.swing.JList();
        scpUsuario = new javax.swing.JScrollPane();
        lstUsuario = new javax.swing.JList();
        scpUsuarioPermitido = new javax.swing.JScrollPane();
        lstUsuarioPermitido = new javax.swing.JList();
        lblPerfil = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        btnEliminarItemPerfil = new javax.swing.JButton();
        btnAgregarItemPerfil = new javax.swing.JButton();
        btnEliminarItemUsuario = new javax.swing.JButton();
        btnAgregarItemUsuario = new javax.swing.JButton();
        lblFechaActivo = new javax.swing.JLabel();
        try {
            txtFechaActivo = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        tblTalonario = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(555, 265));
        setPreferredSize(new java.awt.Dimension(555, 265));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblEstado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstado.setText("Estado");
        add(lblEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 80, 25));

        groEstado.add(optActivo);
        optActivo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        optActivo.setText("ACTIVO");
        optActivo.setOpaque(false);
        add(optActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 35, 80, 25));

        groEstado.add(optAnulado);
        optAnulado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        optAnulado.setText("ANULADO");
        optAnulado.setOpaque(false);
        add(optAnulado, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 35, 80, 25));

        groEstado.add(optInactivo);
        optInactivo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        optInactivo.setText("INACTIVO");
        optInactivo.setOpaque(false);
        add(optInactivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 35, 80, 25));

        groEstado.add(optTerminado);
        optTerminado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        optTerminado.setText("TERMINADO");
        optTerminado.setOpaque(false);
        add(optTerminado, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 35, 90, 25));

        groEstado.add(optVencido);
        optVencido.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        optVencido.setText("VENCIDO");
        optVencido.setOpaque(false);
        add(optVencido, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 35, 80, 25));

        groTipoPermiso.add(optPerfil);
        optPerfil.setFont(new java.awt.Font("Arial Narrow", 0, 10)); // NOI18N
        optPerfil.setText("PERFIL");
        optPerfil.setOpaque(false);
        add(optPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 20, 25));

        groTipoPermiso.add(optUsuario);
        optUsuario.setFont(new java.awt.Font("Arial Narrow", 0, 10)); // NOI18N
        optUsuario.setText("USUARIO");
        optUsuario.setOpaque(false);
        add(optUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 160, 20, 25));

        lstPerfil.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lstPerfil.setForeground(new java.awt.Color(0, 102, 204));
        lstPerfil.setToolTipText("");
        scpPerfil.setViewportView(lstPerfil);

        add(scpPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, 210, 100));

        lstPerfilPermitido.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lstPerfilPermitido.setForeground(new java.awt.Color(0, 102, 204));
        scpPerfilPermitido.setViewportView(lstPerfilPermitido);

        add(scpPerfilPermitido, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 60, 210, 100));

        lstUsuario.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lstUsuario.setForeground(new java.awt.Color(0, 102, 204));
        scpUsuario.setViewportView(lstUsuario);

        add(scpUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 160, 210, 100));

        lstUsuarioPermitido.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lstUsuarioPermitido.setForeground(new java.awt.Color(0, 102, 204));
        scpUsuarioPermitido.setViewportView(lstUsuarioPermitido);

        add(scpUsuarioPermitido, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 160, 210, 100));

        lblPerfil.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblPerfil.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPerfil.setText("Perfil");
        add(lblPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 80, 25));

        lblUsuario.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUsuario.setText("Usuario");
        add(lblUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, 80, 25));

        btnEliminarItemPerfil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/eliminaritem.png"))); // NOI18N
        btnEliminarItemPerfil.setToolTipText("Eliminar Perfil");
        btnEliminarItemPerfil.setOpaque(false);
        add(btnEliminarItemPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 115, 20, 40));

        btnAgregarItemPerfil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/agregaritem.png"))); // NOI18N
        btnAgregarItemPerfil.setToolTipText("Agregar Perfil");
        btnAgregarItemPerfil.setOpaque(false);
        add(btnAgregarItemPerfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 65, 20, 40));

        btnEliminarItemUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/eliminaritem.png"))); // NOI18N
        btnEliminarItemUsuario.setToolTipText("Eliminar Usuario");
        btnEliminarItemUsuario.setOpaque(false);
        add(btnEliminarItemUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 215, 20, 40));

        btnAgregarItemUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/agregaritem.png"))); // NOI18N
        btnAgregarItemUsuario.setToolTipText("Agregar Usuario");
        btnAgregarItemUsuario.setOpaque(false);
        add(btnAgregarItemUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 165, 20, 40));

        lblFechaActivo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaActivo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaActivo.setText("Fecha Activo");
        add(lblFechaActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 80, 25));
        add(txtFechaActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, -1, -1));

        tblTalonario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        add(tblTalonario, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 0, 0));
    }// </editor-fold>//GEN-END:initComponents

    private void btnTalonarioActionPerformed(java.awt.event.ActionEvent evt) {
        this.configurarPanelTalonario();
    }                                        

    private void optActivoItemStateChanged(java.awt.event.ItemEvent evt) {
        especifico.entTalonario ent = mod.getEntidad();
        if (optActivo.isSelected()) {
            if (ent.getFechaActivo()==null) {
                ent.setFechaActivo(new java.util.Date());
                this.txtFechaActivo.setFecha(ent.getFechaActivo());
            }
        }
    }

    private void optInactivoItemStateChanged(java.awt.event.ItemEvent evt) {
        especifico.entTalonario ent = mod.getEntidad();
        if (optInactivo.isSelected()) {
            if (ent.getFechaActivo()!=null) {
                this.txtFechaActivo.setFecha(ent.getFechaActivo());
            }
        }
    }

    private void optPerfilItemStateChanged(java.awt.event.ItemEvent evt) {
        if (this.optPerfil.isSelected() && this.optPerfil.isEnabled()) {
            this.habilitarPerfil(true);
            this.habilitarUsuario(false);
        }
    }

    private void optUsuarioItemStateChanged(java.awt.event.ItemEvent evt) {
        if (this.optUsuario.isSelected() && this.optUsuario.isEnabled()) {
            this.habilitarPerfil(false);
            this.habilitarUsuario(true);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnAgregarItemPerfil;
    public javax.swing.JButton btnAgregarItemUsuario;
    public javax.swing.JButton btnEliminarItemPerfil;
    public javax.swing.JButton btnEliminarItemUsuario;
    private javax.swing.ButtonGroup groEstado;
    private javax.swing.ButtonGroup groTipoPermiso;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblFechaActivo;
    private javax.swing.JLabel lblPerfil;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JList lstPerfil;
    private javax.swing.JList lstPerfilPermitido;
    private javax.swing.JList lstUsuario;
    private javax.swing.JList lstUsuarioPermitido;
    private javax.swing.JRadioButton optActivo;
    private javax.swing.JRadioButton optAnulado;
    private javax.swing.JRadioButton optInactivo;
    private javax.swing.JRadioButton optPerfil;
    private javax.swing.JRadioButton optTerminado;
    private javax.swing.JRadioButton optUsuario;
    private javax.swing.JRadioButton optVencido;
    private javax.swing.JScrollPane scpPerfil;
    private javax.swing.JScrollPane scpPerfilPermitido;
    private javax.swing.JScrollPane scpUsuario;
    private javax.swing.JScrollPane scpUsuarioPermitido;
    public javax.swing.JTable tblTalonario;
    private panel.fecha txtFechaActivo;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        optActivo.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                optActivoItemStateChanged(evt);
            }
        });
        optInactivo.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                optInactivoItemStateChanged(evt);
            }
        });
        optPerfil.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                optPerfilItemStateChanged(evt);
            }
        });
        optUsuario.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                optUsuarioItemStateChanged(evt);
            }
        });
        btnAgregarItemPerfil.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarItem(lstPerfil, lstPerfilPermitido);
            }
        });
        btnEliminarItemPerfil.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarItem(lstPerfilPermitido);
            }
        });
        btnAgregarItemUsuario.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarItem(lstUsuario, lstUsuarioPermitido);
            }
        });
        btnEliminarItemUsuario.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarItem(lstUsuarioPermitido);
            }
        });
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.optActivo.setEnabled(bmodo);
        this.optInactivo.setEnabled(bmodo);
        this.optAnulado.setEnabled(false);
        this.optTerminado.setEnabled(false);
        this.optVencido.setEnabled(false);
        this.txtFechaActivo.setEnabled(false);
        this.radTipoPermiso.setEnabled(bmodo);
        int iidPerfil = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%TIPO%PERMISO%", "PERFIL");
        int iidUsuario = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%TIPO%PERMISO%", "USUARIO");
        if (this.radTipoPermiso.getIdSeleccionado()==iidPerfil) {
            this.habilitarPerfil(bmodo);
            this.habilitarUsuario(false);
        }
        if (this.radTipoPermiso.getIdSeleccionado()==iidUsuario) {
            this.habilitarPerfil(false);
            this.habilitarUsuario(bmodo);
        }
        if (this.radTipoPermiso.getIdSeleccionado()!=iidPerfil && this.radTipoPermiso.getIdSeleccionado()!=iidUsuario) {
            this.habilitarPerfil(false);
            this.habilitarUsuario(false);
        }
    }
    
    private void iniciarComponentes() {
        this.radEstadoTalonario = new utilitario.comRadioButton(this.groEstado, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwsubtipo)+" WHERE tipo LIKE '%ESTADO%TALONARIO%' ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.radTipoPermiso = new utilitario.comRadioButton(this.groTipoPermiso, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwsubtipo)+" WHERE tipo LIKE '%TIPO%PERMISO%' ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        
        this.lstPerfil.setModel(new generico.modLista()); // establece el modelo
        this.lstPerfilPermitido.setModel(new generico.modLista()); // establece el modelo
        this.lstUsuario.setModel(new generico.modLista()); // establece el modelo
        this.lstUsuarioPermitido.setModel(new generico.modLista()); // establece el modelo
        // instancia de la interfaz de la aprobación
        //int iidActivo = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "ACTIVO");
        //int iidInactivo = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "INACTIVO");
        //sfiltroFormulario = " AND (idestado="+iidActivo+" OR idestado="+iidInactivo+")";

        this.lstPerfil.setToolTipText(especifico.entTalonario.TEXTO_PERFIL);
        this.lstPerfilPermitido.setToolTipText(especifico.entTalonario.TEXTO_PERFIL_PERMITIDO);
        this.lstUsuario.setToolTipText(especifico.entTalonario.TEXTO_USUARIO);
        this.lstUsuarioPermitido.setToolTipText(especifico.entTalonario.TEXTO_USUARIO_PERMITIDO);
        this.radEstadoTalonario.setToolTipText(especifico.entTalonario.TEXTO_ESTADO);
        this.radTipoPermiso.setToolTipText(especifico.entTalonario.TEXTO_TIPO_PERMISO);
    }
    
    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) { 
            this.setInterfaz(new especifico.entTalonario());
        }
        this.obtenerDatoPermisoTalonario();
    }
    
    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        especifico.entTalonario ent = mod.getEntidad();
        // activo o inactivo
        if (ent.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "ACTIVO") || ent.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "INACTIVO")) {
            this.setInterfaz(ent);
        } else {
            return false;
        }
        return true;
    }
    
    public boolean establecer() {
        especifico.entTalonario ent = mod.getEntidad().copiar(new especifico.entTalonario()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        // activo, inactivo
        if (!ent.esValidoOtorgarPermiso(((generico.modLista)this.lstPerfilPermitido.getModel()).getSize(), ((generico.modLista)this.lstUsuarioPermitido.getModel()).getSize())) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }
    
    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        this.guardarPermisoTalonario();
        return true;
    }

    public boolean obtenerDato() {
        this.obtenerDatoPerfilUsuario();
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro+sfiltroFormulario, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            return false;
        }
            javax.swing.JOptionPane.showMessageDialog(null, "2");
        return true;
    }
    
    public void refrescarComponente() {
    }
    
    private void getInterfaz(especifico.entTalonario ent) {
        ent.setFechaActivo(this.txtFechaActivo.getFecha());
        ent.setIdEstado(this.radEstadoTalonario.getIdSeleccionado());
        ent.setEstado(this.radEstadoTalonario.getDescripcionSeleccionado());
        ent.setIdTipoPermiso(this.radTipoPermiso.getIdSeleccionado());
        ent.setTipoPermiso(this.radTipoPermiso.getDescripcionSeleccionado());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entTalonario ent) {
        try                 {this.txtFechaActivo.setFecha(ent.getFechaActivo());}
        catch (Exception e) {this.txtFechaActivo.setFechaVacia();}
        try                 {this.radEstadoTalonario.setId(ent.getIdEstado());}
        catch (Exception e) {this.groEstado.clearSelection();}
        try                 {this.radTipoPermiso.setId(ent.getIdTipoPermiso());}
        catch (Exception e) {this.groTipoPermiso.clearSelection();}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

    //======================================
    // METODOS DE LOS PERMISOS
    //======================================
    
    private void habilitarPerfil(boolean bmodo) {
        this.lstPerfil.setEnabled(bmodo);
        this.lstPerfilPermitido.setEnabled(bmodo);
        this.btnAgregarItemPerfil.setEnabled(bmodo);
        this.btnEliminarItemPerfil.setEnabled(bmodo);
    }
    
    private void habilitarUsuario(boolean bmodo) {
        this.lstUsuario.setEnabled(bmodo);
        this.lstUsuarioPermitido.setEnabled(bmodo);
        this.btnAgregarItemUsuario.setEnabled(bmodo);
        this.btnEliminarItemUsuario.setEnabled(bmodo);
    }
    
    public void obtenerDatoPerfilUsuario() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(generico.vista.getNombre(generico.vista.vwperfil1), "", "ORDER BY descripcion"))) {
            ((generico.modLista)this.lstPerfil.getModel()).cargarModelo(mnt.intPrincipal.pnlEstado.conexion.rs);
            this.lstPerfil.updateUI();
        }
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(generico.vista.getNombre(generico.vista.vwusuario2), " AND activo='TRUE'", " ORDER BY descripcion"))) {
            ((generico.modLista)this.lstUsuario.getModel()).cargarModelo(mnt.intPrincipal.pnlEstado.conexion.rs);
            this.lstUsuario.updateUI();
        }
    }
    
    private void obtenerDatoPermisoTalonario() {
        ((generico.modLista)this.lstPerfilPermitido.getModel()).removerTodo();
        ((generico.modLista)this.lstUsuarioPermitido.getModel()).removerTodo();
        int iidPerfil = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%TIPO%PERMISO%", "PERFIL");
        int iidUsuario = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%TIPO%PERMISO%", "USUARIO");
        if (this.radTipoPermiso.getIdSeleccionado()==iidPerfil) {
            if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(generico.vista.getNombre(generico.vista.vwpermisotalonarioperfil1), " AND idtalonario="+mod.getEntidad().getId(), " ORDER BY descripcion"))) {
                ((generico.modLista)this.lstPerfilPermitido.getModel()).cargarModelo(mnt.intPrincipal.pnlEstado.conexion.rs);
            }
        }
        if (this.radTipoPermiso.getIdSeleccionado()==iidUsuario) {
            if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(generico.vista.getNombre(generico.vista.vwpermisotalonariousuario1), " AND idtalonario="+mod.getEntidad().getId(), " ORDER BY descripcion"))) {
                ((generico.modLista)this.lstUsuarioPermitido.getModel()).cargarModelo(mnt.intPrincipal.pnlEstado.conexion.rs);
            }
        }
        this.lstPerfilPermitido.updateUI();
        this.lstUsuarioPermitido.updateUI();
    }
    
    private boolean guardarPermisoTalonario() {
        boolean confirmado = false;
        if (this.radTipoPermiso.getIdSeleccionado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%TIPO%PERMISO%", "PERFIL")) {
            //elimina todos los permisos del talonario
            confirmado = mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getCampoTalonarioPermiso(0, mod.getEntidad().getId(), 0, 0, generico.entConstante.estadoregistro_eliminado)); // elimina todos
            if (confirmado) {
                generico.modLista modPerfil = (generico.modLista)this.lstPerfilPermitido.getModel();
                for (int i=0; i<modPerfil.getSize(); i++) {
                    confirmado = mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getCampoTalonarioPermiso(mnt.intPrincipal.pnlEstado.conexion.obtenerId("permisotalonario"), mod.getEntidad().getId(), modPerfil.getEntidad(i).getId(), 0, generico.entConstante.estadoregistro_insertado)); // inserta
                }
            }
        }
        if (this.radTipoPermiso.getIdSeleccionado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%TIPO%PERMISO%", "USUARIO")) {
            //elimina todos los permisos del talonario
            confirmado = mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getCampoTalonarioPermiso(0, mod.getEntidad().getId(), 0, 0, generico.entConstante.estadoregistro_eliminado)); // elimina todos
            if (confirmado) {
                generico.modLista modUsuario = (generico.modLista)this.lstUsuarioPermitido.getModel();
                for (int i=0; i<modUsuario.getSize(); i++) {
                    confirmado = mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getCampoTalonarioPermiso(mnt.intPrincipal.pnlEstado.conexion.obtenerId("permisotalonario"), mod.getEntidad().getId(), 0, modUsuario.getEntidad(i).getId(), generico.entConstante.estadoregistro_insertado)); // inserta
                }
            }
        }
        if (!confirmado) javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
        return confirmado;
    }
    
    private void agregarItem(javax.swing.JList lista, javax.swing.JList permitido) {
        try {
            generico.modLista modLista = (generico.modLista)lista.getModel();
            generico.modLista modPermitido = (generico.modLista)permitido.getModel();
            if (!existeItem(modLista.getEntidad(lista.getSelectedIndex()).getId(), permitido)) {
                modPermitido.agregarItem(modLista.getEntidad(lista.getSelectedIndex()));
            }
            permitido.updateUI();
        } catch(Exception e) { }
    }

    private boolean existeItem(int iid, javax.swing.JList permitido) {
        for (int i=0; i<((generico.modLista)permitido.getModel()).getSize(); i++) {
            if (((generico.modLista)permitido.getModel()).getEntidad(i).getId()==iid) return true;
        }
        return false;
    }
    
    private void eliminarItem(javax.swing.JList permitido) {
        try {
            generico.modLista modPermitido = (generico.modLista)permitido.getModel();
            modPermitido.eliminarItem(permitido.getSelectedIndex());
            permitido.updateUI();
        } catch(Exception e) { }
    }
    
    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================

    private void configurarPanelTalonario() {
        intTalonario = new especifico.intTalonario(mnt, this.tblTalonario);
        generico.intPanel panel = new generico.intPanel("Talonario :: Mantenimiento :: " + generico.mnuMantenimientoDetalle.visualizar, intTalonario);
        intTalonario.setEnabled(false);
        especifico.entTalonario ent = mod.getEntidad().copiar(new especifico.entTalonario()); // obtiene la entidad del registro seleccionado
        intTalonario.mod.insertar(ent);
        intTalonario.mod.getTable().setRowSelectionInterval(0,0);
        intTalonario.cargarEditor();
        if (intTalonario.mod.getTable().getSelectedRow()>=0) panel.abrir();
    }

}
