/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intbOrdenPagoImprimir.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class intbOrdenPagoImprimir extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    private utilitario.comComboBox cmbEstado;
    private utilitario.comComboBox cmbRegional;
    private utilitario.comComboBox cmbCuenta;
    private utilitario.comComboBox cmbFilial;
    
    /** Creates new form intAhorroProgramadoRetirar 
     @param mnt.
     */
    public intbOrdenPagoImprimir(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
        this.setEnabled();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfIdEstadoAhorro = new javax.swing.JFormattedTextField();
        pnlEstado = new javax.swing.JPanel();
        lblEstado = new javax.swing.JLabel();
        cmboEstado = new javax.swing.JComboBox();
        chkEstado = new javax.swing.JCheckBox();
        btnImprimirEstadoRegional = new javax.swing.JButton();
        chkRegional = new javax.swing.JCheckBox();
        lblRegional = new javax.swing.JLabel();
        lblCuenta = new javax.swing.JLabel();
        chkCuenta = new javax.swing.JCheckBox();
        try {
            txtFechaDesde = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        try {
            txtFechaHasta = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblDesde = new javax.swing.JLabel();
        lblHasta = new javax.swing.JLabel();
        cmboRegional = new javax.swing.JComboBox();
        cmboCuenta = new javax.swing.JComboBox();
        cmboCentroCosto = new javax.swing.JComboBox();
        lblCentroCosto = new javax.swing.JLabel();
        chkCentroCosto = new javax.swing.JCheckBox();
        lblPromotor = new javax.swing.JLabel();
        cmboPromotor = new javax.swing.JComboBox();
        chkPromotor = new javax.swing.JCheckBox();
        lblFilial = new javax.swing.JLabel();
        cmboFilial = new javax.swing.JComboBox();
        pnlImprimirLiquidacion = new javax.swing.JPanel();
        lblNumeroOrden = new javax.swing.JLabel();
        txtfNumeroOrdenDesde = new javax.swing.JFormattedTextField();
        btnImprimirOrden = new javax.swing.JButton();
        txtfNumeroOrdenHasta = new javax.swing.JFormattedTextField();
        lblHasta1 = new javax.swing.JLabel();

        setMinimumSize(new java.awt.Dimension(408, 300));
        setPreferredSize(new java.awt.Dimension(408, 300));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfIdEstadoAhorro.setFocusable(false);
        txtfIdEstadoAhorro.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfIdEstadoAhorro, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 0, 25));

        pnlEstado.setBackground(new java.awt.Color(204, 204, 204));
        pnlEstado.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pnlEstado.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblEstado.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstado.setText("Estado");
        pnlEstado.add(lblEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 80, 25));

        cmboEstado.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 10, 260, 25));

        chkEstado.setToolTipText("");
        chkEstado.setOpaque(false);
        pnlEstado.add(chkEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 20, 25));

        btnImprimirEstadoRegional.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirEstadoRegional.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirEstadoRegional.setToolTipText("Imprimir");
        btnImprimirEstadoRegional.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirEstadoRegional.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirEstadoRegional.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirEstadoRegional.setOpaque(false);
        btnImprimirEstadoRegional.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlEstado.add(btnImprimirEstadoRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 170, 25, 25));

        chkRegional.setToolTipText("");
        chkRegional.setOpaque(false);
        pnlEstado.add(chkRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 35, 20, 25));

        lblRegional.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblRegional.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblRegional.setText("Regional");
        pnlEstado.add(lblRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 80, 25));

        lblCuenta.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblCuenta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCuenta.setText("Cuenta");
        pnlEstado.add(lblCuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 80, 25));

        chkCuenta.setToolTipText("");
        chkCuenta.setOpaque(false);
        pnlEstado.add(chkCuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 20, 25));
        pnlEstado.add(txtFechaDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 160, -1, 25));
        pnlEstado.add(txtFechaHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 185, -1, 25));

        lblDesde.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblDesde.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDesde.setText("Desde");
        pnlEstado.add(lblDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 60, 25));

        lblHasta.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblHasta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblHasta.setText("Hasta");
        pnlEstado.add(lblHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 185, 60, 25));

        cmboRegional.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboRegional, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 35, 260, 25));

        cmboCuenta.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboCuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 60, 260, 25));

        cmboCentroCosto.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        pnlEstado.add(cmboCentroCosto, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 85, 260, 25));

        lblCentroCosto.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblCentroCosto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCentroCosto.setText("Centro de Costo");
        pnlEstado.add(lblCentroCosto, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 85, 80, 25));

        chkCentroCosto.setToolTipText("");
        chkCentroCosto.setOpaque(false);
        pnlEstado.add(chkCentroCosto, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 85, 20, 25));

        lblPromotor.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblPromotor.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPromotor.setText("Promotor");
        pnlEstado.add(lblPromotor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 80, 25));

        cmboPromotor.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        pnlEstado.add(cmboPromotor, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 110, 260, 25));

        chkPromotor.setToolTipText("");
        chkPromotor.setOpaque(false);
        pnlEstado.add(chkPromotor, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, 20, 25));

        lblFilial.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFilial.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFilial.setText("Filial");
        pnlEstado.add(lblFilial, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 135, 50, 25));

        cmboFilial.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        pnlEstado.add(cmboFilial, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 135, 260, 25));

        add(pnlEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 390, 230));

        pnlImprimirLiquidacion.setBackground(new java.awt.Color(204, 204, 204));
        pnlImprimirLiquidacion.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pnlImprimirLiquidacion.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblNumeroOrden.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblNumeroOrden.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumeroOrden.setText("Nº Orden de Pago");
        pnlImprimirLiquidacion.add(lblNumeroOrden, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 90, 25));

        txtfNumeroOrdenDesde.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfNumeroOrdenDesde.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        txtfNumeroOrdenDesde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfNumeroOrdenDesdeActionPerformed(evt);
            }
        });
        pnlImprimirLiquidacion.add(txtfNumeroOrdenDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 90, 25));

        btnImprimirOrden.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirOrden.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirOrden.setToolTipText("Imprimir Solicitud");
        btnImprimirOrden.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirOrden.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirOrden.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirOrden.setOpaque(false);
        btnImprimirOrden.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlImprimirLiquidacion.add(btnImprimirOrden, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 10, 25, 25));

        txtfNumeroOrdenHasta.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfNumeroOrdenHasta.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        txtfNumeroOrdenHasta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfNumeroOrdenHastaActionPerformed(evt);
            }
        });
        pnlImprimirLiquidacion.add(txtfNumeroOrdenHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 10, 90, 25));

        lblHasta1.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblHasta1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblHasta1.setText("Hasta");
        pnlImprimirLiquidacion.add(lblHasta1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, 40, 25));

        add(pnlImprimirLiquidacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 390, 45));
    }// </editor-fold>//GEN-END:initComponents

    private void txtfNumeroOrdenDesdeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfNumeroOrdenDesdeActionPerformed
        this.txtfNumeroOrdenHasta.setText(this.txtfNumeroOrdenDesde.getText());
        this.txtfNumeroOrdenHasta.requestFocus();
        this.txtfNumeroOrdenHasta.selectAll();
    }//GEN-LAST:event_txtfNumeroOrdenDesdeActionPerformed

    private void txtfNumeroOrdenHastaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfNumeroOrdenHastaActionPerformed
        this.btnImprimirOrden.requestFocus();
    }//GEN-LAST:event_txtfNumeroOrdenHastaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnImprimirEstadoRegional;
    private javax.swing.JButton btnImprimirOrden;
    private javax.swing.JCheckBox chkCentroCosto;
    private javax.swing.JCheckBox chkCuenta;
    private javax.swing.JCheckBox chkEstado;
    private javax.swing.JCheckBox chkPromotor;
    private javax.swing.JCheckBox chkRegional;
    public javax.swing.JComboBox cmboCentroCosto;
    private javax.swing.JComboBox cmboCuenta;
    private javax.swing.JComboBox cmboEstado;
    private javax.swing.JComboBox cmboFilial;
    private javax.swing.JComboBox cmboPromotor;
    private javax.swing.JComboBox cmboRegional;
    private javax.swing.JLabel lblCentroCosto;
    private javax.swing.JLabel lblCuenta;
    private javax.swing.JLabel lblDesde;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblFilial;
    private javax.swing.JLabel lblHasta;
    private javax.swing.JLabel lblHasta1;
    private javax.swing.JLabel lblNumeroOrden;
    private javax.swing.JLabel lblPromotor;
    private javax.swing.JLabel lblRegional;
    private javax.swing.JPanel pnlEstado;
    private javax.swing.JPanel pnlImprimirLiquidacion;
    private panel.fecha txtFechaDesde;
    private panel.fecha txtFechaHasta;
    private javax.swing.JFormattedTextField txtfIdEstadoAhorro;
    public javax.swing.JFormattedTextField txtfNumeroOrdenDesde;
    public javax.swing.JFormattedTextField txtfNumeroOrdenHasta;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnImprimirOrden.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirOrden();
            }
        });
        btnImprimirEstadoRegional.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirEstadoRegional();
            }
        });
        chkRegional.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbRegional.setId(0);
                cmbRegional.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkEstado.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmboEstado.setSelectedIndex(-1);
                cmboEstado.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkCuenta.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbCuenta.setId(0);
                cmbCuenta.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
    }

    private void setEnabled() {
        this.cmbFilial.setEnabled(mnt.intPrincipal.pnlEstado.usuario.getIdFilial()==1);
        this.chkRegional.setSelected(true);
        this.chkEstado.setSelected(true);
        this.chkCuenta.setSelected(true);
    }
    
    private void iniciarComponentes() {
        this.cmbEstado = new utilitario.comComboBox(this.cmboEstado, this.lblEstado, "SELECT id, descripcion FROM vwsubtipo WHERE tipo LIKE '%ESTADO%ORDEN%PAGO%' ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbRegional = new utilitario.comComboBox(this.cmboRegional, this.lblRegional, "SELECT id, descripcion FROM vwregional1 ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbCuenta = new utilitario.comComboBox(this.cmboCuenta, this.lblCuenta, "SELECT id, descripcion FROM banco.vwbancocuenta1 ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbFilial = new utilitario.comComboBox(this.cmboFilial, this.lblFilial, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwempresa1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        
        this.cmbFilial.setId(mnt.intPrincipal.pnlEstado.usuario.getIdFilial());
        
        this.txtfNumeroOrdenDesde.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfNumeroOrdenDesde.setToolTipText("Número de Orden de Pago desde el cual se imprimirá");
        this.txtfNumeroOrdenHasta.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfNumeroOrdenHasta.setToolTipText("Número de Orden de Pago hasta el cual se imprimirá");
        this.btnImprimirOrden.setToolTipText("Imprimir Orden de Pago para firma");
        this.btnImprimirEstadoRegional.setToolTipText("Imprimir Informe de Órdenes de Pago según criterio");
        this.chkEstado.setToolTipText("Todos los estados de Orden de Pago");
        this.cmbEstado.setToolTipText("Estado de Orden de Pago");
        this.chkRegional.setToolTipText("Todos las Regionales");
        this.cmbRegional.setToolTipText("Regionales");
        this.chkCuenta.setToolTipText("Todos las Cuentas");
        this.cmbCuenta.setToolTipText("Cuentas");
        this.cmbFilial.setToolTipText(especifico.entbOrdenPago.TEXTO_FILIAL);
        this.txtFechaDesde.setToolTipText("Fecha desde");
        this.txtFechaHasta.setToolTipText("Fecha hasta");
    }

    private void imprimirOrden() {
        String sfiltro = " AND estado LIKE '%CONFIRMADO%'";
        int total = 0;

        if (this.txtfNumeroOrdenDesde.getText().equals("") && this.txtfNumeroOrdenHasta.getText().equals("")) {
            javax.swing.JOptionPane.showMessageDialog(null, "Debe introducir un valor de rango válido para Nº de Orden de pago");
            return;
        }
        if (utilitario.utiNumero.convertirToInt(this.txtfNumeroOrdenDesde.getText())> 0 && this.txtfNumeroOrdenHasta.getText().equals("")) {
            sfiltro += " AND nroordenpago >= " + utilitario.utiNumero.convertirToInt(this.txtfNumeroOrdenDesde.getText());
        }
        if (this.txtfNumeroOrdenDesde.getText().equals("") && utilitario.utiNumero.convertirToInt(this.txtfNumeroOrdenHasta.getText())>0) {
            sfiltro += " AND nroordenpago <= " + utilitario.utiNumero.convertirToInt(this.txtfNumeroOrdenHasta.getText());
        }
        if (utilitario.utiNumero.convertirToInt(this.txtfNumeroOrdenDesde.getText())> 0 && utilitario.utiNumero.convertirToInt(this.txtfNumeroOrdenHasta.getText())>0) {
            sfiltro += " AND nroordenpago >= " + utilitario.utiNumero.convertirToInt(this.txtfNumeroOrdenDesde.getText());
            sfiltro += " AND nroordenpago <= " + utilitario.utiNumero.convertirToInt(this.txtfNumeroOrdenHasta.getText());
        }
        if(mnt.intPrincipal.pnlEstado.usuario.getIdFilial()!=1) sfiltro+=" AND idfilial="+mnt.intPrincipal.pnlEstado.usuario.getIdFilial();
        //System.out.println(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco."+generico.vista.getNombre(generico.vista.vwrp_ordenpago1), sfiltro, " ORDER BY numeroorden"));
        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco."+generico.vista.getNombre(generico.vista.vwrp_ordenpago1), sfiltro, " ORDER BY numeroorden"));
        
        try {
            this.mnt.intPrincipal.pnlEstado.conexion.rs.last();
            total = this.mnt.intPrincipal.pnlEstado.conexion.rs.getRow();
        } catch (Exception e) {}
        if (total==0) {
            javax.swing.JOptionPane.showMessageDialog(null, "El documento no tiene páginas");
            return;
        }
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repbOrdenPago, mnt.intPrincipal.pnlEstado.conexion.rs, null, null, null, "Orden de Pago", "", mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }

    }

    private void imprimirEstadoRegional() {
        String sfiltro = "";
        String stextoFiltro = "";
        if (this.cmbEstado.getIdSeleccionado()>0) {
            sfiltro += " AND idestado="+this.cmbEstado.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbEstado.getDescripcionSeleccionado();
        }
        if (this.cmbRegional.getIdSeleccionado()>0) {
            sfiltro += " AND idregional="+this.cmbRegional.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbRegional.getDescripcionSeleccionado();
        }
        if (this.cmbCuenta.getIdSeleccionado()>0) {
            sfiltro += " AND idbancocuenta="+this.cmbCuenta.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbCuenta.getDescripcionSeleccionado();
        }
        if (this.cmbFilial.getIdSeleccionado()>0) {
            sfiltro += " AND idfilial="+this.cmbFilial.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro +=this.cmbFilial.getDescripcionSeleccionado();
        } else if (mnt.intPrincipal.pnlEstado.usuario.getIdFilial()!=1) {
            sfiltro += " AND idfilial="+mnt.intPrincipal.pnlEstado.usuario.getIdFilial();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += mnt.intPrincipal.pnlEstado.usuario.getFilial();
        }
        if (this.txtFechaDesde.getFecha()==null && this.txtFechaHasta.getFecha()!=null) {
            sfiltro += " AND fecha<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "FECHA: "+utilitario.utiNumero.convertirToString(this.txtFechaHasta.getFecha().getDate()+100).substring(1)+"/"+utilitario.utiFecha.getNumeroMes(this.txtFechaHasta.getFecha())+"/"+utilitario.utiFecha.getAnho(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()==null) {
            sfiltro += " AND fecha>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "FECHA: "+utilitario.utiNumero.convertirToString(this.txtFechaDesde.getFecha().getDate()+100).substring(1)+"/"+utilitario.utiFecha.getNumeroMes(this.txtFechaDesde.getFecha())+"/"+utilitario.utiFecha.getAnho(this.txtFechaDesde.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()!=null) {
            sfiltro += " AND fecha>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND fecha<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "FECHA: "+utilitario.utiNumero.convertirToString(this.txtFechaDesde.getFecha().getDate()+100).substring(1)+"/"+utilitario.utiFecha.getNumeroMes(this.txtFechaDesde.getFecha())+"/"+utilitario.utiFecha.getAnho(this.txtFechaDesde.getFecha())+" A "+utilitario.utiNumero.convertirToString(this.txtFechaDesde.getFecha().getDate()+100).substring(1)+"/"+utilitario.utiFecha.getNumeroMes(this.txtFechaHasta.getFecha())+"/"+utilitario.utiFecha.getAnho(this.txtFechaHasta.getFecha());
        }
        especifico.modbOrdenPago mod = new especifico.modbOrdenPago();
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco.vwordenpago1", sfiltro, " ORDER BY nroordenpago"))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        }    
        System.out.println(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco.vwordenpago1", sfiltro, " ORDER BY estado, regional, cuentabanco"));
        if (mod.getRowCount()==0) {
            javax.swing.JOptionPane.showMessageDialog(null, "El documento no tiene páginas");
            return;
        }
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repbOrdenPagoFecha, mod, null, null, null, "Informe Órdenes de Pago", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }
    
}
