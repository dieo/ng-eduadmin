/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intLugarLaboral.java
 *
 * Created on 07-abr-2012, 21:48:31
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intDetalleEstadoSocio extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modDetalleEstadoSocio mod = new especifico.modDetalleEstadoSocio();
    private utilitario.comComboBox cmbEstadoAnterior;
    private utilitario.comComboBox cmbEstadoActual;
    private utilitario.comComboBox cmbDocumento;
    private String svista="", sfiltro="", sorden="";
    
    /** Creates new form intLugarLaboral 
     @param mnt.
     @param tbl.
     */
    public intDetalleEstadoSocio(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }
    
    public intDetalleEstadoSocio(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        this.mod.setTable(new javax.swing.JTable());
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        txtfObservacion = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblEstadoAnterior = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        lblObservacion = new javax.swing.JLabel();
        try {
            txtFecha = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblDocumento = new javax.swing.JLabel();
        cmboEstadoAnterior = new javax.swing.JComboBox();
        cmboDocumento = new javax.swing.JComboBox();
        try {
            txtFechaDocumento = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblFechaDocumento = new javax.swing.JLabel();
        lblEstadoActual = new javax.swing.JLabel();
        cmboEstadoActual = new javax.swing.JComboBox();
        txtfNumeroDocumento = new javax.swing.JFormattedTextField();
        lblNumeroDocumento = new javax.swing.JLabel();
        chkActivo = new javax.swing.JCheckBox();
        lblActivo = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(515, 165));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(515, 165));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 110, 25));

        txtfObservacion.setToolTipText("");
        txtfObservacion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 135, 420, 25));

        lblId.setBackground(new java.awt.Color(255, 255, 255));
        lblId.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 80, 25));

        lblEstadoAnterior.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblEstadoAnterior.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstadoAnterior.setText("Estado Anterior");
        lblEstadoAnterior.setToolTipText("");
        add(lblEstadoAnterior, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 80, 25));

        lblFecha.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblFecha.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFecha.setText("Fecha");
        add(lblFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, 80, 25));

        lblObservacion.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblObservacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObservacion.setText("Observación");
        add(lblObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 135, 80, 25));
        add(txtFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 10, -1, -1));

        lblDocumento.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblDocumento.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDocumento.setText("Documento");
        add(lblDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 85, 80, 25));

        cmboEstadoAnterior.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboEstadoAnterior, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 35, 420, 25));

        cmboDocumento.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 85, 290, 25));
        add(txtFechaDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, -1, -1));

        lblFechaDocumento.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblFechaDocumento.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaDocumento.setText("Fecha Docum.");
        add(lblFechaDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 110, 80, 25));

        lblEstadoActual.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblEstadoActual.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstadoActual.setText("Estado Actual");
        add(lblEstadoActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 80, 25));

        cmboEstadoActual.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboEstadoActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 420, 25));

        txtfNumeroDocumento.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfNumeroDocumento.setToolTipText("");
        txtfNumeroDocumento.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfNumeroDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 85, 60, 25));

        lblNumeroDocumento.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblNumeroDocumento.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumeroDocumento.setText("Nro. Doc.");
        add(lblNumeroDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 85, 50, 25));

        chkActivo.setOpaque(false);
        add(chkActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(485, 10, 20, 25));

        lblActivo.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblActivo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblActivo.setText("Activo");
        add(lblActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 10, 80, 25));
    }// </editor-fold>//GEN-END:initComponents

    private void tblListadoChange(javax.swing.event.ListSelectionEvent e) {
        this.cargarEditor();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkActivo;
    private javax.swing.JComboBox cmboDocumento;
    private javax.swing.JComboBox cmboEstadoActual;
    private javax.swing.JComboBox cmboEstadoAnterior;
    private javax.swing.JLabel lblActivo;
    private javax.swing.JLabel lblDocumento;
    private javax.swing.JLabel lblEstadoActual;
    private javax.swing.JLabel lblEstadoAnterior;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblFechaDocumento;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblNumeroDocumento;
    private javax.swing.JLabel lblObservacion;
    private panel.fecha txtFecha;
    private panel.fecha txtFechaDocumento;
    private javax.swing.JFormattedTextField txtfId;
    private javax.swing.JFormattedTextField txtfNumeroDocumento;
    private javax.swing.JFormattedTextField txtfObservacion;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        this.mod.getTable().getSelectionModel().addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            @Override
            public void valueChanged(javax.swing.event.ListSelectionEvent e){
                tblListadoChange(e);
            }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfObservacion);
        uti.agregarEventoKeyReleased(this.txtfObservacion);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.cmbEstadoAnterior.setEnabled(false);
        this.cmbEstadoActual.setEnabled(bmodo);
        this.chkActivo.setEnabled(bmodo);
        this.txtFecha.setEnabled(bmodo);
        this.cmbDocumento.setEnabled(bmodo);
        this.txtfNumeroDocumento.setEnabled(bmodo);
        this.txtFechaDocumento.setEnabled(bmodo);
        this.txtfObservacion.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.cmbEstadoAnterior = new utilitario.comComboBox(this.cmboEstadoAnterior, this.lblEstadoAnterior, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwsubtipo)+" WHERE tipo LIKE '%ESTADO%SOCIO%' ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbEstadoActual = new utilitario.comComboBox(this.cmboEstadoActual, this.lblEstadoActual, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwsubtipo)+" WHERE tipo LIKE '%ESTADO%SOCIO%' ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbDocumento = new utilitario.comComboBox(this.cmboDocumento, this.lblDocumento, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwdocumento1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        
        this.txtfObservacion.setDocument(new generico.modFormatoCadena(this.txtfObservacion, especifico.entDetalleEstadoSocio.LONGITUD_OBSERVACION, true));
        this.txtfNumeroDocumento.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfId.setToolTipText(especifico.entDetalleEstadoSocio.TEXTO_ID);
        this.cmbEstadoAnterior.setToolTipText(especifico.entDetalleEstadoSocio.TEXTO_ESTADO_ANTERIOR);
        this.cmbEstadoActual.setToolTipText(especifico.entDetalleEstadoSocio.TEXTO_ESTADO_ACTUAL);
        this.chkActivo.setToolTipText(especifico.entDetalleEstadoSocio.TEXTO_ACTIVO);
        this.txtFecha.setToolTipText(especifico.entDetalleEstadoSocio.TEXTO_FECHA);
        this.cmbDocumento.setToolTipText(especifico.entDetalleEstadoSocio.TEXTO_DOCUMENTO);
        this.txtfNumeroDocumento.setToolTipText(especifico.entDetalleEstadoSocio.TEXTO_NUMERO_DOCUMENTO);
        this.txtFechaDocumento.setToolTipText(especifico.entDetalleEstadoSocio.TEXTO_FECHA_DOCUMENTO);
        this.txtfObservacion.setToolTipText(especifico.entDetalleEstadoSocio.TEXTO_OBSERVACION);
    }
    
    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entDetalleEstadoSocio());
        }
    }

    public boolean insertar() {
        especifico.entSocio socio = ((especifico.modSocio)mnt.tblListado.getModel()).getEntidad(mnt.tblListado.getSelectedRow());
        // activo - gestion cobro - judicial - incobrable - asesoria juridica - gestion cobro gir - gestion cobro ven
        if (socio.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SOCIO%", "ACTIVO") || socio.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SOCIO%", "GESTION%COBRO") || socio.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SOCIO%", "JUDICIAL")  || socio.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SOCIO%", "COBRO JUDICIAL") || socio.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SOCIO%", "INCOBRABLE") || socio.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SOCIO%", "ASESORIA JURIDICA") || socio.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SOCIO%", "GESTION COBRO GIR") || socio.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SOCIO%", "GESTION COBRO VEN") || socio.getIdEstado() == mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%SOCIO%", "RETIRADO")) {
            especifico.entDetalleEstadoSocio ent = new especifico.entDetalleEstadoSocio();
            ent.setFecha(new java.util.Date());
            ent.setIdSocio(socio.getId()); // obtiene el id de socio del modelo y asigna
            ent.setIdEstadoAnterior(socio.getIdEstado());
            ent.setFechaDocumento(new java.util.Date());
            ent.setActivo(true);
            ent.setObservacion("---");
            ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
            mod.insertar(ent); // inserta nueva entidad
            this.setInterfaz(ent);
            return true;
        }
        return false;
    }

    public boolean modificar() {
        this.cmbEstadoActual.setEnabled(false);
        this.chkActivo.setEnabled(false);
        this.txtFecha.setEnabled(false);
        this.cmbDocumento.setEnabled(true);
        this.txtFechaDocumento.setEnabled(true);
        this.txtfObservacion.setEnabled(true);
        return true;
    }

    public boolean establecer() {
        especifico.entDetalleEstadoSocio ent = mod.getEntidad().copiar(new especifico.entDetalleEstadoSocio());
        this.getInterfaz(ent);
        if (!ent.esValido()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("detalleestadosocio"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        //mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return false;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
        this.cmbEstadoAnterior.refresh();
        this.cmbEstadoActual.refresh();
        this.cmbDocumento.refresh();
   }
    
    private void getInterfaz(especifico.entDetalleEstadoSocio ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setIdEstadoAnterior(this.cmbEstadoAnterior.getIdSeleccionado());
        ent.setEstadoAnterior(this.cmbEstadoAnterior.getDescripcionSeleccionado());
        ent.setIdEstadoActual(this.cmbEstadoActual.getIdSeleccionado());
        ent.setEstadoActual(this.cmbEstadoActual.getDescripcionSeleccionado());
        ent.setActivo(this.chkActivo.isSelected());
        ent.setFecha(this.txtFecha.getFecha());
        ent.setIdDocumento(this.cmbDocumento.getIdSeleccionado());
        ent.setDocumento(this.cmbDocumento.getDescripcionSeleccionado());
        ent.setNumeroDocumento(utilitario.utiNumero.convertirToInt(this.txtfNumeroDocumento.getText()));
        ent.setFechaDocumento(this.txtFechaDocumento.getFecha());
        ent.setObservacion(this.txtfObservacion.getText());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entDetalleEstadoSocio ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.cmbEstadoAnterior.setId(ent.getIdEstadoAnterior());}
        catch (Exception e) {this.cmbEstadoAnterior.setId(0);}
        try                 {this.cmbEstadoActual.setId(ent.getIdEstadoActual());}
        catch (Exception e) {this.cmbEstadoActual.setId(0);}
        try                 {this.chkActivo.setSelected(ent.getActivo());}
        catch (Exception e) {this.chkActivo.setSelected(false);}
        try                 {this.txtFecha.setFecha(ent.getFecha());}
        catch (Exception e) {this.txtFecha.setFechaVacia();}
        try                 {this.cmbDocumento.setId(ent.getIdDocumento());}
        catch (Exception e) {this.cmbDocumento.setId(0);}
        try                 {this.txtfNumeroDocumento.setValue(ent.getNumeroDocumento());}
        catch (Exception e) {this.txtfNumeroDocumento.setValue(0);}
        try                 {this.txtFechaDocumento.setFecha(ent.getFechaDocumento());}
        catch (Exception e) {this.txtFechaDocumento.setFechaVacia();}
        try                 {this.txtfObservacion.setText(ent.getObservacion());}
        catch (Exception e) {this.txtfObservacion.setText("");}
    }
    
    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

}
