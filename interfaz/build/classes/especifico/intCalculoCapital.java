/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intPais.java
 *
 * Created on 05-abr-2012, 8:07:13
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intCalculoCapital extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    private String svista="", sfiltro="", sorden="";

    /** Creates new form intPais 
     @param mnt.
     */
    public intCalculoCapital(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfDesembolso = new javax.swing.JFormattedTextField();
        txtfPlazo = new javax.swing.JFormattedTextField();
        lblDesembolso = new javax.swing.JLabel();
        lblPlazo = new javax.swing.JLabel();
        txtfTasaInteres = new javax.swing.JFormattedTextField();
        lblTasaInteres = new javax.swing.JLabel();
        lblSolicitado = new javax.swing.JLabel();
        txtfSolicitado = new javax.swing.JFormattedTextField();
        btnCalcular = new javax.swing.JButton();
        lblCalcular = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(285, 120));
        setPreferredSize(new java.awt.Dimension(285, 120));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfDesembolso.setForeground(new java.awt.Color(0, 102, 204));
        txtfDesembolso.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfDesembolso.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfDesembolso.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfDesembolso.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(txtfDesembolso, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 100, 25));

        txtfPlazo.setForeground(new java.awt.Color(0, 102, 204));
        txtfPlazo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfPlazo.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfPlazo.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfPlazo.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(txtfPlazo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 35, 50, 25));

        lblDesembolso.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDesembolso.setText("Desembolso");
        add(lblDesembolso, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 80, 25));

        lblPlazo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPlazo.setText("Plazo");
        add(lblPlazo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 80, 25));

        txtfTasaInteres.setForeground(new java.awt.Color(0, 102, 204));
        txtfTasaInteres.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfTasaInteres.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfTasaInteres.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfTasaInteres.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(txtfTasaInteres, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 50, 25));

        lblTasaInteres.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTasaInteres.setText("Tasa Interés");
        add(lblTasaInteres, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 80, 25));

        lblSolicitado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSolicitado.setText("Solicitado");
        add(lblSolicitado, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 85, 80, 25));

        txtfSolicitado.setForeground(new java.awt.Color(0, 102, 204));
        txtfSolicitado.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfSolicitado.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfSolicitado.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfSolicitado.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(txtfSolicitado, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 85, 100, 25));

        btnCalcular.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnCalcular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/generar22.png"))); // NOI18N
        btnCalcular.setToolTipText("Calcular");
        btnCalcular.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCalcular.setMaximumSize(new java.awt.Dimension(65, 35));
        btnCalcular.setMinimumSize(new java.awt.Dimension(65, 35));
        btnCalcular.setOpaque(false);
        btnCalcular.setPreferredSize(new java.awt.Dimension(65, 35));
        add(btnCalcular, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 10, 25, 25));

        lblCalcular.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCalcular.setText("Calcular");
        add(lblCalcular, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 10, 50, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcular;
    private javax.swing.JLabel lblCalcular;
    private javax.swing.JLabel lblDesembolso;
    private javax.swing.JLabel lblPlazo;
    private javax.swing.JLabel lblSolicitado;
    private javax.swing.JLabel lblTasaInteres;
    public javax.swing.JFormattedTextField txtfDesembolso;
    public javax.swing.JFormattedTextField txtfPlazo;
    public javax.swing.JFormattedTextField txtfSolicitado;
    public javax.swing.JFormattedTextField txtfTasaInteres;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcular();
            }
        });
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfDesembolso.setEnabled(bmodo);
        this.txtfPlazo.setEnabled(false);
        this.txtfTasaInteres.setEnabled(false);
        this.txtfSolicitado.setEnabled(false);
    }
    
    private void iniciarComponentes() {
        this.txtfDesembolso.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfPlazo.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfTasaInteres.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 2));
        this.txtfSolicitado.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfDesembolso.setToolTipText("Capital a desembolsar");
        this.txtfPlazo.setToolTipText("Plazo solicitado");
        this.txtfTasaInteres.setToolTipText("Tasa de interés");
        this.txtfSolicitado.setToolTipText("Capital a solicitar");
    }
    
    public void cargarEditor() {
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        return true;
    }

    public boolean eliminar() {
        return true;
    }

    public boolean cancelar() {
        return true;
    }

    public boolean guardar() {
        return true;
    }

    public boolean obtenerDato() {
        return true;
    }

    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entPais ent) {
    }
    
    private void setInterfaz(especifico.entPais ent) {
        this.txtfDesembolso.setValue(0.0);
        this.txtfPlazo.setValue(0);
        this.txtfTasaInteres.setValue(0.0);
        this.txtfSolicitado.setValue(0.0);
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
    }

    public void calcular() {
        double usolicitado = (1000 * utilitario.utiNumero.convertirToDoubleMac(this.txtfDesembolso.getText())) / (1000 - (utilitario.utiNumero.convertirToInt(this.txtfPlazo.getText()) * utilitario.utiNumero.convertirToDoubleMac(this.txtfTasaInteres.getText())));
        this.txtfSolicitado.setValue(usolicitado);
    }
    
}
