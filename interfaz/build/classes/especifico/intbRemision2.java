/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intbRemision.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class intbRemision2 extends javax.swing.JPanel {

    private generico.intPrincipal pcp;
    public generico.intMantenimiento mnt;
    private generico.intPanel panel;
    public especifico.modbOrdenPago mod = new especifico.modbOrdenPago();
    private utilitario.comComboBox cmbRemision;
    private String svista="", sfiltro="", sorden="", sfiltroFormulario="";

    /** Creates new form intbRemision 
     @param mnt.
     */
    public intbRemision2(generico.intMantenimiento mnt) {
        this.pcp = pcp;
        this.mnt = mnt;
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
        this.setEnabled();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfIdEstadoAhorro = new javax.swing.JFormattedTextField();
        pnlEstado = new javax.swing.JPanel();
        btnImprimir = new javax.swing.JButton();
        lblRemision = new javax.swing.JLabel();
        try {
            txtFechaDesde = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        try {
            txtFechaHasta = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblDesde = new javax.swing.JLabel();
        lblHasta = new javax.swing.JLabel();
        cmboRemision = new javax.swing.JComboBox();
        btnGenerarExcel = new javax.swing.JButton();
        chkReimpresion = new javax.swing.JCheckBox();
        chkRemision = new javax.swing.JCheckBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(510, 150));
        setPreferredSize(new java.awt.Dimension(510, 150));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfIdEstadoAhorro.setFocusable(false);
        txtfIdEstadoAhorro.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfIdEstadoAhorro, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 0, 25));

        pnlEstado.setBackground(new java.awt.Color(255, 255, 255));
        pnlEstado.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlEstado.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnImprimir.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimir.setToolTipText("Imprimir");
        btnImprimir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimir.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimir.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimir.setOpaque(false);
        btnImprimir.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlEstado.add(btnImprimir, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 85, 25, 25));

        lblRemision.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        lblRemision.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblRemision.setText("Remisión");
        pnlEstado.add(lblRemision, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 65, 25));
        pnlEstado.add(txtFechaDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, -1, 25));
        pnlEstado.add(txtFechaHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 85, -1, 25));

        lblDesde.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        lblDesde.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDesde.setText("Desde");
        pnlEstado.add(lblDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 65, 25));

        lblHasta.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        lblHasta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblHasta.setText("Hasta");
        pnlEstado.add(lblHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 85, 65, 25));

        cmboRemision.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboRemision, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 10, 370, 25));

        btnGenerarExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/visualizar.png"))); // NOI18N
        btnGenerarExcel.setToolTipText("Generar archivo Excel");
        btnGenerarExcel.setFocusable(false);
        btnGenerarExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarExcelActionPerformed(evt);
            }
        });
        pnlEstado.add(btnGenerarExcel, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 85, 25, 25));

        chkReimpresion.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
        chkReimpresion.setText("Reimpresion");
        chkReimpresion.setToolTipText("");
        chkReimpresion.setOpaque(false);
        pnlEstado.add(chkReimpresion, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 85, 100, 25));

        chkRemision.setToolTipText("");
        chkRemision.setOpaque(false);
        pnlEstado.add(chkRemision, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 20, 25));

        add(pnlEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 490, 130));
    }// </editor-fold>//GEN-END:initComponents

    private void btnGenerarExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarExcelActionPerformed
        try {
            generarExcel();
        } catch(Exception e) {
            javax.swing.JOptionPane.showMessageDialog(null, "No se puede generar el achivo excel");
        }
    }//GEN-LAST:event_btnGenerarExcelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnGenerarExcel;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JCheckBox chkReimpresion;
    private javax.swing.JCheckBox chkRemision;
    private javax.swing.JComboBox cmboRemision;
    private javax.swing.JLabel lblDesde;
    private javax.swing.JLabel lblHasta;
    private javax.swing.JLabel lblRemision;
    private javax.swing.JPanel pnlEstado;
    private panel.fecha txtFechaDesde;
    private panel.fecha txtFechaHasta;
    private javax.swing.JFormattedTextField txtfIdEstadoAhorro;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimir();
            }
        });
    }

    private void setEnabled() {
        this.cmbRemision.setEnabled(true);
        this.txtFechaDesde.setEnabled(true);
        this.txtFechaHasta.setEnabled(true);
        this.btnGenerarExcel.setEnabled(true);
        this.btnImprimir.setEnabled(true);
        this.chkReimpresion.setSelected(false);
    }
    
    private void iniciarComponentes() {
        mnt.visualizarBarra(false);
        this.btnGenerarExcel.setVisible(true);
        this.btnImprimir.setVisible(true);
        this.cmbRemision = new utilitario.comComboBox(this.cmboRemision, this.lblRemision, "SELECT id, localidad||' - '||nombre||' '||apellido AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwrepresentante1)+" WHERE destinatario='TRUE' ORDER BY localidad, nombre, apellido", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        
        this.btnImprimir.setToolTipText("Imprimir Reporte Remisión");
        this.cmbRemision.setToolTipText("Localidad y Representante");
        this.txtFechaDesde.setToolTipText("Fecha de emisión desde");
        this.txtFechaHasta.setToolTipText("Fecha hasta");
        this.chkReimpresion.setToolTipText("Volver a imprimir remision hecha");
    }
    
    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro+sfiltroFormulario, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }
    
    public void refrescarComponente() {
        this.cmbRemision.refresh();
    }

    
    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }
    
    
    public void asignarpanel(generico.intPanel panel) {
        this.panel = panel;
    }    
    
    private void generarExcel() throws java.io.IOException {
        String sfiltroestado=" AND estadocheque = 'IMPRESO'";
        String sfiltrofecha="";
        String sfiltrorepresentante="";
        String sfiltroestadoentrega="";
        int total = 0;
        
        //validación de fechas
        if (this.txtFechaDesde.getFecha()==null || this.txtFechaHasta.getFecha()==null) {
            javax.swing.JOptionPane.showMessageDialog(null, "Debe introducir un rango de fecha válido.");
            return;
        }
        if (utilitario.utiFecha.obtenerDiferenciaDia(this.txtFechaDesde.getFecha(), this.txtFechaHasta.getFecha())<0) {
            javax.swing.JOptionPane.showMessageDialog(null, "Debe introducir un rango de fecha válido.");
            return;
        }
        if (utilitario.utiFecha.obtenerDiferenciaDia(this.txtFechaDesde.getFecha(), this.txtFechaHasta.getFecha())>=0) { //fecha válida
            sfiltrofecha = " AND fechaemisioncheque BETWEEN "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        //si se imprime por primera vez tomará en cuenta el estado pendiente
        if (!this.chkReimpresion.isSelected()){
            sfiltroestadoentrega+=" AND idestadoentrega="+mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "PENDIENTE");
        }        
        if (this.cmbRemision.getIdSeleccionado()==0){
            javax.swing.JOptionPane.showMessageDialog(null, "Debe seleccionar un referente");
            return;
        } else {
            sfiltrorepresentante=" AND idremision="+this.cmbRemision.getIdSeleccionado();
            mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco."+generico.vista.getNombre(generico.vista.vwordenpago2), sfiltroestado+sfiltrofecha+sfiltrorepresentante+sfiltroestadoentrega, " ORDER BY id"));
            System.out.println(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco."+generico.vista.getNombre(generico.vista.vwordenpago2), sfiltroestado+sfiltrofecha+sfiltrorepresentante+sfiltroestadoentrega, " ORDER BY id"));
            try {
                 this.mnt.intPrincipal.pnlEstado.conexion.rs.last();
                 total = this.mnt.intPrincipal.pnlEstado.conexion.rs.getRow();
            } catch (Exception e) {
            }
            if (total==0) {
                 javax.swing.JOptionPane.showMessageDialog(null, "El documento no tiene páginas");
                 return;
            }
            
            String rutaArchivoCabecera = System.getProperty("user.home")+"/Remision.XLS"; //definir la ruta para almacenar el archivo
            java.io.File archivoXLS = new java.io.File(rutaArchivoCabecera);  //la variable conternedora de tipo archivo
            if(archivoXLS.exists()) archivoXLS.delete();  
            archivoXLS.createNewFile();                   //reemplaza el archivo definido si ya existe
            org.apache.poi.hssf.usermodel.HSSFWorkbook libro = new org.apache.poi.hssf.usermodel.HSSFWorkbook();  //creacion del libro excel
            java.io.FileOutputStream archivo = new java.io.FileOutputStream(archivoXLS);  //creacion de archivo de salida
            org.apache.poi.hssf.usermodel.HSSFSheet hoja = libro.createSheet("Remision");   //creacion de la hoja de excel
            org.apache.poi.hssf.usermodel.HSSFRow fila = hoja.createRow(0);   //inserta una nueva fila  inicia en 0
            org.apache.poi.hssf.usermodel.HSSFCell celda;
            String[] Encabezado = new String[]{"Nº", "Cargo", "Monto", "Nº Nota Crédito", "Beneficiario", "Observaciones"};  
            for(int c=0;c<6;c++){
                celda = fila.createCell(c); celda.setCellValue(Encabezado[c]);
            }
            try {
                this.mnt.intPrincipal.pnlEstado.conexion.rs.beforeFirst();
                for(int f=1;f<total+1;f++) {
                    this.mnt.intPrincipal.pnlEstado.conexion.rs.next();
                    fila = hoja.createRow(f);
                    celda = fila.createCell(0); celda.setCellValue(mnt.intPrincipal.pnlEstado.conexion.rs.getInt("numerocheque"));            
                    celda = fila.createCell(1); celda.setCellValue(mnt.intPrincipal.pnlEstado.conexion.rs.getString("banco"));            
                    celda = fila.createCell(2); celda.setCellValue(mnt.intPrincipal.pnlEstado.conexion.rs.getDouble("montocheque"));            
                    celda = fila.createCell(3); celda.setCellValue(mnt.intPrincipal.pnlEstado.conexion.rs.getInt("numeronota"));            
                    celda = fila.createCell(4); celda.setCellValue(mnt.intPrincipal.pnlEstado.conexion.rs.getString("descripcion"));            
                }
            } catch (Exception e) {               
            }
            libro.write(archivo);
            archivo.close();
            java.awt.Desktop.getDesktop().open(archivoXLS);
            
            //actualiza entrega de cheque a enviado solo en caso de ser pendiente
            if (!this.chkReimpresion.isSelected()){ 
                String sentencia = "";
                utilitario.conConexion con = new utilitario.conConexion();
                con.setHostBaseDatos(generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
                con.setConexion(true, null, null);
                try {
                    mnt.intPrincipal.pnlEstado.conexion.rs.beforeFirst(); //ACTUALIZA TABLA ENTREGACHEQUE
                    for (int j = 0; j < total;j++) {
                         mnt.intPrincipal.pnlEstado.conexion.rs.next();
                         sentencia+="UPDATE banco.entregacheque SET fechaenvio = "+utilitario.utiFecha.getFechaGuardadoMac(new java.util.Date())+",";
                         sentencia+=" idestado = "+con.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "ENVIADO")+",";
                         sentencia+=" intermediario = "+utilitario.utiCadena.getTextoGuardado(this.cmbRemision.getDescripcionSeleccionado());
                         sentencia+=" WHERE idcheque = "+mnt.intPrincipal.pnlEstado.conexion.rs.getInt("idchequeemitido");
                         sentencia+=" AND idestado = "+con.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "PENDIENTE")+";\n";
                    }
                    mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(sentencia);                    
                } catch (Exception e){
                    System.out.println(e);
                } System.out.println(sentencia);               
                con.setConexion(false, null, null);
            }
        }
    }

    
    private void imprimir() {
        String sfiltroestado=" AND estadocheque = 'IMPRESO'";
        String sfiltrofecha="";
        String sfiltrorepresentante="";
        String stextoFiltro ="";
        String sfiltroestadoentrega="";
        int total = 0;
        
        //validación de fechas
        if (this.txtFechaDesde.getFecha()==null || this.txtFechaHasta.getFecha()==null) {
            javax.swing.JOptionPane.showMessageDialog(null, "Debe introducir un rango de fecha válido.");
            return;
        }
        if (utilitario.utiFecha.obtenerDiferenciaDia(this.txtFechaDesde.getFecha(), this.txtFechaHasta.getFecha())<0) {
            javax.swing.JOptionPane.showMessageDialog(null, "Debe introducir un rango de fecha válido.");
            return;
        }
        if (utilitario.utiFecha.obtenerDiferenciaDia(this.txtFechaDesde.getFecha(), this.txtFechaHasta.getFecha())>=0) { //fecha válida
            sfiltrofecha = " AND fechaemisioncheque BETWEEN "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            stextoFiltro += "FECHA EMISION DESDE: "+utilitario.utiNumero.convertirToString(this.txtFechaDesde.getFecha().getDate()+100).substring(1)+"/"+utilitario.utiFecha.getNumeroMes(this.txtFechaDesde.getFecha())+"/"+utilitario.utiFecha.getAnho(this.txtFechaDesde.getFecha())+" HASTA: "+utilitario.utiNumero.convertirToString(this.txtFechaHasta.getFecha().getDate()+100).substring(1)+"/"+utilitario.utiFecha.getNumeroMes(this.txtFechaHasta.getFecha())+"/"+utilitario.utiFecha.getAnho(this.txtFechaHasta.getFecha());
        }
        //si se imprime por primera vez tomará en cuenta el estado pendiente
        if (!this.chkReimpresion.isSelected()){
            sfiltroestadoentrega+=" AND idestadoentrega="+mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "PENDIENTE");
        }        
        //si se elige un representante o todos
        String sentencia = "";
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        utilitario.conConexion con = new utilitario.conConexion();
        con.setHostBaseDatos(generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);                    
        if (this.cmbRemision.getIdSeleccionado()==0){
            for (int i=1;i<this.cmbRemision.mod.getSize(); i++){
                sfiltrorepresentante=" AND idremision="+this.cmbRemision.mod.getEntidad(i).getId();
                mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco."+generico.vista.getNombre(generico.vista.vwordenpago2), sfiltroestado+sfiltrofecha+sfiltrorepresentante+sfiltroestadoentrega, " ORDER BY id"));
                System.out.println(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco."+generico.vista.getNombre(generico.vista.vwordenpago2), sfiltroestado+sfiltrofecha+sfiltrorepresentante+sfiltroestadoentrega, " ORDER BY id"));
                try {
                    this.mnt.intPrincipal.pnlEstado.conexion.rs.last();
                    total = this.mnt.intPrincipal.pnlEstado.conexion.rs.getRow();
                } catch (Exception e) {
                }
                if (total>0) {
                    if (!rep.imprimir(generico.reporte.repbRemision, mnt.intPrincipal.pnlEstado.conexion.rs, null, null, null, "REMISION DE DOCUMENTOS AL INTERIOR", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
                            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
                    }
                    //actualiza entrega de cheque a enviado solo en caso de ser pendiente
                    if (!this.chkReimpresion.isSelected()){ 
                        con.setConexion(true, null, null);
                        try {
                            mnt.intPrincipal.pnlEstado.conexion.rs.beforeFirst(); //ACTUALIZA TABLA ENTREGACHEQUE
                            for (int j = 0; j < total;j++) {
                                mnt.intPrincipal.pnlEstado.conexion.rs.next();
                                sentencia+="UPDATE banco.entregacheque SET fechaenvio = "+utilitario.utiFecha.getFechaGuardadoMac(new java.util.Date())+",";
                                sentencia+=" idestado = "+con.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "ENVIADO")+",";
                                sentencia+=" intermediario = "+utilitario.utiCadena.getTextoGuardado(this.cmbRemision.mod.getEntidad(i).getDescripcion());
                                sentencia+=" WHERE idcheque = "+mnt.intPrincipal.pnlEstado.conexion.rs.getInt("idchequeemitido");
                                sentencia+=" AND idestado = "+con.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "PENDIENTE")+";\n";
                            }
                            System.out.println(sentencia);
                            con.ejecutarSentencia(sentencia);                    
                        } catch (Exception e){
                        }
                        con.setConexion(false, null, null);
                    }
                }
            }
        } else {
            sfiltrorepresentante=" AND idremision="+this.cmbRemision.getIdSeleccionado();
            mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco."+generico.vista.getNombre(generico.vista.vwordenpago2), sfiltroestado+sfiltrofecha+sfiltrorepresentante+sfiltroestadoentrega, " ORDER BY id"));
            System.out.println(mnt.intPrincipal.pnlEstado.conexion.getConsulta("banco."+generico.vista.getNombre(generico.vista.vwordenpago2), sfiltroestado+sfiltrofecha+sfiltrorepresentante+sfiltroestadoentrega, " ORDER BY id"));
            try {
                 this.mnt.intPrincipal.pnlEstado.conexion.rs.last();
                 total = this.mnt.intPrincipal.pnlEstado.conexion.rs.getRow();
            } catch (Exception e) {
            }
            if (total==0) {
                 javax.swing.JOptionPane.showMessageDialog(null, "El documento no tiene páginas");
                 return;
            }
            if (!rep.imprimir(generico.reporte.repbRemision, mnt.intPrincipal.pnlEstado.conexion.rs, null, null, null, "REMISION DE DOCUMENTOS AL INTERIOR", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
                        javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
            }
            //actualiza entrega de cheque a enviado solo en caso de ser pendiente
            if (!this.chkReimpresion.isSelected()){ 
                con.setConexion(true, null, null);
                try {
                    mnt.intPrincipal.pnlEstado.conexion.rs.beforeFirst(); //ACTUALIZA TABLA ENTREGACHEQUE
                    for (int j = 0; j < total;j++) {
                         mnt.intPrincipal.pnlEstado.conexion.rs.next();
                         sentencia+="UPDATE banco.entregacheque SET fechaenvio = "+utilitario.utiFecha.getFechaGuardadoMac(new java.util.Date())+",";
                         sentencia+=" idestado = "+con.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "ENVIADO")+",";
                         sentencia+=" intermediario = "+utilitario.utiCadena.getTextoGuardado(this.cmbRemision.getDescripcionSeleccionado());
                         sentencia+=" WHERE idcheque = "+mnt.intPrincipal.pnlEstado.conexion.rs.getInt("idchequeemitido");
                         sentencia+=" AND idestado = "+con.obtenerIdSubtipo("%ESTADO%ENTREGA%CHEQUE%", "PENDIENTE")+";\n";
                    }
                    System.out.println(sentencia);
                    con.ejecutarSentencia(sentencia);                    
                } catch (Exception e){
                }
                con.setConexion(false, null, null);
            }
         }
    }
    
    public void configurarPanel() {
        //especifico.intbRemision intbRemision = new especifico.intbRemision(mnt);
        //generico.intPanel panel = new generico.intPanel("Remisión de Documentos :: Impresión", intbRemision);
        //intbRemision.asignarpanel(panel);
        //panel.btnEstablecer.setVisible(false);
        //panel.abrir();
    }
}
