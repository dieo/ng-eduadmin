/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intBarrio.java
 *
 * Created on 03-abr-2012, 10:25:12
 */
package especifico;
/**
 *
 * @author Ing. Edison Martinez
 */
public class intExtractoCerradoCobradov3 extends javax.swing.JPanel {
    private generico.intMantenimiento mnt;
    private especifico.modExtractoCerradoCobradov3 mod = new especifico.modExtractoCerradoCobradov3();
    private especifico.intListaSocio intListaSocio;
    private especifico.intListaFuncionarioMSP intListaPersona;
    private especifico.entSocio entPersona = new especifico.entSocio();
    private especifico.entLugarLaboral entLabor = new especifico.entLugarLaboral();
    private String svista="", sfiltro="", sorden="";
    private String scedula;
    
    /** Creates new form intBarrio 
     @param mnt.
     */
    public intExtractoCerradoCobradov3(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        initComponents();
        mod.setTable(this.tblDetallePeriodo);
        this.iniciarComponentes();
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblPeriodoInicial = new javax.swing.JLabel();
        lblRubro = new javax.swing.JLabel();
        lblGiraduria = new javax.swing.JLabel();
        lblSocio = new javax.swing.JLabel();
        txtfSocio = new javax.swing.JFormattedTextField();
        txtfCedulaNombreApellido = new javax.swing.JFormattedTextField();
        scpDetallePeriodo = new javax.swing.JScrollPane();
        tblDetallePeriodo = new javax.swing.JTable();
        txtfGiraduria = new javax.swing.JFormattedTextField();
        txtfRubro = new javax.swing.JFormattedTextField();
        btnImprimir = new javax.swing.JButton();
        lblEstado = new javax.swing.JLabel();
        txtfEstado = new javax.swing.JFormattedTextField();
        btnSocio = new javax.swing.JButton();
        btnFuncionarioMSP = new javax.swing.JButton();
        spnMesInicial = new javax.swing.JSpinner();
        spnAnoInicial = new javax.swing.JSpinner();
        lblPeriodoFinal = new javax.swing.JLabel();
        spnMesFinal = new javax.swing.JSpinner();
        spnAnoFinal = new javax.swing.JSpinner();
        btnGenerar = new javax.swing.JButton();
        chkDetalle = new javax.swing.JCheckBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(811, 475));
        setPreferredSize(new java.awt.Dimension(811, 475));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblPeriodoInicial.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblPeriodoInicial.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPeriodoInicial.setText("Inicial");
        add(lblPeriodoInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 55, 25));

        lblRubro.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblRubro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblRubro.setText("Rubro/Sue.");
        add(lblRubro, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 60, 65, 25));

        lblGiraduria.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblGiraduria.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblGiraduria.setText("Giraduría");
        add(lblGiraduria, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 55, 25));

        lblSocio.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblSocio.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSocio.setText("Socio");
        add(lblSocio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 55, 25));

        txtfSocio.setForeground(new java.awt.Color(0, 102, 204));
        txtfSocio.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtfSocio.setToolTipText("");
        txtfSocio.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfSocio.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfSocio.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        add(txtfSocio, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, 70, 25));

        txtfCedulaNombreApellido.setBorder(null);
        txtfCedulaNombreApellido.setForeground(new java.awt.Color(0, 102, 204));
        txtfCedulaNombreApellido.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtfCedulaNombreApellido.setFocusable(false);
        txtfCedulaNombreApellido.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtfCedulaNombreApellido.setOpaque(false);
        add(txtfCedulaNombreApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, 470, 25));

        scpDetallePeriodo.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scpDetallePeriodo.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tblDetallePeriodo.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        scpDetallePeriodo.setViewportView(tblDetallePeriodo);

        add(scpDetallePeriodo, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 85, 790, 380));

        txtfGiraduria.setForeground(new java.awt.Color(0, 102, 204));
        txtfGiraduria.setToolTipText("");
        txtfGiraduria.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfGiraduria.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfGiraduria.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(txtfGiraduria, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 60, 320, 25));

        txtfRubro.setForeground(new java.awt.Color(0, 102, 204));
        txtfRubro.setToolTipText("");
        txtfRubro.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfRubro.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfRubro.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(txtfRubro, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 60, 305, 25));

        btnImprimir.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimir.setToolTipText("Imprimir por Socio");
        btnImprimir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimir.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimir.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimir.setOpaque(false);
        btnImprimir.setPreferredSize(new java.awt.Dimension(65, 35));
        add(btnImprimir, new org.netbeans.lib.awtextra.AbsoluteConstraints(365, 35, 25, 25));

        lblEstado.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEstado.setText("Estado");
        add(lblEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 35, 65, 25));

        txtfEstado.setForeground(new java.awt.Color(0, 102, 204));
        txtfEstado.setToolTipText("");
        txtfEstado.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfEstado.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfEstado.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(txtfEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 35, 305, 25));

        btnSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/persona22.png"))); // NOI18N
        btnSocio.setToolTipText("Obtener Socio");
        btnSocio.setOpaque(false);
        add(btnSocio, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, 25, 25));

        btnFuncionarioMSP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/persona22.png"))); // NOI18N
        btnFuncionarioMSP.setToolTipText("Obtener Funcionario MSP");
        btnFuncionarioMSP.setOpaque(false);
        add(btnFuncionarioMSP, new org.netbeans.lib.awtextra.AbsoluteConstraints(165, 10, 25, 25));

        spnMesInicial.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        spnMesInicial.setModel(new javax.swing.SpinnerListModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
        add(spnMesInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 35, 45, 25));

        spnAnoInicial.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        spnAnoInicial.setModel(new javax.swing.SpinnerListModel(new String[] {"2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090", "2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099", "2100"}));
        add(spnAnoInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 35, 60, 25));

        lblPeriodoFinal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPeriodoFinal.setText("Final");
        add(lblPeriodoFinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 35, 40, 25));

        spnMesFinal.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        spnMesFinal.setModel(new javax.swing.SpinnerListModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
        add(spnMesFinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(235, 35, 45, 25));

        spnAnoFinal.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        spnAnoFinal.setModel(new javax.swing.SpinnerListModel(new String[] {"2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090", "2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099", "2100"}));
        add(spnAnoFinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 35, 60, 25));

        btnGenerar.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnGenerar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/generar20.png"))); // NOI18N
        btnGenerar.setToolTipText("Generar");
        btnGenerar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGenerar.setMaximumSize(new java.awt.Dimension(65, 35));
        btnGenerar.setMinimumSize(new java.awt.Dimension(65, 35));
        btnGenerar.setOpaque(false);
        btnGenerar.setPreferredSize(new java.awt.Dimension(65, 35));
        add(btnGenerar, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 35, 25, 25));

        chkDetalle.setText("Detalle");
        chkDetalle.setToolTipText("");
        chkDetalle.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        chkDetalle.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        chkDetalle.setOpaque(false);
        add(chkDetalle, new org.netbeans.lib.awtextra.AbsoluteConstraints(705, 5, 90, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFuncionarioMSP;
    private javax.swing.JButton btnGenerar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnSocio;
    private javax.swing.JCheckBox chkDetalle;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblGiraduria;
    private javax.swing.JLabel lblPeriodoFinal;
    private javax.swing.JLabel lblPeriodoInicial;
    private javax.swing.JLabel lblRubro;
    private javax.swing.JLabel lblSocio;
    private javax.swing.JScrollPane scpDetallePeriodo;
    private javax.swing.JSpinner spnAnoFinal;
    private javax.swing.JSpinner spnAnoInicial;
    private javax.swing.JSpinner spnMesFinal;
    private javax.swing.JSpinner spnMesInicial;
    private javax.swing.JTable tblDetallePeriodo;
    private javax.swing.JFormattedTextField txtfCedulaNombreApellido;
    private javax.swing.JFormattedTextField txtfEstado;
    private javax.swing.JFormattedTextField txtfGiraduria;
    private javax.swing.JFormattedTextField txtfRubro;
    private javax.swing.JFormattedTextField txtfSocio;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        txtfSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode()==10) { // Enter
                    ((java.awt.Component) evt.getSource()).transferFocus();
                    entPersona = new especifico.entSocio();
                    entLabor = new especifico.entLugarLaboral();
                    mod.removerTodo();
                    txtfCedulaNombreApellido.setText(intListaSocio.configurarPanelSeleccion(((javax.swing.JTextField)evt.getSource()).getText()));
                    if (intListaSocio.mod.getRowCount()>0) {
                        obtenerDatoSocio();
                    }
                    //mod.removerTodo();
                }
            }
        });
        btnSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entPersona = new especifico.entSocio();
                entLabor = new especifico.entLugarLaboral();
                mod.removerTodo();
                txtfCedulaNombreApellido.setText(intListaSocio.configurarPanelSeleccion(txtfSocio.getText()));
                if (intListaSocio.mod.getRowCount()>0) {
                    obtenerDatoSocio();
                }
                //mod.removerTodo();
            }
        });
        btnGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (intListaSocio.mod.getRowCount()>0) {
                    // HILO DE PROGRESO
                    final generico.intCargaProceso proceso = new generico.intCargaProceso();
                    proceso.setVisible(true);
                    final javax.swing.SwingWorker worker = new javax.swing.SwingWorker(){
                        @Override
                        protected Object doInBackground() throws Exception {
                            try {
                                proceso.prgProgreso.setMaximum(1000);
                                proceso.prgProgreso.setIndeterminate(true);
                                Thread.currentThread().sleep(2);
                                obtenerOperacion();
                            }catch (Exception ex) {
                                System.out.println(ex);
                            }  
                            proceso.prgProgreso.setIndeterminate(false);
                            proceso.setVisible(false);
                            return null;
                        }
                    };     
        	    worker.execute();                    
                    // FIN DE HILO DE PROGRESO
                    //obtenerOperacion();
                }
            }
        });
        btnFuncionarioMSP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entPersona = new especifico.entSocio();
                entLabor = new especifico.entLugarLaboral();
                mod.removerTodo();
                txtfCedulaNombreApellido.setText(intListaPersona.configurarPanelSeleccion(txtfSocio.getText()));
                if (intListaPersona.mod.getRowCount()>0) {
                    // HILO DE PROGRESO
                    final generico.intCargaProceso proceso = new generico.intCargaProceso();
                    proceso.setVisible(true);
                    final javax.swing.SwingWorker worker = new javax.swing.SwingWorker(){
                        @Override
                        protected Object doInBackground() throws Exception {
                            try {
                                proceso.prgProgreso.setMaximum(1000);
                                proceso.prgProgreso.setIndeterminate(true);
                                Thread.currentThread().sleep(2);
                                obtenerDatoFuncionarioMSP();
                            }catch (Exception ex) {
                                System.out.println(ex);
                            }  
                            proceso.prgProgreso.setIndeterminate(false);
                            proceso.setVisible(false);
                            return null;
                        }
                    };     
        	    worker.execute();                    
                    // FIN DE HILO DE PROGRESO
                    //obtenerDatoFuncionarioMSP();
                }
            }
        });
        tblDetallePeriodo.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) { 
                if (evt.getClickCount() == 2) { // doble click
                    if (mod.getTable().getSelectedRow() < 0) return; // no tiene datos
                    especifico.intListaDetalle lista = new especifico.intListaDetalle(mnt);
                }
            }
        });
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirSocio();
            }
        });
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfEstado.setEnabled(false);
        this.txtfGiraduria.setEnabled(false);
        this.txtfRubro.setEnabled(false);
    }

    private void iniciarComponentes() {
        mnt.visualizarBarra(false);
        intListaSocio = new especifico.intListaSocio(mnt);
        intListaSocio.mod.insertar(new especifico.entListaSocio());
        intListaPersona = new especifico.intListaFuncionarioMSP(mnt);
        intListaPersona.mod.insertar(new especifico.entListaFuncionarioMSP());
        this.spnMesInicial.setValue(utilitario.utiFecha.convertirToStringDMA(new java.util.Date()).substring(3,5));
        this.spnAnoInicial.setValue(utilitario.utiFecha.convertirToStringDMA(new java.util.Date()).substring(6,10));
        this.spnMesFinal.setValue(utilitario.utiFecha.convertirToStringDMA(new java.util.Date()).substring(3,5));
        this.spnAnoFinal.setValue(utilitario.utiFecha.convertirToStringDMA(new java.util.Date()).substring(6,10));
    }

    public void cargarEditor() {
    }

    public boolean insertar() {
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        return true;
    }

    public boolean eliminar() {
        return true;
    }

    public boolean cancelar() {
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia("")) {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
    }
    
    private void getInterfaz(utilitario.entFechaCierre ent) {
    }
    
    private void setInterfaz(especifico.entExtractoCerradoCobrado ent) {
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

    private void obtenerDatoSocio() {
        especifico.intSocio socio = new especifico.intSocio(mnt);
        especifico.intLugarLaboral labor = new especifico.intLugarLaboral(mnt);
        socio.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwsocio1), " AND id="+intListaSocio.mod.getEntidad(0).getId(), "");
        socio.obtenerDato();
        if (socio.mod.getRowCount()<=0) return;
        labor.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwlugarlaboral1), " AND idsocio="+intListaSocio.mod.getEntidad(0).getId()+" AND activo='TRUE'", "");
        labor.obtenerDato();
        if (labor.mod.getRowCount()<=0) return;
        this.txtfEstado.setText(socio.mod.getEntidad(0).getEstado());
        this.txtfGiraduria.setText(labor.mod.getEntidad(0).getGiraduria());
        this.txtfRubro.setText(labor.mod.getEntidad(0).getRubro());   
        scedula = intListaSocio.mod.getEntidad(0).getCedula();
        entPersona.setCedula(socio.mod.getEntidad(0).getCedula());
        entPersona.setNumeroSocio(socio.mod.getEntidad(0).getNumeroSocio());
        entPersona.setNombre(socio.mod.getEntidad(0).getNombre());
        entPersona.setApellido(socio.mod.getEntidad(0).getApellido());
        entPersona.setEstado(this.txtfEstado.getText());
        entLabor.setGiraduria(this.txtfGiraduria.getText());
        entLabor.setRubro(this.txtfRubro.getText());   
    }

    private void obtenerDatoFuncionarioMSP() {
        intListaSocio.mod.getEntidad(0).setId(0);
        if (intListaPersona.mod.getRowCount()<=0) return;
        String ssentencia = "SELECT * FROM "+generico.vista.getNombre(generico.vista.vwfuncionariomsp1)+" WHERE cedula='"+intListaPersona.mod.getEntidad(0).getCedula()+"'";
        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(ssentencia);
        try {
            mnt.intPrincipal.pnlEstado.conexion.rs.beforeFirst();
            mnt.intPrincipal.pnlEstado.conexion.rs.next();
            this.txtfGiraduria.setText(mnt.intPrincipal.pnlEstado.conexion.rs.getString("giraduria"));
            this.txtfRubro.setText(mnt.intPrincipal.pnlEstado.conexion.rs.getString("rubro"));
            entPersona.setCedula(mnt.intPrincipal.pnlEstado.conexion.rs.getString("cedula"));
            entPersona.setNombre(mnt.intPrincipal.pnlEstado.conexion.rs.getString("nombreapellido"));
            entPersona.setApellido("");
        } catch (Exception e) {
            this.txtfGiraduria.setText("");
            this.txtfRubro.setText("");
        }
        this.txtfEstado.setText("NO SOCIO");
        scedula = intListaPersona.mod.getEntidad(0).getCedula();
        entPersona.setEstado(this.txtfEstado.getText());
        entPersona.setNumeroSocio(0);
        entLabor.setGiraduria(this.txtfGiraduria.getText());
        entLabor.setRubro(this.txtfRubro.getText());
    }

    private void obtenerOperacion() {
        //if (intListaSocio.mod.getRowCount()<=0) return;
        java.util.Date dfechaInicial = utilitario.utiFecha.convertirToDateDMA("01/"+this.spnMesInicial.getValue().toString()+"/"+this.spnAnoInicial.getValue().toString());
        java.util.Date dfechaFinal = utilitario.utiFecha.convertirToDateDMA("01/"+this.spnMesFinal.getValue().toString()+"/"+this.spnAnoFinal.getValue().toString());
        java.util.ArrayList lst = this.crearRango(dfechaInicial, dfechaFinal);
        if (this.chkDetalle.isSelected()) {
            System.out.println(new especifico.entExtractoCerradoCobradov3().getConsultaCerradoDetalle(dfechaInicial, dfechaFinal, intListaSocio.mod.getEntidad(0).getId(), lst.get(0).toString(), lst.get(1).toString(), lst.get(2).toString(), lst.get(3).toString(), lst.get(4).toString(), lst.get(5).toString(), scedula));
            if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(new especifico.entExtractoCerradoCobradov3().getConsultaCerradoDetalle(dfechaInicial, dfechaFinal, intListaSocio.mod.getEntidad(0).getId(), lst.get(0).toString(), lst.get(1).toString(), lst.get(2).toString(), lst.get(3).toString(), lst.get(4).toString(), lst.get(5).toString(), scedula))) { // obtiene datos de la base de datos
                mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
            }
        }   else {    
            System.out.println(new especifico.entExtractoCerradoCobradov3().getConsultaCerrado(dfechaInicial, dfechaFinal, intListaSocio.mod.getEntidad(0).getId(), lst.get(0).toString(), lst.get(1).toString(), lst.get(2).toString(), lst.get(3).toString(), lst.get(4).toString(), lst.get(5).toString(), scedula));
            if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(new especifico.entExtractoCerradoCobradov3().getConsultaCerrado(dfechaInicial, dfechaFinal, intListaSocio.mod.getEntidad(0).getId(), lst.get(0).toString(), lst.get(1).toString(), lst.get(2).toString(), lst.get(3).toString(), lst.get(4).toString(), lst.get(5).toString(), scedula))) { // obtiene datos de la base de datos
                mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
            }
        }
        mod.getTable().updateUI();
    }

    private java.util.ArrayList crearRango(java.util.Date dfechaInicial, java.util.Date dfechaFinal) {
        java.util.ArrayList<String> lst = new java.util.ArrayList<String>();
        String ssentencia = "SELECT * from fechacierre where fechaproceso >= '"+utilitario.utiFecha.getUltimoDia(dfechaInicial)+"' and fechaproceso <= '"+utilitario.utiFecha.getUltimoDia(dfechaFinal)+"' order by fechaproceso";
        String srango1 = "";
        String srango2 = "";
        String srango3 = "";
        String srango4 = "";
        String srango5 = "";
        String srango6 = "";
        utilitario.conConexion con = new utilitario.conConexion();
        con.setHostBaseDatos(generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        con.setConexion(true, null, null);
        con.ejecutarSentencia(ssentencia);
        try {
            con.rs.beforeFirst();
            while (con.rs.next()) {
                if (!srango1.isEmpty()) srango1 += "OR ";
                if (!srango2.isEmpty()) srango2 += "OR ";
                if (!srango3.isEmpty()) srango3 += "OR ";
                if (!srango4.isEmpty()) srango4 += "OR ";
                if (!srango5.isEmpty()) srango5 += "OR ";
                if (!srango6.isEmpty()) srango6 += "OR ";

                srango1 += "ci.fechacierre = '"+con.rs.getDate("fechaproceso")+"' "; 
                srango2 += "i.fechaaaplicar = '"+con.rs.getDate("fechaproceso")+"' "; 
                srango3 += "(i.fechaaaplicar = '"+con.rs.getDate("fechaproceso")+"' and ci.fechacierre = '"+con.rs.getDate("fechaproceso")+"') "; 
                srango4 += "(i.fechaaaplicar >= '"+con.rs.getDate("fechafin")+"' and i.fechaaaplicar <= '"+con.rs.getDate("fechainicio")+"' and ci.fechacierre = '"+con.rs.getDate("fechaproceso")+"') "; 
                srango5 += "TO_CHAR(di.fechavencimiento,'YYYYMM00') = TO_CHAR('"+con.rs.getDate("fechaproceso")+"'::date, 'YYYYMM00') "; 
                srango6 += "ag.periodo = '"+con.rs.getDate("fechaproceso")+"' "; 
            }
        } catch (Exception e) {}
        lst.add(srango1);
        lst.add(srango2);
        lst.add(srango3);
        lst.add(srango4);
        lst.add(srango5);
        lst.add(srango6);
        con.setConexion(false, null, null);
        return lst;
    }
  
    private void imprimirSocio() {
        java.util.Date dfechaInicial = utilitario.utiFecha.convertirToDateDMA("01/"+this.spnMesInicial.getValue().toString()+"/"+this.spnAnoInicial.getValue().toString());
        java.util.Date dfechaFinal = utilitario.utiFecha.convertirToDateDMA("01/"+this.spnMesFinal.getValue().toString()+"/"+this.spnAnoFinal.getValue().toString());
        String stextoFiltro = "";
        stextoFiltro += "PERIODO: " + utilitario.utiFecha.convertirToStringDMA(utilitario.utiFecha.getUltimoDia(dfechaInicial)) + " a " + utilitario.utiFecha.convertirToStringDMA(utilitario.utiFecha.getUltimoDia(dfechaFinal));
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (this.chkDetalle.isSelected()) {
            if (!rep.imprimir(generico.reporte.repExtractoCerradoCobradov3, entPersona, entLabor, mod, null, "Extracto Cerrado Cobrado Detallado", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
                javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
            }    
        } else {        
            if (!rep.imprimir(generico.reporte.repExtractoCerradoCobradov3, entPersona, entLabor, mod, null, "Extracto Cerrado Cobrado", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
                javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
            }    
        }
    }
    
    private void imprimirFuncionario() {
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repExtractoCerradoCobradoFuncMSP, intListaPersona.mod.getEntidad(0), mod, null, null, "Extracto Cerrado Cobrado", "", mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }
    
}
