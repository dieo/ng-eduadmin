/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intEntidad.java
 *
 * Created on 06-abr-2012, 9:29:19
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intTalonario extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modTalonario mod = new especifico.modTalonario();
    private especifico.intDetalleTalonario intBoleta;
    private especifico.intbCuentaTalonario intCuenta;
    private utilitario.comComboBox cmbTipoTalonario;
    private utilitario.comComboBox cmbEmisorTalonario;
    private utilitario.comComboBox cmbSucursal;
    private utilitario.comComboBox cmbExpedicion;
    private final generico.mnuMantenimientoDetalle mnuDetalle = new generico.mnuMantenimientoDetalle();
    private String svista="", sfiltro="", sorden="", sfiltroFormulario="";
    
    /** Creates new form intEntidad 
     @param mnt.
     @param tbl.
     */
    public intTalonario(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }
    
    public intTalonario(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        this.mod.setTable(new javax.swing.JTable());
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        cmboTipoTalonario = new javax.swing.JComboBox();
        txtfNumeroInicial = new javax.swing.JFormattedTextField();
        txtfNumeroFinal = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblTipoTalonario = new javax.swing.JLabel();
        lblNumeroInicial = new javax.swing.JLabel();
        lblNumeroFinal = new javax.swing.JLabel();
        lblFechaVencimiento = new javax.swing.JLabel();
        try {
            txtFechaVencimiento = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        txtfEstadoTalonario = new javax.swing.JFormattedTextField();
        txtfSerieInicial = new javax.swing.JFormattedTextField();
        lblFechaOperacion = new javax.swing.JLabel();
        try {
            txtFechaOperacion = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblEmisorTalonario = new javax.swing.JLabel();
        lblSecuencia = new javax.swing.JLabel();
        txtfSecuencia = new javax.swing.JFormattedTextField();
        lblBoleta = new javax.swing.JLabel();
        scpBoleta = new javax.swing.JScrollPane();
        tblBoleta = new javax.swing.JTable();
        lblLeyenda = new javax.swing.JLabel();
        txtfDescripcion = new javax.swing.JFormattedTextField();
        lblDescripcion = new javax.swing.JLabel();
        cmboEmisorTalonario = new javax.swing.JComboBox();
        cmboSucursal = new javax.swing.JComboBox();
        cmboExpedicion = new javax.swing.JComboBox();
        txtfNumeroSucursal = new javax.swing.JFormattedTextField();
        txtfNumeroExpedicion = new javax.swing.JFormattedTextField();
        txtfSerieFinal = new javax.swing.JFormattedTextField();
        btnOtorgarPermiso = new javax.swing.JButton();
        btnAnular = new javax.swing.JButton();
        txtfDescripcionCuenta = new javax.swing.JFormattedTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(555, 415));
        setPreferredSize(new java.awt.Dimension(555, 415));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setToolTipText("");
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 110, 25));

        cmboTipoTalonario.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(cmboTipoTalonario, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 35, 460, 25));

        txtfNumeroInicial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        txtfNumeroInicial.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfNumeroInicial.setToolTipText("");
        txtfNumeroInicial.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfNumeroInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 135, 90, 25));

        txtfNumeroFinal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfNumeroFinal.setToolTipText("");
        txtfNumeroFinal.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfNumeroFinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 160, 90, 25));

        lblId.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 80, 25));

        lblTipoTalonario.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblTipoTalonario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTipoTalonario.setText("Tipo Talonario");
        add(lblTipoTalonario, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 80, 25));

        lblNumeroInicial.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblNumeroInicial.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumeroInicial.setText("Número Inicial");
        add(lblNumeroInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 135, 80, 25));

        lblNumeroFinal.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblNumeroFinal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumeroFinal.setText("Número Final");
        add(lblNumeroFinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 70, 25));

        lblFechaVencimiento.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaVencimiento.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaVencimiento.setText("Fecha Vencim.");
        add(lblFechaVencimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, 25));
        add(txtFechaVencimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, -1, -1));

        txtfEstadoTalonario.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        txtfEstadoTalonario.setForeground(new java.awt.Color(0, 102, 204));
        txtfEstadoTalonario.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtfEstadoTalonario.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfEstadoTalonario.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfEstadoTalonario.setFocusable(false);
        txtfEstadoTalonario.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtfEstadoTalonario.setOpaque(false);
        add(txtfEstadoTalonario, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 10, 220, 25));

        txtfSerieInicial.setToolTipText("");
        txtfSerieInicial.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfSerieInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 135, 30, 25));

        lblFechaOperacion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaOperacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaOperacion.setText("Fecha Operac.");
        add(lblFechaOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 85, 80, 25));
        add(txtFechaOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 85, -1, -1));

        lblEmisorTalonario.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblEmisorTalonario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEmisorTalonario.setText("Emisor Talonario");
        add(lblEmisorTalonario, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 80, 25));

        lblSecuencia.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblSecuencia.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSecuencia.setText("Secuencia");
        add(lblSecuencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 135, 70, 25));

        txtfSecuencia.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfSecuencia.setToolTipText("");
        txtfSecuencia.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        add(txtfSecuencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 135, 110, 25));

        lblBoleta.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        lblBoleta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblBoleta.setText("Boletas");
        add(lblBoleta, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 210, 90, 20));

        scpBoleta.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tblBoleta.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        scpBoleta.setViewportView(tblBoleta);

        add(scpBoleta, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 550, 180));

        lblLeyenda.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        lblLeyenda.setForeground(new java.awt.Color(153, 153, 153));
        lblLeyenda.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        add(lblLeyenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 210, 330, 20));

        txtfDescripcion.setBorder(null);
        txtfDescripcion.setFocusable(false);
        txtfDescripcion.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        txtfDescripcion.setOpaque(false);
        add(txtfDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 185, 460, 25));

        lblDescripcion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblDescripcion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDescripcion.setText("Descripción");
        add(lblDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 185, 80, 25));

        cmboEmisorTalonario.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(cmboEmisorTalonario, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 460, 25));

        cmboSucursal.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboSucursal, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 135, 50, 25));

        cmboExpedicion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboExpedicion, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 135, 50, 25));

        try {
            txtfNumeroSucursal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfNumeroSucursal.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtfNumeroSucursal.setToolTipText("");
        txtfNumeroSucursal.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfNumeroSucursal, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 160, 50, 25));

        try {
            txtfNumeroExpedicion.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfNumeroExpedicion.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtfNumeroExpedicion.setToolTipText("");
        txtfNumeroExpedicion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfNumeroExpedicion, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 160, 50, 25));

        txtfSerieFinal.setToolTipText("");
        txtfSerieFinal.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfSerieFinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 160, 30, 25));

        btnOtorgarPermiso.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnOtorgarPermiso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/aprobar20.png"))); // NOI18N
        btnOtorgarPermiso.setToolTipText("Otorgar Permiso");
        btnOtorgarPermiso.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOtorgarPermiso.setMaximumSize(new java.awt.Dimension(65, 35));
        btnOtorgarPermiso.setMinimumSize(new java.awt.Dimension(65, 35));
        btnOtorgarPermiso.setOpaque(false);
        btnOtorgarPermiso.setPreferredSize(new java.awt.Dimension(65, 35));
        add(btnOtorgarPermiso, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 10, 25, 25));

        btnAnular.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnAnular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/anular20-2.png"))); // NOI18N
        btnAnular.setToolTipText("Anular");
        btnAnular.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAnular.setMaximumSize(new java.awt.Dimension(65, 35));
        btnAnular.setMinimumSize(new java.awt.Dimension(65, 35));
        btnAnular.setOpaque(false);
        btnAnular.setPreferredSize(new java.awt.Dimension(65, 35));
        add(btnAnular, new org.netbeans.lib.awtextra.AbsoluteConstraints(525, 10, 25, 25));

        txtfDescripcionCuenta.setBorder(null);
        txtfDescripcionCuenta.setForeground(new java.awt.Color(0, 102, 204));
        txtfDescripcionCuenta.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtfDescripcionCuenta.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfDescripcionCuenta.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfDescripcionCuenta.setFocusable(false);
        txtfDescripcionCuenta.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        txtfDescripcionCuenta.setOpaque(false);
        add(txtfDescripcionCuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 90, 360, 25));
    }// </editor-fold>//GEN-END:initComponents

    private void scpListadoBoletaMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == 3) { // click derecho
            java.awt.Point posMouse = java.awt.MouseInfo.getPointerInfo().getLocation();
            this.mnuDetalle.show(mnt.intPrincipal, posMouse.x-mnt.intPrincipal.getLocation().x, posMouse.y-mnt.intPrincipal.getLocation().y);
        }
    }

    private void tblListadoBoletaMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == 3) { // click derecho
            java.awt.Point posMouse = java.awt.MouseInfo.getPointerInfo().getLocation();
            this.mnuDetalle.show(mnt.intPrincipal, posMouse.x-mnt.intPrincipal.getLocation().x, posMouse.y-mnt.intPrincipal.getLocation().y);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnular;
    private javax.swing.JButton btnOtorgarPermiso;
    private javax.swing.JComboBox cmboEmisorTalonario;
    private javax.swing.JComboBox cmboExpedicion;
    private javax.swing.JComboBox cmboSucursal;
    private javax.swing.JComboBox cmboTipoTalonario;
    private javax.swing.JLabel lblBoleta;
    private javax.swing.JLabel lblDescripcion;
    private javax.swing.JLabel lblEmisorTalonario;
    private javax.swing.JLabel lblFechaOperacion;
    private javax.swing.JLabel lblFechaVencimiento;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblLeyenda;
    private javax.swing.JLabel lblNumeroFinal;
    private javax.swing.JLabel lblNumeroInicial;
    private javax.swing.JLabel lblSecuencia;
    private javax.swing.JLabel lblTipoTalonario;
    private javax.swing.JScrollPane scpBoleta;
    public javax.swing.JTable tblBoleta;
    private panel.fecha txtFechaOperacion;
    private panel.fecha txtFechaVencimiento;
    private javax.swing.JFormattedTextField txtfDescripcion;
    private javax.swing.JFormattedTextField txtfDescripcionCuenta;
    private javax.swing.JFormattedTextField txtfEstadoTalonario;
    private javax.swing.JFormattedTextField txtfId;
    private javax.swing.JFormattedTextField txtfNumeroExpedicion;
    private javax.swing.JFormattedTextField txtfNumeroFinal;
    private javax.swing.JFormattedTextField txtfNumeroInicial;
    private javax.swing.JFormattedTextField txtfNumeroSucursal;
    private javax.swing.JFormattedTextField txtfSecuencia;
    private javax.swing.JFormattedTextField txtfSerieFinal;
    private javax.swing.JFormattedTextField txtfSerieInicial;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        // DE MOUSE Y TECLADO AL JTABLE
        scpBoleta.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) { scpListadoBoletaMouseClicked(evt); }
        });
        tblBoleta.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) { tblListadoBoletaMouseClicked(evt); }
        });
        tblBoleta.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) { configurarPanelBoleta(generico.mnuMantenimientoDetalle.getNombre(evt.getKeyCode())); }
        });
        // MENU CONTEXTUAL
        this.mnuDetalle.mnuInsertar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelBoleta(((javax.swing.JMenuItem)evt.getSource()).getText()); }
        });
        this.mnuDetalle.mnuModificar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelBoleta(((javax.swing.JMenuItem)evt.getSource()).getText()); }
        });
        this.mnuDetalle.mnuVisualizar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelBoleta(((javax.swing.JMenuItem)evt.getSource()).getText()); }
        });
        this.mnuDetalle.mnuEliminar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelBoleta(((javax.swing.JMenuItem)evt.getSource()).getText()); }
        });
        //================================================
        btnAnular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelAnular(); }
        });
        btnOtorgarPermiso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelOtorgarPermiso(); }
        });
        this.cmboSucursal.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                txtfNumeroSucursal.setText(cmbSucursal.getDescripcionSeleccionado());
            }
        });
        this.cmboExpedicion.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                txtfNumeroExpedicion.setText(cmbExpedicion.getDescripcionSeleccionado());
            }
        });
        txtfSerieInicial.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtfSerieFinal.setText(txtfSerieInicial.getText());
            }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfSerieInicial);
        uti.agregarEventoFocusGained(this.txtfNumeroInicial);
        uti.agregarEventoFocusGained(this.txtfNumeroFinal);
        uti.agregarEventoKeyReleased(this.txtfSerieInicial);
        uti.agregarEventoKeyReleased(this.txtfNumeroInicial);
        uti.agregarEventoKeyReleased(this.txtfNumeroFinal);
        uti.agregarEventoKeyPressed(this.tblBoleta);
    }

    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.cmbTipoTalonario.setEnabled(bmodo);
        this.cmbEmisorTalonario.setEnabled(bmodo);
        this.txtFechaOperacion.setEnabled(bmodo);
        this.txtFechaVencimiento.setEnabled(bmodo);
        this.txtfSerieInicial.setEnabled(bmodo);
        this.txtfSerieFinal.setEnabled(false);
        this.cmbSucursal.setEnabled(bmodo);
        this.cmbExpedicion.setEnabled(bmodo);
        this.txtfNumeroSucursal.setEnabled(false);
        this.txtfNumeroExpedicion.setEnabled(false);
        this.txtfNumeroInicial.setEnabled(bmodo);
        this.txtfNumeroFinal.setEnabled(bmodo);
        this.txtfSecuencia.setEnabled(false);
        this.mnuDetalle.setEnabled(bmodo);
        this.lblLeyenda.setText(utilitario.utiGeneral.leyenda(bmodo));
        this.componenteEspecial(!bmodo);
    }

    private void componenteEspecial(boolean bmodo) {
        this.btnOtorgarPermiso.setEnabled(bmodo);
        this.btnAnular.setEnabled(bmodo);
    }
    
    private void iniciarComponentes() {
        intBoleta = new especifico.intDetalleTalonario(mnt, this.tblBoleta);
        intCuenta = new especifico.intbCuentaTalonario(mnt);
        this.cmbTipoTalonario = new utilitario.comComboBox(this.cmboTipoTalonario, this.lblTipoTalonario, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwsubtipo)+" WHERE tipo LIKE '%TIPO%TALONARIO%' ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbEmisorTalonario = new utilitario.comComboBox(this.cmboEmisorTalonario, this.lblEmisorTalonario, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwemisortalonario1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbSucursal = new utilitario.comComboBox(this.cmboSucursal, this.lblNumeroInicial, "SELECT id, numerosucursal AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwsucursal1)+" ORDER BY numerosucursal", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbExpedicion = new utilitario.comComboBox(this.cmboExpedicion, this.lblNumeroInicial, "SELECT id, numeroexpedicion AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwpuntoexpedicion1)+" ORDER BY numeroexpedicion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);

        this.txtfSerieInicial.setDocument(new generico.modFormatoCadena(this.txtfSerieInicial, especifico.entTalonario.LONGITUD_SERIE, true));
        this.txtfNumeroInicial.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfNumeroFinal.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfId.setToolTipText(especifico.entTalonario.TEXTO_ID);
        this.cmbEmisorTalonario.setToolTipText(especifico.entTalonario.TEXTO_EMISOR_TALONARIO);
        this.txtFechaOperacion.setToolTipText(especifico.entTalonario.TEXTO_FECHA_OPERACION);
        this.txtfSerieInicial.setToolTipText(especifico.entTalonario.TEXTO_SERIE);
        this.txtfNumeroInicial.setToolTipText(especifico.entTalonario.TEXTO_NUMERO_INICIAL);
        this.txtfNumeroFinal.setToolTipText(especifico.entTalonario.TEXTO_NUMERO_FINAL);
        this.txtFechaVencimiento.setToolTipText(especifico.entTalonario.TEXTO_FECHA_VENCIMIENTO);
        this.txtfSecuencia.setToolTipText(especifico.entTalonario.TEXTO_SECUENCIA);
        this.cmbTipoTalonario.cmb.setToolTipText(especifico.entTalonario.TEXTO_TIPO_TALONARIO);
        this.txtfDescripcion.setToolTipText(especifico.entTalonario.TEXTO_DESCRIPCION);
    }
    
    public void cargarEditor() {
        try {
            intCuenta.setVistaFiltroOrden("banco."+generico.vista.getNombre(generico.vista.vwcuentatalonario1), " AND idtalonario="+mod.getEntidad().getId(), "");
            intCuenta.obtenerDato();
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) { 
            this.setInterfaz(new especifico.entTalonario());
        }
        try {
            intBoleta.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwdetalletalonario1), " AND idtalonario="+mod.getEntidad().getId(), " ORDER BY secuencia");
            intBoleta.obtenerDato();
            intBoleta.mod.getTable().updateUI();
        } catch (Exception e) {
        }
    }
    
    public boolean insertar() {
        especifico.entTalonario ent = new especifico.entTalonario();
        ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("talonario")); // establece nuevo id (definitivo)
        ent.setIdEstado(mnt.intPrincipal.pnlEstado.defecto.getIdEstadoTalonario());
        ent.setEstado(mnt.intPrincipal.pnlEstado.conexion.obtenerDescripcionSubtipo(ent.getIdEstado()));
        ent.setIdEmisorTalonario(mnt.intPrincipal.pnlEstado.defecto.getIdEmisorTalonario());
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar() {
        // inactivo o activo
        if (mod.getEntidad().getIdEstado()==mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "ACTIVO") || mod.getEntidad().getIdEstado()==mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "INACTIVO")) {
            return true;
        }
        return false;
    }
    
    public boolean establecer() {
        especifico.entTalonario ent = mod.getEntidad().copiar(new especifico.entTalonario()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        int iidCheque = ((java.math.BigDecimal)mnt.intPrincipal.pnlEstado.conexion.getCampo(generico.vista.getNombre(generico.vista.vwsubtipo), "tipo LIKE 'TIPO TALONARIO' AND descripcion LIKE 'CHEQUE'", "id")).intValue();
        int iidOrdenCredito = ((java.math.BigDecimal)mnt.intPrincipal.pnlEstado.conexion.getCampo(generico.vista.getNombre(generico.vista.vwsubtipo), "tipo LIKE 'TIPO TALONARIO' AND descripcion LIKE 'ORDEN CREDITO'", "id")).intValue();
        if (!ent.esValidoRegistrar(iidCheque, iidOrdenCredito)) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        this.sfiltro+=" OR id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }
    
    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(new especifico.entTalonario().getCampoTalonarioActualizaEstado(mnt.intPrincipal.pnlEstado.conexion)); // actualiza el estado de TERMINADO y VENCIDO
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }
    
    public boolean obtenerDato() {
        mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(new especifico.entTalonario().getCampoTalonarioActualizaEstado(mnt.intPrincipal.pnlEstado.conexion)); // actualiza el estado de TERMINADO y VENCIDO
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro+sfiltroFormulario, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
        this.cmbEmisorTalonario.refresh();
    }
    
    private void getInterfaz(especifico.entTalonario ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setIdTipoTalonario(this.cmbTipoTalonario.getIdSeleccionado());
        ent.setTipoTalonario(this.cmbTipoTalonario.getDescripcionSeleccionado());
        ent.setIdEstado(ent.getIdEstado());
        ent.setEstado(ent.getEstado());
        ent.setIdEmisorTalonario(this.cmbEmisorTalonario.getIdSeleccionado());
        ent.setEmisorTalonario(this.cmbEmisorTalonario.getDescripcionSeleccionado());
        ent.setFechaOperacion(this.txtFechaOperacion.getFecha());
        ent.setSerie(this.txtfSerieInicial.getText().trim());
        ent.setNumeroSucursal(this.cmbSucursal.getDescripcionSeleccionado());
        ent.setNumeroExpedicion(this.cmbExpedicion.getDescripcionSeleccionado());
        ent.setNumeroInicial(utilitario.utiNumero.convertirToInt(this.txtfNumeroInicial.getText()));
        ent.setNumeroFinal(utilitario.utiNumero.convertirToInt(this.txtfNumeroFinal.getText()));
        ent.setFechaVencimiento(this.txtFechaVencimiento.getFecha());
        ent.setSecuencia(utilitario.utiNumero.convertirToInt(this.txtfSecuencia.getText()));
        if (ent.getSerie().length()>0) ent.setSerie(ent.getSerie()+" ");
        ent.setDescripcion(ent.getTipoTalonario()+" ("+ent.getSerie()+ent.getNumeroSucursal()+"-"+ent.getNumeroExpedicion()+"-"+ent.getNumeroInicial()+"/"+ent.getNumeroFinal()+")");
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entTalonario ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.cmbTipoTalonario.setId(ent.getIdTipoTalonario());}
        catch (Exception e) {this.cmbTipoTalonario.setId(-1);}
        try                 {this.txtfEstadoTalonario.setText(ent.getEstado());}
        catch (Exception e) {this.txtfEstadoTalonario.setText("");}
        try                 {this.cmbEmisorTalonario.setId(ent.getIdEmisorTalonario());}
        catch (Exception e) {this.cmbEmisorTalonario.setId(0);}
        try                 {this.txtFechaOperacion.setFecha(ent.getFechaOperacion());}
        catch (Exception e) {this.txtFechaOperacion.setFechaVacia();}
        try                 {this.txtfSerieInicial.setText(ent.getSerie());}
        catch (Exception e) {this.txtfSerieInicial.setText("");}
        try                 {this.txtfSerieFinal.setText(ent.getSerie());}
        catch (Exception e) {this.txtfSerieFinal.setText("");}
        try                 {this.cmbSucursal.setId(this.buscarItem(this.cmbSucursal, ent.getNumeroSucursal()));}
        catch (Exception e) {this.cmbSucursal.setId(0);}
        try                 {this.cmbExpedicion.setId(this.buscarItem(this.cmbExpedicion, ent.getNumeroExpedicion()));}
        catch (Exception e) {this.cmbExpedicion.setId(0);}
        try                 {this.txtfNumeroInicial.setValue(ent.getNumeroInicial());}
        catch (Exception e) {this.txtfNumeroInicial.setValue(0);}
        try                 {this.txtfNumeroFinal.setValue(ent.getNumeroFinal());}
        catch (Exception e) {this.txtfNumeroFinal.setValue(0);}
        try                 {this.txtFechaVencimiento.setFecha(ent.getFechaVencimiento());}
        catch (Exception e) {this.txtFechaVencimiento.setFechaVacia();}
        try                 {this.txtfSecuencia.setValue(ent.getSecuencia());}
        catch (Exception e) {this.txtfSecuencia.setValue(0);}
        try                 {this.txtfDescripcion.setText(ent.getDescripcion());}
        catch (Exception e) {this.txtfDescripcion.setText("");}
        try                 {this.txtfDescripcionCuenta.setText(intCuenta.mod.getEntidad(0).getCuentaBanco());}
        catch (Exception e) {this.txtfDescripcionCuenta.setText("");}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

    public void setFiltroFormulario(String sfiltroFormulario) {
        this.sfiltroFormulario = sfiltroFormulario;
    }
    
    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================
 
    private void configurarPanelBoleta(String sfuncion) {
        if (sfuncion.equals("")) return; // presiono cualquier tecla
        if (sfuncion.equals(generico.mnuMantenimientoDetalle.eliminar) || sfuncion.equals(generico.mnuMantenimientoDetalle.visualizar)) intBoleta.setEnabled(false);
        else intBoleta.setEnabled(true);
        if (sfuncion.equals(generico.mnuMantenimientoDetalle.insertar)) {
            intBoleta.insertar();
            intBoleta.mod.getTable().setRowSelectionInterval(intBoleta.mod.getTable().getRowCount()-1, intBoleta.mod.getTable().getRowCount()-1);
        }
        if (intBoleta.mod.getTable().getSelectedRow()>=0) intBoleta.cargarEditor();
        generico.intPanel panel = new generico.intPanel("Boleta Inválida :: Mantenimiento :: " + sfuncion, intBoleta);
        int posicion = utilitario.utiCadena.getPosicion(intBoleta.mod.getTable());
        while (true && intBoleta.mod.getTable().getSelectedRow()>=0) {
            panel.abrir();
            if (panel.establecido()) {
                if (sfuncion.equals(generico.mnuMantenimientoDetalle.eliminar)) { if (utilitario.utiAviso.advertirEliminacion()) { if (intBoleta.eliminar()) { if (intBoleta.guardar()) { intBoleta.obtenerDato(); break; } } }
                } else if (sfuncion.equals(generico.mnuMantenimientoDetalle.visualizar)) { break;
                } else { if (intBoleta.establecer()) { if (intBoleta.guardar()) { intBoleta.obtenerDato(); break; } } }
            } else {
                if (!sfuncion.equals(generico.mnuMantenimientoDetalle.eliminar) && !sfuncion.equals(generico.mnuMantenimientoDetalle.visualizar)) { if (utilitario.utiAviso.advertirCancelacion()) { if (intBoleta.cancelar()) break; }
                } else { intBoleta.cancelar(); break; }
            }
        }
        intBoleta.setEnabled(true);
        intBoleta.mod.getTable().updateUI();
        utilitario.utiCadena.setPosicion(intBoleta.mod.getTable(), posicion);
    }

    private void configurarPanelAnular() {
        especifico.intTalonarioAnular intTalonario = new especifico.intTalonarioAnular(mnt, null);
        if (mod.getRowCount() <= 0) return;
        intTalonario.mod.insertar(mod.getEntidad().copiar(new especifico.entTalonario()));
        intTalonario.mod.getTable().setRowSelectionInterval(0,0);
        intTalonario.btnTalonario.setEnabled(false);
        intTalonario.setEnabled(true);
        if (!intTalonario.modificar()) {
            intTalonario.setEnabled(false);
            intTalonario.cargarEditor();
        }
        int posicion = utilitario.utiCadena.getPosicion(mod.getTable());
        generico.intPanel panel = new generico.intPanel("Anulación Talonario :: Mantenimiento :: " + generico.mnuMantenimientoDetalle.modificar, intTalonario);
        int iidActivo = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "ACTIVO");
        int iidInactivo = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "INACTIVO");
        if (mod.getEntidad().getIdEstado() != iidActivo && mod.getEntidad().getIdEstado() != iidInactivo) panel.btnEstablecer.setEnabled(false);
        while (true) {
            panel.abrir();
            if (panel.establecido()) {
                if (intTalonario.isEnabled()) { if (intTalonario.establecer()) { if (intTalonario.guardar()) { if (this.obtenerDato()) break; } } }
                else break;
            } else {
                if (intTalonario.isEnabled()) { if (intTalonario.cancelar()) break; }
                else break;
            }
        }
        intTalonario.setEnabled(true);
        mod.getTable().updateUI();
        utilitario.utiCadena.setPosicion(mod.getTable(), posicion);
    }
    
    private void configurarPanelOtorgarPermiso() {
        especifico.intPermisoPerfil permiso = new especifico.intPermisoPerfil(mnt);
        permiso.permiso[permiso.item6] = "OTORGAR PERMISO";
        if (!permiso.tienePermiso(this.getClass().getSimpleName(), permiso.item6)) return;
        especifico.intTalonarioOtorgarPermiso intPermiso = new especifico.intTalonarioOtorgarPermiso(mnt, null);
        if (mod.getRowCount() <= 0) return;
        intPermiso.mod.insertar(mod.getEntidad().copiar(new especifico.entTalonario()));
        intPermiso.mod.getTable().setRowSelectionInterval(0,0);
        intPermiso.setEnabled(true);
        if (!intPermiso.modificar()) intPermiso.setEnabled(false);
        intPermiso.cargarEditor();
        intPermiso.obtenerDatoPerfilUsuario();
        int posicion = utilitario.utiCadena.getPosicion(mod.getTable());
        generico.intPanel panel = new generico.intPanel("Permiso Talonario :: Mantenimiento :: " + generico.mnuMantenimientoDetalle.modificar, intPermiso);
        int iidActivo = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "ACTIVO");
        int iidInactivo = mnt.intPrincipal.pnlEstado.conexion.obtenerIdSubtipo("%ESTADO%TALONARIO%", "INACTIVO");
        panel.btnEstablecer.setEnabled(false);
        if (mod.getEntidad().getIdEstado()==iidActivo || mod.getEntidad().getIdEstado()==iidInactivo) {
            panel.btnEstablecer.setEnabled(true);
        }
        while (true) {
            panel.abrir();
            if (panel.establecido()) {
                if (intPermiso.isEnabled()) { if (intPermiso.establecer()) { if (intPermiso.guardar()) { if (this.obtenerDato()) break; } } }
                else break;
            } else {
                if (intPermiso.isEnabled()) { if (intPermiso.cancelar()) break; }
                else break;
            }
        }
        intPermiso.setEnabled(true);
        mod.getTable().updateUI();
        utilitario.utiCadena.setPosicion(mod.getTable(), posicion);
    }
    
    private int buscarItem(utilitario.comComboBox cmb, String sbuscado) {
        for (int i=0; i<cmb.mod.getSize(); i++) {
            if (cmb.mod.getEntidad(i).getDescripcion().equals(sbuscado)) return cmb.mod.getEntidad(i).getId();
        }
        return 0;
    }

}
