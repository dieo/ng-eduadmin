/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intEntidad.java
 *
 * Created on 06-abr-2012, 9:29:19
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intDesautorizacion extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modDesautorizacion mod = new especifico.modDesautorizacion();
    private especifico.intListaSocio intListaSocio;
    private utilitario.comComboBox cmbCuenta;
    private String svista="", sfiltro="", sorden="";
    
    /** Creates new form intEntidad 
     @param mnt.
     @param tbl.
     */
    public intDesautorizacion(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }
    
    public intDesautorizacion(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        this.mod.setTable(new javax.swing.JTable());
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        txtfMotivo = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblMotivo = new javax.swing.JLabel();
        lblCuenta = new javax.swing.JLabel();
        chkActivo = new javax.swing.JCheckBox();
        lblLeyendaArticulo = new javax.swing.JLabel();
        lblSocio = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        try {
            txtFecha = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        cmboCuenta = new javax.swing.JComboBox();
        txtfSocio = new javax.swing.JFormattedTextField();
        btnSocio = new javax.swing.JButton();
        txtfCedulaNombreApellido = new javax.swing.JFormattedTextField();
        lblActivo = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        setMinimumSize(new java.awt.Dimension(595, 140));
        setPreferredSize(new java.awt.Dimension(595, 140));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setToolTipText("");
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 110, 25));

        txtfMotivo.setToolTipText("");
        txtfMotivo.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfMotivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 490, 25));

        lblId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 80, 25));

        lblMotivo.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblMotivo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMotivo.setText("Motivo");
        add(lblMotivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 80, 25));

        lblCuenta.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblCuenta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCuenta.setText("Cuenta");
        add(lblCuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 80, 25));

        chkActivo.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        chkActivo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkActivo.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        chkActivo.setOpaque(false);
        add(chkActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 10, 25, 25));

        lblLeyendaArticulo.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        lblLeyendaArticulo.setForeground(new java.awt.Color(153, 153, 153));
        lblLeyendaArticulo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        add(lblLeyendaArticulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 120, 330, 20));

        lblSocio.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblSocio.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSocio.setText("Socio");
        add(lblSocio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 80, 25));

        lblFecha.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblFecha.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFecha.setText("Fecha");
        add(lblFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 85, 80, 25));
        add(txtFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 85, -1, -1));

        cmboCuenta.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboCuenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 490, 25));

        txtfSocio.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtfSocio.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfSocio, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, 85, 25));

        btnSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/ok20.png"))); // NOI18N
        btnSocio.setToolTipText("Obtener Socio");
        btnSocio.setOpaque(false);
        add(btnSocio, new org.netbeans.lib.awtextra.AbsoluteConstraints(185, 35, 25, 25));

        txtfCedulaNombreApellido.setBorder(null);
        txtfCedulaNombreApellido.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtfCedulaNombreApellido.setFocusable(false);
        txtfCedulaNombreApellido.setFont(new java.awt.Font("Arial Narrow", 1, 12)); // NOI18N
        txtfCedulaNombreApellido.setOpaque(false);
        add(txtfCedulaNombreApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 35, 370, 25));

        lblActivo.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblActivo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblActivo.setText("Activo");
        add(lblActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 10, 80, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSocio;
    private javax.swing.JCheckBox chkActivo;
    private javax.swing.JComboBox cmboCuenta;
    private javax.swing.JLabel lblActivo;
    private javax.swing.JLabel lblCuenta;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblLeyendaArticulo;
    private javax.swing.JLabel lblMotivo;
    private javax.swing.JLabel lblSocio;
    private panel.fecha txtFecha;
    private javax.swing.JFormattedTextField txtfCedulaNombreApellido;
    private javax.swing.JFormattedTextField txtfId;
    private javax.swing.JFormattedTextField txtfMotivo;
    private javax.swing.JFormattedTextField txtfSocio;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        txtfSocio.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode()==10) { // Enter
                    ((java.awt.Component) evt.getSource()).transferFocus();
                    txtfCedulaNombreApellido.setText(intListaSocio.configurarPanelSeleccion(((javax.swing.JTextField)evt.getSource()).getText()));
                }
            }
        });
        btnSocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) { 
                txtfCedulaNombreApellido.setText(intListaSocio.configurarPanelSeleccion(txtfSocio.getText()));
            }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfMotivo);
        uti.agregarEventoKeyReleased(this.txtfMotivo);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.txtfSocio.setEnabled(bmodo);
        this.btnSocio.setEnabled(bmodo);
        this.cmbCuenta.setEnabled(bmodo);
        this.txtFecha.setEnabled(bmodo);
        this.txtfMotivo.setEnabled(bmodo);
        this.chkActivo.setEnabled(bmodo);
    }
    
    private void iniciarComponentes() {
        String scondicion1 = " AND idtipocuenta<>"+mnt.intPrincipal.pnlEstado.conexion.obtenerId("tipocuenta", "descripcion LIKE 'AHORRO%PROGRAMADO'");
        String scondicion2 = " AND idtipocuenta<>"+mnt.intPrincipal.pnlEstado.conexion.obtenerId("tipocuenta", "descripcion LIKE 'ORDEN%CREDITO'");
        String scondicion3 = " AND idtipocuenta<>"+mnt.intPrincipal.pnlEstado.conexion.obtenerId("tipocuenta", "descripcion LIKE 'PRESTAMO%MUTUAL'");
        String scondicion4 = " AND idtipocuenta<>"+mnt.intPrincipal.pnlEstado.conexion.obtenerId("tipocuenta", "descripcion LIKE 'PRESTAMO%FINANCIERA'");
        this.cmbCuenta = new utilitario.comComboBox(this.cmboCuenta, this.lblCuenta, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwcuenta1)+" WHERE 0=0"+scondicion1+scondicion2+scondicion3+scondicion4+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        intListaSocio = new especifico.intListaSocio(mnt);
        intListaSocio.mod.insertar(new especifico.entListaSocio());

        this.txtfMotivo.setDocument(new generico.modFormatoCadena(this.txtfMotivo, especifico.entDesautorizacion.LONGITUD_MOTIVO, true));
        this.txtfId.setToolTipText(especifico.entDesautorizacion.TEXTO_ID);
        this.cmbCuenta.setToolTipText(especifico.entDesautorizacion.TEXTO_CUENTA);
        this.txtfSocio.setToolTipText("Realiza búsqueda en Cédula, Número, Nombre y Apellido (presione Enter)");
        this.txtfCedulaNombreApellido.setToolTipText(especifico.entDesautorizacion.TEXTO_SOCIO);
        this.txtFecha.setToolTipText(especifico.entDesautorizacion.TEXTO_FECHA);
        this.txtfMotivo.setToolTipText(especifico.entDesautorizacion.TEXTO_MOTIVO);
        this.chkActivo.setToolTipText(especifico.entDesautorizacion.TEXTO_ACTIVO);
    }
    
    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) { 
            this.setInterfaz(new especifico.entDesautorizacion());
        }
    }
    
    public boolean insertar() {
        especifico.entDesautorizacion ent = new especifico.entDesautorizacion();
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        ent.setFecha(new java.util.Date());
        ent.setActivo(true);
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        especifico.entDesautorizacion ent = mod.getEntidad().copiar(new especifico.entDesautorizacion()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValido()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("desautorizacion"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }
    
    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
        this.cmbCuenta.refresh();
    }
    
    private void getInterfaz(especifico.entDesautorizacion ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setIdCuenta(this.cmbCuenta.getIdSeleccionado());
        ent.setCuenta(this.cmbCuenta.getDescripcionSeleccionado());
        ent.setIdSocio(intListaSocio.mod.getEntidad(0).getId());
        ent.setCedula(intListaSocio.mod.getEntidad(0).getCedula());
        ent.setFecha(this.txtFecha.getFecha());
        ent.setMotivo(this.txtfMotivo.getText());
        ent.setActivo(this.chkActivo.isSelected());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entDesautorizacion ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.cmbCuenta.setId(ent.getIdCuenta());}
        catch (Exception e) {this.cmbCuenta.setId(0);}
        try                 {this.txtfCedulaNombreApellido.setText(intListaSocio.obtenerNombreApellido(ent.getCedula(), ent.getNumeroSocio(), ent.getApellidoNombre(), ""));}
        catch (Exception e) {this.txtfCedulaNombreApellido.setText("");}
        try                 {this.txtFecha.setFecha(ent.getFecha());}
        catch (Exception e) {this.txtFecha.setFechaVacia();}
        try                 {this.txtfMotivo.setText(ent.getMotivo());}
        catch (Exception e) {this.txtfMotivo.setText("");}
        try                 {this.chkActivo.setSelected(ent.getActivo());}
        catch (Exception e) {this.chkActivo.setSelected(false);}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================

    public void imprimir() {
        especifico.intDesautorizacionImprimir intDesautorizacionImprimir = new especifico.intDesautorizacionImprimir(mnt);
        generico.intPanel panel = new generico.intPanel("Desautorización :: Impresión", intDesautorizacionImprimir);
        panel.btnEstablecer.setEnabled(false);
        panel.abrir();
    }

}
