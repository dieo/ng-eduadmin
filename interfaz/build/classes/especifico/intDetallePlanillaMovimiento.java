 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intRubro.java
 *
 * Created on 04-abr-2012, 18:23:29
 */
package especifico;
/*
 *
 * @author Ing. Edison Martinez
 */
public class intDetallePlanillaMovimiento extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modDetallePlanillaMovimiento mod = new especifico.modDetallePlanillaMovimiento();
    public especifico.modDetallePlanillaMovimiento2 modDet = new especifico.modDetallePlanillaMovimiento2();
    private utilitario.comComboBox cmbOperacion;
    private String svista="", sfiltro="", sfiltroFormulario="", sorden="";
    
    /** Creates new form intRubro 
     @param mnt.
     @param tbl.
     */
    public intDetallePlanillaMovimiento(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    public intDetallePlanillaMovimiento(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblOperacion = new javax.swing.JLabel();
        cmboOperacion = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        setMinimumSize(new java.awt.Dimension(580, 45));
        setPreferredSize(new java.awt.Dimension(580, 45));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblOperacion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblOperacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblOperacion.setText("Operación");
        add(lblOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 60, 25));

        cmboOperacion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboOperacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 490, 25));
    }// </editor-fold>//GEN-END:initComponents
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmboOperacion;
    private javax.swing.JLabel lblOperacion;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        utilitario.utiEvento uti = new utilitario.utiEvento();
    }

    @Override
    public final void setEnabled(boolean bmodo) {
        this.cmbOperacion.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.cmbOperacion = new utilitario.comComboBox(this.cmboOperacion, this.lblOperacion, "SELECT id, numerooperacion||' '||tipodocumento||' '||cedula||' '||nombre||' '||apellido AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwcontrolarchivo1)+" WHERE fechacancela IS NOT NULL ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);

        this.cmbOperacion.setToolTipText(especifico.entDetallePlanillaMovimiento.TEXTO_OPERACION);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) { 
            this.setInterfaz(new especifico.entDetallePlanillaMovimiento());
        }
    }
    
    public boolean insertar() {
        especifico.entDetallePlanillaMovimiento ent = new especifico.entDetallePlanillaMovimiento();
        especifico.intPlanillaMovimiento planilla = (especifico.intPlanillaMovimiento)new generico.clsPadre().getPadre(mod.getTable(), especifico.intPlanillaMovimiento.class);
        ent.setIdPlanilla(planilla.mod.getEntidad(planilla.mod.getTable().getSelectedRow()).getId()); // obtiene el id de analisis del modelo y asigna
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        especifico.entDetallePlanillaMovimiento ent = mod.getEntidad().copiar(new especifico.entDetallePlanillaMovimiento()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        especifico.intDetallePlanillaMovimiento archivo = new especifico.intDetallePlanillaMovimiento(mnt);
        archivo.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwdetalleplanillamovimiento1), " AND idoperacion="+ent.getIdOperacion()+" AND idplanilla="+ent.getIdPlanilla()+" AND id<>"+ent.getId(), "");
        archivo.obtenerDato();
        boolean esDuplicado = false;
        if (archivo.mod.getRowCount()>0) esDuplicado = true;
        if (!ent.esValido(esDuplicado)) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("detalleplanillamovimiento"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro+sfiltroFormulario, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
            modDet.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
    }

    private void getInterfaz(especifico.entDetallePlanillaMovimiento ent) {
        //ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setIdOperacion(this.cmbOperacion.getIdSeleccionado());
        ent.setOperacion(this.cmbOperacion.getDescripcionSeleccionado());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entDetallePlanillaMovimiento ent) {
        //try                 {this.txtfId.setValue(ent.getId());}
        //catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.cmbOperacion.setId(ent.getIdOperacion());}
        catch (Exception e) {this.cmbOperacion.setId(0);}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }
    
    public void setFiltroFormulario(String sfiltroFormulario) {
        if (!sfiltroFormulario.isEmpty()) this.sfiltroFormulario = sfiltroFormulario;
    }
    
    public void imprimir() {
    }
    
    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================

}
