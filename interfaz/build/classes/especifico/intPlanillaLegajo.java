/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intCiudad.java
 *
 * Created on 10/10/2011, 09:59:12 AM
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intPlanillaLegajo extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modPlanillaLegajo mod = new especifico.modPlanillaLegajo();
    private utilitario.comComboBox cmbTipoPlanilla;
    private String svista="", sfiltro="", sorden="";

    /** Creates new form intCiudad 
     @param mnt.
     @param tbl.
     */
    public intPlanillaLegajo(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    public intPlanillaLegajo(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        this.mod.setTable(new javax.swing.JTable());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        txtfDescripcion = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblDescripcion = new javax.swing.JLabel();
        try {
            txtFechaApertura = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblFechaApertura = new javax.swing.JLabel();
        try {
            txtFechaCierre = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblFechaCierre = new javax.swing.JLabel();
        txtfHoraApertura = new javax.swing.JFormattedTextField();
        txtfHoraCierre = new javax.swing.JFormattedTextField();
        lblHoraApertura = new javax.swing.JLabel();
        lblHoraCierre = new javax.swing.JLabel();
        chkActivo = new javax.swing.JCheckBox();
        cmboTipoPlanilla = new javax.swing.JComboBox();
        btnCerrarPlanilla = new javax.swing.JButton();
        lblNumeroPlanilla = new javax.swing.JLabel();
        txtfNumeroPlanilla = new javax.swing.JFormattedTextField();
        txtfSecuencia = new javax.swing.JFormattedTextField();
        txtfAno = new javax.swing.JFormattedTextField();
        btnObtenerSecuencia = new javax.swing.JButton();
        lblTipoPlanilla = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setMinimumSize(new java.awt.Dimension(465, 165));
        setPreferredSize(new java.awt.Dimension(465, 165));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setToolTipText("");
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 110, 25));

        txtfDescripcion.setForeground(new java.awt.Color(0, 102, 204));
        txtfDescripcion.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfDescripcion.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfDescripcion.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(txtfDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 370, 25));

        lblId.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 80, 25));

        lblDescripcion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblDescripcion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDescripcion.setText("Descripción");
        add(lblDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 80, 25));
        add(txtFechaApertura, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, -1, -1));

        lblFechaApertura.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaApertura.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaApertura.setText("Fecha Apertura");
        add(lblFechaApertura, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 110, 80, 25));
        add(txtFechaCierre, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 135, -1, -1));

        lblFechaCierre.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFechaCierre.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFechaCierre.setText("Fecha Cierre");
        add(lblFechaCierre, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 135, 80, 25));

        try {
            txtfHoraApertura.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfHoraApertura.setText("");
        txtfHoraApertura.setToolTipText("");
        txtfHoraApertura.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfHoraApertura, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 110, 50, 25));

        try {
            txtfHoraCierre.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfHoraCierre.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfHoraCierre, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 135, 50, 25));

        lblHoraApertura.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblHoraApertura.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblHoraApertura.setText("Hora Apertura");
        add(lblHoraApertura, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 110, 80, 25));

        lblHoraCierre.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblHoraCierre.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblHoraCierre.setText("Hora Cierre");
        add(lblHoraCierre, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 135, 80, 25));

        chkActivo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        chkActivo.setText("Activo");
        chkActivo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        chkActivo.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        chkActivo.setOpaque(false);
        add(chkActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 10, 70, 25));

        cmboTipoPlanilla.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboTipoPlanilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 85, 265, 25));

        btnCerrarPlanilla.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnCerrarPlanilla.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/anular20-2.png"))); // NOI18N
        btnCerrarPlanilla.setToolTipText("Cerrar Planilla");
        btnCerrarPlanilla.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCerrarPlanilla.setMaximumSize(new java.awt.Dimension(65, 35));
        btnCerrarPlanilla.setMinimumSize(new java.awt.Dimension(65, 35));
        btnCerrarPlanilla.setOpaque(false);
        btnCerrarPlanilla.setPreferredSize(new java.awt.Dimension(65, 35));
        add(btnCerrarPlanilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(435, 10, 25, 25));

        lblNumeroPlanilla.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblNumeroPlanilla.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumeroPlanilla.setText("Número Planilla");
        add(lblNumeroPlanilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 80, 25));

        txtfNumeroPlanilla.setForeground(new java.awt.Color(0, 102, 204));
        txtfNumeroPlanilla.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfNumeroPlanilla.setCaretColor(new java.awt.Color(0, 102, 204));
        txtfNumeroPlanilla.setDisabledTextColor(new java.awt.Color(0, 102, 204));
        txtfNumeroPlanilla.setFocusable(false);
        txtfNumeroPlanilla.setFont(new java.awt.Font("Tahoma", 1, 9)); // NOI18N
        add(txtfNumeroPlanilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 35, 110, 25));

        try {
            txtfSecuencia.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfSecuencia.setText("");
        txtfSecuencia.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfSecuencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 85, 50, 25));

        try {
            txtfAno.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfAno.setText("");
        txtfAno.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfAno, new org.netbeans.lib.awtextra.AbsoluteConstraints(355, 85, 30, 25));

        btnObtenerSecuencia.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnObtenerSecuencia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/ascendente20.png"))); // NOI18N
        btnObtenerSecuencia.setToolTipText("Obtener Secuencia");
        btnObtenerSecuencia.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnObtenerSecuencia.setMaximumSize(new java.awt.Dimension(65, 35));
        btnObtenerSecuencia.setMinimumSize(new java.awt.Dimension(65, 35));
        btnObtenerSecuencia.setOpaque(false);
        btnObtenerSecuencia.setPreferredSize(new java.awt.Dimension(65, 35));
        add(btnObtenerSecuencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(385, 85, 25, 25));

        lblTipoPlanilla.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblTipoPlanilla.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTipoPlanilla.setText("Tipo Planilla");
        add(lblTipoPlanilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 85, 80, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCerrarPlanilla;
    private javax.swing.JButton btnObtenerSecuencia;
    private javax.swing.JCheckBox chkActivo;
    private javax.swing.JComboBox cmboTipoPlanilla;
    private javax.swing.JLabel lblDescripcion;
    private javax.swing.JLabel lblFechaApertura;
    private javax.swing.JLabel lblFechaCierre;
    private javax.swing.JLabel lblHoraApertura;
    private javax.swing.JLabel lblHoraCierre;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblNumeroPlanilla;
    private javax.swing.JLabel lblTipoPlanilla;
    private panel.fecha txtFechaApertura;
    private panel.fecha txtFechaCierre;
    private javax.swing.JFormattedTextField txtfAno;
    private javax.swing.JFormattedTextField txtfDescripcion;
    private javax.swing.JFormattedTextField txtfHoraApertura;
    private javax.swing.JFormattedTextField txtfHoraCierre;
    private javax.swing.JFormattedTextField txtfId;
    private javax.swing.JFormattedTextField txtfNumeroPlanilla;
    private javax.swing.JFormattedTextField txtfSecuencia;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnCerrarPlanilla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelCierre(); }
        });
        btnObtenerSecuencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (cmbTipoPlanilla.getIdSeleccionado()<=0 || txtfAno.getText().isEmpty()) return;
                String ssentencia = "SELECT SUBSTR(codigo,4,4) AS secuencia FROM planillalegajo WHERE SUBSTR(codigo,1,1)='"+cmbTipoPlanilla.getDescripcionSeleccionado().substring(cmbTipoPlanilla.getDescripcionSeleccionado().length()-1)+"' AND SUBSTR(codigo,2,2)='"+txtfAno.getText()+"' ORDER BY codigo DESC LIMIT 1";
                mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(ssentencia);
                try {
                    mnt.intPrincipal.pnlEstado.conexion.rs.beforeFirst();
                    mnt.intPrincipal.pnlEstado.conexion.rs.next();
                    txtfSecuencia.setText(utilitario.utiNumero.convertirToString(utilitario.utiNumero.convertirToInt(mnt.intPrincipal.pnlEstado.conexion.rs.getString("secuencia"))+10001).substring(1,5));
                } catch (Exception e) {
                    txtfSecuencia.setText(utilitario.utiNumero.convertirToString(utilitario.utiNumero.convertirToInt("0000")+10001).substring(1,5));
                }
            }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfNumeroPlanilla);
        uti.agregarEventoFocusGained(this.txtfDescripcion);
        uti.agregarEventoFocusGained(this.txtfSecuencia);
        uti.agregarEventoKeyReleased(this.txtfNumeroPlanilla);
        uti.agregarEventoKeyReleased(this.txtfDescripcion);
        uti.agregarEventoKeyReleased(this.txtfSecuencia);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.chkActivo.setEnabled(false);
        this.txtfNumeroPlanilla.setEnabled(false);
        this.txtfAno.setEnabled(bmodo);
        this.btnObtenerSecuencia.setEnabled(bmodo);
        this.txtfSecuencia.setEnabled(bmodo);
        this.txtfDescripcion.setEnabled(bmodo);
        this.cmbTipoPlanilla.setEnabled(bmodo);
        this.txtFechaApertura.setEnabled(bmodo);
        this.txtfHoraApertura.setEnabled(bmodo);
        this.txtFechaCierre.setEnabled(false);
        this.txtfHoraCierre.setEnabled(false);
        this.componenteEspecial(!bmodo);
    }

    private void componenteEspecial(boolean bmodo) {
        this.btnCerrarPlanilla.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.cmbTipoPlanilla = new utilitario.comComboBox(this.cmboTipoPlanilla, this.lblTipoPlanilla, "SELECT id, descripcion||' - '||codigo AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwtipoplanillalegajo1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);

        this.txtfDescripcion.setDocument(new generico.modFormatoCadena(this.txtfDescripcion, especifico.entPlanillaLegajo.LONGITUD_DESCRIPCION, true));
        this.txtfNumeroPlanilla.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfId.setToolTipText(especifico.entPlanillaLegajo.TEXTO_ID);
        this.chkActivo.setToolTipText(especifico.entPlanillaLegajo.TEXTO_ACTIVO);
        this.txtfNumeroPlanilla.setToolTipText(especifico.entPlanillaLegajo.TEXTO_NUMERO_PLANILLA);
        this.cmbTipoPlanilla.setToolTipText(especifico.entPlanillaLegajo.TEXTO_CODIGO);
        this.txtfAno.setToolTipText(especifico.entPlanillaLegajo.TEXTO_CODIGO);
        this.txtfSecuencia.setToolTipText(especifico.entPlanillaLegajo.TEXTO_CODIGO);
        this.txtfDescripcion.setToolTipText(especifico.entPlanillaLegajo.TEXTO_DESCRIPCION);
        this.txtFechaApertura.setToolTipText(especifico.entPlanillaLegajo.TEXTO_FECHA_APERTURA);
        this.txtfHoraApertura.setToolTipText(especifico.entPlanillaLegajo.TEXTO_HORA_APERTURA);
        this.txtFechaCierre.setToolTipText(especifico.entPlanillaLegajo.TEXTO_FECHA_CIERRE);
        this.txtfHoraCierre.setToolTipText(especifico.entPlanillaLegajo.TEXTO_HORA_CIERRE);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) { 
            this.setInterfaz(new especifico.entPlanillaLegajo());
        }
    }

    public boolean insertar() {
        especifico.entPlanillaLegajo ent = new especifico.entPlanillaLegajo();
        java.util.Date fecha = new java.util.Date();
        String hora = ((Integer)(fecha.getHours()+100)).toString().substring(1)+":"+((Integer)(fecha.getMinutes()+100)).toString().substring(1)+":"+((Integer)(fecha.getSeconds()+100)).toString().substring(1);
        ent.setActivo(true);
        ent.setNumeroPlanilla(mnt.intPrincipal.pnlEstado.conexion.obtenerId("numeroplanillalegajo"));
        ent.setDescripcion("PLANILLA Nº "+utilitario.utiNumero.getMascara(ent.getNumeroPlanilla(),0)+" ("+utilitario.utiFecha.convertirToStringDMA(fecha)+" "+hora+")");
        ent.setCodigo("X"+utilitario.utiFecha.convertirToStringDMA(fecha).substring(utilitario.utiFecha.convertirToStringDMA(fecha).length()-2)+"0000");
        ent.setFechaApertura(fecha);
        ent.setHoraApertura(hora);
        ent.setHoraCierre("00:00:00");
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        especifico.entPlanillaLegajo ent = mod.getEntidad().copiar(new especifico.entPlanillaLegajo());
        this.getInterfaz(ent);
        especifico.intPlanillaLegajo planilla = new especifico.intPlanillaLegajo(mnt);
        planilla.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwplanillalegajo1), " AND codigo='"+ent.getCodigo().trim()+"' AND id<>"+ent.getId(), "");
        boolean esDuplicado = false;
        if (planilla.mod.getRowCount()>0) esDuplicado = true;
        if (!ent.esValido(esDuplicado)) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("planillalegajo"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
        this.cmbTipoPlanilla.refresh();
    }
    
    private void getInterfaz(especifico.entPlanillaLegajo ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setActivo(this.chkActivo.isSelected());
        ent.setNumeroPlanilla(utilitario.utiNumero.convertirToInt(this.txtfNumeroPlanilla.getText()));
        ent.setDescripcion(this.txtfDescripcion.getText());
        ent.setCodigo(this.cmbTipoPlanilla.getDescripcionSeleccionado().substring(this.cmbTipoPlanilla.getDescripcionSeleccionado().length()-1)+this.txtfAno.getText()+this.txtfSecuencia.getText());
        ent.setIdTipoPlanillaLegajo(this.cmbTipoPlanilla.getIdSeleccionado());
        ent.setTipoPlanillaLegajo(this.cmbTipoPlanilla.getDescripcionSeleccionado());
        ent.setFechaApertura(this.txtFechaApertura.getFecha());
        ent.setHoraApertura(this.txtfHoraApertura.getText());
        ent.setFechaCierre(this.txtFechaCierre.getFecha());
        ent.setHoraCierre(this.txtfHoraCierre.getText());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entPlanillaLegajo ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.chkActivo.setSelected(ent.getActivo());}
        catch (Exception e) {this.chkActivo.setSelected(false);}
        try                 {this.txtfNumeroPlanilla.setValue(ent.getNumeroPlanilla());}
        catch (Exception e) {this.txtfNumeroPlanilla.setValue(0);}
        try                 {this.txtfDescripcion.setText(ent.getDescripcion());}
        catch (Exception e) {this.txtfDescripcion.setText("");}
        try                 {this.cmbTipoPlanilla.setId(ent.getIdTipoPlanillaLegajo());}
        catch (Exception e) {this.cmbTipoPlanilla.setId(0);}
        try                 {this.txtfAno.setText(ent.getCodigo().substring(1,3));}
        catch (Exception e) {this.txtfAno.setText("00");}
        try                 {this.txtfSecuencia.setText(ent.getCodigo().substring(3,7));}
        catch (Exception e) {this.txtfSecuencia.setText("");}
        try                 {this.txtFechaApertura.setFecha(ent.getFechaApertura());}
        catch (Exception e) {this.txtFechaApertura.setFechaVacia();}
        try                 {this.txtfHoraApertura.setText(ent.getHoraApertura());}
        catch (Exception e) {this.txtfHoraApertura.setText("");}
        try                 {this.txtFechaCierre.setFecha(ent.getFechaCierre());}
        catch (Exception e) {this.txtFechaCierre.setFechaVacia();}
        try                 {this.txtfHoraCierre.setText(ent.getHoraCierre());}
        catch (Exception e) {this.txtfHoraCierre.setText("");}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

    public void imprimir() {
    }
    
    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================

    private void configurarPanelCierre() {
        especifico.intPlanillaLegajoCerrar intPlanilla = new especifico.intPlanillaLegajoCerrar(mnt, null);
        if (mod.getRowCount() <= 0) return;
        intPlanilla.mod.insertar(mod.getEntidad().copiar(new especifico.entPlanillaLegajo()));
        intPlanilla.mod.getTable().setRowSelectionInterval(0,0);
        intPlanilla.btnPlanillaIngreso.setEnabled(false);
        intPlanilla.setEnabled(true);
        if (!intPlanilla.modificar()) {
            intPlanilla.setEnabled(false);
            intPlanilla.cargarEditor();
        }
        int posicion = utilitario.utiGeneral.getPosicion(mod.getTable());
        generico.intPanel panel = new generico.intPanel("Cierre Planilla de Legajo :: Mantenimiento :: " + generico.mnuMantenimientoDetalle.modificar, intPlanilla);
        if (mod.getEntidad().getActivo() != true) panel.btnEstablecer.setEnabled(false);
        while (true) {
            panel.abrir();
            if (panel.establecido()) {
                if (intPlanilla.isEnabled()) { if (intPlanilla.establecer()) { if (intPlanilla.guardar()) { if (this.obtenerDato()) break; } } }
                else break;
            } else {
                if (intPlanilla.isEnabled()) { if (intPlanilla.cancelar()) break; }
                else break;
            }
        }
        intPlanilla.setEnabled(true);
        mod.getTable().updateUI();
        utilitario.utiGeneral.setPosicion(mod.getTable(), posicion);
    }

}
