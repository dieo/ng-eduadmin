/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intUsuario.java
 *
 * Created on 14-feb-2013, 16:32:23
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intControlVerificador extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modControlVerificador mod = new especifico.modControlVerificador();
    private utilitario.comComboBox cmbFuncionario;
    private String svista="", sfiltro="", sorden="";

    /** Creates new form intUsuario 
     @param mnt.
     @param tbl.
     */
    public intControlVerificador(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        txtfCargo = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblFuncionario = new javax.swing.JLabel();
        lblCargo = new javax.swing.JLabel();
        chkActivo = new javax.swing.JCheckBox();
        chkCorrector = new javax.swing.JCheckBox();
        lblCorrector = new javax.swing.JLabel();
        cmboFuncionario = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(475, 115));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(475, 115));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setToolTipText("");
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 110, 25));

        txtfCargo.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfCargo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 370, 25));

        lblId.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 90, 25));

        lblFuncionario.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFuncionario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFuncionario.setText("Funcionario");
        add(lblFuncionario, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 90, 25));

        lblCargo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblCargo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCargo.setText("Cargo");
        add(lblCargo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 90, 25));

        chkActivo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        chkActivo.setText("Activo");
        chkActivo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        chkActivo.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        chkActivo.setOpaque(false);
        add(chkActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 10, 80, 25));

        chkCorrector.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        chkCorrector.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        chkCorrector.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        chkCorrector.setOpaque(false);
        add(chkCorrector, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 85, 20, 25));

        lblCorrector.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblCorrector.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCorrector.setText("Corrector");
        add(lblCorrector, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 85, 90, 25));

        cmboFuncionario.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboFuncionario, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, 370, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkActivo;
    private javax.swing.JCheckBox chkCorrector;
    private javax.swing.JComboBox cmboFuncionario;
    private javax.swing.JLabel lblCargo;
    private javax.swing.JLabel lblCorrector;
    private javax.swing.JLabel lblFuncionario;
    private javax.swing.JLabel lblId;
    private javax.swing.JFormattedTextField txtfCargo;
    private javax.swing.JFormattedTextField txtfId;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfCargo);
        uti.agregarEventoKeyReleased(this.txtfCargo);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.cmbFuncionario.setEnabled(bmodo);
        this.txtfCargo.setEnabled(bmodo);
        this.chkActivo.setEnabled(bmodo);
        this.chkCorrector.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.cmbFuncionario = new utilitario.comComboBox(this.cmboFuncionario, this.lblFuncionario, "SELECT id, apellidonombre AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwfuncionario2)+" ORDER BY apellidonombre", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        
        this.txtfCargo.setDocument(new generico.modFormatoCadena(this.txtfCargo, especifico.entControlVerificador.LONGITUD_CARGO, true));
        this.txtfId.setToolTipText(especifico.entControlVerificador.TEXTO_ID);
        this.cmbFuncionario.setToolTipText(especifico.entControlVerificador.TEXTO_FUNCIONARIO);
        this.txtfCargo.setToolTipText(especifico.entControlVerificador.TEXTO_CARGO);
        this.chkActivo.setToolTipText(especifico.entControlVerificador.TEXTO_ACTIVO);
        this.chkCorrector.setToolTipText(especifico.entControlVerificador.TEXTO_CORRECTOR);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) { 
            this.setInterfaz(new especifico.entControlVerificador());
        }
    }

    public boolean insertar() {
        especifico.entControlVerificador ent = new especifico.entControlVerificador();
        ent.setActivo(true);
        ent.setCorrector(false);
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        especifico.entControlVerificador ent = mod.getEntidad().copiar(new especifico.entControlVerificador()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValido()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("verificador"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
        this.cmbFuncionario.refresh();
    }

    private void getInterfaz(especifico.entControlVerificador ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setIdFuncionario(this.cmbFuncionario.getIdSeleccionado());
        ent.setFuncionario(this.cmbFuncionario.getDescripcionSeleccionado());
        ent.setCargo(this.txtfCargo.getText());
        ent.setActivo(this.chkActivo.isSelected());
        ent.setCorrector(this.chkCorrector.isSelected());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entControlVerificador ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.cmbFuncionario.setId(ent.getIdFuncionario());}
        catch (Exception e) {this.cmbFuncionario.setId(0);}
        try                 {this.txtfCargo.setText(ent.getCargo());}
        catch (Exception e) {this.txtfCargo.setText("");}
        try                 {this.chkActivo.setSelected(ent.getActivo());}
        catch (Exception e) {this.chkActivo.setSelected(false);}
        try                 {this.chkCorrector.setSelected(ent.getCorrector());}
        catch (Exception e) {this.chkCorrector.setSelected(false);}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }
    
}
