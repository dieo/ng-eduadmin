/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intAhorroProgramadoAnular.java
 *
 * Created on 23-feb-2013, 9:53:15
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intAtencionSocioImprimir extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    private utilitario.comComboBox cmbUsuario;
    private utilitario.comComboBox cmbObjeto;
    private utilitario.comComboBox cmbSolucion;
    
    /** Creates new form intAhorroProgramadoRetirar 
     @param mnt.
     */
    public intAtencionSocioImprimir(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
        this.setEnabled();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfIdEstadoAhorro = new javax.swing.JFormattedTextField();
        pnlEstado = new javax.swing.JPanel();
        btnImprimirFecha = new javax.swing.JButton();
        try {
            txtFechaDesde = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        try {
            txtFechaHasta = new panel.fecha();
        } catch (java.text.ParseException e1) {
            e1.printStackTrace();
        }
        lblDesde = new javax.swing.JLabel();
        lblHasta = new javax.swing.JLabel();
        chkUsuario = new javax.swing.JCheckBox();
        cmboUsuario = new javax.swing.JComboBox();
        lblUsuario = new javax.swing.JLabel();
        lblObjeto = new javax.swing.JLabel();
        chkObjeto = new javax.swing.JCheckBox();
        cmboObjeto = new javax.swing.JComboBox();
        lblSolucion = new javax.swing.JLabel();
        chkSolucion = new javax.swing.JCheckBox();
        cmboSolucion = new javax.swing.JComboBox();
        pnlImprimirLiquidacion = new javax.swing.JPanel();
        lblCedula = new javax.swing.JLabel();
        btnImprimirSocio = new javax.swing.JButton();
        txtfCedula = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(405, 220));
        setPreferredSize(new java.awt.Dimension(405, 220));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfIdEstadoAhorro.setFocusable(false);
        txtfIdEstadoAhorro.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        add(txtfIdEstadoAhorro, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 0, 25));

        pnlEstado.setBackground(new java.awt.Color(255, 255, 255));
        pnlEstado.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlEstado.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnImprimirFecha.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirFecha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirFecha.setToolTipText("Imprimir por Fecha");
        btnImprimirFecha.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirFecha.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirFecha.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirFecha.setOpaque(false);
        btnImprimirFecha.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlEstado.add(btnImprimirFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 110, 25, 25));
        pnlEstado.add(txtFechaDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(85, 85, -1, 25));
        pnlEstado.add(txtFechaHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(85, 110, -1, 25));

        lblDesde.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblDesde.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDesde.setText("Desde");
        pnlEstado.add(lblDesde, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 85, 65, 25));

        lblHasta.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblHasta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblHasta.setText("Hasta");
        pnlEstado.add(lblHasta, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 65, 25));

        chkUsuario.setToolTipText("");
        chkUsuario.setOpaque(false);
        pnlEstado.add(chkUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 20, 25));

        cmboUsuario.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 275, 25));

        lblUsuario.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUsuario.setText("Usuario");
        pnlEstado.add(lblUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 65, 25));

        lblObjeto.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblObjeto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblObjeto.setText("Objeto");
        pnlEstado.add(lblObjeto, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 65, 25));

        chkObjeto.setToolTipText("");
        chkObjeto.setOpaque(false);
        pnlEstado.add(chkObjeto, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 35, 20, 25));

        cmboObjeto.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboObjeto, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, 275, 25));

        lblSolucion.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblSolucion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSolucion.setText("Solución");
        pnlEstado.add(lblSolucion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 65, 25));

        chkSolucion.setToolTipText("");
        chkSolucion.setOpaque(false);
        pnlEstado.add(chkSolucion, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, 20, 25));

        cmboSolucion.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlEstado.add(cmboSolucion, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 275, 25));

        add(pnlEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 65, 385, 145));

        pnlImprimirLiquidacion.setBackground(new java.awt.Color(255, 255, 255));
        pnlImprimirLiquidacion.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlImprimirLiquidacion.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblCedula.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        lblCedula.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCedula.setText("Cédula");
        pnlImprimirLiquidacion.add(lblCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 25));

        btnImprimirSocio.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnImprimirSocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/imprimir20.png"))); // NOI18N
        btnImprimirSocio.setToolTipText("Imprimir por Socio");
        btnImprimirSocio.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImprimirSocio.setMaximumSize(new java.awt.Dimension(65, 35));
        btnImprimirSocio.setMinimumSize(new java.awt.Dimension(65, 35));
        btnImprimirSocio.setOpaque(false);
        btnImprimirSocio.setPreferredSize(new java.awt.Dimension(65, 35));
        pnlImprimirLiquidacion.add(btnImprimirSocio, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 10, 25, 25));

        txtfCedula.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pnlImprimirLiquidacion.add(txtfCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 90, 25));

        add(pnlImprimirLiquidacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 385, 45));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnImprimirFecha;
    private javax.swing.JButton btnImprimirSocio;
    private javax.swing.JCheckBox chkObjeto;
    private javax.swing.JCheckBox chkSolucion;
    private javax.swing.JCheckBox chkUsuario;
    private javax.swing.JComboBox cmboObjeto;
    private javax.swing.JComboBox cmboSolucion;
    private javax.swing.JComboBox cmboUsuario;
    private javax.swing.JLabel lblCedula;
    private javax.swing.JLabel lblDesde;
    private javax.swing.JLabel lblHasta;
    private javax.swing.JLabel lblObjeto;
    private javax.swing.JLabel lblSolucion;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPanel pnlEstado;
    private javax.swing.JPanel pnlImprimirLiquidacion;
    private panel.fecha txtFechaDesde;
    private panel.fecha txtFechaHasta;
    public javax.swing.JTextField txtfCedula;
    private javax.swing.JFormattedTextField txtfIdEstadoAhorro;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        btnImprimirSocio.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirSocio();
            }
        });
        btnImprimirFecha.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimirFecha();
            }
        });
        chkUsuario.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbUsuario.setId(0);
                cmbUsuario.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkObjeto.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbObjeto.setId(0);
                cmbObjeto.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
        chkSolucion.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) { 
                cmbSolucion.setId(0);
                cmbSolucion.setEnabled(!((javax.swing.JCheckBox)evt.getSource()).isSelected());
            }
        });
    }

    private void setEnabled() {
        this.chkUsuario.setSelected(true);
        this.chkObjeto.setSelected(true);
        this.chkSolucion.setSelected(true);
    }
    
    private void iniciarComponentes() {
        this.cmbUsuario = new utilitario.comComboBox(this.cmboUsuario, this.lblUsuario, "SELECT id, apellidonombre AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwusuario1)+" ORDER BY apellidonombre", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbObjeto = new utilitario.comComboBox(this.cmboObjeto, this.lblObjeto, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwobjetoatencion1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        this.cmbSolucion = new utilitario.comComboBox(this.cmboSolucion, this.lblSolucion, "SELECT id, descripcion FROM "+generico.vista.getNombre(generico.vista.vwsolucionatencion1)+" ORDER BY descripcion", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        
        this.txtfCedula.setToolTipText("Cédula del Socio");
        this.btnImprimirSocio.setToolTipText("Imprime por Socio");
        this.chkUsuario.setToolTipText("Habilita/Deshabilita la selección de Usuario");
        this.chkObjeto.setToolTipText("Habilita/Deshabilita la selección de Objeto de Llamada");
        this.chkSolucion.setToolTipText("Habilita/Deshabilita la selección de Solución de Llamada");
        this.cmbUsuario.setToolTipText("Usuario");
        this.cmbObjeto.setToolTipText("Objeto de Llamada");
        this.cmbSolucion.setToolTipText("Solución de Llamada");
        this.txtFechaDesde.setToolTipText("Fecha desde");
        this.txtFechaHasta.setToolTipText("Fecha hasta");
        this.btnImprimirSocio.setToolTipText("Imprime según el criterio");
    }

    private void imprimirSocio() {
        especifico.intAtencionSocio llamada = new especifico.intAtencionSocio(mnt);
        int idSocio = mnt.intPrincipal.pnlEstado.conexion.obtenerId(generico.vista.getNombre(generico.vista.vwsocio1), "cedula="+utilitario.utiCadena.getTextoGuardado(this.txtfCedula.getText().trim()));
        llamada.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwatencionsocio1), " AND idsocio="+idSocio, "");
        llamada.obtenerDato();
        if (llamada.mod.getRowCount()<=0) {
            javax.swing.JOptionPane.showMessageDialog(null, "Cédula "+this.txtfCedula.getText().trim()+" no existe");
            return;
        }
        // =====================
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repAtencionSocio, llamada.mod, null, null, null, "Informe de Atención por Socio", mnt.intPrincipal.pnlEstado.sfiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }

    private void imprimirFecha() {
        String sfiltro = "";
        String stextoFiltro = "";
        if (this.cmbUsuario.getIdSeleccionado()>0) {
            sfiltro += " AND idusuario="+this.cmbUsuario.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbUsuario.getDescripcionSeleccionado();
        }
        if (this.cmbObjeto.getIdSeleccionado()>0) {
            sfiltro += " AND idobjetoatencion="+this.cmbObjeto.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbObjeto.getDescripcionSeleccionado();
        }
        if (this.cmbSolucion.getIdSeleccionado()>0) {
            sfiltro += " AND idsolucionatencion="+this.cmbSolucion.getIdSeleccionado();
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += this.cmbSolucion.getDescripcionSeleccionado();
        }
        if (this.txtFechaDesde.getFecha()==null && this.txtFechaHasta.getFecha()!=null) {
            sfiltro += " AND fechaatencion="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "FECHA ATENCION: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()==null) {
            sfiltro += " AND fechallamada="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "FECHA ATENCION: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha());
        }
        if (this.txtFechaDesde.getFecha()!=null && this.txtFechaHasta.getFecha()!=null) {
            sfiltro += " AND fechaatencion>="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" AND fechaatencion<="+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
            if (!stextoFiltro.isEmpty()) stextoFiltro += " - ";
            stextoFiltro += "FECHA ATENCION: "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaDesde.getFecha())+" a "+utilitario.utiFecha.getFechaGuardadoMac(this.txtFechaHasta.getFecha());
        }
        especifico.intAtencionSocio llamada = new especifico.intAtencionSocio(mnt);
        llamada.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwatencionsocio1), sfiltro, " ORDER BY usuarionombre, usuarioapellido, fechaatencion");
        llamada.obtenerDato();
        reporte.repReporte rep = new reporte.repReporte(mnt.intPrincipal.pnlEstado.defecto.getRutaReporte());
        if (!rep.imprimir(generico.reporte.repAtencionSocioUsuarioFecha, llamada.mod, null, null, null, "Informe de Atención", stextoFiltro, mnt.intPrincipal.pnlEstado.usuario.getUsuario())) {
            javax.swing.JOptionPane.showMessageDialog(null, rep.getMensaje());
        }
    }
    
}
