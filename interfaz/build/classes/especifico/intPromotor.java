/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intFuncionario.java
 *
 * Created on 10/10/2011, 09:59:12 AM
 */
package especifico;

/**
 *
 * @author Lic. Didier F. Barreto
 */
public class intPromotor extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modPromotor mod = new especifico.modPromotor();
    private utilitario.comComboBox cmbFuncionario;
    private String svista="", sfiltro="", sorden="";

    /** Creates new form intFuncionario 
     @param mnt.
     @param tbl.
     */
    public intPromotor(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }

    public intPromotor(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        this.mod.setTable(new javax.swing.JTable());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblFuncionario = new javax.swing.JLabel();
        cmboFuncionario = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(470, 65));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(470, 65));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setToolTipText("");
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 110, 25));

        lblId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 90, 25));

        lblFuncionario.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblFuncionario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFuncionario.setText("Funcionario");
        add(lblFuncionario, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 90, 25));

        cmboFuncionario.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboFuncionario, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, 360, 25));
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmboFuncionario;
    private javax.swing.JLabel lblFuncionario;
    private javax.swing.JLabel lblId;
    private javax.swing.JFormattedTextField txtfId;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.cmbFuncionario.setEnabled(bmodo);
    }

    private void iniciarComponentes() {
        this.cmbFuncionario = new utilitario.comComboBox(this.cmboFuncionario, this.lblFuncionario, "SELECT id, apellidonombre AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwfuncionario2)+" ORDER BY apellidonombre", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);

        this.txtfId.setToolTipText(especifico.entPromotor.TEXTO_ID);
        this.cmbFuncionario.setToolTipText(especifico.entPromotor.TEXTO_FUNCIONARIO);
    }

    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) {
            this.setInterfaz(new especifico.entPromotor());
        }
    }

    public boolean insertar() {
        especifico.entPromotor ent = new especifico.entPromotor();
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        especifico.entPromotor ent = mod.getEntidad().copiar(new especifico.entPromotor()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValido()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_insertado) ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("promotor"));
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }

    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
        this.cmbFuncionario.refresh();
    }

    private void getInterfaz(especifico.entPromotor ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setIdFuncionario(this.cmbFuncionario.getIdSeleccionado());
        ent.setFuncionario(this.cmbFuncionario.getDescripcionSeleccionado());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entPromotor ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.cmbFuncionario.setId(ent.getIdFuncionario());}
        catch (Exception e) {this.cmbFuncionario.setId(0);}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }
    
    public void imprimir() {
        especifico.intPromotorImprimir intPromotor = new especifico.intPromotorImprimir(mnt);
        generico.intPanel panel = new generico.intPanel("Promotor :: Impresión", intPromotor);
        panel.btnEstablecer.setEnabled(false);
        panel.abrir();
    }

}
