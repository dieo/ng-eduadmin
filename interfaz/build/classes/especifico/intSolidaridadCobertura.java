/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * intEntidad.java
 *
 * Created on 06-abr-2012, 9:29:19
 */
package especifico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intSolidaridadCobertura extends javax.swing.JPanel {

    private generico.intMantenimiento mnt;
    public especifico.modSolidaridadCobertura mod = new especifico.modSolidaridadCobertura();
    private especifico.intMovimientoDocumento intMovimientoDocumento;
    private utilitario.comComboBox cmbMoneda;
    private final generico.mnuMantenimientoDetalle mnuDetalle = new generico.mnuMantenimientoDetalle();
    private String svista="", sfiltro="", sorden="";
    
    /** Creates new form intEntidad 
     @param mnt.
     @param tbl.
     */
    public intSolidaridadCobertura(generico.intMantenimiento mnt, javax.swing.JTable tbl) {
        this.mnt = mnt;
        if (tbl==null) this.mod.setTable(new javax.swing.JTable());
        else this.mod.setTable(tbl);
        initComponents();
        this.iniciarComponentes();
        this.agregarEventos();
    }
    
    public intSolidaridadCobertura(generico.intMantenimiento mnt) {
        this.mnt = mnt;
        this.mod.setTable(new javax.swing.JTable());
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtfId = new javax.swing.JFormattedTextField();
        txtfDescripcion = new javax.swing.JFormattedTextField();
        lblId = new javax.swing.JLabel();
        lblDescripcion = new javax.swing.JLabel();
        lblMeses = new javax.swing.JLabel();
        txtfMonto = new javax.swing.JFormattedTextField();
        lblMonto = new javax.swing.JLabel();
        txtfAntiguedadMinima = new javax.swing.JFormattedTextField();
        lblAntiguedadMinima = new javax.swing.JLabel();
        chkActivo = new javax.swing.JCheckBox();
        lblDocumento = new javax.swing.JLabel();
        scpListadoDocumento = new javax.swing.JScrollPane();
        tblListadoDocumento = new javax.swing.JTable();
        lblLeyendaArticulo = new javax.swing.JLabel();
        cmboMoneda = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(605, 265));
        setPreferredSize(new java.awt.Dimension(605, 265));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtfId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfId.setToolTipText("");
        txtfId.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfId, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 110, 25));

        txtfDescripcion.setToolTipText("");
        txtfDescripcion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 35, 490, 25));

        lblId.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblId.setText("Nro");
        add(lblId, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 100, 25));

        lblDescripcion.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblDescripcion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDescripcion.setText("Descripción");
        add(lblDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 35, 100, 25));

        lblMeses.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblMeses.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMeses.setText("meses");
        add(lblMeses, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 60, 40, 25));

        txtfMonto.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfMonto.setToolTipText("");
        txtfMonto.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfMonto, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, 110, 25));

        lblMonto.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblMonto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMonto.setText("Monto");
        add(lblMonto, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 100, 25));

        txtfAntiguedadMinima.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtfAntiguedadMinima.setToolTipText("");
        txtfAntiguedadMinima.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(txtfAntiguedadMinima, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 60, 50, 25));

        lblAntiguedadMinima.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblAntiguedadMinima.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblAntiguedadMinima.setText("Antigüedad Mínima");
        add(lblAntiguedadMinima, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 60, 100, 25));

        chkActivo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        chkActivo.setText("Activo   ");
        chkActivo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        chkActivo.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        chkActivo.setOpaque(false);
        add(chkActivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 10, 70, 25));

        lblDocumento.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        lblDocumento.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDocumento.setText("Documento requerido");
        add(lblDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 150, 20));

        scpListadoDocumento.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tblListadoDocumento.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        scpListadoDocumento.setViewportView(tblListadoDocumento);

        add(scpListadoDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 110, 600, 150));

        lblLeyendaArticulo.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        lblLeyendaArticulo.setForeground(new java.awt.Color(153, 153, 153));
        lblLeyendaArticulo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        add(lblLeyendaArticulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 90, 330, 20));

        cmboMoneda.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        add(cmboMoneda, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 60, 45, 25));
    }// </editor-fold>//GEN-END:initComponents

    private void scpListadoMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == 3) { // click derecho
            java.awt.Point posMouse = java.awt.MouseInfo.getPointerInfo().getLocation();
            this.mnuDetalle.show(mnt.intPrincipal, posMouse.x-mnt.intPrincipal.getLocation().x, posMouse.y-mnt.intPrincipal.getLocation().y);
        }
    }

    private void tblListadoMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == 3) { // click derecho
            java.awt.Point posMouse = java.awt.MouseInfo.getPointerInfo().getLocation();
            this.mnuDetalle.show(mnt.intPrincipal, posMouse.x-mnt.intPrincipal.getLocation().x, posMouse.y-mnt.intPrincipal.getLocation().y);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkActivo;
    private javax.swing.JComboBox cmboMoneda;
    private javax.swing.JLabel lblAntiguedadMinima;
    private javax.swing.JLabel lblDescripcion;
    private javax.swing.JLabel lblDocumento;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblLeyendaArticulo;
    private javax.swing.JLabel lblMeses;
    private javax.swing.JLabel lblMonto;
    private javax.swing.JScrollPane scpListadoDocumento;
    public javax.swing.JTable tblListadoDocumento;
    private javax.swing.JFormattedTextField txtfAntiguedadMinima;
    private javax.swing.JFormattedTextField txtfDescripcion;
    private javax.swing.JFormattedTextField txtfId;
    private javax.swing.JFormattedTextField txtfMonto;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        // DE MOUSE Y TECLADO AL JTABLE
        scpListadoDocumento.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) { scpListadoMouseClicked(evt); }
        });
        tblListadoDocumento.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) { tblListadoMouseClicked(evt); }
        });
        tblListadoDocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) { configurarPanelMovimientoDocumento(generico.mnuMantenimientoDetalle.getNombre(evt.getKeyCode())); }
        });
        // MENU CONTEXTUAL
        this.mnuDetalle.mnuInsertar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelMovimientoDocumento(((javax.swing.JMenuItem)evt.getSource()).getText()); }
        });
        this.mnuDetalle.mnuModificar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelMovimientoDocumento(((javax.swing.JMenuItem)evt.getSource()).getText()); }
        });
        this.mnuDetalle.mnuVisualizar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelMovimientoDocumento(((javax.swing.JMenuItem)evt.getSource()).getText()); }
        });
        this.mnuDetalle.mnuEliminar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) { configurarPanelMovimientoDocumento(((javax.swing.JMenuItem)evt.getSource()).getText()); }
        });
        utilitario.utiEvento uti = new utilitario.utiEvento();
        uti.agregarEventoFocusGained(this.txtfDescripcion);
        uti.agregarEventoFocusGained(this.txtfMonto);
        uti.agregarEventoFocusGained(this.txtfAntiguedadMinima);
        uti.agregarEventoKeyReleased(this.txtfDescripcion);
        uti.agregarEventoKeyReleased(this.txtfMonto);
        uti.agregarEventoKeyReleased(this.txtfAntiguedadMinima);
        uti.agregarEventoKeyPressed(this.tblListadoDocumento);
    }
    
    @Override
    public final void setEnabled(boolean bmodo) {
        this.txtfId.setEnabled(false);
        this.txtfDescripcion.setEnabled(bmodo);
        this.txtfMonto.setEnabled(bmodo);
        this.cmbMoneda.setEnabled(bmodo);
        this.txtfAntiguedadMinima.setEnabled(bmodo);
        this.chkActivo.setEnabled(bmodo);
    }
    
    private void iniciarComponentes() {
        this.cmbMoneda = new utilitario.comComboBox(this.cmboMoneda, this.lblMonto, "SELECT id, simbolo AS descripcion FROM "+generico.vista.getNombre(generico.vista.vwmoneda1)+" ORDER BY simbolo", generico.entConstante.host, generico.entConstante.baseDato, generico.entConstante.port);
        intMovimientoDocumento = new especifico.intMovimientoDocumento(mnt, this.tblListadoDocumento);

        this.txtfDescripcion.setDocument(new generico.modFormatoCadena(this.txtfDescripcion, especifico.entSolidaridadCobertura.LONGITUD_DESCRIPCION, true));
        this.txtfMonto.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(true, 0));
        this.txtfAntiguedadMinima.setFormatterFactory(new generico.modFormatoNumero().FormatoDecimal(false, 0));
        this.txtfId.setToolTipText(especifico.entSolidaridadCobertura.TEXTO_ID);
        this.txtfDescripcion.setToolTipText(especifico.entSolidaridadCobertura.TEXTO_DESCRIPCION);
        this.txtfMonto.setToolTipText(especifico.entSolidaridadCobertura.TEXTO_MONTO);
        this.txtfAntiguedadMinima.setToolTipText(especifico.entSolidaridadCobertura.TEXTO_ANTIGUEDAD_MINIMA);
        this.chkActivo.setToolTipText(especifico.entSolidaridadCobertura.TEXTO_ACTIVO);
        this.cmbMoneda.setToolTipText(especifico.entAhorroProgramadoPlan.TEXTO_MONEDA);
    }
    
    public void cargarEditor() {
        try {
            this.setInterfaz(mod.getEntidad());
        } catch (Exception e) { 
            this.setInterfaz(new especifico.entSolidaridadCobertura());
        }
        try {
            intMovimientoDocumento.setVistaFiltroOrden(generico.vista.getNombre(generico.vista.vwdetallecobertura1), " AND idcobertura="+mod.getEntidad().getId(), " ORDER BY id");
            intMovimientoDocumento.obtenerDato();
            intMovimientoDocumento.mod.getTable().updateUI();
        } catch (Exception e) { }
    }
    
    public boolean insertar() {
        especifico.entSolidaridadCobertura ent = new especifico.entSolidaridadCobertura();
        ent.setId(mnt.intPrincipal.pnlEstado.conexion.obtenerId("cobertura")); // establece nuevo id (definitivo)
        ent.setEstadoRegistro(generico.entConstante.estadoregistro_pendiente); // establece nuevo estado (pendiente)
        ent.setIdMoneda(mnt.intPrincipal.pnlEstado.defecto.getIdMoneda());
        ent.setActivo(true);
        mod.insertar(ent); // inserta nueva entidad
        this.setInterfaz(ent);
        return true;
    }

    public boolean modificar() {
        return true;
    }
    
    public boolean establecer() {
        especifico.entSolidaridadCobertura ent = mod.getEntidad().copiar(new especifico.entSolidaridadCobertura()); // obtiene la entidad del registro seleccionado
        this.getInterfaz(ent);
        if (!ent.esValido()) { // valida la entidad
            javax.swing.JOptionPane.showMessageDialog(null, ent.getMensaje());
            return false; // no es válido
        }
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) ent.setEstadoRegistro(generico.entConstante.estadoregistro_insertado);
        if (ent.getEstadoRegistro()==generico.entConstante.estadoregistro_predeterminado) ent.setEstadoRegistro(generico.entConstante.estadoregistro_modificado);
        if (mnt.intPrincipal.pnlEstado.sfiltro.isEmpty()) this.sfiltro=" AND id="+ent.getId();
        mod.modificar(ent); // modifica
        return true;
    }
    
    public boolean eliminar() {
        mod.getEntidad().setEstadoRegistro(generico.entConstante.estadoregistro_eliminado);
        return true;
    }

    public boolean cancelar() {
        if (mod.getEntidad().getEstadoRegistro()==generico.entConstante.estadoregistro_pendiente) {
            mod.eliminar(); // elimina el nuevo registro
        }
        return true;
    }

    public boolean guardar() {
        if (!mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mod.getEntidad().getSentencia())) { // obtiene datos de la base de datos
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public boolean obtenerDato() {
        if (mnt.intPrincipal.pnlEstado.conexion.ejecutarSentencia(mnt.intPrincipal.pnlEstado.conexion.getConsulta(svista, sfiltro, sorden))) { // obtiene datos de la base de datos
            mod.cargarTable(mnt.intPrincipal.pnlEstado.conexion.rs);
        } else {
            javax.swing.JOptionPane.showMessageDialog(null, mnt.intPrincipal.pnlEstado.conexion.getMensaje());
            return false;
        }
        return true;
    }

    public void refrescarComponente() {
        this.cmbMoneda.refresh();
    }
    
    private void getInterfaz(especifico.entSolidaridadCobertura ent) {
        ent.setId(utilitario.utiNumero.convertirToInt(this.txtfId.getText()));
        ent.setDescripcion(this.txtfDescripcion.getText());
        ent.setMonto(utilitario.utiNumero.convertirToDoubleMac(this.txtfMonto.getText()));
        ent.setIdMoneda(this.cmbMoneda.getIdSeleccionado());
        ent.setMoneda(this.cmbMoneda.getDescripcionSeleccionado());
        ent.setAntiguedadMinima(utilitario.utiNumero.convertirToInt(this.txtfAntiguedadMinima.getText()));
        ent.setActivo(this.chkActivo.isSelected());
        ent.setEstadoRegistro(ent.getEstadoRegistro());
    }
    
    private void setInterfaz(especifico.entSolidaridadCobertura ent) {
        try                 {this.txtfId.setValue(ent.getId());}
        catch (Exception e) {this.txtfId.setValue(0);}
        try                 {this.txtfDescripcion.setText(ent.getDescripcion());}
        catch (Exception e) {this.txtfDescripcion.setText("");}
        try                 {this.txtfMonto.setValue(ent.getMonto());}
        catch (Exception e) {this.txtfMonto.setValue(0);}
        try                 {this.cmbMoneda.setId(ent.getIdMoneda());}
        catch (Exception e) {this.cmbMoneda.setId(0);}
        try                 {this.txtfAntiguedadMinima.setValue(ent.getAntiguedadMinima());}
        catch (Exception e) {this.txtfAntiguedadMinima.setValue(0);}
        try                 {this.chkActivo.setSelected(ent.getActivo());}
        catch (Exception e) {this.chkActivo.setSelected(false);}
    }

    public void setVistaFiltroOrden(String svista, String sfiltro, String sorden) {
        if (!svista.isEmpty())  this.svista = svista;
        if (!sfiltro.isEmpty()) this.sfiltro = sfiltro;
        if (!sorden.isEmpty())  this.sorden = sorden;
    }

    public void imprimir() {
    }

    //======================================
    // LLAMADAS A LOS DETALLES
    //======================================
    
    private void configurarPanelMovimientoDocumento(String sfuncion) {
        if (sfuncion.equals("")) return; // presiono cualquier tecla
        if (sfuncion.equals(generico.mnuMantenimientoDetalle.eliminar) || sfuncion.equals(generico.mnuMantenimientoDetalle.visualizar)) intMovimientoDocumento.setEnabled(false);
        else intMovimientoDocumento.setEnabled(true);
        if (sfuncion.equals(generico.mnuMantenimientoDetalle.insertar)) {
            intMovimientoDocumento.insertar();
            intMovimientoDocumento.mod.getTable().setRowSelectionInterval(intMovimientoDocumento.mod.getTable().getRowCount()-1, intMovimientoDocumento.mod.getTable().getRowCount()-1);
        }
        if (intMovimientoDocumento.mod.getTable().getSelectedRow()>=0) intMovimientoDocumento.cargarEditor();
        generico.intPanel panel = new generico.intPanel("Movimiento Documento :: Mantenimiento :: " + sfuncion, intMovimientoDocumento);
        int posicion = utilitario.utiCadena.getPosicion(intMovimientoDocumento.mod.getTable());
        while (true && intMovimientoDocumento.mod.getTable().getSelectedRow()>=0) {
            panel.abrir();
            if (panel.establecido()) {
                if (sfuncion.equals(generico.mnuMantenimientoDetalle.eliminar)) { if (utilitario.utiAviso.advertirEliminacion()) { if (intMovimientoDocumento.eliminar()) { if (intMovimientoDocumento.guardar()) { intMovimientoDocumento.obtenerDato(); break; } } }
                } else if (sfuncion.equals(generico.mnuMantenimientoDetalle.visualizar)) { break;
                } else { if (intMovimientoDocumento.establecer()) { if (intMovimientoDocumento.guardar()) { intMovimientoDocumento.obtenerDato(); break; } } }
            } else {
                if (!sfuncion.equals(generico.mnuMantenimientoDetalle.eliminar) && !sfuncion.equals(generico.mnuMantenimientoDetalle.visualizar)) { if (utilitario.utiAviso.advertirCancelacion()) { if (intMovimientoDocumento.cancelar()) break; }
                } else { intMovimientoDocumento.cancelar(); break; }
            }
        }
        intMovimientoDocumento.setEnabled(true);
        intMovimientoDocumento.mod.getTable().updateUI();
        utilitario.utiCadena.setPosicion(intMovimientoDocumento.mod.getTable(), posicion);
    }

}
