/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * intPais.java
 *
 * Created on 05-abr-2012, 8:07:13
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class intOrdenar extends javax.swing.JPanel {

    private javax.swing.JDialog dlg = new javax.swing.JDialog();
    private String stitulo = "Orden";
    private String sorden = "";
    private boolean bestablecido = false;
    
    /** Creates new form intPais */
    public intOrdenar() {
        initComponents();
        this.cmbCampo.setModel(new generico.modLista());
        ((generico.modLista)this.cmbCampo.getModel()).cargarModelo(new generico.entLista(0, "Predeterminado", "0=0", (short)0));
        this.cmbCampo.setSelectedIndex(0);
        this.establecerOrden();
        dlg.setTitle(stitulo);
        dlg.setResizable(false);
        dlg.setMinimumSize(new java.awt.Dimension(this.getPreferredSize().width+5, this.getPreferredSize().height+30));
        dlg.add(this);
        dlg.setLocation(new java.awt.Point((java.awt.Toolkit.getDefaultToolkit().getScreenSize().width-dlg.getSize().width)/2, (java.awt.Toolkit.getDefaultToolkit().getScreenSize().height-dlg.getSize().height)/2));
        dlg.setModal(true);
        this.agregarEventos();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groModo = new javax.swing.ButtonGroup();
        scpBoton = new javax.swing.JPanel();
        btnEstablecer = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        pnlDato = new javax.swing.JPanel();
        lblCampo = new javax.swing.JLabel();
        cmbCampo = new javax.swing.JComboBox();
        lblModo = new javax.swing.JLabel();
        optDescendente = new javax.swing.JRadioButton();
        optAscendente = new javax.swing.JRadioButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(280, 130));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(280, 130));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        scpBoton.setBackground(new java.awt.Color(255, 255, 255));

        btnEstablecer.setText("Aceptar");
        btnEstablecer.setToolTipText("Establecer");
        btnEstablecer.setMaximumSize(new java.awt.Dimension(65, 35));
        btnEstablecer.setMinimumSize(new java.awt.Dimension(65, 35));
        btnEstablecer.setPreferredSize(new java.awt.Dimension(65, 35));
        btnEstablecer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEstablecerActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.setToolTipText("Cancelar");
        btnCancelar.setMaximumSize(new java.awt.Dimension(65, 35));
        btnCancelar.setMinimumSize(new java.awt.Dimension(65, 35));
        btnCancelar.setPreferredSize(new java.awt.Dimension(65, 35));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout scpBotonLayout = new javax.swing.GroupLayout(scpBoton);
        scpBoton.setLayout(scpBotonLayout);
        scpBotonLayout.setHorizontalGroup(
            scpBotonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, scpBotonLayout.createSequentialGroup()
                .addContainerGap(96, Short.MAX_VALUE)
                .addComponent(btnEstablecer, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        scpBotonLayout.setVerticalGroup(
            scpBotonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scpBotonLayout.createSequentialGroup()
                .addGroup(scpBotonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEstablecer, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        add(scpBoton, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 100, 270, 25));

        pnlDato.setBackground(new java.awt.Color(255, 255, 255));
        pnlDato.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlDato.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblCampo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblCampo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCampo.setText("Campo");
        pnlDato.add(lblCampo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 50, 25));

        cmbCampo.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        pnlDato.add(cmbCampo, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 50, 200, 25));

        lblModo.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblModo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblModo.setText("Modo");
        pnlDato.add(lblModo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 5, 50, 25));

        groModo.add(optDescendente);
        optDescendente.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        optDescendente.setText("DESCENDENTE");
        optDescendente.setOpaque(false);
        pnlDato.add(optDescendente, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 25, 100, 20));

        groModo.add(optAscendente);
        optAscendente.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        optAscendente.setSelected(true);
        optAscendente.setText("ASCENDENTE");
        optAscendente.setOpaque(false);
        pnlDato.add(optAscendente, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 5, 100, 20));

        add(pnlDato, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 5, 270, 90));
    }// </editor-fold>//GEN-END:initComponents

    private void btnEstablecerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEstablecerActionPerformed
        this.establecerOrden();
        this.bestablecido = true;
        this.cerrar();
    }//GEN-LAST:event_btnEstablecerActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.bestablecido = false;
        this.cerrar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnCancelar;
    public javax.swing.JButton btnEstablecer;
    private javax.swing.JComboBox cmbCampo;
    private javax.swing.ButtonGroup groModo;
    private javax.swing.JLabel lblCampo;
    private javax.swing.JLabel lblModo;
    private javax.swing.JRadioButton optAscendente;
    private javax.swing.JRadioButton optDescendente;
    private javax.swing.JPanel pnlDato;
    private javax.swing.JPanel scpBoton;
    // End of variables declaration//GEN-END:variables

    private void agregarEventos() {
        dlg.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosed(java.awt.event.WindowEvent evt) { }
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                bestablecido = false;
                cerrar();
            }
        });    
    }
    
    public final void establecerOrden() {
        this.sorden = "";
        try{
            if (this.optAscendente.isSelected()) {
                this.sorden = " ORDER BY " + ((generico.entLista)this.cmbCampo.getSelectedItem()).getCampo() + " ASC";
            }
            if (this.optDescendente.isSelected()) {
                this.sorden = " ORDER BY " + ((generico.entLista)this.cmbCampo.getSelectedItem()).getCampo() + " DESC";
            }
        } catch(Exception e) {
            this.sorden = "";
        }
    }
    
    public String getOrden() {
        return this.sorden;
    }
    
    public void setOrden(String sorden) {
        this.sorden = sorden;
    }

    public void abrir() {
        this.dlg.show(); // abre el cuadro de diálogo
    }
    
    private void cerrar() {
        this.dlg.setVisible(false);
        this.dlg.dispose(); // cierra el cuadro de diálogo
    }

    public boolean establecido() {
        return this.bestablecido;
    }

    public void setModelo(java.util.ArrayList<generico.entLista> lst) {
        ((generico.modLista)this.cmbCampo.getModel()).cargarModelo(lst);
    }
   
}
