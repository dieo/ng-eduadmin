package generico;

import com.lowagie.text.factories.ElementFactory;
import java.awt.Dialog;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
/**
 *
 * @author EArguello
 */
public class intCargaProceso extends javax.swing.JFrame {   
    //Indica el valor maximo de las barras de carga
    private int maximo=1000;
    public intCargaProceso() {
        initComponents();
        this.setLocation(new java.awt.Point((java.awt.Toolkit.getDefaultToolkit().getScreenSize().width-this.getSize().width)/2, (java.awt.Toolkit.getDefaultToolkit().getScreenSize().height-this.getSize().height)/2));
        //CargaProceso();
    }
    @SuppressWarnings("unchecked") 
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        prgProgreso = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setForeground(new java.awt.Color(204, 255, 255));
        setMinimumSize(new java.awt.Dimension(300, 62));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(300, 62));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        prgProgreso.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        prgProgreso.setMaximum(0);
        prgProgreso.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        prgProgreso.setOpaque(true);
        prgProgreso.setString("Espere unos segundos...");
        prgProgreso.setStringPainted(true);
        jPanel1.add(prgProgreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 280, 20));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 300, 60));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
     public static void main(String args[]) throws ClassNotFoundException {
         java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new intCargaProceso().setVisible(true);
            }
        });
        
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    public javax.swing.JProgressBar prgProgreso;
    // End of variables declaration//GEN-END:variables
}
