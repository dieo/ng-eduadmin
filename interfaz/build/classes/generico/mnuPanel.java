/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class mnuPanel extends javax.swing.JPopupMenu {
    
    public javax.swing.JMenuItem mnuEstablecer = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuCancelar = new javax.swing.JMenuItem();
    private Object tipo;
    
    public mnuPanel() {
        this.aplicarFormato();
        this.add(mnuEstablecer);
        this.add(mnuCancelar);
        this.setActivar(false);
    }

    public void setTipo(Object tipo) {
        this.tipo = tipo;
    }
    
    public Object getTipo() {
        return this.tipo;
    }
    
    private void aplicarFormato() {
        java.awt.Font fuente = new java.awt.Font("Arial", 0, 11);
        mnuEstablecer.setFont(fuente);
        mnuEstablecer.setText("Establecer");
        mnuCancelar.setFont(fuente);
        mnuCancelar.setText("Cancelar");
    }
    
    @Override
    public void setEnabled(boolean bactivo) {
        mnuEstablecer.setEnabled(bactivo);
        mnuCancelar.setEnabled(bactivo);
    }
    
    public void setActivar(boolean bactivo) { // activa o desactiva los controles
        mnuEstablecer.setEnabled(bactivo);
        mnuCancelar.setEnabled(bactivo);
    }

}
