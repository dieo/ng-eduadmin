/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package generico;

/**
 *
 * @author Informatica
 */
public class clsVentana {
    
    javax.swing.JDesktopPane pnl;
    javax.swing.JMenu menuVentanaActiva;
    
    public clsVentana(javax.swing.JDesktopPane pnl, javax.swing.JMenu menuVentanaActiva) {
        this.pnl = pnl;
        this.menuVentanaActiva = menuVentanaActiva;
    }
    
    public boolean existeVentana(javax.swing.JMenuItem menu) {
        javax.swing.JInternalFrame vi[] = this.pnl.getAllFrames();
        for (int i=0; i<vi.length; i++) {
            if (((generico.intMantenimiento)vi[i]).mnuItemActivo.getText().contains(menu.getText())) {
                return true;
            }
        }
        return false;
    }

    public void activarVentana(javax.swing.JMenuItem menu) {
        javax.swing.JMenuItem menuDisparador = new javax.swing.JMenuItem();
        for (int i=0; i<menuVentanaActiva.getItemCount(); i++) {
            menuVentanaActiva.getItem(i).setIcon(null);
            if (menuVentanaActiva.getItem(i).getText().equals(menu.getText())) {
                menuDisparador = menuVentanaActiva.getItem(i);
            }
        }
        javax.swing.JInternalFrame vi[] = this.pnl.getAllFrames();
        for (int i=0; i<vi.length; i++) {
            if (((generico.intMantenimiento)vi[i]).mnuItemActivo.getText().equals(menuDisparador.getText())) {
                try{
                    menuDisparador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/establecer15.png")));
                    ((generico.intMantenimiento)vi[i]).setSelected(true);
                } catch (Exception e){ }
            }
        }
    }

    public void actualizarVentanaActiva() {
        menuVentanaActiva.removeAll();
        javax.swing.JInternalFrame vi[] = this.pnl.getAllFrames();
        for (int i=0; i<vi.length; i++) {
            javax.swing.JMenuItem nuevo = new javax.swing.JMenuItem();
            if (((generico.intMantenimiento)vi[i]).isSelected()) {
                nuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/establecer15.png")));
            }
            nuevo.setFont(((generico.intMantenimiento)vi[i]).mnuItemActivo.getFont());
            nuevo.setText(((generico.intMantenimiento)vi[i]).mnuItemActivo.getText());
            nuevo.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    activarVentana((javax.swing.JMenuItem)evt.getSource());
                }
            });
            menuVentanaActiva.add(nuevo);
        }
    }

    public void marcarMenuActivo(javax.swing.JMenuItem menu) {
        for (int i=0; i<menuVentanaActiva.getItemCount(); i++) {
            menuVentanaActiva.getItem(i).setIcon(null);
        }
        for (int i=0; i<menuVentanaActiva.getItemCount(); i++) {
            if (menuVentanaActiva.getItem(i).getText().equals(menu.getText()))  menuVentanaActiva.getItem(i).setIcon(new javax.swing.ImageIcon(getClass().getResource("/icono/establecer15.png")));
        }
    }

    public void removerMenuActivo(javax.swing.JMenuItem menu) {
        for (int i=0; i<menuVentanaActiva.getItemCount(); i++) {
            if (menuVentanaActiva.getItem(i).getText().equals(menu.getText())) {
                menuVentanaActiva.remove(i);
            }
        }
    }
    
    public void cerrarVentanaActiva(javax.swing.JMenu menuVentanaActiva) {
        javax.swing.JInternalFrame vi[] = this.pnl.getAllFrames();
        for (int i=0; i<vi.length; i++) {
            if (vi[i].isSelected()) vi[i].dispose();
        }
        this.actualizarVentanaActiva();
    }
    
    public void cerrarTodasVentanas(javax.swing.JMenu menuVentanaActiva) {
        javax.swing.JInternalFrame vi[] = this.pnl.getAllFrames();
        for (int i=0; i<vi.length; i++) {
            vi[i].dispose();
        }
        menuVentanaActiva.removeAll();
    }

    public void organizarCascada() {
        javax.swing.JInternalFrame vi[] = this.pnl.getAllFrames();
        for (int i=vi.length-1; i>=0; i--) {
            vi[i].setSize(this.pnl.getSize().width*50/100, this.pnl.getSize().height*50/100);
            vi[i].setLocation(i*30, i*30);
        }
    }
    
    public void organizarVertical() {
        javax.swing.JInternalFrame vi[] = this.pnl.getAllFrames();
        int y = 0;
        for (int i=vi.length-1; i>=0; i--) {
            vi[i].setSize(this.pnl.getSize().width/vi.length, this.pnl.getSize().height);
            vi[i].setLocation(y, 0);
            y += this.pnl.getSize().width/vi.length;
        }
    }
    
    public void organizarHorizontal() {
        javax.swing.JInternalFrame vi[] = this.pnl.getAllFrames();
        int x = 0;
        for (int i=vi.length-1; i>=0; i--) {
            vi[i].setSize(this.pnl.getSize().width, this.pnl.getSize().height/vi.length);
            vi[i].setLocation(0, x);
            x += this.pnl.getSize().height/vi.length;
        }
    }
    
    public void organizarMosaico() {
        javax.swing.JInternalFrame vi[] = this.pnl.getAllFrames();
        int m[][] = {{1,1},{1,2},{2,2},{2,3},{3,3},{3,4},{4,4},{4,5},{5,5},{5,6},{6,6},{6,7},{7,7},{7,8},{8,8},{8,9},{9,9},{9,10},{10,10}};
        int i = 0, f, c;
        while (m[i][0]*m[i][1] < vi.length) i++;
        f = m[i][0];    // cantidad de filas
        c = m[i][1];    // cantidad de columnas
        int comp = f*c; // cantidad de componentes de la matriz
        int ancho = 0;  // dimensión de ancho
        int alto = 0;   // dimensión de alto
        try {
            ancho = this.pnl.getSize().width/c; // dimensión de ancho
            alto = this.pnl.getSize().height/f; // dimensión de alto
        } catch (Exception e) { }
        int ex=0;  // indicador de cantidad de excepciones
        for (int j=0; j<f; j++) {
            for (int k=0; k<c; k++) {
                try {
                    comp--;
                    vi[f*c-1-comp].setSize(ancho, alto);
                    vi[f*c-1-comp].setLocation(k*ancho, j*alto);
                } catch(Exception e) {
                    ex++;
                }
            }
        }
        if (ex > 0) { // hubo excepciones
            ancho = 0;
            try {
                ancho = this.pnl.getSize().width/(c-ex); // dimensión del nuevo ancho
            } catch (Exception e) { }
            for (int k=0; k<c-ex; k++) {
                try {
                    vi[f*c-c+k].setSize(ancho, alto);
                    vi[f*c-c+k].setLocation(k*ancho, (f-1)*alto);
                } catch(Exception e) { }
            }
        }
    }
    
}
