/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package generico;

/**
 *
 * @author Informatica
 */
public class clsPadre {
    
    public clsPadre() {
    }
    
    public static final generico.intPrincipal getPrincipal(Object objeto) {
        try {
            while (!(objeto instanceof generico.intPrincipal)) objeto = ((java.awt.Component)objeto).getParent();
            return (generico.intPrincipal)objeto;
        } catch (Exception e) {
            return null;
        }
    }
    
    public static final generico.intMantenimiento getMantenimiento(Object objeto) {
        try {
            while (!(objeto instanceof generico.intMantenimiento)) objeto = ((java.awt.Component)objeto).getParent();
            return (generico.intMantenimiento)objeto;
        } catch (Exception e) {
            return null;
        }
    }
    
    public static final Object getPadre(Object objeto, Class tipo) {
        try {
            while (!(objeto.getClass() == tipo)) objeto = ((java.awt.Component)objeto).getParent();
            return objeto;
        } catch (Exception e) {
            return null;
        }
    }
    
}
