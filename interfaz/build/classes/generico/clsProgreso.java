/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package generico;

/**
 *
 * @author Informatica
 */
public class clsProgreso implements java.awt.event.ActionListener {

        public javax.swing.JProgressBar barra;
        public javax.swing.Timer timer;

        public clsProgreso() {
        }

        public void iniciarComponentes(javax.swing.JProgressBar barra, int imaximo) {
            this.barra = barra;
            this.barra.setMaximum(imaximo);
            this.barra.setStringPainted(true);
            timer = new javax.swing.Timer(200, new generico.clsProgreso());
        }
        
        public void actionPerformed(java.awt.event.ActionEvent e) {
            if (barra.getValue()<barra.getMaximum()) {
            //    barra.setValue(barra.getValue()+1);
                javax.swing.JOptionPane.showMessageDialog(null, "x");
            } else {
                timer.stop();
                barra.setString("Concluido...");
            }
        }
    
}
