/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;
/**
 *
 * @author Informatica
 */
public class Impresora {

    java.awt.Font fuente = new java.awt.Font("Dialog", java.awt.Font.TYPE1_FONT, 10);
    java.awt.PrintJob pj;
    java.awt.Graphics pagina;
 
    public Impresora() {
        pj = java.awt.Toolkit.getDefaultToolkit().getPrintJob(new java.awt.Frame(), "SCAT", null);
    }
 
    public void imprimir(String Cadena) {
        try {
            pagina = pj.getGraphics();
            pagina.setFont(fuente);
            pagina.setColor(java.awt.Color.black);
            pagina.drawString(Cadena, 60, 60);
            pagina.dispose();
            pj.end();
        } catch(Exception e) { }
    }
    
}
