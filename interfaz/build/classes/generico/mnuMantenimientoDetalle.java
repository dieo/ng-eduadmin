/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Ing. Edison Martinez
 */
public class mnuMantenimientoDetalle extends javax.swing.JPopupMenu {
    
    public javax.swing.JMenuItem mnuInsertar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuModificar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuEliminar = new javax.swing.JMenuItem();
    public javax.swing.JMenuItem mnuVisualizar = new javax.swing.JMenuItem();
    public static String insertar = "Insertar";
    public static String modificar = "Modificar";
    public static String visualizar = "Visualizar";
    public static String eliminar = "Eliminar";

    // LEYENDA DE EDICION DE DETALLE
    public static final String leyenda(boolean bmodo) {
        if (bmodo) return "MODO EDICION (F2 Modificar, F3 Insertar, F4 Visualizar, F5 Eliminar)";
        else return "MODO LECTURA";
    }

    public static final String leyendaMultilinea(boolean bmodo) {
        if (bmodo) return "<html>MODO EDICION<P><html>- F2: Modificar<P><html>- F3: Insertar<P><html>- F4: Visualizar<P><html>- F5: Eliminar<P>";
        else return "MODO LECTURA";
    }
    
    public mnuMantenimientoDetalle() {
        this.aplicarFormato();
        this.add(mnuInsertar);
        this.add(mnuModificar);
        this.add(mnuEliminar);
        this.add(mnuVisualizar);
    }

    public void setEnabled(boolean bmodo) {
        this.mnuInsertar.setEnabled(bmodo);
        this.mnuModificar.setEnabled(bmodo);
        this.mnuEliminar.setEnabled(bmodo);
        this.mnuVisualizar.setEnabled(true);
    }
    
    private void aplicarFormato() {
        java.awt.Font fuente = new java.awt.Font("Arial", 0, 11);
        mnuInsertar.setFont(fuente);
        mnuInsertar.setText(insertar);
        mnuModificar.setFont(fuente);
        mnuModificar.setText(modificar);
        mnuVisualizar.setFont(fuente);
        mnuVisualizar.setText(visualizar);
        mnuEliminar.setFont(fuente);
        mnuEliminar.setText(eliminar);
    }

    public static final String getNombre(int icodigoTecla) {
        if (icodigoTecla==114) return insertar;   // código de la tecla F3
        if (icodigoTecla==113) return modificar;  // código de la tecla F2
        if (icodigoTecla==115) return visualizar; // código de la tecla F4
        if (icodigoTecla==116) return eliminar;   // código de la tecla F5
        return "";
    }
    
}
