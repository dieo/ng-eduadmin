package utilitario;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class utiFecha {

    public static final short edad_mayor = 18;

    public Date getFechaFacturaIndividual() {
        return setFecha(1, 9, 2017);
    }

    public static final Date getPrimerVencimiento(Date dfecha) {
        Date dinicio = new Date(dfecha.getYear(), dfecha.getMonth(), 1);
        dinicio.setMonth(dinicio.getMonth() + 2);
        dinicio.setDate(dinicio.getDate() - 1);
        return dinicio;
    }

    public static final Date getPrimerVencimientoIngreso(Date dfecha) {
        Date dinicio = new Date(dfecha.getYear(), dfecha.getMonth(), 1);
        dinicio.setMonth(dinicio.getMonth() + 1);
        dinicio.setDate(dinicio.getDate() - 1);
        return dinicio;
    }

    public static final Date getVencimiento(Date dinicio, int iplazo) {
        if (iplazo == 0) {
            return null;
        }
        Date dfecha = new Date(dinicio.getYear(), dinicio.getMonth(), 1);
        for (int i = 0; i < iplazo; i++) {
            dfecha.setMonth(dfecha.getMonth() + 1);
        }
        dfecha.setDate(dfecha.getDate() - 1);
        return dfecha;
    }

    public static Object convertirToDateAMD(Date aux) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public static final String leyenda(boolean bmodo) {
        if (bmodo) {
            return "MODO EDICION (F2: MOD. - F3: INS. - F4: VIS. - F5: ELI.)";
        }
        return "MODO LECTURA";
    }

    public static final String leyendaMultilinea(boolean bmodo) {
        if (bmodo) {
            return "<html>MODO EDICION<P><html>- F2: MOD.<P><html>- F3: INS.<P><html>- F4: VIS.<P><html>- F5: ELI.<P>";
        }
        return "MODO LECTURA";
    }

    public static final Date convertirToDate(Date dfecha) {
        Date fecha = (Date) dfecha.clone();
        fecha.setYear(fecha.getYear() - 1900);
        fecha.setMonth(fecha.getMonth() - 1);
        return fecha;
    }

    public static final Date setFecha(int idia, int imes, int iano) {
        Date fecha = new Date();
        fecha.setDate(idia);
        fecha.setMonth(imes - 1);
        fecha.setYear(iano - 1900);
        return fecha;
    }

    public static final Date convertirToDateDMA(String sfecha) {
        SimpleDateFormat formatoFechaDMA = new SimpleDateFormat("dd/MM/yyyy");
        Date dfecha;
        try {
            dfecha = formatoFechaDMA.parse(sfecha);
        } catch (Exception e) {
            dfecha = null;
        }
        return dfecha;
    }

    public static final Date convertirToDateAMD(String sfecha) {
        SimpleDateFormat formatoFechaDMA = new SimpleDateFormat("yyyy/MM/dd");
        Date dfecha;
        try {
            dfecha = formatoFechaDMA.parse(sfecha);
        } catch (Exception e) {
            dfecha = null;
        }
        return dfecha;
    }

    public static final String convertirToStringDMA(Date dfecha) {
        SimpleDateFormat formatoFechaDMA = new SimpleDateFormat("dd/MM/yyyy");
        String sfecha;
        try {
            sfecha = formatoFechaDMA.format(dfecha);
        } catch (Exception e) {
            sfecha = "";
        }
        return sfecha;
    }
    
    public static final String convertirToStringDMAMac(Date dfecha) {
        SimpleDateFormat formatoFechaDMA = new SimpleDateFormat("yyyy-MM-dd");
        String sfecha;
        try {
            sfecha = formatoFechaDMA.format(dfecha);
        } catch (Exception e) {
            sfecha = "";
        }
        return sfecha;
    }

    public static final String convertirToStringDMACorto(Date dfecha) {
        SimpleDateFormat formatoFechaDMA = new SimpleDateFormat("dd/MM/yy");
        String sfecha;
        try {
            sfecha = formatoFechaDMA.format(dfecha);
        } catch (Exception e) {
            sfecha = "";
        }
        return sfecha;
    }

    public static final String getFechaGuardado(Date dfecha) {
        String sfecha = convertirToStringDMA(dfecha);
        if (sfecha.isEmpty()) {
            return null;
        }
        return "'" + sfecha + "'";
    }
    
    
    
    public static final String getFechaGuardadoMac(Date dfecha) {
        String sfecha = convertirToStringDMAMac(dfecha);
        if (sfecha.isEmpty()) {
            return null;
        }
        return "'" + sfecha + "'";
    }

    public static final boolean esValidoHoraCorta(String stime) {
        if (stime.length() != 5) {
            return false;
        }
        if (utiNumero.convertirToInt(stime.substring(0, 2)) > 23) {
            return false;
        }
        if (utiNumero.convertirToInt(stime.substring(3, 5)) > 59) {
            return false;
        }
        return true;
    }

    public static final boolean esValidoHoraLarga(String stime) {
        if (stime.length() != 8) {
            return false;
        }
        if (utiNumero.convertirToInt(stime.substring(0, 2)) > 23) {
            return false;
        }
        if (utiNumero.convertirToInt(stime.substring(3, 5)) > 59) {
            return false;
        }
        if (utiNumero.convertirToInt(stime.substring(6, 8)) > 59) {
            return false;
        }
        return true;
    }

    public static final int[] obtenerDiferencia(Date finicio, Date ffinal) {
        int[] cant = {0, 0, 0};
        if ((finicio == null) || (ffinal == null)) {
            return cant;
        }
        DateFormat df = DateFormat.getDateInstance(2);
        try {
            finicio = df.parse(df.format(finicio));
            ffinal = df.parse(df.format(ffinal));
        } catch (ParseException ex) {
            return cant;
        }
        if (getAMD(ffinal) <= getAMD(finicio)) {
            return cant;
        }
        while (getAMD(ffinal) >= getAMD(finicio)) {
            finicio.setYear(finicio.getYear() + 1);
            if (getAMD(ffinal) >= getAMD(finicio)) {
                cant[0] += 1;
            }
        }
        if ((finicio.getMonth() == ffinal.getMonth()) && (finicio.getDate() == ffinal.getDate())) {
            return cant;
        }
        finicio.setYear(finicio.getYear() - 1);
        while (getAMD(ffinal) >= getAMD(finicio)) {
            finicio.setMonth(finicio.getMonth() + 1);
            if (getAMD(ffinal) >= getAMD(finicio)) {
                cant[1] += 1;
            }
        }
        finicio.setMonth(finicio.getMonth() - 1);
        while (getAMD(ffinal) >= getAMD(finicio)) {
            finicio.setDate(finicio.getDate() + 1);
            if (getAMD(ffinal) >= getAMD(finicio)) {
                cant[2] += 1;
            }
        }
        return cant;
    }

    public static final String obtenerAntiguedad(Date finicio, Date ffinal) {
        int[] cant = {0, 0, 0};
        if ((finicio == null) || (ffinal == null)) {
            return "";
        }
        DateFormat df = DateFormat.getDateInstance(2);
        try {
            finicio = df.parse(df.format(finicio));
            ffinal = df.parse(df.format(ffinal));
        } catch (ParseException ex) {
            return "";
        }
        if (getAMD(ffinal) <= getAMD(finicio)) {
            return "";
        }
        while (getAMD(ffinal) >= getAMD(finicio)) {
            finicio.setYear(finicio.getYear() + 1);
            if (getAMD(ffinal) >= getAMD(finicio)) {
                cant[0] += 1;
            }
        }
        if ((finicio.getMonth() == ffinal.getMonth()) && (finicio.getDate() == ffinal.getDate())) {
            return "";
        }
        finicio.setYear(finicio.getYear() - 1);
        while (getAMD(ffinal) >= getAMD(finicio)) {
            finicio.setMonth(finicio.getMonth() + 1);
            if (getAMD(ffinal) >= getAMD(finicio)) {
                cant[1] += 1;
            }
        }
        finicio.setMonth(finicio.getMonth() - 1);
        while (getAMD(ffinal) >= getAMD(finicio)) {
            finicio.setDate(finicio.getDate() + 1);
            if (getAMD(ffinal) >= getAMD(finicio)) {
                cant[2] += 1;
            }
        }
        return cant[0] + "a." + "  " + cant[1] + "m." + "  " + cant[2] + "d.";
    }

    public static int obtenerDiferenciaDia(Date finicio, Date ffinal) {
        int dia = 0;
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        try {
            finicio = df.parse(df.format(finicio));
            ffinal = df.parse(df.format(ffinal));
        } catch (ParseException ex) {
            return dia;
        }
        if (getAMD(ffinal) == getAMD(finicio)) {
            return dia;
        }
        if (getAMD(ffinal) > getAMD(finicio)) {
            while (getAMD(ffinal) >= getAMD(finicio)) {
                finicio.setDate(finicio.getDate() + 1);
                if (getAMD(ffinal) >= getAMD(finicio)) {
                    dia++;
                }
            }
        }
        while (getAMD(ffinal) <= getAMD(finicio)) {
            ffinal.setDate(ffinal.getDate() + 1);
            if (getAMD(ffinal) <= getAMD(finicio)) {
                dia--;
            }
        }
        return dia + 1;
    }

    public static final int[] obtenerDiferenciaMes(Date finicio, Date ffinal) {
        int[] cant = {0, 0};
        DateFormat df = DateFormat.getDateInstance(2);
        try {
            finicio = df.parse(df.format(finicio));
            ffinal = df.parse(df.format(ffinal));
        } catch (ParseException ex) {
            return cant;
        }
        if (getAMD(ffinal) <= getAMD(finicio)) {
            return cant;
        }
        while (getAMD(ffinal) >= getAMD(finicio)) {
            finicio.setMonth(finicio.getMonth() + 1);
            if (getAMD(ffinal) >= getAMD(finicio)) {
                cant[0] += 1;
            }
        }
        finicio.setMonth(finicio.getMonth() - 1);
        while (getAMD(ffinal) >= getAMD(finicio)) {
            finicio.setDate(finicio.getDate() + 1);
            if (getAMD(ffinal) >= getAMD(finicio)) {
                cant[1] += 1;
            }
        }
        return cant;
    }

    public static final int getAMD(Date dfecha) {
        if (dfecha == null) {
            return 0;
        }
        return (dfecha.getYear() + 1900) * 10000 + (dfecha.getMonth() + 1) * 100 + dfecha.getDate();
    }

    public static final int getAM(Date dfecha) {
        if (dfecha == null) {
            return 0;
        }
        return (dfecha.getYear() + 1900) * 10000 + (dfecha.getMonth() + 1) * 100;
    }

    public static final boolean esBisiesto(int a) {
        if (((a % 4 == 0) && (a % 100 != 0)) || (a % 400 == 0)) {
            return true;
        }
        return false;
    }

    public static final Date getPrimerDia(Date dfecha) {
        Date aux = (Date) dfecha.clone();
        aux.setDate(1);
        return aux;
    }

    public static final Date getUltimoDia(Date dfecha) {
        Date aux = (Date) dfecha.clone();
        aux.setDate(1);
        aux.setMonth(aux.getMonth() + 1);
        aux.setDate(aux.getDate() - 1);
        return aux;
    }

    public static final String[] getMeses() {
        return new String[]{"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"};
    }

    public static final String[] getDias() {
        return new String[]{"Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado"};
    }

    public static final int getMes(String smes) {
        for (int i = 0; i < getMeses().length; i++) {
            if (getMeses()[i].equals(smes)) {
                return i;
            }
        }
        return -1;
    }

    public static final Date calcularSiguienteVencimiento(Date dfecha) {
        Date nuevaFecha = (Date) dfecha.clone();
        nuevaFecha.setDate(1);
        nuevaFecha.setMonth(nuevaFecha.getMonth() + 2);
        nuevaFecha.setDate(nuevaFecha.getDate() - 1);
        return nuevaFecha;
    }
    
    public static final Date calcularSiguienteVencimientoMensual(Date dfecha) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dfecha);
        cal.add(Calendar.MONTH, 1);
        return cal.getTime();
    }

    public static final Date getMesAnterior(Date dfecha) {
        Date nuevaFecha = (Date) dfecha.clone();
        nuevaFecha.setDate(1);
        nuevaFecha.setDate(nuevaFecha.getDate() - 1);
        return nuevaFecha;
    }

    public static final int getDia(Date dfecha) {
        int dia = dfecha.getDate();
        return dia;
    }

    public static final String getMes(Date dfecha) {
        int mes = dfecha.getMonth();
        return getMeses()[mes].toLowerCase();
    }

    public static final int getAnho(Date dfecha) {
        int anho = dfecha.getYear();
        return anho + 1900;
    }

    public static final String getNumeroMes(Date dfecha) {
        int mes = dfecha.getMonth();
        mes += 101;
        return utiNumero.convertirToString(mes).substring(1);
    }

    public static final String getHora(Date dfecha) {
        String shora = Integer.valueOf(dfecha.getHours() + 100).toString().substring(1);
        String sminuto = Integer.valueOf(dfecha.getMinutes() + 100).toString().substring(1);
        String ssegundo = Integer.valueOf(dfecha.getSeconds() + 100).toString().substring(1);
        return shora + ":" + sminuto + ":" + ssegundo;
    }
}
