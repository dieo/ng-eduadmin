package utilitario;

import generico.entLista;
import java.util.ArrayList;
import javax.swing.JTable;

public class utiCadena
{
  public static final short edad_mayor = 18;
  
  public static final String getTextoGuardado(String stexto)
  {
    if (stexto == null) {
      return stexto;
    }
    if (stexto.isEmpty()) {
      return null;
    }
    return "'" + stexto + "'";
  }
  
  public static final String sacarCaracter(String scadena, String scaracterViejo, String scaracterNuevo)
  {
    for (int i = 0; i < scadena.length(); i++) {
      scadena = scadena.replace(scaracterViejo, scaracterNuevo);
    }
    return scadena;
  }
  
  public static final String cambiarCaracter(String scadena, String scaracterViejo, String scaracterNuevo)
  {
    try
    {
      scadena = scadena.replace(scaracterViejo, scaracterNuevo);
    }
    catch (Exception e) {}
    return scadena;
  }
  
  public static final String resumirCuota(ArrayList<entLista> lst)
  {
    String resumen = "";
    for (int i = 0; i < lst.size() - 1; i++) {
      if (i == 0)
      {
        if (((entLista)lst.get(i)).getId() - ((entLista)lst.get(i + 1)).getId() == -1) {
          ((entLista)lst.get(i)).setCampo("-");
        }
        if (((entLista)lst.get(i)).getId() - ((entLista)lst.get(i + 1)).getId() != -1) {
          ((entLista)lst.get(i)).setCampo(",");
        }
      }
      else
      {
        if ((((entLista)lst.get(i)).getId() - ((entLista)lst.get(i - 1)).getId() == 1) && (((entLista)lst.get(i)).getId() - ((entLista)lst.get(i + 1)).getId() == -1)) {
          ((entLista)lst.get(i)).setCampo("*");
        }
        if (((entLista)lst.get(i)).getCampo().isEmpty())
        {
          if (((entLista)lst.get(i)).getId() - ((entLista)lst.get(i - 1)).getId() != -1) {
            ((entLista)lst.get(i)).setCampo("-");
          }
          if (((entLista)lst.get(i)).getId() - ((entLista)lst.get(i + 1)).getId() != -1) {
            ((entLista)lst.get(i)).setCampo(",");
          }
        }
      }
    }
    int k = 0;
    while (k < lst.size()) {
      if (((entLista)lst.get(k)).getCampo().equals("*")) {
        lst.remove(k);
      } else {
        k++;
      }
    }
    for (int i = 0; i < lst.size(); i++) {
      resumen = resumen + ((entLista)lst.get(i)).getId() + ((entLista)lst.get(i)).getCampo();
    }
    return resumen;
  }
  
  public static final int obtenerPrioridad(String sprioridad)
  {
    int iprioridad = 0;
    try
    {
      iprioridad += utiNumero.convertirToInt(sprioridad.substring(0, 2)) * 1000000;
      iprioridad += utiNumero.convertirToInt(sprioridad.substring(3, 5)) * 10000;
      iprioridad += utiNumero.convertirToInt(sprioridad.substring(6, 8)) * 100;
      iprioridad += utiNumero.convertirToInt(sprioridad.substring(9, 11));
    }
    catch (Exception e) {}
    return iprioridad;
  }
  
  public static final String convertirMayusMinus(String scadena)
  {
    if (scadena == null) {
      return "";
    }
    if (scadena.isEmpty()) {
      return "";
    }
    scadena = scadena.toLowerCase();
    for (int i = 0; i < scadena.length(); i++) {
      if ((i == 0) || ((i > 0) && (" ./(-".contains(scadena.substring(i - 1, i))))) {
        scadena = scadena.substring(0, i) + scadena.substring(i, i + 1).toUpperCase() + scadena.substring(i + 1, scadena.length());
      }
    }
    return scadena;
  }
  
  public static final int getPosicion(JTable tbl)
  {
    int posicion = tbl.getSelectedRow();
    if (posicion < 0) {
      posicion = 0;
    }
    return posicion;
  }
  
  public static final void setPosicion(JTable tbl, int posicion)
  {
    try
    {
      if (posicion >= tbl.getRowCount() - 1) {
        posicion = tbl.getRowCount() - 1;
      }
      tbl.setRowSelectionInterval(posicion, posicion);
    }
    catch (Exception e)
    {
      tbl.clearSelection();
    }
  }
  
  public static final int getPosicion(String scadena, String ssubcadena, int idesde)
  {
    for (int i = idesde; i < scadena.length(); i++) {
      if (scadena.substring(i, ssubcadena.length() + i).equals(ssubcadena)) {
        return i;
      }
    }
    return scadena.length();
  }
  
  public static String rellenar(String cadena, String caracter, int cantidad, boolean derecha)
  {
    String aux = "";String res = "";
    for (int i = 0; i < 255; i++) {
      aux = aux + caracter;
    }
    if (derecha) {
      res = cadena + aux.substring(0, cantidad - cadena.length());
    } else {
      res = aux.substring(0, cantidad - cadena.length()) + cadena;
    }
    return res;
  }
  
  public static final String getNumeroDocumento(String slocal, String spuntoExpedicion, int inumero)
  {
    if (inumero == 0) {
      return "";
    }
    return slocal.trim() + "-" + spuntoExpedicion.trim() + "-" + utiNumero.convertirToString(inumero + 100000000).substring(1);
  }
  
  public static final String getNumeroDocumentoLocal(String snumeroDocumento)
  {
    return snumeroDocumento.substring(0, 3);
  }
  
  public static final String getNumeroDocumentoPuntoExpedicion(String snumeroDocumento)
  {
    return snumeroDocumento.substring(4, 7);
  }
  
  public static final String getNumeroDocumentoNumero(String snumeroDocumento)
  {
    return snumeroDocumento.substring(8, 16);
  }
}
