/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import javax.swing.JList;
import javax.swing.JOptionPane;

public class comList
{
  public JList lst;
  public modLista mod;
  public String sentencia;
  private String shost;
  private String sbaseDato;
  private String sport;
  
  public comList() {}
  
  public comList(JList nlst, String sentencia, String shost, String sbaseDato, String sport)
  {
    this.lst = nlst;
    this.sentencia = sentencia;
    this.shost = shost;
    this.sbaseDato = sbaseDato;
    this.sport = sport;
    this.mod = new modLista();
    this.lst.setModel(this.mod);
    obtenerDato();
  }
  
  public void setSentencia(String sentencia)
  {
    this.sentencia = sentencia;
  }
  
  public boolean isEnabled()
  {
    return this.lst.isEnabled();
  }
  
  public void setEnabled(boolean bmodo)
  {
    this.lst.setEnabled(bmodo);
  }
  
  public void setToolTipText(String stexto)
  {
    this.lst.setToolTipText(stexto);
  }
  
  public void refresh()
  {
    obtenerDato();
    this.lst.updateUI();
  }
  
  public void obtenerDato()
  {
    conConexion con = new conConexion();
    con.setHostBaseDatos(this.shost, this.sbaseDato, this.sport);
    con.setConexion(true, null, null);
    this.mod.removerTodo();
    if (con.ejecutarSentencia(this.sentencia)) {
      this.mod.cargarModelo(con.rs);
    } else {
      JOptionPane.showMessageDialog(null, con.getMensaje());
    }
  }
  
  public entLista getEntidad()
  {
    return this.mod.getEntidad(this.lst.getSelectedIndex());
  }
}
