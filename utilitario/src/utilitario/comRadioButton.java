/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

public class comRadioButton
{
  public ButtonGroup gro;
  public String sentencia;
  private String shost;
  private String sbaseDato;
  private String sport;
  public modLista mod = new modLista();
  
  public comRadioButton() {}
  
  public comRadioButton(ButtonGroup gro, String sentencia, String shost, String sbaseDato, String sport)
  {
    this.gro = gro;
    this.sentencia = sentencia;
    this.shost = shost;
    this.sbaseDato = sbaseDato;
    this.sport = sport;
    obtenerDato();
    establecerItem();
  }
  
  public void setEnabled(boolean bactivo)
  {
    for (Enumeration<AbstractButton> e = this.gro.getElements(); e.hasMoreElements();)
    {
      AbstractButton button = (AbstractButton)e.nextElement();
      if ((button instanceof JRadioButton))
      {
        JRadioButton opt = (JRadioButton)button;
        opt.setEnabled(bactivo);
      }
    }
  }
  
  public void setToolTipText(String stexto)
  {
    for (Enumeration<AbstractButton> e = this.gro.getElements(); e.hasMoreElements();)
    {
      AbstractButton button = (AbstractButton)e.nextElement();
      if ((button instanceof JRadioButton))
      {
        JRadioButton opt = (JRadioButton)button;
        opt.setToolTipText(stexto);
      }
    }
  }
  
  public void obtenerDato()
  {
    conConexion con = new conConexion();
    con.setHostBaseDatos(this.shost, this.sbaseDato, this.sport);
    con.setConexion(true, null, null);
    this.mod.removerTodo();
    if (con.ejecutarSentencia(this.sentencia)) {
      this.mod.cargarModelo(con.rs);
    } else {
      JOptionPane.showMessageDialog(null, con.getMensaje());
    }
    con.setConexion(false, null, null);
  }
  
  public void setId(int iid)
  {
    this.gro.clearSelection();
    Enumeration<AbstractButton> e;
    for (int i = 0; i < this.mod.getSize(); i++) {
      if (this.mod.getEntidad(i).getId() == iid) {
        for (e = this.gro.getElements(); e.hasMoreElements();)
        {
          AbstractButton button = (AbstractButton)e.nextElement();
          if ((button instanceof JRadioButton))
          {
            JRadioButton opt = (JRadioButton)button;
            if (this.mod.getEntidad(i).getDescripcion().equals(opt.getText()))
            {
              opt.setSelected(true);
              return;
            }
          }
        }
      }
    }
  }
  
  public short getIdSeleccionado()
  {
    for (Enumeration<AbstractButton> e = this.gro.getElements(); e.hasMoreElements();)
    {
      AbstractButton button = (AbstractButton)e.nextElement();
      if ((button instanceof JRadioButton))
      {
        JRadioButton opt = (JRadioButton)button;
        if (opt.isSelected()) {
          for (int j = 0; j < this.mod.getSize(); j++) {
            if (opt.getText().equals(this.mod.getEntidad(j).getDescripcion())) {
              return (short)this.mod.getEntidad(j).getId();
            }
          }
        }
      }
    }
    return 0;
  }
  
  public short getId(String sdescripcion)
  {
    return this.mod.getId(sdescripcion);
  }
  
  public String getDescripcionSeleccionado()
  {
    for (Enumeration<AbstractButton> e = this.gro.getElements(); e.hasMoreElements();)
    {
      AbstractButton button = (AbstractButton)e.nextElement();
      if ((button instanceof JRadioButton))
      {
        JRadioButton opt = (JRadioButton)button;
        if (opt.isSelected()) {
          return opt.getText();
        }
      }
    }
    return "";
  }
  
  public final void establecerItem()
  {
    int i = 0;
    for (Enumeration<AbstractButton> e = this.gro.getElements(); e.hasMoreElements();)
    {
      AbstractButton button = (AbstractButton)e.nextElement();
      if ((button instanceof JRadioButton))
      {
        JRadioButton opt = (JRadioButton)button;
        opt.setText(this.mod.getEntidad(i).getDescripcion());
      }
      i++;
    }
  }
}