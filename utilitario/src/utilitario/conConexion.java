/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package utilitario;

import especifico.entUsuario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import javax.swing.JOptionPane;

public class conConexion
{
  public static String clase = "org.postgresql.Driver";
  public static String odbc = "jdbc:postgresql";
  public static String host = "localhost";
  public static String baseDato = "Mutualmspjun";
  public static final String user = "postgres";
  public static final String password = "1";
  public static String port = "5432";
  public ResultSet rs;
  public entUsuario usuario;
  private Connection conexion;
  private Statement sentencia;
  private String mensaje;
  
  public conConexion()
  {
    this.usuario = new entUsuario();
    this.mensaje = "";
  }
  
  public String getMensaje()
  {
    return this.mensaje;
  }
  
  public int getRowCountRs()
  {
    int total = 0;
    try
    {
      this.rs.last();
      total = this.rs.getRow();
    }
    catch (Exception e) {}
    return total;
  }
  
  public void setHostBaseDatos(String shost, String sbaseDato, String sport)
  {
    host = shost;
    baseDato = sbaseDato;
    port = sport;
  }
  
  public boolean setConexion(boolean bconexion, String user, String password)
  {
    if (bconexion)
    {
      try
      {
        Class.forName(clase);
      }
      catch (Exception e)
      {
        JOptionPane.showMessageDialog(null, "Error al abrir conexi�n: " + e.toString());
        return false;
      }
      try
      {
        if (user == null) {
          this.conexion = DriverManager.getConnection(odbc + "://" + host + ":" + port + "/" + baseDato, "postgres", "1");
        }
        if (user != null) {
          this.conexion = DriverManager.getConnection(odbc + "://" + host + ":" + port + "/" + baseDato, user, password);
        }
      }
      catch (Exception e)
      {
        JOptionPane.showMessageDialog(null, "Error al abrir conexi�n: Usuario y/o Contrase�a fallidos");
        return false;
      }
    }
    else
    {
      try
      {
        this.conexion.close();
      }
      catch (Exception e)
      {
        JOptionPane.showMessageDialog(null, "Error al cerrar conexion: " + e.toString());
        return false;
      }
      return true;
    }
    return true;
  }
  
  public boolean ejecutarSentencia(String scomando)
  {
    try
    {
      this.sentencia = this.conexion.createStatement(1004, 1008);
      this.rs = this.sentencia.executeQuery(scomando);
    }
    catch (Exception e)
    {
      this.mensaje = ("Error al ejecutar sentencia: " + e.toString());
      return false;
    }
    return true;
  }
  
  public boolean ejecutarSentencia(String scomando, ResultSet rs)
  {
    try
    {
      this.sentencia = this.conexion.createStatement(1004, 1008);
      rs = this.sentencia.executeQuery(scomando);
    }
    catch (Exception e)
    {
      this.mensaje = ("Error al ejecutar sentencia: " + e.toString());
      return false;
    }
    return true;
  }
  
  public boolean ejecutarActualizacion(String scomando)
  {
    try
    {
      this.sentencia.executeUpdate(scomando);
    }
    catch (Exception e)
    {
      this.mensaje = ("Error al ejecutar sentencia: " + e.toString());
      return false;
    }
    return true;
  }
  
  public int obtenerId(String stabla)
  {
    if (!ejecutarSentencia("SELECT obtenerid('" + stabla + "')")) {
      return -1;
    }
    try
    {
      this.rs.first();
      return this.rs.getInt(1);
    }
    catch (Exception e) {}
    return 0;
  }
  
  public int obtenerNumeroCuota(int iid, String stabla)
  {
    if (!ejecutarSentencia("SELECT obtenernumerocuota(" + iid + "," + utiCadena.getTextoGuardado(stabla) + ")")) {
      return -1;
    }
    try
    {
      this.rs.first();
      return this.rs.getInt(1);
    }
    catch (Exception e) {}
    return 0;
  }
  
  public int obtenerId(String stabla, String scondicion)
  {
    if (!ejecutarSentencia("SELECT * FROM " + stabla + " WHERE " + scondicion)) {
      return -1;
    }
    try
    {
      this.rs.first();
      return this.rs.getInt(1);
    }
    catch (Exception e) {}
    return 0;
  }
  
  public int getId(String codigo)
  {
    if (!ejecutarSentencia("SELECT id FROM cuenta WHERE codigo='" + codigo + "'")) {
      return -1;
    }
    try
    {
      this.rs.first();
      return this.rs.getInt(1);
    }
    catch (Exception e) {}
    return 0;
  }
  
  public String getCampo(String codigo, String campo)
  {
    if (!ejecutarSentencia("SELECT " + campo + " FROM cuenta WHERE codigo='" + codigo + "'")) {
      return "";
    }
    try
    {
      this.rs.first();
      return this.rs.getString(1);
    }
    catch (Exception e) {}
    return "";
  }
  
  public Object obtenerCampo(String stabla, String scondicion, String scolumna)
  {
    if (!ejecutarSentencia("SELECT * FROM " + stabla + " WHERE " + scondicion)) {
      return Integer.valueOf(-1);
    }
    try
    {
      this.rs.first();
      return this.rs.getObject(scolumna);
    }
    catch (Exception e) {}
    return new Object();
  }
  
  public Object getCampo(String stabla, String scondicion, String scolumna)
  {
    if (!ejecutarSentencia("SELECT * FROM " + stabla + " WHERE " + scondicion)) {
      return Integer.valueOf(-1);
    }
    try
    {
      this.rs.first();
      return this.rs.getObject(scolumna);
    }
    catch (Exception e) {}
    return new Object();
  }
  
  public int obtenerIdSubtipo(String stipo, String ssubtipo)
  {
    if (!ejecutarSentencia("SELECT * FROM vwsubtipo WHERE tipo LIKE '" + stipo + "' AND descripcion LIKE '" + ssubtipo + "'")) {
      return -1;
    }
    try
    {
      this.rs.first();
      return this.rs.getInt("id");
    }
    catch (Exception e) {}
    return 0;
  }
  
  public String obtenerDescripcionSubtipo(int iid)
  {
    if (!ejecutarSentencia("SELECT * FROM vwsubtipo WHERE id=" + iid)) {
      return "";
    }
    try
    {
      this.rs.first();
      return this.rs.getString("descripcion");
    }
    catch (Exception e) {}
    return "";
  }
  
  public int obtenerUltimaBoleta(int iid)
  {
    if (!ejecutarSentencia("SELECT COALESCE(dt.secuencia+1, t.numeroinicial) AS secuencia FROM talonario AS t LEFT JOIN detalletalonario AS dt ON t.id=dt.idtalonario WHERE t.id=" + iid + " ORDER BY dt.secuencia DESC LIMIT 1")) {
      return -1;
    }
    try
    {
      this.rs.first();
      return this.rs.getInt(1);
    }
    catch (Exception e) {}
    return 0;
  }
  
  public String getConsulta(String svista, String sfiltro, String sorden)
  {
    return "SELECT * FROM " + svista + " WHERE 0=0 " + sfiltro + sorden;
  }
  
  public boolean generarCuota(int iidMovimiento, String stabla)
  {
    return ejecutarSentencia("SELECT generarcuota(" + iidMovimiento + "," + utiCadena.getTextoGuardado(stabla) + ")");
  }
  
  public boolean generarCuota(int iidMovimiento, String stabla, Date dfechaVencimiento)
  {
    return ejecutarSentencia("SELECT generarcuota(" + iidMovimiento + "," + utiCadena.getTextoGuardado(stabla) + "," + utiFecha.getFechaGuardado(dfechaVencimiento) + ")");
  }
  
  public entFechaCierre obtenerFechaCierre(int itipo, int iid)
  {
    String ssentencia = "";
    if (itipo == 1) {
      ssentencia = "SELECT fc.* FROM socio AS s LEFT JOIN lugarlaboral AS l ON s.id=l.idsocio LEFT JOIN fechacierre AS fc ON l.idgiraduria=fc.idgiraduria WHERE s.id=" + iid + " AND l.activo=TRUE ORDER BY fc.fechacorte DESC LIMIT 1";
    }
    if (itipo == 2) {
      ssentencia = "SELECT fc.* FROM fechacierre AS fc WHERE fc.idgiraduria=" + iid + " ORDER BY fc.fechacorte DESC LIMIT 1";
    }
    modFechaCierre mod = new modFechaCierre();
    if (ejecutarSentencia(ssentencia))
    {
      mod.cargarTable(this.rs);
      if (mod.getRowCount() > 0) {
        return mod.getEntidad(0);
      }
    }
    return new entFechaCierre();
  }
  
  public entFechaCierre obtenerFechaCierre(int iidSocio, Date dfecha)
  {
    String ssentencia = "SELECT fc.* FROM socio AS s LEFT JOIN lugarlaboral AS l ON s.id=l.idsocio LEFT JOIN fechacierre AS fc ON l.idgiraduria=fc.idgiraduria WHERE l.activo=TRUE AND s.id=" + iidSocio + " AND fc.fechacorte=" + utiFecha.getFechaGuardado(utiFecha.getUltimoDia(dfecha));
    modFechaCierre mod = new modFechaCierre();
    if (ejecutarSentencia(ssentencia))
    {
      mod.cargarTable(this.rs);
      if (mod.getRowCount() > 0) {
        return mod.getEntidad(0);
      }
    }
    return new entFechaCierre();
  }
  
  public entFechaCierre obtenerUltimoCierre()
  {
    String ssentencia = "SELECT fc.* FROM fechacierre AS fc ORDER BY fc.fechaproceso DESC LIMIT 1";
    modFechaCierre mod = new modFechaCierre();
    if (ejecutarSentencia(ssentencia))
    {
      mod.cargarTable(this.rs);
      if (mod.getRowCount() > 0) {
        return mod.getEntidad(0);
      }
    }
    return new entFechaCierre();
  }
}
