/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

public class comComboBox
{
  public JComboBox cmb;
  public JButton refresh;
  public JLabel etiqueta;
  public modLista mod;
  public String sentencia;
  private String shost;
  private String sbaseDato;
  private String sport;
  
  public comComboBox() {}
  
  public comComboBox(JComboBox ncmb, ArrayList lst)
  {
    this.cmb = ncmb;
    this.mod = new modLista();
    this.cmb.setModel(this.mod);
    AutoCompleteDecorator.decorate(this.cmb);
    for (int i = 0; i < lst.size(); i++) {
      this.mod.agregarItem(new entLista(0, lst.get(i).toString(), "", (short)0));
    }
  }
  
  public void setId(String codigo)
  {
    this.cmb.setSelectedIndex(-1);
    for (int i = 0; i < this.mod.getSize(); i++) {
      if (this.mod.getEntidad(i).getDescripcion().equals(codigo)) {
        this.cmb.setSelectedIndex(i);
      }
    }
  }
  
  public comComboBox(JComboBox ncmb, JLabel etiqueta, String sentencia, String shost, String sbaseDato, String sport)
  {
    this.cmb = ncmb;
    this.etiqueta = etiqueta;
    this.sentencia = sentencia;
    this.shost = shost;
    this.sbaseDato = sbaseDato;
    this.sport = sport;
    this.mod = new modLista();
    this.cmb.setModel(this.mod);
    AutoCompleteDecorator.decorate(this.cmb);
    obtenerDato();
    try
    {
      this.etiqueta.addMouseListener(new MouseAdapter()
      {
        @Override
        public void mouseClicked(MouseEvent evt)
        {
          comComboBox.this.obtenerDato();
          comComboBox.this.cmb.updateUI();
        }
      });
    }
    catch (Exception e) {}
  }
  
  public void setSentencia(String sentencia)
  {
    this.sentencia = sentencia;
  }
  
  public boolean isEnabled()
  {
    return this.cmb.isEnabled();
  }
  
  public void setEnabled(boolean bmodo)
  {
    this.cmb.setEnabled(bmodo);
    try
    {
      this.refresh.setEnabled(bmodo);
    }
    catch (Exception e) {}
  }
  
  public void setToolTipText(String stexto)
  {
    this.cmb.setToolTipText(stexto);
    try
    {
      this.refresh.setToolTipText("Refrescar");
    }
    catch (Exception e) {}
  }
  
  public void refresh()
  {
    obtenerDato();
    this.cmb.updateUI();
  }
  
  public void obtenerDato()
  {
    conConexion con = new conConexion();
    con.setHostBaseDatos(this.shost, this.sbaseDato, this.sport);
    con.setConexion(true, null, null);
    this.mod.removerTodo();
    if (con.ejecutarSentencia(this.sentencia)) {
      this.mod.cargarModelo(con.rs);
    } else {
      JOptionPane.showMessageDialog(null, con.getMensaje());
    }
    con.setConexion(false, null, null);
  }
  
  public void setId(int hid)
  {
    this.cmb.setSelectedItem(this.mod.setItem(hid));
  }
  
  public short getId(String sdescripcion)
  {
    return this.mod.getId(sdescripcion);
  }
  
  public short getIdSeleccionado()
  {
    try
    {
      return (short)this.mod.getEntidad().getId();
    }
    catch (Exception e) {}
    return 0;
  }
  
  public String getDescripcionSeleccionado()
  {
    try
    {
      return this.mod.getEntidad().getDescripcion();
    }
    catch (Exception e) {}
    return "";
  }
}