package utilitario;

import java.text.DecimalFormat;

public class utiNumero {

    public static final String convertirToString(double unumero) {
        return String.valueOf(unumero);
    }

    public static final String convertirToString(int inumero) {
        return String.valueOf(inumero);
    }

    public static final double convertirToDouble(String snumero) {
        try {
            snumero = snumero.replaceAll(" ", "").replace(".", "").replace(",", ".");
            snumero = snumero.replace(",", ".");
            snumero = snumero.replace(".", "");
            return Double.parseDouble(snumero);
        } catch (Exception e) {
        }
        return 0.0D;
    }

    public static final double convertirToDoubleMac(String snumero) {
        try {
            snumero = snumero.replaceAll(" ", "").replace(",", "").replace(".", ",");
            snumero = snumero.replaceAll(",", ".");
            return Double.parseDouble(snumero);
        } catch (Exception e) {
        }
        return 0.0D;
    }

    public static final int convertirToInt(String snumero) {
        try {
            snumero = snumero.replaceAll(" ", "");
            snumero = snumero.replace(",", ".");
            snumero = snumero.replace(".", "");
            return Integer.parseInt(snumero);
        } catch (Exception e) {
        }
        return 0;
    }

    public static final int convertirToIntMac(String snumero) {
        try {
            snumero = snumero.replaceAll(" ", "").replace(",", "").replace(".", ",");
            snumero = snumero.replace(".", ",");
            snumero = snumero.replace(",", "");
            return Integer.parseInt(snumero);
        } catch (Exception e) {
        }
        return 0;
    }

    public static final double redondear(double numero, int cantidadDigitos) {
        int cifras = (int) Math.pow(10.0D, cantidadDigitos);
        return Math.rint(numero * cifras) / cifras;
    }

    public static final double redondearMas(double numero, int cantidadDigitos) {
        int cifras = (int) Math.pow(10.0D, cantidadDigitos);
        return Math.ceil(numero * cifras) / cifras;
    }

    public static final String getMascara(Object onumero, int idecimal) {
        String scadena = "";
        if ((onumero instanceof String)) {
            scadena = (String) onumero;
            for (int i = scadena.length() - 3; i > 0; i -= 3) {
                scadena = scadena.substring(0, i) + "." + scadena.substring(i, scadena.length());
            }
        }
        if (((onumero instanceof Double)) || ((onumero instanceof Integer))) {
            DecimalFormat formato = new DecimalFormat("#,###");
            if (idecimal > 0) {
                formato = new DecimalFormat("#,###." + "0000000000".substring(0, idecimal));
            }
            if ((onumero instanceof Double)) {
                return formato.format((Double) onumero);
            }
            if ((onumero instanceof Integer)) {
                return formato.format((Integer) onumero);
            }
        }
        return scadena;
    }

    public static final int obtenerDigitoVerificador(String snumero, int modulo) {
        String snumeroAuxiliar = "";
        for (int i = 0; i < snumero.length(); i++) {
            char c = snumero.charAt(i);
            if (Character.isDigit(c)) {
                snumeroAuxiliar = snumeroAuxiliar + c;
            } else {
                snumeroAuxiliar = snumeroAuxiliar + c;
            }
        }
        int k = 2;
        int itotal = 0;
        for (int i = snumeroAuxiliar.length() - 1; i >= 0; i--) {
            k = k > modulo ? 2 : k;
            int inumeroAuxiliar = snumeroAuxiliar.charAt(i) - '0';
            itotal += inumeroAuxiliar * k++;
        }
        int iresto = itotal % 11;
        int idigito = iresto > 1 ? 11 - iresto : 0;
        return idigito;
    }
}
