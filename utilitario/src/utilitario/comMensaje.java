/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Timer;
import org.netbeans.lib.awtextra.AbsoluteLayout;

public class comMensaje
{
  private JFrame frm = new JFrame();
  private JPanel pnl = new JPanel();
  private JDialog dlg = new JDialog(this.frm, true);
  public JTextArea txt = new JTextArea();
  private Timer timer;
  
  public comMensaje(Dimension dim, int itiempo)
  {
    iniciarComponentes(dim);
    this.timer = new Timer(itiempo, new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        comMensaje.this.cerrar();
      }
    });
  }
  
  public comMensaje(Dimension dim)
  {
    iniciarComponentes(dim);
  }
  
  private void iniciarComponentes(Dimension dim)
  {
    this.pnl.setLayout(new AbsoluteLayout());
    this.txt.setColumns(20);
    this.txt.setRows(5);
    this.txt.setSize(dim);
    this.txt.setEditable(false);
    this.txt.setFont(new Font("Arial", 0, 11));
    this.txt.setBorder(BorderFactory.createBevelBorder(0));
    this.txt.setBackground(new Color(204, 204, 204));
    this.dlg.add(this.txt);
    this.dlg.setUndecorated(true);
    this.dlg.setMinimumSize(dim);
    this.dlg.setLocation(new Point((Toolkit.getDefaultToolkit().getScreenSize().width - this.dlg.getSize().width) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - this.dlg.getSize().height) / 2));
  }
  
  public void desplegar(String msj)
  {
    this.txt.setText(msj);
    if (this.timer != null)
    {
      this.timer.setRepeats(false);
      this.timer.start();
    }
    this.dlg.setVisible(true);
  }
  
  public void desplegar()
  {
    if (this.timer != null)
    {
      this.timer.setRepeats(false);
      this.timer.start();
    }
    this.dlg.setVisible(true);
  }
  
  public void cerrar()
  {
    this.dlg.setVisible(false);
    this.dlg.dispose();
  }
}
