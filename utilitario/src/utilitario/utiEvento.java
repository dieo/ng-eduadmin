package utilitario;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JFormattedTextField;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextArea;

public class utiEvento
{
  private String stexto;
  public static final short edad_mayor = 18;
  
  public void agregarEventoFocusGained(Object obj)
  {
    if ((obj instanceof JFormattedTextField)) {
      ((JFormattedTextField)obj).addFocusListener(new FocusAdapter()
      {
        public void focusGained(FocusEvent evt)
        {
          utiEvento.this.campoFocusGained(evt);
        }
      });
    }
    if ((obj instanceof JPasswordField)) {
      ((JPasswordField)obj).addFocusListener(new FocusAdapter()
      {
        public void focusGained(FocusEvent evt)
        {
          utiEvento.this.campoFocusGained(evt);
        }
      });
    }
    if ((obj instanceof JTextArea)) {
      ((JTextArea)obj).addFocusListener(new FocusAdapter()
      {
        public void focusGained(FocusEvent evt)
        {
          utiEvento.this.campoFocusGained(evt);
        }
      });
    }
  }
  
  private void campoFocusGained(FocusEvent evt)
  {
    Object obj = evt.getSource();
    if ((evt.getSource() instanceof JFormattedTextField))
    {
      ((JFormattedTextField)obj).setSelectionStart(0);
      ((JFormattedTextField)obj).setSelectionEnd(((JFormattedTextField)obj).getText().length());
      this.stexto = ((JFormattedTextField)obj).getText();
    }
    if ((evt.getSource() instanceof JPasswordField))
    {
      ((JPasswordField)obj).setSelectionStart(0);
      ((JPasswordField)obj).setSelectionEnd(((JPasswordField)obj).getText().length());
      this.stexto = ((JPasswordField)obj).getText();
    }
    if ((evt.getSource() instanceof JTextArea))
    {
      ((JTextArea)obj).setSelectionStart(0);
      ((JTextArea)obj).setSelectionEnd(((JTextArea)obj).getText().length());
      this.stexto = ((JTextArea)obj).getText();
    }
  }
  
  public void agregarEventoKeyReleased(Object obj)
  {
    if ((obj instanceof JFormattedTextField)) {
      ((JFormattedTextField)obj).addKeyListener(new KeyAdapter()
      {
        public void keyReleased(KeyEvent evt)
        {
          utiEvento.this.campoKeyReleased(evt);
        }
      });
    }
    if ((obj instanceof JPasswordField)) {
      ((JPasswordField)obj).addKeyListener(new KeyAdapter()
      {
        public void keyReleased(KeyEvent evt)
        {
          utiEvento.this.campoKeyReleased(evt);
        }
      });
    }
    if ((obj instanceof JTextArea)) {
      ((JTextArea)obj).addKeyListener(new KeyAdapter()
      {
        public void keyReleased(KeyEvent evt)
        {
          utiEvento.this.campoKeyReleased(evt);
        }
      });
    }
  }
  
  private void campoKeyReleased(KeyEvent evt)
  {
    if (evt.getKeyCode() != 27) {
      return;
    }
    Object obj = evt.getSource();
    if ((evt.getSource() instanceof JFormattedTextField)) {
      ((JFormattedTextField)obj).setText(this.stexto);
    }
    if ((evt.getSource() instanceof JPasswordField)) {
      ((JPasswordField)obj).setText(this.stexto);
    }
    if ((evt.getSource() instanceof JTextArea)) {
      ((JTextArea)obj).setText(this.stexto);
    }
  }
  
  public void agregarEventoKeyPressed(Object obj)
  {
    if ((obj instanceof JTextArea)) {
      ((JTextArea)obj).addKeyListener(new KeyAdapter()
      {
        public void keyPressed(KeyEvent evt)
        {
          utiEvento.this.campoKeyPressed(evt);
        }
      });
    }
    if ((obj instanceof JTable)) {
      ((JTable)obj).addKeyListener(new KeyAdapter()
      {
        public void keyPressed(KeyEvent evt)
        {
          utiEvento.this.campoKeyPressed(evt);
        }
      });
    }
  }
  
  private void campoKeyPressed(KeyEvent evt)
  {
    if (evt.getKeyChar() == '\t')
    {
      Object obj = evt.getSource();
      if ((evt.getSource() instanceof JTextArea))
      {
        ((JTextArea)obj).setText(((JTextArea)obj).getText().trim());
        ((JTextArea)obj).transferFocus();
      }
      if ((evt.getSource() instanceof JTable)) {
        ((JTable)obj).transferFocus();
      }
    }
  }
}
