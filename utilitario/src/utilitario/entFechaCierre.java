/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utilitario;

import java.sql.ResultSet;
import java.util.Date;

public class entFechaCierre
{
  private int iid;
  private int iidGiraduria;
  private Date dperiodo;
  private Date dfechaProceso;
  private Date dfechaCorte;
  private Date dfechaRecibo;
  private Date dfechaContabilidad;
  private short hidEstado;
  
  public entFechaCierre()
  {
    this.iid = 0;
    this.iidGiraduria = 0;
    this.dperiodo = null;
    this.dfechaProceso = null;
    this.dfechaCorte = null;
    this.dfechaRecibo = null;
    this.dfechaContabilidad = null;
    this.hidEstado = 0;
  }
  
  public entFechaCierre(int iid, int iidGiraduria, Date dperiodo, Date dfechaProceso, Date dfechaCorte, Date dfechaCorteSiguiente, Date dfechaPrimerVencimiento, Date dfechaVencimiento, Date dfechaRecibo, Date dfechaContabilidad)
  {
    this.iid = iid;
    this.iidGiraduria = iidGiraduria;
    this.dperiodo = dperiodo;
    this.dfechaProceso = dfechaProceso;
    this.dfechaCorte = dfechaCorte;
    this.dfechaRecibo = dfechaRecibo;
    this.dfechaContabilidad = dfechaContabilidad;
    this.hidEstado = 0;
  }
  
  public void setEntidad(int iid, int iidGiraduria, Date dperiodo, Date dfechaProceso, Date dfechaCorte, Date dfechaRecibo, Date dfechaContabilidad, short hidEstado)
  {
    this.iid = iid;
    this.iidGiraduria = iidGiraduria;
    this.dperiodo = dperiodo;
    this.dfechaProceso = dfechaProceso;
    this.dfechaCorte = dfechaCorte;
    this.dfechaRecibo = dfechaRecibo;
    this.dfechaContabilidad = dfechaContabilidad;
    this.hidEstado = hidEstado;
  }
  
  public entFechaCierre copiar(entFechaCierre destino)
  {
    destino.setEntidad(getId(), getIdGiraduria(), getPeriodo(), getFechaProceso(), getFechaCorte(), getFechaRecibo(), getFechaContabilidad(), getEstadoRegistro());
    return destino;
  }
  
  public entFechaCierre cargar(ResultSet rs)
  {
    try
    {
      setId(rs.getInt("id"));
    }
    catch (Exception e) {}
    try
    {
      setIdGiraduria(rs.getShort("idgiraduria"));
    }
    catch (Exception e) {}
    try
    {
      setPeriodo(rs.getDate("periodo"));
    }
    catch (Exception e) {}
    try
    {
      setFechaProceso(rs.getDate("fechaproceso"));
    }
    catch (Exception e) {}
    try
    {
      setFechaCorte(rs.getDate("fechacorte"));
    }
    catch (Exception e) {}
    try
    {
      setFechaRecibo(rs.getDate("fecharecibo"));
    }
    catch (Exception e) {}
    try
    {
      setFechaContabilidad(rs.getDate("fechacontabilidad"));
    }
    catch (Exception e) {}
    return this;
  }
  
  public void setId(int iid)
  {
    this.iid = iid;
  }
  
  public void setIdGiraduria(int iidGiraduria)
  {
    this.iidGiraduria = iidGiraduria;
  }
  
  public void setPeriodo(Date dperiodo)
  {
    this.dperiodo = dperiodo;
  }
  
  public void setFechaProceso(Date dfechaProceso)
  {
    this.dfechaProceso = dfechaProceso;
  }
  
  public void setFechaCorte(Date dfechaCorte)
  {
    this.dfechaCorte = dfechaCorte;
  }
  
  public void setFechaRecibo(Date dfechaRecibo)
  {
    this.dfechaRecibo = dfechaRecibo;
  }
  
  public void setFechaContabilidad(Date dfechaContabilidad)
  {
    this.dfechaContabilidad = dfechaContabilidad;
  }
  
  public void setEstadoRegistro(short hidEstado)
  {
    this.hidEstado = hidEstado;
  }
  
  public int getId()
  {
    return this.iid;
  }
  
  public int getIdGiraduria()
  {
    return this.iidGiraduria;
  }
  
  public Date getPeriodo()
  {
    return this.dperiodo;
  }
  
  public Date getFechaProceso()
  {
    return this.dfechaProceso;
  }
  
  public Date getFechaCorte()
  {
    return this.dfechaCorte;
  }
  
  public Date getFechaRecibo()
  {
    return this.dfechaRecibo;
  }
  
  public Date getFechaContabilidad()
  {
    return this.dfechaContabilidad;
  }
  
  public short getEstadoRegistro()
  {
    return this.hidEstado;
  }
  
  public String getSentencia()
  {
    return "SELECT fechacierre(" + getId() + "," + utiFecha.getFechaGuardado(getFechaProceso()) + "," + utiFecha.getFechaGuardado(getFechaCorte()) + "," + utiFecha.getFechaGuardado(getFechaRecibo()) + "," + getIdGiraduria() + "," + utiFecha.getFechaGuardado(getFechaContabilidad()) + "," + getEstadoRegistro() + ")";
  }
}

