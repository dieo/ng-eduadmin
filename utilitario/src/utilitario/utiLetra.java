/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utilitario;

/**
 *
 * @author Informatica
 */
public class utiLetra {
    
    public utiLetra() { }
    
    public String leerNumero(Double unumero) {
        String snumero = convertirString(unumero);
        if (!esValido(snumero)) return "";
        if (!snumero.contains(",")) return leerMillar(snumero);
        int i=0;
        while(!snumero.substring(i,i+1).equals(",")) i++;
        String smillar = leerMillar(snumero.substring(0,i));
        String sagregado = "";
        if (smillar.equals("UN") || smillar.equals("CERO")) {
            if (smillar.equals("UN")) sagregado = "A UNIDAD";
            if (smillar.equals("CERO")) sagregado = " UNIDAD";
        } else {
            sagregado = " UNIDADES";
        }
        String sdecimal = leerDecimal(snumero.substring(i+1,snumero.length()));
        if (sdecimal.equals("")) return smillar;
        return smillar + sagregado + " " + sdecimal;
    }
    
    private String leerDecimal(String snumero) { //,123456789
        String sletraDecimal[] = {"DECIMAS","CENTESIMAS","MILESIMAS","DIEZMILESIMAS","CIENMILESIMAS","MILLONESIMAS","DIEZ MILLONESIMAS","CIEN MILLONESIMAS","MIL MILLONESIMAS"};
        String smillar = leerMillar(snumero);
        String sdecimal = sletraDecimal[snumero.length()-1];
        if (smillar.equals("CERO")) return "";
        return smillar + " " + sdecimal;
    }

    private String leerMillar(String snumero) { // 123 456.789 253.456 789.123 456.789 123.456 789
        String sletraMillar[] = {"QUINTILLONES","CUATRILLONES","TRILLONES","BILLONES","MILLONES"," "};
        String sletra = "";
        while (snumero.length()!=36) snumero = "0"+snumero;
        for (int i=0; i<snumero.length(); i+=6) {
            if (!snumero.substring(i,i+6).equals("000000")) {
                if (!snumero.substring(i,i+3).equals("000")) sletra += leerMil(snumero.substring(i,i+3)) + " MIL ";
                if (!snumero.substring(i+3,i+6).equals("000")) sletra += leerMil(snumero.substring(i+3,i+6)) + " ";
                sletra += sletraMillar[i/6] + " ";
                if (snumero.substring(i+3,i+6).equals("001")) sletra = sletra.substring(0,sletra.length()-3) + " ";
            } else {
                if (i==30 && sletra.trim().isEmpty()) {
                    sletra += leerMil(snumero.substring(i+3,i+6));
                }
            }
        }
        return sletra.trim();
    }
    
    private String leerMil(String smil) {
        while (smil.length()!=3) smil = "0"+smil;
        int icentena = convertirToInt(smil.substring(0,1));
        int idecena = convertirToInt(smil.substring(1,2));
        int iunidad = convertirToInt(smil.substring(2,3));
        String sletra = "";
        String sletraUnidad[] = {"CERO","UN","DOS","TRES","CUATRO","CINCO","SEIS","SIETE","OCHO","NUEVE"};
        String sletraEspecial[] = {"","ONCE","DOCE","TRECE","CATORCE","QUINCE"};
        String sletraDecena[] = {"","DIEZ","VEINTE","TREINTA","CUARENTA","CINCUENTA","SESENTA","SETENTA","OCHENTA","NOVENTA"};
        String sletraCentena[] = {"","CIENTO","DOSCIENTOS","TRESCIENTOS","CUATROCIENTOS","QUINIENTOS","SEISCIENTOS","SETECIENTOS","OCHOCIENTOS","NOVECIENTOS"};
        if (icentena==0 && idecena==0 && iunidad==0) return sletraUnidad[iunidad]; // 000
        if (icentena==1 && idecena==0 && iunidad==0) return sletraCentena[icentena].substring(0,4); // 100
        if (icentena>=1) sletra += sletraCentena[icentena]; // 100 200 300 400 500 600 700 800 900
        if (idecena==1 && iunidad>=1 && iunidad<=5) return (sletra + " " + sletraEspecial[iunidad]).trim(); // 11 12 13 14 15
        if (idecena>=1) sletra += " " + sletraDecena[idecena]; // 10
        if (idecena>0 && iunidad>0) sletra += " Y";
        if (iunidad>0) sletra += " " + sletraUnidad[iunidad]; // 1 2 3 4 5 6 7 8 9
        return sletra.trim();
    }
    
    private boolean esValido(String snumero) {
        if (snumero.contains(",")) {
            if (snumero.length() <= 17) return true;
        } else {
            if (snumero.length() <= 16) return true;
        }
        return false;
    }
    
    private String convertirString(Double unumero) {
        String snumero = convertirToString(unumero);
        int posComa = 0, posE = 0, cantDecimal = 0;
        snumero = snumero.replace(".",",");
        while (!snumero.substring(posComa, posComa+1).equals(",")) posComa++;
        if (snumero.contains("E")) while (!snumero.substring(posE, posE+1).equals("E")) posE++;
        if (posE>0) cantDecimal = convertirToInt(snumero.substring(posE+1,snumero.length()));
        if (posE>0) snumero = snumero.substring(0,posE);
        String sparteEntera = snumero.substring(0,posComa);
        String sparteDecimal = snumero.substring(posComa+1,snumero.length());
        if (cantDecimal>0) {
            int aux = cantDecimal - sparteDecimal.length();
            while (aux>0) { sparteDecimal += "0"; aux--; }
            if (sparteDecimal.length()>cantDecimal) snumero = sparteEntera + sparteDecimal.substring(0,cantDecimal) + "," + sparteDecimal.substring(cantDecimal,sparteDecimal.length());
            else snumero = sparteEntera + sparteDecimal;
        }
        if (cantDecimal<0) {
            int aux = cantDecimal * -1;
            while (aux>0) { sparteEntera = "0" + sparteEntera; aux--; }
            snumero = sparteEntera.substring(0,1) + "," + sparteEntera.substring(1,sparteEntera.length());
            if (!sparteDecimal.substring(0,sparteDecimal.length()).equals("0")) snumero += sparteDecimal;
        }
        return snumero;
    }

    private int convertirToInt(String snumero) {
        try {
            snumero = snumero.replaceAll(" ", "").replace(".", "").replace(",", ".");
            return Integer.parseInt(snumero);
        } catch (Exception e) {
            return 0;
        }
    }
    
    private String convertirToString(double dnumero) {
        return String.valueOf(dnumero);
    }
    
}
