package utilitario;

import java.util.Date;
import javax.swing.JTable;

public class utiGeneral
{
  public static final Date fechaInteresMoratorio = utiFecha.convertirToDate(new Date(2017, 4, 30));
  
  public static final String leyenda(boolean bmodo)
  {
    if (bmodo) {
      return "MODO EDICION (F2: MOD. - F3: INS. - F4: VIS. - F5: ELI.)";
    }
    return "MODO LECTURA";
  }
  
  public static final String leyendaMultilinea(boolean bmodo)
  {
    if (bmodo) {
      return "<html>MODO EDICION<P><html>- F2: MOD.<P><html>- F3: INS.<P><html>- F4: VIS.<P><html>- F5: ELI.<P>";
    }
    return "MODO LECTURA";
  }
  
  public static final int getPosicion(JTable tbl)
  {
    int posicion = tbl.getSelectedRow();
    if (posicion < 0) {
      posicion = 0;
    }
    return posicion;
  }
  
  public static final void setPosicion(JTable tbl, int posicion)
  {
    try
    {
      if (posicion >= tbl.getRowCount() - 1) {
        posicion = tbl.getRowCount() - 1;
      }
      tbl.setRowSelectionInterval(posicion, posicion);
    }
    catch (Exception e)
    {
      tbl.clearSelection();
    }
  }
  
  public static final boolean calculaMora(Date dfechaVencimiento)
  {
    if (utiFecha.getAMD(dfechaVencimiento) <= utiFecha.getAMD(fechaInteresMoratorio)) {
      return false;
    }
    return true;
  }
}
