/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import javax.swing.JOptionPane;

public class utiAviso
{
  public static final boolean advertirSesion()
  {
    if (JOptionPane.showConfirmDialog(null, "Iniciar� una sesi�n en otra Filial designada.\nEst� seguro?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirGeneracionSiguienteCuota()
  {
    if (JOptionPane.showConfirmDialog(null, "Realizar� el proceso de generaci�n de siguiente cuota de las operaciones con plazo indefinido.\nEst� seguro?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirAnulacionCheque()
  {
    if (JOptionPane.showConfirmDialog(null, "La anulaci�n de una Orden de Pago en estado CONFIRMADO anula el cheque asociado.\nEst� seguro?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirCantidadBoleta(int icantidad)
  {
    if (icantidad <= 10)
    {
      if (icantidad <= 0)
      {
        JOptionPane.showMessageDialog(null, "Boleta terminada...");
        return false;
      }
      JOptionPane.showMessageDialog(null, "Falta(n) " + icantidad + " boleta(s) para terminar...");
    }
    return true;
  }
  
  public static final boolean advertirCantidadMaximaImpresionCheque()
  {
    if (JOptionPane.showConfirmDialog(null, "Si sobrepasa la cantidad de impresi�n de 50 cheques, quedar� desfasado.\nEst� seguro?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirCancelacionCredito()
  {
    if (JOptionPane.showConfirmDialog(null, "El Tipo de Cr�dito no permite Cancelaci�n.\nEst� seguro de cancelar?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirCambioRubro()
  {
    if (JOptionPane.showConfirmDialog(null, "Proceder� a cambiar la v�a de cobro normal.\nEst� seguro?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertir(String stexto)
  {
    if (JOptionPane.showConfirmDialog(null, stexto, "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirEliminacion()
  {
    if (JOptionPane.showConfirmDialog(null, "Eliminar� el registro.\nEst� seguro?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirCancelacion()
  {
    if (JOptionPane.showConfirmDialog(null, "Tiene cambios pendientes sin guardar.\nEst� seguro de cancelar?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirSobreescritura()
  {
    if (JOptionPane.showConfirmDialog(null, "Sobreescribir� los datos actuales.\nEst� seguro de sobreescribir?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirActualizacion()
  {
    if (JOptionPane.showConfirmDialog(null, "Su sistema se actualizar�, posteriormente se reiniciar�.\nEst� seguro de actualizar?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
  
  public static final boolean advertirSalidaSistema()
  {
    if (JOptionPane.showConfirmDialog(null, "Desea salir del Sistema?", "Advertencia", 0) == 1) {
      return false;
    }
    return true;
  }
}
