package utilitario;

import java.sql.ResultSet;
import java.util.LinkedList;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class modFechaCierre
  implements TableModel
{
  private LinkedList data = new LinkedList();
  private LinkedList list = new LinkedList();
  
  public int getRowCount()
  {
    return this.data.size();
  }
  
  public int getColumnCount()
  {
    return 0;
  }
  
  public String getColumnName(int columnIndex)
  {
    switch (columnIndex)
    {
    }
    return null;
  }
  
  public Class<?> getColumnClass(int columnIndex)
  {
    switch (columnIndex)
    {
    }
    return Object.class;
  }
  
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }
  
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    entFechaCierre ent = (entFechaCierre)this.data.get(rowIndex);
    switch (columnIndex)
    {
    }
    return null;
  }
  
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    entFechaCierre ent = (entFechaCierre)this.data.get(rowIndex);
    switch (columnIndex)
    {
    }
    TableModelEvent evento = new TableModelEvent(this, rowIndex, rowIndex, columnIndex);
    avisaSuscriptores(evento);
  }
  
  public void addTableModelListener(TableModelListener l)
  {
    this.list.add(l);
  }
  
  public void removeTableModelListener(TableModelListener l)
  {
    this.list.remove(l);
  }
  
  public entFechaCierre getEntidad(int rowIndex)
  {
    return (entFechaCierre)this.data.get(rowIndex);
  }
  
  private void avisaSuscriptores(TableModelEvent evento)
  {
    for (int i = 0; i < this.list.size(); i++) {
      ((TableModelListener)this.list.get(i)).tableChanged(evento);
    }
  }
  
  public void insertar(entFechaCierre nuevo)
  {
    try
    {
      this.data.add(nuevo);
      
      TableModelEvent evento = new TableModelEvent(this, getRowCount() - 1, getRowCount() - 1, -1, 1);
      avisaSuscriptores(evento);
    }
    catch (Exception e) {}
  }
  
  public void modificar(entFechaCierre nuevo, int rowIndex)
  {
    nuevo.copiar((entFechaCierre)this.data.get(rowIndex));
    
    TableModelEvent evento = new TableModelEvent(this, getRowCount() - 1, getRowCount() - 1, -1, 0);
    avisaSuscriptores(evento);
  }
  
  public void eliminar(int fila)
  {
    try
    {
      this.data.remove(fila);
      TableModelEvent evento = new TableModelEvent(this, fila, fila, -1, -1);
      avisaSuscriptores(evento);
    }
    catch (Exception e) {}
  }
  
  public void removerTodo()
  {
    this.data.removeAll(this.data);
  }
  
  public void cargarTable(ResultSet rs)
  {
    removerTodo();
    try
    {
      rs.beforeFirst();
      while (rs.next()) {
        insertar(new entFechaCierre().cargar(rs));
      }
    }
    catch (Exception e) {}
  }
  
  public void establecerFormato(JTable tbl)
  {
    tbl.getColumnModel().getColumn(0).setPreferredWidth(300);
  }
}
