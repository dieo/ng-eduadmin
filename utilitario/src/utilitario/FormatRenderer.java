/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import java.text.DateFormat;
import java.text.Format;
import javax.swing.table.DefaultTableCellRenderer;

public class FormatRenderer
  extends DefaultTableCellRenderer
{
  private Format formatter;
  
  public FormatRenderer(Format formatter)
  {
    this.formatter = formatter;
  }
  
  public void setValue(Object value)
  {
    try
    {
      if (value != null) {
        value = this.formatter.format(value);
      }
    }
    catch (IllegalArgumentException e) {}
    super.setValue(value);
  }
  
  public static FormatRenderer getDateTimeRenderer()
  {
    return new FormatRenderer(DateFormat.getDateTimeInstance());
  }
  
  public static FormatRenderer getTimeRenderer()
  {
    return new FormatRenderer(DateFormat.getTimeInstance());
  }
}