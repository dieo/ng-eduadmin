package utilitario;

import java.sql.ResultSet;

public class entLista
{
  public static final short tipo_texto = 0;
  public static final short tipo_numero = 1;
  public static final short tipo_fecha = 2;
  private int iid;
  private String sdescripcion;
  private String scampo;
  private short htipo;
  
  public entLista()
  {
    this.iid = 0;
    this.sdescripcion = "";
    this.scampo = "";
    this.htipo = 0;
  }
  
  public entLista(int iid, String sdescripcion, String scampo, short htipo)
  {
    this.iid = iid;
    this.sdescripcion = sdescripcion;
    this.scampo = scampo;
    this.htipo = htipo;
  }
  
  public entLista cargar(ResultSet rs)
  {
    try
    {
      setId(rs.getInt("id"));
    }
    catch (Exception e) {}
    try
    {
      setDescripcion(rs.getString("descripcion"));
    }
    catch (Exception e) {}
    try
    {
      setCampo("");
    }
    catch (Exception e) {}
    try
    {
      setTipo((short)0);
    }
    catch (Exception e) {}
    return this;
  }
  
  public String toString()
  {
    return getDescripcion();
  }
  
  public void setId(int iid)
  {
    this.iid = iid;
  }
  
  public void setDescripcion(String sdescripcion)
  {
    this.sdescripcion = sdescripcion;
  }
  
  public void setCampo(String scampo)
  {
    this.scampo = scampo;
  }
  
  public void setTipo(short htipo)
  {
    this.htipo = htipo;
  }
  
  public int getId()
  {
    return this.iid;
  }
  
  public String getDescripcion()
  {
    return this.sdescripcion;
  }
  
  public String getCampo()
  {
    return this.scampo;
  }
  
  public short getTipo()
  {
    return this.htipo;
  }
}
