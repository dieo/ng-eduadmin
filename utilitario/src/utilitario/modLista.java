package utilitario;

import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public class modLista
  implements ComboBoxModel
{
  private ArrayList<entLista> data = new ArrayList();
  private ArrayList<ListDataListener> list = new ArrayList();
  private entLista selectedItem;
  
  public void setSelectedItem(Object anItem)
  {
    this.selectedItem = ((anItem instanceof entLista) ? (entLista)anItem : null);
    for (ListDataListener l : this.list) {
      l.contentsChanged(new ListDataEvent(this, 0, 0, 0));
    }
  }
  
  public Object getSelectedItem()
  {
    return this.selectedItem;
  }
  
  public int getSize()
  {
    return this.data.size();
  }
  
  public Object getElementAt(int index)
  {
    return this.data.get(index);
  }
  
  public void addListDataListener(ListDataListener l)
  {
    this.list.add(l);
  }
  
  public void removeListDataListener(ListDataListener l)
  {
    this.list.remove(l);
  }
  
  public entLista getEntidad(int index)
  {
    return (entLista)this.data.get(index);
  }
  
  public entLista getEntidad()
  {
    return this.selectedItem == null ? null : this.selectedItem;
  }
  
  public void removerTodo()
  {
    this.data.removeAll(this.data);
  }
  
  public void cargarModelo(ArrayList<entLista> lst)
  {
    for (int i = 0; i < lst.size(); i++) {
      this.data.add(lst.get(i));
    }
  }
  
  public void agregarItem(entLista ent)
  {
    this.data.add(ent);
  }
  
  public void eliminarItem(int i)
  {
    this.data.remove(i);
  }
  
  public void cargarModelo(ResultSet rs)
  {
    removerTodo();
    try
    {
      rs.beforeFirst();
      while (rs.next()) {
        this.data.add(new entLista().cargar(rs));
      }
    }
    catch (Exception e) {}
  }
  
  public void cargarModelo(entLista ent)
  {
    this.data.add(ent);
  }
  
  public entLista setItem(int id)
  {
    for (entLista ent : this.data) {
      if (ent.getId() == id) {
        return ent;
      }
    }
    return null;
  }
  
  public short getId(String sdescripcion)
  {
    for (int i = 0; i < getSize(); i++) {
      if (getEntidad(i).getDescripcion().equals(sdescripcion)) {
        return (short)getEntidad(i).getId();
      }
    }
    return 0;
  }
  
  public String getDescripcion(short hid)
  {
    for (int i = 0; i < getSize(); i++) {
      if (getEntidad(i).getId() == hid) {
        return getEntidad(i).getDescripcion();
      }
    }
    return "";
  }
}
